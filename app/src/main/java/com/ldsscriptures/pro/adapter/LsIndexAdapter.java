package com.ldsscriptures.pro.adapter;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.ldsscriptures.pro.R;
import com.ldsscriptures.pro.activity.ReadingActivity;
import com.ldsscriptures.pro.activity.SubIndexActivity;
import com.ldsscriptures.pro.model.LsMainIndexData;
import com.ldsscriptures.pro.model.LsSubIndexData;
import com.ldsscriptures.pro.model.MoreOptionMenuData;
import com.ldsscriptures.pro.utils.Global;

import java.util.ArrayList;
import java.util.List;


public class LsIndexAdapter extends RecyclerView.Adapter<LsIndexAdapter.MyViewHolder> {

    private final String lcID;
    private final String lsId;
    private List<LsMainIndexData> indexList;
    private Activity activity;
    public static ArrayList<LsSubIndexData> subIndexList = new ArrayList<>();

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView tvIndexItem;
        RelativeLayout relMain;

        public MyViewHolder(View view) {
            super(view);
            tvIndexItem = (TextView) view.findViewById(R.id.tvVerse);
            relMain = (RelativeLayout) view.findViewById(R.id.relMain);
        }
    }

    public LsIndexAdapter(Activity activity, ArrayList<LsMainIndexData> indexList, String lcID, String lsId) {
        this.indexList = indexList;
        this.activity = activity;
        this.lcID = lcID;
        this.lsId = lsId;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, final int position) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_ls_index, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {

        holder.tvIndexItem.setText(indexList.get(position).getTitle_html());

        holder.relMain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                subIndexList = indexList.get(position).getLsSubIndexDatas();

                Global.bookmarkData.setChapterUri(indexList.get(position).getUri());

                if (subIndexList != null) {
                    // set parameter for more option
                    Global.moreOptionMenuDatas.set(2, new MoreOptionMenuData(lcID, lsId, indexList.get(position).getId(), indexList.get(position).getTitle_html()));

                    Intent intent = new Intent(activity, SubIndexActivity.class);
                    intent.putExtra(activity.getString(R.string.indexTitle), indexList.get(position).getTitle_html());
                    intent.putExtra(activity.getString(R.string.SubIndexPosition), indexList.get(position).getId());
                    intent.putExtra(activity.getString(R.string.lcID), lcID);
                    intent.putExtra(activity.getString(R.string.lsId), lsId);
                    activity.startActivity(intent);
                    activity.overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);

                } else {

                    // set parameter for more option
                    Global.moreOptionMenuDatas.set(3, new MoreOptionMenuData(lcID, lsId, indexList.get(position).getId(), false, true, position));


                    Intent intent = new Intent(activity, ReadingActivity.class);
                    intent.putExtra(activity.getString(R.string.indexForReading), indexList.get(position).getId());
                    intent.putExtra(activity.getString(R.string.isFromIndex), true);
                    intent.putExtra(activity.getString(R.string.is_history), false);
                    intent.putExtra(activity.getString(R.string.lcID), lcID);
                    intent.putExtra(activity.getString(R.string.lsId), lsId);
                    activity.startActivity(intent);

                    activity.overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return indexList.size();
    }
}