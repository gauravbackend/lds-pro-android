package com.ldsscriptures.pro.adapter;

import android.app.Activity;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.daimajia.swipe.SwipeLayout;
import com.daimajia.swipe.adapters.RecyclerSwipeAdapter;
import com.ldsscriptures.pro.R;
import com.ldsscriptures.pro.activity.menuscreens.MenuNotesActivity;
import com.ldsscriptures.pro.database.CommonDatabaseHelper;
import com.ldsscriptures.pro.model.BookmarkData;
import com.ldsscriptures.pro.utils.PushToServerData;
import com.ldsscriptures.pro.utils.SessionManager;

import java.util.ArrayList;

public class MenuNotesListAdapter extends RecyclerSwipeAdapter<MenuNotesListAdapter.MyViewHolder> {

    private ArrayList<BookmarkData> notesArrayList = new ArrayList<>();
    private Activity activity;
    CommonDatabaseHelper commonDatabaseHelper;
    SessionManager sessionManager;

    @Override
    public int getSwipeLayoutResourceId(int position) {
        return R.id.noteswipe;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        private TextView tvTitleNotes;
        private TextView tvVerseNotes;
        SwipeLayout swipeLayout;
        TextView tvDelete;

        public MyViewHolder(View view) {
            super(view);
            tvTitleNotes = (TextView) view.findViewById(R.id.tvTitleNotes);
            tvVerseNotes = (TextView) view.findViewById(R.id.tvVerseNotes);
            swipeLayout = (SwipeLayout) itemView.findViewById(R.id.noteswipe);
            tvDelete = (TextView) itemView.findViewById(R.id.tvDelete_note);
        }
    }

    public MenuNotesListAdapter(Activity activity, ArrayList<BookmarkData> notesArrayList) {
        this.notesArrayList = notesArrayList;
        this.activity = activity;
        commonDatabaseHelper = new CommonDatabaseHelper(activity);
        sessionManager = new SessionManager(activity);
    }

    @Override
    public MenuNotesListAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, final int position) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_notes_inmenu, parent, false);
        return new MenuNotesListAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MenuNotesListAdapter.MyViewHolder holder, final int position) {
        holder.tvTitleNotes.setText(notesArrayList.get(position).getTitle());
        holder.tvVerseNotes.setText(notesArrayList.get(position).getVerseText());


        holder.swipeLayout.setShowMode(SwipeLayout.ShowMode.PullOut);
        // Drag From Right
        holder.swipeLayout.addDrag(SwipeLayout.DragEdge.Right, holder.swipeLayout.findViewById(R.id.bottom_wrapper_note));
        // Handling different events when swiping
        holder.swipeLayout.addSwipeListener(new SwipeLayout.SwipeListener() {
            @Override
            public void onClose(SwipeLayout layout) {
                //when the SurfaceView totally cover the BottomView.
            }

            @Override
            public void onUpdate(SwipeLayout layout, int leftOffset, int topOffset) {
                //you are swiping.
            }

            @Override
            public void onStartOpen(SwipeLayout layout) {

            }

            @Override
            public void onOpen(SwipeLayout layout) {
                //when the BottomView totally show.
            }

            @Override
            public void onStartClose(SwipeLayout layout) {

            }

            @Override
            public void onHandRelease(SwipeLayout layout, float xvel, float yvel) {
                //when user's hand released.
            }
        });
        holder.tvDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                final String paraUri = notesArrayList.get(position).getItemuri().toString().trim();
                final String paraVerseId = notesArrayList.get(position).getVerseNumber().toString().trim();
                final String paraVerseAId = notesArrayList.get(position).getVerseaid().toString().trim();
                final String paraNoteTitle = notesArrayList.get(position).getTitle().toString().trim();
                final String paraNote = notesArrayList.get(position).getVerseText().toString().trim();
                final String serverid = notesArrayList.get(position).getServerid().toString().trim();

                try {
                    android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(activity);
                    builder.setTitle("Warning");
                    builder.setMessage(R.string.are_you_sure_delete)

                            .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            })
                            .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    // Yes button clicked, do something
                                    dialog.dismiss();
                                    deleteLocally(holder, position, notesArrayList, paraVerseAId);
                                    new PushToServerData(activity);
                                }
                            });

                    builder.show();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return notesArrayList.size();
    }

    public void deleteLocally(final MyViewHolder holder,
                              final int position,
                              final ArrayList<BookmarkData> notesArrayList,
                              final String paraVerseAId) {
        mItemManger.removeShownLayouts(holder.swipeLayout);
        notesArrayList.remove(position);
        notifyItemRemoved(position);
        notifyItemRangeChanged(position, notesArrayList.size());
        mItemManger.closeAllItems();
       /* commonDatabaseHelper.deleteRowData(CommonDatabaseHelper.TABLE_NOTE_MARSTER, commonDatabaseHelper.KEY_VERSEAID2 + " = ?",
                new String[]{paraVerseAId});*/

        ContentValues contentValues = new ContentValues();
        contentValues.put(CommonDatabaseHelper.KEY_DELETED, "1");
        commonDatabaseHelper.updateRowData(CommonDatabaseHelper.TABLE_NOTE_MARSTER, contentValues, CommonDatabaseHelper.KEY_VERSEAID2 + " = ?",
                new String[]{String.valueOf(paraVerseAId)});


        if (notesArrayList.size() == 0) {
            MenuNotesActivity.getInstance().visibleHideView();
        }
    }

}

