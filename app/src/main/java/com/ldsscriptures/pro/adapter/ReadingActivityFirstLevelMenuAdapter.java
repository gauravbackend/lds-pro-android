package com.ldsscriptures.pro.adapter;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.ldsscriptures.pro.R;
import com.ldsscriptures.pro.activity.ReadingActivity;
import com.ldsscriptures.pro.dbClass.GetDataFromDatabase;
import com.ldsscriptures.pro.model.LsIndexPreviewData;
import com.ldsscriptures.pro.model.LsMainIndexData;

import java.util.ArrayList;


public class ReadingActivityFirstLevelMenuAdapter extends RecyclerView.Adapter<ReadingActivityFirstLevelMenuAdapter.MyViewHolder> {

    private Activity activity;
    private ArrayList<LsMainIndexData> lsDataArrayList = new ArrayList<>();
    private String lsId;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView textView;
        public LinearLayout linDropDown;

        public MyViewHolder(View view) {
            super(view);
            textView = (TextView) view.findViewById(R.id.textView);
            linDropDown = (LinearLayout) view.findViewById(R.id.linDropDown);
        }
    }


    public ReadingActivityFirstLevelMenuAdapter(Activity activity, ArrayList<LsMainIndexData> lsDataArrayList, String lsId) {
        this.lsDataArrayList = lsDataArrayList;
        this.activity = activity;
        this.lsId = lsId;
    }

    @Override
    public ReadingActivityFirstLevelMenuAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, final int position) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_dropdown_menu, parent, false);
        return new ReadingActivityFirstLevelMenuAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ReadingActivityFirstLevelMenuAdapter.MyViewHolder holder, final int position) {

        holder.textView.setTextColor(activity.getResources().getColor(R.color.navIndexText));
        holder.textView.setText(lsDataArrayList.get(position).getTitle_html());

        holder.textView.setOnClickListener(new View.OnClickListener() {
            public ArrayList<LsIndexPreviewData> indexArray;

            @Override
            public void onClick(View v) {
                ArrayList<LsIndexPreviewData> arrayList = new GetDataFromDatabase(activity, lsId).GetIndexPreviewDataFromDB((lsDataArrayList.get(position).getId()));
                if (ReadingActivity.getInstance() != null)
                    ReadingActivity.getInstance().setSecondLevel(arrayList);
            }
        });
    }

    @Override
    public int getItemCount() {
        return lsDataArrayList.size();
    }
}