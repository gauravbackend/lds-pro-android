package com.ldsscriptures.pro.adapter;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.ldsscriptures.pro.R;
import com.ldsscriptures.pro.activity.LibraryIndexActivity;
import com.ldsscriptures.pro.activity.SubIndexActivity;
import com.ldsscriptures.pro.dbClass.GetDataFromDatabase;
import com.ldsscriptures.pro.dbClass.GetLibCollectionFromDB;
import com.ldsscriptures.pro.model.Item;
import com.ldsscriptures.pro.model.LsData;
import com.ldsscriptures.pro.model.LsFullIndexData;
import com.ldsscriptures.pro.model.LsMainIndexData;
import com.ldsscriptures.pro.utils.DownloadSubDBZipFile;
import com.ldsscriptures.pro.utils.Global;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;


public class SubIndexFirstLevelMenuAdapter extends RecyclerView.Adapter<SubIndexFirstLevelMenuAdapter.MyViewHolder> {

    private final String LcID;
    private Activity activity;
    private ArrayList<LsData> lsDataArrayList = new ArrayList<>();
    ArrayList<LsMainIndexData> combineIndexDataArray = new ArrayList<>();
    private ArrayList<Item> dbCatelogArraylist = new ArrayList<>();

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView textView;
        public LinearLayout linDropDown;

        public MyViewHolder(View view) {
            super(view);
            textView = (TextView) view.findViewById(R.id.textView);
            linDropDown = (LinearLayout) view.findViewById(R.id.linDropDown);
        }
    }


    public SubIndexFirstLevelMenuAdapter(Activity activity, ArrayList<LsData> lsDataArrayList, String LcID) {
        this.lsDataArrayList = lsDataArrayList;
        this.activity = activity;
        this.LcID = LcID;
        dbCatelogArraylist = Global.getListSharedPreferneces(activity);
    }

    @Override
    public SubIndexFirstLevelMenuAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, final int position) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_dropdown_menu, parent, false);
        return new SubIndexFirstLevelMenuAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(SubIndexFirstLevelMenuAdapter.MyViewHolder holder, final int position) {

        holder.textView.setTextColor(activity.getResources().getColor(R.color.drop_down_second_level_text));
        holder.textView.setText(lsDataArrayList.get(position).getTitle());

        holder.linDropDown.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (dbCatelogArraylist != null) {

                    String item_id = String.valueOf(lsDataArrayList.get(position).getItem_id());

                    for (int i6 = 0; i6 < dbCatelogArraylist.size(); i6++) {
                        if (dbCatelogArraylist.get(i6).getId().equals(item_id)) {

                            if (!isFolderExists(dbCatelogArraylist.get(i6).getId())) {
                                new DownloadFileFromURL().execute(dbCatelogArraylist.get(i6).getUrl(), dbCatelogArraylist.get(i6).getId());

                            } else {
                                ArrayList<LsFullIndexData> lsFullIndexDataArray = new GetDataFromDatabase(activity, item_id).GetMainDataFromDB();

                                combineIndexDataArray = new ArrayList<>();
                                if (lsFullIndexDataArray != null) {
                                    for (int i = 0; i < lsFullIndexDataArray.size(); i++) {
                                        for (int j = 0; j < lsFullIndexDataArray.get(i).getTitleIndexDatas().size(); j++) {
                                            LsMainIndexData mainIndexData = new LsMainIndexData();
                                            mainIndexData.setId(lsFullIndexDataArray.get(i).getTitleIndexDatas().get(j).getId());
                                            mainIndexData.setNav_section_id(lsFullIndexDataArray.get(i).getTitleIndexDatas().get(j).getNav_section_id());
                                            mainIndexData.setPosition(lsFullIndexDataArray.get(i).getTitleIndexDatas().get(j).getPosition());
                                            mainIndexData.setImage_renditions(lsFullIndexDataArray.get(i).getTitleIndexDatas().get(j).getImage_rendition());
                                            mainIndexData.setTitle_html(lsFullIndexDataArray.get(i).getTitleIndexDatas().get(j).getTitle_html());
                                            mainIndexData.setSubtitle(lsFullIndexDataArray.get(i).getTitleIndexDatas().get(j).getSubtitle());
                                            mainIndexData.setUri(lsFullIndexDataArray.get(i).getTitleIndexDatas().get(j).getUri());
                                            mainIndexData.setLsSubIndexDatas(null);
                                            combineIndexDataArray.add(mainIndexData);
                                        }
                                        combineIndexDataArray.addAll(lsFullIndexDataArray.get(i).getItemMainIndexArray());
                                    }

                                    Collections.sort(combineIndexDataArray, new Comparator<LsMainIndexData>() {
                                        @Override
                                        public int compare(LsMainIndexData lhs, LsMainIndexData rhs) {
                                            return Integer.valueOf(lhs.getId()).compareTo(rhs.getId());
                                        }
                                    });
                                }

                                if (SubIndexActivity.getInstance() != null) {
                                    SubIndexActivity.getInstance().setSecondLevel(combineIndexDataArray, item_id);
                                }
                            }
                        }
                    }
                }
            }
        });
    }


    private boolean isFolderExists(String id) {
        File destDir = new File(Global.AppFolderPath + id);

        if (!destDir.exists()) {
            return false;
        } else {
            if (destDir.isDirectory()) {
                String[] files = destDir.list();
                if (files == null)
                    return false;

                if (files.length == 0) {
                    //directory is empty
                    return false;
                } else
                    return true;
            }
        }
        return false;
    }

    @Override
    public int getItemCount() {
        return lsDataArrayList.size();
    }

    class DownloadFileFromURL extends AsyncTask<String, String, String> {

        String item_id = "";

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            SubIndexActivity.getInstance().showprogress();
        }

        @Override
        protected String doInBackground(String... f_url) {
            int count;
            try {

                item_id = f_url[1];


                new DownloadSubDBZipFile(activity, f_url[0], item_id);

                URL url = new URL(f_url[0]);
                URLConnection conection = url.openConnection();
                conection.connect();
                // getting file length
                int lenghtOfFile = conection.getContentLength();

                // input stream to read file - with 8k buffer
                InputStream input = new BufferedInputStream(url.openStream(), 8192);

                // Output stream to write file
                OutputStream output = new FileOutputStream("/sdcard/downloadedfile.jpg");

                byte data[] = new byte[1024];

                long total = 0;

                while ((count = input.read(data)) != -1) {
                    total += count;
                    // publishing the progress....
                    // After this onProgressUpdate will be called
                    publishProgress("" + (int) ((total * 100) / lenghtOfFile));

                    // writing data to file
                    output.write(data, 0, count);
                }

                // flushing output
                output.flush();

                // closing streams
                output.close();
                input.close();

            } catch (Exception e) {
                Log.e("Error: ", e.getMessage());
            }

            return null;
        }

        /**
         * Updating progress bar
         */
        protected void onProgressUpdate(String... progress) {
            // setting progress percentage
            //pDialog.setProgress(Integer.parseInt(progress[0]));
        }

        /**
         * After completing background task
         * Dismiss the progress dialog
         **/
        @Override
        protected void onPostExecute(String file_url) {
            // dismiss the dialog after the file was downloaded
            SubIndexActivity.getInstance().dismissprogress();

            ArrayList<LsFullIndexData> lsFullIndexDataArray = new GetDataFromDatabase(activity, item_id).GetMainDataFromDB();

            combineIndexDataArray = new ArrayList<>();
            if (lsFullIndexDataArray != null) {
                for (int i = 0; i < lsFullIndexDataArray.size(); i++) {
                    for (int j = 0; j < lsFullIndexDataArray.get(i).getTitleIndexDatas().size(); j++) {
                        LsMainIndexData mainIndexData = new LsMainIndexData();
                        mainIndexData.setId(lsFullIndexDataArray.get(i).getTitleIndexDatas().get(j).getId());
                        mainIndexData.setNav_section_id(lsFullIndexDataArray.get(i).getTitleIndexDatas().get(j).getNav_section_id());
                        mainIndexData.setPosition(lsFullIndexDataArray.get(i).getTitleIndexDatas().get(j).getPosition());
                        mainIndexData.setImage_renditions(lsFullIndexDataArray.get(i).getTitleIndexDatas().get(j).getImage_rendition());
                        mainIndexData.setTitle_html(lsFullIndexDataArray.get(i).getTitleIndexDatas().get(j).getTitle_html());
                        mainIndexData.setSubtitle(lsFullIndexDataArray.get(i).getTitleIndexDatas().get(j).getSubtitle());
                        mainIndexData.setUri(lsFullIndexDataArray.get(i).getTitleIndexDatas().get(j).getUri());
                        mainIndexData.setLsSubIndexDatas(null);
                        combineIndexDataArray.add(mainIndexData);
                    }
                    combineIndexDataArray.addAll(lsFullIndexDataArray.get(i).getItemMainIndexArray());
                }

                Collections.sort(combineIndexDataArray, new Comparator<LsMainIndexData>() {
                    @Override
                    public int compare(LsMainIndexData lhs, LsMainIndexData rhs) {
                        return Integer.valueOf(lhs.getId()).compareTo(rhs.getId());
                    }
                });
            }

            if (SubIndexActivity.getInstance() != null) {
                SubIndexActivity.getInstance().setSecondLevel(combineIndexDataArray, item_id);
            }
        }
    }


}