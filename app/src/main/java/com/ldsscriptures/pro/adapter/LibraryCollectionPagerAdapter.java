package com.ldsscriptures.pro.adapter;


import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.ldsscriptures.pro.fragment.BookmarksFragment;
import com.ldsscriptures.pro.fragment.HistoryFragment;
import com.ldsscriptures.pro.fragment.LibraryCollectionFragment;


public class LibraryCollectionPagerAdapter extends FragmentStatePagerAdapter {
    int tabCount;

    public LibraryCollectionPagerAdapter(FragmentManager fm, int tabCount) {
        super(fm);
        this.tabCount = tabCount;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return new LibraryCollectionFragment();
            case 1:
                return new BookmarksFragment();
            case 2:
                return new HistoryFragment();
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return tabCount;
    }
}