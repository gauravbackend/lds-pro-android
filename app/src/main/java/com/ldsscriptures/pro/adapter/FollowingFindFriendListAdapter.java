package com.ldsscriptures.pro.adapter;

import android.app.Activity;
import android.content.ContentValues;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.ldsscriptures.pro.BuildConfig;
import com.ldsscriptures.pro.R;
import com.ldsscriptures.pro.activity.socialmenu.FollowingFindFriendActivity;
import com.ldsscriptures.pro.database.CommonDatabaseHelper;
import com.ldsscriptures.pro.model.EmailFriendData;
import com.ldsscriptures.pro.model.FollowingModel;
import com.ldsscriptures.pro.retroutils.APIClient;
import com.ldsscriptures.pro.retroutils.APIInterface;
import com.ldsscriptures.pro.utils.SessionManager;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.ldsscriptures.pro.retroutils.APIClient.DEVICE_KEY;

public class FollowingFindFriendListAdapter extends RecyclerView.Adapter<FollowingFindFriendListAdapter.ViewHolder> {

    private ArrayList<EmailFriendData> followingarraylist;
    private Activity activity;
    private SessionManager sessionManager;
    String paraUserId, paraSessionId;
    CommonDatabaseHelper commonDatabaseHelper;

    public FollowingFindFriendListAdapter(Activity activity, ArrayList<EmailFriendData> followingarraylist) {
        this.followingarraylist = followingarraylist;
        this.activity = activity;
        sessionManager = new SessionManager(activity);
        commonDatabaseHelper = new CommonDatabaseHelper(activity);

    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.activity_following_list_item, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder viewHolder, int i) {

        final String following_user_id = followingarraylist.get(i).getFriend_user_id();
        String following_username = followingarraylist.get(i).getFriend_name();
        String friend_img_url = followingarraylist.get(i).getFriend_img_url();
        Boolean friend_flag = followingarraylist.get(i).getFriend_flag();
        Boolean friend_invite_flag = followingarraylist.get(i).getFriend_invite_flag();
        String followed_by = followingarraylist.get(i).getFollowed_by();
        String following = followingarraylist.get(i).getFollowing();
        final String following_user_email = followingarraylist.get(i).getFriend_email();
        final int following_pos = i;

        if (FollowingFindFriendActivity.getInstance().getUserInfo(SessionManager.KEY_USER_ID) != null) {
            paraUserId = FollowingFindFriendActivity.getInstance().getUserInfo(SessionManager.KEY_USER_ID);
        }
        if (FollowingFindFriendActivity.getInstance().getUserInfo(SessionManager.KEY_USER_SESSION_ID) != null) {
            paraSessionId = FollowingFindFriendActivity.getInstance().getUserInfo(SessionManager.KEY_USER_SESSION_ID);
        }

        if (friend_flag == false) {
            viewHolder.localFollowLayout.setVisibility(View.VISIBLE);
            viewHolder.serverFollowLayout.setVisibility(View.GONE);
            viewHolder.followingLevel.setText(followingarraylist.get(i).getFriend_level());


            if (friend_invite_flag == false) {
                viewHolder.followInvite.setVisibility(View.VISIBLE);
                viewHolder.followInvited.setVisibility(View.GONE);
            } else {
                viewHolder.followInvite.setVisibility(View.GONE);
                viewHolder.followInvited.setVisibility(View.VISIBLE);
            }

        } else {
            viewHolder.localFollowLayout.setVisibility(View.GONE);
            viewHolder.serverFollowLayout.setVisibility(View.VISIBLE);
            viewHolder.followingLevel.setText("level" + " " + followingarraylist.get(i).getFriend_level());

            if (followed_by.equals("true") && following.equals("true")) {
                viewHolder.followedFriends.setVisibility(View.VISIBLE);
                viewHolder.addFriends.setVisibility(View.GONE);
                viewHolder.txt_following.setVisibility(View.GONE);
            } else {

                if (following.equals("true")) {
                    viewHolder.txt_following.setVisibility(View.VISIBLE);
                    viewHolder.followedFriends.setVisibility(View.GONE);
                    viewHolder.addFriends.setVisibility(View.GONE);
                } else {
                    viewHolder.followedFriends.setVisibility(View.GONE);
                    viewHolder.addFriends.setVisibility(View.VISIBLE);
                    viewHolder.txt_following.setVisibility(View.GONE);
                }
            }
        }

        if (friend_img_url != null) {
            Picasso.with(activity).load(friend_img_url).into(viewHolder.contactImage);
        } else {
            viewHolder.contactImage.setBackgroundResource(R.drawable.profile_lg);
        }


        viewHolder.followingName.setText(following_username);
        viewHolder.followInvite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                inviteFriendAPI(paraUserId,
                        paraSessionId,
                        following_user_email,
                        following_pos,
                        viewHolder.followInvite,
                        viewHolder.followInvited);
            }
        });


        viewHolder.addFriends.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                followUnfollowFriendAPI(paraUserId,
                        paraSessionId,
                        following_user_id,
                        "follow",
                        following_pos,
                        viewHolder.addFriends,
                        viewHolder.followedFriends,
                        viewHolder.txt_following);
            }
        });

        viewHolder.txt_following.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                followUnfollowFriendAPI(paraUserId,
                        paraSessionId,
                        following_user_id,
                        "unfollow",
                        following_pos,
                        viewHolder.addFriends,
                        viewHolder.followedFriends,
                        viewHolder.txt_following);
            }
        });

        viewHolder.followedFriends.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                viewHolder.txt_following.callOnClick();
            }
        });
    }

    @Override
    public int getItemCount() {
        return followingarraylist.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private CircleImageView contactImage;
        private TextView followingName;
        private TextView followingLevel;
        private ImageView followedFriends;
        private ImageView addFriends;
        private FrameLayout serverFollowLayout;
        private TextView followInvited;
        private TextView followInvite;
        private FrameLayout localFollowLayout;
        private TextView txt_following;

        public ViewHolder(View view) {
            super(view);
            contactImage = (CircleImageView) view.findViewById(R.id.contact_image);
            followingName = (TextView) view.findViewById(R.id.following_name);
            followingLevel = (TextView) view.findViewById(R.id.following_level);
            followedFriends = (ImageView) view.findViewById(R.id.followedFriends);
            addFriends = (ImageView) view.findViewById(R.id.addFriends);
            serverFollowLayout = (FrameLayout) view.findViewById(R.id.serverFollowLayout);
            followInvited = (TextView) view.findViewById(R.id.followInvited);
            followInvite = (TextView) view.findViewById(R.id.followInvite);
            localFollowLayout = (FrameLayout) view.findViewById(R.id.localFollowLayout);
            txt_following = (TextView) view.findViewById(R.id.txt_following);
        }
    }

    private void inviteFriendAPI(String paraUserId, String paraSessionId, String paraFriendEmail, final int pos, final TextView followInvite, final TextView followInvited) {

        FollowingFindFriendActivity.getInstance().showprogress();

        APIInterface apiService = APIClient.getClient().create(APIInterface.class);
        String url = APIClient.BASE_URL;

        url += "sendInvite.pl?user_id=" + paraUserId +
                "&session_id=" + paraSessionId +
                "&apiauth=" + APIClient.AUTH_KEY +
                "&email=" + paraFriendEmail;

        Call<FollowingModel<EmailFriendData>> callObject = apiService.getFollowingContactEmail(url.replaceAll(" ", "%20"));
        callObject.enqueue(new Callback<FollowingModel<EmailFriendData>>() {

            @Override
            public void onResponse(Call<FollowingModel<EmailFriendData>> call, Response<FollowingModel<EmailFriendData>> response) {

                if (response.body() != null && !response.body().toString().trim().isEmpty()) {

                    final String email = response.body().getEmail();
                    final String friend_email = followingarraylist.get(pos).getFriend_email();

                    if (friend_email.equals(email)) {
                        activity.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {

                                ContentValues contentValues = new ContentValues();
                                contentValues.put(CommonDatabaseHelper.KEY_FRIEND_USER_INVITE_FLAG, true);
                                commonDatabaseHelper.updateRowData(CommonDatabaseHelper.FOLLOWING_MASTER, contentValues, commonDatabaseHelper.KEY_FRIEND_EMAIL + " = ?",
                                        new String[]{String.valueOf(email)});

                                followInvite.setVisibility(View.GONE);
                                followInvited.setVisibility(View.VISIBLE);

                            }
                        });
                    }
                    FollowingFindFriendActivity.getInstance().dismissprogress();
                }
            }

            @Override
            public void onFailure(Call<FollowingModel<EmailFriendData>> call, Throwable t) {
                FollowingFindFriendActivity.getInstance().dismissprogress();
            }
        });
    }

    private void followUnfollowFriendAPI(String paraUserId, String paraSessionId, String paraFollowingUserId, String paraAction, final int pos, final ImageView followImageview, final ImageView followedFriends, final TextView txt_following) {

        FollowingFindFriendActivity.getInstance().showprogress();

        APIInterface apiService = APIClient.getClient().create(APIInterface.class);
        String url = APIClient.BASE_URL;

        url += "followUser.pl?user_id=" + paraUserId +
                "&session_id=" + paraSessionId +
                "&apiauth=" + APIClient.AUTH_KEY +
                "&following_user_id=" + paraFollowingUserId +
                "&app_version=" + BuildConfig.VERSION_NAME +
                "&device=" + DEVICE_KEY +
                "&action=" + paraAction;


        Call<FollowingModel<EmailFriendData>> callObject = apiService.getFollowingContactEmail(url.replaceAll(" ", "%20"));
        callObject.enqueue(new Callback<FollowingModel<EmailFriendData>>() {

            @Override
            public void onResponse(Call<FollowingModel<EmailFriendData>> call, Response<FollowingModel<EmailFriendData>> response) {

                if (response.body() != null && !response.body().toString().trim().isEmpty()) {

                    final String followingUserId = response.body().getFollowing_user_id();
                    final String friendUserId = followingarraylist.get(pos).getFriend_user_id();
                    final String followed_by = followingarraylist.get(pos).getFollowed_by();
                    final String action = response.body().getAction();


                    if (followingUserId.equals(friendUserId)) {
                        activity.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {

                                if (action.equals("follow")) {

                                    followImageview.setVisibility(View.GONE);

                                    if (followed_by.equals("true")) {
                                        followedFriends.setVisibility(View.VISIBLE);
                                    } else {
                                        txt_following.setVisibility(View.VISIBLE);
                                    }
                                } else {
                                    txt_following.setVisibility(View.GONE);
                                    followedFriends.setVisibility(View.GONE);
                                    followImageview.setVisibility(View.VISIBLE);
                                }
                            }
                        });
                    }


                    FollowingFindFriendActivity.getInstance().dismissprogress();
                }
            }

            @Override
            public void onFailure(Call<FollowingModel<EmailFriendData>> call, Throwable t) {
                FollowingFindFriendActivity.getInstance().dismissprogress();
            }
        });
    }

}