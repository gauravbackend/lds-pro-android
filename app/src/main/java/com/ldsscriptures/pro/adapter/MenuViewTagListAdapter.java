package com.ldsscriptures.pro.adapter;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.daimajia.swipe.SwipeLayout;
import com.daimajia.swipe.adapters.RecyclerSwipeAdapter;
import com.ldsscriptures.pro.R;
import com.ldsscriptures.pro.activity.BaseActivity;
import com.ldsscriptures.pro.activity.ReadingActivity;
import com.ldsscriptures.pro.activity.menuscreens.MenuTagActivity;
import com.ldsscriptures.pro.database.DataBaseHelper;
import com.ldsscriptures.pro.model.TimeLine.VerseDataTimeLineModel;
import com.ldsscriptures.pro.model.VerseTagsData;
import com.ldsscriptures.pro.utils.PushToServerData;
import com.ldsscriptures.pro.utils.SessionManager;

import java.util.ArrayList;

public class MenuViewTagListAdapter extends RecyclerSwipeAdapter<MenuViewTagListAdapter.MyViewHolder> {

    private ArrayList<VerseTagsData> VerseTagsDatas = new ArrayList<>();
    private Activity activity;
    DataBaseHelper databasehelper;
    private SessionManager sessionManager;

    @Override
    public int getSwipeLayoutResourceId(int position) {
        return R.id.tagswipeone;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView tvVerseName, tv_verseTextTag;
        LinearLayout tagmenu_layoutmain;
        SwipeLayout swipeLayout;
        TextView tvDelete;

        public MyViewHolder(View view) {
            super(view);
            tvVerseName = (TextView) view.findViewById(R.id.tvVerseName);
            tv_verseTextTag = (TextView) view.findViewById(R.id.tv_verseTextTag);
            tagmenu_layoutmain = (LinearLayout) view.findViewById(R.id.tagmenu_layoutmain);
            swipeLayout = (SwipeLayout) itemView.findViewById(R.id.tagswipeone);
            tvDelete = (TextView) itemView.findViewById(R.id.tvDeletetagone);
        }
    }

    public MenuViewTagListAdapter(Activity activity, ArrayList<VerseTagsData> VerseTagsDatas) {
        this.VerseTagsDatas = VerseTagsDatas;
        this.activity = activity;
        databasehelper = new DataBaseHelper(activity);
        sessionManager = new SessionManager(activity);
    }

    @Override
    public MenuViewTagListAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, final int position) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_view_tag, parent, false);
        return new MenuViewTagListAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MenuViewTagListAdapter.MyViewHolder holder, final int position) {

        holder.tvVerseName.setText(VerseTagsDatas.get(position).getTagTitle());

        final String verseID = VerseTagsDatas.get(position).getVerseNumber();
        String verseURI = VerseTagsDatas.get(position).getItemuri();

        VerseDataTimeLineModel verseDataTimeLineModel = new VerseDataTimeLineModel();
        verseDataTimeLineModel = BaseActivity.getInstance().getVerseText(verseURI, verseID.trim());

        String verseText = verseDataTimeLineModel.getVereseText();
        String versepath = verseDataTimeLineModel.getVerseFullPath();
        final String verseLcId = verseDataTimeLineModel.getLcId();
        final String verseLsId = verseDataTimeLineModel.getLsId();
        final String indexForReading = verseDataTimeLineModel.getIndexForReading();
        final String verseAid = verseDataTimeLineModel.getVerseAid();

        holder.tv_verseTextTag.setText(verseText);
        holder.tagmenu_layoutmain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {
                redirectReadingActivity(verseLcId, verseLsId, indexForReading, verseAid, verseID);
            }
        });

        holder.swipeLayout.setShowMode(SwipeLayout.ShowMode.PullOut);
        // Drag From Right
        holder.swipeLayout.addDrag(SwipeLayout.DragEdge.Right, holder.swipeLayout.findViewById(R.id.tag_wrapperone));
        // Handling different events when swiping
        holder.swipeLayout.addSwipeListener(new SwipeLayout.SwipeListener() {
            @Override
            public void onClose(SwipeLayout layout) {
                //when the SurfaceView totally cover the BottomView.
            }

            @Override
            public void onUpdate(SwipeLayout layout, int leftOffset, int topOffset) {
                //you are swiping.
            }

            @Override
            public void onStartOpen(SwipeLayout layout) {

            }

            @Override
            public void onOpen(SwipeLayout layout) {
                //when the BottomView totally show.
            }

            @Override
            public void onStartClose(SwipeLayout layout) {

            }

            @Override
            public void onHandRelease(SwipeLayout layout, float xvel, float yvel) {
                //when user's hand released.
            }
        });
        holder.tvDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                final String paraUri = VerseTagsDatas.get(position).getItemuri().toString().trim();
                final String paraVerseId = VerseTagsDatas.get(position).getVerseNumber().toString().trim();
                final String paraVerseAId = VerseTagsDatas.get(position).getVerseaid().toString().trim();
                final String paraNoteTitle = VerseTagsDatas.get(position).getTagname().toString().trim();
                final String serverid = VerseTagsDatas.get(position).getServerid().toString().trim();

                try {
                    android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(activity);
                    builder.setTitle("Warning");
                    builder.setMessage(R.string.are_you_sure_delete)
                            .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            })
                            .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    // Yes button clicked, do something
                                    dialog.dismiss();
                                    deleteLocally(paraVerseAId, paraNoteTitle, holder, position);
                                    new PushToServerData(activity);
                                }
                            });

                    builder.show();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

    }

    @Override
    public int getItemCount() {
        return VerseTagsDatas.size();
    }

    public void redirectReadingActivity(String verseLcId, String verseLsId, String indexForReading, String verseAid, String verseId) {
        if (verseLcId != null && verseLsId != null && indexForReading != null) {
            Intent intent = new Intent(activity, ReadingActivity.class);
            intent.putExtra(activity.getString(R.string.indexForReading), Integer.parseInt(indexForReading));
            intent.putExtra(activity.getString(R.string.isFromIndex), true);
            intent.putExtra(activity.getString(R.string.is_history), false);
            intent.putExtra(activity.getString(R.string.lcID), verseLcId);
            intent.putExtra(activity.getString(R.string.lsId), verseLsId);
            intent.putExtra(activity.getString(R.string.verseAId), Integer.parseInt(verseAid));
            intent.putExtra(activity.getString(R.string.verseId), Integer.parseInt(verseId));
            activity.startActivity(intent);
            activity.overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
        }
    }

    public void deleteLocally(final String paraVerseAId, final String paraNoteTitle, final MyViewHolder holder, final int position) {

        mItemManger.removeShownLayouts(holder.swipeLayout);
        VerseTagsDatas.remove(position);
        notifyItemRemoved(position);
        notifyItemRangeChanged(position, VerseTagsDatas.size());
        mItemManger.closeAllItems();

        ArrayList<VerseTagsData> NewVerseTagsDatas;
        NewVerseTagsDatas = sessionManager.getPreferencesTagsArrayList(activity);
        for (int i = 0; i < NewVerseTagsDatas.size(); i++) {
            if (NewVerseTagsDatas.get(i).getTagname().equals(paraNoteTitle) && NewVerseTagsDatas.get(i).getVerseaid().equals(paraVerseAId)) {
                //NewVerseTagsDatas.remove(i);
                NewVerseTagsDatas.get(i).setDeleted("1");
            }
        }
        sessionManager.setPreferencesTagsArrayList(activity, NewVerseTagsDatas);

        if (VerseTagsDatas.size() == 0) {
            activity.startActivity(new Intent(activity, MenuTagActivity.class));
        }
    }

}
