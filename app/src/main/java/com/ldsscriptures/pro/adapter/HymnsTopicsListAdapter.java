package com.ldsscriptures.pro.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.ldsscriptures.pro.R;
import com.ldsscriptures.pro.model.HymnsTopicsData;

import java.util.ArrayList;

public class HymnsTopicsListAdapter extends BaseAdapter {
    private Context context;
    private ArrayList<HymnsTopicsData> hymnsTopicsDataArrayList = new ArrayList<>();
    private HymnsTopicsListAdapter.ViewHolder holder;

    public HymnsTopicsListAdapter(Context context, ArrayList<HymnsTopicsData> hymnsTopicsDataArrayList) {
        this.context = context;
        this.hymnsTopicsDataArrayList = hymnsTopicsDataArrayList;
    }

    @Override
    public int getCount() {
        return hymnsTopicsDataArrayList.size();
    }

    @Override
    public Object getItem(int position) {
        return hymnsTopicsDataArrayList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = LayoutInflater.from(context).inflate(R.layout.item_hymns_topics_list, null);
            holder = new HymnsTopicsListAdapter.ViewHolder();
            holder.linHymns = (LinearLayout) convertView.findViewById(R.id.linHymns);
            holder.txtTopicsName = (TextView) convertView.findViewById(R.id.txtTopicsName);
            convertView.setTag(holder);
        } else {
            holder = (HymnsTopicsListAdapter.ViewHolder) convertView.getTag();
        }

        if (hymnsTopicsDataArrayList.get(position).getName() != null)
            holder.txtTopicsName.setText(hymnsTopicsDataArrayList.get(position).getName());

        if (hymnsTopicsDataArrayList.get(position).isSelected()) {
            holder.txtTopicsName.setTextColor(context.getResources().getColor(R.color.sand));
        } else {
            holder.txtTopicsName.setTextColor(context.getResources().getColor(R.color.textColor));
        }
        return convertView;
    }

    private static class ViewHolder {
        TextView txtTopicsName;
        LinearLayout linHymns;
    }
}