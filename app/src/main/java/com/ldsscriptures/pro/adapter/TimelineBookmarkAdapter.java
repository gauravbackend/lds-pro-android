package com.ldsscriptures.pro.adapter;

import android.app.Activity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.ldsscriptures.pro.R;
import com.ldsscriptures.pro.model.TimelineBookmarkItem;
import com.ldsscriptures.pro.model.TimelineItem;
import com.ldsscriptures.pro.utils.SessionManager;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

public class TimelineBookmarkAdapter extends RecyclerView.Adapter<TimelineBookmarkAdapter.ViewHolder> {

    private ArrayList<TimelineBookmarkItem> timelineBookmarkItems;
    private Activity activity;
    private SessionManager sessionManager;

    public TimelineBookmarkAdapter(Activity activity, ArrayList<TimelineBookmarkItem> timelineBookmarkItems) {
        this.timelineBookmarkItems = timelineBookmarkItems;
        this.activity = activity;
        sessionManager = new SessionManager(activity);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.activity_timeline_bookmark_item, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int i) {

        String var_timeline_bookmark_nephi = timelineBookmarkItems.get(i).getBookmark_nephi();
        String var_timeline_bookmark_time_ago = timelineBookmarkItems.get(i).getBookmark_time_ago();

        viewHolder.timeline_bookmark_nephi.setText(var_timeline_bookmark_nephi.toString());
        viewHolder.timeline_bookmark_time_ago.setText(var_timeline_bookmark_time_ago.toString());
        viewHolder.bookmark_image.setImageResource(R.drawable.bookmark1);

    }

    @Override
    public int getItemCount() {
        return timelineBookmarkItems.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private ImageView bookmark_image;
        private TextView timeline_bookmark_nephi;
        private TextView timeline_bookmark_time_ago;


        public ViewHolder(View view) {
            super(view);

            bookmark_image = (ImageView) view.findViewById(R.id.bookmark_image);
            timeline_bookmark_nephi = (TextView) view.findViewById(R.id.timeline_bookmark_nephi);
            timeline_bookmark_time_ago=(TextView)view.findViewById(R.id.timeline_bookmark_time_ago);

        }
    }


}