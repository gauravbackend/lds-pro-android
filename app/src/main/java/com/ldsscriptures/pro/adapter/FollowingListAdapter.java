package com.ldsscriptures.pro.adapter;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.ldsscriptures.pro.R;

import com.ldsscriptures.pro.model.FollowersMainData;
import com.ldsscriptures.pro.utils.SessionManager;
import com.squareup.picasso.Picasso;

import java.text.DecimalFormat;
import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

public class FollowingListAdapter extends RecyclerView.Adapter<FollowingListAdapter.ViewHolder> {

    private ArrayList<FollowersMainData> followingarraylist;
    private Activity activity;
    private SessionManager sessionManager;
    private int activityFlag, pointFlag;

    public FollowingListAdapter(Activity activity, ArrayList<FollowersMainData> followingarraylist, int activityFlag, int pointFlag) {
        this.followingarraylist = followingarraylist;
        this.activity = activity;
        sessionManager = new SessionManager(activity);
        this.activityFlag = activityFlag;
        this.pointFlag = pointFlag;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.activity_following_list_item, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int i) {

        String following_level = followingarraylist.get(i).getFollowing_level();
        String following_img_url = followingarraylist.get(i).getFollowing_img_url();
        String following_name = followingarraylist.get(i).getFollowing_name();
        String following_userid = followingarraylist.get(i).getFollowing_user_id();
        String following_points_week = followingarraylist.get(i).getFollowing_points_week();
        String following_points_month = followingarraylist.get(i).getFollowing_points_month();
        String following_points_year = followingarraylist.get(i).getFollowing_points_year();

        if (activityFlag == 0) {
            viewHolder.img_follow_right_arrow.setVisibility(View.VISIBLE);
            viewHolder.serverFollowLayout.setVisibility(View.GONE);
            viewHolder.localFollowLayout.setVisibility(View.GONE);
        } else {
            viewHolder.img_follow_right_arrow.setVisibility(View.GONE);
            viewHolder.serverFollowLayout.setVisibility(View.GONE);
            viewHolder.localFollowLayout.setVisibility(View.GONE);
            viewHolder.leaderboard_point.setVisibility(View.VISIBLE);

            if (pointFlag == 1) {
                setPoint(following_points_week.toString(), viewHolder.leaderboard_point);
            } else if (pointFlag == 2) {
                setPoint(following_points_month.toString(), viewHolder.leaderboard_point);
            } else if (pointFlag == 3) {
                setPoint(following_points_year.toString(), viewHolder.leaderboard_point);
            }
        }


        viewHolder.followingName.setText(following_name.toString());
        viewHolder.followingLevel.setText("level" + " " + following_level.toString());

        if (following_img_url != null) {
            Picasso.with(activity).load(following_img_url).into(viewHolder.contactImage);
        } else {
            viewHolder.contactImage.setBackgroundResource(R.drawable.profile_lg);
        }
    }

    @Override
    public int getItemCount() {
        return followingarraylist.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private CircleImageView contactImage;
        private TextView followingName;
        private TextView followingLevel;
        private ImageView followedFriends;
        private ImageView addFriends;
        private FrameLayout serverFollowLayout;
        private TextView followInvited;
        private TextView followInvite;
        private FrameLayout localFollowLayout;
        private ImageView img_follow_right_arrow;
        private TextView leaderboard_point;


        public ViewHolder(View view) {
            super(view);
            contactImage = (CircleImageView) view.findViewById(R.id.contact_image);
            followingName = (TextView) view.findViewById(R.id.following_name);
            followingLevel = (TextView) view.findViewById(R.id.following_level);
            followedFriends = (ImageView) view.findViewById(R.id.followedFriends);
            addFriends = (ImageView) view.findViewById(R.id.addFriends);
            serverFollowLayout = (FrameLayout) view.findViewById(R.id.serverFollowLayout);
            followInvited = (TextView) view.findViewById(R.id.followInvited);
            followInvite = (TextView) view.findViewById(R.id.followInvite);
            localFollowLayout = (FrameLayout) view.findViewById(R.id.localFollowLayout);
            img_follow_right_arrow = (ImageView) view.findViewById(R.id.img_follow_right_arrow);
            leaderboard_point = (TextView) view.findViewById(R.id.leaderboard_point);
        }
    }

    public void setPoint(String varpoint, TextView txtLeaderboard_point) {
        int x = Integer.valueOf(varpoint);

        if (x >= 10000) {
            int point = (int) Math.round((x / 1000));
            DecimalFormat form = new DecimalFormat("#,##");
            txtLeaderboard_point.setText(form.format(point) + "k");
        } else {
            txtLeaderboard_point.setText(varpoint);
        }
    }


}