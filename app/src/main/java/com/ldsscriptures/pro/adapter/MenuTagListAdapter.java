package com.ldsscriptures.pro.adapter;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.daimajia.swipe.SwipeLayout;
import com.daimajia.swipe.adapters.RecyclerSwipeAdapter;
import com.ldsscriptures.pro.R;
import com.ldsscriptures.pro.activity.menuscreens.MenuViewTagActivity;
import com.ldsscriptures.pro.database.CommonDatabaseHelper;
import com.ldsscriptures.pro.model.VerseTagsData;
import com.ldsscriptures.pro.utils.SessionManager;

import java.util.ArrayList;

public class MenuTagListAdapter extends RecyclerSwipeAdapter<MenuTagListAdapter.MyViewHolder> {

    private ArrayList<VerseTagsData> verseTagsArrayList = new ArrayList<>();
    private Activity activity;
    private SessionManager sessionManager;
    private int myPosition;
    CommonDatabaseHelper commonDatabaseHelper;

    @Override
    public int getSwipeLayoutResourceId(int position) {
        return 0;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView tvTitle, tvCount;
        SwipeLayout swipeLayout;
        TextView tvDelete;

        public MyViewHolder(View view) {
            super(view);
            tvTitle = (TextView) view.findViewById(R.id.tvTitle);
            tvCount = (TextView) view.findViewById(R.id.tvCount);
           /* swipeLayout = (SwipeLayout) itemView.findViewById(R.id.tagswipe);
            tvDelete = (TextView) itemView.findViewById(R.id.tvDeletetag);*/
        }
    }

    public MenuTagListAdapter(Activity activity, ArrayList<VerseTagsData> verseTagsArrayList) {
        this.verseTagsArrayList = verseTagsArrayList;
        this.activity = activity;
        sessionManager = new SessionManager(activity);
        commonDatabaseHelper = new CommonDatabaseHelper(activity);
    }

    @Override
    public MenuTagListAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, final int position) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_tag_inmenu, parent, false);
        return new MenuTagListAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MenuTagListAdapter.MyViewHolder holder, final int position) {

        holder.tvTitle.setText(verseTagsArrayList.get(position).getTagname());

        holder.tvTitle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {
                Intent intent = new Intent(activity, MenuViewTagActivity.class);
                intent.putExtra("Tagname", verseTagsArrayList.get(position).getTagname());
                activity.startActivity(intent);
                activity.overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
            }
        });
    }

    @Override
    public int getItemCount() {
        return verseTagsArrayList.size();
    }

}
