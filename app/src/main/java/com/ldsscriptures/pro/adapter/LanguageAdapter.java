package com.ldsscriptures.pro.adapter;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.ldsscriptures.pro.R;
import com.ldsscriptures.pro.model.Language;
import com.ldsscriptures.pro.utils.SessionManager;

import java.util.ArrayList;
import java.util.List;


public class LanguageAdapter extends RecyclerView.Adapter<LanguageAdapter.MyViewHolder> {

    private final Activity activity;
    private List<Language> indexList;
    private SessionManager sessionManager;
    public static String langId = "";
    public static String langCode = "";
    public static String langCollectionCode = "";

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView tvLanguage;
        ImageView iv_selected;

        public MyViewHolder(View view) {
            super(view);
            tvLanguage = (TextView) view.findViewById(R.id.tvLanguage);
            iv_selected = (ImageView) view.findViewById(R.id.iv_selected);
        }
    }

    public LanguageAdapter(Activity activity, ArrayList<Language> indexList) {
        this.indexList = indexList;
        this.activity = activity;
        sessionManager = new SessionManager(activity);
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, final int position) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_language, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {

        if (indexList.get(position).isSelected()) {
            holder.iv_selected.setVisibility(View.VISIBLE);
        } else
            holder.iv_selected.setVisibility(View.INVISIBLE);

        holder.tvLanguage.setText(indexList.get(position).getName());

        holder.tvLanguage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                langId = indexList.get(position).getId();
                langCode = indexList.get(position).getLang_code();
                langCollectionCode = indexList.get(position).getRootLibCollectionID();

                for (int i = 0; i < indexList.size(); i++) {
                    indexList.get(i).setSelected(false);
                }
                indexList.get(position).setSelected(true);
                notifyDataSetChanged();
            }
        });
    }

    @Override
    public int getItemCount() {
        return indexList.size();
    }
}