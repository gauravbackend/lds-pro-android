package com.ldsscriptures.pro.adapter;

import android.app.Activity;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.daimajia.swipe.SwipeLayout;
import com.daimajia.swipe.adapters.RecyclerSwipeAdapter;
import com.ldsscriptures.pro.R;
import com.ldsscriptures.pro.activity.BaseActivity;
import com.ldsscriptures.pro.activity.verses.VerseBookmarkActivity;
import com.ldsscriptures.pro.activity.verses.VerseEditBookmarkActivity;
import com.ldsscriptures.pro.activity.verses.VerseHighlightActivity;
import com.ldsscriptures.pro.database.CommonDatabaseHelper;
import com.ldsscriptures.pro.model.BookmarkData;
import com.ldsscriptures.pro.model.TimeLine.VerseDataTimeLineModel;
import com.ldsscriptures.pro.utils.EditItemTouchHelperCallback;
import com.ldsscriptures.pro.utils.Global;
import com.ldsscriptures.pro.utils.ItemTouchHelperViewHolder;
import com.ldsscriptures.pro.utils.PushToServerData;
import com.ldsscriptures.pro.utils.SessionManager;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;


public class BookmarkVerseListAdapter extends RecyclerSwipeAdapter<BookmarkVerseListAdapter.MyViewHolder> implements EditItemTouchHelperCallback.ItemTouchHelperAdapter {


    private final String verseAId;
    private ArrayList<BookmarkData> bookmarkDataArrayList = new ArrayList<>();
    private ArrayList<BookmarkData> savebookmarkDataArrayList = new ArrayList<>();

    private Activity activity;
    private CommonDatabaseHelper commonDatabaseHelper;
    String verseText;
    String verseId;
    String currentDateTimeString;
    String paraUserId;
    String paraSessionId;
    private SessionManager sessionManager;
    private boolean setFalg = false;

    @Override
    public int getSwipeLayoutResourceId(int position) {
        return R.id.bookmarkverseswipe;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder implements ItemTouchHelperViewHolder {
        public TextView tvBookmarkTitle, tvBookmarkSentense, txt_time_ago_title;
        private LinearLayout llMain;
        private ImageView ivArrow;
        SwipeLayout swipeLayout;
        TextView tvDelete;
        ImageView imagright_arrow_swipe;

        public MyViewHolder(View view) {
            super(view);
            tvBookmarkTitle = (TextView) view.findViewById(R.id.tvBookmarkTitle);
            tvBookmarkSentense = (TextView) view.findViewById(R.id.tvBookmarkSentense);
            txt_time_ago_title = (TextView) view.findViewById(R.id.txt_time_ago_title);
            llMain = (LinearLayout) view.findViewById(R.id.llMain);
            ivArrow = (ImageView) view.findViewById(R.id.ivArrow);
            swipeLayout = (SwipeLayout) itemView.findViewById(R.id.bookmarkverseswipe);
            tvDelete = (TextView) itemView.findViewById(R.id.tvDeletebook);
            imagright_arrow_swipe = (ImageView) itemView.findViewById(R.id.imagright_arrow_swipe);
        }

        @Override
        public void onItemSelected() {
            if (setFalg) {
                itemView.setBackgroundResource(R.drawable.shadow_layout);
            } else {
                itemView.setBackgroundResource(0);
            }
        }

        @Override
        public void onItemClear() {
            itemView.setBackgroundColor(0);
        }
    }

    public BookmarkVerseListAdapter(Activity activity, ArrayList<BookmarkData> bookmarkDataArrayList, String verseAId, String verseText, String verseId, int indexId, int versePos, String itemUri, String paraUserId, String paraSessionId) {
        this.bookmarkDataArrayList = bookmarkDataArrayList;
        this.activity = activity;
        this.verseAId = verseAId;
        this.verseId = verseId;
        this.verseText = verseText;
        commonDatabaseHelper = new CommonDatabaseHelper(activity);
        currentDateTimeString = new SimpleDateFormat("dd-MMM-yyyy hh:mm:ss").format(Calendar.getInstance().getTime());
        this.paraUserId = paraUserId;
        this.paraSessionId = paraSessionId;
        sessionManager = new SessionManager(activity);
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, final int position) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_verse_bookmark, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        if (setFalg) {
            holder.imagright_arrow_swipe.setVisibility(View.VISIBLE);
        } else {
            holder.imagright_arrow_swipe.setVisibility(View.GONE);
        }

        holder.tvBookmarkTitle.setText(bookmarkDataArrayList.get(position).getTitle());
        String var_timeline_uri = bookmarkDataArrayList.get(position).getItemuri();
        String var_timeline_Verse_Id = bookmarkDataArrayList.get(position).getVerseNumber();

        VerseDataTimeLineModel verseDataTimeLineModel = new VerseDataTimeLineModel();
        verseDataTimeLineModel = BaseActivity.getInstance().getVerseText(var_timeline_uri, var_timeline_Verse_Id.trim());
        final String verseTextNew = verseDataTimeLineModel.getVereseText();
        holder.tvBookmarkSentense.setText(verseTextNew);

        holder.llMain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (!setFalg) {
                    holder.ivArrow.setVisibility(View.VISIBLE);

                    ContentValues contentValues = new ContentValues();
                    contentValues.put("serverid", bookmarkDataArrayList.get(position).getServerid());
                    contentValues.put("verseaid", verseAId);
                    contentValues.put("chapteruri", Global.bookmarkData.getChapterUri());
                    contentValues.put("itemuri", Global.bookmarkData.getItemuri());
                    contentValues.put("bookuri", Global.bookmarkData.getBookuri());
                    contentValues.put("versetitle", bookmarkDataArrayList.get(position).getTitle());
                    contentValues.put("versenumber", verseId);
                    contentValues.put("versetext", verseTextNew);
                    contentValues.put("createdAt", currentDateTimeString);
                    contentValues.put("deleted", bookmarkDataArrayList.get(position).getDeleted());
                    contentValues.put("modified", "1");
                    contentValues.put("sort_order", bookmarkDataArrayList.get(position).getSort_order());

                    commonDatabaseHelper.updateRowData(CommonDatabaseHelper.TABLE_BOOKMARK_MARSTER, contentValues, CommonDatabaseHelper.KEY_ID + " = ?",
                            new String[]{String.valueOf(bookmarkDataArrayList.get(position).getId())});

                    if (VerseHighlightActivity.getInstance() != null) {
                        VerseHighlightActivity.getInstance().goToReadingView();
                    }
                    activity.finish();
                    new PushToServerData(activity);
                }
            }
        });


        holder.llMain.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                if (!setFalg) {
                    Intent intent = new Intent(activity, VerseEditBookmarkActivity.class);
                    intent.putExtra(activity.getString(R.string.position), position);
                    intent.putExtra(activity.getString(R.string.verseId), bookmarkDataArrayList.get(position).getVerseNumber());
                    intent.putExtra(activity.getString(R.string.verseText), verseTextNew);
                    intent.putExtra(activity.getString(R.string.bookmarkTitle), bookmarkDataArrayList.get(position).getTitle());
                    intent.putExtra(activity.getString(R.string.verseAId), bookmarkDataArrayList.get(position).getVerseaid());
                    intent.putExtra(activity.getString(R.string.serverId), bookmarkDataArrayList.get(position).getServerid());
                    intent.putExtra(activity.getString(R.string.itemUri), bookmarkDataArrayList.get(position).getItemuri());
                    activity.startActivity(intent);
                    activity.finish();
                    activity.overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
                }
                return true;
            }
        });


        holder.swipeLayout.setShowMode(SwipeLayout.ShowMode.PullOut);
        // Drag From Right
        holder.swipeLayout.addDrag(SwipeLayout.DragEdge.Right, holder.swipeLayout.findViewById(R.id.bottom_wrapperbook));
        // Handling different events when swiping
        holder.swipeLayout.addSwipeListener(new SwipeLayout.SwipeListener() {
            @Override
            public void onClose(SwipeLayout layout) {
                //when the SurfaceView totally cover the BottomView.
            }

            @Override
            public void onUpdate(SwipeLayout layout, int leftOffset, int topOffset) {
                //you are swiping.
            }

            @Override
            public void onStartOpen(SwipeLayout layout) {

            }

            @Override
            public void onOpen(SwipeLayout layout) {
                //when the BottomView totally show.
            }

            @Override
            public void onStartClose(SwipeLayout layout) {

            }

            @Override
            public void onHandRelease(SwipeLayout layout, float xvel, float yvel) {
                //when user's hand released.
            }
        });

        holder.tvDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                final String paraUri = bookmarkDataArrayList.get(position).getItemuri().toString().trim();
                final String paraVerseId = bookmarkDataArrayList.get(position).getVerseNumber().toString().trim();
                final String paraVerseAId = bookmarkDataArrayList.get(position).getVerseaid().toString().trim();
                final String paraNoteTitle = bookmarkDataArrayList.get(position).getTitle().toString().trim();
                final String serverid = bookmarkDataArrayList.get(position).getServerid().toString().trim();

                try {
                    android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(activity);
                    builder.setTitle("Warning");
                    builder.setMessage(R.string.are_you_sure_delete)

                            .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            })
                            .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    // Yes button clicked, do something
                                    dialog.dismiss();
                                    deleteLocally(holder, position, bookmarkDataArrayList, paraVerseAId);
                                    new PushToServerData(activity);
                                }
                            });

                    builder.show();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return bookmarkDataArrayList.size();
    }

    public void deleteLocally(final MyViewHolder holder, final int position, final ArrayList<BookmarkData> bookmarkDataArrayList, final String paraVerseAId) {
        mItemManger.removeShownLayouts(holder.swipeLayout);
        bookmarkDataArrayList.remove(position);
        notifyItemRemoved(position);
        notifyItemRangeChanged(position, bookmarkDataArrayList.size());
        mItemManger.closeAllItems();

        ContentValues contentValues = new ContentValues();
        contentValues.put(CommonDatabaseHelper.KEY_DELETED, "1");
        commonDatabaseHelper.updateRowData(CommonDatabaseHelper.TABLE_BOOKMARK_MARSTER, contentValues, CommonDatabaseHelper.KEY_VERSEAID2 + " = ?",
                new String[]{String.valueOf(paraVerseAId)});

        if (bookmarkDataArrayList.size() == 0) {
            VerseBookmarkActivity.getInstance().visibleHideView();
        }
    }

    @Override
    public void onItemMove(int fromPosition, int toPosition) {
        if (setFalg) {
            if (fromPosition < toPosition) {
                for (int i = fromPosition; i < toPosition; i++) {
                    Collections.swap(bookmarkDataArrayList, i, i + 1);
                }
            } else {
                for (int i = fromPosition; i > toPosition; i--) {
                    Collections.swap(bookmarkDataArrayList, i, i - 1);
                }
            }
            notifyItemMoved(fromPosition, toPosition);

            String ServerId = bookmarkDataArrayList.get(toPosition).getServerid();
            String SortOrder = String.valueOf(toPosition + 1);

            if (!bookmarkDataArrayList.get(toPosition).getServerid().equals("0")) {
                savebookmarkDataArrayList = sessionManager.getBookmarkSortOrderArraylist();
                if (savebookmarkDataArrayList != null && savebookmarkDataArrayList.size() > 0) {
                    for (int i = 0; i < savebookmarkDataArrayList.size(); i++) {
                        if (savebookmarkDataArrayList.get(i).getSort_order().equalsIgnoreCase(SortOrder) ||
                                savebookmarkDataArrayList.get(i).getServerid().equalsIgnoreCase(ServerId)) {
                            savebookmarkDataArrayList.remove(i);
                        }
                    }
                    bookmarkSorting(fromPosition, toPosition);
                } else {
                    savebookmarkDataArrayList = new ArrayList<>();
                    bookmarkSorting(fromPosition, toPosition);
                }
                sessionManager.saveBookmarkSortOrderArraylist(savebookmarkDataArrayList);
            }
        }
    }

    @Override
    public void onItemDismiss(int position) {


    }

    public void bookmarkSorting(int fromPosition, int toPosition) {
        if (fromPosition <= toPosition) {
            for (int i = fromPosition; i <= toPosition; i++) {
                BookmarkData bookmarkData = new BookmarkData();
                bookmarkData.setSort_order(String.valueOf(i + 1));
                bookmarkData.setServerid(bookmarkDataArrayList.get(i).getServerid());
                bookmarkData.setId(bookmarkDataArrayList.get(i).getId());
                bookmarkData.setVerseNumber(bookmarkDataArrayList.get(i).getVerseNumber());
                bookmarkData.setVerseaid(bookmarkDataArrayList.get(i).getVerseaid());
                bookmarkData.setChapterUri(bookmarkDataArrayList.get(i).getChapterUri());
                bookmarkData.setItemuri(bookmarkDataArrayList.get(i).getItemuri());
                bookmarkData.setBookuri(bookmarkDataArrayList.get(i).getBookuri());
                bookmarkData.setTitle(bookmarkDataArrayList.get(i).getTitle());
                bookmarkData.setVerseText(bookmarkDataArrayList.get(i).getVerseText());
                bookmarkData.setDeleted(bookmarkDataArrayList.get(i).getDeleted());
                bookmarkData.setModified(bookmarkDataArrayList.get(i).getModified());
                savebookmarkDataArrayList.add(bookmarkData);
            }
        } else {
            for (int i = fromPosition; i >= toPosition; i--) {
                BookmarkData bookmarkData = new BookmarkData();
                bookmarkData.setSort_order(String.valueOf(i + 1));
                bookmarkData.setServerid(bookmarkDataArrayList.get(i).getServerid());
                bookmarkData.setId(bookmarkDataArrayList.get(i).getId());
                bookmarkData.setVerseNumber(bookmarkDataArrayList.get(i).getVerseNumber());
                bookmarkData.setVerseaid(bookmarkDataArrayList.get(i).getVerseaid());
                bookmarkData.setChapterUri(bookmarkDataArrayList.get(i).getChapterUri());
                bookmarkData.setItemuri(bookmarkDataArrayList.get(i).getItemuri());
                bookmarkData.setBookuri(bookmarkDataArrayList.get(i).getBookuri());
                bookmarkData.setTitle(bookmarkDataArrayList.get(i).getTitle());
                bookmarkData.setVerseText(bookmarkDataArrayList.get(i).getVerseText());
                bookmarkData.setDeleted(bookmarkDataArrayList.get(i).getDeleted());
                bookmarkData.setModified(bookmarkDataArrayList.get(i).getModified());
                savebookmarkDataArrayList.add(bookmarkData);
            }
        }
    }

    public boolean isSetFalg(Boolean setFalg) {
        this.setFalg = setFalg;
        return setFalg;
    }

}
