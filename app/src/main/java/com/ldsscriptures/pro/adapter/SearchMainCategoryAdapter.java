package com.ldsscriptures.pro.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.text.SpannableStringBuilder;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.ldsscriptures.pro.R;
import com.ldsscriptures.pro.activity.SearchMainSubActivity;
import com.ldsscriptures.pro.activity.SearchSubActivity;
import com.ldsscriptures.pro.model.SerachData;
import com.ldsscriptures.pro.utils.DataHolder;
import com.ldsscriptures.pro.utils.Global;

import java.util.ArrayList;

public class SearchMainCategoryAdapter extends RecyclerView.Adapter<SearchMainCategoryAdapter.MyViewHolder> {

    private Context activity;
    private ArrayList<SerachData> categoryMainArrayList, filterList, lcDataArrayListString;
    private String serachQuery;

    public SearchMainCategoryAdapter(Context activity, ArrayList<SerachData> categoryMainArrayList, String serachQuery) {
        this.categoryMainArrayList = categoryMainArrayList;
        this.activity = activity;
        this.filterList = new ArrayList<SerachData>();
        this.lcDataArrayListString = new ArrayList<SerachData>();
        this.filterList.addAll(this.categoryMainArrayList);
        this.serachQuery = serachQuery;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, final int position) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.serach_category_layout, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {

        final String tvstringlcTitle = filterList.get(position).getStringlcTitle();

        if (DataHolder.hasData1()) {
            lcDataArrayListString = DataHolder.getData1();
        }

        final int tvstringlccount = LSDataCount(lcDataArrayListString, tvstringlcTitle, serachQuery);
        holder.tvcategoryTitle.setText(tvstringlcTitle);
        holder.tvstringlscount.setText(String.valueOf(tvstringlccount));

        holder.main_serach_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent i1 = new Intent(activity, SearchMainSubActivity.class);
                i1.putExtra("lctitle", tvstringlcTitle);
                i1.putExtra("serachQuery", serachQuery);
                DataHolder.setData1(lcDataArrayListString);
                activity.startActivity(i1);
            }
        });
    }

    @Override
    public int getItemCount() {
        return (null != filterList ? filterList.size() : 0);
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView tvcategoryTitle, tvstringlscount;
        LinearLayout main_serach_layout;

        public MyViewHolder(View view) {
            super(view);
            tvcategoryTitle = (TextView) view.findViewById(R.id.tvcategoryTitle);
            tvstringlscount = (TextView) view.findViewById(R.id.tvstringlscount);
            main_serach_layout = (LinearLayout) view.findViewById(R.id.main_serach_layout);
        }
    }

    // Do Search...
    public void filter(final String text) {

        // Searching could be complex..so we will dispatch it to a different thread...
        new Thread(new Runnable() {
            @Override
            public void run() {

                // Clear the filter list
                filterList.clear();

                // If there is no search value, then add all original list items to filter list
                if (TextUtils.isEmpty(text)) {
                    filterList.addAll(categoryMainArrayList);
                } else {
                    // Iterate in the original List and add it to filter list...

                    for (SerachData item : categoryMainArrayList) {
                        if (item.getStringSentense().toLowerCase().contains(text.toLowerCase())) {
                            filterList.add(item);
                        }
                    }
                }

                // Set on UI Thread
                ((Activity) activity).runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        // Notify the List that the DataSet has changed...
                        notifyDataSetChanged();
                    }
                });

            }
        }).start();

    }

    public String convertStringToArrayList(ArrayList<SerachData> arrayList) {
        Gson gson = new Gson();
        String json = gson.toJson(arrayList);
        return json;
    }

    public int LSDataCount(ArrayList<SerachData> arrayList, final String lctitle, final String serachQuery) {

        int count = 0;
        for (int i = 0; i < arrayList.size(); i++) {
            if (arrayList.get(i).getStringlcTitle().contains(lctitle)) {

                String myStringNew = "";

                if (arrayList.get(i).getStringSentense() != null && arrayList.get(i).getStringId() != null)
                    myStringNew = arrayList.get(i).getStringSentense().replace(arrayList.get(i).getStringId(), "");

                SpannableStringBuilder text = new SpannableStringBuilder(Global.noTrailingwhiteLines(Html.fromHtml(myStringNew)));

                if (text.toString().toLowerCase().contains(serachQuery.toLowerCase())) {
                    count++;
                }
            }
        }
        return count;
    }


}
