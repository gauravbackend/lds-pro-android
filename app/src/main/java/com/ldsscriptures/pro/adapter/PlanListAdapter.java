package com.ldsscriptures.pro.adapter;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.ldsscriptures.pro.R;
import com.ldsscriptures.pro.activity.studytools.NewPlanSelectPaceActivity;
import com.ldsscriptures.pro.activity.studytools.PlanActivity;
import com.ldsscriptures.pro.activity.studytools.PlanDetailsActivity;
import com.ldsscriptures.pro.model.LsData;
import com.ldsscriptures.pro.model.PlanModel;
import com.ldsscriptures.pro.utils.Global;
import com.ldsscriptures.pro.utils.SessionManager;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

public class PlanListAdapter extends RecyclerView.Adapter<PlanListAdapter.ViewHolder> {

    private ArrayList<PlanModel> newPlanArrayList;
    private Activity activity;
    private SessionManager sessionManager;

    public PlanListAdapter(Activity activity, ArrayList<PlanModel> newPlanArrayList) {
        this.newPlanArrayList = newPlanArrayList;
        this.activity = activity;
        sessionManager = new SessionManager(activity);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.activity_plan_item, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder viewHolder, int i) {

        final String var_plan_id = newPlanArrayList.get(i).getPlan_id();
        final String var_Plan_name = newPlanArrayList.get(i).getPlan_name();
        final String var_Plan_Main_LCID = newPlanArrayList.get(i).getPlan_Main_LCID();
        final String var_Plan_Sub_LSID = newPlanArrayList.get(i).getPlan_Sub_LSID();
        final String var_Plan_days = newPlanArrayList.get(i).getPlan_days();
        final String var_Plan_flag = newPlanArrayList.get(i).getPlan_flag();
        final String var_Plan_reading_minute = newPlanArrayList.get(i).getPlan_reading_minute();
        final String var_Plan_created_date = newPlanArrayList.get(i).getPlan_created_date();
        final String remove_Days = var_Plan_days.replace("days", "");
        final int targetPlanDay = Integer.parseInt(remove_Days.replaceAll("\\s", ""));
        final int createDay = (Integer.valueOf(var_Plan_reading_minute) / 60) / 24;
        final String currant_date = new SimpleDateFormat("dd/MM/yyyy").format(java.util.Calendar.getInstance().getTime());
        final int dateDiff = Math.abs((int) PlanActivity.getInstance().getDateDiff(new SimpleDateFormat("dd/MM/yyyy"), var_Plan_created_date.toString(), currant_date));
        final int leftDaysDifferent = dateDiff - createDay;
        final int leftDaysPercentage = (int) Math.round((createDay / targetPlanDay) * 100);

        String left_Days = "";
        if (leftDaysDifferent == 0) {
            left_Days = "On schedule";
        } else if (leftDaysDifferent <= -1) {
            left_Days = Math.abs((int) leftDaysDifferent) + " " + "days ahead";
        } else if (leftDaysDifferent >= 1) {
            left_Days = Math.abs((int) leftDaysDifferent) + " " + "days behind";
        }

        SimpleDateFormat postFormater = new SimpleDateFormat("MMM dd, yyyy");
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        java.util.Date date = null;
        Calendar c = Calendar.getInstance();
        try {
            c.setTime(sdf.parse(var_Plan_created_date));
            date = sdf.parse(var_Plan_created_date);
        } catch (Exception e) {
            e.printStackTrace();
        }
        c.add(Calendar.DATE, targetPlanDay);  // number of days to add, can also use Calendar.DAY_OF_MONTH in place of Calendar.DATE
        final String var_Plan_finish_date = postFormater.format(c.getTime());
        final String new_var_Plan_created_date = postFormater.format(date);


        viewHolder.txt_plan_name.setText(var_Plan_name.toString());
        viewHolder.txt_plan_day_of_day.setText("Day" + "  " + createDay + "  " + "of" + "  " + var_Plan_days.toString());
        viewHolder.txt_days_left.setText(left_Days);
        viewHolder.ProgressBarLeftDays.setProgress(leftDaysPercentage);

        viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(activity, PlanDetailsActivity.class);
                i.putExtra("var_Plan_created_date", new_var_Plan_created_date.toString());
                i.putExtra("var_Plan_createDay", String.valueOf(createDay).toString());
                i.putExtra("var_Plan_days", String.valueOf(targetPlanDay).toString());
                i.putExtra("var_Plan_left_Days", viewHolder.txt_days_left.getText().toString());
                i.putExtra("var_Plan_finish_date", var_Plan_finish_date.toString());
                i.putExtra("var_Plan_name", var_Plan_name.toString());
                i.putExtra("var_Plan_id", var_plan_id.toString());
                i.putExtra("var_leftDaysPercentage",String.valueOf(leftDaysPercentage).toString());

                activity.startActivity(i);
            }
        });
    }

    @Override
    public int getItemCount() {
        return newPlanArrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private TextView txt_plan_name;
        private TextView txt_plan_day_of_day;
        private TextView txt_days_left;
        private ProgressBar ProgressBarLeftDays;

        public ViewHolder(View view) {
            super(view);

            txt_plan_name = (TextView) view.findViewById(R.id.txt_plan_name);
            txt_plan_day_of_day = (TextView) view.findViewById(R.id.txt_plan_day_of_day);
            txt_days_left = (TextView) view.findViewById(R.id.txt_days_left);
            ProgressBarLeftDays = (ProgressBar) view.findViewById(R.id.ProgressBarLeftDays);
        }
    }

}