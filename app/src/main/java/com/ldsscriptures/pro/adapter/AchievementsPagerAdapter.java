package com.ldsscriptures.pro.adapter;

import android.app.Activity;
import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.ldsscriptures.pro.R;
import com.ldsscriptures.pro.model.AchivementsGridModel;

import java.util.ArrayList;

public class AchievementsPagerAdapter extends PagerAdapter {

    Activity activity;
    LayoutInflater mLayoutInflater;
    int[] mResources;

    public AchievementsPagerAdapter(Activity activity, int[] mResources) {
        this.activity = activity;
        mLayoutInflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.mResources = mResources;
    }

    @Override
    public int getCount() {
        return mResources.length;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == ((LinearLayout) object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        View itemView = mLayoutInflater.inflate(R.layout.activity_achievements_pager_item, container, false);

        TextView achivements_count, achivements_count_total, achivements_Category_title;

        achivements_count = (TextView) itemView.findViewById(R.id.achivements_count1);
        achivements_count_total = (TextView) itemView.findViewById(R.id.achivements_count_total1);
        achivements_Category_title = (TextView) itemView.findViewById(R.id.achivements_Category_title);

        RecyclerView recyclerView = (RecyclerView) itemView.findViewById(R.id.achievements_recycler_view);
        recyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(activity, 3);
        recyclerView.setLayoutManager(layoutManager);

        String var_count = String.valueOf(position + 1);
        String var_count_total = "/" + String.valueOf(mResources.length);

        achivements_count.setText(var_count.toString());
        achivements_count_total.setText(var_count_total.toString());

        ArrayList<AchivementsGridModel> achivementsGridarraylist = new ArrayList<>();
        achivementsGridarraylist.clear();

        if (position == 0) {

            achivements_Category_title.setText(R.string.STREAK);

            achivementsGridarraylist.add(new AchivementsGridModel("101", activity.getString(R.string.three_days_streak), R.drawable.lock));
            achivementsGridarraylist.add(new AchivementsGridModel("102", activity.getString(R.string.five_days_streak), R.drawable.lock_4));
            achivementsGridarraylist.add(new AchivementsGridModel("103", activity.getString(R.string.seven_days_streak), R.drawable.lock_3));
            achivementsGridarraylist.add(new AchivementsGridModel("104", activity.getString(R.string.ten_days_streak), R.drawable.lock_2));
            achivementsGridarraylist.add(new AchivementsGridModel("105", activity.getString(R.string.unlock_level_2), R.drawable.lock));
            achivementsGridarraylist.add(new AchivementsGridModel("106", activity.getString(R.string.unlock_level_3), R.drawable.lock_4));
            achivementsGridarraylist.add(new AchivementsGridModel("107", activity.getString(R.string.unlock_level_4), R.drawable.lock_3));
            achivementsGridarraylist.add(new AchivementsGridModel("108", activity.getString(R.string.unlock_level_5), R.drawable.lock_2));
        }
        if (position == 1) {

            achivements_Category_title.setText(R.string.GOALS);

            achivementsGridarraylist.add(new AchivementsGridModel("201", activity.getString(R.string.hundred_perecent_goal_hit), R.drawable.lock_4));
            achivementsGridarraylist.add(new AchivementsGridModel("202", activity.getString(R.string.one_hundred_fifty_goal_hit), R.drawable.lock_3));
            achivementsGridarraylist.add(new AchivementsGridModel("203", activity.getString(R.string.unlock_level_3), R.drawable.lock_2));
            achivementsGridarraylist.add(new AchivementsGridModel("204", activity.getString(R.string.unlock_level_5), R.drawable.lock_4));
            achivementsGridarraylist.add(new AchivementsGridModel("205", activity.getString(R.string.unlock_level_10), R.drawable.lock_3));
            achivementsGridarraylist.add(new AchivementsGridModel("206", activity.getString(R.string.unlock_level_15), R.drawable.lock_2));
            achivementsGridarraylist.add(new AchivementsGridModel("2011", activity.getString(R.string.three_days_goal_streak), R.drawable.lock_3));
            achivementsGridarraylist.add(new AchivementsGridModel("2021", activity.getString(R.string.unlock_level_3), R.drawable.lock_2));
            achivementsGridarraylist.add(new AchivementsGridModel("2031", activity.getString(R.string.unlock_level_5), R.drawable.lock_4));
            achivementsGridarraylist.add(new AchivementsGridModel("2041", activity.getString(R.string.unlock_level_10), R.drawable.lock_3));
            achivementsGridarraylist.add(new AchivementsGridModel("2051", activity.getString(R.string.unlock_level_15), R.drawable.lock_2));
            achivementsGridarraylist.add(new AchivementsGridModel("2061", activity.getString(R.string.unlock_level_20), R.drawable.lock_4));

        }
        if (position == 2) {

            achivements_Category_title.setText(R.string.SCRIPTURE_FEAST);

            achivementsGridarraylist.add(new AchivementsGridModel("301", activity.getString(R.string.oneDay5k), R.drawable.lock_4));
            achivementsGridarraylist.add(new AchivementsGridModel("302", activity.getString(R.string.oneDay6k), R.drawable.lock_4));
            achivementsGridarraylist.add(new AchivementsGridModel("303", activity.getString(R.string.unlock_level_3), R.drawable.lock_4));
            achivementsGridarraylist.add(new AchivementsGridModel("304", activity.getString(R.string.unlock_level_5), R.drawable.lock_4));
            achivementsGridarraylist.add(new AchivementsGridModel("305", activity.getString(R.string.unlock_level_5), R.drawable.lock_4));
            achivementsGridarraylist.add(new AchivementsGridModel("306", activity.getString(R.string.unlock_level_10), R.drawable.lock_4));
            achivementsGridarraylist.add(new AchivementsGridModel("307", activity.getString(R.string.unlock_level_10), R.drawable.lock_4));
            achivementsGridarraylist.add(new AchivementsGridModel("308", activity.getString(R.string.unlock_level_15), R.drawable.lock_4));
            achivementsGridarraylist.add(new AchivementsGridModel("309", activity.getString(R.string.unlock_level_15), R.drawable.lock_4));
            achivementsGridarraylist.add(new AchivementsGridModel("3010", activity.getString(R.string.unlock_level_20), R.drawable.lock_4));
        }
        if (position == 3) {

            achivements_Category_title.setText(R.string.HIDDEN_BADGES);

            achivementsGridarraylist.add(new AchivementsGridModel("401", activity.getString(R.string.hidden_11), R.drawable.ac_31_s));
            achivementsGridarraylist.add(new AchivementsGridModel("402", activity.getString(R.string.hidden_214), R.drawable.ac_32_s));
            achivementsGridarraylist.add(new AchivementsGridModel("403", activity.getString(R.string.hidden_46), R.drawable.ac_33_s));
            achivementsGridarraylist.add(new AchivementsGridModel("404", activity.getString(R.string.hidden_515), R.drawable.ac_34_s));
            achivementsGridarraylist.add(new AchivementsGridModel("405", activity.getString(R.string.hidden_724), R.drawable.ac_35_s));
            achivementsGridarraylist.add(new AchivementsGridModel("406", activity.getString(R.string.hidden_922), R.drawable.ac_36_s));
            achivementsGridarraylist.add(new AchivementsGridModel("407", activity.getString(R.string.hidden_1223), R.drawable.ac_37_s));
            achivementsGridarraylist.add(new AchivementsGridModel("408", activity.getString(R.string.hidden_1225), R.drawable.ac_38_s));
            achivementsGridarraylist.add(new AchivementsGridModel("409", activity.getString(R.string.hidden_100h), R.drawable.ac_39_s));
            achivementsGridarraylist.add(new AchivementsGridModel("4010", activity.getString(R.string.hidden_500h), R.drawable.ac_40_s));
            achivementsGridarraylist.add(new AchivementsGridModel("4011", activity.getString(R.string.hidden_1000h), R.drawable.ac_41_s));
            achivementsGridarraylist.add(new AchivementsGridModel("4012", activity.getString(R.string.hidden_100t), R.drawable.ac_42_s));
            achivementsGridarraylist.add(new AchivementsGridModel("4013", activity.getString(R.string.hidden_500t), R.drawable.ac_43_s));
            achivementsGridarraylist.add(new AchivementsGridModel("4014", activity.getString(R.string.hidden_1000t), R.drawable.ac_44_s));
            achivementsGridarraylist.add(new AchivementsGridModel("4015", activity.getString(R.string.hidden_100b), R.drawable.ac_45_s));
            achivementsGridarraylist.add(new AchivementsGridModel("4016", activity.getString(R.string.hidden_500b), R.drawable.ac_46_s));
            achivementsGridarraylist.add(new AchivementsGridModel("4017", activity.getString(R.string.hidden_1000b), R.drawable.ac_47_s));
        }

        AchivementsGridAdapter adapter = new AchivementsGridAdapter(activity, achivementsGridarraylist, position);
        recyclerView.setAdapter(adapter);

        container.addView(itemView);

        return itemView;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((LinearLayout) object);
    }
}