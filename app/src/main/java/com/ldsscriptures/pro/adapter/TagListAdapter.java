package com.ldsscriptures.pro.adapter;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.daimajia.swipe.SwipeLayout;
import com.daimajia.swipe.adapters.RecyclerSwipeAdapter;
import com.ldsscriptures.pro.BuildConfig;
import com.ldsscriptures.pro.R;
import com.ldsscriptures.pro.activity.BaseActivity;
import com.ldsscriptures.pro.model.BookmarkData;
import com.ldsscriptures.pro.model.VerseTagsData;
import com.ldsscriptures.pro.retroutils.APIClient;
import com.ldsscriptures.pro.retroutils.APIInterface;
import com.ldsscriptures.pro.utils.Global;
import com.ldsscriptures.pro.utils.SessionManager;

import java.util.ArrayList;
import java.util.Arrays;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class TagListAdapter extends RecyclerSwipeAdapter<TagListAdapter.MyViewHolder> {

    private ArrayList<VerseTagsData> verseTagsArrayList = new ArrayList<>();
    private ArrayList<VerseTagsData> oldverseTagsArrayList = new ArrayList<>();
    private Activity activity;
    public String verseText, verseId, verseAId;
    private SessionManager sessionManager;
    private LinearLayout tag_llMain;
    private String stringTag, title;

    @Override
    public int getSwipeLayoutResourceId(int position) {
        return 0;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView tvTags, tvCount;
        public CheckBox chkSelected;
        SwipeLayout swipeLayout;
        TextView tvDelete;

        public MyViewHolder(View view) {
            super(view);
            tvTags = (TextView) view.findViewById(R.id.tvTags);
            tvCount = (TextView) view.findViewById(R.id.tvCount);
            chkSelected = (CheckBox) view.findViewById(R.id.chkSelected);
            tag_llMain = (LinearLayout) view.findViewById(R.id.tag_llMain);
            /*swipeLayout = (SwipeLayout) itemView.findViewById(R.id.tagswipeinner);
            tvDelete = (TextView) itemView.findViewById(R.id.tvDeletetaginner);*/
        }
    }

    public TagListAdapter(Activity activity, ArrayList<VerseTagsData> verseTagsArrayList, ArrayList<VerseTagsData> oldverseTagsArrayList, String verseText, String verseId, String verseAId, String stringTag, String title) {
        this.verseTagsArrayList = verseTagsArrayList;
        this.activity = activity;
        this.verseId = verseId;
        this.verseText = verseText;
        this.verseAId = verseAId;
        this.oldverseTagsArrayList = oldverseTagsArrayList;
        this.stringTag = stringTag;
        this.title = title;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, final int position) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_verse_tag, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {

        final String tagTitle = verseTagsArrayList.get(position).getTagname();
        String tagverseAId = verseTagsArrayList.get(position).getVerseaid();
        final String isdeleted = verseTagsArrayList.get(position).getDeleted();
        final String ismodified = verseTagsArrayList.get(position).getModified();

        holder.tvTags.setText(tagTitle);
        sessionManager = new SessionManager(activity);

        ArrayList<String> items = new ArrayList<String>(Arrays.asList(stringTag.split("\\s*,\\s*")));
        if (items.contains(tagTitle)) {
            holder.chkSelected.setChecked(true);
        } else {
            if (verseAId.equals(tagverseAId)) {
                holder.chkSelected.setChecked(true);
            } else {
                holder.chkSelected.setChecked(false);
            }
        }

        holder.chkSelected.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                CheckBox cb = (CheckBox) v;
                if (cb.isChecked()) {
                    updateSessionData(position, tagTitle, true, title, isdeleted, ismodified);
                } else {
                    updateSessionData(position, tagTitle, false, title, isdeleted, ismodified);
                }
            }
        });

        tag_llMain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                holder.chkSelected.performClick();
            }
        });

    }

    @Override
    public int getItemCount() {
        return verseTagsArrayList.size();
    }

    private void updateSessionData(int position, String title, boolean flagTag, String newtitle, String isdeleted, String ismodified) {

        VerseTagsData verseTagsData = new VerseTagsData();
        verseTagsData.setVerseaid(verseAId);
        verseTagsData.setTagname(title);
        verseTagsData.setVerseNumber(verseId);
        verseTagsData.setTagTitle(newtitle + ":" + verseId);
        verseTagsData.setVerseText(verseText);
        verseTagsData.setChapterUri(Global.bookmarkData.getChapterUri());
        verseTagsData.setItemuri(Global.bookmarkData.getItemuri());
        verseTagsData.setBookuri(Global.bookmarkData.getBookuri());
        verseTagsData.setServerid(String.valueOf(position));
        verseTagsData.setSelected(true);
        verseTagsData.setDeleted(isdeleted);
        verseTagsData.setModified(ismodified);

        if (oldverseTagsArrayList != null) {

            if (flagTag == true) {
                for (int i = 0; i < oldverseTagsArrayList.size(); i++) {

                    if (oldverseTagsArrayList.get(i).getVerseText().equals(verseText)) {
                        if (oldverseTagsArrayList.get(i).getTagTitle().equals(title)) {
                            Global.showToast(activity, "Already Added");
                            break;
                        }
                    } else {
                        oldverseTagsArrayList.add(verseTagsData);

                        String Subscribed = sessionManager.getKeySubscribed();
                        if (Subscribed.equals("1")) {
                            callTagApi(Global.bookmarkData.getItemuri(), verseId, verseAId, title);
                        }
                        break;
                    }
                }
            } else {

                for (int i = 0; i < oldverseTagsArrayList.size(); i++) {
                    if (oldverseTagsArrayList.get(i).getVerseText().equals(verseText)) {
                        if (oldverseTagsArrayList.get(i).getTagTitle().equals(title)) {
                            oldverseTagsArrayList.remove(i);
                            break;
                        }
                    }
                }
            }

            sessionManager.setPreferencesTagsArrayList(activity, oldverseTagsArrayList);
        }
    }

    private void callTagApi(String paraUri, String paraVerseId, String paraVerseAId, String paraNoteTitle) {

        APIInterface apiService = APIClient.getClient().create(APIInterface.class);
        String url = APIClient.BASE_URL;

        String paraUserId = "", paraSessionId = "";

        if (BaseActivity.getInstance() != null) {

            if (BaseActivity.getInstance().getUserInfo(SessionManager.KEY_USER_ID) != null) {
                paraUserId = BaseActivity.getInstance().getUserInfo(SessionManager.KEY_USER_ID);
            }
            if (BaseActivity.getInstance().getUserInfo(SessionManager.KEY_USER_SESSION_ID) != null) {
                paraSessionId = BaseActivity.getInstance().getUserInfo(SessionManager.KEY_USER_SESSION_ID);
            }
        }

        url += "push.pl?user_id=" + paraUserId +
                "&session_id=" + paraSessionId +
                "&apiauth=" + APIClient.AUTH_KEY +
                "&action=" + "add" +
                "&type=" + "tag" +
                "&device=" + APIClient.DEVICE_KEY +
                "&app_version=" + BuildConfig.VERSION_NAME +
                "&uri=" + paraUri +
                "&verse=" + paraVerseId +
                "&tag=" + paraNoteTitle +
                "&aid=" + paraVerseAId +
                "&sort_order=DESC" +
                "&title=" + paraNoteTitle +
                "&locale=" + Global.LANGUAGE;

        Call<BookmarkData> callObject = apiService.getNote(url.replaceAll(" ", "%20"));
        callObject.enqueue(new Callback<BookmarkData>() {

            @Override
            public void onResponse(Call<BookmarkData> call, Response<BookmarkData> response) {

                if (response.body() != null && !response.body().toString().trim().isEmpty()) {

                }
            }

            @Override
            public void onFailure(Call<BookmarkData> call, Throwable t) {

            }
        });
    }

}
