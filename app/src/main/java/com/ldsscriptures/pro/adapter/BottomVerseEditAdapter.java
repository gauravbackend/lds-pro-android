package com.ldsscriptures.pro.adapter;

import android.app.Activity;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.ldsscriptures.pro.R;
import com.ldsscriptures.pro.activity.verses.VerseAddNotesActivity;
import com.ldsscriptures.pro.activity.verses.VerseBookmarkActivity;
import com.ldsscriptures.pro.activity.verses.VerseCrossReferenceActivity;
import com.ldsscriptures.pro.activity.verses.VerseTagActivity;
import com.ldsscriptures.pro.activity.verses.VerseTimelineShareActivity;
import com.ldsscriptures.pro.model.SubMenu;
import com.ldsscriptures.pro.utils.Global;

import java.util.ArrayList;
import java.util.List;

import static android.content.Context.CLIPBOARD_SERVICE;

public class BottomVerseEditAdapter extends RecyclerView.Adapter<BottomVerseEditAdapter.ViewHolder> {

    private String verseAId = "";
    private String title = "";
    private Activity activity;
    private List<SubMenu> subMenus = new ArrayList<>();
    private String verseText;
    private String verseId;
    private String citation_itemUri;
    private int indexId = 0;
    private int versePos = 0;
    private String itemUri;
    private String stringTag;

    public BottomVerseEditAdapter(Activity activity, List<SubMenu> subMenus, String verseId, String verseText, String verseAId, String title, String citation_itemUri, int indexId, int versePos, String itemUri, String stringTag) {
        this.verseId = verseId;
        this.verseText = verseText;
        this.activity = activity;
        this.subMenus = subMenus;
        this.title = title;
        this.verseAId = verseAId;
        this.citation_itemUri = citation_itemUri;
        this.indexId = indexId;
        this.versePos = versePos;
        this.itemUri = itemUri;
        this.stringTag = stringTag;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View cardView = inflater.inflate(R.layout.item_bottom_menu, null, false);
        ViewHolder viewHolder = new ViewHolder(cardView);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        holder.ivSubItem.setImageResource(subMenus.get(position).submenuIcon);
        holder.tvSubItem.setText(subMenus.get(position).submenuTitle);

        holder.ll_bottom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                handleClicks(subMenus.get(position).submenuTitle);
            }
        });

    }

    private void handleClicks(String submenuTitle) {
        switch (submenuTitle) {
            case "Bookmark verse":

                Intent intent = new Intent(activity, VerseBookmarkActivity.class);
                intent.putExtra(activity.getString(R.string.verseText), verseText);
                intent.putExtra(activity.getString(R.string.versePos), versePos);
                intent.putExtra(activity.getString(R.string.verseId), verseId);
                intent.putExtra(activity.getString(R.string.verseAId), verseAId);
                intent.putExtra(activity.getString(R.string.indexId), indexId);
                intent.putExtra(activity.getString(R.string.itemUri), itemUri);

                activity.startActivity(intent);
                activity.overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);

                break;
            case "Add note":
                Intent notsIntent = new Intent(activity, VerseAddNotesActivity.class);
                notsIntent.putExtra(activity.getString(R.string.verseId), verseId);
                notsIntent.putExtra(activity.getString(R.string.verseText), verseText);
                notsIntent.putExtra(activity.getString(R.string.verseAId), verseAId);
                activity.startActivity(notsIntent);
                activity.overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
                break;

            case "Tag verse":
                Intent tagIntent = new Intent(activity, VerseTagActivity.class);
                tagIntent.putExtra(activity.getString(R.string.verseId), verseId);
                tagIntent.putExtra(activity.getString(R.string.verseText), verseText);
                tagIntent.putExtra(activity.getString(R.string.verseAId), verseAId);
                tagIntent.putExtra("stringTag", stringTag);
                tagIntent.putExtra("title", title);

                activity.startActivity(tagIntent);
                activity.overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
                break;

            case "Cross reference":
                Intent crossReferenceIntent = new Intent(activity, VerseCrossReferenceActivity.class);
                crossReferenceIntent.putExtra("title", title);
                crossReferenceIntent.putExtra(activity.getString(R.string.verseId), verseId);
                crossReferenceIntent.putExtra(activity.getString(R.string.verseText), verseText);
                crossReferenceIntent.putExtra(activity.getString(R.string.verseAId), verseAId);
                activity.startActivity(crossReferenceIntent);
                activity.overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
                break;

            case "Copy Verse":
                ClipboardManager clipboard = (ClipboardManager) activity.getSystemService(CLIPBOARD_SERVICE);
                ClipData clip = ClipData.newPlainText("verseText", verseText);
                clipboard.setPrimaryClip(clip);
                Global.showToast(activity, activity.getString(R.string.verse_copied_to_clipboard));

                break;

            case "Timeline Share":

                Intent verseTimelineShare = new Intent(activity, VerseTimelineShareActivity.class);
                verseTimelineShare.putExtra("citation_itemUri", citation_itemUri);
                verseTimelineShare.putExtra("title", title);
                verseTimelineShare.putExtra(activity.getString(R.string.verseId), verseId);
                verseTimelineShare.putExtra(activity.getString(R.string.verseText), verseText);
                verseTimelineShare.putExtra(activity.getString(R.string.verseAId), verseAId);

                activity.startActivity(verseTimelineShare);
                activity.overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
                break;

            case "Share":
                Intent sendIntent = new Intent();
                sendIntent.setAction(Intent.ACTION_SEND);
                sendIntent.putExtra(Intent.EXTRA_TEXT, verseText);
                sendIntent.setType("text/*");
                activity.startActivity(Intent.createChooser(sendIntent, activity.getResources().getText(R.string.send_to)));
                activity.overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);

                break;
        }

    }

    @Override
    public int getItemCount() {
        if (subMenus != null)
            return subMenus.size();
        else return 0;
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        private ImageView ivSubItem;
        private TextView tvSubItem;
        private LinearLayout ll_bottom;

        public ViewHolder(View itemView) {
            super(itemView);
            ivSubItem = (ImageView) itemView.findViewById(R.id.ivSubItem);
            tvSubItem = (TextView) itemView.findViewById(R.id.tvSubItem);
            ll_bottom = (LinearLayout) itemView.findViewById(R.id.ll_bottom);
        }
    }

}
