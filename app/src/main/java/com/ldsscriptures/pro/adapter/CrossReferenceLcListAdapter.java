package com.ldsscriptures.pro.adapter;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.ldsscriptures.pro.R;
import com.ldsscriptures.pro.activity.studytools.AddLessonScripture;
import com.ldsscriptures.pro.activity.verses.VerseCrossReferenceActivity;
import com.ldsscriptures.pro.model.LcData;

import java.util.ArrayList;


public class CrossReferenceLcListAdapter extends RecyclerView.Adapter<CrossReferenceLcListAdapter.MyViewHolder> {

    private ArrayList<LcData> indexList = new ArrayList<>();
    private String s;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView tvName;
        public LinearLayout relMain;

        public MyViewHolder(View view) {
            super(view);
            tvName = (TextView) view.findViewById(R.id.tvName);
            relMain = (LinearLayout) view.findViewById(R.id.relMain);
        }
    }

    public CrossReferenceLcListAdapter(Activity activity, ArrayList<LcData> indexList, String s) {
        this.indexList = indexList;
        this.s = s;
    }

    @Override
    public CrossReferenceLcListAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, final int position) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_cross_reference_ls_list, parent, false);
        return new CrossReferenceLcListAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(CrossReferenceLcListAdapter.MyViewHolder holder, final int position) {

        holder.tvName.setText(indexList.get(position).getTitle());

        holder.relMain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (s.equals("1")) {
                    if (AddLessonScripture.getInstance() != null) {
                        AddLessonScripture.getInstance().setLsData(indexList.get(position).getId());
                    }
                } else {
                    if (VerseCrossReferenceActivity.getInstance() != null) {
                        VerseCrossReferenceActivity.getInstance().setLsData(indexList.get(position).getId());
                    }
                }

            }
        });
    }

    @Override
    public int getItemCount() {
        return indexList.size();
    }
}