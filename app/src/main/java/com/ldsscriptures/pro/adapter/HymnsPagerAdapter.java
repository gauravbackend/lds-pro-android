package com.ldsscriptures.pro.adapter;


import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.ldsscriptures.pro.R;
import com.ldsscriptures.pro.activity.content.LyricsFragment;
import com.ldsscriptures.pro.activity.content.MusicSheetFragment;
import com.ldsscriptures.pro.utils.Global;


public class HymnsPagerAdapter extends FragmentStatePagerAdapter {
    private final String pageUrl;
    private final String pdfUrl;
    private final Context context;
    int tabCount;

    public HymnsPagerAdapter(Context context, FragmentManager fm, int tabCount, String pageUrl, String pdfUrl) {
        super(fm);
        this.context = context;
        this.tabCount = tabCount;
        this.pageUrl = pageUrl;
        this.pdfUrl = pdfUrl;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                Bundle bundle = new Bundle();
                bundle.putString(context.getResources().getString(R.string.page_url), pageUrl);
                LyricsFragment lyricsFragment = new LyricsFragment();
                lyricsFragment.setArguments(bundle);
                return lyricsFragment;
            case 1:
                Bundle bundle1 = new Bundle();
                bundle1.putString(context.getResources().getString(R.string.pdf_url), pdfUrl);
                MusicSheetFragment musicSheetFragment = new MusicSheetFragment();
                musicSheetFragment.setArguments(bundle1);
                return musicSheetFragment;
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return tabCount;
    }
}