package com.ldsscriptures.pro.adapter;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.ldsscriptures.pro.R;
import com.ldsscriptures.pro.activity.studytools.AddLessonScripture;
import com.ldsscriptures.pro.activity.verses.VerseCrossReferenceActivity;
import com.ldsscriptures.pro.model.LsMainIndexData;

import java.util.ArrayList;

public class CrossReferenceLibIndexAdapter extends RecyclerView.Adapter<CrossReferenceLibIndexAdapter.MyViewHolder> {

    private final String lsId;
    private ArrayList<LsMainIndexData> indexList = new ArrayList<>();
    private Activity activity;
    String s;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView tvName;
        public LinearLayout relMain;


        public MyViewHolder(View view) {
            super(view);
            tvName = (TextView) view.findViewById(R.id.tvName);
            relMain = (LinearLayout) view.findViewById(R.id.relMain);
        }
    }

    public CrossReferenceLibIndexAdapter(Activity activity, ArrayList<LsMainIndexData> indexList, String lsId, String s) {
        this.indexList = indexList;
        this.activity = activity;
        this.lsId = lsId;
        this.s = s;
    }

    @Override
    public CrossReferenceLibIndexAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, final int position) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_cross_reference_ls_list, parent, false);
        return new CrossReferenceLibIndexAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(CrossReferenceLibIndexAdapter.MyViewHolder holder, final int position) {

        holder.tvName.setText(indexList.get(position).getTitle_html());

        holder.relMain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (s.equals("1")) {
                    if (AddLessonScripture.getInstance() != null) {
                        AddLessonScripture.getInstance().setLibSubIndex(position, indexList.get(position).getId(), indexList.get(position).getTitle_html(), lsId);
                    }
                } else {
                    if (VerseCrossReferenceActivity.getInstance() != null) {
                        VerseCrossReferenceActivity.getInstance().setLibSubIndex(position, indexList.get(position).getId(), indexList.get(position).getTitle_html(), lsId);
                    }
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return indexList.size();
    }
}