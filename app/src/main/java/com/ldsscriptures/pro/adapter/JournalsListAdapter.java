package com.ldsscriptures.pro.adapter;


import android.content.Context;
import android.database.DataSetObserver;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListAdapter;
import android.widget.TextView;

import com.ldsscriptures.pro.R;
import com.ldsscriptures.pro.model.JournalData;
import com.ldsscriptures.pro.utils.Global;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Locale;

public class JournalsListAdapter implements ExpandableListAdapter {

    private Context context;
    private ArrayList<JournalData> journalDataArrayList = new ArrayList<JournalData>();

    public JournalsListAdapter(Context context, ArrayList<JournalData> journalDataArrayList) {
        this.context = context;
        this.journalDataArrayList = journalDataArrayList;
    }

    @Override
    public void registerDataSetObserver(DataSetObserver observer) {

    }

    @Override
    public void unregisterDataSetObserver(DataSetObserver observer) {


    }

    @Override
    public int getGroupCount() {
        return journalDataArrayList.size();
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return 1;
    }

    @Override
    public Object getGroup(int groupPosition) {
        return journalDataArrayList.get(groupPosition);
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        return journalDataArrayList.get(groupPosition);
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public boolean hasStableIds() {
        return true;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        ParentHolder parentHolder = null;
        JournalData journalData = (JournalData) getGroup(groupPosition);

        if (convertView == null) {
            LayoutInflater userInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = userInflater.inflate(R.layout.item_journal_group_date, null);
            convertView.setHorizontalScrollBarEnabled(true);

            parentHolder = new ParentHolder();
            convertView.setTag(parentHolder);

        } else {
            parentHolder = (ParentHolder) convertView.getTag();
        }
        parentHolder.textView = (TextView) convertView.findViewById(R.id.textView);

        if (journalData.getGroupingdate() != null) {

            String inputDateStr = journalData.getGroupingdate();
            if (checkIsYesterday(inputDateStr))
                parentHolder.textView.setText(R.string.yesterday);
            else
                parentHolder.textView.setText(inputDateStr);
        }

        return convertView;
    }

    public boolean checkIsYesterday(String date) {
        int dateDifference = (int) Global.getDateDiff(new SimpleDateFormat("MMM dd, yyyy", Locale.US), date, Global.getCurrentDate());
        return dateDifference == 1;
    }

    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {

        ChildHolder childHolder = null;
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.item_journal_group_child, parent, false);
            childHolder = new ChildHolder();
            convertView.setTag(childHolder);
        } else {
            childHolder = (ChildHolder) convertView.getTag();
        }

        childHolder.rcSubMenu = (RecyclerView) convertView.findViewById(R.id.rcSubMenu);
        childHolder.rcSubMenu.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false));
        childHolder.rcSubMenu.setAdapter(new JournalGroupSubItemAdapter(context, journalDataArrayList.get(groupPosition).getJournalItemDataArrayList()));

        return convertView;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return false;
    }

    @Override
    public boolean areAllItemsEnabled() {
        return false;
    }

    @Override
    public boolean isEmpty() {
        return false;
    }

    @Override
    public void onGroupExpanded(int groupPosition) {
    }

    @Override
    public void onGroupCollapsed(int groupPosition) {

    }

    @Override
    public long getCombinedChildId(long groupId, long childId) {
        return 0;
    }

    @Override
    public long getCombinedGroupId(long groupId) {
        return 0;
    }

    private static class ChildHolder {
        static RecyclerView rcSubMenu;
    }

    private static class ParentHolder {
        TextView textView;
    }
}
