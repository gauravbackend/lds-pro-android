package com.ldsscriptures.pro.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.ldsscriptures.pro.fragment.LeaderboardMonthTabFragment;
import com.ldsscriptures.pro.fragment.LeaderboardWeekTabFragment;
import com.ldsscriptures.pro.fragment.LeaderboardYearTabFragment;

public class LeaderBoardPagerAdapter extends FragmentStatePagerAdapter {
    int mNumOfTabs;

    public LeaderBoardPagerAdapter(FragmentManager fm, int NumOfTabs) {
        super(fm);
        this.mNumOfTabs = NumOfTabs;

    }

    @Override
    public Fragment getItem(int position) {

        switch (position) {
            case 0:
                LeaderboardWeekTabFragment tab1 = new LeaderboardWeekTabFragment();
                return tab1;
            case 1:
                LeaderboardMonthTabFragment tab2 = new LeaderboardMonthTabFragment();
                return tab2;
            case 2:
                LeaderboardYearTabFragment tab3 = new LeaderboardYearTabFragment();
                return tab3;
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return mNumOfTabs;
    }

}
