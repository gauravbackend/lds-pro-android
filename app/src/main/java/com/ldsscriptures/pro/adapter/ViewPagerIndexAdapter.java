package com.ldsscriptures.pro.adapter;

import android.app.Activity;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Environment;
import android.os.Handler;
import android.os.Parcelable;
import android.support.v4.view.PagerAdapter;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.Html;
import android.text.SpannableString;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.ldsscriptures.pro.R;
import com.ldsscriptures.pro.activity.ReadingActivity;
import com.ldsscriptures.pro.database.CitationsDatabaseHelper;
import com.ldsscriptures.pro.dbClass.GetDataFromDatabase;
import com.ldsscriptures.pro.model.IndexWrapper;
import com.ldsscriptures.pro.model.ReadingViewpagerData;
import com.ldsscriptures.pro.model.VerseData;
import com.ldsscriptures.pro.utils.Global;
import com.ldsscriptures.pro.utils.SessionManager;

import org.jsoup.select.Elements;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ViewPagerIndexAdapter extends PagerAdapter {

    private final String lcId;
    private final String lsId;
    private final int indexForReading;
    ArrayList<ReadingViewpagerData> lsIndexPreviewDatas;
    private LayoutInflater inflater;
    final Activity activity;
    private Elements studySummary;
    private List<IndexWrapper> wrappers = new ArrayList<>();
    private TimerTask scrollerSchedule;
    private Timer scrollTimer;
    private Handler handler = new Handler();
    private Runnable runnable;
    private CitationsDatabaseHelper citationsDatabaseHelper;
    private int currentSelected = 0;
    private int[] scrollingSpeedArr = {2, 4, 6, 8, 10, 12, 14, 16, 18, 20};
    private int fixSpeedAttribute = 10;
    private int currentSpeed = 5;
    private String[] dataAid;
    private int indexID, verseAId, verseId;
    private ReadingStepRecyclerAdapter readingStepRecyclerAdapter;
    public static ViewPagerIndexAdapter viewPagerIndexAdapter;
    private int mPosition;
    SessionManager sessionManager;
    String headerContent = "";
    String verseIdArrayStrig = "";
    RecyclerView recyclerViewNew;

    public static ViewPagerIndexAdapter getInstance() {
        return viewPagerIndexAdapter;
    }

    public ViewPagerIndexAdapter(Activity activity, ArrayList<ReadingViewpagerData> lsIndexPreviewDatas, String lcId, String lsId, int verseAId, int verseId, int indexForReading, String title, String verseIdArrayStrig) {
        this.activity = activity;
        this.lcId = lcId;
        this.lsId = lsId;
        this.lsIndexPreviewDatas = lsIndexPreviewDatas;
        inflater = LayoutInflater.from(activity);
        this.verseAId = verseAId;
        this.verseId = verseId;
        this.indexForReading = indexForReading;
        citationsDatabaseHelper = new CitationsDatabaseHelper(activity);
        this.sessionManager = new SessionManager(activity);
        this.verseIdArrayStrig = verseIdArrayStrig;
    }

    private void setAutoScroll(final RecyclerView recyclerView, final LinearLayout linScroll) {
        if (scrollTimer != null)
            scrollTimer.cancel();

        Handler handler1 = new Handler();
        handler1.postDelayed(new Runnable() {
            public void run() {
                if (ReadingActivity.getInstance() != null) {
                    ReadingActivity.getInstance().HideLayout();
                }
                if (linScroll != null) {
                    if (linScroll.getVisibility() == View.VISIBLE) {
                        linScroll.setVisibility(View.GONE);
                    }
                }
            }
        }, 4000);


        handler = new Handler();

        scrollTimer = new Timer();
        scrollerSchedule = new TimerTask() {
            @Override
            public void run() {
                runnable = new Runnable() {
                    @Override
                    public void run() {
                        new moveScrollView().execute(recyclerView);
                    }
                };
                handler.post(runnable);
            }
        };
        scrollTimer.schedule(scrollerSchedule, 0, scrollingSpeedArr[currentSpeed] * fixSpeedAttribute);
    }

    @Override
    public int getCount() {
        return lsIndexPreviewDatas.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view.equals(object);
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }

    @Override
    public Object instantiateItem(ViewGroup view, final int position) {

        this.mPosition = position;

        View inflate = inflater.inflate(R.layout.item_viepager_index, view, false);
        assert inflate != null;
        final RecyclerView recyclerView = (RecyclerView) inflate.findViewById(R.id.recyclerView);
        final LinearLayout linScroll = (LinearLayout) inflate.findViewById(R.id.linScroll);
        final TextView txtScroll = (TextView) inflate.findViewById(R.id.txtScroll);
        final ImageView ivScrollMinus = (ImageView) inflate.findViewById(R.id.ivScrollMinus);
        final ImageView ivScrollPlus = (ImageView) inflate.findViewById(R.id.ivScrollPlus);
        final EditText edSearch = (EditText) inflate.findViewById(R.id.edSearch);
        final TextView txtSearchCount = (TextView) inflate.findViewById(R.id.txtSearchCount);
        final ImageView ivSearchUp = (ImageView) inflate.findViewById(R.id.ivSearchUp);
        final ImageView ivSearchDown = (ImageView) inflate.findViewById(R.id.ivSearchDown);
        final ImageView ivClose = (ImageView) inflate.findViewById(R.id.ivClose);
        final LinearLayout linSearchOption = (LinearLayout) inflate.findViewById(R.id.linSearchOption);
        final LinearLayout linScrollOption = (LinearLayout) inflate.findViewById(R.id.linScrollOption);
        final LinearLayout lnrHeader = (LinearLayout) inflate.findViewById(R.id.lnrHeader);
        final LinearLayout lnrBody = (LinearLayout) inflate.findViewById(R.id.lnrBody);
        final WebView webView = (WebView) inflate.findViewById(R.id.webView);
        int nextID = lsIndexPreviewDatas.get(position).getId();

        SetrecyclerViewNew(recyclerView);

        ArrayList<VerseData> stringArrayList = new ArrayList<>();
        stringArrayList = getDataSession(activity, lsId, nextID);

        if (stringArrayList != null) {
            if (stringArrayList.size() != 0) {
                lnrHeader.setVisibility(View.GONE);
                lnrBody.setVisibility(View.VISIBLE);

                if (Global.audioFlag == 1) {
                    linScroll.setVisibility(View.VISIBLE);
                } else {
                    linScroll.setVisibility(View.GONE);
                }


                indexID = lsIndexPreviewDatas.get(position).getId();
                String indexURI = lsIndexPreviewDatas.get(position).getUri();
                getSubItemHtmlData(position, recyclerView, linScroll, edSearch, txtSearchCount, stringArrayList, indexURI);

                /*if (Global.visible_flag.contains("1")) {
                    linScroll.setVisibility(View.GONE);
                }*/

                edSearch.setOnTouchListener(new View.OnTouchListener() {
                    @Override
                    public boolean onTouch(View v, MotionEvent event) {
                        linScrollOption.setVisibility(View.GONE);
                        linSearchOption.setVisibility(View.VISIBLE);
                        return false;
                    }
                });

                recyclerView.setOnTouchListener(new View.OnTouchListener() {
                    @Override
                    public boolean onTouch(View v, MotionEvent event) {
                        if (scrollTimer != null)
                            scrollTimer.cancel();
                        return false;
                    }
                });

                ivSearchUp.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (currentSelected != 0) {
                            currentSelected--;
                            txtSearchCount.setText(currentSelected + "/" + wrappers.size());
                            Global.hideKeyboard(activity);
                            if (wrappers.get(currentSelected).getIndex() != 0)
                                recyclerView.scrollToPosition(wrappers.get(currentSelected).getIndex());
                        }
                    }
                });

                ivSearchDown.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (currentSelected != wrappers.size()) {
                            currentSelected++;
                            txtSearchCount.setText(currentSelected + "/" + wrappers.size());
                            Global.hideKeyboard(activity);
                            if (wrappers.size() != 0) {
                                if (wrappers.get(currentSelected).getIndex() != 0)
                                    recyclerView.scrollToPosition(wrappers.get(currentSelected).getIndex());
                            }
                        }
                    }
                });


                ivClose.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        linScrollOption.setVisibility(View.VISIBLE);
                        linSearchOption.setVisibility(View.GONE);
                        edSearch.setText("");
                        currentSelected = 0;
                        Global.hideKeyboard(activity);
                    }
                });


                txtScroll.setOnClickListener(new View.OnClickListener()

                {
                    @Override
                    public void onClick(View v) {
                        setAutoScroll(recyclerView, linScroll);
                    }
                });

                ivScrollMinus.setOnClickListener(new View.OnClickListener()

                {
                    @Override
                    public void onClick(View v) {
                        if (currentSpeed != scrollingSpeedArr.length - 1)
                            currentSpeed++;
                        else {
                            Global.showToast(activity, "Minimum speed set...");
                        }
                        if (scrollTimer != null)
                            scrollTimer.cancel();
                        scrollTimer = new Timer();
                        scrollerSchedule = new TimerTask() {
                            @Override
                            public void run() {

                                handler.post(new Runnable() {
                                    @Override
                                    public void run() {
                                        new moveScrollView().execute(recyclerView);
                                    }
                                });
                            }
                        };
                        scrollTimer.schedule(scrollerSchedule, 0, scrollingSpeedArr[currentSpeed] * fixSpeedAttribute);
                    }
                });

                ivScrollPlus.setOnClickListener(new View.OnClickListener()

                {
                    @Override
                    public void onClick(View v) {
                        if (currentSpeed != 0)
                            currentSpeed--;
                        else {
                            Global.showToast(activity, "Maximum speed set...");
                        }
                        if (scrollTimer != null)
                            scrollTimer.cancel();
                        scrollTimer = new Timer();
                        scrollerSchedule = new TimerTask() {
                            @Override
                            public void run() {

                                handler.post(new Runnable() {

                                    @Override
                                    public void run() {
                                        //Here I want to increment the scrollbar's vertical  position by 10
                                        new moveScrollView().execute(recyclerView);
                                    }
                                });
                            }
                        };
                        scrollTimer.schedule(scrollerSchedule, 0, scrollingSpeedArr[currentSpeed] * fixSpeedAttribute);
                    }
                });
            } else {

                lnrBody.setVisibility(View.GONE);
                lnrHeader.setVisibility(View.VISIBLE);
                final String htmlData = new GetDataFromDatabase(activity, lsId).getWebViewData(lsIndexPreviewDatas.get(position).getId());
                String SUB_DB_PATH;
                if (android.os.Build.VERSION.SDK_INT >= 17) {
                    SUB_DB_PATH = Environment.getExternalStorageDirectory() + "/LDS/" + lsId + "/";
                } else {
                    SUB_DB_PATH = Environment.getExternalStorageDirectory() + "/LDS/" + lsId + "/";
                }
                String myPath = SUB_DB_PATH + "webView.html";
                // Save your stream, don't forget to flush() it before closing it.
                final File file = new File(myPath);
                try {
                    file.createNewFile();
                    FileOutputStream fOut = new FileOutputStream(file);
                    OutputStreamWriter myOutWriter = new OutputStreamWriter(fOut);
                    myOutWriter.append(htmlData);
                    myOutWriter.close();
                    fOut.flush();
                    fOut.close();
                } catch (IOException e) {
                    Log.e("Exception", "File write failed: " + e.toString());
                }

                final String url1 = "file://" + file.getAbsolutePath();
                webView.getSettings().setCacheMode(WebSettings.LOAD_DEFAULT);
                try {
                    if (Global.isNetworkAvailable(activity)) {
                        WebSettings settings = webView.getSettings();
                        settings.setJavaScriptEnabled(true);
                        webView.setScrollBarStyle(WebView.SCROLLBARS_OUTSIDE_OVERLAY);
//                    DialogUtils.displayAlert(getContext());

                        webView.setWebViewClient(new WebViewClient() {
                            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                                view.loadUrl(url1);
                                return false;
                            }

                            public void onPageFinished(WebView view, String url) {
                                view.clearCache(true);
                            }


                            public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
                            }
                        });
                        Map<String, String> noCacheHeaders = new HashMap<String, String>(2);
                        noCacheHeaders.put("Pragma", "no-cache");
                        noCacheHeaders.put("Cache-Control", "no-cache");
                        webView.loadUrl(url1, noCacheHeaders);
                        webView.setFocusableInTouchMode(false);
                        webView.setFocusable(false);
                    } else {
                        //Global.showNetworkAlert(activity);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }


        final int[] y = new int[1];

        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                y[0] = dy;

                LinearLayoutManager llm = (LinearLayoutManager) recyclerView.getLayoutManager();
                int recycleviewOffset = llm.findFirstCompletelyVisibleItemPosition();

                if (recycleviewOffset != 0) {
                    sessionManager.setoffset(recycleviewOffset);
                }

            }

            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);

                if (recyclerView.SCROLL_STATE_DRAGGING == newState) {
                    ReadingActivity.mainSwipeLayout.setVisibility(View.GONE);
                }
                if (recyclerView.SCROLL_STATE_IDLE == newState) {

                    if (y[0] <= 0) {
                        ReadingActivity.mainSwipeLayout.setVisibility(View.VISIBLE);
                        ReadingActivity.hide_view.setVisibility(View.VISIBLE);

                    } else {
                        y[0] = 0;
                        ReadingActivity.mainSwipeLayout.setVisibility(View.GONE);
                        ReadingActivity.hide_view.setVisibility(View.GONE);
                    }
                }
            }
        });

        view.addView(inflate, 0);
        return inflate;
    }

    private void countSearchWord(String searchString, ArrayList<VerseData> stringArrayList) {
        wrappers = new ArrayList<>();
        for (int i = 1; i < stringArrayList.size(); i++) {
            String myStringNew = "";
            if (stringArrayList.get(i).getStringSentense() != null && stringArrayList.get(i).getId() != null)
                myStringNew = stringArrayList.get(i).getStringSentense().replace(stringArrayList.get(i).getId(), "");
            SpannableString text = new SpannableString(Html.fromHtml(myStringNew));
            Matcher matcher = Pattern.compile(Pattern.quote(searchString.trim()), Pattern.CASE_INSENSITIVE).matcher(text.toString());
            while (matcher.find()) {
                int end = matcher.end();
                int start = matcher.start();
                IndexWrapper wrapper = new IndexWrapper(start, end, i);
                wrappers.add(wrapper);
            }
        }
    }

    private class moveScrollView extends AsyncTask<RecyclerView, Void, Void> {
        @Override
        protected Void doInBackground(final RecyclerView... params) {
            ((Activity) activity).runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    // TODO
                    int scrollPos = (int) (params[0].getScrollY() + 2.0);
                    params[0].scrollBy(0, scrollPos);


                }
            });
            return null;
        }

        protected void onProgressUpdate(Void... progress) {

        }

    }

    @Override
    public Parcelable saveState() {
        return null;
    }

    private void getSubItemHtmlData(int position, final RecyclerView recyclerView, final LinearLayout linScroll, final EditText edSearch, final TextView txtSearchCount, final ArrayList<VerseData> stringArrayList, final String indexURI) {

        int nextId = lsIndexPreviewDatas.get(position).getId();
        final String title = lsIndexPreviewDatas.get(position).getTitle_html();
        headerContent = new GetDataFromDatabase(activity, lsId).getHeaderString(nextId);
        HashMap citationHashMap = null;

        Cursor cursor;
        cursor = citationsDatabaseHelper.getCitationCountTable(indexURI);

        if (cursor != null && cursor.moveToFirst()) {
            while (!cursor.isAfterLast()) {
                citationHashMap = convertHashMApToString(cursor.getString(cursor.getColumnIndex("json_count")));
                cursor.moveToNext();
            }
        }

        citationsDatabaseHelper.close();

        if (cursor != null) {
            cursor.close();
        }

        setRecyclerView(stringArrayList, recyclerView, linScroll, headerContent, "", edSearch, title, citationHashMap, indexURI);

        final HashMap finalCitationHashMap = citationHashMap;
        edSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                txtSearchCount.setText(activity.getResources().getString(R.string._0_0));
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                highLightSearch(stringArrayList, recyclerView, linScroll, headerContent, s.toString(), edSearch, title, finalCitationHashMap, indexURI);
                countSearchWord(s.toString(), stringArrayList);
                txtSearchCount.setText(currentSelected + "/" + wrappers.size());
                if (s.toString().length() < 1) {
                    currentSelected = 0;
                    txtSearchCount.setText(activity.getResources().getString(R.string._0_0));
                }
            }
        });
    }

    private void setRecyclerView(final ArrayList<VerseData> stringArrayList, RecyclerView recyclerView, LinearLayout linScroll, String headerText, String searchContent, EditText edSearch, String title, HashMap citationHashMap, String indexURI) {

        LinearLayoutManager llm = new LinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(llm);
        stringArrayList.add(0, new VerseData());
        readingStepRecyclerAdapter = new ReadingStepRecyclerAdapter(activity, stringArrayList, linScroll, searchContent, headerText, indexID, recyclerView, edSearch, lsId, verseAId, title, citationHashMap, indexURI, verseIdArrayStrig);
        recyclerView.setAdapter(readingStepRecyclerAdapter);

        if (verseId != 0) {

            //Global.printLog("======SCROLL=====", "1111111111");

            Global.recycleviewpos = verseId;
            recyclerView.scrollToPosition(Global.recycleviewpos);
        } else {

            // Global.printLog("======SCROLL=====", "2222222" + sessionManager.getoffset());

            LinearLayoutManager llm1 = (LinearLayoutManager) recyclerView.getLayoutManager();
            llm1.scrollToPositionWithOffset(sessionManager.getoffset(), stringArrayList.size());
        }

    }

    private void highLightSearch(ArrayList<VerseData> displayDataArrayList, RecyclerView recyclerView, LinearLayout linScroll, String headerContent, String string, EditText edSearch, String title, HashMap citationHashMap, String indexURI) {
        recyclerView.setAdapter(new ReadingStepRecyclerAdapter(activity, displayDataArrayList, linScroll, string, headerContent, indexID, recyclerView, edSearch, lsId, verseAId, title, citationHashMap, indexURI, verseIdArrayStrig));
    }

    public int getItemPosition(Object object) {
        return POSITION_NONE;
    }

    public void notifyFontStyleChange() {
        readingStepRecyclerAdapter.notifyDataSetChanged();
    }

    public HashMap convertHashMApToString(String content) {
        Gson gson = new Gson();
        Type type = new TypeToken<HashMap>() {
        }.getType();
        // ArrayList<CitationList> arrayList = gson.fromJson(content, type);
        HashMap arrayList = gson.fromJson(content, type);
        return arrayList;
    }

    public ArrayList<VerseData> getDataSession(Activity activity, String lsId, int nextID) {

        ArrayList<VerseData> stringArrayList = new ArrayList<>();

        try {
            stringArrayList = sessionManager.getReadInnerArrayList(String.valueOf(nextID));

            if (stringArrayList == null) {

                stringArrayList = new ArrayList<>();
                stringArrayList = new GetDataFromDatabase(activity, lsId).getSubItemHtmlData(nextID);

            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return stringArrayList;
    }

    public void SetrecyclerViewNew(RecyclerView recyclerView) {
        recyclerViewNew = recyclerView;
    }

    public RecyclerView getRecyclerViewNew() {
        return recyclerViewNew;
    }


}

