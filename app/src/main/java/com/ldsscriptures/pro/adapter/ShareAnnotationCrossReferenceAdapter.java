package com.ldsscriptures.pro.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.ldsscriptures.pro.R;
import com.ldsscriptures.pro.model.CrossRefrenceData;
import com.ldsscriptures.pro.utils.Global;

import java.util.ArrayList;
import java.util.Date;


public class ShareAnnotationCrossReferenceAdapter extends RecyclerView.Adapter<ShareAnnotationCrossReferenceAdapter.ViewHolder> {
    private Context context;
    private ArrayList<CrossRefrenceData> notesDataArrayList = new ArrayList<>();

    public ShareAnnotationCrossReferenceAdapter(Context context, ArrayList<CrossRefrenceData> notesDataArrayList) {
        this.notesDataArrayList = notesDataArrayList;
        this.context = context;
    }

    @Override
    public ShareAnnotationCrossReferenceAdapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_cross_ref_share_annotation, viewGroup, false);
        return new ShareAnnotationCrossReferenceAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ShareAnnotationCrossReferenceAdapter.ViewHolder viewHolder, int position) {
        if (notesDataArrayList.get(position).getTitle() != null) {
            viewHolder.txtTitle.setText(notesDataArrayList.get(position).getTitle());
            viewHolder.txtTitle.setVisibility(View.VISIBLE);
        } else {
            viewHolder.txtTitle.setVisibility(View.INVISIBLE);
        }

        if (notesDataArrayList.get(position).getUri() != null) {
            viewHolder.txtDetails.setText(notesDataArrayList.get(position).getUri());
        } else {
            viewHolder.txtDetails.setVisibility(View.INVISIBLE);
        }

        if (Integer.parseInt(notesDataArrayList.get(position).getCount()) != 0) {
            viewHolder.txtCount.setText(notesDataArrayList.get(position).getCount());
            viewHolder.txtCount.setVisibility(View.VISIBLE);

        } else {
            viewHolder.txtCount.setVisibility(View.INVISIBLE);
        }
    }

    @Override
    public int getItemCount() {
        return notesDataArrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView txtTitle;
        TextView txtDetails;
        TextView txtCount;
        View view1;

        public ViewHolder(View view) {
            super(view);
            txtTitle = (TextView) view.findViewById(R.id.txtTitle);
            txtCount = (TextView) view.findViewById(R.id.txtCount);
            txtDetails = (TextView) view.findViewById(R.id.txtDetails);
            view1 = view.findViewById(R.id.view);
        }
    }

}