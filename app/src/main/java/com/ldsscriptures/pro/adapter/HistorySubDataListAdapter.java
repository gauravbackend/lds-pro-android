package com.ldsscriptures.pro.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.ldsscriptures.pro.R;
import com.ldsscriptures.pro.database.DataBaseHelper;
import com.ldsscriptures.pro.database.SubDataBaseHelper;
import com.ldsscriptures.pro.model.HistorySubData;
import com.ldsscriptures.pro.model.TimeLine.VerseDataTimeLineModel;

import java.util.ArrayList;

public class HistorySubDataListAdapter extends RecyclerView.Adapter<HistorySubDataListAdapter.ViewHolder> {

    private Context context;
    private ArrayList<HistorySubData> historySubDataArrayList = new ArrayList<>();
    DataBaseHelper databasehelper;
    VerseDataTimeLineModel verseDataTimeLineModel;

    public HistorySubDataListAdapter(Context context, ArrayList<HistorySubData> historySubDataArrayList) {
        this.context = context;
        this.historySubDataArrayList = historySubDataArrayList;
        databasehelper = new DataBaseHelper(context);
    }

    @Override
    public HistorySubDataListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View cardView = inflater.inflate(R.layout.item_history_list, null, false);
        HistorySubDataListAdapter.ViewHolder viewHolder = new HistorySubDataListAdapter.ViewHolder(cardView);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(HistorySubDataListAdapter.ViewHolder holder, final int position) {
        if (historySubDataArrayList.get(position).getTitle() != null)
            holder.tvTitle.setText(historySubDataArrayList.get(position).getTitle());


        holder.relMain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                try {
                    Intent intent = new Intent(context, Class.forName(historySubDataArrayList.get(position).getTag()));
                    intent.putExtra(context.getString(R.string.indexForReading), historySubDataArrayList.get(position).getReadingIndex());
                    intent.putExtra(context.getString(R.string.isFromIndex), historySubDataArrayList.get(position).isFromIndex());
                    intent.putExtra(context.getString(R.string.is_history), historySubDataArrayList.get(position).isFromHistory());
                    intent.putExtra(context.getString(R.string.lcID), historySubDataArrayList.get(position).getLcId());
                    intent.putExtra(context.getString(R.string.lsId), historySubDataArrayList.get(position).getLsId());
                    context.startActivity(intent);
                    ((Activity) context).overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
                } catch (ClassNotFoundException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        if (historySubDataArrayList != null)
            return historySubDataArrayList.size();
        else return 0;
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }


    public static class ViewHolder extends RecyclerView.ViewHolder {
        private TextView tvTitle;
        private RelativeLayout relMain;

        public ViewHolder(View itemView) {
            super(itemView);
            tvTitle = (TextView) itemView.findViewById(R.id.tvTitle);
            relMain = (RelativeLayout) itemView.findViewById(R.id.relMain);
        }
    }


    public VerseDataTimeLineModel getVerseText(String fullVerseUri, String var_timeline_Verse_Id) {

        Cursor cursor;
        String LcId = "";
        try {

            String[] URI_TXT = fullVerseUri.split("\\/");
            String URI_TXT_First = URI_TXT[1];
            String URI_TXT_Second = URI_TXT[2];
            cursor = databasehelper.getVerseTextAndVersePath(URI_TXT_First);
            if (cursor != null && cursor.moveToFirst()) {

                while (!cursor.isAfterLast()) {
                    LcId = cursor.getString(cursor.getColumnIndex(DataBaseHelper.KEY_ID));
                    cursor.moveToNext();
                }
            }
            if (cursor != null) {
                cursor.close();
            }
            String URI_TXT_First1 = URI_TXT_First.substring(0, 1).toUpperCase() + URI_TXT_First.substring(1);

            verseDataTimeLineModel = getLsDataFromDb(LcId, URI_TXT_Second, fullVerseUri, var_timeline_Verse_Id, URI_TXT_First1);
            verseDataTimeLineModel.setLcId(LcId);


        } catch (Exception e) {
            e.printStackTrace();
        }
        return verseDataTimeLineModel;
    }

    public VerseDataTimeLineModel getLsDataFromDb(String LcId, String verseUri, String fullVerseUri, String var_timeline_Verse_Id, String URI_TXT_First) {

        Cursor cursor;
        String LsItemId = "";
        String LsItemTitle = "";
        if (LcId.isEmpty()) {
            int index = fullVerseUri.lastIndexOf('/');
            cursor = databasehelper.getVerseLSPathEmpty(fullVerseUri.substring(0, index));
        } else {
            cursor = databasehelper.getVerseLSPath(Integer.parseInt(LcId), verseUri);
        }

        if (cursor != null && cursor.moveToFirst()) {
            while (!cursor.isAfterLast()) {
                LsItemId = cursor.getString(cursor.getColumnIndex(DataBaseHelper.KEY_LS_RESULT_ITEM_ID));
                LsItemTitle = cursor.getString(cursor.getColumnIndex(DataBaseHelper.KEY_LS_RESULT_TITLE_HTML));

                URI_TXT_First = URI_TXT_First + " / " + LsItemTitle;
                verseDataTimeLineModel = getVerseIndexDataFromDB(LsItemId, fullVerseUri, var_timeline_Verse_Id, URI_TXT_First);
                verseDataTimeLineModel.setLsId(LsItemId);

                cursor.moveToNext();
            }
        }
        return verseDataTimeLineModel;
    }

    public VerseDataTimeLineModel getVerseIndexDataFromDB(String LsItemId, String fullVerseUri, String var_timeline_Verse_Id, String URI_TXT_First) {
        SubDataBaseHelper subDataBaseHelper;
        subDataBaseHelper = new SubDataBaseHelper(context, LsItemId);

        Cursor cursor;
        cursor = subDataBaseHelper.getVerseTextFromURI(fullVerseUri);

        if (cursor != null && cursor.moveToFirst()) {
            while (!cursor.isAfterLast()) {

                String LsHrmlTextItem = cursor.getString(cursor.getColumnIndex(DataBaseHelper.KEY_LS_RESULT_TITLE_HTML));
                byte[] blob = cursor.getBlob(cursor.getColumnIndex(SubDataBaseHelper.KEY_SUB_ITEM_HTML_C1CONTENT_HTML));
                String idForReading = cursor.getString(cursor.getColumnIndex(SubDataBaseHelper.KEY_ID));
                String postionForReading = cursor.getString(cursor.getColumnIndex(SubDataBaseHelper.KEY_FT_POSITION));
                String newHtmlVerseArray = new String(blob);
                verseDataTimeLineModel.setVersePath(LsHrmlTextItem);
                verseDataTimeLineModel.setVerseFullPath(URI_TXT_First + " / " + LsHrmlTextItem);
                verseDataTimeLineModel.setIndexForReading(idForReading);

                cursor.moveToNext();
            }
        }
        subDataBaseHelper.close();
        if (cursor != null) {
            cursor.close();
        }
        return verseDataTimeLineModel;
    }


}
