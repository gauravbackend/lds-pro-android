package com.ldsscriptures.pro.adapter;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.ldsscriptures.pro.R;
import com.ldsscriptures.pro.activity.activitymenu.AchievementsDetailsActivity;
import com.ldsscriptures.pro.model.AchivementsFlagModel;
import com.ldsscriptures.pro.model.AchivementsGridModel;
import com.ldsscriptures.pro.utils.Global;
import com.ldsscriptures.pro.utils.SessionManager;

import java.util.ArrayList;

/**
 * Created by IBL InfoTech on 7/25/2017.
 */

public class AchivementsGridAdapter extends RecyclerView.Adapter<AchivementsGridAdapter.ViewHolder> {
    private ArrayList<AchivementsGridModel> achivementsGridarraylist;
    private Activity activity;
    private SessionManager sessionManager;
    ArrayList<AchivementsFlagModel> achivementsFlagarraylist = new ArrayList<>();
    int adapter_pos;

    public AchivementsGridAdapter(Activity activity, ArrayList<AchivementsGridModel> achivementsGridarraylist, int adapter_pos) {
        this.achivementsGridarraylist = achivementsGridarraylist;
        this.activity = activity;
        sessionManager = new SessionManager(activity);
        achivementsFlagarraylist = sessionManager.getAchivementsFlagArrayList();
        this.adapter_pos = adapter_pos;
    }

    @Override
    public AchivementsGridAdapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.activity_achievements_grid_item, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(AchivementsGridAdapter.ViewHolder viewHolder, int i) {

        String achivements_id = achivementsGridarraylist.get(i).getAchivements_id();
        String achievements_titles = achivementsGridarraylist.get(i).getAchivements_title();
        int achievements_image = achivementsGridarraylist.get(i).getAchivements_image();
        int achivements_pos = i;

        if (adapter_pos == 3) {
            viewHolder.achievements_titles.setVisibility(View.GONE);
            viewHolder.achievements_images.setVisibility(View.GONE);
        } else {
            viewHolder.achievements_titles.setText(achievements_titles.toString());
            viewHolder.achievements_images.setImageResource(achievements_image);
        }

        setAchivementsImages(Integer.parseInt(achivements_id), viewHolder.achievements_images, achivements_pos, viewHolder.achievements_titles, viewHolder);
    }

    @Override
    public int getItemCount() {
        return achivementsGridarraylist.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView achievements_titles;
        private ImageView achievements_images;

        public ViewHolder(View view) {
            super(view);

            achievements_titles = (TextView) view.findViewById(R.id.achievements_titles);
            achievements_images = (ImageView) view.findViewById(R.id.achievements_images);
        }
    }

    public void setAchivementsImages(int achivements_id, ImageView achievements_images, int pos, TextView achievements_titles, ViewHolder viewHolder) {
        switch (achivements_id) {
            case 101:
                checkAchivementFlag(1, achivements_id, achievements_images, R.drawable.oval_4, achievements_titles, activity.getResources().getString(R.string.three_days_streak), viewHolder);
                break;
            case 102:
                checkAchivementFlag(1, achivements_id, achievements_images, R.drawable.rectangle_4_18, achievements_titles, activity.getResources().getString(R.string.five_days_streak), viewHolder);
                break;
            case 103:
                checkAchivementFlag(1, achivements_id, achievements_images, R.drawable.polygon_15, achievements_titles, activity.getResources().getString(R.string.seven_days_streak), viewHolder);
                break;
            case 104:
                checkAchivementFlag(1, achivements_id, achievements_images, R.drawable.polygon_14, achievements_titles, activity.getResources().getString(R.string.ten_days_streak), viewHolder);
                break;
            case 105:
                checkAchivementFlag(1, achivements_id, achievements_images, R.drawable.oval_3, achievements_titles, activity.getString(R.string.fortnightstreak), viewHolder);
                break;
            case 106:
                checkAchivementFlag(1, achivements_id, achievements_images, R.drawable.rectangle_4_17, achievements_titles, activity.getString(R.string.monthStreak), viewHolder);
                break;
            case 107:
                checkAchivementFlag(1, achivements_id, achievements_images, R.drawable.polygon_13, achievements_titles, activity.getString(R.string.centuryStreak), viewHolder);
                break;
            case 108:
                checkAchivementFlag(1, achivements_id, achievements_images, R.drawable.polygon_12, achievements_titles, activity.getString(R.string.yearStreak), viewHolder);
                break;
            case 201:
                checkAchivementFlag(2, achivements_id, achievements_images, R.drawable.rectangle_4_16, achievements_titles, activity.getString(R.string.hundred_perecent_goal_hit), viewHolder);
                break;
            case 202:
                checkAchivementFlag(2, achivements_id, achievements_images, R.drawable.polygon_11, achievements_titles, activity.getString(R.string.one_hundred_fifty_goal_hit), viewHolder);
                break;
            case 203:
                checkAchivementFlag(2, achivements_id, achievements_images, R.drawable.polygon_10, achievements_titles, activity.getString(R.string.goalHit200), viewHolder);
                break;
            case 204:
                checkAchivementFlag(2, achivements_id, achievements_images, R.drawable.rectangle_4_15, achievements_titles, activity.getString(R.string.goalHit300), viewHolder);
                break;
            case 205:
                checkAchivementFlag(2, achivements_id, achievements_images, R.drawable.polygon_9, achievements_titles, activity.getString(R.string.goalHit400), viewHolder);
                break;
            case 206:
                checkAchivementFlag(2, achivements_id, achievements_images, R.drawable.polygon_8, achievements_titles, activity.getString(R.string.goalHit500), viewHolder);
                break;
            case 2011:
                checkAchivementFlag(2, achivements_id, achievements_images, R.drawable.rectangle_4_14, achievements_titles, activity.getString(R.string.daysGoalStreak3), viewHolder);
                break;
            case 2021:
                checkAchivementFlag(2, achivements_id, achievements_images, R.drawable.polygon_7, achievements_titles, activity.getString(R.string.daysGoalStreak7), viewHolder);
                break;
            case 2031:
                checkAchivementFlag(2, achivements_id, achievements_images, R.drawable.polygon_6, achievements_titles, activity.getString(R.string.fortnightGoalStreak), viewHolder);
                break;
            case 2041:
                checkAchivementFlag(2, achivements_id, achievements_images, R.drawable.rectangle_4_13, achievements_titles, activity.getString(R.string.halfCenturyGoalStreak), viewHolder);
                break;
            case 2051:
                checkAchivementFlag(2, achivements_id, achievements_images, R.drawable.polygon_5, achievements_titles, activity.getString(R.string.centuryGoalStreak), viewHolder);
                break;
            case 2061:
                checkAchivementFlag(2, achivements_id, achievements_images, R.drawable.polygon_14, achievements_titles, activity.getString(R.string.yearGoalStreak), viewHolder);
                break;
            case 301:
                checkAchivementFlag(3, achivements_id, achievements_images, R.drawable.rectangle_4_12, achievements_titles, activity.getString(R.string.oneDay5k), viewHolder);
                break;
            case 302:
                checkAchivementFlag(3, achivements_id, achievements_images, R.drawable.rectangle_4_11, achievements_titles, activity.getString(R.string.oneDay6k), viewHolder);
                break;
            case 303:
                checkAchivementFlag(3, achivements_id, achievements_images, R.drawable.rectangle_4_10, achievements_titles, activity.getString(R.string.oneDay7k), viewHolder);
                break;
            case 304:
                checkAchivementFlag(3, achivements_id, achievements_images, R.drawable.rectangle_4_9, achievements_titles, activity.getString(R.string.oneDay10k), viewHolder);
                break;
            case 305:
                checkAchivementFlag(3, achivements_id, achievements_images, R.drawable.rectangle_4_8, achievements_titles, activity.getString(R.string.oneDay15k), viewHolder);
                break;
            case 306:
                checkAchivementFlag(3, achivements_id, achievements_images, R.drawable.rectangle_4_7, achievements_titles, activity.getString(R.string.oneDay20k), viewHolder);
                break;
            case 307:
                checkAchivementFlag(3, achivements_id, achievements_images, R.drawable.rectangle_4_6, achievements_titles, activity.getString(R.string.oneDay30k), viewHolder);
                break;
            case 308:
                checkAchivementFlag(3, achivements_id, achievements_images, R.drawable.rectangle_4_5, achievements_titles, activity.getString(R.string.oneDay40k), viewHolder);
                break;
            case 309:
                checkAchivementFlag(3, achivements_id, achievements_images, R.drawable.rectangle_4_4, achievements_titles, activity.getString(R.string.oneDay50k), viewHolder);
                break;
            case 3010:
                checkAchivementFlag(3, achivements_id, achievements_images, R.drawable.rectangle_4_3, achievements_titles, activity.getString(R.string.oneDay75k), viewHolder);
                break;
            case 401:
                checkAchivementFlag(4, achivements_id, achievements_images, R.drawable.ac_31_s, achievements_titles, activity.getString(R.string.hidden_11), viewHolder);
                break;
            case 402:
                checkAchivementFlag(4, achivements_id, achievements_images, R.drawable.ac_32_s, achievements_titles, activity.getString(R.string.hidden_214), viewHolder);
                break;
            case 403:
                checkAchivementFlag(4, achivements_id, achievements_images, R.drawable.ac_33_s, achievements_titles, activity.getString(R.string.hidden_46), viewHolder);
                break;
            case 404:
                checkAchivementFlag(4, achivements_id, achievements_images, R.drawable.ac_34_s, achievements_titles, activity.getString(R.string.hidden_515), viewHolder);
                break;
            case 405:
                checkAchivementFlag(4, achivements_id, achievements_images, R.drawable.ac_35_s, achievements_titles, activity.getString(R.string.hidden_724), viewHolder);
                break;
            case 406:
                checkAchivementFlag(4, achivements_id, achievements_images, R.drawable.ac_36_s, achievements_titles, activity.getString(R.string.hidden_922), viewHolder);
                break;
            case 407:
                checkAchivementFlag(4, achivements_id, achievements_images, R.drawable.ac_37_s, achievements_titles, activity.getString(R.string.hidden_1223), viewHolder);
                break;
            case 408:
                checkAchivementFlag(4, achivements_id, achievements_images, R.drawable.ac_38_s, achievements_titles, activity.getString(R.string.hidden_1225), viewHolder);
                break;
            case 409:
                checkAchivementFlag(4, achivements_id, achievements_images, R.drawable.ac_39_s, achievements_titles, activity.getString(R.string.hidden_100h), viewHolder);
                break;
            case 4010:
                checkAchivementFlag(4, achivements_id, achievements_images, R.drawable.ac_40_s, achievements_titles, activity.getString(R.string.hidden_500h), viewHolder);
                break;
            case 4011:
                checkAchivementFlag(4, achivements_id, achievements_images, R.drawable.ac_41_s, achievements_titles, activity.getString(R.string.hidden_1000h), viewHolder);
                break;
            case 4012:
                checkAchivementFlag(4, achivements_id, achievements_images, R.drawable.ac_42_s, achievements_titles, activity.getString(R.string.hidden_100t), viewHolder);
                break;
            case 4013:
                checkAchivementFlag(4, achivements_id, achievements_images, R.drawable.ac_43_s, achievements_titles, activity.getString(R.string.hidden_500t), viewHolder);
                break;
            case 4014:
                checkAchivementFlag(4, achivements_id, achievements_images, R.drawable.ac_44_s, achievements_titles, activity.getString(R.string.hidden_1000t), viewHolder);
                break;
            case 4015:
                checkAchivementFlag(4, achivements_id, achievements_images, R.drawable.ac_45_s, achievements_titles, activity.getString(R.string.hidden_100b), viewHolder);
                break;
            case 4016:
                checkAchivementFlag(4, achivements_id, achievements_images, R.drawable.ac_46_s, achievements_titles, activity.getString(R.string.hidden_500b), viewHolder);
                break;
            case 4017:
                checkAchivementFlag(4, achivements_id, achievements_images, R.drawable.ac_47_s, achievements_titles, activity.getString(R.string.hidden_1000b), viewHolder);
                break;
            default:
                achievements_images.setImageResource(achivementsGridarraylist.get(pos).getAchivements_image());
        }
    }

    public void checkAchivementFlag(final int flag, int id, ImageView achivementsImage, final int drwable, TextView achievements_titles, final String varTitle, final ViewHolder viewHolder) {
        for (int i = 0; i < achivementsFlagarraylist.size(); i++) {
            int achivementId = achivementsFlagarraylist.get(i).getAchivements_id();
            if (achivementId == id) {
                Boolean acFlag = achivementsFlagarraylist.get(i).getAchivements_flag();
                if (acFlag == true) {

                    achivementsImage.setVisibility(View.VISIBLE);
                    achievements_titles.setVisibility(View.VISIBLE);

                    achivementsImage.setImageResource(drwable);
                    achievements_titles.setText(varTitle);

                    viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Intent i = new Intent(activity, AchievementsDetailsActivity.class);
                            i.putExtra("achievementsDetailsActivity", 0);
                            i.putExtra("achievementsImage", drwable);
                            i.putExtra("achievementsTitle", varTitle.toString());
                            i.putExtra("achievementsflag", flag);
                            activity.startActivity(i);
                        }
                    });
                }
            }
        }
    }
}