package com.ldsscriptures.pro.adapter;

import android.app.Activity;
import android.app.ProgressDialog;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.ldsscriptures.pro.BuildConfig;
import com.ldsscriptures.pro.R;
import com.ldsscriptures.pro.model.TimeLine.CommentTimeLineModel;
import com.ldsscriptures.pro.model.TimeLine.LikesTimeLineModel;
import com.ldsscriptures.pro.model.TimeLineShareSuccess;
import com.ldsscriptures.pro.retroutils.APIClient;
import com.ldsscriptures.pro.retroutils.APIInterface;
import com.ldsscriptures.pro.utils.SessionManager;
import com.ldsscriptures.pro.utils.TimeAgoClass;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class TimelineCommentAdapter extends RecyclerView.Adapter<TimelineCommentAdapter.ViewHolder> {

    private ArrayList<CommentTimeLineModel> timelineCommentItems;
    private Activity activity;
    private SessionManager sessionManager;
    private String var_timeline_share_id;
    private int colorflag;
    String paraUserId = "";
    String paraUserImage = "";
    String paraUserName = "";
    String paraSessionId = "";
    ProgressDialog progressDoalog;

    public TimelineCommentAdapter(Activity activity, ArrayList<CommentTimeLineModel> timelineCommentItems, String var_timeline_share_id, int colorflag, String paraUserId, String paraUserImage, String paraUserName, String paraSessionId) {
        this.timelineCommentItems = timelineCommentItems;
        this.activity = activity;
        this.sessionManager = new SessionManager(activity);
        this.var_timeline_share_id = var_timeline_share_id;
        this.colorflag = colorflag;
        this.paraUserId = paraUserId;
        this.paraUserImage = paraUserImage;
        this.paraUserName = paraUserName;
        this.paraSessionId = paraSessionId;

        progressDoalog = new ProgressDialog(activity);
        progressDoalog.setMessage("Loading...");
        progressDoalog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDoalog.setCancelable(false);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.activity_timeline_comment_item, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, final int i) {

        final String var_timeline_comment_Id = timelineCommentItems.get(i).getId();
        String var_timeline_comment_user_name = timelineCommentItems.get(i).getUsername();
        String var_timeline_comment_timeago = timelineCommentItems.get(i).getCreated();
        String var_timeline_user_comment = timelineCommentItems.get(i).getComment();
        String var_timeline_comment_image = timelineCommentItems.get(i).getImg_url();

        final boolean[] var_timeline_likeFlag = {false};

        final String finalParaUserId = paraUserId;
        final String finalparaUserImage = paraUserImage;
        final String finalParaparaUserName = paraUserName;

        ArrayList<LikesTimeLineModel> likesTimeLineArrayList = new ArrayList<>();
        likesTimeLineArrayList = timelineCommentItems.get(i).getLikes();

        if (var_timeline_comment_user_name != null) {
            viewHolder.timeline_comment_user_name.setText(var_timeline_comment_user_name.toString());
        }
        if (var_timeline_comment_timeago != null || var_timeline_comment_timeago.length() != 0 || !var_timeline_comment_timeago.equals("")) {
            viewHolder.timeline_comment_timeago.setText(TimeAgoClass.getTimeAgo(Long.parseLong(var_timeline_comment_timeago.toString())));
        }
        if (var_timeline_user_comment != null) {
            viewHolder.timeline_user_comment.setText(var_timeline_user_comment.toString());
        }
        if (var_timeline_comment_user_name != null) {
            viewHolder.timeline_comment_user_name.setText(var_timeline_comment_user_name.toString());
        }

        if (var_timeline_comment_image != null) {
            Picasso.with(activity).load(var_timeline_comment_image).into(viewHolder.comment_user_image);
        } else {
            viewHolder.comment_user_image.setBackgroundResource(R.drawable.profile_lg);
        }

        if (likesTimeLineArrayList != null) {
            viewHolder.txt_fav_timeline_comment.setText(String.valueOf(likesTimeLineArrayList.size()));

            for (int j = 0; j < likesTimeLineArrayList.size(); j++) {
                if (paraUserId.equals(likesTimeLineArrayList.get(j).getUid())) {
                    var_timeline_likeFlag[0] = true;
                } else {
                    var_timeline_likeFlag[0] = false;
                }
            }

        } else {
            viewHolder.txt_fav_timeline_comment.setText("0");
            var_timeline_likeFlag[0] = false;
        }

        if (var_timeline_likeFlag[0] == true) {
            viewHolder.img_fav_timeline_comment.setImageResource(R.drawable.main);
        } else {
            viewHolder.img_fav_timeline_comment.setImageResource(R.drawable.fav_dark);
        }

        viewHolder.img_fav_timeline_comment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (var_timeline_likeFlag[0]) {

                    for (int j = 0; j < timelineCommentItems.get(i).getLikes().size(); j++) {
                        if (finalParaUserId.equals(timelineCommentItems.get(i).getLikes().get(j).getUid())) {
                            timelineCommentItems.get(i).getLikes().remove(j);
                        }
                    }
                    callTimelinePostLikeApi(var_timeline_comment_Id, "unlike");

                } else {

                    if (timelineCommentItems.get(i).getLikes() != null) {
                        timelineCommentItems.get(i).getLikes().add(new LikesTimeLineModel(finalParaUserId));
                    } else {
                        ArrayList<LikesTimeLineModel> arrayList = new ArrayList<LikesTimeLineModel>();
                        arrayList.add(new LikesTimeLineModel(finalParaUserId));
                        timelineCommentItems.get(i).setLikes(arrayList);

                    }

                    callTimelinePostLikeApi(var_timeline_comment_Id, "like");
                }
            }
        });

        if (colorflag == 1) {
            viewHolder.timeline_user_comment.setTextColor(activity.getResources().getColor(R.color.white));
            viewHolder.txt_fav_timeline_comment.setTextColor(activity.getResources().getColor(R.color.white));
        }

    }

    @Override
    public int getItemCount() {
        return timelineCommentItems.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private CircleImageView comment_user_image;
        private TextView timeline_comment_user_name;
        private TextView timeline_comment_timeago;
        private TextView timeline_user_comment;
        private ImageView img_fav_timeline_comment;
        private TextView txt_fav_timeline_comment;

        public ViewHolder(View view) {
            super(view);

            comment_user_image = (CircleImageView) view.findViewById(R.id.comment_user_image);
            timeline_comment_user_name = (TextView) view.findViewById(R.id.timeline_comment_user_name);
            timeline_comment_timeago = (TextView) view.findViewById(R.id.timeline_comment_timeago);
            timeline_user_comment = (TextView) view.findViewById(R.id.timeline_user_comment);
            img_fav_timeline_comment = (ImageView) view.findViewById(R.id.img_fav_timeline_comment);
            txt_fav_timeline_comment = (TextView) view.findViewById(R.id.txt_fav_timeline_comment);
        }
    }

    private void callTimelinePostLikeApi(final String var_timeline_comment_Id, final String share_action) {

        progressDoalog.show();

        APIInterface apiService = APIClient.getClient().create(APIInterface.class);
        String url = APIClient.BASE_URL;


        url += "timelinePush.pl?user_id=" + paraUserId +
                "&session_id=" + paraSessionId +
                "&action=" + share_action +
                "&type=" + "comments_likes" +
                "&device=" + APIClient.DEVICE_KEY +
                "&app_version=" + BuildConfig.VERSION_NAME +
                "&share_id=" + var_timeline_share_id +
                "&comment_id=" + var_timeline_comment_Id +
                "&apiauth=" + APIClient.AUTH_KEY;


        Call<TimeLineShareSuccess> callObject = apiService.getTimeLineShareSuccess(url.replaceAll(" ", "%20"));
        callObject.enqueue(new Callback<TimeLineShareSuccess>() {

            @Override
            public void onResponse(Call<TimeLineShareSuccess> call, Response<TimeLineShareSuccess> response) {

                if (response.body() != null && !response.body().toString().trim().isEmpty()) {

                    if (response.body().getSuccess() == 1) {

                    }

                    notifyDataSetChanged();
                    progressDoalog.dismiss();
                }
            }

            @Override
            public void onFailure(Call<TimeLineShareSuccess> call, Throwable t) {
                progressDoalog.dismiss();
            }
        });
    }

}