package com.ldsscriptures.pro.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.ldsscriptures.pro.R;
import com.ldsscriptures.pro.activity.content.HymnsDetailsActivity;
import com.ldsscriptures.pro.model.HymnsTopicContentData;
import com.turingtechnologies.materialscrollbar.ICustomAdapter;
import com.turingtechnologies.materialscrollbar.INameableAdapter;

import java.util.ArrayList;

public class HymnsTopicContentListAdapter extends RecyclerView.Adapter<HymnsTopicContentListAdapter.MyViewHolder> implements ICustomAdapter, INameableAdapter, Filterable {

    private final Context context;
    private ArrayList<HymnsTopicContentData> hymnsTopicContentDataArrayList = new ArrayList<>();
    private ArrayList<HymnsTopicContentData> originalTopicContentDataArrayList = new ArrayList<>();

    private ValueFilter valueFilter;

    public HymnsTopicContentListAdapter(Context context, ArrayList<HymnsTopicContentData> hymnsTopicContentDataArrayList) {
        this.context = context;
        this.hymnsTopicContentDataArrayList = hymnsTopicContentDataArrayList;
        this.originalTopicContentDataArrayList = hymnsTopicContentDataArrayList;

    }


    @Override
    public Character getCharacterForElement(int element) {
        Character c = hymnsTopicContentDataArrayList.get(element).getName().charAt(0);
        if (Character.isDigit(c)) {
            c = '#';
        }
        return c;
    }

    @Override
    public String getCustomStringForElement(int element) {
        return String.valueOf(element + 1);
    }

    @Override
    public Filter getFilter() {

        if (valueFilter == null) {

            valueFilter = new ValueFilter();
        }

        return valueFilter;
    }

    private class ValueFilter extends Filter {


        //Invoked in a worker thread to filter the data according to the constraint.
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {

            FilterResults results = new FilterResults();

            if (constraint != null && constraint.length() > 0) {
                ArrayList<HymnsTopicContentData> filterList = new ArrayList<HymnsTopicContentData>();
                for (int i = 0; i < originalTopicContentDataArrayList.size(); i++) {

                    if (originalTopicContentDataArrayList.get(i).getName().toLowerCase().contains(constraint.toString().toLowerCase())) {
                        filterList.add(originalTopicContentDataArrayList.get(i));
                    }
                }
                results.count = filterList.size();

                results.values = filterList;

            } else {
                results.count = originalTopicContentDataArrayList.size();
                results.values = originalTopicContentDataArrayList;
            }
            return results;
        }


        //Invoked in the UI thread to publish the filtering results in the user interface.
        @SuppressWarnings("unchecked")
        @Override
        protected void publishResults(CharSequence constraint,
                                      FilterResults results) {
            hymnsTopicContentDataArrayList = (ArrayList<HymnsTopicContentData>) results.values;
            notifyDataSetChanged();
        }
    }


    public class MyViewHolder extends RecyclerView.ViewHolder {
        private TextView txtSubTopicsName;
        private LinearLayout linHymns;
        private TextView txtSubTopicsID;

        public MyViewHolder(View view) {
            super(view);
            txtSubTopicsName = (TextView) view.findViewById(R.id.txtSubTopicsName);
            linHymns = (LinearLayout) view.findViewById(R.id.linHymns);
            txtSubTopicsID = (TextView) view.findViewById(R.id.txtSubTopicsID);
        }
    }

    @Override
    public HymnsTopicContentListAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, final int position) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_hymns_topic_sub, parent, false);
        return new HymnsTopicContentListAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(HymnsTopicContentListAdapter.MyViewHolder holder, final int position) {

        if (hymnsTopicContentDataArrayList.get(position).getName() != null)
            holder.txtSubTopicsName.setText(hymnsTopicContentDataArrayList.get(position).getName());

        holder.linHymns.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, HymnsDetailsActivity.class);
                intent.putExtra(context.getString(R.string.title), hymnsTopicContentDataArrayList.get(position).getName());
                intent.putExtra(context.getString(R.string.page_url), hymnsTopicContentDataArrayList.get(position).getPage_url());
                intent.putExtra(context.getString(R.string.pdf_url), hymnsTopicContentDataArrayList.get(position).getPdf_url());
                context.startActivity(intent);
            }
        });

        holder.txtSubTopicsID.setText(String.valueOf(hymnsTopicContentDataArrayList.get(position).getId()));
    }

    @Override
    public int getItemCount() {
        return hymnsTopicContentDataArrayList.size();
    }
}