package com.ldsscriptures.pro.adapter;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.os.AsyncTask;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.ldsscriptures.pro.R;
import com.ldsscriptures.pro.activity.ReadingActivity;
import com.ldsscriptures.pro.activity.verses.CitationSubListActivity;
import com.ldsscriptures.pro.database.DataBaseHelper;
import com.ldsscriptures.pro.database.SubDataBaseHelper;
import com.ldsscriptures.pro.model.CitationSubList;
import com.ldsscriptures.pro.model.Item;
import com.ldsscriptures.pro.model.TimeLine.VerseDataTimeLineModel;
import com.ldsscriptures.pro.utils.DownloadSubDBZipFile;
import com.ldsscriptures.pro.utils.Global;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;


public class CitationSubListAdapter extends RecyclerView.Adapter<CitationSubListAdapter.MyViewHolder> {

    private List<CitationSubList> indexList;
    private Activity activity;
    VerseDataTimeLineModel verseDataTimeLineModel;
    DataBaseHelper databasehelper;
    private ArrayList<Item> dbCatelogArraylist = new ArrayList<>();

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView tvIndexItem;
        LinearLayout relMaincitation;


        public MyViewHolder(View view) {
            super(view);
            tvIndexItem = (TextView) view.findViewById(R.id.tvVerse);
            relMaincitation = (LinearLayout) view.findViewById(R.id.relMaincitation);

        }
    }

    public CitationSubListAdapter(Activity activity, ArrayList<CitationSubList> indexList) {
        this.indexList = indexList;
        this.activity = activity;
        databasehelper = new DataBaseHelper(activity);
        dbCatelogArraylist = Global.getListSharedPreferneces(activity);
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, final int position) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_citation_sublist, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {

        String sourcetitle = indexList.get(position).getSource_title();
        String sourcesubtitle = indexList.get(position).getSource_subtitle();
        String sourcesetText = "";

        if (sourcesubtitle != null && !sourcesubtitle.isEmpty() && !sourcesubtitle.equals("null")) {
            sourcesetText = sourcetitle + " : " + sourcesubtitle;
        } else {
            sourcesetText = sourcetitle;
        }

        holder.tvIndexItem.setText(sourcesetText);

        holder.relMaincitation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String getVerseText1 = indexList.get(position).getSource_uri().trim();

                int index = getVerseText1.lastIndexOf("/");
                String getVerseText_Split = getVerseText1.substring(0, index);

                verseDataTimeLineModel = new VerseDataTimeLineModel();
                verseDataTimeLineModel = getVerseText(getVerseText_Split, getVerseText1);
                final String verseLcId = verseDataTimeLineModel.getLcId();
                final String verseLsId = verseDataTimeLineModel.getLsId();
                final String indexForReading = verseDataTimeLineModel.getIndexForReading();
                redirectReadingActivity(verseLcId, verseLsId, indexForReading);

            }
        });
    }

    @Override
    public int getItemCount() {
        return indexList.size();
    }

    public void redirectReadingActivity(String verseLcId, String verseLsId, String indexForReading) {
        if (verseLcId != null || verseLsId != null || indexForReading != null) {

            Intent intent = new Intent(activity, ReadingActivity.class);
            intent.putExtra(activity.getString(R.string.indexForReading), Integer.parseInt(indexForReading));
            intent.putExtra(activity.getString(R.string.isFromIndex), true);
            intent.putExtra(activity.getString(R.string.is_history), false);
            intent.putExtra(activity.getString(R.string.lcID), verseLcId);
            intent.putExtra(activity.getString(R.string.lsId), verseLsId);

            activity.startActivity(intent);
            activity.overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
        }
    }

    public VerseDataTimeLineModel getVerseText(String fullVerseUri, final String fullgetVerseText) {

        Cursor cursor;
        String ItemID = "";
        String LCID = "";

        try {
            cursor = databasehelper.getVerseTextAndVersePath_URI(fullVerseUri);
            if (cursor != null && cursor.moveToFirst()) {
                while (!cursor.isAfterLast()) {
                    ItemID = cursor.getString(cursor.getColumnIndex("ItemID"));
                    LCID = cursor.getString(cursor.getColumnIndex("LCID"));
                    if (dbCatelogArraylist != null) {
                        for (int i = 0; i < dbCatelogArraylist.size(); i++) {
                            if (dbCatelogArraylist.get(i).getId().equals(ItemID)) {
                                if (!isFolderExists(dbCatelogArraylist.get(i).getId())) {

                                    new DownloadFileFromURL().execute(dbCatelogArraylist.get(i).getUrl(), dbCatelogArraylist.get(i).getId(), fullgetVerseText, LCID, fullVerseUri);
                                } else {
                                    verseDataTimeLineModel = getVerseIndexDataFromDB(ItemID, fullgetVerseText);
                                    verseDataTimeLineModel.setLsId(ItemID);
                                    verseDataTimeLineModel.setLcId(LCID);
                                }
                            }
                        }
                    }

                    cursor.moveToNext();
                }
            }
            if (cursor != null) {
                cursor.close();
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return verseDataTimeLineModel;
    }

    public VerseDataTimeLineModel getVerseIndexDataFromDB(String LsItemId, String fullgetVerseText) {
        SubDataBaseHelper subDataBaseHelper;
        subDataBaseHelper = new SubDataBaseHelper(activity, LsItemId);

        Cursor cursor;
        cursor = subDataBaseHelper.getVerseTextFromURI(fullgetVerseText);

        if (cursor != null && cursor.moveToFirst()) {
            while (!cursor.isAfterLast()) {

                String LsHrmlTextItem = cursor.getString(cursor.getColumnIndex(DataBaseHelper.KEY_LS_RESULT_TITLE_HTML));
                byte[] blob = cursor.getBlob(cursor.getColumnIndex(SubDataBaseHelper.KEY_SUB_ITEM_HTML_C1CONTENT_HTML));
                String idForReading = cursor.getString(cursor.getColumnIndex(SubDataBaseHelper.KEY_ID));

                verseDataTimeLineModel.setVersePath(LsHrmlTextItem);
                verseDataTimeLineModel.setIndexForReading(idForReading);

                cursor.moveToNext();
            }
        }
        subDataBaseHelper.close();
        if (cursor != null) {
            cursor.close();
        }
        return verseDataTimeLineModel;
    }

    private boolean isFolderExists(String id) {
        File destDir = new File(Global.AppFolderPath + id);

        if (!destDir.exists()) {
            return false;
        } else {
            if (destDir.isDirectory()) {
                String[] files = destDir.list();
                if (files == null)
                    return false;

                if (files.length == 0) {
                    //directory is empty
                    return false;
                } else
                    return true;
            }
        }
        return false;
    }

    class DownloadFileFromURL extends AsyncTask<String, String, String> {

        String ItemID = "";
        String LCID = "";
        String fullgetVerseText = "";
        String fullVerseUri = "";

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            CitationSubListActivity.getInstance().showprogress();
        }

        @Override
        protected String doInBackground(String... f_url) {
            int count;
            try {

                ItemID = f_url[1];
                fullgetVerseText = f_url[2];
                LCID = f_url[3];
                fullVerseUri = f_url[4];

                new DownloadSubDBZipFile(activity, f_url[0], ItemID);

                URL url = new URL(f_url[0]);
                URLConnection conection = url.openConnection();
                conection.connect();
                // getting file length
                int lenghtOfFile = conection.getContentLength();

                // input stream to read file - with 8k buffer
                InputStream input = new BufferedInputStream(url.openStream(), 8192);

                // Output stream to write file
                OutputStream output = new FileOutputStream("/sdcard/downloadedfile.jpg");

                byte data[] = new byte[1024];

                long total = 0;

                while ((count = input.read(data)) != -1) {
                    total += count;
                    // publishing the progress....
                    // After this onProgressUpdate will be called
                    publishProgress("" + (int) ((total * 100) / lenghtOfFile));

                    // writing data to file
                    output.write(data, 0, count);
                }

                // flushing output
                output.flush();

                // closing streams
                output.close();
                input.close();

            } catch (Exception e) {
                Log.e("Error: ", e.getMessage());
            }

            return null;
        }

        /**
         * Updating progress bar
         */
        protected void onProgressUpdate(String... progress) {
            // setting progress percentage
            //pDialog.setProgress(Integer.parseInt(progress[0]));
        }

        /**
         * After completing background task
         * Dismiss the progress dialog
         **/
        @Override
        protected void onPostExecute(String file_url) {
            // dismiss the dialog after the file was downloaded
            CitationSubListActivity.getInstance().dismissprogress();

            verseDataTimeLineModel = getVerseText(fullVerseUri, fullgetVerseText);
            final String verseLcId = verseDataTimeLineModel.getLcId();
            final String verseLsId = verseDataTimeLineModel.getLsId();
            final String indexForReading = verseDataTimeLineModel.getIndexForReading();

            redirectReadingActivity(verseLcId, verseLsId, indexForReading);
        }

    }
}