package com.ldsscriptures.pro.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.ldsscriptures.pro.fragment.BookmarksFragment;
import com.ldsscriptures.pro.fragment.HistoryFragment;
import com.ldsscriptures.pro.fragment.ShareAnnotationCrossReferenceFragment;
import com.ldsscriptures.pro.fragment.ShareAnnotationNotesFragment;
import com.ldsscriptures.pro.fragment.ShareAnnotationTagsFragment;


public class ShareAnnotationsPagerAdapter extends FragmentStatePagerAdapter {
    int tabCount;

    public ShareAnnotationsPagerAdapter(FragmentManager fm, int tabCount) {
        super(fm);
        this.tabCount = tabCount;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return new ShareAnnotationNotesFragment();
            case 1:
                return new ShareAnnotationTagsFragment();
            case 2:
                return new ShareAnnotationCrossReferenceFragment();
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return tabCount;
    }
}