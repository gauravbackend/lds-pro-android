package com.ldsscriptures.pro.adapter;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.ldsscriptures.pro.R;
import com.ldsscriptures.pro.activity.activitymenu.AchievementsDetailsActivity;
import com.ldsscriptures.pro.activity.studytools.NewPlanSelectPaceActivity;
import com.ldsscriptures.pro.model.LsData;
import com.ldsscriptures.pro.utils.SessionManager;
import com.squareup.picasso.Picasso;

import java.text.DecimalFormat;
import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

public class NewPlanSubListAdapter extends RecyclerView.Adapter<NewPlanSubListAdapter.ViewHolder> {

    private ArrayList<LsData> newplansubarraylist;
    private Activity activity;
    private SessionManager sessionManager;
    private String var_LcID;

    public NewPlanSubListAdapter(Activity activity, ArrayList<LsData> newplansubarraylist, String LcID) {
        this.newplansubarraylist = newplansubarraylist;
        this.activity = activity;
        this.var_LcID = LcID;
        sessionManager = new SessionManager(activity);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.activity_new_plan_item, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int i) {

        String var_newPlansubBookname = newplansubarraylist.get(i).getTitle();
        final int var_LsID = newplansubarraylist.get(i).getItem_id();
        final String var_SubTitle = newplansubarraylist.get(i).getTitle();

        viewHolder.newPlansubBookname.setText(var_newPlansubBookname.toString());

        viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(activity, NewPlanSelectPaceActivity.class);
                i.putExtra("planActivity", 0);
                i.putExtra("plan_Main_LcID", Integer.parseInt(var_LcID));
                i.putExtra("plan_Sub_LsID", var_LsID);
                i.putExtra("plan_Sub_Title", var_SubTitle);
                activity.startActivity(i);
            }
        });
    }

    @Override
    public int getItemCount() {
        return newplansubarraylist.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private TextView newPlansubBookname;

        public ViewHolder(View view) {
            super(view);
            newPlansubBookname = (TextView) view.findViewById(R.id.newPlansubBookname);
        }
    }


}