package com.ldsscriptures.pro.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.ldsscriptures.pro.R;
import com.ldsscriptures.pro.model.NotesData;
import com.ldsscriptures.pro.utils.Global;

import java.util.ArrayList;
import java.util.Date;


public class ShareAnnotationNotesAdapter extends RecyclerView.Adapter<ShareAnnotationNotesAdapter.ViewHolder> {
    private Context context;
    private ArrayList<NotesData> notesDataArrayList = new ArrayList<>();

    public ShareAnnotationNotesAdapter(Context context, ArrayList<NotesData> notesDataArrayList) {
        this.notesDataArrayList = notesDataArrayList;
        this.context = context;
    }

    @Override
    public ShareAnnotationNotesAdapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_notes_share_annotation, viewGroup, false);
        return new ShareAnnotationNotesAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ShareAnnotationNotesAdapter.ViewHolder viewHolder, int position) {
        if (notesDataArrayList.get(position).getTitle() != null) {
            viewHolder.txtTitle.setText(notesDataArrayList.get(position).getTitle());
            viewHolder.txtTitle.setVisibility(View.VISIBLE);
        } else {
            viewHolder.txtTitle.setVisibility(View.INVISIBLE);
        }

        if (notesDataArrayList.get(position).getUser().getUsername() != null) {
            viewHolder.txtUsername.setVisibility(View.VISIBLE);
            viewHolder.txtUsername.setText(notesDataArrayList.get(position).getUser().getUsername());
        } else {
            viewHolder.txtUsername.setVisibility(View.INVISIBLE);
        }

        if (notesDataArrayList.get(position).getCreated() != null) {
            viewHolder.txtTime.setVisibility(View.VISIBLE);
            viewHolder.txtTime.setText(convertDate(notesDataArrayList.get(position).getCreated()));
        } else {
            viewHolder.txtTime.setVisibility(View.INVISIBLE);
        }


        if (Integer.parseInt(notesDataArrayList.get(position).getLike_cnt()) != 0) {
            viewHolder.txtFavCount.setText(notesDataArrayList.get(position).getLike_cnt());
            viewHolder.txtFavCount.setVisibility(View.VISIBLE);

        } else {
            viewHolder.txtFavCount.setVisibility(View.INVISIBLE);
        }

        if (notesDataArrayList.get(position).getNote() != null) {
            viewHolder.txtDetails.setText(notesDataArrayList.get(position).getNote());
            viewHolder.txtDetails.setVisibility(View.VISIBLE);
        } else {
            viewHolder.txtDetails.setVisibility(View.INVISIBLE);
        }

        if (notesDataArrayList.get(position).isLiked_by_user())
            viewHolder.ivFav.setImageResource(R.drawable.main);
        else
            viewHolder.ivFav.setImageResource(R.drawable.light);

        if (position == getItemCount() - 1) {
            viewHolder.view1.setVisibility(View.INVISIBLE);
        }
    }

    public String convertDate(String dateInMilliseconds) {
        Date date = new Date(Long.parseLong(dateInMilliseconds));
        return Global.getTimeAgo(date, context);
    }

    @Override
    public int getItemCount() {
        return notesDataArrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView txtTitle;
        TextView txtUsername;
        TextView txtTime;
        TextView txtDetails;
        TextView txtFavCount;
        ImageView ivFav;
        View view1;

        public ViewHolder(View view) {
            super(view);
            txtTitle = (TextView) view.findViewById(R.id.txtTitle);
            txtUsername = (TextView) view.findViewById(R.id.txtUsername);
            txtTime = (TextView) view.findViewById(R.id.txtTime);
            txtDetails = (TextView) view.findViewById(R.id.txtDetails);
            txtFavCount = (TextView) view.findViewById(R.id.txtFavCount);
            ivFav = (ImageView) view.findViewById(R.id.ivFav);
            view1 = view.findViewById(R.id.view);
        }
    }

}