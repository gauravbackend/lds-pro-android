package com.ldsscriptures.pro.adapter;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.ldsscriptures.pro.R;
import com.ldsscriptures.pro.activity.ReadingActivity;
import com.ldsscriptures.pro.model.LsIndexPreviewData;

import java.util.ArrayList;


public class ReadingActivitySecondLevelMenuAdapter extends RecyclerView.Adapter<ReadingActivitySecondLevelMenuAdapter.MyViewHolder> {

    private Activity activity;
    private ArrayList<LsIndexPreviewData> lsDataArrayList = new ArrayList<>();

    private final String lcId;
    private final String lsId;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView textView;
        public LinearLayout linDropDown;

        public MyViewHolder(View view) {
            super(view);
            textView = (TextView) view.findViewById(R.id.textView);
            linDropDown = (LinearLayout) view.findViewById(R.id.linDropDown);
        }
    }

    public ReadingActivitySecondLevelMenuAdapter(Activity activity, ArrayList<LsIndexPreviewData> lsDataArrayList, String lcId, String lsId) {
        this.lsDataArrayList = lsDataArrayList;
        this.activity = activity;
        this.lcId = lcId;
        this.lsId = lsId;
    }

    public void addAll(ArrayList<LsIndexPreviewData> lsDataArrayList) {
        this.lsDataArrayList = lsDataArrayList;
        notifyDataSetChanged();
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, final int position) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_dropdown_menu, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {

        holder.textView.setTextColor(activity.getResources().getColor(R.color.navSubIndexText));
        holder.textView.setText(lsDataArrayList.get(position).getHtml_title());

        holder.linDropDown.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(activity, ReadingActivity.class);

                intent.putExtra(activity.getString(R.string.indexForReading), lsDataArrayList.get(position).getId());
                intent.putExtra(activity.getString(R.string.isFromIndex), false);
                intent.putExtra(activity.getString(R.string.is_history), false);
                intent.putExtra(activity.getString(R.string.lcID), lcId);
                intent.putExtra(activity.getString(R.string.lsId), lsId);

                activity.startActivity(intent);
                activity.overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
                activity.finish();
            }
        });
    }

    @Override
    public int getItemCount() {
        return lsDataArrayList.size();
    }
}