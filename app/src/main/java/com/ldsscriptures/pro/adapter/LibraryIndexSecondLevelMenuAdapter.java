package com.ldsscriptures.pro.adapter;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.ldsscriptures.pro.R;
import com.ldsscriptures.pro.activity.LibraryIndexActivity;
import com.ldsscriptures.pro.activity.verses.CitationSubListActivity;
import com.ldsscriptures.pro.model.Item;
import com.ldsscriptures.pro.model.LsData;
import com.ldsscriptures.pro.utils.DownloadSubDBZipFile;
import com.ldsscriptures.pro.utils.Global;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;


public class LibraryIndexSecondLevelMenuAdapter extends RecyclerView.Adapter<LibraryIndexSecondLevelMenuAdapter.MyViewHolder> {

    private final String LcID;
    private Activity activity;
    private ArrayList<LsData> lsDataArrayList = new ArrayList<>();
    private ArrayList<Item> dbCatelogArraylist = new ArrayList<>();

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView textView;
        public LinearLayout linDropDown;

        public MyViewHolder(View view) {
            super(view);
            textView = (TextView) view.findViewById(R.id.textView);
            linDropDown = (LinearLayout) view.findViewById(R.id.linDropDown);

        }
    }


    public LibraryIndexSecondLevelMenuAdapter(Activity activity, ArrayList<LsData> lsDataArrayList, String LcID) {
        this.lsDataArrayList = lsDataArrayList;
        this.activity = activity;
        this.LcID = LcID;
        dbCatelogArraylist = Global.getListSharedPreferneces(activity);
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, final int position) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_dropdown_menu, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {

        holder.textView.setTextColor(activity.getResources().getColor(R.color.drop_down_second_level_text));
        holder.textView.setText(lsDataArrayList.get(position).getTitle());

        holder.textView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (dbCatelogArraylist != null) {

                    for (int i = 0; i < dbCatelogArraylist.size(); i++) {

                        String item_id = String.valueOf(lsDataArrayList.get(position).getItem_id());

                        if (dbCatelogArraylist.get(i).getId().equals(item_id)) {

                            if (!isFolderExists(dbCatelogArraylist.get(i).getId())) {

                                String pos = String.valueOf(lsDataArrayList.get(position).getPosition());
                                String title = lsDataArrayList.get(position).getTitle();

                                new DownloadFileFromURL().execute(dbCatelogArraylist.get(i).getUrl(), dbCatelogArraylist.get(i).getId(), LcID, pos, title);

                            } else {
                                Intent intent = new Intent(activity, LibraryIndexActivity.class);
                                intent.putExtra(activity.getString(R.string.lcID), LcID);
                                intent.putExtra(activity.getString(R.string.lcSubItemPos), lsDataArrayList.get(position).getPosition());
                                intent.putExtra(activity.getString(R.string.lsId), String.valueOf(lsDataArrayList.get(position).getItem_id()));
                                intent.putExtra(activity.getString(R.string.lsTitle), lsDataArrayList.get(position).getTitle());
                                activity.startActivity(intent);
                                activity.overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
                                activity.finish();
                            }
                        }
                    }
                }

            }
        });
    }

    @Override
    public int getItemCount() {
        return lsDataArrayList.size();
    }

    private boolean isFolderExists(String id) {
        File destDir = new File(Global.AppFolderPath + id);

        if (!destDir.exists()) {
            return false;
        } else {
            if (destDir.isDirectory()) {
                String[] files = destDir.list();
                if (files == null)
                    return false;

                if (files.length == 0) {
                    //directory is empty
                    return false;
                } else
                    return true;
            }
        }
        return false;
    }

    class DownloadFileFromURL extends AsyncTask<String, String, String> {

        String item_id = "";
        String lcID = "";
        String pos = "";
        String title = "";

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            LibraryIndexActivity.getInstance().showprogress();
        }

        @Override
        protected String doInBackground(String... f_url) {
            int count;
            try {

                item_id = f_url[1];
                lcID = f_url[2];
                pos = f_url[3];
                title = f_url[4];

                new DownloadSubDBZipFile(activity, f_url[0], item_id);

                URL url = new URL(f_url[0]);
                URLConnection conection = url.openConnection();
                conection.connect();
                // getting file length
                int lenghtOfFile = conection.getContentLength();

                // input stream to read file - with 8k buffer
                InputStream input = new BufferedInputStream(url.openStream(), 8192);

                // Output stream to write file
                OutputStream output = new FileOutputStream("/sdcard/downloadedfile.jpg");

                byte data[] = new byte[1024];

                long total = 0;

                while ((count = input.read(data)) != -1) {
                    total += count;
                    // publishing the progress....
                    // After this onProgressUpdate will be called
                    publishProgress("" + (int) ((total * 100) / lenghtOfFile));

                    // writing data to file
                    output.write(data, 0, count);
                }

                // flushing output
                output.flush();

                // closing streams
                output.close();
                input.close();

            } catch (Exception e) {
                Log.e("Error: ", e.getMessage());
            }

            return null;
        }

        /**
         * Updating progress bar
         */
        protected void onProgressUpdate(String... progress) {
            // setting progress percentage
            //pDialog.setProgress(Integer.parseInt(progress[0]));
        }

        /**
         * After completing background task
         * Dismiss the progress dialog
         **/
        @Override
        protected void onPostExecute(String file_url) {
            // dismiss the dialog after the file was downloaded
            LibraryIndexActivity.getInstance().dismissprogress();

            Intent intent = new Intent(activity, LibraryIndexActivity.class);
            intent.putExtra(activity.getString(R.string.lcID), LcID);
            intent.putExtra(activity.getString(R.string.lcSubItemPos), pos);
            intent.putExtra(activity.getString(R.string.lsId), item_id);
            intent.putExtra(activity.getString(R.string.lsTitle), title);
            activity.startActivity(intent);
            activity.overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
            activity.finish();
        }

    }
}