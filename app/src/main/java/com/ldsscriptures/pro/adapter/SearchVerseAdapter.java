package com.ldsscriptures.pro.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.text.SpannableStringBuilder;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.ldsscriptures.pro.R;
import com.ldsscriptures.pro.activity.ReadingActivity;
import com.ldsscriptures.pro.database.DataBaseHelper;
import com.ldsscriptures.pro.database.SubDataBaseHelper;
import com.ldsscriptures.pro.model.IndexWrapper;
import com.ldsscriptures.pro.model.LsData;
import com.ldsscriptures.pro.model.SerachData;
import com.ldsscriptures.pro.model.TimeLine.VerseDataTimeLineModel;
import com.ldsscriptures.pro.utils.Global;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.util.ArrayList;
import java.util.List;

public class SearchVerseAdapter extends RecyclerView.Adapter<SearchVerseAdapter.MyViewHolder> {

    private Context activity;
    private ArrayList<SerachData> lcDataArrayList, filterList;
    VerseDataTimeLineModel verseDataTimeLineModel;
    DataBaseHelper databasehelper;

    public SearchVerseAdapter(Context activity, ArrayList<SerachData> lcDataArrayList) {
        this.lcDataArrayList = lcDataArrayList;
        this.activity = activity;
        this.filterList = new ArrayList<SerachData>();
        this.filterList.addAll(this.lcDataArrayList);
        databasehelper = new DataBaseHelper(activity);
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, final int position) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.serach_verse_layout, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {

        String myStringNew = "";

        if (filterList.get(position).getStringSentense() != null && filterList.get(position).getStringId() != null)
            myStringNew = filterList.get(position).getStringSentense().replace(filterList.get(position).getStringId(), "");

        String tvstringlsTitle = filterList.get(position).getStringlsTitle();
        String tvstringSubTitle = filterList.get(position).getStringSubTitle();
        final String tvstringSubId = filterList.get(position).getStringId();
        final String tvstringUrl = filterList.get(position).getSetData_URL();
        String versePath = tvstringSubTitle + ":" + tvstringSubId;

        SpannableStringBuilder text = new SpannableStringBuilder(Global.noTrailingwhiteLines(Html.fromHtml(myStringNew)));

        holder.tvSerachVerse.setText(text);
        holder.tvstringSubTitle.setText(versePath);

        holder.subserach_Layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                verseDataTimeLineModel = new VerseDataTimeLineModel();

                verseDataTimeLineModel = getVerseText(tvstringUrl, tvstringSubId);
                final String verseLcId = verseDataTimeLineModel.getLcId();
                final String verseLsId = verseDataTimeLineModel.getLsId();
                final String indexForReading = verseDataTimeLineModel.getIndexForReading();
                final String verseAid = verseDataTimeLineModel.getVerseAid();

                redirectReadingActivity(verseLcId, verseLsId, indexForReading, verseAid, tvstringSubId);
            }
        });

    }

    @Override
    public int getItemCount() {
        return (null != filterList ? filterList.size() : 0);
    }

    public String convertStringToArrayList(ArrayList<LsData> arrayList) {
        Gson gson = new Gson();
        String json = gson.toJson(arrayList);
        return json;
    }

    // Do Search...
    public void filter(final String text, final String lstitle) {

        // Searching could be complex..so we will dispatch it to a different thread...
        new Thread(new Runnable() {
            @Override
            public void run() {

                // Clear the filter list
                filterList.clear();

                // If there is no search value, then add all original list items to filter list
                if (TextUtils.isEmpty(text)) {

                    filterList.addAll(lcDataArrayList);

                } else {
                    // Iterate in the original List and add it to filter list...
                    for (SerachData item : lcDataArrayList) {
                        if (item.getStringSentense().toLowerCase().contains(text.toLowerCase()) &&
                                item.getStringlsTitle().toLowerCase().contains(lstitle.toLowerCase())) {
                            filterList.add(item);
                        }
                    }
                }

                // Set on UI Thread
                ((Activity) activity).runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        // Notify the List that the DataSet has changed...
                        notifyDataSetChanged();
                    }
                });

            }
        }).start();

    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView tvSerachVerse, tvstringSubTitle;
        LinearLayout subserach_Layout;

        public MyViewHolder(View view) {
            super(view);
            tvSerachVerse = (TextView) view.findViewById(R.id.tvSerachVerse);
            tvstringSubTitle = (TextView) view.findViewById(R.id.tvstringSubTitle);
            subserach_Layout = (LinearLayout) view.findViewById(R.id.subserach_Layout);
        }
    }

    public void redirectReadingActivity(String verseLcId, String verseLsId, String indexForReading, String verseAid, String verseId) {
        if (verseLcId != null && verseLsId != null && indexForReading != null && !verseAid.isEmpty() && !verseId.isEmpty() && !indexForReading.isEmpty()) {

            Intent intent = new Intent(activity, ReadingActivity.class);
            intent.putExtra(activity.getString(R.string.indexForReading), Integer.parseInt(indexForReading));
            intent.putExtra(activity.getString(R.string.isFromIndex), true);
            intent.putExtra(activity.getString(R.string.is_history), false);
            intent.putExtra(activity.getString(R.string.lcID), verseLcId);
            intent.putExtra(activity.getString(R.string.lsId), verseLsId);
            intent.putExtra(activity.getString(R.string.verseAId), Integer.parseInt(verseAid));
            intent.putExtra(activity.getString(R.string.verseId), Integer.parseInt(verseId));
            activity.startActivity(intent);
            ((Activity) activity).overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
        }
    }

    public VerseDataTimeLineModel getVerseText(String fullVerseUri, String var_timeline_Verse_Id) {

        Cursor cursor;
        String LcId = "";
        try {

            String[] URI_TXT = fullVerseUri.split("\\/");
            String URI_TXT_First = URI_TXT[1];
            String URI_TXT_First11 = URI_TXT[2];
            int index = fullVerseUri.lastIndexOf('/');
            String URI_TXT_Second = fullVerseUri.substring(0, index);
            String URI_TXT_Third = "/" + URI_TXT_First + "/" + URI_TXT_First11;

            cursor = databasehelper.getVerseTextAndVersePath(removeDashes(URI_TXT_First));

            if (cursor != null && cursor.moveToFirst()) {
                while (!cursor.isAfterLast()) {
                    LcId = cursor.getString(cursor.getColumnIndex(DataBaseHelper.KEY_ID));
                    cursor.moveToNext();
                }
            }
            if (cursor != null) {
                cursor.close();
            }

            String URI_TXT_First1 = URI_TXT_First.substring(0, 1).toUpperCase() + URI_TXT_First.substring(1);
            verseDataTimeLineModel = getLsDataFromDb(LcId, URI_TXT_Second, fullVerseUri, var_timeline_Verse_Id, URI_TXT_First1, URI_TXT_Third);
            verseDataTimeLineModel.setLcId(LcId);

        } catch (Exception e) {
            e.printStackTrace();
        }
        return verseDataTimeLineModel;
    }

    public VerseDataTimeLineModel getLsDataFromDb(String LcId, String verseUri, String fullVerseUri, String var_timeline_Verse_Id, String URI_TXT_First, String URI_TXT_Third) {

        Cursor cursor;
        String LsItemId = "";
        String LsItemTitle = "";
        cursor = databasehelper.getVerseLSPath_new(Integer.parseInt(LcId), verseUri, URI_TXT_Third);

        if (cursor != null && cursor.moveToFirst()) {
            while (!cursor.isAfterLast()) {
                LsItemId = cursor.getString(cursor.getColumnIndex(DataBaseHelper.KEY_LS_RESULT_ITEM_ID));
                LsItemTitle = cursor.getString(cursor.getColumnIndex(DataBaseHelper.KEY_LS_RESULT_TITLE_HTML));

                URI_TXT_First = URI_TXT_First + " / " + LsItemTitle;
                verseDataTimeLineModel = getVerseIndexDataFromDB(LsItemId, fullVerseUri, var_timeline_Verse_Id, URI_TXT_First);
                verseDataTimeLineModel.setLsId(LsItemId);

                cursor.moveToNext();
            }
        }
        return verseDataTimeLineModel;
    }

    public VerseDataTimeLineModel getVerseIndexDataFromDB(String LsItemId, String fullVerseUri, String var_timeline_Verse_Id, String URI_TXT_First) {
        SubDataBaseHelper subDataBaseHelper;
        subDataBaseHelper = new SubDataBaseHelper(activity, LsItemId);

        Cursor cursor;
        cursor = subDataBaseHelper.getVerseTextFromURI(fullVerseUri);

        if (cursor != null && cursor.moveToFirst()) {
            while (!cursor.isAfterLast()) {

                String LsHrmlTextItem = cursor.getString(cursor.getColumnIndex(DataBaseHelper.KEY_LS_RESULT_TITLE_HTML));
                byte[] blob = cursor.getBlob(cursor.getColumnIndex(SubDataBaseHelper.KEY_SUB_ITEM_HTML_C1CONTENT_HTML));
                String idForReading = cursor.getString(cursor.getColumnIndex(SubDataBaseHelper.KEY_ID));
                String postionForReading = cursor.getString(cursor.getColumnIndex(SubDataBaseHelper.KEY_FT_POSITION));
                String newHtmlVerseArray = new String(blob);

                verseDataTimeLineModel = getCrossRefSubItemHtmlData(newHtmlVerseArray, var_timeline_Verse_Id);
                verseDataTimeLineModel.setVersePath(LsHrmlTextItem);
                verseDataTimeLineModel.setVerseFullPath(URI_TXT_First + " / " + LsHrmlTextItem);
                verseDataTimeLineModel.setIndexForReading(idForReading);

                cursor.moveToNext();
            }
        }
        subDataBaseHelper.close();
        if (cursor != null) {
            cursor.close();
        }
        return verseDataTimeLineModel;
    }

    public VerseDataTimeLineModel getCrossRefSubItemHtmlData(String newHtmlVerseArray, String verse_Id) {
        String verseText = "";
        ArrayList<String> selectedArrayList = new ArrayList<>();
        Document doc = Jsoup.parse(newHtmlVerseArray);
        Elements tvVersemain = doc.select("div[class^=body-block]");
        Elements tvVerse = tvVersemain.select("p[class^=verse]");

        if (!tvVerse.isEmpty() && tvVerse != null && !tvVerse.equals("null")) {

            Element element2 = tvVerse.get(Integer.parseInt(verse_Id) - 1);
            Elements tvVerseSpan = element2.select("span[class^=study-note-ref]");
            for (int j = 0; j < tvVerseSpan.size(); j++) {
                String e2 = tvVerseSpan.get(j).text();
                selectedArrayList.add(e2);
            }

            List<IndexWrapper> indexWrapperList = Global.findIndexesForKeyword(element2.outerHtml(), "data-aid");
            String[] dataAid = new String[0];
            for (int k = 0; k < indexWrapperList.size(); k++) {
                String substring = element2.outerHtml().substring(indexWrapperList.get(k).getEnd() + 2);
                dataAid = substring.split("\"");
            }
            verseText = Html.fromHtml(element2.outerHtml()).toString();
            for (int i = 0; i < selectedArrayList.size(); i++) {
                verseText = verseText.replace(selectedArrayList.get(i), selectedArrayList.get(i).substring(1));
            }
            verseText = verseText.replace(verseText, verseText.substring(verse_Id.length()).trim());
            verseDataTimeLineModel.setVereseText(verseText);
            verseDataTimeLineModel.setVerseAid(dataAid[0]);
        } else {

            Elements tvVerse1 = tvVersemain.select("p");

            for (int i = 0; i < tvVerse1.size(); i++) {

                String pid = tvVerse1.get(i).select("p").attr("id");
                verseText = tvVerse1.get(i).outerHtml().toString();
                String verseAid = tvVerse1.get(i).attr("data-aid");
                String verseID = "";

                if (pid.startsWith("p")) {
                    verseID = pid.substring(1);
                }

                if (verseID.equals(verse_Id)) {

                    verseDataTimeLineModel.setVereseText(String.valueOf(Html.fromHtml(verseText)));
                    verseDataTimeLineModel.setVerseAid(verseAid);
                }

            }


        }

        return verseDataTimeLineModel;
    }

    public static String removeDashes(String c) {
        String dashless = c.replaceAll("\\-", " ");

        return dashless;
    }

}
