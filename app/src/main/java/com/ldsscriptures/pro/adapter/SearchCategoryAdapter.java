package com.ldsscriptures.pro.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.text.SpannableStringBuilder;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.ldsscriptures.pro.R;
import com.ldsscriptures.pro.activity.SearchActivity;
import com.ldsscriptures.pro.activity.SearchSubActivity;
import com.ldsscriptures.pro.model.SerachData;
import com.ldsscriptures.pro.utils.DataHolder;
import com.ldsscriptures.pro.utils.Global;

import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class SearchCategoryAdapter extends RecyclerView.Adapter<SearchCategoryAdapter.MyViewHolder> {

    private Context activity;
    private ArrayList<SerachData> categoryArrayList, filterList, lcDataArrayListString;
    private String serachQuery;

    public SearchCategoryAdapter(Context activity, ArrayList<SerachData> categoryArrayList, String serachQuery) {
        this.categoryArrayList = categoryArrayList;
        this.activity = activity;
        this.lcDataArrayListString = new ArrayList<SerachData>();
        this.filterList = new ArrayList<SerachData>();
        this.filterList.addAll(this.categoryArrayList);
        this.serachQuery = serachQuery;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, final int position) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.serach_category_layout, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {

        final String tvstringlsTitle = filterList.get(position).getStringlsTitle();

        if (DataHolder.hasData1()) {
            lcDataArrayListString = DataHolder.getData1();
        }

        final int tvstringlscount = LcDataCount(lcDataArrayListString, tvstringlsTitle, serachQuery);

        holder.tvcategoryTitle.setText(tvstringlsTitle);
        holder.tvstringlscount.setText(String.valueOf(tvstringlscount));

        holder.main_serach_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i1 = new Intent(activity, SearchSubActivity.class);
                i1.putExtra("lstitle", tvstringlsTitle);
                i1.putExtra("serachQuery", serachQuery);

                DataHolder.setData1(lcDataArrayListString);
                activity.startActivity(i1);
            }
        });
    }

    @Override
    public int getItemCount() {
        return (null != filterList ? filterList.size() : 0);
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView tvcategoryTitle, tvstringlscount;
        LinearLayout main_serach_layout;

        public MyViewHolder(View view) {
            super(view);
            tvcategoryTitle = (TextView) view.findViewById(R.id.tvcategoryTitle);
            tvstringlscount = (TextView) view.findViewById(R.id.tvstringlscount);
            main_serach_layout = (LinearLayout) view.findViewById(R.id.main_serach_layout);
        }
    }

    // Do Search...
    public void filter(final String text) {
        // Searching could be complex..so we will dispatch it to a different thread...
        new Thread(new Runnable() {
            @Override
            public void run() {
                // Clear the filter list
                filterList.clear();
                // If there is no search value, then add all original list items to filter list
                if (TextUtils.isEmpty(text)) {
                    filterList.addAll(categoryArrayList);
                } else {

                    if (DataHolder.hasData1()) {
                        lcDataArrayListString = DataHolder.getData1();
                    }
                    // Iterate in the original List and add it to filter list...
                    for (SerachData item : lcDataArrayListString) {

                        if (item.getStringSentense().toLowerCase().contains(text.toLowerCase())) {
                            filterList.add(item);
                            removeDuplicates(filterList);
                        }
                    }
                }

                // Set on UI Thread
                ((Activity) activity).runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (filterList != null && filterList.size() != 0) {
                            SearchActivity.getInstance().HideShow();
                        } else {
                            SearchActivity.getInstance().ShowHide();
                        }
                        notifyDataSetChanged();
                    }
                });
            }
        }).start();
    }


    public int LcDataCount(ArrayList<SerachData> arrayList, final String lstitle, final String serachQuery) {

        int count = 0;
        for (int i = 0; i < arrayList.size(); i++) {
            if (arrayList.get(i).getStringlsTitle().contains(lstitle)) {

                String myStringNew = "";

                if (arrayList.get(i).getStringSentense() != null && arrayList.get(i).getStringId() != null)
                    myStringNew = arrayList.get(i).getStringSentense().replace(arrayList.get(i).getStringId(), "");

                SpannableStringBuilder text = new SpannableStringBuilder(Global.noTrailingwhiteLines(Html.fromHtml(myStringNew)));

                Pattern word = Pattern.compile(serachQuery.toLowerCase());
                Matcher match = word.matcher(text.toString().toLowerCase());
                while (match.find()) {
                    count++;
                }

            }
        }
        return count;
    }

    private void removeDuplicates(ArrayList<SerachData> categoryArrayList) {
        int count = categoryArrayList.size();

        for (int i = 0; i < count; i++) {
            for (int j = i + 1; j < count; j++) {
                if (categoryArrayList.get(i).getStringlsTitle().equals(categoryArrayList.get(j).getStringlsTitle())) {
                    categoryArrayList.remove(j--);
                    count--;
                }
            }
        }
    }

}
