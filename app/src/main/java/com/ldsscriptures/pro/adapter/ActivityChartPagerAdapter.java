package com.ldsscriptures.pro.adapter;

import android.content.Context;
import android.graphics.Color;
import android.support.v4.view.PagerAdapter;
import android.text.format.DateFormat;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.utils.ColorTemplate;
import com.ldsscriptures.pro.R;
import com.ldsscriptures.pro.model.ActivityData;
import com.ldsscriptures.pro.utils.Global;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class ActivityChartPagerAdapter extends PagerAdapter {

    Context mContext;
    LayoutInflater mLayoutInflater;
    int tabCount = 0;
    int adapterFlag;
    private ArrayList<String> xAxis;
    private ArrayList<ActivityData> ChartViseActivityArrayList = new ArrayList<>();
    private ArrayList<String> TextviewArrayList = new ArrayList<>();

    public ActivityChartPagerAdapter(Context context, int tabCount, int adapterFlag, ArrayList<ActivityData> ChartViseActivityArrayList) {
        mContext = context;
        mLayoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.tabCount = tabCount;
        this.adapterFlag = adapterFlag;
        this.ChartViseActivityArrayList = ChartViseActivityArrayList;

    }

    @Override
    public int getCount() {
        return tabCount;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == ((LinearLayout) object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        View itemView = mLayoutInflater.inflate(R.layout.activitychartlayout, container, false);

        BarChart barChart = (BarChart) itemView.findViewById(R.id.barChart);
        LineChart lineChart = (LineChart) itemView.findViewById(R.id.lineChart);
        LinearLayout layout_linechart = (LinearLayout) itemView.findViewById(R.id.layout_linechart);
        LinearLayout linechart_xieslayout = (LinearLayout) itemView.findViewById(R.id.linechart_xieslayout);
        LinearLayout layout_barchart = (LinearLayout) itemView.findViewById(R.id.layout_barchart);
        LinearLayout barchart_xieslayout = (LinearLayout) itemView.findViewById(R.id.barchart_xieslayout);

        BarData barData = null;
        LineData lineData = null;

        switch (adapterFlag) {
            case 0:
                barData = new BarData(getXAxisValuesDay(), getDataSetDay());
                barchart_xieslayout.setVisibility(View.VISIBLE);
                XAxis xAxis = barChart.getXAxis();
                xAxis.setDrawAxisLine(false);
                xAxis.setEnabled(true);
                createTextview(barchart_xieslayout, 0);
                //barChart.getXAxis().setSpaceBetweenLabels(0);
                break;
            case 1:
                barData = new BarData(getXAxisValuesWeek(), getDataSetWeek());
                barChart.getXAxis().setSpaceBetweenLabels(0);
                break;
            case 2:
                barData = new BarData(getXAxisValuesMonth(), getDataSetMonth());
                barChart.getXAxis().setSpaceBetweenLabels(0);
                break;
            case 3:
                barData = new BarData(getXAxisValuesYear(), getDataSetYear());
                barChart.getXAxis().setSpaceBetweenLabels(0);
                break;
            default:
                break;
        }

        barChart.setBackgroundColor(Color.TRANSPARENT); //set whatever color you prefer
        barChart.setDrawGridBackground(false);// this is a must
        barChart.setDrawBorders(false);
        barChart.setData(barData);
        barChart.setDescription("");
        //barChart.animateXY(2000, 2000);
        barChart.invalidate();
        barChart.getAxisRight().setEnabled(false);
        barChart.getAxisLeft().setEnabled(false);
        barChart.getXAxis().setDrawGridLines(false);
        barChart.getXAxis().setDrawGridLines(false);
        barChart.getLegend().setEnabled(false);
        barChart.setTouchEnabled(false);
        XAxis xAxis = barChart.getXAxis();
        xAxis.setDrawAxisLine(false);
        xAxis.setTextColor(Color.GRAY);
        xAxis.setTextSize(13);
        barChart.getXAxis().setAdjustXLabels(true);

        lineChart.setBackgroundColor(Color.TRANSPARENT); //set whatever color you prefer
        lineChart.setDrawGridBackground(false);// this is a must
        lineChart.setDrawBorders(false);
        lineChart.setData(lineData);
        lineChart.setDescription("");
        //lineChart.animateXY(2000, 2000);
        lineChart.invalidate();
        lineChart.getAxisRight().setEnabled(false);
        lineChart.getAxisLeft().setEnabled(false);
        lineChart.getXAxis().setDrawGridLines(false);
        lineChart.getXAxis().setDrawGridLines(false);
        lineChart.getLegend().setEnabled(false);
        lineChart.setTouchEnabled(false);
        XAxis xAxis1 = lineChart.getXAxis();
        xAxis1.setDrawAxisLine(false);
        xAxis1.setTextColor(Color.GRAY);
        xAxis1.setTextSize(13);
        xAxis1.setEnabled(false);

        container.addView(itemView);

        return itemView;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((LinearLayout) object);
    }

    private ArrayList<String> getXAxisValuesYear() {

        xAxis = new ArrayList<>();
        xAxis.add("J");
        xAxis.add("F");
        xAxis.add("M");
        xAxis.add("A");
        xAxis.add("M");
        xAxis.add("J");
        xAxis.add("J");
        xAxis.add("A");
        xAxis.add("S");
        xAxis.add("O");
        xAxis.add("N");
        xAxis.add("D");

        return xAxis;
    }

    private ArrayList<BarDataSet> getDataSetYear() {

        ArrayList<BarDataSet> dataSets = null;
        ArrayList<BarEntry> valueSet1 = new ArrayList<>();
        BarEntry v1e1;

        for (int i = 0; i < ChartViseActivityArrayList.size(); i++) {

            for (int j = 0; j < xAxis.size(); j++) {

                int barmonth = ChartViseActivityArrayList.get(i).getActivity_dayOfMonth();
                int barMonthNumber = j;

                if (barmonth == barMonthNumber) {

                    Global.printLog("YEAR", "barmonth" + barmonth);
                    Global.printLog("YEAR", "barMonthNumber" + barMonthNumber);

                    float barGoalpoint = ChartViseActivityArrayList.get(i).getActivity_goal_point();
                    v1e1 = new BarEntry(barGoalpoint, barMonthNumber - 1);
                    valueSet1.add(v1e1);
                }
            }
        }


        BarDataSet barDataSet1 = new BarDataSet(valueSet1, "");
        barDataSet1.setColors(ColorTemplate.VORDIPLOM_COLORS);
        barDataSet1.setDrawValues(false);
        dataSets = new ArrayList<>();
        dataSets.add(barDataSet1);

        return dataSets;
    }

    private ArrayList<String> getXAxisValuesMonth() {

        xAxis = new ArrayList<>();
        for (int i = 1; i <= 32; i++) {
            xAxis.add(String.valueOf(i));
        }
        return xAxis;
    }

    private ArrayList<BarDataSet> getDataSetMonth() {

        ArrayList<BarDataSet> dataSets = null;
        ArrayList<BarEntry> valueSet1 = new ArrayList<>();
        BarEntry v1e1;

        for (int i = 0; i < ChartViseActivityArrayList.size(); i++) {

            for (int j = 0; j < xAxis.size(); j++) {

                try {
                    String activityWeekDate = ChartViseActivityArrayList.get(i).getActivity_date();
                    SimpleDateFormat gsdf = new SimpleDateFormat("yyyy-MM-dd");
                    Date dt1 = gsdf.parse(activityWeekDate);
                    int monthnumber = Integer.parseInt((String) DateFormat.format("dd", dt1));
                    int barMonthNumber = j;

                    if (monthnumber == barMonthNumber) {
                        float barGoalpoint = ChartViseActivityArrayList.get(i).getActivity_goal_point();
                        v1e1 = new BarEntry(barGoalpoint, barMonthNumber - 1);
                        valueSet1.add(v1e1);
                    }
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }
        }

        BarDataSet barDataSet1 = new BarDataSet(valueSet1, "");
        barDataSet1.setColors(ColorTemplate.VORDIPLOM_COLORS);
        barDataSet1.setDrawValues(false);
        dataSets = new ArrayList<>();
        dataSets.add(barDataSet1);

        return dataSets;
    }

    private ArrayList<String> getXAxisValuesWeek() {

        xAxis = new ArrayList<>();
        xAxis.add("MON");
        xAxis.add("TUE");
        xAxis.add("WED");
        xAxis.add("THU");
        xAxis.add("FRI");
        xAxis.add("SAT");
        xAxis.add("SUN");

        return xAxis;

    }

    private ArrayList<BarDataSet> getDataSetWeek() {

        ArrayList<BarDataSet> dataSets = null;
        ArrayList<BarEntry> valueSet1 = new ArrayList<>();
        BarEntry v1e1;

        for (int i = 0; i < ChartViseActivityArrayList.size(); i++) {

            for (int j = 0; j < xAxis.size(); j++) {

                try {
                    String activityWeekDate = ChartViseActivityArrayList.get(i).getActivity_date();
                    SimpleDateFormat gsdf = new SimpleDateFormat("yyyy-MM-dd");
                    SimpleDateFormat sdf = new SimpleDateFormat("EEE");
                    Date dt1 = gsdf.parse(activityWeekDate);
                    String monthnumber = sdf.format(dt1).toUpperCase();
                    String barMonthNumber = xAxis.get(j);
                    SimpleDateFormat sdf1 = new SimpleDateFormat("dd");
                    Date d = gsdf.parse(activityWeekDate);

                    if (monthnumber.equals(barMonthNumber)) {

                        float barGoalpoint = ChartViseActivityArrayList.get(i).getActivity_goal_point();
                        v1e1 = new BarEntry(barGoalpoint, j);
                        valueSet1.add(v1e1);
                    }
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }
        }

        BarDataSet barDataSet1 = new BarDataSet(valueSet1, "");
        barDataSet1.setColors(ColorTemplate.VORDIPLOM_COLORS);
        barDataSet1.setDrawValues(false);
        dataSets = new ArrayList<>();
        dataSets.add(barDataSet1);

        return dataSets;
    }

    private ArrayList<String> getXAxisValuesDay() {

        xAxis = new ArrayList<>();
        for (int i = 1; i < 24; i++) {
            xAxis.add(String.valueOf(i));
        }
        return xAxis;
    }

    private ArrayList<BarDataSet> getDataSetDay() {

        ArrayList<BarDataSet> dataSets = null;
        ArrayList<BarEntry> valueSet1 = new ArrayList<>();
        BarEntry v1e1;
        valueSet1.clear();

        for (int i = 0; i < ChartViseActivityArrayList.size(); i++) {

            for (int j = 0; j < xAxis.size(); j++) {

                int activityHour = ChartViseActivityArrayList.get(i).getActivity_hour();
                int barDayHourNumber = j;
                if (activityHour == barDayHourNumber) {
                    float barGoalpoint = ChartViseActivityArrayList.get(i).getActivity_goal_point();
                    v1e1 = new BarEntry(barGoalpoint, barDayHourNumber - 1);
                    valueSet1.add(v1e1);
                }
            }
        }

        BarDataSet lineDataSet = new BarDataSet(valueSet1, "");
        lineDataSet.setColors(ColorTemplate.VORDIPLOM_COLORS);
        lineDataSet.setDrawValues(false);
        dataSets = new ArrayList<>();
        dataSets.add(lineDataSet);

        return dataSets;
    }

    private void createTextview(LinearLayout ll, int layoutFlag) {

        if (layoutFlag == 0) {
            TextviewArrayList.clear();
            TextviewArrayList.add("12 AM");
            TextviewArrayList.add("12 PM");
            TextviewArrayList.add("12 AM");

            LinearLayout.LayoutParams param = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.MATCH_PARENT, 1.0f);
            TextView[] textViews;
            textViews = new TextView[TextviewArrayList.size()];
            for (int i = 0; i < TextviewArrayList.size(); i++) {
                textViews[i] = new TextView(mContext);
                textViews[i].setText(TextviewArrayList.get(i));
                textViews[i].setTextColor(Color.GRAY);
                textViews[i].setBackgroundResource(0);
                textViews[i].setTextSize(13);
                textViews[i].setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT));
                textViews[i].setLayoutParams(param);
                textViews[i].setPadding(0, 0, 0, 0);
                textViews[i].setGravity(Gravity.CENTER_HORIZONTAL);
                ll.addView(textViews[i]);
            }
        } else {


            for (int i = 0; i < ChartViseActivityArrayList.size(); i++) {

                final TextView[] myTextViews = new TextView[xAxis.size()];
                LinearLayout.LayoutParams param = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.MATCH_PARENT, 1.0f);

                for (int j = 0; j < xAxis.size(); j++) {

                    try {
                        String activityWeekDate = ChartViseActivityArrayList.get(i).getActivity_date();

                        SimpleDateFormat gsdf = new SimpleDateFormat("yyyy-MM-dd");
                        SimpleDateFormat sdf = new SimpleDateFormat("EEE");
                        Date dt1 = gsdf.parse(activityWeekDate);
                        String monthnumber = sdf.format(dt1).toUpperCase();
                        String barMonthNumber = xAxis.get(j);
                        SimpleDateFormat sdf1 = new SimpleDateFormat("dd");
                        Date d = gsdf.parse(activityWeekDate);

                        if (monthnumber.equals(barMonthNumber)) {
                            myTextViews[j].setText(String.valueOf(sdf1.format(d)));
                        }
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }

    @Override
    public int getItemPosition(Object object) {
        return POSITION_NONE;
    }
}

