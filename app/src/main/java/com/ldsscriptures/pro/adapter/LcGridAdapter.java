package com.ldsscriptures.pro.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.ldsscriptures.pro.R;
import com.ldsscriptures.pro.model.LcData;
import com.ldsscriptures.pro.utils.SessionManager;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class LcGridAdapter extends BaseAdapter {
    private Context context;
    private ArrayList<LcData> lcDataArrayList = new ArrayList<>();
    private ViewHolder holder;
    private SessionManager sessionManager;

    public LcGridAdapter(Context context, ArrayList<LcData> lcDataArrayList) {
        this.context = context;
        this.lcDataArrayList = lcDataArrayList;
        sessionManager = new SessionManager(context);
    }

    @Override
    public int getCount() {
        return lcDataArrayList.size();
    }

    @Override
    public Object getItem(int position) {
        return lcDataArrayList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        if (convertView == null) {
            convertView = LayoutInflater.from(context).inflate(R.layout.item_grid, null);
            holder = new ViewHolder();
            holder.ivLcImage = (ImageView) convertView.findViewById(R.id.ivLcImage);
            holder.tvLcTitle = (TextView) convertView.findViewById(R.id.tvLcTitle);
            holder.panel_content = (LinearLayout) convertView.findViewById(R.id.panel_content);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        holder.tvLcTitle.setText(lcDataArrayList.get(position).getTitle());
        String mUrl = getImageUrl(lcDataArrayList.get(position).getCover_renditions());

        if (!mUrl.isEmpty()) {
            Glide.with(context).load(mUrl)
                    .crossFade()
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .into(holder.ivLcImage);
        }
        return convertView;
    }

    private String getImageUrl(String cover_renditions) {

        String mImageUrl = "";

        // split on ':' and on '::'
        String[] Urls = cover_renditions.split("\n");

        Map<String, String> map = new HashMap<>();
        int maxWidth = 0;

        for (String pair : Urls)                        //iterate over the pairs
        {
            String[] entry = pair.split(",");                   //split the pairs to get key and value
            map.put(entry[0].trim(), entry[1].trim());          //add them to the hashmap and trim whitespaces

            String mSize = entry[0];
            int mWidth = Integer.parseInt(mSize.substring(0, mSize.indexOf("x")));

            if (maxWidth < mWidth) {
                maxWidth = mWidth;
                mImageUrl = entry[1].trim();
            }
        }
        return mImageUrl;
    }

    private static class ViewHolder {
        ImageView ivLcImage;
        TextView tvLcTitle;
        LinearLayout panel_content;
    }
}