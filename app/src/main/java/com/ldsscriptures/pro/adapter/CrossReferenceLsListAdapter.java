package com.ldsscriptures.pro.adapter;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.ldsscriptures.pro.R;
import com.ldsscriptures.pro.activity.studytools.AddLessonScripture;
import com.ldsscriptures.pro.activity.verses.VerseCrossReferenceActivity;
import com.ldsscriptures.pro.model.LsData;

import java.util.ArrayList;


public class CrossReferenceLsListAdapter extends RecyclerView.Adapter<CrossReferenceLsListAdapter.MyViewHolder> {

    private ArrayList<LsData> lsDataArrayList = new ArrayList<>();
    private Activity activity;
    String s;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView tvName;
        public LinearLayout relMain;

        public MyViewHolder(View view) {
            super(view);
            tvName = (TextView) view.findViewById(R.id.tvName);
            relMain = (LinearLayout) view.findViewById(R.id.relMain);
        }
    }

    public CrossReferenceLsListAdapter(Activity activity, ArrayList<LsData> lsDataArrayList, String s) {
        this.lsDataArrayList = lsDataArrayList;
        this.activity = activity;
        this.s = s;
    }

    @Override
    public CrossReferenceLsListAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, final int position) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_cross_reference_ls_list, parent, false);
        return new CrossReferenceLsListAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(CrossReferenceLsListAdapter.MyViewHolder holder, final int position) {

        holder.tvName.setText(lsDataArrayList.get(position).getTitle());

        holder.relMain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(s.equals("1"))
                {
                    if (AddLessonScripture.getInstance() != null) {
                        AddLessonScripture.getInstance().setLibraryIndex(String.valueOf(lsDataArrayList.get(position).getItem_id()), lsDataArrayList.get(position).getTitle());
                    }
                }
                else
                {
                    if (VerseCrossReferenceActivity.getInstance() != null) {
                        VerseCrossReferenceActivity.getInstance().setLibraryIndex(String.valueOf(lsDataArrayList.get(position).getItem_id()), lsDataArrayList.get(position).getTitle());
                    }
                }

            }
        });
    }

    @Override
    public int getItemCount() {
        return lsDataArrayList.size();
    }
}