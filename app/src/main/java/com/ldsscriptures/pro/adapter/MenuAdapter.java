package com.ldsscriptures.pro.adapter;


import android.app.Activity;
import android.database.DataSetObserver;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.ldsscriptures.pro.R;
import com.ldsscriptures.pro.model.MenuData;

import java.util.ArrayList;
import java.util.List;

public class MenuAdapter implements ExpandableListAdapter {
    private Activity activity;
    private List<MenuData> menuArray = new ArrayList<MenuData>();

    public MenuAdapter(Activity activity, ArrayList<MenuData> menuArray) {
        this.activity = activity;
        this.menuArray = menuArray;
    }

    @Override
    public void registerDataSetObserver(DataSetObserver observer) {

    }

    @Override
    public void unregisterDataSetObserver(DataSetObserver observer) {

    }

    @Override
    public int getGroupCount() {
        return menuArray.size();
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return 1;
    }

    @Override
    public Object getGroup(int groupPosition) {
        return menuArray.get(groupPosition);
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        return menuArray.get(groupPosition);
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public boolean hasStableIds() {
        return true;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {

        ParentHolder parentHolder = null;
        MenuData menuData = (MenuData) getGroup(groupPosition);

        if (convertView == null) {
            LayoutInflater userInflater = (LayoutInflater) activity.getSystemService(activity.LAYOUT_INFLATER_SERVICE);
            convertView = userInflater.inflate(R.layout.item_menu, null);
            convertView.setHorizontalScrollBarEnabled(true);

            parentHolder = new ParentHolder();
            convertView.setTag(parentHolder);

        } else {
            parentHolder = (ParentHolder) convertView.getTag();
        }

        parentHolder.text_header = (TextView) convertView.findViewById(R.id.text_header);
        parentHolder.text_header.setText(menuData.menuName);
        final float scale = activity.getResources().getDisplayMetrics().density;

        int pixels;
        if (isExpanded) {
            parentHolder.text_header.setTextSize(13);
            convertView.setBackgroundResource(R.color.half_black);
            pixels = (int) (30 * scale + 0.5f);
        } else {
            parentHolder.text_header.setTextSize(17);
            pixels = (int) (68 * scale + 0.5f);
            convertView.setBackgroundResource(android.R.color.transparent);
        }
        parentHolder.text_header.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, pixels));

        return convertView;
    }

    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {

        ChildHolder childHolder = null;
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) activity.getSystemService(activity.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.item_group_child, parent, false);
            childHolder = new ChildHolder();
            convertView.setTag(childHolder);
        } else {
            childHolder = (ChildHolder) convertView.getTag();
        }

        childHolder.rcSubMenu = (RecyclerView) convertView.findViewById(R.id.rcSubMenu);
        childHolder.rcSubMenu.setLayoutManager(new LinearLayoutManager(activity, LinearLayoutManager.HORIZONTAL, false));
        childHolder.rcSubMenu.setAdapter(new SubMenuAdapter(activity, menuArray.get(groupPosition).subMenus, R.layout.item_child));

        return convertView;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return false;
    }

    @Override
    public boolean areAllItemsEnabled() {
        return false;
    }

    @Override
    public boolean isEmpty() {
        return false;
    }

    @Override
    public void onGroupExpanded(int groupPosition) {
    }

    @Override
    public void onGroupCollapsed(int groupPosition) {

    }

    @Override
    public long getCombinedChildId(long groupId, long childId) {
        return 0;
    }

    @Override
    public long getCombinedGroupId(long groupId) {
        return 0;
    }

    private static class ChildHolder {
        static RecyclerView rcSubMenu;
    }

    private static class ParentHolder {
        TextView text_header;
        ImageView indicator;
    }
}
