package com.ldsscriptures.pro.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.ldsscriptures.pro.R;
import com.ldsscriptures.pro.activity.MainActivity;
import com.ldsscriptures.pro.activity.activitymenu.AchievementsActivity;
import com.ldsscriptures.pro.activity.activitymenu.ActivityScreen;
import com.ldsscriptures.pro.activity.content.HymnsActivity;
import com.ldsscriptures.pro.activity.menuscreens.MenuNotesActivity;
import com.ldsscriptures.pro.activity.menuscreens.MenuTagActivity;
import com.ldsscriptures.pro.activity.socialmenu.FollowingActivity;
import com.ldsscriptures.pro.activity.socialmenu.LeaderBoardActivity;
import com.ldsscriptures.pro.activity.socialmenu.TimelineActivity;
import com.ldsscriptures.pro.activity.studytools.JournalsActivity;
import com.ldsscriptures.pro.activity.studytools.LessonsActivity;
import com.ldsscriptures.pro.activity.studytools.PlanActivity;
import com.ldsscriptures.pro.activity.studytools.StudyDailyActivity;
import com.ldsscriptures.pro.activity.studytools.StudyRandomActivity;
import com.ldsscriptures.pro.model.HistorySubData;
import com.ldsscriptures.pro.model.SubMenu;
import com.ldsscriptures.pro.utils.Global;
import com.ldsscriptures.pro.utils.SessionManager;

import java.util.ArrayList;
import java.util.List;

import static com.ldsscriptures.pro.R.id;
import static com.ldsscriptures.pro.R.string;

public class SubMenuAdapter extends RecyclerView.Adapter<SubMenuAdapter.ViewHolder> {

    private Activity activity;
    private int layout_item_child;
    private List<SubMenu> subMenus = new ArrayList<SubMenu>();
    private SessionManager sessionManager;

    public SubMenuAdapter(Activity activity, List<SubMenu> subMenus, int layout_item_child) {
        this.activity = activity;
        this.subMenus = subMenus;
        this.layout_item_child = layout_item_child;
        sessionManager = new SessionManager(this.activity);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View cardView = inflater.inflate(layout_item_child, null, false);
        ViewHolder viewHolder = new ViewHolder(cardView);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        holder.ivSubItem.setImageResource(subMenus.get(position).submenuIcon);
        holder.tvSubItem.setText(subMenus.get(position).submenuTitle);
        holder.llSubMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                handleClicks(subMenus.get(position).submenuTitle);
            }
        });
    }

    private void handleClicks(String submenuTitle) {
        Intent mainIntent = new Intent(activity, MainActivity.class);
        mainIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

        switch (submenuTitle) {
            case "Reading Now":


                if (sessionManager.getPreferencesHistoryArrayList(activity) != null)
                    if (sessionManager.getPreferencesHistoryArrayList(activity).size() != 0)
                        try {
                            HistorySubData historyData = sessionManager.getPreferencesHistoryArrayList(activity).get(0);

                            Intent intent = new Intent(activity, Class.forName(historyData.getTag()));
                            intent.putExtra(activity.getString(R.string.indexForReading), historyData.getReadingIndex());
                            intent.putExtra(activity.getString(R.string.isFromIndex), historyData.isFromIndex());

                            // set the false because no require to refresh list of history
                            intent.putExtra(activity.getString(R.string.is_history), false);
                            intent.putExtra(activity.getString(R.string.lcID), historyData.getLcId());
                            intent.putExtra(activity.getString(R.string.lsId), historyData.getLsId());
                            activity.startActivity(intent);
                            (activity).overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
                        } catch (ClassNotFoundException e) {
                            e.printStackTrace();
                        }
                    else
                        Global.showToast(activity, "No reading screen found.");
                else
                    Global.showToast(activity, "No reading screen found.");
                break;
            case "Library":
                mainIntent.putExtra(activity.getString(string.menuSelection), "Library");
                activity.startActivity(mainIntent);
                activity.overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
                break;

            case "History":
                mainIntent.putExtra(activity.getString(string.menuSelection), "History");
                activity.startActivity(mainIntent);
                activity.overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
                break;
            case "Bookmarks":
                mainIntent.putExtra(activity.getString(string.menuSelection), "Bookmarks");
                activity.startActivity(mainIntent);
                activity.overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
                break;

            case "Notes":
                Intent noteIntent = new Intent(activity, MenuNotesActivity.class);
                activity.startActivity(noteIntent);
                activity.overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
                activity.finish();
                break;

            case "Tags":
                Intent tagIntent = new Intent(activity, MenuTagActivity.class);
                activity.startActivity(tagIntent);
                activity.overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
                activity.finish();
                break;

            case "Daily":
                Intent dailyIntent = new Intent(activity, StudyDailyActivity.class);
                activity.startActivity(dailyIntent);
                activity.overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
                activity.finish();
                sessionManager.setUserDailyPoints(25);
                break;

            case "Random":
                Intent randomIntent = new Intent(activity, StudyRandomActivity.class);
                activity.startActivity(randomIntent);
                activity.overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
                activity.finish();
                sessionManager.setUserDailyPoints(25);
                break;
            case "Hymns":
                Intent hymnsIntent = new Intent(activity, HymnsActivity.class);
                activity.startActivity(hymnsIntent);
                activity.overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
                activity.finish();
                break;
            case "Lessons":
                Intent lessonsIntent = new Intent(activity, LessonsActivity.class);
                activity.startActivity(lessonsIntent);
                activity.overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
                activity.finish();
                break;
            case "Achievements":
                Intent achievementsIntent = new Intent(activity, AchievementsActivity.class);
                activity.startActivity(achievementsIntent);
                activity.overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
                activity.finish();
                break;
            case "Journal":
                Intent journalIntent = new Intent(activity, JournalsActivity.class);
                activity.startActivity(journalIntent);
                activity.overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
                activity.finish();
                break;
            case "Following":
                Intent followingIntent = new Intent(activity, FollowingActivity.class);
                activity.startActivity(followingIntent);
                activity.overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
                activity.finish();
                break;

            case "Leaderboard":
                Intent leaderboardIntent = new Intent(activity, LeaderBoardActivity.class);
                activity.startActivity(leaderboardIntent);
                activity.overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
                activity.finish();
                break;

            case "Timeline":
                Intent timelineIntent = new Intent(activity, TimelineActivity.class);
                activity.startActivity(timelineIntent);
                activity.overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
                activity.finish();
                break;

            case "Plans":
                Intent planIntent = new Intent(activity, PlanActivity.class);
                activity.startActivity(planIntent);
                activity.overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
                activity.finish();
                break;

            case "Activity":
                Intent activityIntent = new Intent(activity, ActivityScreen.class);
                activity.startActivity(activityIntent);
                activity.overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
                activity.finish();
                break;

        }
    }

    @Override
    public int getItemCount() {
        if (subMenus != null)
            return subMenus.size();
        else return 0;
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        private ImageView ivSubItem;
        private TextView tvSubItem;
        private LinearLayout llSubMenu;

        public ViewHolder(View itemView) {
            super(itemView);
            ivSubItem = (ImageView) itemView.findViewById(id.ivSubItem);
            tvSubItem = (TextView) itemView.findViewById(id.tvSubItem);
            llSubMenu = (LinearLayout) itemView.findViewById(id.llSubMenu);
        }
    }
}
