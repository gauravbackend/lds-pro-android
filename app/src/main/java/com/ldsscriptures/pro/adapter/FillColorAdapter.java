package com.ldsscriptures.pro.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.ldsscriptures.pro.R;
import com.ldsscriptures.pro.model.ColorData;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

public class FillColorAdapter extends RecyclerView.Adapter<FillColorAdapter.ViewHolder> {
    private Context context;
    private ArrayList<ColorData> colorDatas;
    private boolean isBgColor;
    public static String selectedColor;

    public FillColorAdapter(Context context, ArrayList<ColorData> colorDatas) {
        super();
        this.context = context;
        this.colorDatas = colorDatas;
        selectedColor = "#Fbf351";
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_color, viewGroup, false);
        ViewHolder viewHolder = new ViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {

        int colorId = android.graphics.Color.parseColor(colorDatas.get(position).color);
        holder.ivColor.setColorFilter(colorId);
        holder.ivColorSelected.setColorFilter(colorId);

        if (colorDatas.get(position).isSelected) {
            holder.ivColorSelected.setVisibility(View.VISIBLE);
        } else {
            holder.ivColorSelected.setVisibility(View.GONE);
        }
        holder.rlColors.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {

                    selectedColor = colorDatas.get(position).color;

                    for (int i = 0; i < colorDatas.size(); i++) {
                        if (i == position) {
                            colorDatas.get(i).isSelected = true;
                        } else {
                            colorDatas.get(i).isSelected = false;
                        }
                    }
                    notifyDataSetChanged();

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return colorDatas.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private CircleImageView ivColor, ivColorSelected;
        private RelativeLayout rlColors;

        public ViewHolder(final View itemView) {
            super(itemView);
            ivColor = (CircleImageView) itemView.findViewById(R.id.ivColor);
            rlColors = (RelativeLayout) itemView.findViewById(R.id.rlColors);
            ivColorSelected = (CircleImageView) itemView.findViewById(R.id.ivColorSelected);
        }
    }
}