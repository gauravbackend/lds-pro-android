package com.ldsscriptures.pro.adapter;

import android.app.Activity;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.text.SpannableStringBuilder;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.daimajia.swipe.SwipeLayout;
import com.daimajia.swipe.adapters.RecyclerSwipeAdapter;
import com.ldsscriptures.pro.BuildConfig;
import com.ldsscriptures.pro.R;
import com.ldsscriptures.pro.activity.BaseActivity;
import com.ldsscriptures.pro.activity.ReadingActivity;
import com.ldsscriptures.pro.activity.studytools.LessonsActivity;
import com.ldsscriptures.pro.database.CommonDatabaseHelper;
import com.ldsscriptures.pro.database.DataBaseHelper;
import com.ldsscriptures.pro.database.SubDataBaseHelper;
import com.ldsscriptures.pro.model.IndexWrapper;
import com.ldsscriptures.pro.model.LessonSubItemData;
import com.ldsscriptures.pro.model.ServerResponse;
import com.ldsscriptures.pro.model.TimeLine.VerseDataTimeLineModel;
import com.ldsscriptures.pro.retroutils.APIClient;
import com.ldsscriptures.pro.retroutils.APIInterface;
import com.ldsscriptures.pro.utils.EditItemTouchHelperCallback;
import com.ldsscriptures.pro.utils.Global;
import com.ldsscriptures.pro.utils.ItemTouchHelperViewHolder;
import com.ldsscriptures.pro.utils.PushToServerData;
import com.ldsscriptures.pro.utils.SessionManager;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LessonSubListAdapter extends RecyclerSwipeAdapter<LessonSubListAdapter.MyViewHolder> implements EditItemTouchHelperCallback.ItemTouchHelperAdapter {

    private final Context context;
    private ArrayList<LessonSubItemData> lessonSubItemDataArrayList = new ArrayList<>();
    VerseDataTimeLineModel verseDataTimeLineModel;
    DataBaseHelper databasehelper;
    CommonDatabaseHelper commonDatabaseHelper;
    SessionManager sessionManager;
    String verseLcId, verseLsId, indexForReading, verseAid, verseIdArrayStrig;
    private boolean setFalg = false;
    private ArrayList<LessonSubItemData> saveLessionSortOrderArraylist = new ArrayList<>();

    public LessonSubListAdapter(Context context, ArrayList<LessonSubItemData> lessonSubItemDataArrayList) {
        this.context = context;
        this.lessonSubItemDataArrayList = lessonSubItemDataArrayList;
        databasehelper = new DataBaseHelper(context);
        commonDatabaseHelper = new CommonDatabaseHelper(context);
        sessionManager = new SessionManager(context);
    }

    @Override
    public int getSwipeLayoutResourceId(int position) {
        return R.id.lessonsswipe;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder implements ItemTouchHelperViewHolder {
        ImageView ivIcon;
        TextView txtLessonTitle;
        TextView txtLessonDetails;
        LinearLayout linLesson;
        SwipeLayout swipeLayout;
        TextView tvDelete;
        ImageView imagright_arrow;

        public MyViewHolder(View view) {
            super(view);
            ivIcon = (ImageView) view.findViewById(R.id.ivMenu);
            txtLessonTitle = (TextView) view.findViewById(R.id.txtLessonTitle);
            txtLessonDetails = (TextView) view.findViewById(R.id.txtLessonDetails);
            linLesson = (LinearLayout) view.findViewById(R.id.linLesson);
            swipeLayout = (SwipeLayout) itemView.findViewById(R.id.lessonsswipe);
            tvDelete = (TextView) itemView.findViewById(R.id.tvDelete_lessons);
            imagright_arrow = (ImageView) itemView.findViewById(R.id.imagright_arrow);
        }

        @Override
        public void onItemSelected() {
            if (setFalg) {
                itemView.setBackgroundResource(R.drawable.shadow_layout);
            } else {
                itemView.setBackgroundResource(0);
            }
        }

        @Override
        public void onItemClear() {
            itemView.setBackgroundColor(0);
        }
    }

    @Override
    public LessonSubListAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, final int position) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_lessons_sub_list, parent, false);
        return new LessonSubListAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final LessonSubListAdapter.MyViewHolder holder, final int position) {

        switch (lessonSubItemDataArrayList.get(position).getLessonType()) {
            case 1: // for note
                if (lessonSubItemDataArrayList.get(position).getNotetext() != null) {
                    holder.linLesson.setVisibility(View.VISIBLE);
                    holder.txtLessonTitle.setVisibility(View.GONE);
                    holder.ivIcon.setImageResource(R.drawable.notes_sand);
                    holder.txtLessonDetails.setText(lessonSubItemDataArrayList.get(position).getNotetext());
                } else {
                    holder.linLesson.setVisibility(View.GONE);
                }
                break;
            case 2: // for scripture

                String[] allIdsArray = TextUtils.split(lessonSubItemDataArrayList.get(position).getVerseID(), ",");
                ArrayList<String> idsList = new ArrayList<String>(Arrays.asList(allIdsArray));
                String verseIDNew = idsList.get(0).toString().trim();

                if (idsList.get(0).contains("-")) {
                    String[] allIdsArray1 = TextUtils.split(idsList.get(0).toString().trim(), "-");
                    int start = Integer.parseInt(allIdsArray1[0]);
                    int end = Integer.parseInt(allIdsArray1[1]);
                    verseIDNew = String.valueOf(start);
                }
                verseDataTimeLineModel = new VerseDataTimeLineModel();
                verseDataTimeLineModel = getVerseText(lessonSubItemDataArrayList.get(position).getUri(), verseIDNew);
                String verseText = verseDataTimeLineModel.getVereseText();
                String verseFullPath = verseDataTimeLineModel.getVerseFullPath();
                String lastWord = verseFullPath.substring(verseFullPath.lastIndexOf("/") + 1);
                if (verseText != null) {
                    holder.linLesson.setVisibility(View.VISIBLE);
                    holder.ivIcon.setImageResource(R.drawable.book);
                    holder.txtLessonTitle.setVisibility(View.VISIBLE);

                    String verseTitlewithID = lastWord + ":" + lessonSubItemDataArrayList.get(position).getVerseID();
                    holder.txtLessonTitle.setText(verseTitlewithID.toString().trim());

                    verseText = verseText.replace(lessonSubItemDataArrayList.get(position).getVerseID() + " ", "");
                    SpannableStringBuilder text = new SpannableStringBuilder(Global.noTrailingwhiteLines(Html.fromHtml(verseText)));
                    holder.txtLessonDetails.setText(text);
                } else {
                    holder.linLesson.setVisibility(View.GONE);
                }
                break;
            case 3://for URL
                if (lessonSubItemDataArrayList.get(position).getUrltitle() != null) {
                    holder.linLesson.setVisibility(View.VISIBLE);
                    holder.txtLessonTitle.setVisibility(View.VISIBLE);
                    holder.ivIcon.setImageResource(R.drawable.language_sand);
                    holder.txtLessonTitle.setText(lessonSubItemDataArrayList.get(position).getUrltitle());

                    if (lessonSubItemDataArrayList.get(position).getUrl() != null) {
                        holder.txtLessonDetails.setMaxLines(1);
                        holder.txtLessonDetails.setText(lessonSubItemDataArrayList.get(position).getUrl());
                    }
                } else {
                    holder.linLesson.setVisibility(View.GONE);
                }
                break;
        }

        holder.linLesson.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String fullVerseUri = lessonSubItemDataArrayList.get(position).getUri();
                verseDataTimeLineModel = new VerseDataTimeLineModel();
                verseDataTimeLineModel = getVerseText(fullVerseUri, "0");
                verseLcId = verseDataTimeLineModel.getLcId();
                verseLsId = verseDataTimeLineModel.getLsId();
                indexForReading = verseDataTimeLineModel.getIndexForReading();
                verseAid = verseDataTimeLineModel.getVerseAid();
                verseIdArrayStrig = lessonSubItemDataArrayList.get(position).getVerseID();
                if (verseAid == null) {
                    verseAid = "0";
                }
                redirectReadingActivity(verseLcId, verseLsId, indexForReading, verseAid, "0", verseIdArrayStrig);
            }
        });

        if (setFalg) {
            holder.imagright_arrow.setBackgroundResource(R.drawable.hamburger);
        } else {
            holder.imagright_arrow.setBackgroundResource(R.drawable.arrow_right_gray);
        }

        holder.swipeLayout.setShowMode(SwipeLayout.ShowMode.PullOut);
        // Drag From Right
        holder.swipeLayout.addDrag(SwipeLayout.DragEdge.Right, holder.swipeLayout.findViewById(R.id.bottom_wrapper_lessons));
        // Handling different events when swiping
        holder.swipeLayout.addSwipeListener(new SwipeLayout.SwipeListener() {
            @Override
            public void onClose(SwipeLayout layout) {
                //when the SurfaceView totally cover the BottomView.
            }

            @Override
            public void onUpdate(SwipeLayout layout, int leftOffset, int topOffset) {
                //you are swiping.
            }

            @Override
            public void onStartOpen(SwipeLayout layout) {

            }

            @Override
            public void onOpen(SwipeLayout layout) {
                //when the BottomView totally show.
            }

            @Override
            public void onStartClose(SwipeLayout layout) {

            }

            @Override
            public void onHandRelease(SwipeLayout layout, float xvel, float yvel) {
                //when user's hand released.
            }
        });
        holder.tvDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                final String lessonId = lessonSubItemDataArrayList.get(position).getLessonid();
                final int itemType = lessonSubItemDataArrayList.get(position).getLessonType();
                final String serverid;

                if (lessonSubItemDataArrayList.get(position).getServerid() == null) {
                    serverid = "0";
                } else {
                    serverid = lessonSubItemDataArrayList.get(position).getServerid();
                }
                final int id = lessonSubItemDataArrayList.get(position).get_id();

                try {
                    android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(context);
                    builder.setTitle("Warning");
                    builder.setMessage(R.string.are_you_sure_delete)

                            .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            })
                            .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    // Yes button clicked, do something
                                    dialog.dismiss();
                                    deleteLocally(holder, position, lessonSubItemDataArrayList, id);
                                    new PushToServerData(context);
                                }
                            });

                    builder.show();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

    }

    public VerseDataTimeLineModel getVerseText(String fullVerseUri, String var_timeline_Verse_Id) {

        Cursor cursor;
        String LcId = "";
        try {


            String[] URI_TXT = fullVerseUri.split("\\/");
            String URI_TXT_First = URI_TXT[1];
            String URI_TXT_Second = URI_TXT[2];

            cursor = databasehelper.getVerseTextAndVersePath(URI_TXT_First);
            if (cursor != null && cursor.moveToFirst()) {

                while (!cursor.isAfterLast()) {
                    LcId = cursor.getString(cursor.getColumnIndex(DataBaseHelper.KEY_ID));
                    cursor.moveToNext();
                }
            }
            if (cursor != null) {
                cursor.close();
            }
            String URI_TXT_First1 = URI_TXT_First.substring(0, 1).toUpperCase() + URI_TXT_First.substring(1);
            verseDataTimeLineModel = getLsDataFromDb(LcId, URI_TXT_Second, fullVerseUri, var_timeline_Verse_Id, URI_TXT_First1);
            verseDataTimeLineModel.setLcId(LcId);

        } catch (Exception e) {
            e.printStackTrace();
        }
        return verseDataTimeLineModel;
    }

    public VerseDataTimeLineModel getLsDataFromDb(String LcId, String verseUri, String fullVerseUri, String var_timeline_Verse_Id, String URI_TXT_First) {

        Cursor cursor;
        String LsItemId = "";
        String LsItemTitle = "";

        if (LcId.isEmpty()) {
            int index = fullVerseUri.lastIndexOf('/');
            cursor = databasehelper.getVerseLSPathEmpty(fullVerseUri.substring(0, index));
        } else {
            cursor = databasehelper.getVerseLSPath(Integer.parseInt(LcId), verseUri);
        }

        if (cursor != null && cursor.moveToFirst()) {
            while (!cursor.isAfterLast()) {
                LsItemId = cursor.getString(cursor.getColumnIndex(DataBaseHelper.KEY_LS_RESULT_ITEM_ID));
                LsItemTitle = cursor.getString(cursor.getColumnIndex(DataBaseHelper.KEY_LS_RESULT_TITLE_HTML));

                URI_TXT_First = URI_TXT_First + " / " + LsItemTitle;
                verseDataTimeLineModel = getVerseIndexDataFromDB(LsItemId, fullVerseUri, var_timeline_Verse_Id, URI_TXT_First);
                verseDataTimeLineModel.setLsId(LsItemId);

                cursor.moveToNext();
            }
        }
        return verseDataTimeLineModel;
    }

    public VerseDataTimeLineModel getVerseIndexDataFromDB(String LsItemId, String fullVerseUri, String var_timeline_Verse_Id, String URI_TXT_First) {
        SubDataBaseHelper subDataBaseHelper;
        subDataBaseHelper = new SubDataBaseHelper(context, LsItemId);

        Cursor cursor;
        cursor = subDataBaseHelper.getVerseTextFromURI(fullVerseUri);

        if (cursor != null && cursor.moveToFirst()) {
            while (!cursor.isAfterLast()) {

                String LsHrmlTextItem = cursor.getString(cursor.getColumnIndex(DataBaseHelper.KEY_LS_RESULT_TITLE_HTML));
                byte[] blob = cursor.getBlob(cursor.getColumnIndex(SubDataBaseHelper.KEY_SUB_ITEM_HTML_C1CONTENT_HTML));
                String idForReading = cursor.getString(cursor.getColumnIndex(SubDataBaseHelper.KEY_ID));
                String postionForReading = cursor.getString(cursor.getColumnIndex(SubDataBaseHelper.KEY_FT_POSITION));
                String newHtmlVerseArray = new String(blob);

                if (var_timeline_Verse_Id != "0") {
                    verseDataTimeLineModel = getCrossRefSubItemHtmlData(newHtmlVerseArray, var_timeline_Verse_Id);
                }

                verseDataTimeLineModel.setVersePath(LsHrmlTextItem);
                verseDataTimeLineModel.setVerseFullPath(URI_TXT_First + " / " + LsHrmlTextItem);
                verseDataTimeLineModel.setIndexForReading(idForReading);

                cursor.moveToNext();
            }
        }
        subDataBaseHelper.close();
        if (cursor != null) {
            cursor.close();
        }
        return verseDataTimeLineModel;
    }

    public VerseDataTimeLineModel getCrossRefSubItemHtmlData(String newHtmlVerseArray, String verse_Id) {
        String verseText = "";
        ArrayList<String> selectedArrayList = new ArrayList<>();
        Document doc = Jsoup.parse(newHtmlVerseArray);
        Elements tvVersemain = doc.select("div[class^=body-block]");
        Elements tvVerse = tvVersemain.select("p[class^=verse]");

        if (!tvVerse.isEmpty() && tvVerse != null && !tvVerse.equals("null")) {
            Element element2 = tvVerse.get(Integer.parseInt(verse_Id) - 1);
            Elements tvVerseSpan = element2.select("span[class^=study-note-ref]");
            for (int j = 0; j < tvVerseSpan.size(); j++) {
                String e2 = tvVerseSpan.get(j).text();
                selectedArrayList.add(e2);
            }

            List<IndexWrapper> indexWrapperList = Global.findIndexesForKeyword(element2.outerHtml(), "data-aid");
            String[] dataAid = new String[0];
            for (int k = 0; k < indexWrapperList.size(); k++) {
                String substring = element2.outerHtml().substring(indexWrapperList.get(k).getEnd() + 2);
                dataAid = substring.split("\"");
            }
            verseText = Html.fromHtml(element2.outerHtml()).toString();
            for (int i = 0; i < selectedArrayList.size(); i++) {
                verseText = verseText.replace(selectedArrayList.get(i), selectedArrayList.get(i).substring(1));
            }
            verseText = verseText.replace(verseText, verseText.substring(verse_Id.length()).trim());
            verseDataTimeLineModel.setVereseText(verseText);
            verseDataTimeLineModel.setVerseAid(dataAid[0]);

        } else {

            Elements tvVerse1 = tvVersemain.select("p");
            for (int i = 0; i < tvVerse1.size(); i++) {

                String pid = tvVerse1.get(i).select("p").attr("id");
                verseText = tvVerse1.get(i).outerHtml().toString();
                String verseAid = tvVerse1.get(i).attr("data-aid");
                String verseID = "";

                if (pid.startsWith("p")) {
                    verseID = pid.substring(1);
                }

                if (verseID.equals(verse_Id)) {

                    verseDataTimeLineModel.setVereseText(String.valueOf(Html.fromHtml(verseText)));
                    verseDataTimeLineModel.setVerseAid(verseAid);
                }

            }

        }

        return verseDataTimeLineModel;
    }

    public void redirectReadingActivity(String verseLcId, String verseLsId, String indexForReading, String verseAid, String verseId, String verseIdArrayStrig) {

        if (verseLcId != null || verseLsId != null || indexForReading != null) {

            Intent intent = new Intent(context, ReadingActivity.class);
            intent.putExtra(context.getString(R.string.indexForReading), Integer.parseInt(indexForReading));
            intent.putExtra(context.getString(R.string.isFromIndex), true);
            intent.putExtra(context.getString(R.string.is_history), false);
            intent.putExtra(context.getString(R.string.lcID), verseLcId);
            intent.putExtra(context.getString(R.string.lsId), verseLsId);
            intent.putExtra(context.getString(R.string.verseAId), Integer.parseInt(verseAid));
            intent.putExtra(context.getString(R.string.verseId), Integer.parseInt(verseId));
            intent.putExtra("verseIdArrayStrig", verseIdArrayStrig);

            context.startActivity(intent);
            Activity activity = (Activity) context;
            activity.overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
        }
    }

    @Override
    public int getItemCount() {
        return lessonSubItemDataArrayList.size();
    }

    private void callDeletelessonAPI(String lessonId, int itemType, String serverid, final MyViewHolder holder, final int position, final ArrayList<LessonSubItemData> lessonSubItemDataArrayList, final int id) {

        //Global.showProgressDialog(context);
        APIInterface apiService = APIClient.getClient().create(APIInterface.class);
        String url = APIClient.BASE_URL;

        String paraUserId = "", paraSessionId = "";

        if (BaseActivity.getInstance().getUserInfo(SessionManager.KEY_USER_ID) != null) {
            paraUserId = BaseActivity.getInstance().getUserInfo(SessionManager.KEY_USER_ID);
        }
        if (BaseActivity.getInstance().getUserInfo(SessionManager.KEY_USER_SESSION_ID) != null) {
            paraSessionId = BaseActivity.getInstance().getUserInfo(SessionManager.KEY_USER_SESSION_ID);
        }

        url += "push.pl?user_id=" + paraUserId +
                "&session_id=" + paraSessionId +
                "&action=" + "delete" +
                "&type=" + "lesson_item" +
                "&device=" + APIClient.DEVICE_KEY +
                "&app_version=" + BuildConfig.VERSION_NAME +
                "&lesson_id=" + lessonId +
                "&item_type=" + itemType +
                "&title=" + "" +
                "&uri=" + "" +
                "&verse=" + "" +
                "&aid=" + "0" +
                "&note=" + "" +
                "&url=" + "" +
                "&locale=" + Global.LANGUAGE +
                "&sort_order=" + "1" +
                "&apiauth=" + APIClient.AUTH_KEY +
                "&id=" + serverid;

        Call<ServerResponse> callObject = apiService.sentToServer(url.replaceAll(" ", "%20"));

        callObject.enqueue(new Callback<ServerResponse>() {

            @Override
            public void onResponse(Call<ServerResponse> call, Response<ServerResponse> response) {

                if (response.body() != null && !response.body().toString().trim().isEmpty()) {
                    //Global.dismissProgressDialog();
                }

            }

            @Override
            public void onFailure(Call<ServerResponse> call, Throwable t) {
                //Global.dismissProgressDialog();
            }
        });
    }

    public void deleteLocally(final MyViewHolder holder, final int position, final ArrayList<LessonSubItemData> lessonSubItemDataArrayList, final int id) {
        mItemManger.removeShownLayouts(holder.swipeLayout);
        lessonSubItemDataArrayList.remove(position);
        notifyItemRemoved(position);
        notifyItemRangeChanged(position, lessonSubItemDataArrayList.size());
        mItemManger.closeAllItems();
        /*commonDatabaseHelper.deleteRowData(CommonDatabaseHelper.TABLE_SUB_ITEM_LESSONS, commonDatabaseHelper.KEY_ID + " = ?",
                new String[]{String.valueOf(id)});*/

        ContentValues contentValues = new ContentValues();
        contentValues.put(CommonDatabaseHelper.KEY_DELETED, "1");
        commonDatabaseHelper.updateRowData(CommonDatabaseHelper.TABLE_SUB_ITEM_LESSONS, contentValues, CommonDatabaseHelper.KEY_ID + " = ?",
                new String[]{String.valueOf(id)});

        if (lessonSubItemDataArrayList.size() == 0) {
            context.startActivity(new Intent(context, LessonsActivity.class));
        }
    }

    @Override
    public void onItemMove(int fromPosition, int toPosition) {

        try {
            if (setFalg) {
                if (fromPosition < toPosition) {
                    for (int i = fromPosition; i < toPosition; i++) {
                        Collections.swap(lessonSubItemDataArrayList, i, i + 1);
                    }
                } else {
                    for (int i = fromPosition; i > toPosition; i--) {
                        Collections.swap(lessonSubItemDataArrayList, i, i - 1);
                    }
                }
                notifyItemMoved(fromPosition, toPosition);

                String ServerId = lessonSubItemDataArrayList.get(toPosition).getServerid();
                String SortOrder = String.valueOf(toPosition + 1);

                if (!lessonSubItemDataArrayList.get(toPosition).getServerid().equals("0")) {
                    saveLessionSortOrderArraylist = sessionManager.getLessionSortOrderArraylist();
                    if (saveLessionSortOrderArraylist != null && saveLessionSortOrderArraylist.size() > 0) {
                        for (int i = 0; i < saveLessionSortOrderArraylist.size(); i++) {
                            if (saveLessionSortOrderArraylist.get(i).getSort_order().equalsIgnoreCase(SortOrder) ||
                                    saveLessionSortOrderArraylist.get(i).getServerid().equalsIgnoreCase(ServerId)) {
                                saveLessionSortOrderArraylist.remove(i);
                            }
                        }
                        LessonSubItemData lessonSubItemData = new LessonSubItemData();
                        lessonSubItemData.setSort_order(SortOrder);
                        lessonSubItemData.setServerid(ServerId);
                        lessonSubItemData.set_id(lessonSubItemDataArrayList.get(toPosition).get_id());
                        lessonSubItemData.setLessonid(lessonSubItemDataArrayList.get(toPosition).getLessonid());
                        lessonSubItemData.setServerid(lessonSubItemDataArrayList.get(toPosition).getServerid());
                        lessonSubItemData.setLessonType(lessonSubItemDataArrayList.get(toPosition).getLessonType());
                        lessonSubItemData.setNotetext(lessonSubItemDataArrayList.get(toPosition).getNotetext());
                        lessonSubItemData.setUrltitle(lessonSubItemDataArrayList.get(toPosition).getUrltitle());
                        lessonSubItemData.setUrl(lessonSubItemDataArrayList.get(toPosition).getUrl());
                        lessonSubItemData.setUri(lessonSubItemDataArrayList.get(toPosition).getUri());
                        lessonSubItemData.setVerseID(lessonSubItemDataArrayList.get(toPosition).getVerseID());
                        lessonSubItemData.setVerseAid(lessonSubItemDataArrayList.get(toPosition).getVerseAid());
                        lessonSubItemData.setVerseString(lessonSubItemDataArrayList.get(toPosition).getVerseString());
                        lessonSubItemData.setVerseTitle(lessonSubItemDataArrayList.get(toPosition).getVerseTitle());
                        lessonSubItemData.setDeleted(lessonSubItemDataArrayList.get(toPosition).getDeleted());
                        lessonSubItemData.setModified(lessonSubItemDataArrayList.get(toPosition).getModified());
                        saveLessionSortOrderArraylist.add(lessonSubItemData);
                    } else {
                        saveLessionSortOrderArraylist = new ArrayList<>();
                        LessonSubItemData lessonSubItemData = new LessonSubItemData();
                        lessonSubItemData.setSort_order(SortOrder);
                        lessonSubItemData.setServerid(ServerId);
                        lessonSubItemData.set_id(lessonSubItemDataArrayList.get(toPosition).get_id());
                        lessonSubItemData.setLessonid(lessonSubItemDataArrayList.get(toPosition).getLessonid());
                        lessonSubItemData.setServerid(lessonSubItemDataArrayList.get(toPosition).getServerid());
                        lessonSubItemData.setLessonType(lessonSubItemDataArrayList.get(toPosition).getLessonType());
                        lessonSubItemData.setNotetext(lessonSubItemDataArrayList.get(toPosition).getNotetext());
                        lessonSubItemData.setUrltitle(lessonSubItemDataArrayList.get(toPosition).getUrltitle());
                        lessonSubItemData.setUrl(lessonSubItemDataArrayList.get(toPosition).getUrl());
                        lessonSubItemData.setUri(lessonSubItemDataArrayList.get(toPosition).getUri());
                        lessonSubItemData.setVerseID(lessonSubItemDataArrayList.get(toPosition).getVerseID());
                        lessonSubItemData.setVerseAid(lessonSubItemDataArrayList.get(toPosition).getVerseAid());
                        lessonSubItemData.setVerseString(lessonSubItemDataArrayList.get(toPosition).getVerseString());
                        lessonSubItemData.setVerseTitle(lessonSubItemDataArrayList.get(toPosition).getVerseTitle());
                        lessonSubItemData.setDeleted(lessonSubItemDataArrayList.get(toPosition).getDeleted());
                        lessonSubItemData.setModified(lessonSubItemDataArrayList.get(toPosition).getModified());
                        saveLessionSortOrderArraylist.add(lessonSubItemData);
                    }
                    sessionManager.saveLessionSortOrderArraylist(saveLessionSortOrderArraylist);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onItemDismiss(int position) {
    }

    public boolean isSetFalg(Boolean setFalg) {
        this.setFalg = setFalg;
        return setFalg;
    }
}