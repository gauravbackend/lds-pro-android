package com.ldsscriptures.pro.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.ldsscriptures.pro.R;
import com.ldsscriptures.pro.activity.MainActivity;
import com.ldsscriptures.pro.activity.subscriptionmenu.PremiumActivity;
import com.ldsscriptures.pro.model.Premium;
import com.ldsscriptures.pro.utils.SessionManager;

import java.util.ArrayList;

public class PremiumPagerAdapter extends PagerAdapter {

    ArrayList<Integer> imageList;
    ArrayList<Premium> sliderImageList;
    Activity context;
    SessionManager sessionManager;


    public PremiumPagerAdapter(Activity context, ArrayList<Premium> sliderImageList) {
        this.context = context;
        this.sliderImageList = sliderImageList;
        sessionManager = new SessionManager(context);

        imageList = new ArrayList<>();
        imageList.add(R.drawable.bg_splash);
        imageList.add(R.drawable.bg_premium_2);
        imageList.add(R.drawable.bg_premium_3);
        imageList.add(R.drawable.bg_premium_4);
        imageList.add(R.drawable.bg_premium_5);
    }

    @Override
    public int getCount() {
        return sliderImageList.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }

    @Override
    public View instantiateItem(ViewGroup container, final int position) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.item_premium, null);

        LinearLayout lnr_other = (LinearLayout) view.findViewById(R.id.lnr_other);
        RelativeLayout lnr_subscription = (RelativeLayout) view.findViewById(R.id.lnr_subscription);

        ImageView iv_image = (ImageView) view.findViewById(R.id.iv_image);
        TextView tv_title = (TextView) view.findViewById(R.id.tv_title);
        TextView tv_subTitle = (TextView) view.findViewById(R.id.tv_subTitle);
        TextView clicktocontinue = (TextView) view.findViewById(R.id.clicktocontinue);
        TextView txt_plan1 = (TextView) view.findViewById(R.id.txt_plan1);
        TextView txt_plan2 = (TextView) view.findViewById(R.id.txt_plan2);
        TextView txt_99_year = (TextView) view.findViewById(R.id.txt_99_year);
        TextView txt_99_month = (TextView) view.findViewById(R.id.txt_99_month);

        String plan_type = sessionManager.getSubscribeType();
        if (!plan_type.isEmpty() && !plan_type.equals("") && plan_type != null) {
            if (plan_type.equals("12")) {

                txt_99_year.setBackgroundResource(R.drawable.bg_rect_gray_border);
                txt_99_month.setBackgroundResource(R.drawable.bg_rect_gray_border_filled);
                txt_plan1.setText(R.string.currant_plan);
                txt_plan2.setText(R.string.choose);
                clicktocontinue.setVisibility(View.GONE);

            } else if (plan_type.equals("1")) {

                txt_99_month.setBackgroundResource(R.drawable.bg_rect_gray_border);
                txt_99_year.setBackgroundResource(R.drawable.bg_rect_gray_border_filled);

                txt_plan1.setText(R.string.choose);
                txt_plan2.setText(R.string.currant_plan);
                clicktocontinue.setVisibility(View.GONE);
            } else {

                txt_99_month.setBackgroundResource(R.drawable.bg_rect_gray_border_filled);
                txt_99_year.setBackgroundResource(R.drawable.bg_rect_gray_border_filled);

                txt_plan1.setVisibility(View.GONE);
                txt_plan2.setVisibility(View.GONE);
            }
        } else {

            txt_99_month.setBackgroundResource(R.drawable.bg_rect_gray_border_filled);
            txt_99_year.setBackgroundResource(R.drawable.bg_rect_gray_border_filled);

            txt_plan1.setVisibility(View.GONE);
            txt_plan2.setVisibility(View.GONE);
        }


        iv_image.setBackgroundResource(imageList.get(position));

        if (position == 0) {
            lnr_subscription.setVisibility(View.VISIBLE);
            lnr_other.setVisibility(View.GONE);
        } else {
            lnr_subscription.setVisibility(View.GONE);
            lnr_other.setVisibility(View.VISIBLE);

            tv_title.setText(sliderImageList.get(position).getTitle());
            tv_subTitle.setText(sliderImageList.get(position).getSubTitle());
        }

        txt_99_year.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (PremiumActivity.getInstance() != null)
                    PremiumActivity.getInstance().purchaseYear();
            }
        });

        txt_99_month.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (PremiumActivity.getInstance() != null)
                    PremiumActivity.getInstance().purchaseMonth();
            }
        });

        clicktocontinue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                context.startActivity(new Intent(context, MainActivity.class));

               /* Thread t = new Thread(new Runnable() {
                    @Override
                    public void run() {
                        String purchaseToken = "inapp:" + context.getPackageName() + ":android.test.purchased";
                        try {
                            Log.d("", "Running");

                            int response = IabHelper.mService.consumePurchase(3, context.getPackageName(), purchaseToken);
                            if (response == 0) {
                                Log.d("Consumed", "Consumed");
                            } else {
                                Log.d("", "No" + response);
                            }
                        } catch (RemoteException e) {
                            Log.d("Errorr", "" + e);
                        }

                    }
                });
                t.start();*/

            }
        });

        container.addView(view);
        return view;
    }

}
