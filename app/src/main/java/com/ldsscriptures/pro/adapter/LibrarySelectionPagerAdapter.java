package com.ldsscriptures.pro.adapter;


import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.ldsscriptures.pro.R;
import com.ldsscriptures.pro.fragment.BookmarksFragment;
import com.ldsscriptures.pro.fragment.HistoryFragment;
import com.ldsscriptures.pro.fragment.LibrarySelectionFragment;


public class LibrarySelectionPagerAdapter extends FragmentStatePagerAdapter {
    private final String lcID;
    int tabCount;
    Activity activity;

    public LibrarySelectionPagerAdapter(Activity activity, FragmentManager fm, int tabCount, String lcID) {
        super(fm);
        this.tabCount = tabCount;
        this.lcID = lcID;
        this.activity = activity;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                Bundle bundle = new Bundle();
                bundle.putString(activity.getResources().getString(R.string.lcID), lcID);
                LibrarySelectionFragment fragObject = new LibrarySelectionFragment();
                fragObject.setArguments(bundle);

                return fragObject;
            case 1:
                return new BookmarksFragment();
            case 2:
                return new HistoryFragment();
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return tabCount;
    }
}