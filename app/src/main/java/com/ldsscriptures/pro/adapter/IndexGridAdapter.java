package com.ldsscriptures.pro.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.ldsscriptures.pro.R;
import com.ldsscriptures.pro.model.LsIndexPreviewData;

import java.util.ArrayList;

public class IndexGridAdapter extends BaseAdapter {
    private Context context;
    private ArrayList<LsIndexPreviewData> lcDataArrayList = new ArrayList<>();
    private ViewHolder holder;

    public IndexGridAdapter(Context context, ArrayList<LsIndexPreviewData> lcDataArrayList) {
        this.context = context;
        this.lcDataArrayList = lcDataArrayList;
    }

    @Override
    public int getCount() {
        return lcDataArrayList.size();
    }

    @Override
    public Object getItem(int position) {
        return lcDataArrayList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = LayoutInflater.from(context).inflate(R.layout.item_grid_index, null);
            holder = new ViewHolder();
            holder.tvGridItem = (TextView) convertView.findViewById(R.id.tvGridItem);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        holder.tvGridItem.setText(String.valueOf(position + 1));

        return convertView;
    }

    private static class ViewHolder {
        TextView tvGridItem;
    }
}