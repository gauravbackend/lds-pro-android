package com.ldsscriptures.pro.adapter;

import android.app.Activity;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.ldsscriptures.pro.R;
import com.ldsscriptures.pro.activity.studytools.AddLessonScripture;
import com.ldsscriptures.pro.activity.verses.VerseCrossReferenceActivity;
import com.ldsscriptures.pro.model.VerseCrossReferenceData;
import com.ldsscriptures.pro.model.VerseData;
import com.ldsscriptures.pro.utils.Global;
import com.ldsscriptures.pro.utils.SessionManager;

import java.util.ArrayList;


public class CrossReferenceReadingHtmlDataAdapter extends RecyclerView.Adapter<CrossReferenceReadingHtmlDataAdapter.MyViewHolder> {

    private String verseAId = "";
    private String uri = "";
    private String title = "";
    private ArrayList<VerseData> verseDatas = new ArrayList<>();
    private ArrayList<VerseData> verseDatasScripture = new ArrayList<>();
    private Activity activity;
    public SessionManager sessionManager;
    private String s;

    static CrossReferenceReadingHtmlDataAdapter crossReferenceReadingHtmlDataAdapter;

    public static CrossReferenceReadingHtmlDataAdapter getInstance() {
        return crossReferenceReadingHtmlDataAdapter;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        private final CheckBox checkbox;
        public TextView tvId;
        public TextView tvName;
        public LinearLayout relMain;

        public MyViewHolder(View view) {
            super(view);
            tvId = (TextView) view.findViewById(R.id.tvId);
            tvName = (TextView) view.findViewById(R.id.tvName);
            relMain = (LinearLayout) view.findViewById(R.id.relMain);
            checkbox = (CheckBox) view.findViewById(R.id.checkbox);
            sessionManager = new SessionManager(activity);
        }
    }

    public CrossReferenceReadingHtmlDataAdapter(Activity activity, ArrayList<VerseData> verseDatas, String title, String verseAId, String uri, String s) {
        this.verseDatas = verseDatas;
        this.activity = activity;
        crossReferenceReadingHtmlDataAdapter = this;
        this.title = title;
        this.verseAId = verseAId;
        this.uri = uri;
        this.s = s;
        verseDatasScripture.clear();
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, final int position) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_cross_reference_reading_html, parent, false);
        return new MyViewHolder(itemView);
    }

    public ArrayList<VerseData> selectedReferenceArray() {
        return verseDatas;
    }

    public ArrayList<VerseData> selectedScriptureArray() {
        return verseDatasScripture;
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {

        String temp = (String) Global.noTrailingwhiteLines(Html.fromHtml(verseDatas.get(position).getStringSentense()).toString());
        for (int i = 0; i < verseDatas.get(position).getFootnoteArray().size(); i++) {
            temp = temp.replace(verseDatas.get(position).getFootnoteArray().get(i), verseDatas.get(position).getFootnoteArray().get(i).substring(1));
        }

        temp = temp.replace(verseDatas.get(position).getId(), "");
        holder.tvName.setVisibility(View.VISIBLE);
        holder.tvId.setText(verseDatas.get(position).getId());
        holder.tvName.setText(temp);

        if (verseDatas.get(position).isSelected()) {
            holder.checkbox.setChecked(true);
            holder.relMain.setBackgroundColor(activity.getResources().getColor(R.color.camel));
        } else {
            holder.relMain.setBackgroundColor(Color.TRANSPARENT);
            holder.checkbox.setChecked(false);
        }

        holder.relMain.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                if (s.equals("0")) {
                    if (!verseDatas.get(position).getData_aid().equalsIgnoreCase(verseAId)) {
                        if (sessionManager.getPreferencesCrossRefArrayList(activity) != null) {
                            ArrayList<VerseCrossReferenceData> verseCrossReferenceDataArrayList = sessionManager.getPreferencesCrossRefArrayList(activity);
                            if (verseCrossReferenceDataArrayList.size() != 0) {
                                boolean isMath = false;
                                for (int i = 0; i < verseCrossReferenceDataArrayList.size(); i++) {
                                    boolean b1 = verseAId.equalsIgnoreCase(verseCrossReferenceDataArrayList.get(i).getRefAid1())
                                            || verseAId.equalsIgnoreCase(verseCrossReferenceDataArrayList.get(i).getRefAid2());
                                    boolean b2 = verseDatas.get(position).getData_aid().equalsIgnoreCase(verseCrossReferenceDataArrayList.get(i).getRefAid1())
                                            || verseDatas.get(position).getData_aid().equalsIgnoreCase(verseCrossReferenceDataArrayList.get(i).getRefAid2());
                                    if (b1 && b2) {
                                        isMath = true;
                                        break;
                                    } else {
                                        isMath = false;
                                    }
                                }
                                if (isMath) {
                                    Global.showToast(activity, activity.getString(R.string.already_added));
                                } else {
                                    if (verseDatas.get(position).isSelected())
                                        verseDatas.get(position).setSelected(false);
                                    else
                                        verseDatas.get(position).setSelected(true);

                                    // set visibility of "DONE" Button of Cross Reference
                                    boolean isVisible = false;
                                    for (int i = 0; i < verseDatas.size(); i++) {
                                        if (verseDatas.get(i).isSelected()) {
                                            isVisible = true;
                                            break;
                                        }
                                    }

                                    if (isVisible) {
                                        if (VerseCrossReferenceActivity.getInstance() != null) {
                                            VerseCrossReferenceActivity.getInstance().visibleDone(true);
                                        }
                                    } else {
                                        VerseCrossReferenceActivity.getInstance().visibleDone(false);
                                    }

                                    notifyDataSetChanged();
                                }
                            } else {
                                if (verseDatas.get(position).isSelected())
                                    verseDatas.get(position).setSelected(false);
                                else
                                    verseDatas.get(position).setSelected(true);

                                // set visibility of "DONE" Button of Cross Reference
                                boolean isVisible = false;
                                for (int i = 0; i < verseDatas.size(); i++) {
                                    if (verseDatas.get(i).isSelected()) {
                                        isVisible = true;
                                        break;
                                    }
                                }

                                if (isVisible) {
                                    if (VerseCrossReferenceActivity.getInstance() != null) {
                                        VerseCrossReferenceActivity.getInstance().visibleDone(true);
                                    }
                                } else {
                                    VerseCrossReferenceActivity.getInstance().visibleDone(false);
                                }


                                notifyDataSetChanged();
                            }
                        } else {
                            if (verseDatas.get(position).isSelected())
                                verseDatas.get(position).setSelected(false);
                            else
                                verseDatas.get(position).setSelected(true);

                            // set visibility of "DONE" Button of Cross Reference
                            boolean isVisible = false;
                            for (int i = 0; i < verseDatas.size(); i++) {
                                if (verseDatas.get(i).isSelected()) {
                                    isVisible = true;
                                    break;
                                }
                            }

                            if (isVisible) {
                                if (VerseCrossReferenceActivity.getInstance() != null) {
                                    VerseCrossReferenceActivity.getInstance().visibleDone(true);
                                }
                            } else {
                                VerseCrossReferenceActivity.getInstance().visibleDone(false);
                            }

                            notifyDataSetChanged();
                        }

                        sessionManager.setUserDailyPoints(100);

                    } else {
                        Global.showToast(activity, activity.getString(R.string.cross_ref_same_selected_message));
                    }

                } else {

                    if (verseDatas.get(position).isSelected())
                        verseDatas.get(position).setSelected(false);
                    else
                        verseDatas.get(position).setSelected(true);

                    boolean isVisible = false;
                    for (int i = 0; i < verseDatas.size(); i++) {
                        if (verseDatas.get(i).isSelected()) {
                            isVisible = true;
                            break;
                        }
                    }

                    if (isVisible) {
                        if (AddLessonScripture.getInstance() != null) {
                            AddLessonScripture.getInstance().visibleDone(true);
                        }
                    } else {
                        AddLessonScripture.getInstance().visibleDone(false);
                    }

                    notifyDataSetChanged();

                    VerseData verseDatamodel = new VerseData();
                    verseDatamodel.setId(verseDatas.get(position).getId());
                    verseDatamodel.setStringSentense(verseDatas.get(position).getStringSentense());
                    verseDatamodel.setData_aid(verseDatas.get(position).getData_aid());
                    verseDatamodel.setUri(verseDatas.get(position).getUri());
                    verseDatamodel.setVersetitle(verseDatas.get(position).getVersetitle());
                    verseDatasScripture.add(verseDatamodel);
                }
            }
        });

    }

    @Override
    public int getItemCount() {
        return verseDatas.size();
    }
}