package com.ldsscriptures.pro.adapter;

import android.app.Activity;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.daimajia.swipe.SwipeLayout;
import com.daimajia.swipe.adapters.RecyclerSwipeAdapter;
import com.ldsscriptures.pro.R;
import com.ldsscriptures.pro.activity.ReadingActivity;
import com.ldsscriptures.pro.database.CommonDatabaseHelper;
import com.ldsscriptures.pro.database.DataBaseHelper;
import com.ldsscriptures.pro.database.SubDataBaseHelper;
import com.ldsscriptures.pro.fragment.BookmarksFragment;
import com.ldsscriptures.pro.model.BookmarkData;
import com.ldsscriptures.pro.model.IndexWrapper;
import com.ldsscriptures.pro.model.TimeLine.VerseDataTimeLineModel;
import com.ldsscriptures.pro.utils.EditItemTouchHelperCallback;
import com.ldsscriptures.pro.utils.Global;
import com.ldsscriptures.pro.utils.ItemTouchHelperViewHolder;
import com.ldsscriptures.pro.utils.PushToServerData;
import com.ldsscriptures.pro.utils.SessionManager;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;


public class BookmarkFragmentListAdapter extends RecyclerSwipeAdapter<BookmarkFragmentListAdapter.MyViewHolder> implements EditItemTouchHelperCallback.ItemTouchHelperAdapter {

    private ArrayList<BookmarkData> bookmarkDataArrayList = new ArrayList<>();
    private Activity activity;
    DataBaseHelper databasehelper;
    VerseDataTimeLineModel verseDataTimeLineModel;
    String paraUserId;
    String paraSessionId;
    CommonDatabaseHelper commonDatabaseHelper;
    SessionManager sessionManager;

    @Override
    public int getSwipeLayoutResourceId(int position) {
        return R.id.bookmarkswipe;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder implements ItemTouchHelperViewHolder {
        public TextView tvBookmarkTitle, tvBookmarkSentense, txt_currant_date_time;
        private LinearLayout relMain;
        SwipeLayout swipeLayout;
        TextView tvDelete;

        public MyViewHolder(View view) {
            super(view);
            tvBookmarkTitle = (TextView) view.findViewById(R.id.tvBookmarkTitle);
            tvBookmarkSentense = (TextView) view.findViewById(R.id.tvBookmarkSentense);
            txt_currant_date_time = (TextView) view.findViewById(R.id.txt_currant_date_time);
            relMain = (LinearLayout) view.findViewById(R.id.relMain);
            swipeLayout = (SwipeLayout) itemView.findViewById(R.id.bookmarkswipe);
            tvDelete = (TextView) itemView.findViewById(R.id.tvDelete);
            verseDataTimeLineModel = new VerseDataTimeLineModel();
        }

        @Override
        public void onItemSelected() {

        }

        @Override
        public void onItemClear() {

        }
    }

    public BookmarkFragmentListAdapter(Activity activity, ArrayList<BookmarkData> bookmarkDataArrayList, String paraUserId, String paraSessionId) {
        this.bookmarkDataArrayList = bookmarkDataArrayList;
        this.activity = activity;
        databasehelper = new DataBaseHelper(activity);
        this.paraUserId = paraUserId;
        this.paraSessionId = paraSessionId;
        commonDatabaseHelper = new CommonDatabaseHelper(activity);
        sessionManager = new SessionManager(activity);
    }

    @Override
    public BookmarkFragmentListAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, final int position) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_bookmark_fragment, parent, false);
        return new BookmarkFragmentListAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final BookmarkFragmentListAdapter.MyViewHolder holder, final int position) {

        String modified = bookmarkDataArrayList.get(position).getModified();
        String deleted = bookmarkDataArrayList.get(position).getDeleted();

        holder.tvBookmarkTitle.setText(bookmarkDataArrayList.get(position).getTitle());
        holder.tvBookmarkSentense.setText(bookmarkDataArrayList.get(position).getVerseText());
        verseDataTimeLineModel = getVerseText(bookmarkDataArrayList.get(position).getItemuri().toString().trim(), bookmarkDataArrayList.get(position).getVerseNumber().toString().trim());
        String verseTextNew = verseDataTimeLineModel.getVereseText();
        holder.tvBookmarkSentense.setText(verseTextNew);

        holder.relMain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                verseDataTimeLineModel = getVerseText(bookmarkDataArrayList.get(position).getItemuri().toString().trim(), bookmarkDataArrayList.get(position).getVerseNumber().toString().trim());
                String verseText = verseDataTimeLineModel.getVereseText();
                String versepath = verseDataTimeLineModel.getVerseFullPath();
                final String verseLcId = verseDataTimeLineModel.getLcId();
                final String verseLsId = verseDataTimeLineModel.getLsId();
                final String indexForReading = verseDataTimeLineModel.getIndexForReading();
                final String verseAid = verseDataTimeLineModel.getVerseAid();
                final String verseId = bookmarkDataArrayList.get(position).getVerseNumber();
                redirectReadingActivity(verseLcId, verseLsId, indexForReading, verseAid, verseId);
            }
        });

        holder.swipeLayout.setShowMode(SwipeLayout.ShowMode.PullOut);
        // Drag From Right
        holder.swipeLayout.addDrag(SwipeLayout.DragEdge.Right, holder.swipeLayout.findViewById(R.id.bottom_wrapper));
        // Handling different events when swiping
        holder.swipeLayout.addSwipeListener(new SwipeLayout.SwipeListener() {
            @Override
            public void onClose(SwipeLayout layout) {
                //when the SurfaceView totally cover the BottomView.
            }

            @Override
            public void onUpdate(SwipeLayout layout, int leftOffset, int topOffset) {
                //you are swiping.
            }

            @Override
            public void onStartOpen(SwipeLayout layout) {

            }

            @Override
            public void onOpen(SwipeLayout layout) {
                //when the BottomView totally show.
            }

            @Override
            public void onStartClose(SwipeLayout layout) {

            }

            @Override
            public void onHandRelease(SwipeLayout layout, float xvel, float yvel) {
                //when user's hand released.
            }
        });

        holder.tvDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                final String paraUri = bookmarkDataArrayList.get(position).getItemuri().toString().trim();
                final String paraVerseId = bookmarkDataArrayList.get(position).getVerseNumber().toString().trim();
                final String paraVerseAId = bookmarkDataArrayList.get(position).getVerseaid().toString().trim();
                final String paraNoteTitle = bookmarkDataArrayList.get(position).getTitle().toString().trim();
                final String serverid = bookmarkDataArrayList.get(position).getServerid().toString().trim();

                try {
                    android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(activity);
                    builder.setTitle("Warning");
                    builder.setMessage(R.string.are_you_sure_delete)

                            .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            })
                            .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    // Yes button clicked, do something
                                    dialog.dismiss();
                                    deleteLocally(holder, position, bookmarkDataArrayList, paraVerseAId);

                                    new PushToServerData(activity);
                                }
                            });

                    builder.show();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return bookmarkDataArrayList.size();
    }

    public void redirectReadingActivity(String verseLcId, String verseLsId, String indexForReading, String verseAid, String verseId) {

        if (verseLcId != null && verseLsId != null && indexForReading != null) {
            Intent intent = new Intent(activity, ReadingActivity.class);
            intent.putExtra(activity.getString(R.string.indexForReading), Integer.parseInt(indexForReading));
            intent.putExtra(activity.getString(R.string.isFromIndex), true);
            intent.putExtra(activity.getString(R.string.is_history), false);
            intent.putExtra(activity.getString(R.string.lcID), verseLcId);
            intent.putExtra(activity.getString(R.string.lsId), verseLsId);
            intent.putExtra(activity.getString(R.string.verseAId), Integer.parseInt(verseAid));
            intent.putExtra(activity.getString(R.string.verseId), Integer.parseInt(verseId));
            activity.startActivity(intent);
            activity.overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
        }
    }

    public VerseDataTimeLineModel getVerseText(String fullVerseUri, String var_timeline_Verse_Id) {

        Cursor cursor;
        String LcId = "";
        try {

            String[] URI_TXT = fullVerseUri.split("\\/");
            String URI_TXT_First = URI_TXT[1];
            String URI_TXT_Second = URI_TXT[2];

            cursor = databasehelper.getVerseTextAndVersePath(URI_TXT_First);
            if (cursor != null && cursor.moveToFirst()) {

                while (!cursor.isAfterLast()) {
                    LcId = cursor.getString(cursor.getColumnIndex(DataBaseHelper.KEY_ID));
                    cursor.moveToNext();
                }
            }
            if (cursor != null) {
                cursor.close();
            }
            String URI_TXT_First1 = URI_TXT_First.substring(0, 1).toUpperCase() + URI_TXT_First.substring(1);
            verseDataTimeLineModel = getLsDataFromDb(LcId, URI_TXT_Second, fullVerseUri, var_timeline_Verse_Id, URI_TXT_First1);
            verseDataTimeLineModel.setLcId(LcId);

        } catch (Exception e) {
            e.printStackTrace();
        }
        return verseDataTimeLineModel;
    }

    public VerseDataTimeLineModel getLsDataFromDb(String LcId, String verseUri, String fullVerseUri, String var_timeline_Verse_Id, String URI_TXT_First) {

        Cursor cursor;
        String LsItemId = "";
        String LsItemTitle = "";

        if (LcId.isEmpty()) {
            int index = fullVerseUri.lastIndexOf('/');
            cursor = databasehelper.getVerseLSPathEmpty(fullVerseUri.substring(0, index));
        } else {
            cursor = databasehelper.getVerseLSPath(Integer.parseInt(LcId), verseUri);
        }

        if (cursor != null && cursor.moveToFirst()) {
            while (!cursor.isAfterLast()) {
                LsItemId = cursor.getString(cursor.getColumnIndex(DataBaseHelper.KEY_LS_RESULT_ITEM_ID));
                LsItemTitle = cursor.getString(cursor.getColumnIndex(DataBaseHelper.KEY_LS_RESULT_TITLE_HTML));

                URI_TXT_First = URI_TXT_First + " / " + LsItemTitle;
                verseDataTimeLineModel = getVerseIndexDataFromDB(LsItemId, fullVerseUri, var_timeline_Verse_Id, URI_TXT_First);
                verseDataTimeLineModel.setLsId(LsItemId);

                cursor.moveToNext();
            }
        }
        return verseDataTimeLineModel;
    }

    public VerseDataTimeLineModel getVerseIndexDataFromDB(String LsItemId, String fullVerseUri, String var_timeline_Verse_Id, String URI_TXT_First) {
        SubDataBaseHelper subDataBaseHelper;
        subDataBaseHelper = new SubDataBaseHelper(activity, LsItemId);

        Cursor cursor;
        cursor = subDataBaseHelper.getVerseTextFromURI(fullVerseUri);

        if (cursor != null && cursor.moveToFirst()) {
            while (!cursor.isAfterLast()) {

                String LsHrmlTextItem = cursor.getString(cursor.getColumnIndex(DataBaseHelper.KEY_LS_RESULT_TITLE_HTML));
                byte[] blob = cursor.getBlob(cursor.getColumnIndex(SubDataBaseHelper.KEY_SUB_ITEM_HTML_C1CONTENT_HTML));
                String idForReading = cursor.getString(cursor.getColumnIndex(SubDataBaseHelper.KEY_ID));
                String postionForReading = cursor.getString(cursor.getColumnIndex(SubDataBaseHelper.KEY_FT_POSITION));
                String newHtmlVerseArray = new String(blob);

                verseDataTimeLineModel = getCrossRefSubItemHtmlData(newHtmlVerseArray, var_timeline_Verse_Id);
                verseDataTimeLineModel.setVersePath(LsHrmlTextItem);
                verseDataTimeLineModel.setVerseFullPath(URI_TXT_First + " / " + LsHrmlTextItem);
                verseDataTimeLineModel.setIndexForReading(idForReading);

                cursor.moveToNext();
            }
        }
        subDataBaseHelper.close();
        if (cursor != null) {
            cursor.close();
        }
        return verseDataTimeLineModel;
    }

    public VerseDataTimeLineModel getCrossRefSubItemHtmlData(String newHtmlVerseArray, String verse_Id) {
        String verseText = "";
        ArrayList<String> selectedArrayList = new ArrayList<>();
        Document doc = Jsoup.parse(newHtmlVerseArray);


        Elements tvVersemain = doc.select("div[class^=body-block]");
        Elements tvVerse = tvVersemain.select("p[class^=verse]");

        if (!tvVerse.isEmpty() && tvVerse != null && !tvVerse.equals("null")) {
            Element element2 = tvVerse.get(Integer.parseInt(verse_Id) - 1);
            Elements tvVerseSpan = element2.select("span[class^=study-note-ref]");
            for (int j = 0; j < tvVerseSpan.size(); j++) {
                String e2 = tvVerseSpan.get(j).text();
                selectedArrayList.add(e2);
            }

            List<IndexWrapper> indexWrapperList = Global.findIndexesForKeyword(element2.outerHtml(), "data-aid");
            String[] dataAid = new String[0];
            for (int k = 0; k < indexWrapperList.size(); k++) {
                String substring = element2.outerHtml().substring(indexWrapperList.get(k).getEnd() + 2);
                dataAid = substring.split("\"");
            }
            verseText = Html.fromHtml(element2.outerHtml()).toString();
            for (int i = 0; i < selectedArrayList.size(); i++) {
                verseText = verseText.replace(selectedArrayList.get(i), selectedArrayList.get(i).substring(1));
            }
            verseText = verseText.replace(verseText, verseText.substring(verse_Id.length()).trim());
            verseDataTimeLineModel.setVereseText(verseText);
            verseDataTimeLineModel.setVerseAid(dataAid[0]);
        } else {

            Elements tvVerse1 = tvVersemain.select("p");

            for (int i = 0; i < tvVerse1.size(); i++) {

                String pid = tvVerse1.get(i).select("p").attr("id");
                verseText = tvVerse1.get(i).outerHtml().toString();
                String verseAid = tvVerse1.get(i).attr("data-aid");
                String verseID = "";

                if (pid.startsWith("p")) {
                    verseID = pid.substring(1);
                }

                if (verseID.equals(verse_Id)) {

                    verseDataTimeLineModel.setVereseText(String.valueOf(Html.fromHtml(verseText)));
                    verseDataTimeLineModel.setVerseAid(verseAid);
                }

            }
        }

        return verseDataTimeLineModel;
    }

    public void deleteLocally(final MyViewHolder holder, final int position, final ArrayList<BookmarkData> bookmarkDataArrayList, final String paraVerseAId) {
        mItemManger.removeShownLayouts(holder.swipeLayout);
        bookmarkDataArrayList.remove(position);
        notifyItemRemoved(position);
        notifyItemRangeChanged(position, bookmarkDataArrayList.size());
        mItemManger.closeAllItems();

       /* commonDatabaseHelper.deleteRowData(CommonDatabaseHelper.TABLE_BOOKMARK_MARSTER, commonDatabaseHelper.KEY_VERSEAID2 + " = ?",
                new String[]{paraVerseAId});
*/
        ContentValues contentValues = new ContentValues();
        contentValues.put(CommonDatabaseHelper.KEY_DELETED, "1");
        commonDatabaseHelper.updateRowData(CommonDatabaseHelper.TABLE_BOOKMARK_MARSTER, contentValues, CommonDatabaseHelper.KEY_VERSEAID2 + " = ?",
                new String[]{String.valueOf(paraVerseAId)});

        if (bookmarkDataArrayList.size() == 0) {
            BookmarksFragment.getInstance().visibleHideView();
        }
    }

    @Override
    public void onItemDismiss(int position) {

    }

    @Override
    public void onItemMove(int fromPosition, int toPosition) {
        if (fromPosition < toPosition) {
            for (int i = fromPosition; i < toPosition; i++) {
                Collections.swap(bookmarkDataArrayList, i, i + 1);
            }
        } else {
            for (int i = fromPosition; i > toPosition; i--) {
                Collections.swap(bookmarkDataArrayList, i, i - 1);
            }
        }
        notifyItemMoved(fromPosition, toPosition);
    }
}
