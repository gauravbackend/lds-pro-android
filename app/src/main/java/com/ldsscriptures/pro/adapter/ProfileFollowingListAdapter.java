package com.ldsscriptures.pro.adapter;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.ldsscriptures.pro.R;
import com.ldsscriptures.pro.model.FollowersMainData;
import com.ldsscriptures.pro.utils.Global;
import com.ldsscriptures.pro.utils.SessionManager;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

public class ProfileFollowingListAdapter extends RecyclerView.Adapter<ProfileFollowingListAdapter.ViewHolder> {

    private ArrayList<FollowersMainData> followingarraylist;
    private Activity activity;
    private SessionManager sessionManager;
    float rv_width;

    public ProfileFollowingListAdapter(Activity activity, ArrayList<FollowersMainData> followingarraylist, float rv_width) {
        this.followingarraylist = followingarraylist;
        this.activity = activity;
        sessionManager = new SessionManager(activity);
        this.rv_width = rv_width;

    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.activity_profile_following_list, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int i) {

        String following_level = followingarraylist.get(i).getFollowing_level();
        String following_name = followingarraylist.get(i).getFollowing_name();
        String following_userid = followingarraylist.get(i).getFollowing_user_id();
        String following_points_week = followingarraylist.get(i).getFollowing_points_week();
        String following_points_month = followingarraylist.get(i).getFollowing_points_month();
        String following_points_year = followingarraylist.get(i).getFollowing_points_year();
        String following_img_url = followingarraylist.get(i).getFollowing_img_url();

        if (following_img_url != null) {
            Picasso.with(activity).load(following_img_url).into(viewHolder.profile_following_image);
        } else {
            viewHolder.profile_following_image.setBackgroundResource(R.drawable.profile_lg);
        }


    }

    @Override
    public int getItemCount() {
        return followingarraylist.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private CircleImageView profile_following_image;
        private LinearLayout profile_image_layout;

        public ViewHolder(View view) {
            super(view);
            profile_following_image = (CircleImageView) view.findViewById(R.id.profile_following_image);
            profile_image_layout = (LinearLayout) view.findViewById(R.id.profile_image_layout);

            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams((int) rv_width, (int) rv_width);
            profile_image_layout.setLayoutParams(layoutParams);
            profile_following_image.setLayoutParams(layoutParams);
            itemView.setHorizontalScrollBarEnabled(true);
        }
    }


}