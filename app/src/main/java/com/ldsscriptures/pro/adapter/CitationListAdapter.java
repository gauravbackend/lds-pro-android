package com.ldsscriptures.pro.adapter;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.ldsscriptures.pro.R;
import com.ldsscriptures.pro.activity.verses.CitationSubListActivity;
import com.ldsscriptures.pro.model.CitationList;

import java.util.ArrayList;
import java.util.List;


public class CitationListAdapter extends RecyclerView.Adapter<CitationListAdapter.MyViewHolder> {

    private List<CitationList> indexList;
    private Activity activity;
    private String citation_itemUri, verseId;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView tvIndexItem, tvCount;
        RelativeLayout relMain;

        public MyViewHolder(View view) {
            super(view);
            tvCount = (TextView) view.findViewById(R.id.tvCount);
            tvIndexItem = (TextView) view.findViewById(R.id.tvVerse);
            relMain = (RelativeLayout) view.findViewById(R.id.relMain);
        }
    }

    public CitationListAdapter(Activity activity, ArrayList<CitationList> indexList, String citation_itemUri, String verseId) {
        this.indexList = indexList;
        this.activity = activity;
        this.citation_itemUri = citation_itemUri;
        this.verseId = verseId;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, final int position) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_citation_list, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {

        holder.tvIndexItem.setText(indexList.get(position).getName());
        holder.tvCount.setText(indexList.get(position).getCnt());

        holder.relMain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(activity, CitationSubListActivity.class);
                intent.putExtra(activity.getString(R.string.citation_uri), citation_itemUri);
                intent.putExtra(activity.getString(R.string.verseId), verseId);
                intent.putExtra(activity.getString(R.string.source_group), indexList.get(position).getSource_group());
                activity.startActivity(intent);
                activity.overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
            }
        });
    }

    @Override
    public int getItemCount() {
        return indexList.size();
    }
}