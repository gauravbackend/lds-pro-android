package com.ldsscriptures.pro.adapter;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.ldsscriptures.pro.R;
import com.ldsscriptures.pro.model.GlobalSearchModel;

import java.util.ArrayList;
import java.util.List;


public class SearchLSAdapter extends RecyclerView.Adapter<SearchLSAdapter.MyViewHolder> {

    private List<GlobalSearchModel> filtered_indexList;
    private Activity activity;
    private int mPosition;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView tvIndexItem, tvCount;
        RelativeLayout relMain;

        public MyViewHolder(View view) {
            super(view);
            tvCount = (TextView) view.findViewById(R.id.tvCount);
            tvIndexItem = (TextView) view.findViewById(R.id.tvVerse);
            relMain = (RelativeLayout) view.findViewById(R.id.relMain);
        }
    }

    public SearchLSAdapter(Activity activity, ArrayList<GlobalSearchModel> indexList, int position) {
        this.filtered_indexList = indexList;
        this.activity = activity;
        this.mPosition = position;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, final int position) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_citation_list, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {

        GlobalSearchModel globalSearchVerseModel = filtered_indexList.get(position);

        holder.tvIndexItem.setText(globalSearchVerseModel.getSubTitle());

        holder.tvCount.setText(globalSearchVerseModel.getGlobalSearchModels().size() + "");

        holder.relMain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
           /*     Intent intent = new Intent(activity, SearchItemActivity.class);
                intent.putExtra(activity.getString(R.string.position), mPosition);
                intent.putExtra(activity.getString(R.string.sub_position), position);
                activity.startActivity(intent);
                activity.overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);*/
            }
        });
    }

    @Override
    public int getItemCount() {
        return filtered_indexList.size();
    }


}