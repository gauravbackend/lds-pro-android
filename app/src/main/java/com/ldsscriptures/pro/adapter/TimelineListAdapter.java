package com.ldsscriptures.pro.adapter;

import android.app.Activity;
import android.app.ProgressDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.ldsscriptures.pro.BuildConfig;
import com.ldsscriptures.pro.R;
import com.ldsscriptures.pro.activity.BaseActivity;
import com.ldsscriptures.pro.database.DataBaseHelper;
import com.ldsscriptures.pro.model.TimeLine.CommentTimeLineModel;
import com.ldsscriptures.pro.model.TimeLine.InnerTimeLineModel;
import com.ldsscriptures.pro.model.TimeLine.LikesTimeLineModel;
import com.ldsscriptures.pro.model.TimeLine.VerseDataTimeLineModel;
import com.ldsscriptures.pro.model.TimeLineShareSuccess;
import com.ldsscriptures.pro.retroutils.APIClient;
import com.ldsscriptures.pro.retroutils.APIInterface;
import com.ldsscriptures.pro.utils.Global;
import com.ldsscriptures.pro.utils.SessionManager;
import com.ldsscriptures.pro.utils.TimeAgo;
import com.squareup.picasso.Picasso;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class TimelineListAdapter extends RecyclerView.Adapter<TimelineListAdapter.ViewHolder> {

    private ArrayList<InnerTimeLineModel> timelineItemArrayList = new ArrayList<>();
    private Activity activity;
    private SessionManager sessionManager;
    private int colorflag;
    DataBaseHelper databasehelper;
    String paraUserId = "";
    String paraUserImage = "";
    String paraUserName = "";
    String paraSessionId = "";
    ProgressDialog progressDoalog;

    public TimelineListAdapter(Activity activity, ArrayList<InnerTimeLineModel> timelineItemArrayList, int colorflag, String paraUserId, String paraUserImage, String paraUserName, String paraSessionId) {
        this.timelineItemArrayList = timelineItemArrayList;
        this.activity = activity;
        this.colorflag = colorflag;
        sessionManager = new SessionManager(activity);
        databasehelper = new DataBaseHelper(activity);
        this.paraUserId = paraUserId;
        this.paraUserImage = paraUserImage;
        this.paraUserName = paraUserName;
        this.paraSessionId = paraSessionId;

        progressDoalog = new ProgressDialog(activity);
        progressDoalog.setMessage("Loading...");
        progressDoalog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDoalog.setCancelable(false);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.activity_timeline_list_item, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder viewHolder, final int i) {

        String var_contact_image = timelineItemArrayList.get(i).getProfile_img();
        String var_timeline_user_name = timelineItemArrayList.get(i).getName();
        String var_timeline_user_time_ago = timelineItemArrayList.get(i).getCreated();
        String var_timeline_uri = timelineItemArrayList.get(i).getUri();
        String var_timeline_Verse_Id = timelineItemArrayList.get(i).getVerse();
        String var_timeline_content = timelineItemArrayList.get(i).getNote();
        final String var_timeline_share_id = timelineItemArrayList.get(i).getId();

        String var_time_Zone = sessionManager.getKeyTimeZone();

        final boolean[] var_timeline_likeFlag = {false};
        final boolean[] var_timeline_commentFlag = {false};
        final int[] btnCommentFlag = {0};


        final String finalParaUserId = paraUserId;
        final String finalparaUserImage = paraUserImage;
        final String finalParaparaUserName = paraUserName;

        ArrayList<CommentTimeLineModel> commentTimeLineArrayList = new ArrayList<>();
        commentTimeLineArrayList = timelineItemArrayList.get(i).getComments();

        ArrayList<LikesTimeLineModel> likesTimeLineArrayList = new ArrayList<>();
        likesTimeLineArrayList = timelineItemArrayList.get(i).getLikes();

        if (paraUserImage != null) {
            Picasso.with(activity).load(paraUserImage).into(viewHolder.commant_user_image);
        } else {
            viewHolder.contact_image.setBackgroundResource(R.drawable.profile_lg);
        }

        if (var_contact_image != null) {
            Picasso.with(activity).load(var_contact_image).into(viewHolder.contact_image);
        } else {
            viewHolder.contact_image.setBackgroundResource(R.drawable.profile_lg);
        }

        if (var_timeline_user_name != null) {
            viewHolder.timeline_user_name.setText(var_timeline_user_name.toString());
        }

        if (var_timeline_user_time_ago != null) {

            if (var_time_Zone != null) {
                try {
                    String inputPattern = "yyyy-MM-dd HH:mm:ss";
                    String timezone = getFormatDateWithUTC(var_timeline_user_time_ago, var_time_Zone, inputPattern, inputPattern);
                    SimpleDateFormat dateFormat = new SimpleDateFormat(inputPattern);
                    Date myDate = null;
                    myDate = dateFormat.parse(timezone);
                    TimeAgo var_timeAgo = new TimeAgo(activity);
                    String timeago = var_timeAgo.timeAgo(myDate);
                    viewHolder.timeline_user_time_ago.setText(timeago);

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }

        if (var_timeline_Verse_Id != null) {
            try {
                VerseDataTimeLineModel verseDataTimeLineModel = new VerseDataTimeLineModel();
                verseDataTimeLineModel = BaseActivity.getInstance().getVerseText(var_timeline_uri, var_timeline_Verse_Id.trim());
                String verseText = verseDataTimeLineModel.getVereseText();
                String versepath = verseDataTimeLineModel.getVersePath();

                if (verseText != null || verseText.length() != 0) {
                    viewHolder.timeline_nephi_content.setText(verseText);
                }

                if (versepath != null || versepath.length() != 0) {
                    versepath = versepath + " : " + var_timeline_Verse_Id;
                    viewHolder.timeline_nephi_time.setText(versepath);
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        if (var_timeline_content != null) {
            viewHolder.timeline_content.setText(var_timeline_content.toString());
        }

        if (likesTimeLineArrayList.size() != 0 || likesTimeLineArrayList != null) {
            viewHolder.txt_fav_timeline.setText(String.valueOf(likesTimeLineArrayList.size()));

            for (int j = 0; j < likesTimeLineArrayList.size(); j++) {
                if (paraUserId.equals(likesTimeLineArrayList.get(j).getUid())) {
                    var_timeline_likeFlag[0] = true;
                } else {
                    var_timeline_likeFlag[0] = false;
                }
            }

        } else {
            viewHolder.txt_fav_timeline.setText("0");
            var_timeline_likeFlag[0] = false;
        }


        if (commentTimeLineArrayList.size() != 0 || commentTimeLineArrayList != null) {
            viewHolder.txt_comment_timeline.setText(String.valueOf(commentTimeLineArrayList.size()));

            for (int j = 0; j < commentTimeLineArrayList.size(); j++) {
                if (paraUserId.equals(commentTimeLineArrayList.get(j).getUid())) {
                    var_timeline_commentFlag[0] = true;
                } else {
                    var_timeline_commentFlag[0] = false;
                }
            }

        } else {
            viewHolder.txt_comment_timeline.setText("0");
            var_timeline_commentFlag[0] = false;
        }

        if (var_timeline_likeFlag[0] == true) {
            viewHolder.img_fav_timeline.setImageResource(R.drawable.main);
        } else {
            viewHolder.img_fav_timeline.setImageResource(R.drawable.fav_dark);
        }

        if (commentTimeLineArrayList.size() != 0 || commentTimeLineArrayList != null) {
            viewHolder.txt_comment_timeline.setText(String.valueOf(commentTimeLineArrayList.size()));
            viewHolder.mCommentAdapter = new TimelineCommentAdapter(activity, commentTimeLineArrayList, var_timeline_share_id, colorflag, paraUserId, paraUserImage, paraUserName, paraSessionId);
            viewHolder.timeline_comment_recycler_view.setAdapter(viewHolder.mCommentAdapter);
        } else {
            viewHolder.txt_comment_timeline.setText("0");
        }

        if (var_timeline_commentFlag[0] == true) {
            viewHolder.img_comment_timeline.setImageResource(R.drawable.main_comment);
        } else {
            viewHolder.img_comment_timeline.setImageResource(R.drawable.dark);
        }

        viewHolder.img_comment_timeline.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (btnCommentFlag[0] == 0) {
                    viewHolder.layout_comment.setVisibility(View.VISIBLE);
                    btnCommentFlag[0] = 1;

                } else {
                    viewHolder.layout_comment.setVisibility(View.GONE);
                    btnCommentFlag[0] = 0;
                }
            }
        });


        viewHolder.post_comment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String getTimelineCommentText = viewHolder.edt_comment_post.getText().toString().trim();
                if (getTimelineCommentText.length() != 0) {

                    long strLong = Global.currentDate().getTime();
                    String timeInMilliseconds = Long.toString(strLong);


                    if (timelineItemArrayList.get(i).getComments() != null) {
                        timelineItemArrayList.get(i).getComments().add(new CommentTimeLineModel(finalparaUserImage, finalParaUserId, finalParaparaUserName, timeInMilliseconds, getTimelineCommentText));

                    } else {
                        ArrayList<CommentTimeLineModel> arrayList = new ArrayList<CommentTimeLineModel>();
                        arrayList.add(new CommentTimeLineModel(finalparaUserImage, finalParaUserId, finalParaparaUserName, timeInMilliseconds, getTimelineCommentText));
                        timelineItemArrayList.get(i).setComments(arrayList);

                    }

                    callTimelineCommentApi(var_timeline_share_id, getTimelineCommentText);
                    viewHolder.edt_comment_post.setText("");
                } else {
                    Toast.makeText(activity, R.string.write_a_comment, Toast.LENGTH_LONG).show();
                }
            }
        });


        viewHolder.img_fav_timeline.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (var_timeline_likeFlag[0]) {

                    for (int j = 0; j < timelineItemArrayList.get(i).getLikes().size(); j++) {
                        if (finalParaUserId.equals(timelineItemArrayList.get(i).getLikes().get(j).getUid())) {
                            timelineItemArrayList.get(i).getLikes().remove(j);
                        }
                    }

                    String Subscribed = sessionManager.getKeySubscribed();
                    if (!Subscribed.isEmpty() && Subscribed != null && !Subscribed.equals("null")) {
                        callTimelinePostLikeApi(var_timeline_share_id, "unlike");
                    }

                } else {

                    if (timelineItemArrayList.get(i).getLikes() != null) {
                        timelineItemArrayList.get(i).getLikes().add(new LikesTimeLineModel(finalParaUserId));
                    } else {
                        ArrayList<LikesTimeLineModel> arrayList = new ArrayList<LikesTimeLineModel>();
                        arrayList.add(new LikesTimeLineModel(finalParaUserId));
                        timelineItemArrayList.get(i).setLikes(arrayList);

                    }

                    String Subscribed = sessionManager.getKeySubscribed();
                    if (Subscribed.equals("1")) {
                        callTimelinePostLikeApi(var_timeline_share_id, "like");
                    }
                }
            }
        });

        if (colorflag == 1) {
            viewHolder.timeline_content.setTextColor(activity.getResources().getColor(R.color.white));
            viewHolder.txt_fav_timeline.setTextColor(activity.getResources().getColor(R.color.white));
            viewHolder.txt_comment_timeline.setTextColor(activity.getResources().getColor(R.color.white));
            viewHolder.timeline_verticle_divider.setTextColor(activity.getResources().getColor(R.color.settings_divider));
            viewHolder.timeline_adapter_comment.setTextColor(activity.getResources().getColor(R.color.navLibSelText));

        }

    }

    @Override
    public int getItemCount() {
        return timelineItemArrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private CircleImageView contact_image;
        private TextView timeline_user_name;
        private TextView timeline_user_time_ago;
        private TextView timeline_nephi_time;
        private TextView timeline_nephi_content;
        private TextView timeline_content;
        private ImageView img_fav_timeline;
        private TextView txt_fav_timeline;
        private ImageView img_comment_timeline;
        private TextView txt_comment_timeline;
        private RecyclerView timeline_bookmark_recycler_view;
        private LinearLayout layout_comment;
        private RecyclerView timeline_comment_recycler_view;
        private CircleImageView commant_user_image;
        private EditText edt_comment_post;
        private TextView post_comment;
        private TextView timeline_verticle_divider;
        private TimelineBookmarkAdapter mBookmarkAdapter;
        private TimelineCommentAdapter mCommentAdapter;
        private TextView timeline_adapter_comment;

        public ViewHolder(View view) {
            super(view);
            contact_image = (CircleImageView) view.findViewById(R.id.contact_image);
            timeline_user_name = (TextView) view.findViewById(R.id.timeline_user_name);
            timeline_user_time_ago = (TextView) view.findViewById(R.id.timeline_user_time_ago);
            timeline_nephi_time = (TextView) view.findViewById(R.id.timeline_nephi_time);
            timeline_nephi_content = (TextView) view.findViewById(R.id.timeline_nephi_content);
            timeline_content = (TextView) view.findViewById(R.id.timeline_content);
            img_fav_timeline = (ImageView) view.findViewById(R.id.img_fav_timeline);
            img_comment_timeline = (ImageView) view.findViewById(R.id.img_comment_timeline);
            txt_fav_timeline = (TextView) view.findViewById(R.id.txt_fav_timeline);
            txt_comment_timeline = (TextView) view.findViewById(R.id.txt_comment_timeline);
            timeline_bookmark_recycler_view = (RecyclerView) view.findViewById(R.id.timeline_bookmark_recycler_view);
            layout_comment = (LinearLayout) view.findViewById(R.id.layout_comment);
            timeline_comment_recycler_view = (RecyclerView) view.findViewById(R.id.timeline_comment_recycler_view);
            commant_user_image = (CircleImageView) view.findViewById(R.id.commant_user_image);
            edt_comment_post = (EditText) view.findViewById(R.id.edt_comment_post);
            post_comment = (TextView) view.findViewById(R.id.post_comment);
            timeline_verticle_divider = (TextView) view.findViewById(R.id.timeline_verticle_divider);
            timeline_adapter_comment = (TextView) view.findViewById(R.id.timeline_adapter_comment);

            timeline_bookmark_recycler_view.setHasFixedSize(true);
            RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(activity);
            timeline_bookmark_recycler_view.setLayoutManager(layoutManager);

            timeline_comment_recycler_view.setHasFixedSize(true);
            RecyclerView.LayoutManager layoutManager1 = new LinearLayoutManager(activity);
            timeline_comment_recycler_view.setLayoutManager(layoutManager1);
        }
    }

    @Override
    public int getItemViewType(int position) {
        return super.getItemViewType(position);
    }

    private void callTimelineCommentApi(String share_id, String commentText) {

        progressDoalog.show();

        APIInterface apiService = APIClient.getClient().create(APIInterface.class);
        String url = APIClient.BASE_URL;


        url += "timelinePush.pl?user_id=" + paraUserId +
                "&session_id=" + paraSessionId +
                "&action=" + "add" +
                "&type=" + "comments" +
                "&device=" + APIClient.DEVICE_KEY +
                "&app_version=" + BuildConfig.VERSION_NAME +
                "&share_id=" + share_id +
                "&comment=" + commentText +
                "&locale=" + Global.LANGUAGE +
                "&apiauth=" + APIClient.AUTH_KEY;


        Call<TimeLineShareSuccess> callObject = apiService.getTimeLineShareSuccess(url.replaceAll(" ", "%20"));
        callObject.enqueue(new Callback<TimeLineShareSuccess>() {

            @Override
            public void onResponse(Call<TimeLineShareSuccess> call, Response<TimeLineShareSuccess> response) {

                if (response.body() != null && !response.body().toString().trim().isEmpty()) {
                    progressDoalog.dismiss();
                }
                notifyDataSetChanged();
            }

            @Override
            public void onFailure(Call<TimeLineShareSuccess> call, Throwable t) {

                progressDoalog.dismiss();
            }
        });
    }

    private void callTimelinePostLikeApi(final String share_id, final String share_action) {

        progressDoalog.show();

        APIInterface apiService = APIClient.getClient().create(APIInterface.class);
        String url = APIClient.BASE_URL;

        url += "timelinePush.pl?user_id=" + paraUserId +
                "&session_id=" + paraSessionId +
                "&action=" + share_action +
                "&type=" + "share_likes" +
                "&device=" + APIClient.DEVICE_KEY +
                "&app_version=" + BuildConfig.VERSION_NAME +
                "&share_id=" + share_id +
                "&apiauth=" + APIClient.AUTH_KEY;

        Call<TimeLineShareSuccess> callObject = apiService.getTimeLineShareSuccess(url.replaceAll(" ", "%20"));
        callObject.enqueue(new Callback<TimeLineShareSuccess>() {

            @Override
            public void onResponse(Call<TimeLineShareSuccess> call, Response<TimeLineShareSuccess> response) {

                if (response.body() != null && !response.body().toString().trim().isEmpty()) {

                    if (response.body().getSuccess() == 1) {

                    }

                    notifyDataSetChanged();
                    progressDoalog.dismiss();
                }
            }

            @Override
            public void onFailure(Call<TimeLineShareSuccess> call, Throwable t) {

                progressDoalog.dismiss();
            }
        });
    }

    public String getFormatDateWithUTC(String date, String var_time_Zone, String inputPattern, String outputPattern) {
        try {
            SimpleDateFormat sdf3 = new SimpleDateFormat(inputPattern, Locale.ENGLISH);
            SimpleDateFormat sdfout = new SimpleDateFormat(outputPattern, Locale.ENGLISH);

            if (var_time_Zone.equals("PST")) {
                sdf3.setTimeZone(TimeZone.getTimeZone("PST8PDT"));
            } else {
                sdf3.setTimeZone(TimeZone.getTimeZone(var_time_Zone));
            }

            Date d1 = null;
            d1 = sdf3.parse(date);
            sdf3.setTimeZone(TimeZone.getDefault());
            assert d1 != null;
            return sdfout.format(d1);
        } catch (Exception ignored) {
        }
        return "";
    }
}