package com.ldsscriptures.pro.adapter;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.ldsscriptures.pro.R;
import com.ldsscriptures.pro.model.ReadingNowData;
import com.ldsscriptures.pro.utils.Global;
import com.ldsscriptures.pro.utils.SessionManager;

import java.io.File;
import java.util.ArrayList;

public class TabsViewPagerAdapter extends RecyclerView.Adapter<TabsViewPagerAdapter.MyViewHolder> {

    private final SessionManager sessionManager;
    private ArrayList<ReadingNowData> tabsDataArrayList = new ArrayList<>();
    private Activity context;
    private int selectedPos = 0;

    public TabsViewPagerAdapter(Activity context, ArrayList<ReadingNowData> tabsDataArrayList) {
        this.tabsDataArrayList = tabsDataArrayList;
        this.context = context;

        sessionManager = new SessionManager(context);
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        private ImageView imageView;
        private ImageView ivBookmark;


        public MyViewHolder(View view) {
            super(view);
            imageView = (ImageView) view.findViewById(R.id.imageView);
            ivBookmark = (ImageView) view.findViewById(R.id.ivBookmark);
        }
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_tabs_viewpager, parent, false);
        return new MyViewHolder(itemView);
    }

    public void remove(int position) {
        if (position < 0 || position >= tabsDataArrayList.size()) {
            return;
        }
        tabsDataArrayList.remove(position);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        holder.itemView.setSelected(selectedPos == position);
        File imgFile = new File(tabsDataArrayList.get(position).getPath());
        if (imgFile.exists()) {
            Bitmap myBitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath());
            holder.imageView.setImageBitmap(myBitmap);
        }

        if (tabsDataArrayList.get(position).isBookmarked) {
            holder.ivBookmark.setImageResource(R.drawable.ic_bkmrk_on_tabs);
        } else {
            holder.ivBookmark.setImageResource(R.drawable.ic_bkmrk_off_tabs);
        }

        holder.ivBookmark.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ArrayList<ReadingNowData> arrayList = sessionManager.getPreferencesArrayList(context);
                if (arrayList == null) {
                    arrayList = new ArrayList<>();
                }

                if (!tabsDataArrayList.get(position).isBookmarked) {
                    arrayList.get(position).setBookmarked(true);
                    tabsDataArrayList.get(position).setBookmarked(true);
                    holder.ivBookmark.setImageResource(R.drawable.ic_bkmrk_on_tabs);
                } else {
                    tabsDataArrayList.get(position).setBookmarked(false);
                    arrayList.get(position).setBookmarked(false);
                    holder.ivBookmark.setImageResource(R.drawable.ic_bkmrk_off_tabs);
                }
                sessionManager.setPreferencesArrayList(context, arrayList);
            }
        });

        holder.imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    Intent intent = new Intent(context, Class.forName(tabsDataArrayList.get(position).getTag()));
                    switch (tabsDataArrayList.get(position).getActivityIdentifier()) {
                        case "LibCollection":
                            break;
                        case "LibSelection":
                            intent.putExtra(context.getString(R.string.lcID), tabsDataArrayList.get(position).getLibSelection().getLcId());
                            intent.putExtra(context.getString(R.string.lcname), tabsDataArrayList.get(position).getLibSelection().getTitle());
                            break;
                        case "LibIndex":
                            intent.putExtra(context.getString(R.string.lcID), tabsDataArrayList.get(position).getLibIndex().getLcId());
                            intent.putExtra(context.getString(R.string.lsTitle), tabsDataArrayList.get(position).getLibIndex().getTitle());
                            intent.putExtra(context.getString(R.string.lsId), tabsDataArrayList.get(position).getLibIndex().getLsId());
                            break;
                        case "LibSubIndex":
                            intent.putExtra(context.getString(R.string.indexTitle), tabsDataArrayList.get(position).getLibSubIndex().getTitle());
                            intent.putExtra(context.getString(R.string.SubIndexPosition), tabsDataArrayList.get(position).getLibSubIndex().getIndex());
                            intent.putExtra(context.getString(R.string.lcID), tabsDataArrayList.get(position).getLibSubIndex().getLcId());
                            intent.putExtra(context.getString(R.string.lsId), tabsDataArrayList.get(position).getLibSubIndex().getLsId());
                            break;
                        case "Reading":
                            intent.putExtra(context.getString(R.string.indexForReading), tabsDataArrayList.get(position).getReading().getIndex());
                            intent.putExtra(context.getString(R.string.isFromIndex), tabsDataArrayList.get(position).getReading().isFromIndex());
                            intent.putExtra(context.getString(R.string.is_history), tabsDataArrayList.get(position).getReading().isHistory());
                            intent.putExtra(context.getString(R.string.lcID), tabsDataArrayList.get(position).getReading().getLcId());
                            intent.putExtra(context.getString(R.string.lsId), tabsDataArrayList.get(position).getReading().getLsId());
                            break;
                    }
                    context.startActivity(intent);
                    context.overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
                    context.finish();

                } catch (ClassNotFoundException e) {
                    e.printStackTrace();
                }
            }
        });

    }

    @Override
    public int getItemCount() {
        return tabsDataArrayList.size();
    }
}
