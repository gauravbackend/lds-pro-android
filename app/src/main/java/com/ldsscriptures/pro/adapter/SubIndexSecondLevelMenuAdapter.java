package com.ldsscriptures.pro.adapter;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.ldsscriptures.pro.R;
import com.ldsscriptures.pro.activity.LibraryIndexActivity;
import com.ldsscriptures.pro.activity.SubIndexActivity;
import com.ldsscriptures.pro.model.LsMainIndexData;
import com.ldsscriptures.pro.utils.Global;

import java.util.ArrayList;


public class SubIndexSecondLevelMenuAdapter extends RecyclerView.Adapter<SubIndexSecondLevelMenuAdapter.MyViewHolder> {

    private Activity activity;
    private ArrayList<LsMainIndexData> lsDataArrayList = new ArrayList<>();
    String lcId, lsId;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView textView;
        public LinearLayout linDropDown;

        public MyViewHolder(View view) {
            super(view);
            textView = (TextView) view.findViewById(R.id.textView);
            linDropDown = (LinearLayout) view.findViewById(R.id.linDropDown);
        }
    }

    public SubIndexSecondLevelMenuAdapter(Activity activity, ArrayList<LsMainIndexData> lsDataArrayList, String lcId, String lsId) {
        this.lsDataArrayList = lsDataArrayList;
        this.activity = activity;
        this.lcId = lcId;
        this.lsId = lsId;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, final int position) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_dropdown_menu, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {

        holder.textView.setTextColor(activity.getResources().getColor(R.color.drop_down_second_level_text));
        holder.textView.setText(lsDataArrayList.get(position).getTitle_html());

        holder.linDropDown.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(activity, SubIndexActivity.class);
                intent.putExtra(activity.getString(R.string.indexTitle), lsDataArrayList.get(position).getTitle_html());
                intent.putExtra(activity.getString(R.string.SubIndexPosition), lsDataArrayList.get(position).getId());
                intent.putExtra(activity.getString(R.string.lcID), lcId);
                intent.putExtra(activity.getString(R.string.lsId), lsId);

                activity.startActivity(intent);
                activity.overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
                activity.finish();
            }
        });
    }

    @Override
    public int getItemCount() {
        return lsDataArrayList.size();
    }
}