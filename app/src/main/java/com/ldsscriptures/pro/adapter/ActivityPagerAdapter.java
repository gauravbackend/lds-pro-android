package com.ldsscriptures.pro.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.ldsscriptures.pro.fragment.ActivityDayPagerTabFragment;
import com.ldsscriptures.pro.fragment.ActivityMonthPagerTabFragment;
import com.ldsscriptures.pro.fragment.ActivityWeekPagerTabFragment;
import com.ldsscriptures.pro.fragment.ActivityYearPagerTabFragment;

public class ActivityPagerAdapter extends FragmentStatePagerAdapter {

    int mNumOfTabs;

    public ActivityPagerAdapter(FragmentManager fm, int NumOfTabs) {
        super(fm);
        this.mNumOfTabs = NumOfTabs;
    }

    @Override
    public Fragment getItem(int position) {

        switch (position) {
            case 0:
                return new ActivityDayPagerTabFragment();

            case 1:
                return new ActivityWeekPagerTabFragment();

            case 2:
                return new ActivityMonthPagerTabFragment();

            case 3:
                return new ActivityYearPagerTabFragment();

            default:
                return null;
        }

    }

    @Override
    public int getCount() {
        return mNumOfTabs;
    }

    @Override
    public int getItemPosition(Object object) {
        return POSITION_NONE;
    }


}
