package com.ldsscriptures.pro.adapter;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.ldsscriptures.pro.R;
import com.ldsscriptures.pro.activity.ReadingActivity;
import com.ldsscriptures.pro.model.LsIndexPreviewData;
import com.ldsscriptures.pro.model.MoreOptionMenuData;
import com.ldsscriptures.pro.utils.Global;

import java.util.ArrayList;


public class SubIndexAdapter extends RecyclerView.Adapter<SubIndexAdapter.MyViewHolder> {

    public static ArrayList<LsIndexPreviewData> indexList = new ArrayList<>();
    private final String lcId;
    private final String lsId;
    private Activity activity;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView tvIndexItem, tvReview;
        public LinearLayout llMain;

        public MyViewHolder(View view) {
            super(view);
            tvIndexItem = (TextView) view.findViewById(R.id.tvVerse);
            tvReview = (TextView) view.findViewById(R.id.tvReview);
            llMain = (LinearLayout) view.findViewById(R.id.llMain);
        }
    }


    public SubIndexAdapter(Activity activity, ArrayList<LsIndexPreviewData> indexList, String lcId, String lsId) {
        this.indexList = indexList;
        this.activity = activity;
        this.lcId = lcId;
        this.lsId = lsId;

    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, final int position) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_sub_index, parent, false);

        itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            }
        });

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        holder.tvIndexItem.setText(indexList.get(position).getHtml_title());
        holder.tvReview.setText(indexList.get(position).getPreview());

        holder.llMain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                // set parameter for more option
                Global.moreOptionMenuDatas.set(3, new MoreOptionMenuData(lcId, lsId, indexList.get(position).getId(), false, true, position));

                // store title for store into bookmark.
                Global.bookmarkData.setTitle(indexList.get(position).getHtml_title());

                Intent intent = new Intent(activity, ReadingActivity.class);
                intent.putExtra(activity.getString(R.string.indexForReading), indexList.get(position).getId());
                intent.putExtra(activity.getString(R.string.position), position);
                intent.putExtra(activity.getString(R.string.isFromIndex), true);
                intent.putExtra(activity.getString(R.string.is_history), false);
                intent.putExtra(activity.getString(R.string.lcID), lcId);
                intent.putExtra(activity.getString(R.string.lsId), lsId);

                activity.startActivity(intent);
                activity.overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
            }
        });
    }

    @Override
    public int getItemCount() {
        return indexList.size();
    }
}