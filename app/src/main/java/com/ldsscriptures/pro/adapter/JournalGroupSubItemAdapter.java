package com.ldsscriptures.pro.adapter;

import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.daimajia.swipe.SwipeLayout;
import com.daimajia.swipe.adapters.RecyclerSwipeAdapter;
import com.ldsscriptures.pro.R;
import com.ldsscriptures.pro.activity.studytools.AddJournalsActivity;
import com.ldsscriptures.pro.activity.studytools.JournalsActivity;
import com.ldsscriptures.pro.database.CommonDatabaseHelper;
import com.ldsscriptures.pro.model.JournalData;
import com.ldsscriptures.pro.model.JournalItemData;
import com.ldsscriptures.pro.utils.PushToServerData;
import com.ldsscriptures.pro.utils.SessionManager;

import java.util.ArrayList;

import static com.ldsscriptures.pro.R.id;

public class JournalGroupSubItemAdapter extends RecyclerSwipeAdapter<JournalGroupSubItemAdapter.ViewHolder> {

    private Context context;
    private ArrayList<JournalItemData> journalItemDataArrayList = new ArrayList<JournalItemData>();
    CommonDatabaseHelper commonDatabaseHelper;
    SessionManager sessionManager;

    public JournalGroupSubItemAdapter(Context context, ArrayList<JournalItemData> journalItemDataArrayList) {
        this.context = context;
        this.journalItemDataArrayList = journalItemDataArrayList;
        commonDatabaseHelper = new CommonDatabaseHelper(context);
        sessionManager = new SessionManager(context);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View cardView = inflater.inflate(R.layout.item_journal_group_sub_item, null, false);
        ViewHolder viewHolder = new ViewHolder(cardView);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {

        if (journalItemDataArrayList.get(position).getJournaltitle() != null) {
            holder.txtContent.setText(journalItemDataArrayList.get(position).getJournaltitle());
        }

        holder.swipeLayout.setShowMode(SwipeLayout.ShowMode.PullOut);
        // Drag From Right
        holder.swipeLayout.addDrag(SwipeLayout.DragEdge.Right, holder.swipeLayout.findViewById(id.bottom_wrapper_jounal));
        // Handling different events when swiping
        holder.swipeLayout.addSwipeListener(new SwipeLayout.SwipeListener() {
            @Override
            public void onClose(SwipeLayout layout) {
                //when the SurfaceView totally cover the BottomView.
            }

            @Override
            public void onUpdate(SwipeLayout layout, int leftOffset, int topOffset) {
                //you are swiping.
            }

            @Override
            public void onStartOpen(SwipeLayout layout) {

            }

            @Override
            public void onOpen(SwipeLayout layout) {
                //when the BottomView totally show.
            }

            @Override
            public void onStartClose(SwipeLayout layout) {

            }

            @Override
            public void onHandRelease(SwipeLayout layout, float xvel, float yvel) {
                //when user's hand released.
            }
        });

        holder.tvDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                final String Journaltitle = journalItemDataArrayList.get(position).getJournaltitle().toString().trim();
                final String Journaltext = journalItemDataArrayList.get(position).getJournaltext().toString().trim();
                final String serverid = journalItemDataArrayList.get(position).getServerid().toString().trim();
                final int mainid = journalItemDataArrayList.get(position).getId();

                try {
                    android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(context);
                    builder.setTitle("Warning");
                    builder.setMessage(R.string.are_you_sure_delete)

                            .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            })
                            .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    // Yes button clicked, do something
                                    dialog.dismiss();
                                    deleteLocally(holder, position, journalItemDataArrayList, mainid);
                                    new PushToServerData(context);
                                }
                            });

                    builder.show();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        holder.layoutSubJournal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent tagIntent = new Intent(context, AddJournalsActivity.class);
                tagIntent.putExtra("Journaltitle", journalItemDataArrayList.get(position).getJournaltitle());
                tagIntent.putExtra("Journaltext", journalItemDataArrayList.get(position).getJournaltext());
                tagIntent.putExtra("Journalid", journalItemDataArrayList.get(position).getId());
                context.startActivity(tagIntent);
            }
        });
    }

    @Override
    public int getItemCount() {
        if (journalItemDataArrayList != null)
            return journalItemDataArrayList.size();
        else return 0;
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    @Override
    public int getSwipeLayoutResourceId(int position) {
        return R.id.jounalswipe;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        private TextView txtContent;
        SwipeLayout swipeLayout;
        TextView tvDelete;
        RelativeLayout layoutSubJournal;

        public ViewHolder(View itemView) {
            super(itemView);
            txtContent = (TextView) itemView.findViewById(id.txtContent);
            swipeLayout = (SwipeLayout) itemView.findViewById(id.jounalswipe);
            tvDelete = (TextView) itemView.findViewById(id.tvDelete_jounal);
            layoutSubJournal = (RelativeLayout) itemView.findViewById(id.layoutSubJournal);
        }
    }

    public void deleteLocally(final ViewHolder holder, final int position, final ArrayList<JournalItemData> journalItemDataArrayList, final int mainid) {
        mItemManger.removeShownLayouts(holder.swipeLayout);
        journalItemDataArrayList.remove(position);
        notifyItemRemoved(position);
        notifyItemRangeChanged(position, journalItemDataArrayList.size());
        mItemManger.closeAllItems();

        /*commonDatabaseHelper.deleteRowData(CommonDatabaseHelper.TABLE_JOURNAL, commonDatabaseHelper.KEY_ID + " = ?",
                new String[]{String.valueOf(mainid)});*/

        ContentValues contentValues = new ContentValues();
        contentValues.put(CommonDatabaseHelper.KEY_DELETED, "1");
        commonDatabaseHelper.updateRowData(CommonDatabaseHelper.TABLE_JOURNAL, contentValues, CommonDatabaseHelper.KEY_ID + " = ?",
                new String[]{String.valueOf(mainid)});

        if (journalItemDataArrayList.size() == 0) {
            ArrayList<JournalData> journalDataArrayList = JournalsActivity.getInstance().journalArraylistMethod();
            for (int i = 0; i < journalDataArrayList.size(); i++) {
                if (journalDataArrayList.get(i).getJournalItemDataArrayList().size() == 0) {
                    journalDataArrayList.remove(i);
                }
            }
            if (journalDataArrayList.size() == 0) {
                JournalsActivity.getInstance().visibleHideView();
            }
        }

    }
}
