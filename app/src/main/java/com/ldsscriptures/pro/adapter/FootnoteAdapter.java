package com.ldsscriptures.pro.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Typeface;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.text.style.URLSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.ldsscriptures.pro.R;
import com.ldsscriptures.pro.activity.ReadingActivity;
import com.ldsscriptures.pro.database.DataBaseHelper;
import com.ldsscriptures.pro.database.SubDataBaseHelper;
import com.ldsscriptures.pro.model.FootNotesData;
import com.ldsscriptures.pro.model.IndexWrapper;
import com.ldsscriptures.pro.model.TimeLine.VerseDataTimeLineModel;
import com.ldsscriptures.pro.utils.Global;
import com.ldsscriptures.pro.utils.SessionManager;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

public class FootnoteAdapter extends RecyclerView.Adapter<FootnoteAdapter.ViewHolder> {

    private Context con;
    private Activity act;
    private ArrayList<FootNotesData> footNotesDatas = new ArrayList<>();
    private SessionManager sessionManager;

    public FootnoteAdapter(Context con, Activity act, ArrayList<FootNotesData> footNotesDatas) {
        this.con = con;
        this.act = act;
        this.footNotesDatas = footNotesDatas;
        sessionManager = new SessionManager(con);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(act).inflate(R.layout.item_footnote, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {

        Typeface typeface;
        int fontSize = 0;
        if (sessionManager.getFontStyle() != null && sessionManager.getFontStyle().length() != 0) {
            typeface = Typeface.createFromAsset(con.getAssets(), sessionManager.getFontStyle());
        } else {
            typeface = Typeface.createFromAsset(con.getAssets(), con.getResources().getString(R.string.font_fira_sans));
        }
        if (sessionManager.getFontSize() != 0) {
            fontSize = sessionManager.getFontSize();

        } else {
            fontSize = 16;
        }

        holder.tvLetter.setText(Html.fromHtml(footNotesDatas.get(position).getLabel_html()).toString());
        holder.tvLetter.setTypeface(typeface);
        holder.tvLetter.setTextSize(fontSize);
        holder.tvFootnote.setTypeface(typeface);
        holder.tvFootnote.setTextSize(fontSize);

        String newHtmlUri = footNotesDatas.get(position).getContent_html().toString();
        holder.tvFootnote.setText(Global.noTrailingwhiteLines(Html.fromHtml(newHtmlUri)));
        holder.tvFootnote.setMovementMethod(LinkMovementMethod.getInstance());

        CharSequence text = holder.tvFootnote.getText();
        if (text instanceof Spannable) {
            int end = text.length();
            Spannable sp = (Spannable) holder.tvFootnote.getText();
            URLSpan[] urls = sp.getSpans(0, end, URLSpan.class);
            SpannableStringBuilder style = new SpannableStringBuilder(text);
            style.clearSpans();//should clear old spans
            for (URLSpan url : urls) {
                try {
                    CustomerTextClick click = new CustomerTextClick(url.getURL(), act);
                    style.setSpan(click, sp.getSpanStart(url), sp.getSpanEnd(url), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                } catch (Exception e) {

                }
            }
            holder.tvFootnote.setText(style);
        }
    }

    @Override
    public int getItemCount() {
        return footNotesDatas.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView tvFootnote, tvLetter;
        LinearLayout relMain;

        public ViewHolder(View itemView) {
            super(itemView);
            relMain = (LinearLayout) itemView.findViewById(R.id.relMain);
            tvFootnote = (TextView) itemView.findViewById(R.id.tvFootnote);
            tvLetter = (TextView) itemView.findViewById(R.id.tvLetter);
        }
    }

    public class CustomerTextClick extends ClickableSpan {

        private String mUrl;
        private Context con;
        DataBaseHelper databasehelper;
        VerseDataTimeLineModel verseDataTimeLineModel;

        CustomerTextClick(String url, Context con) {
            this.con = con;
            mUrl = url;
            databasehelper = new DataBaseHelper(con);
        }

        @Override
        public void onClick(View widget) {
            redirectOnReadingActivity(mUrl);
        }

        public void redirectOnReadingActivity(String url) {

            String verseId = "0";
            String[] split = url.split("gospellibrary://content");
            String firstSubString = split[1];
            String[] fullVerseUri1 = firstSubString.split("\\?|\\#");
            String fullVerseUri = fullVerseUri1[0];
            Uri uri = Uri.parse(firstSubString);
            Set<String> arrayList = uri.getQueryParameterNames();
            List<String> list = new ArrayList<String>(arrayList);

            for (int i = 0; i < list.size(); i++) {
                if (list.get(i).equals("verse")) {
                    verseId = uri.getQueryParameter("verse");
                } else {
                    verseId = "0";
                }
            }

            if (ReadingActivity.getInstance() != null) {

                verseDataTimeLineModel = new VerseDataTimeLineModel();
                verseDataTimeLineModel = getVerseText(fullVerseUri, verseId);

                final String verseLcId = verseDataTimeLineModel.getLcId();
                final String verseLsId = verseDataTimeLineModel.getLsId();
                final String indexForReading = verseDataTimeLineModel.getIndexForReading();
                String verseAid = verseDataTimeLineModel.getVerseAid();

                if (verseAid == null) {
                    verseAid = "0";
                }

                redirectReadingActivity(verseLcId, verseLsId, indexForReading, verseAid, verseId);
            }
        }

        public VerseDataTimeLineModel getVerseText(String fullVerseUri, String var_timeline_Verse_Id) {

            Cursor cursor;
            String LcId = "";
            try {

                String[] URI_TXT = fullVerseUri.split("\\/");
                String URI_TXT_First = URI_TXT[1];
                String URI_TXT_Second = URI_TXT[2];

                cursor = databasehelper.getVerseTextAndVersePath(URI_TXT_First);
                if (cursor != null && cursor.moveToFirst()) {

                    while (!cursor.isAfterLast()) {
                        LcId = cursor.getString(cursor.getColumnIndex(DataBaseHelper.KEY_ID));
                        cursor.moveToNext();
                    }
                }
                if (cursor != null) {
                    cursor.close();
                }
                String URI_TXT_First1 = URI_TXT_First.substring(0, 1).toUpperCase() + URI_TXT_First.substring(1);
                verseDataTimeLineModel = getLsDataFromDb(LcId, URI_TXT_Second, fullVerseUri, var_timeline_Verse_Id, URI_TXT_First1);
                verseDataTimeLineModel.setLcId(LcId);

            } catch (Exception e) {
                e.printStackTrace();
            }
            return verseDataTimeLineModel;
        }

        public VerseDataTimeLineModel getLsDataFromDb(String LcId, String verseUri, String fullVerseUri, String var_timeline_Verse_Id, String URI_TXT_First) {

            Cursor cursor;
            String LsItemId = "";
            String LsItemTitle = "";
            if (LcId.isEmpty()) {
                int index = fullVerseUri.lastIndexOf('/');
                cursor = databasehelper.getVerseLSPathEmpty(fullVerseUri.substring(0, index));
            } else {
                cursor = databasehelper.getVerseLSPath(Integer.parseInt(LcId), verseUri);
            }

            if (cursor != null && cursor.moveToFirst()) {
                while (!cursor.isAfterLast()) {
                    LsItemId = cursor.getString(cursor.getColumnIndex(DataBaseHelper.KEY_LS_RESULT_ITEM_ID));
                    LsItemTitle = cursor.getString(cursor.getColumnIndex(DataBaseHelper.KEY_LS_RESULT_TITLE_HTML));

                    URI_TXT_First = URI_TXT_First + " / " + LsItemTitle;
                    verseDataTimeLineModel = getVerseIndexDataFromDB(LsItemId, fullVerseUri, var_timeline_Verse_Id, URI_TXT_First);
                    verseDataTimeLineModel.setLsId(LsItemId);

                    cursor.moveToNext();
                }
            }
            return verseDataTimeLineModel;
        }

        public VerseDataTimeLineModel getVerseIndexDataFromDB(String LsItemId, String fullVerseUri, String var_timeline_Verse_Id, String URI_TXT_First) {
            SubDataBaseHelper subDataBaseHelper;
            subDataBaseHelper = new SubDataBaseHelper(con, LsItemId);

            Cursor cursor;
            cursor = subDataBaseHelper.getVerseTextFromURI(fullVerseUri);

            if (cursor != null && cursor.moveToFirst()) {
                while (!cursor.isAfterLast()) {

                    String LsHrmlTextItem = cursor.getString(cursor.getColumnIndex(DataBaseHelper.KEY_LS_RESULT_TITLE_HTML));
                    byte[] blob = cursor.getBlob(cursor.getColumnIndex(SubDataBaseHelper.KEY_SUB_ITEM_HTML_C1CONTENT_HTML));
                    String idForReading = cursor.getString(cursor.getColumnIndex(SubDataBaseHelper.KEY_ID));
                    String postionForReading = cursor.getString(cursor.getColumnIndex(SubDataBaseHelper.KEY_FT_POSITION));

                    String newHtmlVerseArray = new String(blob);

                    if (var_timeline_Verse_Id != "0") {
                        verseDataTimeLineModel = getCrossRefSubItemHtmlData(newHtmlVerseArray, var_timeline_Verse_Id);
                    }

                    verseDataTimeLineModel.setVersePath(LsHrmlTextItem);
                    verseDataTimeLineModel.setVerseFullPath(URI_TXT_First + " / " + LsHrmlTextItem);
                    verseDataTimeLineModel.setIndexForReading(idForReading);

                    cursor.moveToNext();
                }
            }
            subDataBaseHelper.close();
            if (cursor != null) {
                cursor.close();
            }
            return verseDataTimeLineModel;
        }

        public VerseDataTimeLineModel getCrossRefSubItemHtmlData(String newHtmlVerseArray, String verse_Id) {
            String verseText = "";
            ArrayList<String> selectedArrayList = new ArrayList<>();
            Document doc = Jsoup.parse(newHtmlVerseArray);
            Elements tvVersemain = doc.select("div[class^=body-block]");
            Elements tvVerse = tvVersemain.select("p[class^=verse]");

            if (!tvVerse.isEmpty() && tvVerse != null && !tvVerse.equals("null")) {
                Element element2 = tvVerse.get(Integer.parseInt(verse_Id) - 1);
                Elements tvVerseSpan = element2.select("span[class^=study-note-ref]");
                for (int j = 0; j < tvVerseSpan.size(); j++) {
                    String e2 = tvVerseSpan.get(j).text();
                    selectedArrayList.add(e2);
                }

                List<IndexWrapper> indexWrapperList = Global.findIndexesForKeyword(element2.outerHtml(), "data-aid");
                String[] dataAid = new String[0];
                for (int k = 0; k < indexWrapperList.size(); k++) {
                    String substring = element2.outerHtml().substring(indexWrapperList.get(k).getEnd() + 2);
                    dataAid = substring.split("\"");
                }
                verseText = Html.fromHtml(element2.outerHtml()).toString();
                for (int i = 0; i < selectedArrayList.size(); i++) {
                    verseText = verseText.replace(selectedArrayList.get(i), selectedArrayList.get(i).substring(1));
                }
                verseText = verseText.replace(verseText, verseText.substring(verse_Id.length()).trim());
                verseDataTimeLineModel.setVereseText(verseText);
                verseDataTimeLineModel.setVerseAid(dataAid[0]);


            } else {

                Elements tvVerse1 = tvVersemain.select("p");

                for (int i = 0; i < tvVerse1.size(); i++) {

                    String pid = tvVerse1.get(i).select("p").attr("id");
                    verseText = tvVerse1.get(i).outerHtml().toString();
                    String verseAid = tvVerse1.get(i).attr("data-aid");
                    String verseID = "";

                    if (pid.startsWith("p")) {
                        verseID = pid.substring(1);
                    }

                    if (verseID.equals(verse_Id)) {

                        verseDataTimeLineModel.setVereseText(String.valueOf(Html.fromHtml(verseText)));
                        verseDataTimeLineModel.setVerseAid(verseAid);
                    }

                }

            }

            return verseDataTimeLineModel;
        }

        public void redirectReadingActivity(String verseLcId, String verseLsId, String indexForReading, String verseAid, String verseId) {
            if (verseLcId != null || verseLsId != null || indexForReading != null) {

                Intent intent = new Intent(con, ReadingActivity.class);
                intent.putExtra(con.getString(R.string.indexForReading), Integer.parseInt(indexForReading));
                intent.putExtra(con.getString(R.string.isFromIndex), true);
                intent.putExtra(con.getString(R.string.is_history), false);
                intent.putExtra(con.getString(R.string.lcID), verseLcId);
                intent.putExtra(con.getString(R.string.lsId), verseLsId);
                intent.putExtra(con.getString(R.string.verseAId), Integer.parseInt(verseAid));
                intent.putExtra(con.getString(R.string.verseId), Integer.parseInt(verseId));

                con.startActivity(intent);
                Activity activity = (Activity) con;
                activity.overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
            }
        }
    }
}
