package com.ldsscriptures.pro.adapter;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.TextUtils;
import android.text.method.LinkMovementMethod;
import android.text.style.AbsoluteSizeSpan;
import android.text.style.BackgroundColorSpan;
import android.text.style.ClickableSpan;
import android.text.style.ImageSpan;
import android.text.style.TextAppearanceSpan;
import android.text.style.UnderlineSpan;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.ldsscriptures.pro.R;
import com.ldsscriptures.pro.activity.ReadingActivity;
import com.ldsscriptures.pro.activity.verses.VerseHighlightActivity;
import com.ldsscriptures.pro.database.CommonDatabaseHelper;
import com.ldsscriptures.pro.database.HighlightDatabaseHelper;
import com.ldsscriptures.pro.database.SubDataBaseHelper;
import com.ldsscriptures.pro.dbClass.GetDataFromDatabase;
import com.ldsscriptures.pro.dbClass.GetFootnotesFromDB;
import com.ldsscriptures.pro.model.BookmarkData;
import com.ldsscriptures.pro.model.IndexWrapper;
import com.ldsscriptures.pro.model.SpannedTextData;
import com.ldsscriptures.pro.model.VerseCrossReferenceData;
import com.ldsscriptures.pro.model.VerseData;
import com.ldsscriptures.pro.model.VerseTagsData;
import com.ldsscriptures.pro.service.ActivityEventReceiver;
import com.ldsscriptures.pro.utils.Global;
import com.ldsscriptures.pro.utils.SessionManager;
import com.ldsscriptures.pro.view.ColoredUnderlineSpan;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ReadingStepRecyclerAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private final SessionManager sessionManager;
    private final String lsId;
    private final SubDataBaseHelper subDatabaseHelper;
    private String headerContent;
    private Activity activity;
    public ArrayList<VerseData> verseData = new ArrayList<>();
    private GestureDetector gd;
    private int mPosition;
    private int indexID;
    LinearLayout linScroll;
    private String headerText = "";
    RecyclerView recyclerView;
    EditText edSearch;
    private ArrayList<BookmarkData> bookmarkDataArrayList;
    private ArrayList<BookmarkData> notesArrayList;
    private ArrayList<VerseTagsData> tagDataArrayList;
    private String newVerse;
    private String note;
    private String stringTag, StringBookmark;
    private String crossRefText = "";
    HighlightDatabaseHelper Highlightdb;
    private CommonDatabaseHelper commonDatabaseHelper;
    private int verseAId;
    private String title;
    private HashMap citationHashMap;
    private String indexURI;
    private String verseIdArrayStrig;

    public ReadingStepRecyclerAdapter(Activity activity, ArrayList<VerseData> verseData, LinearLayout linScroll, String headerText, String headerContent,
                                      int indexID, RecyclerView recyclerView, EditText edSearch, String lsId, int verseAId, String title, HashMap citationHashMap, String indexURI, String verseIdArrayStrig) {
        this.activity = activity;
        this.verseData = verseData;
        this.linScroll = linScroll;
        this.headerContent = headerContent;
        this.headerText = headerText;
        this.indexID = indexID;
        this.recyclerView = recyclerView;
        this.edSearch = edSearch;
        sessionManager = new SessionManager(activity);
        commonDatabaseHelper = new CommonDatabaseHelper(activity);
        subDatabaseHelper = new SubDataBaseHelper(activity, lsId);
        Highlightdb = new HighlightDatabaseHelper(this.activity);
        this.lsId = lsId;
        this.verseAId = verseAId;
        notesArrayList = new ArrayList<>();
        bookmarkDataArrayList = new ArrayList<>();
        tagDataArrayList = new ArrayList<>();
        this.title = title;
        this.citationHashMap = citationHashMap;
        this.indexURI = indexURI;
        this.verseIdArrayStrig = verseIdArrayStrig;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        if (viewType == 0) {
            View headerRow = LayoutInflater.from(activity).inflate(R.layout.item_reading_header, null);
            return new ViewHolderHeader(headerRow); // view1 holder for normal items
        } else {
            View normalView = LayoutInflater.from(activity).inflate(R.layout.item_reading_recycler, null);
            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            layoutParams.setMargins(0, 5, 0, 5);
            normalView.setLayoutParams(layoutParams);
            return new ViewHolderRow(normalView); // view1 holder for header items
        }
    }

    private boolean isBookmarked(String data_aid) {

        boolean b = false;
        StringBookmark = "";
        bookmarkDataArrayList = getBookmarks(data_aid);

        if (bookmarkDataArrayList != null && bookmarkDataArrayList.size() > 0) {
            for (int i = 0; i < bookmarkDataArrayList.size(); i++) {
                for (int j = 0; j < bookmarkDataArrayList.size(); j++) {
                    if (data_aid.equalsIgnoreCase(bookmarkDataArrayList.get(i).getVerseaid())) {
                        String isDeleted = bookmarkDataArrayList.get(i).getDeleted();
                        if (isDeleted.isEmpty()) {
                            StringBookmark = bookmarkDataArrayList.get(i).getTitle();
                            b = true;
                        }
                    }
                }
            }
        }

        return b;

    }

    private boolean isTagged(String aId) {
        boolean b = false;
        stringTag = "";
        tagDataArrayList = sessionManager.getPreferencesTagsArrayList(activity);
        if (tagDataArrayList != null && tagDataArrayList.size() > 0) {
            for (int i = 0; i < tagDataArrayList.size(); i++) {
                if (aId.equalsIgnoreCase(tagDataArrayList.get(i).getVerseaid())) {
                    String isDeleted = tagDataArrayList.get(i).getDeleted();
                    if (isDeleted == null) {
                        isDeleted = "";
                    }
                    if (isDeleted.equalsIgnoreCase("")) {
                        if (stringTag.length() != 0) {
                            stringTag = stringTag + ", " + tagDataArrayList.get(i).getTagname();
                        } else {
                            stringTag = tagDataArrayList.get(i).getTagname();
                        }
                        b = true;
                    }
                }
            }
        }

        return b;
    }

    private boolean isNoteAdded(String data_aid) {
        notesArrayList = getNotes(data_aid);
        if (notesArrayList != null && notesArrayList.size() > 0) {
            for (int i = 0; i < notesArrayList.size(); i++) {
                if (data_aid.equalsIgnoreCase(notesArrayList.get(i).getVerseaid())) {
                    String isDeleted = notesArrayList.get(i).getDeleted();
                    if (isDeleted.isEmpty()) {
                        note = notesArrayList.get(i).getTitle();
                        return true;
                    }
                }
            }
        }
        return false;
    }

    private boolean isCrossReferenceAdded(String aId) {

        boolean b = false;
        ArrayList<VerseCrossReferenceData> verseCrossReferenceDataArrayList = sessionManager.getPreferencesCrossRefArrayList(activity);

        crossRefText = "";
        if (verseCrossReferenceDataArrayList != null && verseCrossReferenceDataArrayList.size() > 0) {
            for (int i = 0; i < verseCrossReferenceDataArrayList.size(); i++) {

                if (aId.equalsIgnoreCase(verseCrossReferenceDataArrayList.get(i).getRefAid1())) {
                    if (crossRefText.length() != 0) {
                        crossRefText = crossRefText + ", " + new GetDataFromDatabase(activity, lsId).getTitleOnUrl(verseCrossReferenceDataArrayList.get(i).getRefTitle2()) + ":" + verseCrossReferenceDataArrayList.get(i).getRefIndex2();
                    } else {
                        crossRefText = new GetDataFromDatabase(activity, lsId).getTitleOnUrl(verseCrossReferenceDataArrayList.get(i).getRefTitle2()) + ":" + verseCrossReferenceDataArrayList.get(i).getRefIndex2();
                    }
                    b = true;
                }

                if (aId.equalsIgnoreCase(verseCrossReferenceDataArrayList.get(i).getRefAid2())) {
                    if (crossRefText.length() != 0) {
                        crossRefText = crossRefText + ", " + new GetDataFromDatabase(activity, lsId).getTitleOnUrl(verseCrossReferenceDataArrayList.get(i).getRefTitle1()) + ":" + verseCrossReferenceDataArrayList.get(i).getRefIndex1();

                    } else {
                        crossRefText = crossRefText + new GetDataFromDatabase(activity, lsId).getTitleOnUrl(verseCrossReferenceDataArrayList.get(i).getRefTitle1()) + ":" + verseCrossReferenceDataArrayList.get(i).getRefIndex1();
                    }

                    b = true;
                }
            }
        }
        return b;
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder holder, final int position) {
        final int viewType = getItemViewType(position);
        Typeface typeface;
        int fontSize = 0;
        if (sessionManager.getFontStyle() != null && sessionManager.getFontStyle().length() != 0) {
            typeface = Typeface.createFromAsset(activity.getAssets(), sessionManager.getFontStyle());
        } else {
            typeface = Typeface.createFromAsset(activity.getAssets(), activity.getResources().getString(R.string.font_fira_sans));
        }
        if (sessionManager.getFontSize() != 0) {
            fontSize = sessionManager.getFontSize();
        } else {
            fontSize = 16;
        }

        if (viewType == 0) {
            if (!headerContent.isEmpty() && headerContent != null && !headerContent.equals("null")) {
                ((ViewHolderHeader) holder).tvReadingPreview.setText(headerContent);
                ((ViewHolderHeader) holder).tvReadingPreview.setTypeface(typeface);
                ((ViewHolderHeader) holder).tvReadingPreview.setTextSize(fontSize);
                ((ViewHolderHeader) holder).relMainContent.setOnClickListener(new DoubleClickListener() {
                    @Override
                    public void onSingleClick(View v) {
                        // Toast.makeText(activity, "TOAST", Toast.LENGTH_LONG).show();
                    }

                    @Override
                    public void onDoubleClick(View v) {
                        //Toast.makeText(activity, "Double_TOAST", Toast.LENGTH_LONG).show();
                        audioViewHideShow();

                    }
                });
            } else {
                ((ViewHolderHeader) holder).relMainContent.setLayoutParams(new LinearLayout.LayoutParams(-1, 1));
            }

        } else {

            if (verseData.get(position).getLayoutflag() == 1) {

                String myStringNew = "";

                if (verseData.get(position).getStringSentense() != null && verseData.get(position).getId() != null)
                    myStringNew = verseData.get(position).getStringSentense().replace(verseData.get(position).getId(), "");


                if (Integer.parseInt(verseData.get(position).getData_aid()) == verseAId) {
                    ((ViewHolderRow) holder).main_readingstep_Layout.setBackgroundResource(R.color.textbackcolor);
                } else {
                    ((ViewHolderRow) holder).main_readingstep_Layout.setBackgroundResource(0);
                }

                if (!verseIdArrayStrig.equals("")) {

                    String[] allIdsArray = TextUtils.split(verseIdArrayStrig.toString().trim(), ",");
                    ArrayList<String> idsList = new ArrayList<String>(Arrays.asList(allIdsArray));

                    String verseIDNew = "";

                    for (int i = 0; i < idsList.size(); i++) {

                        if (idsList.get(i).contains("-")) {

                            String[] allIdsArray1 = TextUtils.split(idsList.get(i).toString().trim(), "-");

                            int start = Integer.parseInt(allIdsArray1[0]);
                            int end = Integer.parseInt(allIdsArray1[1]);
                            verseIDNew = verseIDNew + "," + getAllNumbersBetween(start, end);

                        } else {
                            verseIDNew = verseIDNew + "," + idsList.get(i);
                        }
                    }

                    verseIDNew = verseIDNew.startsWith(",") ? verseIDNew.substring(1) : verseIDNew;

                    String[] allIdsArraynew = TextUtils.split(verseIDNew.toString().trim(), ",");
                    ArrayList<String> idsListnew = new ArrayList<String>(Arrays.asList(allIdsArraynew));

                    for (int i = 0; i < idsListnew.size(); i++) {
                        if (verseData.get(position).getId().equals(idsListnew.get(i).toString().trim())) {
                            ((ViewHolderRow) holder).main_readingstep_Layout.setBackgroundResource(R.color.textbackcolor);
                        }
                    }
                }

                SpannableStringBuilder text = new SpannableStringBuilder(Global.noTrailingwhiteLines(Html.fromHtml(myStringNew)));

                if (text != null && !text.toString().isEmpty() && !text.equals("")) {

                    ((ViewHolderRow) holder).inner_layout_verse.setVisibility(View.VISIBLE);
                    ((ViewHolderRow) holder).inner_layout_video.setVisibility(View.GONE);
                    ((ViewHolderRow) holder).inner_layout_image.setVisibility(View.GONE);

                    if (!verseData.get(position).getId().equals("")) {
                        ((ViewHolderRow) holder).tvVerseId.setText(verseData.get(position).getId());
                        ((ViewHolderRow) holder).tvVerseId.setTypeface(typeface);
                        ((ViewHolderRow) holder).tvVerseId.setTextSize(fontSize);
                    }

                    ((ViewHolderRow) holder).tvVerse.setTypeface(typeface);
                    ((ViewHolderRow) holder).tvVerse.setTextSize(fontSize);

                    ((ViewHolderRow) holder).txtCitation_CountLayout.setVisibility(View.GONE);

                    final String[] temp = {text.toString()};

                    if (verseData.get(position).getFootnoteArray() != null) {

                        for (int i = 0; i < verseData.get(position).getFootnoteArray().size(); i++) {
                            List<IndexWrapper> indexWrapperList2 = Global.findIndexesForKeyword(text.toString(), verseData.get(position).getFootnoteArray().get(i));
                            for (int j = 0; j < indexWrapperList2.size(); j++) {
                                text.setSpan(new MyClickableSpan(), indexWrapperList2.get(j).getStart(), indexWrapperList2.get(j).getEnd(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                                text.setSpan(new AbsoluteSizeSpan(fontSize + 8), indexWrapperList2.get(j).getStart(), indexWrapperList2.get(j).getStart() + 1, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                            }
                        }
                    }

                    if (isBookmarked(verseData.get(position).getData_aid())) {
                        ((ViewHolderRow) holder).llBookmark.setVisibility(View.VISIBLE);
                        ((ViewHolderRow) holder).tvbookmark.setText(StringBookmark);
                    } else {
                        ((ViewHolderRow) holder).llBookmark.setVisibility(View.GONE);
                    }

                    if (isNoteAdded(verseData.get(position).getData_aid())) {
                        ((ViewHolderRow) holder).llNotes.setVisibility(View.VISIBLE);
                        ((ViewHolderRow) holder).tvNotes.setText(note);
                    } else {
                        ((ViewHolderRow) holder).llNotes.setVisibility(View.GONE);
                    }

                    if (isCrossReferenceAdded(verseData.get(position).getData_aid())) {
                        ((ViewHolderRow) holder).llCrossRef.setVisibility(View.VISIBLE);
                        ((ViewHolderRow) holder).tvCrossRef.setText(crossRefText);
                    } else {
                        ((ViewHolderRow) holder).llCrossRef.setVisibility(View.GONE);
                    }

                    if (isTagged(verseData.get(position).getData_aid())) {
                        ((ViewHolderRow) holder).llTags.setVisibility(View.VISIBLE);
                        ((ViewHolderRow) holder).tvTags.setText(stringTag);
                    } else {
                        ((ViewHolderRow) holder).llTags.setVisibility(View.GONE);
                    }


                    if (headerText != null && headerText.length() != 0) {
                        List<IndexWrapper> indexWrapperList3 = Global.findIndexesForKeyword(temp[0], headerText);
                        for (int i = 0; i < indexWrapperList3.size(); i++) {
                            ColorStateList blueColor = new ColorStateList(new int[][]{new int[]{}}, new int[]{activity.getResources().getColor(R.color.textColor)});
                            TextAppearanceSpan textAppearanceSpan = new TextAppearanceSpan(null, Typeface.NORMAL, -1, blueColor, null);
                            text.setSpan(new BackgroundColorSpan(Color.parseColor("#cb8954")), indexWrapperList3.get(i).getStart(), indexWrapperList3.get(i).getEnd(), 0);
                            text.setSpan(textAppearanceSpan, indexWrapperList3.get(i).getStart(), indexWrapperList3.get(i).getEnd(), 0);
                        }
                    }

                    ArrayList<SpannedTextData> spanArrayList = Highlightdb.getAllHighLightData();

                    if (spanArrayList != null && spanArrayList.size() > 0) {
                        for (int i = 0; i < spanArrayList.size(); i++) {
                            if (verseData.get(position).getData_aid().contains(String.valueOf(spanArrayList.get(i).getVerseAId()))) {
                                String isDeleted = spanArrayList.get(i).getDeleted();
                                if (isDeleted == null) {
                                    isDeleted = "";
                                }
                                if (isDeleted.equalsIgnoreCase("")) {
                                    if (spanArrayList.get(i).getHighlightflag() == 0) {

                                        String[] allStringWordsArray = text.toString().split("\\s+");
                                        int startIndex = 0;
                                        startIndex = spanArrayList.get(i).getStartIndex();
                                        int endIndex = 0;
                                        endIndex = spanArrayList.get(i).getEndIndex();
                                        String finalString = "";

                                        if (startIndex == 0 && endIndex == -1) {
                                            text.setSpan(new BackgroundColorSpan(Color.parseColor(Global.addLetter(spanArrayList.get(i).getColor(), "80", 1))), 0, text.length(), 0);
                                        } else {

                                            if (startIndex == 1) {

                                                startIndex = 0;
                                                endIndex = endIndex - 1;

                                                for (int j = 0; j < allStringWordsArray.length; j++) {

                                                    if (j >= startIndex) {
                                                        if (j <= endIndex) {
                                                            finalString = finalString + " " + allStringWordsArray[j].toString();
                                                        }
                                                    }
                                                }

                                                if (finalString != null) {

                                                    String[] mysplitarray = finalString.toString().split("\\s+");
                                                    int startpos = text.toString().indexOf(mysplitarray[0]);
                                                    int endpos = startpos + finalString.length();
                                                    text.setSpan(new BackgroundColorSpan(Color.parseColor(Global.addLetter(spanArrayList.get(i).getColor(), "80", 1))), startpos, endpos - 1, 0);
                                                }
                                            } else {
                                                for (int j = 0; j < allStringWordsArray.length; j++) {

                                                    if (j >= startIndex) {
                                                        if (j <= endIndex) {
                                                            finalString = finalString + " " + allStringWordsArray[j].toString();
                                                        }
                                                    }
                                                }

                                                if (finalString != null) {

                                                    Pattern word = Pattern.compile(finalString);
                                                    Matcher match = word.matcher(text);

                                                    while (match.find()) {
                                                        text.setSpan(new BackgroundColorSpan(Color.parseColor(Global.addLetter(spanArrayList.get(i).getColor(), "80", 1))), match.start(), (match.end()), 0);
                                                    }
                                                }
                                            }
                                        }
                                    } else {
                                        String[] allStringWordsArray = text.toString().split("\\s+");
                                        int startIndex = 0;
                                        startIndex = spanArrayList.get(i).getStartIndex();
                                        int endIndex = 0;
                                        endIndex = spanArrayList.get(i).getEndIndex();
                                        String finalString = "";

                                        if (startIndex == 0 && endIndex == -1) {
                                            text.setSpan(new ColoredUnderlineSpan(Color.parseColor(Global.addLetter(spanArrayList.get(i).getColor(), "", 1))), 0, text.length(), 0);
                                        } else {

                                            if (startIndex == 1) {

                                                startIndex = 0;
                                                endIndex = endIndex - 1;

                                                for (int j = 0; j < allStringWordsArray.length; j++) {

                                                    if (j >= startIndex) {
                                                        if (j <= endIndex) {
                                                            finalString = finalString + " " + allStringWordsArray[j].toString();
                                                        }
                                                    }
                                                }

                                                if (finalString != null) {

                                                    String[] mysplitarray = finalString.toString().split("\\s+");
                                                    int startpos = text.toString().indexOf(mysplitarray[0]);
                                                    int endpos = startpos + finalString.length();

                                                    text.setSpan(new ColoredUnderlineSpan(Color.parseColor(Global.addLetter(spanArrayList.get(i).getColor(), "", 1))), startpos, endpos, 0);
                                                }
                                            } else {
                                                for (int j = 0; j < allStringWordsArray.length; j++) {

                                                    if (j >= startIndex) {
                                                        if (j <= endIndex) {
                                                            finalString = finalString + " " + allStringWordsArray[j].toString();
                                                        }
                                                    }
                                                }

                                                if (finalString != null) {

                                                    Matcher match = Pattern.compile(Pattern.quote(finalString.trim()), Pattern.UNICODE_CASE).matcher(text);
                                                    while (match.find()) {
                                                        text.setSpan(new ColoredUnderlineSpan(Color.parseColor(Global.addLetter(spanArrayList.get(i).getColor(), "", 1))), match.start(), (match.end()), 0);
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }

                    final String[] normalVerseText = {temp[0]};

                    SpannableString h = new SpannableString("  ");
                    text.append(h);


                    if (citationHashMap != null) {
                        String citationCountfirst = String.valueOf(citationHashMap.get(verseData.get(position).getData_aid()));

                        int citationCount = 0;
                        if (citationCountfirst != null &&
                                !citationCountfirst.isEmpty() &&
                                !citationCountfirst.equals("null")) {

                            citationCount = Integer.parseInt(citationCountfirst);

                            if (citationCount != 0) {
                                Drawable d = createDrawable(String.valueOf(citationCount));
                                d.setBounds(0, 0, d.getIntrinsicWidth(), d.getIntrinsicHeight());
                                ImageSpan span = new ImageSpan(d, ImageSpan.ALIGN_BOTTOM);
                                text.setSpan(span, text.length() - 1, text.length(), Spannable.SPAN_INCLUSIVE_EXCLUSIVE);
                            }
                        }
                    }

                    ((ViewHolderRow) holder).tvVerse.setText(text);
                    ((ViewHolderRow) holder).tvVerse.setMovementMethod(LinkMovementMethod.getInstance());
                    ((ViewHolderRow) holder).tvVerse.setOnTouchListener(new View.OnTouchListener() {

                        @Override
                        public boolean onTouch(View v, MotionEvent event) {

                            if (verseData.get(position).getFootnoteArray() != null) {
                                for (int i = 0; i < verseData.get(position).getFootnoteArray().size(); i++) {
                                    normalVerseText[0] = normalVerseText[0].replace(verseData.get(position).getFootnoteArray().get(i), "&nbsp;" + verseData.get(position).getFootnoteArray().get(i).substring(1));
                                }
                            }

                            newVerse = normalVerseText[0];
                            mPosition = position;
                            gd.onTouchEvent(event);
                            return false;
                        }
                    });

                    gd = new GestureDetector(activity, new GestureDetector.OnGestureListener() {
                        @Override
                        public boolean onSingleTapUp(MotionEvent e) {
                            // TODO Auto-generated method stub
                            return false;
                        }

                        @Override
                        public void onShowPress(MotionEvent e) {
                            // TODO Auto-generated method stub

                        }

                        @Override
                        public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY) {
                            return false;
                        }

                        @Override
                        public void onLongPress(MotionEvent e) {


                            ActivityEventReceiver.cancelAlarm(activity);

                            String citationCountfirst;

                            if (citationHashMap != null) {
                                citationCountfirst = String.valueOf(citationHashMap.get(verseData.get(mPosition).getData_aid()));
                            } else {
                                citationCountfirst = "0";
                            }

                            Intent intent = new Intent(activity, VerseHighlightActivity.class);

                            String intentstringTag = "";
                            if (isTagged(verseData.get(mPosition).getData_aid())) {
                                intentstringTag = stringTag;
                            }

                            intent.putExtra("readingtitle", title);
                            intent.putExtra("stringTag", intentstringTag);
                            intent.putExtra(activity.getString(R.string.verseText), newVerse);
                            intent.putExtra(activity.getString(R.string.versePos), mPosition);
                            intent.putExtra(activity.getString(R.string.verseId), verseData.get(mPosition).getId());
                            intent.putExtra(activity.getString(R.string.verseAId), verseData.get(mPosition).getData_aid());
                            intent.putExtra(activity.getString(R.string.indexId), indexID);
                            intent.putExtra(activity.getString(R.string.itemUri), indexURI);
                            intent.putExtra(activity.getString(R.string.citationCount), citationCountfirst);

                            activity.startActivity(intent);
                            activity.overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);

                        }

                        @Override
                        public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
                            // TODO Auto-generated method stub
                            return false;
                        }

                        @Override
                        public boolean onDown(MotionEvent e) {
                            // TODO Auto-generated method stub
                            return false;
                        }
                    });
                    gd.setOnDoubleTapListener(new GestureDetector.OnDoubleTapListener() {

                        @Override
                        public boolean onDoubleTap(MotionEvent e) {

                            if (ReadingActivity.getInstance() != null) {
                                ReadingActivity.getInstance().showHideLayout();
                            }

                            if (linScroll != null) {
                                if (linScroll.getVisibility() == View.VISIBLE) {
                                    linScroll.setVisibility(View.GONE);
                                } else {
                                    linScroll.setVisibility(View.VISIBLE);
                                }
                            }

                            edSearch.setText("");

                            recyclerView.setAdapter(new ReadingStepRecyclerAdapter(activity, verseData, linScroll, "", headerContent, indexID, recyclerView, edSearch, lsId, verseAId, title, citationHashMap, indexURI, verseIdArrayStrig));
                            LinearLayoutManager llm1 = (LinearLayoutManager) recyclerView.getLayoutManager();
                            llm1.scrollToPositionWithOffset(sessionManager.getoffset(), verseData.size());

                            return false;
                        }

                        @Override
                        public boolean onDoubleTapEvent(MotionEvent e) {
                            // if the second tap hadn't been released and it's being moved
                            return false;
                        }

                        @Override
                        public boolean onSingleTapConfirmed(MotionEvent e) {
                            // TODO Auto-generated method stub
                            return false;
                        }

                    });

                } else {
                    ((ViewHolderRow) holder).inner_layout_verse.setVisibility(View.GONE);
                    ((ViewHolderRow) holder).inner_layout_video.setVisibility(View.GONE);
                    ((ViewHolderRow) holder).inner_layout_image.setVisibility(View.GONE);
                }

            } else if (verseData.get(position).getLayoutflag() == 2) {

                ArrayList<String> videoUrlArray = new ArrayList<>();
                videoUrlArray = verseData.get(position).getVideoUrlArray();

                if (videoUrlArray != null && videoUrlArray.size() != 0) {

                    ((ViewHolderRow) holder).inner_layout_verse.setVisibility(View.GONE);
                    ((ViewHolderRow) holder).inner_layout_video.setVisibility(View.VISIBLE);
                    ((ViewHolderRow) holder).inner_layout_image.setVisibility(View.GONE);

                    final String videoUrl = videoUrlArray.get(0);
                    // SpannableStringBuilder text = new SpannableStringBuilder(Global.noTrailingwhiteLines(Html.fromHtml(videoUrl)));

                    String tempString = "Watch Video";
                    final SpannableString content = new SpannableString(tempString);
                    content.setSpan(new UnderlineSpan(), 0, tempString.length(), 0);
                    ((ViewHolderRow) holder).video_url_textview.setText(content);
                    ((ViewHolderRow) holder).video_url_textview.setTextColor(activity.getResources().getColor(R.color.facebook_color));

                    ((ViewHolderRow) holder).video_url_textview.setOnClickListener(new View.OnClickListener() {

                        @Override
                        public void onClick(View view) {
                            Intent browser = new Intent(Intent.ACTION_VIEW, Uri.parse(videoUrl));
                            try {
                                activity.startActivity(browser);
                            } catch (ActivityNotFoundException ex) {
                                Toast.makeText(activity, "No activity found", Toast.LENGTH_LONG).show(); //Display an error message
                            }
                        }

                    });

                }

            } else if (verseData.get(position).getLayoutflag() == 3) {

                ArrayList<String> imageUrlArray = new ArrayList<>();
                imageUrlArray = verseData.get(position).getImageUrlArray();

                if (imageUrlArray != null && imageUrlArray.size() != 0) {

                    ((ViewHolderRow) holder).inner_layout_verse.setVisibility(View.GONE);
                    ((ViewHolderRow) holder).inner_layout_video.setVisibility(View.GONE);
                    ((ViewHolderRow) holder).inner_layout_image.setVisibility(View.VISIBLE);

                    final String imageUrl = imageUrlArray.get(0);
                    //SpannableStringBuilder text = new SpannableStringBuilder(Global.noTrailingwhiteLines(Html.fromHtml(imageUrl)));

                    String tempString = "View Image";
                    SpannableString content = new SpannableString(tempString);
                    content.setSpan(new UnderlineSpan(), 0, tempString.length(), 0);
                    ((ViewHolderRow) holder).image_url_textview.setText(content);
                    ((ViewHolderRow) holder).image_url_textview.setTextColor(activity.getResources().getColor(R.color.facebook_color));
                    ((ViewHolderRow) holder).image_url_textview.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            Intent browser = new Intent(Intent.ACTION_VIEW, Uri.parse(imageUrl));
                            try {
                                activity.startActivity(browser);
                            } catch (ActivityNotFoundException ex) {
                                Toast.makeText(activity, "No activity found", Toast.LENGTH_LONG).show(); //Display an error message
                            }
                        }
                    });

                }
            } else {
                ((ViewHolderRow) holder).inner_layout_verse.setVisibility(View.GONE);
                ((ViewHolderRow) holder).inner_layout_video.setVisibility(View.GONE);
                ((ViewHolderRow) holder).inner_layout_image.setVisibility(View.GONE);
            }
        }
    }


    public String getAllNumbersBetween(int x, int y) {
        String numbers = "";
        for (int i = x; i <= y; i++) {
            numbers = numbers + "," + i;
        }
        numbers = numbers.startsWith(",") ? numbers.substring(1) : numbers;
        return numbers;
    }

    @Override
    public int getItemViewType(int position) {
        if (position == 0)
            return 0;
        else
            return 1;
    }

    @Override
    public int getItemCount() {
        if (verseData != null)
            return verseData.size();
        else return 0;
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    public static class ViewHolderHeader extends RecyclerView.ViewHolder {

        TextView tvReadingPreview;
        RelativeLayout relMainContent;

        public ViewHolderHeader(View itemView) {
            super(itemView);
            tvReadingPreview = (TextView) itemView.findViewById(R.id.tvReadingPreview);
            relMainContent = (RelativeLayout) itemView.findViewById(R.id.relMainContent);
        }
    }

    private class ViewHolderRow extends RecyclerView.ViewHolder {

        TextView tvVerse, tvNotes, tvTags, tvCrossRef;
        TextView tvVerseId;
        TextView txtCitation_Count_Ad;
        LinearLayout llNotes, llTags, llCrossRef, main_readingstep_Layout, llBookmark, inner_layout_verse, inner_layout_video, inner_layout_image;
        FrameLayout txtCitation_CountLayout;
        TextView tvbookmark, video_url_textview, image_url_textview;

        public ViewHolderRow(View itemView) {
            super(itemView);
//            ivBookmark = (ImageView) itemView.findViewById(R.id.ivBookmark);
            llNotes = (LinearLayout) itemView.findViewById(R.id.llNotes);
            llTags = (LinearLayout) itemView.findViewById(R.id.llTags);
            llCrossRef = (LinearLayout) itemView.findViewById(R.id.llCrossRef);
            main_readingstep_Layout = (LinearLayout) itemView.findViewById(R.id.main_readingstep_Layout);
            tvVerse = (TextView) itemView.findViewById(R.id.tvVerse);
            tvNotes = (TextView) itemView.findViewById(R.id.tvNotes);
            tvTags = (TextView) itemView.findViewById(R.id.tvTags);
            tvVerseId = (TextView) itemView.findViewById(R.id.tvVerseId);
            tvCrossRef = (TextView) itemView.findViewById(R.id.tvCrossRef);
            txtCitation_Count_Ad = (TextView) itemView.findViewById(R.id.txtCitation_Count_Ad);
            txtCitation_CountLayout = (FrameLayout) itemView.findViewById(R.id.txtCitation_CountLayout);
            llBookmark = (LinearLayout) itemView.findViewById(R.id.llBookmark);
            tvbookmark = (TextView) itemView.findViewById(R.id.tvbookmark);
            inner_layout_verse = (LinearLayout) itemView.findViewById(R.id.inner_layout_verse);
            inner_layout_video = (LinearLayout) itemView.findViewById(R.id.inner_layout_video);
            inner_layout_image = (LinearLayout) itemView.findViewById(R.id.inner_layout_image);
            video_url_textview = (TextView) itemView.findViewById(R.id.video_url_textview);
            image_url_textview = (TextView) itemView.findViewById(R.id.image_url_textview);

        }
    }

    private class MyClickableSpan extends ClickableSpan {

        public MyClickableSpan() {
            super();
        }

        public void onClick(View widget) {
            TextView tv = (TextView) widget;
            Spanned s = (Spanned) tv.getText();
            int start = s.getSpanStart(this);
            int end = s.getSpanEnd(this);
            tv.setHighlightColor(activity.getResources().getColor(android.R.color.transparent));
            sessionManager.setUserDailyPoints(25);
            new GetFootnotesFromDB(activity, indexID, s.subSequence(start, end).charAt(0), verseData.get(mPosition).getId(), lsId);
        }

        public void updateDrawState(TextPaint ds) {// override updateDrawState
            ds.setUnderlineText(false); // set to false to remove underline
            ds.setColor(activity.getResources().getColor(R.color.sand)); // set to false to remove underline
        }
    }

    private ArrayList<BookmarkData> getNotes(String data_aid) {
        Cursor cursor;

        notesArrayList.clear();

        cursor = commonDatabaseHelper.getNotesList(data_aid);
        if (cursor != null && cursor.moveToFirst()) {
            while (!cursor.isAfterLast()) {

                BookmarkData noteskData = new BookmarkData();

                noteskData.setServerid(cursor.getString(cursor.getColumnIndex("serverid")));
                noteskData.setVerseaid(cursor.getString(cursor.getColumnIndex("verseaid")));
                noteskData.setChapterUri(cursor.getString(cursor.getColumnIndex("chapteruri")));
                noteskData.setItemuri(cursor.getString(cursor.getColumnIndex("itemuri")));
                noteskData.setBookuri(cursor.getString(cursor.getColumnIndex("bookuri")));
                noteskData.setTitle(cursor.getString(cursor.getColumnIndex("versetitle")));
                noteskData.setVerseNumber(cursor.getString(cursor.getColumnIndex("versenumber")));
                noteskData.setVerseText(cursor.getString(cursor.getColumnIndex("versetext")));
                noteskData.setDeleted(cursor.getString(cursor.getColumnIndex("deleted")));
                noteskData.setModified(cursor.getString(cursor.getColumnIndex("modified")));

                notesArrayList.add(noteskData);
                cursor.moveToNext();
            }
        }
        return notesArrayList;
    }

    private ArrayList<BookmarkData> getBookmarks(String data_aid) {
        Cursor cursor;

        bookmarkDataArrayList.clear();

        cursor = commonDatabaseHelper.getBookmarkList(data_aid);
        if (cursor != null && cursor.moveToFirst()) {
            while (!cursor.isAfterLast()) {

                BookmarkData noteskData = new BookmarkData();

                noteskData.setServerid(cursor.getString(cursor.getColumnIndex("serverid")));
                noteskData.setVerseaid(cursor.getString(cursor.getColumnIndex("verseaid")));
                noteskData.setChapterUri(cursor.getString(cursor.getColumnIndex("chapteruri")));
                noteskData.setItemuri(cursor.getString(cursor.getColumnIndex("itemuri")));
                noteskData.setBookuri(cursor.getString(cursor.getColumnIndex("bookuri")));
                noteskData.setTitle(cursor.getString(cursor.getColumnIndex("versetitle")));
                noteskData.setVerseNumber(cursor.getString(cursor.getColumnIndex("versenumber")));
                noteskData.setVerseText(cursor.getString(cursor.getColumnIndex("versetext")));
                noteskData.setDeleted(cursor.getString(cursor.getColumnIndex("deleted")));
                noteskData.setModified(cursor.getString(cursor.getColumnIndex("modified")));

                bookmarkDataArrayList.add(noteskData);
                cursor.moveToNext();
            }
        }
        return bookmarkDataArrayList;
    }

    public abstract class DoubleClickListener implements View.OnClickListener {

        private static final long DOUBLE_CLICK_TIME_DELTA = 300;//milliseconds

        long lastClickTime = 0;

        @Override
        public void onClick(View v) {
            long clickTime = System.currentTimeMillis();
            if (clickTime - lastClickTime < DOUBLE_CLICK_TIME_DELTA) {
                onDoubleClick(v);
            } else {
                onSingleClick(v);
            }
            lastClickTime = clickTime;
        }

        public abstract void onSingleClick(View v);

        public abstract void onDoubleClick(View v);
    }

    private Drawable createDrawable(String number) {
        if (activity != null) {
            View marker = ((LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.citation_icon_layout, null);
            TextView tv_no = (TextView) marker.findViewById(R.id.tv_no);
            tv_no.setText(number);
            marker.setDrawingCacheEnabled(true);
            marker.measure(View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED), View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED));
            marker.layout(0, 0, marker.getMeasuredWidth(), marker.getMeasuredHeight());
            marker.buildDrawingCache(true);
            Bitmap b = Bitmap.createBitmap(marker.getDrawingCache());
            marker.setDrawingCacheEnabled(false); // clear drawing cache

            Drawable drawable = (Drawable) new BitmapDrawable(activity.getResources(), b);
            return drawable;
        }
        return null;
    }

    public void audioViewHideShow() {
        if (ReadingActivity.getInstance() != null) {
            ReadingActivity.getInstance().showHideLayout();
        }
        if (linScroll != null) {
            if (linScroll.getVisibility() == View.VISIBLE) {
                linScroll.setVisibility(View.GONE);
            } else {
                linScroll.setVisibility(View.VISIBLE);
            }
        }
        edSearch.setText("");
    }
}
