package com.ldsscriptures.pro.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.ldsscriptures.pro.fragment.FollowingFacebookTabFragment;
import com.ldsscriptures.pro.fragment.FollowingContactsTabFragment;

/**
 * Created by IBL InfoTech on 7/27/2017.
 */

public class FollowingFindFriendPagerAdapter extends FragmentStatePagerAdapter {
    int mNumOfTabs;
    public FollowingFindFriendPagerAdapter(FragmentManager fm, int NumOfTabs) {
        super(fm);
        this.mNumOfTabs = NumOfTabs;
    }

    @Override
    public Fragment getItem(int position) {

        switch (position) {
            case 0:
                FollowingFacebookTabFragment tab1 = new FollowingFacebookTabFragment();
                return tab1;
            case 1:
                FollowingContactsTabFragment tab2 = new FollowingContactsTabFragment();
                return tab2;

            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return mNumOfTabs;
    }
}
