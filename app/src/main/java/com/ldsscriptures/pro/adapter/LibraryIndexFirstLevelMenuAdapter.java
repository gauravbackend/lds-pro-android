package com.ldsscriptures.pro.adapter;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.ldsscriptures.pro.R;
import com.ldsscriptures.pro.activity.LibraryIndexActivity;
import com.ldsscriptures.pro.activity.LibrarySelectionTabsActivity;
import com.ldsscriptures.pro.activity.ReadingActivity;
import com.ldsscriptures.pro.activity.verses.CitationSubListActivity;
import com.ldsscriptures.pro.dbClass.GetDataFromDatabase;
import com.ldsscriptures.pro.dbClass.GetLibCollectionFromDB;
import com.ldsscriptures.pro.model.LcData;
import com.ldsscriptures.pro.model.LsData;
import com.ldsscriptures.pro.model.LsIndexPreviewData;
import com.ldsscriptures.pro.utils.DownloadSubDBZipFile;
import com.ldsscriptures.pro.utils.Global;

import java.io.BufferedInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;


public class LibraryIndexFirstLevelMenuAdapter extends RecyclerView.Adapter<LibraryIndexFirstLevelMenuAdapter.MyViewHolder> {

    private final String LcID;
    private Activity activity;
    private ArrayList<LcData> lcDataArrayList = new ArrayList<>();

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView textView;
        public LinearLayout linDropDown;

        public MyViewHolder(View view) {
            super(view);
            textView = (TextView) view.findViewById(R.id.textView);
            linDropDown = (LinearLayout) view.findViewById(R.id.linDropDown);
        }
    }


    public LibraryIndexFirstLevelMenuAdapter(Activity activity, ArrayList<LcData> lcDataArrayList, String LcID) {
        this.lcDataArrayList = lcDataArrayList;
        this.activity = activity;
        this.LcID = LcID;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, final int position) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_dropdown_menu, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {

        holder.textView.setTextColor(activity.getResources().getColor(R.color.drop_down_first_level_text));
        holder.textView.setText(lcDataArrayList.get(position).getTitle());

        holder.textView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                ArrayList<LsData> lsDataArrayList = new ArrayList<>();
                lsDataArrayList = new GetLibCollectionFromDB(activity).getLsDataFromDb(lcDataArrayList.get(position).getId());

                if (LibraryIndexActivity.getInstance() != null) {
                    LibraryIndexActivity.getInstance().setSecondLevel(lsDataArrayList, lcDataArrayList.get(position).getId());
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return lcDataArrayList.size();
    }


}