package com.ldsscriptures.pro.adapter;

import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.daimajia.swipe.SwipeLayout;
import com.daimajia.swipe.adapters.RecyclerSwipeAdapter;
import com.ldsscriptures.pro.R;
import com.ldsscriptures.pro.activity.studytools.AddLessonsActivity;
import com.ldsscriptures.pro.activity.studytools.LessonSubItemActivity;
import com.ldsscriptures.pro.activity.studytools.LessonsActivity;
import com.ldsscriptures.pro.database.CommonDatabaseHelper;
import com.ldsscriptures.pro.model.LessonData;
import com.ldsscriptures.pro.utils.Global;
import com.ldsscriptures.pro.utils.PushToServerData;
import com.ldsscriptures.pro.utils.SessionManager;

import java.util.ArrayList;

public class LessonListAdapter extends RecyclerSwipeAdapter<LessonListAdapter.MyViewHolder> {

    private final Context context;
    private ArrayList<LessonData> lessonDataArrayList = new ArrayList<>();
    CommonDatabaseHelper commonDatabaseHelper;
    private int flag;
    SessionManager sessionManager;

    public LessonListAdapter(Context context, ArrayList<LessonData> lessonDataArrayList, int flag) {
        this.context = context;
        this.lessonDataArrayList = lessonDataArrayList;
        this.flag = flag;
        commonDatabaseHelper = new CommonDatabaseHelper(context);
        sessionManager = new SessionManager(context);
    }

    @Override
    public int getSwipeLayoutResourceId(int position) {
        return R.id.lessonMainswipe;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView txtLessonDate;
        public TextView txtLessonName;
        public LinearLayout linLesson;
        public ImageView ivInfo;
        SwipeLayout swipeLayout;
        TextView tvDelete;

        public MyViewHolder(View view) {
            super(view);
            txtLessonDate = (TextView) view.findViewById(R.id.txtLessonDate);
            txtLessonName = (TextView) view.findViewById(R.id.txtLessonName);
            linLesson = (LinearLayout) view.findViewById(R.id.linLesson);
            ivInfo = (ImageView) view.findViewById(R.id.ivInfo);
            swipeLayout = (SwipeLayout) itemView.findViewById(R.id.lessonMainswipe);
            tvDelete = (TextView) itemView.findViewById(R.id.tvDelete_lessonMain);
        }
    }

    @Override
    public LessonListAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, final int position) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_lessons_list, parent, false);
        return new LessonListAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final LessonListAdapter.MyViewHolder holder, final int position) {

        if (lessonDataArrayList.get(position).getLessonname() != null)
            holder.txtLessonName.setText(lessonDataArrayList.get(position).getLessonname());

        if (lessonDataArrayList.get(position).getLessondate() != null)
            holder.txtLessonDate.setText(Global.convertViewFormat(lessonDataArrayList.get(position).getLessondate()));

        holder.linLesson.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, LessonSubItemActivity.class);
                intent.putExtra(context.getString(R.string.title), lessonDataArrayList.get(position).getLessonname());
                intent.putExtra(context.getString(R.string.lesson_id), lessonDataArrayList.get(position).getId());
                context.startActivity(intent);
            }
        });

        holder.ivInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent tagIntent = new Intent(context, AddLessonsActivity.class);
                tagIntent.putExtra("Lessonname", lessonDataArrayList.get(position).getLessonname());
                tagIntent.putExtra("Lessondate", lessonDataArrayList.get(position).getLessondate());
                tagIntent.putExtra("Lessondesc", lessonDataArrayList.get(position).getLessondesc());
                tagIntent.putExtra("Lessonid", lessonDataArrayList.get(position).getId());
                context.startActivity(tagIntent);
            }
        });

        holder.swipeLayout.setShowMode(SwipeLayout.ShowMode.PullOut);
        // Drag From Right
        holder.swipeLayout.addDrag(SwipeLayout.DragEdge.Right, holder.swipeLayout.findViewById(R.id.bottom_wrapper_lessonMain));
        // Handling different events when swiping
        holder.swipeLayout.addSwipeListener(new SwipeLayout.SwipeListener() {
            @Override
            public void onClose(SwipeLayout layout) {
                //when the SurfaceView totally cover the BottomView.
            }

            @Override
            public void onUpdate(SwipeLayout layout, int leftOffset, int topOffset) {
                //you are swiping.
            }

            @Override
            public void onStartOpen(SwipeLayout layout) {

            }

            @Override
            public void onOpen(SwipeLayout layout) {
                //when the BottomView totally show.
            }

            @Override
            public void onStartClose(SwipeLayout layout) {

            }

            @Override
            public void onHandRelease(SwipeLayout layout, float xvel, float yvel) {
                //when user's hand released.
            }
        });
        holder.tvDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                final String title = lessonDataArrayList.get(position).getLessonname().toString().trim();
                final String description = lessonDataArrayList.get(position).getLessondesc().toString().trim();
                final String date = lessonDataArrayList.get(position).getLessondate().toString().trim();
                final String serverid = lessonDataArrayList.get(position).getServerid().toString().trim();
                final int id = lessonDataArrayList.get(position).getId();

                try {
                    android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(context);
                    builder.setTitle("Warning");
                    builder.setMessage(R.string.are_you_sure_delete)

                            .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            })
                            .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    // Yes button clicked, do something
                                    dialog.dismiss();
                                    deleteLocally(holder, position, lessonDataArrayList, id);
                                    new PushToServerData(context);
                                }
                            });

                    builder.show();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });


    }

    @Override
    public int getItemCount() {
        return lessonDataArrayList.size();
    }

    public void deleteLocally(final MyViewHolder holder,
                              final int position,
                              final ArrayList<LessonData> lessonDataArrayList,
                              final int id) {
        mItemManger.removeShownLayouts(holder.swipeLayout);
        lessonDataArrayList.remove(position);
        notifyItemRemoved(position);
        notifyItemRangeChanged(position, lessonDataArrayList.size());
        mItemManger.closeAllItems();
        /*commonDatabaseHelper.deleteRowData(CommonDatabaseHelper.TABLE_LESSONS, commonDatabaseHelper.KEY_ID + " = ?",
                new String[]{String.valueOf(id)});*/

        ContentValues contentValues = new ContentValues();
        contentValues.put(CommonDatabaseHelper.KEY_DELETED, "1");
        commonDatabaseHelper.updateRowData(CommonDatabaseHelper.TABLE_LESSONS, contentValues, CommonDatabaseHelper.KEY_ID + " = ?",
                new String[]{String.valueOf(id)});

        if (lessonDataArrayList.size() == 0) {
            if (flag == 0) {
                LessonsActivity.getInstance().upcomingView();
            } else if (flag == 1) {
                LessonsActivity.getInstance().previousView();
            }
        }
    }
}