package com.ldsscriptures.pro.fcmservice;

import android.net.Uri;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.ldsscriptures.pro.utils.Global;

public class MyFirebaseMessagingService extends FirebaseMessagingService {

    private static final String TAG = "MyFirebaseMsgService";
    public static boolean isMessageNotification = false;
    private Uri defaultSoundUri;

    /*
     * Called when message is received.
     * @param remoteMessage Object representing the message received from Firebase Cloud Messaging.
     */
    // [START receive_message]
    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        // [START_EXCLUDE]
        // There are two types of messages data messages and notification messages. Data messages are handled
        // here in onMessageReceived whether the app is in the foreground or background. Data messages are the type
        // traditionally used with GCM. Notification messages are only received here in onMessageReceived when the app
        // is in the foreground. When the app is in the background an automatically generated notification is displayed.
        // When the user taps on the notification they are returned to the app. Messages containing both notification
        // and data payloads are treated as notification messages. The Firebase console always sends notification
        // messages. For more see: https://firebase.google.com/docs/cloud-messaging/concept-options
        // [END_EXCLUDE]

        // TODO(developer): Handle FCM messages here.
        // Not getting messages here? See why this may be: https://goo.gl/39bRNJ
        Global.printLog(TAG, "Data: " + remoteMessage.getData());

        // Check if message contains a data payload.
        if (remoteMessage.getData().size() > 0) {
            Global.printLog(TAG, "Message data payload: " + remoteMessage.getData());
            sendNotification(remoteMessage.getData().get("sound"), remoteMessage.getData().get("message"), remoteMessage.getData().get("messageId"), remoteMessage.getData().get("promotionId"));
//            sendNotification(String.valueOf(remoteMessage.getData()));
        }

        // Check if message contains a notification payload.
        if (remoteMessage.getNotification() != null) {
            Global.printLog(TAG, "Message Notification Body: " + remoteMessage.getNotification().getBody());
        }

        // Also if you intend on generating your own notifications as a result of a received FCM
        // message, here is where that should be initiated. See sendNotification method below.
    }
    // [END receive_message]

    /*
     * Create and show a simple notification containing the received FCM message.
     * @param messageBody FCM message body received.
     */
    private void sendNotification(String sound, String message, String messageId, String promotionId) {
//        Intent intent = null;
//
//        if (messageId.equals("0")) {
//            isMessageNotification = false;
//            intent = new Intent(this, BeaconViewPagerActivity.class);
//            getPromotionDetailById(promotionId);
//        } else if (promotionId.equals("0")) {
//            isMessageNotification = true;
//            intent = new Intent(this, MessageActivity.class);
//            getMessage(messageId);
//        }
//
//        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
//        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, intent, PendingIntent.FLAG_ONE_SHOT);
//
//        GlobalClass.printLog(TAG, "Message " + message);
//        if (sound.equals("default"))
//            defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
//
//        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
//                .setSmallIcon(getNotificationIcon())
//                .setContentTitle(getString(R.string.app_name))
//                .setContentText(message)
//                .setAutoCancel(true)
//                .setSound(defaultSoundUri)
//                .setContentIntent(pendingIntent);
//        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
//
//        TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
//        // Adds the back stack for the Intent (but not the Intent itself)
//        stackBuilder.addParentStack(MainActivity1.class);
//        // Adds the Intent that starts the Activity to the top of the stack
//        stackBuilder.addNextIntent(intent);
//        PendingIntent contentIntent = stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);
//        notificationBuilder.setContentIntent(contentIntent);
//        int id = (int) System.currentTimeMillis();
//        notificationManager.notify(id /* ID of notification */, notificationBuilder.build());
//    }
//
//    private int getNotificationIcon() {
//        boolean useWhiteIcon = (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP);
//        return useWhiteIcon ? R.drawable.ic_app_icon_white : R.drawable.ic_app_icon;
    }

    private void getPromotionDetailById(String promotionId) {
//        RestAdapter restAdapter = new RestAdapter.Builder().setEndpoint(GlobalClass.SERVER_URL).build();
//        CallWebServices callWebServices = restAdapter.create(CallWebServices.class);
//
//        callWebServices.getPromotionDetailById(promotionId, new Callback<Response>() {
//
//            @Override
//            public void success(Response response, Response response2) {
//                BufferedReader bufferedReader;
//                String stringResponse;
//                try {
//                    bufferedReader = new BufferedReader(new InputStreamReader(response2.getBody().in()));
//                    stringResponse = bufferedReader.readLine();
//
//                    JSONObject jsonObjectResponse = new JSONObject(stringResponse);
//                    JSONObject jsonObject = jsonObjectResponse.getJSONObject("GetPromotionDetailByIdResult");
//                    JSONObject jsonObjectStatus = jsonObject.getJSONObject(GlobalClass.STATUS);
//
//                    if (jsonObjectStatus.getInt(GlobalClass.STATUS) == 1) {
//                        JSONObject jsonObject1 = jsonObject.getJSONObject("GetPromotionsParams");
//
//                        BeaconViewPagerActivity.categoryTypeId = jsonObject1.getString("IsBeaconAssociatedId");
//                        BeaconViewPagerActivity.categoryByCustomer = LoganSquare.parseList("[" + jsonObject1.toString() + "]", CategoryByCustomer.class);
//
//                        GlobalClass.printLog("category By Customer", "" + BeaconViewPagerActivity.categoryByCustomer.size());
//                    } else {
//                        GlobalClass.showSnackBar(GlobalClass.activity, jsonObjectStatus.getString(GlobalClass.DESCRIPTION));
//                    }
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
//            }
//
//            @Override
//            public void failure(RetrofitError error) {
//                GlobalClass.printLog("RetrofitError", " ---- :  " + error);
//            }
//        });
    }

    private void getMessage(String messageId) {
//        RestAdapter restAdapter = new RestAdapter.Builder().setEndpoint(GlobalClass.SERVER_URL).build();
//        CallWebServices callWebServices = restAdapter.create(CallWebServices.class);
//
//        callWebServices.getNotificationMessageById(messageId, new Callback<Response>() {
//
//            @Override
//            public void success(Response response, Response response2) {
//                BufferedReader bufferedReader;
//                String stringResponse;
//                try {
//                    bufferedReader = new BufferedReader(new InputStreamReader(response2.getBody().in()));
//                    stringResponse = bufferedReader.readLine();
//
//                    JSONObject jsonObjectResponse = new JSONObject(stringResponse);
//                    JSONObject jsonObject = jsonObjectResponse.getJSONObject("GetNotificationMessageByIdResult");
//                    JSONObject jsonObjectStatus = jsonObject.getJSONObject(GlobalClass.STATUS);
//
//                    if (jsonObjectStatus.getInt(GlobalClass.STATUS) == 1) {
//                        JSONObject jsonObject1 = jsonObject.getJSONObject("GetNotificationMessage");
//                        MessageActivity.jsonObject = jsonObject1;
//                    } else {
//                        GlobalClass.showSnackBar(GlobalClass.activity, jsonObjectStatus.getString(GlobalClass.DESCRIPTION));
//                    }
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
//            }
//
//            @Override
//            public void failure(RetrofitError error) {
//                GlobalClass.printLog("RetrofitError", " ---- :  " + error);
//            }
//        });
    }
}