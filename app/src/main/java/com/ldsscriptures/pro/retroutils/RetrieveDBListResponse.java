package com.ldsscriptures.pro.retroutils;

import java.util.ArrayList;

/**
 * Created on 31/05/17 by iblinfotech.
 */

public class RetrieveDBListResponse<T> {

    private String catalog_url;

    private String language_id;

    private String catalog;

    private String citations_url;

    private ArrayList<T> item;

    private String root_library_collection_id;

    private String language;

    private String size;

    public String getCatalog_url() {
        return catalog_url;
    }

    public void setCatalog_url(String catalog_url) {
        this.catalog_url = catalog_url;
    }

    public String getLanguage_id() {
        return language_id;
    }

    public void setLanguage_id(String language_id) {
        this.language_id = language_id;
    }

    public String getCatalog() {
        return catalog;
    }

    public void setCatalog(String catalog) {
        this.catalog = catalog;
    }

    public String getCitations_url() {
        return citations_url;
    }

    public void setCitations_url(String citations_url) {
        this.citations_url = citations_url;
    }

    public ArrayList<T> getItem() {
        return item;
    }

    public void setItem(ArrayList<T> item) {
        this.item = item;
    }

    public String getRoot_library_collection_id() {
        return root_library_collection_id;
    }

    public void setRoot_library_collection_id(String root_library_collection_id) {
        this.root_library_collection_id = root_library_collection_id;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    @Override
    public String toString() {
        return "ClassPojo [catalog_url = " + catalog_url + ", language_id = " + language_id + ", catalog = " + catalog + ", citations_url = " + citations_url + ", item = " + item + ", root_library_collection_id = " + root_library_collection_id + ", language = " + language + ", size = " + size + "]";
    }
}
