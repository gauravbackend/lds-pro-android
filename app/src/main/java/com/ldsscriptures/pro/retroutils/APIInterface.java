package com.ldsscriptures.pro.retroutils;

import com.google.gson.JsonObject;
import com.ldsscriptures.pro.model.BookmarkData;
import com.ldsscriptures.pro.model.EmailFriendData;
import com.ldsscriptures.pro.model.FollowersMainData;
import com.ldsscriptures.pro.model.FollowingMainModel;
import com.ldsscriptures.pro.model.FollowingModel;
import com.ldsscriptures.pro.model.ForgotPasswordData;
import com.ldsscriptures.pro.model.Item;
import com.ldsscriptures.pro.model.PurchaseData;
import com.ldsscriptures.pro.model.ServerResponse;
import com.ldsscriptures.pro.model.ShareAnnotationsData;
import com.ldsscriptures.pro.model.StudyDailyVerse;
import com.ldsscriptures.pro.model.SyncItems;
import com.ldsscriptures.pro.model.SyncResponse;
import com.ldsscriptures.pro.model.TimeLine.InnerTimeLineModel;
import com.ldsscriptures.pro.model.TimeLine.MainTimeLineModel;
import com.ldsscriptures.pro.model.TimeLineShareSuccess;
import com.ldsscriptures.pro.model.UsageDetail;
import com.ldsscriptures.pro.model.UserDetail;
import com.ldsscriptures.pro.model.highlightResponceData;

import java.util.ArrayList;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Url;

public interface APIInterface {


    @GET
    Call<UserDetail> updateUserInfo(@Url String url);

    @GET
    Call<JsonObject> getLogin(@Url String url);

    @GET
    Call<JsonObject> updateUserInfo1(@Url String url);

    @GET
    Call<JsonObject> getDBVersion(@Url String url);

    @GET
    Call<RetrieveDBListResponse<Item>> retrieveDB(@Url String url);

    @GET
    Call<ResponseBody> downloadFileWithDynamicUrl(@Url String fileUrl);

    @GET
    Call<ArrayList<StudyDailyVerse>> getStudyDailyVerse(@Url String url);

    @GET
    Call<FollowingModel<EmailFriendData>> getFollowingContactEmail(@Url String url);

    @GET
    Call<FollowingMainModel<FollowersMainData>> getFollowingData(@Url String url);

    @GET
    Call<ForgotPasswordData> recoverPassword(@Url String url);

    @GET
    Call<BookmarkData> getNote(@Url String url);

    @GET
    Call<ServerResponse> sentToServer(@Url String url);

    @GET
    Call<PurchaseData> callGoogleReceipt(@Url String url);

    @GET
    Call<ShareAnnotationsData> getShareAnnotationData(@Url String url);

    @GET
    Call<SyncResponse<SyncItems>> syncData(@Url String url);

    @GET
    Call<UsageDetail> usageInfo(@Url String url);

    @GET
    Call<MainTimeLineModel<InnerTimeLineModel>> getTimeLineData(@Url String url);

    @GET
    Call<TimeLineShareSuccess> getTimeLineShareSuccess(@Url String url);

    @GET
    Call<highlightResponceData> sendHighlight(@Url String url);





}
