package com.ldsscriptures.pro.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.ldsscriptures.pro.R;
import com.ldsscriptures.pro.adapter.HistoryFragmentListAdapter;
import com.ldsscriptures.pro.model.HistoryData;
import com.ldsscriptures.pro.model.HistorySubData;
import com.ldsscriptures.pro.utils.Global;
import com.ldsscriptures.pro.utils.SessionManager;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;


public class HistoryFragment extends BaseFragment {

    @BindView(R.id.eListView)
    ExpandableListView eListView;

    @BindView(R.id.txtNoData)
    TextView txtNoData;

    private SessionManager sessionManager;
    static HistoryFragment HistoryFragment;

    String TAG = "HistoryFragment";


    public static HistoryFragment getInstance() {
        return HistoryFragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_history, container, false);
        RelativeLayout lnr_root = (RelativeLayout) view.findViewById(R.id.lnr_root);
        ButterKnife.bind(this, lnr_root);
        setContents();
        return view;
    }

    private void setContents() {
        TAG = getClass().getSimpleName();
        HistoryFragment = this;
        sessionManager = new SessionManager(getActivity());
        setRecyclerView();
    }

    public void setRecyclerView() {
        if (sessionManager.getPreferencesHistoryArrayList(getActivity()) != null) {
            if (sessionManager.getPreferencesHistoryArrayList(getActivity()).size() != 0) {
                txtNoData.setVisibility(View.GONE);
                eListView.setVisibility(View.VISIBLE);

                ArrayList<HistorySubData> historyDataArrayList = sessionManager.getPreferencesHistoryArrayList(getActivity());

                ArrayList<HistorySubData> historyToday = new ArrayList<>();
                ArrayList<HistorySubData> historyTowDayAgo = new ArrayList<>();
                ArrayList<HistorySubData> historyOneWeekAgo = new ArrayList<>();

                for (int i = 0; i < historyDataArrayList.size(); i++) {
                    HistorySubData historySubData = new HistorySubData();
                    historySubData.setTitle(historyDataArrayList.get(i).getTitle());
                    historySubData.setTag(historyDataArrayList.get(i).getTag());
                    historySubData.setReadingIndex(historyDataArrayList.get(i).getReadingIndex());
                    historySubData.setDate(historyDataArrayList.get(i).getDate());
                    historySubData.setFromHistory(historyDataArrayList.get(i).isFromHistory());
                    historySubData.setLcId(historyDataArrayList.get(i).getLcId());
                    historySubData.setLsId(historyDataArrayList.get(i).getLsId());
                    historySubData.setItemURI(historyDataArrayList.get(i).getItemURI());

                    if (Global.getCurrentDate().equalsIgnoreCase(historyDataArrayList.get(i).getDate())) {
                        historyToday.add(historySubData);
                    } else if (Global.getDateDiff(new SimpleDateFormat("MMM dd, yyyy", Locale.US), historyDataArrayList.get(i).getDate(), Global.getCurrentDate()) <= 2) {
                        historyTowDayAgo.add(historySubData);
                    } else {
                        historyOneWeekAgo.add(historySubData);
                    }
                }

                ArrayList<HistoryData> mainDataArrayList = new ArrayList<>();
                if (historyToday.size() != 0) {

                    HistoryData historyDataToday = new HistoryData();
                    historyDataToday.setDate(getString(R.string.today));
                    historyDataToday.setHistoryDataArrayList(historyToday);
                    mainDataArrayList.add(historyDataToday);
                }

                if (historyTowDayAgo.size() != 0) {

                    HistoryData historyDataTwoDay = new HistoryData();
                    historyDataTwoDay.setDate(getString(R.string.two_days_ago));
                    historyDataTwoDay.setHistoryDataArrayList(historyTowDayAgo);
                    mainDataArrayList.add(historyDataTwoDay);
                }

                if (historyOneWeekAgo.size() != 0) {

                    HistoryData historyDataOneWeek = new HistoryData();
                    historyDataOneWeek.setDate(getString(R.string.one_week_ago));
                    historyDataOneWeek.setHistoryDataArrayList(historyOneWeekAgo);
                    mainDataArrayList.add(historyDataOneWeek);
                }
                eListView.setAdapter(new HistoryFragmentListAdapter(getActivity(), mainDataArrayList));
                for (int i = 0; i < mainDataArrayList.size(); i++) {
                    eListView.expandGroup(i);
                }
            } else {
                txtNoData.setVisibility(View.VISIBLE);
                eListView.setVisibility(View.GONE);
            }

        } else {
            txtNoData.setVisibility(View.VISIBLE);
            eListView.setVisibility(View.GONE);
        }
    }

}
