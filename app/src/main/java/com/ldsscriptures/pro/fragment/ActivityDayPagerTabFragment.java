package com.ldsscriptures.pro.fragment;

import android.database.Cursor;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.ldsscriptures.pro.R;
import com.ldsscriptures.pro.activity.activitymenu.ActivityScreen;
import com.ldsscriptures.pro.adapter.ActivityChartPagerAdapter;
import com.ldsscriptures.pro.database.CommonDatabaseHelper;
import com.ldsscriptures.pro.model.ActivityData;
import com.ldsscriptures.pro.utils.SessionManager;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * Created by IBL InfoTech on 7/27/2017.
 */

public class ActivityDayPagerTabFragment extends BaseFragment {

    Unbinder unbinder;
    @BindView(R.id.pager_top_layout)
    LinearLayout pagerTopLayout;
    @BindView(R.id.activity_Pager)
    ViewPager activityPager;
    @BindView(R.id.activity_TabLayout)
    TabLayout activityTabLayout;
    @BindView(R.id.imageView3)
    ImageView imageView3;
    @BindView(R.id.pager_bottom_layout)
    LinearLayout pagerBottomLayout;
    @BindView(R.id.activity_total_goalspoint)
    TextView activityTotalGoalspoint;
    @BindView(R.id.txtAvgGoalPoint)
    TextView txtAvgGoalPoint;
    @BindView(R.id.txtActivityDailyGoalpts)
    TextView txtActivityDailyGoalpts;
    @BindView(R.id.txtActivityReadingMinute)
    TextView txtActivityReadingMinute;
    @BindView(R.id.txtActivityAnotationMinute)
    TextView txtActivityAnotationMinute;
    @BindView(R.id.txtActivityAchivementMinute)
    TextView txtActivityAchivementMinute;
    @BindView(R.id.txtActivitydayStreaks)
    TextView txtActivitydayStreaks;
    @BindView(R.id.txt_Avglabel)
    TextView txtAvglabel;
    int postionTab = 0;

    private ArrayList<ActivityData> dayViseActivityArrayList = new ArrayList<>();
    ArrayList<ActivityData> dayChartViseActivityArrayList = new ArrayList<>();
    CommonDatabaseHelper commonDatabaseHelper;
    ActivityChartPagerAdapter mAdapter;
    static ActivityDayPagerTabFragment activityDayPagerTabFragment;
    SessionManager sessionManager;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_activity_day, container, false);
        unbinder = ButterKnife.bind(this, view);
        setContents();
        return view;
    }

    public void setContents() {

        activityDayPagerTabFragment = this;
        commonDatabaseHelper = new CommonDatabaseHelper(getActivity());
        dayViseActivityArrayList = ActivityScreen.getInstance().getdayViseActivityArrayList();

        sessionManager = new SessionManager(getActivity());
        txtActivityDailyGoalpts.setText(sessionManager.getUserGoal() + " " + "PTS");

        if (dayViseActivityArrayList != null && dayViseActivityArrayList.size() != 0) {
            for (int i = 0; i < dayViseActivityArrayList.size(); i++) {
                try {
                    String activityDate = dayViseActivityArrayList.get(i).getActivity_date();

                    SimpleDateFormat gsdf = new SimpleDateFormat("yyyy-MM-dd");
                    SimpleDateFormat sdf = new SimpleDateFormat("EEE, MMM dd");
                    Date d = gsdf.parse(activityDate);
                    activityTabLayout.addTab(activityTabLayout.newTab().setText(sdf.format(d).toUpperCase()));

                    Date date = new Date();

                    if (sdf.format(d).toUpperCase().contains(sdf.format(date).toUpperCase())) {
                        postionTab = i;
                    }


                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            try {
                new Handler().postDelayed(
                        new Runnable() {
                            @Override
                            public void run() {
                                activityTabLayout.getTabAt(postionTab).select();
                                getDayViseChartData(postionTab);
                            }
                        }, 100);
            } catch (Exception e) {
                e.printStackTrace();
            }

            activityTabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
                @Override
                public void onTabSelected(TabLayout.Tab tab) {
                    activityPager.setCurrentItem(tab.getPosition());
                    getDayViseChartData(tab.getPosition());
                }

                @Override
                public void onTabUnselected(TabLayout.Tab tab) {

                }

                @Override
                public void onTabReselected(TabLayout.Tab tab) {

                }
            });

        }

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    public void getDayViseChartData(int pos) {
        try {
            String activityDate = dayViseActivityArrayList.get(pos).getActivity_date();
            selectChartDayViseActivityRecord(activityDate, pos);

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void selectChartDayViseActivityRecord(String varDay, int pos) {
        dayChartViseActivityArrayList.clear();
        Cursor cursor;
        cursor = commonDatabaseHelper.getDayChartViseActivityRecord(varDay);

        if (cursor != null && cursor.moveToFirst()) {
            while (!cursor.isAfterLast()) {
                ActivityData activityData = new ActivityData();
                activityData.setActivity_id(cursor.getInt(cursor.getColumnIndex(CommonDatabaseHelper.KEY_ACTIVITY_ID)));
                activityData.setActivity_date(cursor.getString(cursor.getColumnIndex(CommonDatabaseHelper.KEY_ACTIVITY_DATE)));
                activityData.setActivity_hour(cursor.getInt(cursor.getColumnIndex(CommonDatabaseHelper.KEY_ACTIVITY_HOUR)));
                activityData.setActivity_dayOfWeek(cursor.getInt(cursor.getColumnIndex(CommonDatabaseHelper.KEY_ACTIVITY_DAYOFWEEK)));
                activityData.setActivity_dayOfMonth(cursor.getInt(cursor.getColumnIndex(CommonDatabaseHelper.KEY_ACTIVITY_MONTH)));
                activityData.setActivity_dayOfYear(cursor.getInt(cursor.getColumnIndex(CommonDatabaseHelper.KEY_ACTIVITY_YEAR)));
                activityData.setActivity_DayMonth(cursor.getInt(cursor.getColumnIndex(CommonDatabaseHelper.KEY_ACTIVITY_DAYOFMONTH)));
                activityData.setActivity_goal_point(cursor.getInt(cursor.getColumnIndex(CommonDatabaseHelper.KEY_ACTIVITY_GOAL_POINT)));

                dayChartViseActivityArrayList.add(activityData);
                cursor.moveToNext();
            }

            mAdapter = new ActivityChartPagerAdapter(getActivity(), activityTabLayout.getTabCount(), 0, dayChartViseActivityArrayList);
            activityPager.setAdapter(mAdapter);
            int activityGoalPoint = dayViseActivityArrayList.get(0).getActivity_goal_point();
            int arrayListSize = dayViseActivityArrayList.size();
            ActivityScreen.getInstance().day_strike(txtActivitydayStreaks, txtActivityReadingMinute, txtActivityDailyGoalpts, txtAvgGoalPoint, activityTotalGoalspoint, String.valueOf(activityGoalPoint), arrayListSize, txtActivityAchivementMinute);
            txtAvglabel.setText(R.string.avg_daily);
        }
    }

    public ArrayList<ActivityData> getDayChartViseActivityArrayList() {
        return dayChartViseActivityArrayList;
    }

    public static ActivityDayPagerTabFragment getInstance() {
        return activityDayPagerTabFragment;
    }

}
