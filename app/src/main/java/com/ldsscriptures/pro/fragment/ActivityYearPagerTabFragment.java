package com.ldsscriptures.pro.fragment;

import android.database.Cursor;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.text.format.DateFormat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.ldsscriptures.pro.R;
import com.ldsscriptures.pro.activity.activitymenu.ActivityScreen;
import com.ldsscriptures.pro.adapter.ActivityChartPagerAdapter;
import com.ldsscriptures.pro.database.CommonDatabaseHelper;
import com.ldsscriptures.pro.model.ActivityData;
import com.ldsscriptures.pro.utils.SessionManager;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * Created by IBL InfoTech on 7/27/2017.
 */

public class ActivityYearPagerTabFragment extends BaseFragment {

    Unbinder unbinder;
    @BindView(R.id.pager_top_layout)
    LinearLayout pagerTopLayout;
    @BindView(R.id.activity_Pager)
    ViewPager activityPager;
    @BindView(R.id.activity_TabLayout)
    TabLayout activityTabLayout;
    @BindView(R.id.imageView3)
    ImageView imageView3;
    @BindView(R.id.pager_bottom_layout)
    LinearLayout pagerBottomLayout;
    @BindView(R.id.activity_total_goalspoint)
    TextView activityTotalGoalspoint;
    @BindView(R.id.txtAvgGoalPoint)
    TextView txtAvgGoalPoint;
    @BindView(R.id.txtActivityDailyGoalpts)
    TextView txtActivityDailyGoalpts;
    @BindView(R.id.txtActivityReadingMinute)
    TextView txtActivityReadingMinute;
    @BindView(R.id.txtActivityAnotationMinute)
    TextView txtActivityAnotationMinute;
    @BindView(R.id.txtActivityAchivementMinute)
    TextView txtActivityAchivementMinute;
    @BindView(R.id.txtActivitydayStreaks)
    TextView txtActivitydayStreaks;
    @BindView(R.id.txt_Avglabel)
    TextView txtAvglabel;
    int postionTab = 0;
    SessionManager sessionManager;
    private ArrayList<ActivityData> yearViseActivityArrayList = new ArrayList<>();
    private ArrayList<ActivityData> yearChartViseActivityArrayList = new ArrayList<>();
    CommonDatabaseHelper commonDatabaseHelper;
    ActivityChartPagerAdapter mAdapter;
    static ActivityYearPagerTabFragment activityYearPagerTabFragment;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_activity_day, container, false);
        unbinder = ButterKnife.bind(this, view);
        setContents();
        return view;
    }

    public void setContents() {
        activityYearPagerTabFragment = this;
        commonDatabaseHelper = new CommonDatabaseHelper(getActivity());

        yearViseActivityArrayList = ActivityScreen.getInstance().getyearViseActivityArrayList();

        sessionManager = new SessionManager(getActivity());
        txtActivityDailyGoalpts.setText(sessionManager.getUserGoal() + " " + "PTS");

        if (yearViseActivityArrayList != null && yearViseActivityArrayList.size() != 0) {
            for (int i = 0; i < yearViseActivityArrayList.size(); i++) {
                try {
                    String activityDate = yearViseActivityArrayList.get(i).getActivity_date();
                    SimpleDateFormat gsdf = new SimpleDateFormat("yyyy-MM-dd");
                    Date dt1 = gsdf.parse(activityDate);
                    String monthString = (String) DateFormat.format("yyyy", dt1);

                    activityTabLayout.addTab(activityTabLayout.newTab().setText(monthString));

                    Date date = new Date();
                    String monthString1 = (String) DateFormat.format("yyyy", date);

                    if (monthString.contains(monthString1)) {
                        postionTab = i;
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            try {
                new Handler().postDelayed(
                        new Runnable() {
                            @Override
                            public void run() {
                                activityTabLayout.getTabAt(postionTab).select();
                                getYearViseChartData(postionTab);
                            }
                        }, 100);
            } catch (Exception e) {
                e.printStackTrace();
            }

            activityTabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
                @Override
                public void onTabSelected(TabLayout.Tab tab) {
                    activityPager.setCurrentItem(tab.getPosition());
                    getYearViseChartData(tab.getPosition());
                }

                @Override
                public void onTabUnselected(TabLayout.Tab tab) {

                }

                @Override
                public void onTabReselected(TabLayout.Tab tab) {

                }
            });

        }

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    public void getYearViseChartData(int pos) {
        try {
            String activityDate = yearViseActivityArrayList.get(pos).getActivity_date();
            SimpleDateFormat gsdf = new SimpleDateFormat("yyyy-MM-dd");
            Date dt1 = gsdf.parse(activityDate);
            String varYear = (String) DateFormat.format("yyyy", dt1);
            selectChartYearViseActivityRecord(varYear);

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void selectChartYearViseActivityRecord(String varYear) {
        yearChartViseActivityArrayList.clear();
        Cursor cursor;
        cursor = commonDatabaseHelper.getYearChartViseActivityRecord(varYear);
        if (cursor != null && cursor.moveToFirst()) {
            while (!cursor.isAfterLast()) {

                ActivityData activityData = new ActivityData();
                activityData.setActivity_id(cursor.getInt(cursor.getColumnIndex(CommonDatabaseHelper.KEY_ACTIVITY_ID)));
                activityData.setActivity_date(cursor.getString(cursor.getColumnIndex(CommonDatabaseHelper.KEY_ACTIVITY_DATE)));
                activityData.setActivity_hour(cursor.getInt(cursor.getColumnIndex(CommonDatabaseHelper.KEY_ACTIVITY_HOUR)));
                activityData.setActivity_dayOfWeek(cursor.getInt(cursor.getColumnIndex(CommonDatabaseHelper.KEY_ACTIVITY_DAYOFWEEK)));
                activityData.setActivity_dayOfMonth(cursor.getInt(cursor.getColumnIndex(CommonDatabaseHelper.KEY_ACTIVITY_MONTH)));
                activityData.setActivity_dayOfYear(cursor.getInt(cursor.getColumnIndex(CommonDatabaseHelper.KEY_ACTIVITY_YEAR)));
                activityData.setActivity_goal_point(cursor.getInt(cursor.getColumnIndex(CommonDatabaseHelper.KEY_ACTIVITY_GOAL_POINT)));

                yearChartViseActivityArrayList.add(activityData);
                cursor.moveToNext();
            }

            mAdapter = new ActivityChartPagerAdapter(getActivity(), activityTabLayout.getTabCount(), 3, yearChartViseActivityArrayList);
            activityPager.setAdapter(mAdapter);
            int activityGoalPoint = yearViseActivityArrayList.get(0).getActivity_goal_point();
            int arrayListSize = yearViseActivityArrayList.size();
            ActivityScreen.getInstance().day_strike(txtActivitydayStreaks, txtActivityReadingMinute, txtActivityDailyGoalpts, txtAvgGoalPoint, activityTotalGoalspoint, String.valueOf(activityGoalPoint), arrayListSize, txtActivityAchivementMinute);
            txtAvglabel.setText(R.string.avg_yearly);
        }
    }

    public static ActivityYearPagerTabFragment getInstance() {
        return activityYearPagerTabFragment;
    }
}
