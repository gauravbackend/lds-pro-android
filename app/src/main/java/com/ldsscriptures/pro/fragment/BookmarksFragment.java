package com.ldsscriptures.pro.fragment;

import android.database.Cursor;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.ldsscriptures.pro.R;
import com.ldsscriptures.pro.activity.MainActivity;
import com.ldsscriptures.pro.adapter.BookmarkFragmentListAdapter;
import com.ldsscriptures.pro.database.CommonDatabaseHelper;
import com.ldsscriptures.pro.model.BookmarkData;
import com.ldsscriptures.pro.utils.EditItemTouchHelperCallback;
import com.ldsscriptures.pro.utils.SessionManager;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;


public class BookmarksFragment extends BaseFragment {

    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    @BindView(R.id.tvNoData)
    TextView txtNoData;

    private SessionManager sessionManager;
    static BookmarksFragment bookmarksFragment;
    ArrayList<BookmarkData> bookmarkDataArrayList = new ArrayList<>();
    private CommonDatabaseHelper commonDatabaseHelper;

    public static BookmarksFragment getInstance() {
        return bookmarksFragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_bookmarks, container, false);
        RelativeLayout lnr_root = (RelativeLayout) view.findViewById(R.id.lnr_root);
        ButterKnife.bind(this, lnr_root);
        setContents();
        return view;
    }

    private void setContents() {
        bookmarksFragment = this;
        sessionManager = new SessionManager(getActivity());
        commonDatabaseHelper = new CommonDatabaseHelper(getContext());
        setRecyclerView();
    }

    @Override
    public void onResume() {
        super.onResume();
        if (recyclerView != null)
            setRecyclerView();
    }

    private void setRecyclerView() {
        bookmarkDataArrayList.clear();
        Cursor cursor;
        cursor = commonDatabaseHelper.getBookmarkList();


        if (cursor != null && cursor.moveToFirst()) {
            while (!cursor.isAfterLast()) {
                String deleted = cursor.getString(cursor.getColumnIndex("deleted"));
                if (deleted.isEmpty()) {
                    BookmarkData bookmarkData = new BookmarkData();
                    bookmarkData.setServerid(cursor.getString(cursor.getColumnIndex("serverid")));
                    bookmarkData.setVerseaid(cursor.getString(cursor.getColumnIndex("verseaid")));
                    bookmarkData.setChapterUri(cursor.getString(cursor.getColumnIndex("chapteruri")));
                    bookmarkData.setItemuri(cursor.getString(cursor.getColumnIndex("itemuri")));
                    bookmarkData.setBookuri(cursor.getString(cursor.getColumnIndex("bookuri")));
                    bookmarkData.setTitle(cursor.getString(cursor.getColumnIndex("versetitle")));
                    bookmarkData.setVerseText(cursor.getString(cursor.getColumnIndex("versetext")));
                    bookmarkData.setVerseNumber(cursor.getString(cursor.getColumnIndex("versenumber")));
                    bookmarkData.setDeleted(deleted);
                    bookmarkData.setModified(cursor.getString(cursor.getColumnIndex("modified")));
                    bookmarkDataArrayList.add(bookmarkData);
                }
                cursor.moveToNext();
            }
        }

        if (bookmarkDataArrayList != null) {
            if (bookmarkDataArrayList.size() != 0) {
                txtNoData.setVisibility(View.GONE);
                recyclerView.setVisibility(View.VISIBLE);

                String paraUserId = "", paraSessionId = "";

                if (MainActivity.getInstance().getUserInfo(SessionManager.KEY_USER_ID) != null) {
                    paraUserId = MainActivity.getInstance().getUserInfo(SessionManager.KEY_USER_ID);
                }
                if (MainActivity.getInstance().getUserInfo(SessionManager.KEY_USER_SESSION_ID) != null) {
                    paraSessionId = MainActivity.getInstance().getUserInfo(SessionManager.KEY_USER_SESSION_ID);
                }

                BookmarkFragmentListAdapter bookmarkFragmentListAdapter = new BookmarkFragmentListAdapter(getActivity(), bookmarkDataArrayList, paraUserId, paraSessionId);
                LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity(), RecyclerView.VERTICAL, false);
                recyclerView.setLayoutManager(layoutManager);
                recyclerView.setAdapter(bookmarkFragmentListAdapter);

                ItemTouchHelper.Callback callback = new EditItemTouchHelperCallback(bookmarkFragmentListAdapter);
                ItemTouchHelper touchHelper = new ItemTouchHelper(callback);
                touchHelper.attachToRecyclerView(recyclerView);

            } else {
                txtNoData.setVisibility(View.VISIBLE);
                recyclerView.setVisibility(View.GONE);
                txtNoData.setText(R.string.no_bookmarks);
            }
        } else {
            txtNoData.setText(R.string.no_bookmarks);
            txtNoData.setVisibility(View.VISIBLE);
            recyclerView.setVisibility(View.GONE);
        }
    }

    public void visibleHideView() {
        txtNoData.setText(R.string.no_bookmarks);
        txtNoData.setVisibility(View.VISIBLE);
        recyclerView.setVisibility(View.GONE);
    }
}
