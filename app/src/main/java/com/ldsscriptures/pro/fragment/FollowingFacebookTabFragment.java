package com.ldsscriptures.pro.fragment;

import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphRequestAsyncTask;
import com.facebook.GraphResponse;
import com.facebook.HttpMethod;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.ldsscriptures.pro.R;
import com.ldsscriptures.pro.activity.socialmenu.FollowingFindFriendActivity;
import com.ldsscriptures.pro.adapter.FollowingFindFriendListAdapter;
import com.ldsscriptures.pro.database.CommonDatabaseHelper;
import com.ldsscriptures.pro.model.EmailFriendData;
import com.ldsscriptures.pro.model.FollowingModel;
import com.ldsscriptures.pro.model.UserDetail;
import com.ldsscriptures.pro.retroutils.APIClient;
import com.ldsscriptures.pro.retroutils.APIInterface;
import com.ldsscriptures.pro.utils.Global;
import com.ldsscriptures.pro.utils.SessionManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Arrays;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by IBL InfoTech on 7/27/2017.
 */

public class FollowingFacebookTabFragment extends BaseFragment {

    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    @BindView(R.id.txtsearch)
    TextView txtsearch;
    @BindView(R.id.txtsee_which)
    TextView txtseeWhich;
    @BindView(R.id.txtalready_here)
    TextView txtalreadyHere;
    @BindView(R.id.btn_chkList)
    Button btnChkList;
    @BindView(R.id.lay_firstLoad)
    LinearLayout layFirstLoad;

    String paraUserId, paraSessionId, paraFriendFbId = "", paraFbLoginUserID = "";

    Unbinder unbinder;
    CommonDatabaseHelper commonDatabaseHelper;
    ArrayList<EmailFriendData> followingarraylistNew = new ArrayList<>();
    CallbackManager callbackManager;
    FollowingFindFriendListAdapter mAdapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        facebookSDKInitialize();
        LoginManager.getInstance().registerCallback(callbackManager,
                new FacebookCallback<LoginResult>() {
                    @Override
                    public void onSuccess(LoginResult loginResult) {

                        paraFbLoginUserID = loginResult.getAccessToken().getUserId();
                        updateUserFbId();

                        GraphRequestAsyncTask graphRequestAsyncTask = new GraphRequest(
                                loginResult.getAccessToken(),
                                "/me/taggable_friends",
                                null,
                                HttpMethod.GET,
                                new GraphRequest.Callback() {
                                    public void onCompleted(GraphResponse response) {

                                        try {
                                            JSONArray rawName = response.getJSONObject().getJSONArray("data");
                                            for (int i = 0; i < rawName.length(); i++) {
                                                JSONObject jsonObj = rawName.getJSONObject(i);
                                                String friend_Id = jsonObj.getString("id");
                                                paraFriendFbId = paraFriendFbId + "friend_fb_id[]=" + friend_Id + "&";
                                            }
                                            getFacebookFriendAPI(paraUserId, paraSessionId, paraFriendFbId);

                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        }
                                    }
                                }
                        ).executeAsync();
                    }

                    @Override
                    public void onCancel() {
                        Global.printLog("LOG", "CANCEL");
                    }

                    @Override
                    public void onError(FacebookException exception) {
                        Global.printLog("LOG", "ERROR" + exception.getMessage().toString());
                    }
                });

        View view = inflater.inflate(R.layout.followingtabfragment, container, false);
        unbinder = ButterKnife.bind(this, view);
        setContents();

        return view;
    }

    public void setContents() {


        if (FollowingFindFriendActivity.getInstance().getUserInfo(SessionManager.KEY_USER_ID) != null) {
            paraUserId = FollowingFindFriendActivity.getInstance().getUserInfo(SessionManager.KEY_USER_ID);
        }
        if (FollowingFindFriendActivity.getInstance().getUserInfo(SessionManager.KEY_USER_SESSION_ID) != null) {
            paraSessionId = FollowingFindFriendActivity.getInstance().getUserInfo(SessionManager.KEY_USER_SESSION_ID);
        }

        try {
            PackageInfo info = getActivity().getPackageManager().getPackageInfo(
                    getActivity().getPackageName(),
                    PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
            }
        } catch (PackageManager.NameNotFoundException e) {

        } catch (NoSuchAlgorithmException e) {

        }

        commonDatabaseHelper = new CommonDatabaseHelper(getActivity());

        recyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(layoutManager);

        txtsearch.setText(getResources().getString(R.string.search_facebook));
        txtseeWhich.setText(getResources().getString(R.string.see_which_of_your_friends));
        txtalreadyHere.setText(getResources().getString(R.string.are_already_here));
        btnChkList.setText(getResources().getString(R.string.chk_facebook));
        btnChkList.setBackgroundColor(getResources().getColor(R.color.facebook_color));
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick(R.id.btn_chkList)
    public void onViewClicked() {
        LoginManager.getInstance().logInWithReadPermissions(this, Arrays.asList("public_profile", "user_friends"));
    }

    protected void facebookSDKInitialize() {
        FacebookSdk.sdkInitialize(getActivity());
        callbackManager = CallbackManager.Factory.create();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }

    private void updateUserFbId() {

        APIInterface apiService = APIClient.getClient().create(APIInterface.class);
        String url = APIClient.BASE_URL;

        url += "updateUser.pl?user_id=" + paraUserId +
                "&session_id=" + paraSessionId +
                "&apiauth=" + APIClient.AUTH_KEY +
                "&fb_id=" + paraFbLoginUserID;

        Call<UserDetail> callObject = apiService.updateUserInfo(url.replaceAll(" ", "%20"));

        callObject.enqueue(new Callback<UserDetail>() {
            @Override
            public void onResponse(Call<UserDetail> call, Response<UserDetail> response) {

                if (response.body() != null && !response.body().toString().trim().isEmpty()) {
                    if (Integer.parseInt(response.body().getSuccess()) == 0) {

                    }
                }
            }

            @Override
            public void onFailure(Call<UserDetail> call, Throwable t) {
                call.cancel();

            }
        });
    }

    private void getFacebookFriendAPI(String paraUserId, String paraSessionId, String paraFriendFbId) {

        FollowingFindFriendActivity.getInstance().showprogress();
        followingarraylistNew.clear();

        APIInterface apiService = APIClient.getClient().create(APIInterface.class);
        String url = APIClient.BASE_URL;

        url += "checkFriends.pl?user_id=" + paraUserId +
                "&session_id=" + paraSessionId +
                "&apiauth=" + APIClient.AUTH_KEY + "&" +
                paraFriendFbId;

        Call<FollowingModel<EmailFriendData>> callObject = apiService.getFollowingContactEmail(url.replaceAll(" ", "%20"));
        callObject.enqueue(new Callback<FollowingModel<EmailFriendData>>() {

            @Override
            public void onResponse(Call<FollowingModel<EmailFriendData>> call, Response<FollowingModel<EmailFriendData>> response) {

                if (response.body() != null && !response.body().toString().trim().isEmpty()) {

                    if (response.body().getSuccess().equals("true")) {
                        followingarraylistNew = response.body().getFb_friend();

                        for (int i = 0; i < followingarraylistNew.size(); i++) {
                            followingarraylistNew.get(i).setFriend_flag(true);
                            followingarraylistNew.get(i).setFriend_invite_flag(false);
                        }

                        recyclerView.setVisibility(View.VISIBLE);
                        layFirstLoad.setVisibility(View.GONE);
                        mAdapter = new FollowingFindFriendListAdapter(getActivity(), followingarraylistNew);
                        recyclerView.setAdapter(mAdapter);
                        FollowingFindFriendActivity.getInstance().dismissprogress();
                    } else {
                        Global.showToast(getActivity(), getString(R.string.no_friend_found));
                        FollowingFindFriendActivity.getInstance().dismissprogress();
                    }

                }
            }

            @Override
            public void onFailure(Call<FollowingModel<EmailFriendData>> call, Throwable t) {
                FollowingFindFriendActivity.getInstance().dismissprogress();
            }
        });
    }
}
