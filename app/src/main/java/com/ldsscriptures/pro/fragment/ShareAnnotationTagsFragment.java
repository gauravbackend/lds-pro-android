package com.ldsscriptures.pro.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.ldsscriptures.pro.R;
import com.ldsscriptures.pro.activity.ShareAnnotationsActivity;
import com.ldsscriptures.pro.adapter.ShareAnnotationTagAdapter;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class ShareAnnotationTagsFragment extends BaseFragment {

    Unbinder unbinder;
    @BindView(R.id.tvNoData)
    TextView tvNoData;

    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_notes_share_annotation, container, false);
        unbinder = ButterKnife.bind(this, view);
        setContents();
        return view;
    }

    public void setContents() {
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));

        if (ShareAnnotationsActivity.tagsDataArrayList.size() != 0) {
            recyclerView.setVisibility(View.VISIBLE);
            tvNoData.setVisibility(View.GONE);


            ShareAnnotationTagAdapter shareAnnotationTagAdapter = new ShareAnnotationTagAdapter(getActivity(), ShareAnnotationsActivity.tagsDataArrayList);
            recyclerView.setAdapter(shareAnnotationTagAdapter);
        } else {
            recyclerView.setVisibility(View.GONE);
            tvNoData.setVisibility(View.VISIBLE);
            tvNoData.setText(R.string.no_tag_found);
        }

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }
}