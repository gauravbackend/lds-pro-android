package com.ldsscriptures.pro.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;
import android.widget.LinearLayout;

import com.google.gson.JsonObject;
import com.ldsscriptures.pro.BuildConfig;
import com.ldsscriptures.pro.R;
import com.ldsscriptures.pro.activity.LibrarySelectionTabsActivity;
import com.ldsscriptures.pro.adapter.LcGridAdapter;
import com.ldsscriptures.pro.database.DataBaseHelper;
import com.ldsscriptures.pro.dbClass.GetLibCollectionFromDB;
import com.ldsscriptures.pro.model.Item;
import com.ldsscriptures.pro.model.LcData;
import com.ldsscriptures.pro.model.MoreOptionMenuData;
import com.ldsscriptures.pro.retroutils.APIClient;
import com.ldsscriptures.pro.retroutils.APIInterface;
import com.ldsscriptures.pro.service.LDSMainDBService;
import com.ldsscriptures.pro.utils.Global;
import com.ldsscriptures.pro.utils.SessionManager;

import java.io.File;
import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnItemClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.facebook.internal.Utility.deleteDirectory;

public class LibraryCollectionFragment extends BaseFragment {

    @BindView(R.id.gridView)
    GridView gridView;

    private DataBaseHelper databasehelper;
    private ArrayList<LcData> lcDataArrayList = new ArrayList<>();
    static LibraryCollectionFragment libraryCollectionFragment;
    String fileName = "DBVERSION";
    private SessionManager sessionManager;

    public static LibraryCollectionFragment getInstance() {
        return libraryCollectionFragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_library_collecation, container, false);
        LinearLayout lnr_root = (LinearLayout) view.findViewById(R.id.lnr_root);
        ButterKnife.bind(this, lnr_root);
        setContents();
        return view;
    }

    private void setContents() {

        libraryCollectionFragment = this;
        askForPermission();
        databasehelper = new DataBaseHelper(getActivity());
        sessionManager = new SessionManager(getActivity());
        Global.showProgressDialog(getActivity());
        getDatabaseVersion();
    }

    public void checkDatabse() {
        if (databasehelper.checkDataBase()) {
            lcDataArrayList = new GetLibCollectionFromDB(getActivity()).getLcArrayListFromDB();
            if (lcDataArrayList != null && lcDataArrayList.size() > 0) {

                String getLanguage = sessionManager.getLanguageCode();
                if (getLanguage.equals("eng")) {
                    for (int i = 0; i < lcDataArrayList.size(); i++) {
                        if (lcDataArrayList.get(i).getTitle().equals("Music")) {
                            lcDataArrayList.remove(i);
                        }
                    }
                }
                Global.dismissProgressDialog();
                LcGridAdapter lcGridAdapter = new LcGridAdapter(getActivity(), lcDataArrayList);
                gridView.setAdapter(lcGridAdapter);
            }
        } else {
            getActivity().startService(new Intent(getActivity(), LDSMainDBService.class));
        }
    }

    @OnItemClick(R.id.gridView)
    public void openLSActivity(int position) {

        Global.selectedLibCollection = lcDataArrayList.get(position).getTitle();
        Global.moreOptionMenuDatas.set(0, new MoreOptionMenuData(lcDataArrayList.get(position).getId(), lcDataArrayList.get(position).getTitle()));

        Intent intent = new Intent(getActivity(), LibrarySelectionTabsActivity.class);
        intent.putExtra(getString(R.string.lcID), lcDataArrayList.get(position).getId());
        intent.putExtra(getString(R.string.lcname), lcDataArrayList.get(position).getTitle());
        startActivityWithAnimation(intent);
    }

    public String getItemURL(int position) {
        ArrayList<Item> MainArrayList = Global.getListSharedPreferneces(getActivity());
        if (MainArrayList != null)
            for (int i = 0; i < MainArrayList.size(); i++) {
                if (MainArrayList.get(i).getId().equals(String.valueOf(position))) {
                    return MainArrayList.get(i).getUrl();
                }
            }
        return null;
    }

    private void getDatabaseVersion() {

        APIInterface apiService = APIClient.getClient().create(APIInterface.class);
        String url = APIClient.BASE_URL;

        url += "latestDB.pl?device=" + APIClient.DEVICE_KEY +
                "&appversion=" + BuildConfig.VERSION_NAME +
                "&apiauth=" + APIClient.AUTH_KEY;

        Call<JsonObject> callObject = apiService.getDBVersion(url);

        callObject.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {

                if (response.body() != null && !response.body().toString().trim().isEmpty()) {

                    String latestDbVersion = response.body().get("dbversion").toString();
                    String oldDbVersion = sessionManager.getKeyOldDbVersion();

                    if (!latestDbVersion.isEmpty()) {
                        if (!oldDbVersion.isEmpty()) {
                            if (oldDbVersion.equals(latestDbVersion)) {
                                checkDatabse();
                            } else {
                                sessionManager.setKeyOldDbVersion(latestDbVersion);

                                String PathofLDS = Environment.getExternalStorageDirectory() + "/LDS/";
                                File dir = new File(PathofLDS);
                                if (dir.exists()) {
                                    File[] files = dir.listFiles();
                                    for (int i = 0; i < files.length; i++) {
                                        if (files[i].isDirectory()) {
                                            deleteDirectory(files[i]);
                                        } else {
                                            files[i].delete();
                                        }
                                    }
                                    checkDatabse();
                                }
                            }
                        } else {
                            sessionManager.setKeyOldDbVersion(latestDbVersion);
                            checkDatabse();
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                call.cancel();

            }
        });
    }

}
