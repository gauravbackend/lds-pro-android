package com.ldsscriptures.pro.fragment;

import android.database.Cursor;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.text.format.DateFormat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.ldsscriptures.pro.R;
import com.ldsscriptures.pro.activity.activitymenu.ActivityScreen;
import com.ldsscriptures.pro.adapter.ActivityChartPagerAdapter;
import com.ldsscriptures.pro.database.CommonDatabaseHelper;
import com.ldsscriptures.pro.model.ActivityData;
import com.ldsscriptures.pro.utils.SessionManager;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * Created by IBL InfoTech on 7/27/2017.
 */

public class ActivityWeekPagerTabFragment extends BaseFragment {

    Unbinder unbinder;
    @BindView(R.id.pager_top_layout)
    LinearLayout pagerTopLayout;
    @BindView(R.id.activity_Pager)
    ViewPager activityPager;
    @BindView(R.id.activity_TabLayout)
    TabLayout activityTabLayout;
    @BindView(R.id.imageView3)
    ImageView imageView3;
    @BindView(R.id.pager_bottom_layout)
    LinearLayout pagerBottomLayout;
    @BindView(R.id.activity_total_goalspoint)
    TextView activityTotalGoalspoint;
    @BindView(R.id.txtAvgGoalPoint)
    TextView txtAvgGoalPoint;
    @BindView(R.id.txtActivityDailyGoalpts)
    TextView txtActivityDailyGoalpts;
    @BindView(R.id.txtActivityReadingMinute)
    TextView txtActivityReadingMinute;
    @BindView(R.id.txtActivityAnotationMinute)
    TextView txtActivityAnotationMinute;
    @BindView(R.id.txtActivityAchivementMinute)
    TextView txtActivityAchivementMinute;
    @BindView(R.id.txtActivitydayStreaks)
    TextView txtActivitydayStreaks;
    @BindView(R.id.txt_Avglabel)
    TextView txtAvglabel;
    int postionTab = 0;
    SessionManager sessionManager;
    private ArrayList<ActivityData> weekViseActivityArrayList = new ArrayList<>();
    private ArrayList<ActivityData> weekChartViseActivityArrayList = new ArrayList<>();
    CommonDatabaseHelper commonDatabaseHelper;
    ActivityChartPagerAdapter mAdapter;
    static ActivityWeekPagerTabFragment activityWeekPagerTabFragment;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_activity_day, container, false);
        unbinder = ButterKnife.bind(this, view);
        setContents();
        return view;
    }

    public void setContents() {
        activityWeekPagerTabFragment = this;
        commonDatabaseHelper = new CommonDatabaseHelper(getActivity());
        weekViseActivityArrayList = ActivityScreen.getInstance().getWeekViseActivityArrayList();

        sessionManager = new SessionManager(getActivity());
        txtActivityDailyGoalpts.setText(sessionManager.getUserGoal() + " " + "PTS");

        if (weekViseActivityArrayList != null && weekViseActivityArrayList.size() != 0) {
            for (int i = 0; i < weekViseActivityArrayList.size(); i++) {
                try {
                    String activityWeekStart = weekViseActivityArrayList.get(i).getWeekStart();
                    String activityWeekEnd = weekViseActivityArrayList.get(i).getWeekEnd();

                    SimpleDateFormat gsdf = new SimpleDateFormat("yyyy-MM-dd");
                    Date dt1 = gsdf.parse(activityWeekStart);
                    Date dt2 = gsdf.parse(activityWeekEnd);
                    Date date = new Date();

                    String weekStartMonth = (String) DateFormat.format("MM", dt1);
                    String weekEndMonth = (String) DateFormat.format("MM", dt2);
                    String weekString = "";
                    if (weekStartMonth.equals(weekEndMonth)) {

                        if (dt1.equals(dt2)) {

                            Date myDate = gsdf.parse(activityWeekStart);
                            Calendar cal1 = Calendar.getInstance();
                            cal1.setTime(myDate);
                            cal1.add(Calendar.DAY_OF_YEAR, -6);
                            Date previousDate = cal1.getTime();

                            String weekStartString = (String) DateFormat.format("MMM dd", previousDate);
                            String weekEndString = (String) DateFormat.format("MMM dd", dt2);
                            weekString = weekStartString + " - " + weekEndString;

                        } else {
                            String weekStartString = (String) DateFormat.format("MMM dd", dt1);
                            String weekEndString = (String) DateFormat.format("dd", dt2);
                            weekString = weekStartString + " - " + weekEndString;
                        }

                    } else {

                        String weekStartString = (String) DateFormat.format("MMM dd", dt1);
                        String weekEndString = (String) DateFormat.format("MMM dd", dt2);
                        weekString = weekStartString + " - " + weekEndString;
                    }

                    activityTabLayout.addTab(activityTabLayout.newTab().setText(weekString.toUpperCase()));

                    if (gsdf.format(dt1).toUpperCase().contains(gsdf.format(date).toUpperCase()) ||
                            gsdf.format(dt2).toUpperCase().contains(gsdf.format(date).toUpperCase())) {
                        postionTab = i;
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            try {
                new Handler().postDelayed(
                        new Runnable() {
                            @Override
                            public void run() {
                                activityTabLayout.getTabAt(postionTab).select();
                                getWeekViseChartData(postionTab);
                            }
                        }, 100);
            } catch (Exception e) {
                e.printStackTrace();
            }

            activityTabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
                @Override
                public void onTabSelected(TabLayout.Tab tab) {
                    activityPager.setCurrentItem(tab.getPosition());
                    getWeekViseChartData(tab.getPosition());
                }

                @Override
                public void onTabUnselected(TabLayout.Tab tab) {

                }

                @Override
                public void onTabReselected(TabLayout.Tab tab) {

                }
            });

        }

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    public void getWeekViseChartData(int pos) {
        try {

            String activityWeekStart = weekViseActivityArrayList.get(pos).getWeekStart();
            String activityWeekEnd = weekViseActivityArrayList.get(pos).getWeekEnd();
            selectChartWeekViseActivityRecord(activityWeekStart, activityWeekEnd);

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void selectChartWeekViseActivityRecord(String weekStartString, String weekEndString) {
        weekChartViseActivityArrayList.clear();
        Cursor cursor;

        cursor = commonDatabaseHelper.getWeekChartViseActivityRecord(weekStartString, weekEndString);

        if (cursor != null && cursor.moveToFirst()) {
            while (!cursor.isAfterLast()) {

                ActivityData activityData = new ActivityData();
                activityData.setActivity_id(cursor.getInt(cursor.getColumnIndex(CommonDatabaseHelper.KEY_ACTIVITY_ID)));
                activityData.setActivity_date(cursor.getString(cursor.getColumnIndex(CommonDatabaseHelper.KEY_ACTIVITY_DATE)));
                activityData.setActivity_hour(cursor.getInt(cursor.getColumnIndex(CommonDatabaseHelper.KEY_ACTIVITY_HOUR)));
                activityData.setActivity_dayOfWeek(cursor.getInt(cursor.getColumnIndex(CommonDatabaseHelper.KEY_ACTIVITY_DAYOFWEEK)));
                activityData.setActivity_dayOfMonth(cursor.getInt(cursor.getColumnIndex(CommonDatabaseHelper.KEY_ACTIVITY_MONTH)));
                activityData.setActivity_dayOfYear(cursor.getInt(cursor.getColumnIndex(CommonDatabaseHelper.KEY_ACTIVITY_YEAR)));
                activityData.setActivity_DayMonth(cursor.getInt(cursor.getColumnIndex(CommonDatabaseHelper.KEY_ACTIVITY_DAYOFMONTH)));
                activityData.setActivity_goal_point(cursor.getInt(cursor.getColumnIndex(CommonDatabaseHelper.KEY_ACTIVITY_GOAL_POINT)));

                weekChartViseActivityArrayList.add(activityData);
                cursor.moveToNext();
            }

            mAdapter = new ActivityChartPagerAdapter(getActivity(), activityTabLayout.getTabCount(), 1, weekChartViseActivityArrayList);
            activityPager.setAdapter(mAdapter);
            int activityGoalPoint = weekViseActivityArrayList.get(0).getActivity_goal_point();
            int arrayListSize = weekViseActivityArrayList.size();
            ActivityScreen.getInstance().day_strike(txtActivitydayStreaks, txtActivityReadingMinute, txtActivityDailyGoalpts, txtAvgGoalPoint, activityTotalGoalspoint, String.valueOf(activityGoalPoint), arrayListSize, txtActivityAchivementMinute);
            txtAvglabel.setText(R.string.avg_weekly);
        }
    }

    public ArrayList<ActivityData> getWeekChartViseActivityArrayList() {
        return weekChartViseActivityArrayList;
    }

    public static ActivityWeekPagerTabFragment getInstance() {
        return activityWeekPagerTabFragment;
    }
}
