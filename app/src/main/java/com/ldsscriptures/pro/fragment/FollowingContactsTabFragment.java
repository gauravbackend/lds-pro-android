package com.ldsscriptures.pro.fragment;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.ldsscriptures.pro.R;
import com.ldsscriptures.pro.activity.socialmenu.FollowingFindFriendActivity;
import com.ldsscriptures.pro.adapter.FollowingFindFriendListAdapter;
import com.ldsscriptures.pro.database.CommonDatabaseHelper;
import com.ldsscriptures.pro.model.EmailFriendData;
import com.ldsscriptures.pro.model.FollowingModel;
import com.ldsscriptures.pro.retroutils.APIClient;
import com.ldsscriptures.pro.retroutils.APIInterface;
import com.ldsscriptures.pro.utils.Global;
import com.ldsscriptures.pro.utils.SessionManager;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import permission.auron.com.marshmallowpermissionhelper.PermissionResult;
import permission.auron.com.marshmallowpermissionhelper.PermissionUtils;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class FollowingContactsTabFragment extends BaseFragment {

    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    @BindView(R.id.txtsearch)
    TextView txtsearch;
    @BindView(R.id.txtsee_which)
    TextView txtseeWhich;
    @BindView(R.id.txtalready_here)
    TextView txtalreadyHere;
    @BindView(R.id.btn_chkList)
    Button btnChkList;
    @BindView(R.id.lay_firstLoad)
    LinearLayout layFirstLoad;
    @BindView(R.id.tvNoData)
    TextView tvNoData;
    @BindView(R.id.lin_main_layout)
    LinearLayout linMainLayout;

    Unbinder unbinder;
    CommonDatabaseHelper commonDatabaseHelper;

    ArrayList<EmailFriendData> followingarraylistNew = new ArrayList<>();
    ArrayList<EmailFriendData> localtempArrayList = new ArrayList<>();
    ArrayList<EmailFriendData> dblocalTempArrayList = new ArrayList<>();

    String paraFriendEmail = "";
    String paraUserId;
    String paraSessionId;

    FollowingFindFriendListAdapter mAdapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.followingtabfragment, container, false);
        unbinder = ButterKnife.bind(this, view);
        setContents();
        return view;
    }

    public void setContents() {

        askCompactPermissions(new String[]{PermissionUtils.Manifest_READ_EXTERNAL_STORAGE, PermissionUtils.Manifest_WRITE_EXTERNAL_STORAGE,
                PermissionUtils.Manifest_READ_CONTACTS}, new PermissionResult() {
            @Override
            public void permissionGranted() {
            }

            @Override
            public void permissionDenied() {
            }

            @Override
            public void permissionForeverDenied() {
            }
        });

        commonDatabaseHelper = new CommonDatabaseHelper(getActivity());

        recyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(layoutManager);

        txtsearch.setText(getResources().getString(R.string.search_contact));
        txtseeWhich.setText(getResources().getString(R.string.see_which_of_your_contacts));
        txtalreadyHere.setText(getResources().getString(R.string.are_already_here));
        btnChkList.setText(getResources().getString(R.string.chk_contacts));
        btnChkList.setBackgroundColor(getResources().getColor(R.color.sand));

        btnChkList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                new GetContent().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, "");
            }
        });

    }

    private class GetContent extends AsyncTask<String, Void, String> {

        @Override
        protected void onPreExecute() {
            FollowingFindFriendActivity.getInstance().showprogress();
        }

        @Override
        protected String doInBackground(String... params) {

            getNameEmailDetails();
            return null;
        }

        @Override
        protected void onPostExecute(String result) {

            linMainLayout.setVisibility(View.VISIBLE);
            layFirstLoad.setVisibility(View.GONE);
            FollowingFindFriendActivity.getInstance().dismissprogress();
        }
    }

    public void getNameEmailDetails() {

        ContentResolver cr = getActivity().getContentResolver();
        Cursor cur = cr.query(ContactsContract.Contacts.CONTENT_URI, null, null, null, null);
        if (cur.getCount() > 0) {
            while (cur.moveToNext()) {
                String id = cur.getString(cur.getColumnIndex(ContactsContract.Contacts._ID));
                Cursor cur1 = cr.query(
                        ContactsContract.CommonDataKinds.Email.CONTENT_URI, null,
                        ContactsContract.CommonDataKinds.Email.CONTACT_ID + " = ?",
                        new String[]{id}, null);
                while (cur1.moveToNext()) {

                    String name = cur1.getString(cur1.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME));
                    String email = cur1.getString(cur1.getColumnIndex(ContactsContract.CommonDataKinds.Email.DATA));

                    String contactNo = "";
                    Cursor pCur = cr.query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null, ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = ?", new String[]{id}, null);
                    while (pCur.moveToNext()) {
                        contactNo = pCur.getString(pCur.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
                    }

                    if (email != null) {

                        EmailFriendData followingModel = new EmailFriendData();
                        followingModel.setFriend_name(name);
                        followingModel.setFriend_email(email);
                        followingModel.setFriend_level(contactNo);
                        followingModel.setFriend_flag(false);
                        followingModel.setFriend_invite_flag(false);
                        localtempArrayList.add(followingModel);

                        paraFriendEmail = paraFriendEmail + "friend_email[]=" + email + "&";

                    }
                }
                cur1.close();
            }
        }

        Cursor cursor;
        cursor = commonDatabaseHelper.getFollowingList();
        if (cursor != null && cursor.moveToFirst()) {
            while (!cursor.isAfterLast()) {

                EmailFriendData followingModel1 = new EmailFriendData();
                followingModel1.setFriend_name(cursor.getString(cursor.getColumnIndex(CommonDatabaseHelper.KEY_FRIEND_USERNAME)));
                followingModel1.setFriend_email(cursor.getString(cursor.getColumnIndex(CommonDatabaseHelper.KEY_FRIEND_EMAIL)));
                followingModel1.setFriend_level(cursor.getString(cursor.getColumnIndex(CommonDatabaseHelper.KEY_FRIEND_USER_CONTACT_NO)));

                String friendBoolFlag = cursor.getString(cursor.getColumnIndex(CommonDatabaseHelper.KEY_FRIEND_USER_FLAG));
                if (friendBoolFlag.equals("1")) {
                    followingModel1.setFriend_flag(true);
                } else {
                    followingModel1.setFriend_flag(false);
                }

                String inviteBoolFlag = cursor.getString(cursor.getColumnIndex(CommonDatabaseHelper.KEY_FRIEND_USER_INVITE_FLAG));
                if (inviteBoolFlag.equals("1")) {
                    followingModel1.setFriend_invite_flag(true);
                } else {
                    followingModel1.setFriend_invite_flag(false);
                }
                dblocalTempArrayList.add(followingModel1);

                cursor.moveToNext();
            }
        }


        if (dblocalTempArrayList.size() != 0) {

            for (int i = 0; i < localtempArrayList.size(); i++) {
                for (int j = 0; j < dblocalTempArrayList.size(); j++) {

                    if (localtempArrayList.get(i).getFriend_email().equals(dblocalTempArrayList.get(j).getFriend_email())) {

                        String email_record = dblocalTempArrayList.get(j).getFriend_email();

                        commonDatabaseHelper.deleteRowData(CommonDatabaseHelper.FOLLOWING_MASTER, commonDatabaseHelper.KEY_FRIEND_EMAIL + " = ?",
                                new String[]{email_record});

                    }
                }
            }

            for (int i = 0; i < localtempArrayList.size(); i++) {
                ContentValues contentValues = new ContentValues();
                contentValues.put(CommonDatabaseHelper.KEY_FRIEND_USERNAME, localtempArrayList.get(i).getFriend_name());
                contentValues.put(CommonDatabaseHelper.KEY_FRIEND_USER_CONTACT_NO, localtempArrayList.get(i).getFriend_level());
                contentValues.put(CommonDatabaseHelper.KEY_FRIEND_EMAIL, localtempArrayList.get(i).getFriend_email());
                contentValues.put(CommonDatabaseHelper.KEY_FRIEND_USER_FLAG, localtempArrayList.get(i).getFriend_flag());
                contentValues.put(CommonDatabaseHelper.KEY_FRIEND_USER_INVITE_FLAG, localtempArrayList.get(i).getFriend_invite_flag());
                commonDatabaseHelper.insertData(CommonDatabaseHelper.FOLLOWING_MASTER, contentValues);
            }
        } else {
            for (int i = 0; i < localtempArrayList.size(); i++) {
                ContentValues contentValues = new ContentValues();
                contentValues.put(CommonDatabaseHelper.KEY_FRIEND_USERNAME, localtempArrayList.get(i).getFriend_name());
                contentValues.put(CommonDatabaseHelper.KEY_FRIEND_USER_CONTACT_NO, localtempArrayList.get(i).getFriend_level());
                contentValues.put(CommonDatabaseHelper.KEY_FRIEND_EMAIL, localtempArrayList.get(i).getFriend_email());
                contentValues.put(CommonDatabaseHelper.KEY_FRIEND_USER_FLAG, localtempArrayList.get(i).getFriend_flag());
                contentValues.put(CommonDatabaseHelper.KEY_FRIEND_USER_INVITE_FLAG, localtempArrayList.get(i).getFriend_invite_flag());

                commonDatabaseHelper.insertData(CommonDatabaseHelper.FOLLOWING_MASTER, contentValues);
            }

        }


        if (FollowingFindFriendActivity.getInstance().getUserInfo(SessionManager.KEY_USER_ID) != null) {
            paraUserId = FollowingFindFriendActivity.getInstance().getUserInfo(SessionManager.KEY_USER_ID);
        }
        if (FollowingFindFriendActivity.getInstance().getUserInfo(SessionManager.KEY_USER_SESSION_ID) != null) {
            paraSessionId = FollowingFindFriendActivity.getInstance().getUserInfo(SessionManager.KEY_USER_SESSION_ID);
        }

        getContactFriendAPI(paraUserId, paraSessionId, paraFriendEmail);
       /* paraFriendEmail = "friend_email%5B%5D=nimisharanipa.ibl@gmail.com&friend_email%5B%5D=nimisharanipa1.ibl@gmail.com&friend_email%5B%5D=nimisharanipa2.ibl@gmail.com&friend_email%5B%5D=nimisharanipa3.ibl@gmail.com&friend_email%5B%5D=nimisharanipa4.ibl@gmail.com&";
        getContactFriendAPI("160", "c4be28a959f2fdb6fc73fab36cf78a05", paraFriendEmail);*/
    }

    private void getContactFriendAPI(String paraUserId, String paraSessionId, String paraFriendEmail) {

        followingarraylistNew.clear();

        APIInterface apiService = APIClient.getClient().create(APIInterface.class);
        String url = APIClient.BASE_URL;

        url += "checkFriends.pl?user_id=" + paraUserId +
                "&session_id=" + paraSessionId +
                "&apiauth=" + APIClient.AUTH_KEY + "&" +
                paraFriendEmail;

        Call<FollowingModel<EmailFriendData>> callObject = apiService.getFollowingContactEmail(url.replaceAll(" ", "%20"));

        callObject.enqueue(new Callback<FollowingModel<EmailFriendData>>() {

            @Override
            public void onResponse(Call<FollowingModel<EmailFriendData>> call, Response<FollowingModel<EmailFriendData>> response) {

                if (response.body() != null && !response.body().toString().trim().isEmpty()) {

                    ArrayList<EmailFriendData> emailFriendDataServer = new ArrayList<EmailFriendData>();
                    ArrayList<EmailFriendData> emailFriendDataLocal = new ArrayList<EmailFriendData>();

                    emailFriendDataServer = response.body().getEmail_friend();

                    if (emailFriendDataServer != null) {
                        for (int i = 0; i < response.body().getEmail_friend().size(); i++) {
                            emailFriendDataServer.get(i).setFriend_flag(true);
                            emailFriendDataServer.get(i).setFriend_invite_flag(false);
                        }

                        Cursor cursor;
                        cursor = commonDatabaseHelper.getFollowingList();
                        if (cursor != null && cursor.moveToFirst()) {
                            while (!cursor.isAfterLast()) {
                                EmailFriendData followingModel = new EmailFriendData();
                                followingModel.setFriend_user_id(cursor.getString(cursor.getColumnIndex(CommonDatabaseHelper.KEY_FRIEND_USER_ID)));
                                followingModel.setFriend_name(cursor.getString(cursor.getColumnIndex(CommonDatabaseHelper.KEY_FRIEND_USERNAME)));
                                followingModel.setFriend_email(cursor.getString(cursor.getColumnIndex(CommonDatabaseHelper.KEY_FRIEND_EMAIL)));
                                followingModel.setFriend_level(cursor.getString(cursor.getColumnIndex(CommonDatabaseHelper.KEY_FRIEND_USER_CONTACT_NO)));
                                followingModel.setFriend_flag(false);

                                String inviteBoolFlag = cursor.getString(cursor.getColumnIndex(CommonDatabaseHelper.KEY_FRIEND_USER_INVITE_FLAG));
                                if (inviteBoolFlag.equals("1")) {
                                    followingModel.setFriend_invite_flag(true);
                                } else {
                                    followingModel.setFriend_invite_flag(false);
                                }

                                emailFriendDataLocal.add(followingModel);
                                cursor.moveToNext();
                            }
                        }

                        for (int i1 = 0; i1 < emailFriendDataServer.size(); i1++) {
                            for (int j1 = 0; j1 < emailFriendDataLocal.size(); j1++) {

                                if (emailFriendDataServer.get(i1).getFriend_email().equals(emailFriendDataLocal.get(j1).getFriend_email())) {
                                    emailFriendDataLocal.remove(j1);
                                    break;
                                }
                            }
                        }

                        emailFriendDataServer.addAll(emailFriendDataLocal);
                        followingarraylistNew = emailFriendDataServer;

                        Collections.sort(followingarraylistNew, new Comparator<EmailFriendData>() {
                            @Override
                            public int compare(EmailFriendData lhs, EmailFriendData rhs) {
                                String i1 = lhs.getFriend_name();
                                String i2 = rhs.getFriend_name();
                                return i1.compareToIgnoreCase(i2);
                            }
                        });

                        if (followingarraylistNew.size() != 0) {
                            mAdapter = new FollowingFindFriendListAdapter(getActivity(), followingarraylistNew);
                            recyclerView.setAdapter(mAdapter);
                        } else {
                            Global.showToast(getActivity(), getString(R.string.no_contact_found));
                        }
                    } else {
                        setLocalEmail();
                    }

                } else {
                    setLocalEmail();
                }
            }

            @Override
            public void onFailure(Call<FollowingModel<EmailFriendData>> call, Throwable t) {
                setLocalEmail();
            }
        });
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    public void setLocalEmail() {
        Cursor cursor;
        cursor = commonDatabaseHelper.getFollowingList();
        if (cursor != null && cursor.moveToFirst()) {
            while (!cursor.isAfterLast()) {
                EmailFriendData followingModel = new EmailFriendData();
                followingModel.setFriend_user_id(cursor.getString(cursor.getColumnIndex(CommonDatabaseHelper.KEY_FRIEND_USER_ID)));
                followingModel.setFriend_name(cursor.getString(cursor.getColumnIndex(CommonDatabaseHelper.KEY_FRIEND_USERNAME)));
                followingModel.setFriend_email(cursor.getString(cursor.getColumnIndex(CommonDatabaseHelper.KEY_FRIEND_EMAIL)));
                followingModel.setFriend_level(cursor.getString(cursor.getColumnIndex(CommonDatabaseHelper.KEY_FRIEND_USER_CONTACT_NO)));
                followingModel.setFriend_invite_flag(Boolean.valueOf(cursor.getString(cursor.getColumnIndex(CommonDatabaseHelper.KEY_FRIEND_USER_INVITE_FLAG))));
                followingModel.setFriend_flag(false);
                followingarraylistNew.add(followingModel);
                cursor.moveToNext();
            }
            mAdapter = new FollowingFindFriendListAdapter(getActivity(), followingarraylistNew);
            recyclerView.setAdapter(mAdapter);
        }
    }
}
