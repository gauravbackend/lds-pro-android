package com.ldsscriptures.pro.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;
import android.widget.LinearLayout;

import com.ldsscriptures.pro.R;
import com.ldsscriptures.pro.activity.LibraryIndexActivity;
import com.ldsscriptures.pro.adapter.LsGridAdapter;
import com.ldsscriptures.pro.database.SubDataBaseHelper;
import com.ldsscriptures.pro.dbClass.GetLibCollectionFromDB;
import com.ldsscriptures.pro.model.Item;
import com.ldsscriptures.pro.model.LsData;
import com.ldsscriptures.pro.model.MoreOptionMenuData;
import com.ldsscriptures.pro.utils.Decompress;
import com.ldsscriptures.pro.utils.DownloadFileAsync;
import com.ldsscriptures.pro.utils.EnableMultiDex;
import com.ldsscriptures.pro.utils.Global;

import java.io.File;
import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnItemClick;

public class LibrarySelectionFragment extends BaseFragment {

    @BindView(R.id.gridView)
    GridView gridView;

    private ArrayList<LsData> lsDataArrayList = new ArrayList<>();
    private ArrayList<LsData> lsDataArrayList_new = new ArrayList<>();

    static LibrarySelectionFragment librarySelectionFragment;
    private LsGridAdapter lcGridAdapter;
    String LcID;
    private View view;
    private LinearLayout lin_root;


    public static LibrarySelectionFragment getInstance() {
        return librarySelectionFragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_library_selection, container, false);

        lin_root = (LinearLayout) view.findViewById(R.id.lin_root);

        ButterKnife.bind(this, lin_root);

        setContents();
        return view;
    }

    private void setContents() {
        librarySelectionFragment = this;
        LcID = getArguments().getString(getString(R.string.lcID));

        setGridview(LcID);
    }

    public void setDownloaded(String id) {
        for (int i = 0; i < lsDataArrayList.size(); i++) {
            if (lsDataArrayList.get(i).getItem_id() == Integer.parseInt(id)) {
                lsDataArrayList.get(i).setDownloaded(true);
            }
        }
    }

    public void setGridview(String LcID) {

        lsDataArrayList.clear();
        lsDataArrayList_new.clear();

        lsDataArrayList = new GetLibCollectionFromDB(getActivity()).getLsDataFromDb(LcID);
        lsDataArrayList_new = new GetLibCollectionFromDB(getActivity()).getLsDataFromDbFirst(LcID);
        lsDataArrayList.addAll(lsDataArrayList_new);

        if (lsDataArrayList != null && lsDataArrayList.size() > 0) {
            lcGridAdapter = new LsGridAdapter(getActivity(), lsDataArrayList);
            gridView.setAdapter(lcGridAdapter);
        }
    }

    @OnItemClick(R.id.gridView)
    public void openLSActivity(int position) {
        Global.bookmarkData.setBookuri(lsDataArrayList.get(position).getUri());
        checkDatabse(position);
    }

    public void checkDatabse(int position) {

        SubDataBaseHelper subDataBaseHelper = new SubDataBaseHelper(getActivity(), String.valueOf(lsDataArrayList.get(position).getItem_id()));
        if (subDataBaseHelper.checkDataBaseWithFolderName(String.valueOf(lsDataArrayList.get(position).getItem_id()))) {

            Global.selectedLibSelection = lsDataArrayList.get(position).getTitle();
            Global.moreOptionMenuDatas.set(1, new MoreOptionMenuData(LcID, String.valueOf(lsDataArrayList.get(position).getItem_id()), lsDataArrayList.get(position).getTitle()));

            Intent intent = new Intent(getActivity(), LibraryIndexActivity.class);
            intent.putExtra(getString(R.string.lcID), LcID);
            intent.putExtra(getString(R.string.lsTitle), lsDataArrayList.get(position).getTitle());
            intent.putExtra(getString(R.string.lsId), String.valueOf(lsDataArrayList.get(position).getItem_id()));
            startActivityWithAnimation(intent);

        }
        // if set priority high then set this condition to download first this item. issue: display progress click multiple problem.
        else {

            String LcID = String.valueOf(lsDataArrayList.get(position).getId());
            String ItemId = String.valueOf(lsDataArrayList.get(position).getItem_id());
            String LsTitle = lsDataArrayList.get(position).getTitle();

            if (ItemId.equals("0")) {
                setGridview(LcID);
            } else {
                ArrayList<Item> dbCatelogArraylist = new ArrayList<>();
                dbCatelogArraylist = Global.getListSharedPreferneces(getActivity());

                if (dbCatelogArraylist != null) {

                    for (int i = 0; i < dbCatelogArraylist.size(); i++) {
                        if (dbCatelogArraylist.get(i).getId().equals(ItemId)) {

                            if (!isFolderExists(dbCatelogArraylist.get(i).getId())) {

                                String downloadURl = dbCatelogArraylist.get(i).getUrl();

                                downloadAndUnzipContent(downloadURl, ItemId, LcID, LsTitle);

                            } else {

                                Intent intent = new Intent(getActivity(), LibraryIndexActivity.class);
                                intent.putExtra(getString(R.string.lcID), LcID);
                                intent.putExtra(getString(R.string.lsTitle), LsTitle);
                                intent.putExtra(getString(R.string.lsId), ItemId);
                                startActivityWithAnimation(intent);
                            }
                        }
                    }
                }
            }
        }
    }

    private boolean isFolderExists(String id) {
        File destDir = new File(Global.AppFolderPath + id);

        if (!destDir.exists()) {
            return false;
        } else {
            if (destDir.isDirectory()) {
                String[] files = destDir.list();
                if (files == null)
                    return false;

                if (files.length == 0) {
                    //directory is empty
                    return false;
                } else
                    return true;
            }
        }
        return false;
    }

    private void downloadAndUnzipContent(String downloadURl, final String folderId, final String LcId, final String LsTitle) {

        Global.showProgressDialog(getActivity());

        String url = downloadURl;

        DownloadFileAsync download = new DownloadFileAsync(folderId, new DownloadFileAsync.PostDownload() {
            @Override
            public void downloadDone(File file) {

                // check unzip file now
                Decompress unzip = new Decompress(file);
                unzip.unzip(folderId);

                Global.dismissProgressDialog();

                try {
                    Intent intent = new Intent(EnableMultiDex.getEnableMultiDexApp(), LibraryIndexActivity.class);
                    intent.putExtra(getString(R.string.lcID), LcId);
                    intent.putExtra(getString(R.string.lsTitle), LsTitle);
                    intent.putExtra(getString(R.string.lsId), folderId);
                    startActivityWithAnimation(intent);
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        });
        download.execute(url);
    }
}
