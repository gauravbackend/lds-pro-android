package com.ldsscriptures.pro.fragment;

import android.app.ActivityManager;
import android.app.UiModeManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.ldsscriptures.pro.R;
import com.ldsscriptures.pro.activity.menuscreens.SlideMenuActivity;
import com.ldsscriptures.pro.utils.Font;
import com.ldsscriptures.pro.utils.Global;
import com.ldsscriptures.pro.utils.SessionManager;

import java.util.HashMap;

import permission.auron.com.marshmallowpermissionhelper.FragmentManagePermission;
import permission.auron.com.marshmallowpermissionhelper.PermissionResult;
import permission.auron.com.marshmallowpermissionhelper.PermissionUtils;

public class BaseFragment extends FragmentManagePermission {

    private SessionManager sessionManager;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        sessionManager = new SessionManager(getActivity());

        UiModeManager uiManager = (UiModeManager) getActivity().getSystemService(Context.UI_MODE_SERVICE);
        if (sessionManager.isNightMode()) {
            uiManager.setNightMode(UiModeManager.MODE_NIGHT_YES);
        } else {
            uiManager.setNightMode(UiModeManager.MODE_NIGHT_NO);
        }
        return inflater.inflate(0, container, false);
    }

    public void setFont() {
        try {
            ViewGroup group = (ViewGroup) getActivity().getWindow().getDecorView().findViewById(android.R.id.content);
            Font.setAllTextView(getActivity(), group, getString(R.string.font_roboto_regular));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    void setFont(TextView textView, String sFont) {
        //to set particular text
        Font.setBoldFont(getActivity(), textView, sFont);
    }

    public boolean isMyServiceRunning(Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) getActivity().getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }

    public void setBackground(int image, ImageView imageView_background) {
        Global.SetBackground(getActivity(), imageView_background, image);
    }

    public void showText(TextView tvTitle, String sTitle) {
        tvTitle.setText(sTitle);
    }

    public void setVisible(View ivBack) {
        ivBack.setVisibility(View.VISIBLE);
    }

    public void setVisibilityGone(View ivBack) {
        ivBack.setVisibility(View.GONE);
    }

    public void askForPermission() {
        askCompactPermissions(new String[]{PermissionUtils.Manifest_CAMERA,
                PermissionUtils.Manifest_READ_EXTERNAL_STORAGE, PermissionUtils.Manifest_WRITE_EXTERNAL_STORAGE}, new PermissionResult() {
            @Override
            public void permissionGranted() {
            }

            @Override
            public void permissionDenied() {
            }

            @Override
            public void permissionForeverDenied() {
            }
        });
    }

    String getUserInfo(String getStr) {
        String string = null;
        if (sessionManager.isLoggedIn()) {
            HashMap<String, String> hashMap = sessionManager.getLoginDetails();
            string = hashMap.get(getStr);
        }
        return string;
    }

    public void openSlideMenu() {
        startActivity(new Intent(getActivity(), SlideMenuActivity.class));
        getActivity().overridePendingTransition(R.anim.left_to_right, android.R.anim.fade_out);
    }

    public void startActivityWithAnimation(Intent intent) {
        startActivity(intent);
        getActivity().overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
    }
}
