package com.ldsscriptures.pro.fragment;

import android.database.Cursor;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.text.format.DateFormat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.ldsscriptures.pro.R;
import com.ldsscriptures.pro.activity.activitymenu.ActivityScreen;
import com.ldsscriptures.pro.adapter.ActivityChartPagerAdapter;
import com.ldsscriptures.pro.database.CommonDatabaseHelper;
import com.ldsscriptures.pro.model.ActivityData;
import com.ldsscriptures.pro.utils.Global;
import com.ldsscriptures.pro.utils.SessionManager;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * Created by IBL InfoTech on 7/27/2017.
 */

public class ActivityMonthPagerTabFragment extends BaseFragment {

    Unbinder unbinder;
    @BindView(R.id.pager_top_layout)
    LinearLayout pagerTopLayout;
    @BindView(R.id.activity_Pager)
    ViewPager activityPager;
    @BindView(R.id.activity_TabLayout)
    TabLayout activityTabLayout;
    @BindView(R.id.imageView3)
    ImageView imageView3;
    @BindView(R.id.pager_bottom_layout)
    LinearLayout pagerBottomLayout;
    @BindView(R.id.activity_total_goalspoint)
    TextView activityTotalGoalspoint;
    @BindView(R.id.txtAvgGoalPoint)
    TextView txtAvgGoalPoint;
    @BindView(R.id.txtActivityDailyGoalpts)
    TextView txtActivityDailyGoalpts;
    @BindView(R.id.txtActivityReadingMinute)
    TextView txtActivityReadingMinute;
    @BindView(R.id.txtActivityAnotationMinute)
    TextView txtActivityAnotationMinute;
    @BindView(R.id.txtActivityAchivementMinute)
    TextView txtActivityAchivementMinute;
    @BindView(R.id.txtActivitydayStreaks)
    TextView txtActivitydayStreaks;
    @BindView(R.id.txt_Avglabel)
    TextView txtAvglabel;

    private ArrayList<ActivityData> monthViseActivityArrayList = new ArrayList<>();
    private ArrayList<ActivityData> newmonthViseActivityArrayList = new ArrayList<>(12);
    private ArrayList<ActivityData> monthChartViseActivityArrayList = new ArrayList<>();
    CommonDatabaseHelper commonDatabaseHelper;
    ActivityChartPagerAdapter mAdapter;
    static ActivityMonthPagerTabFragment activityMonthPagerTabFragment;
    int postionTab = 0;
    int[] months = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11};
    SessionManager sessionManager;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_activity_day, container, false);
        unbinder = ButterKnife.bind(this, view);
        setContents();
        return view;
    }

    public void setContents() {
        activityMonthPagerTabFragment = this;
        commonDatabaseHelper = new CommonDatabaseHelper(getActivity());
        monthViseActivityArrayList = ActivityScreen.getInstance().getmonthViseActivityArrayList();

        sessionManager = new SessionManager(getActivity());
        txtActivityDailyGoalpts.setText(sessionManager.getUserGoal() + " " + "PTS");

        newmonthViseActivityArrayList = new ArrayList<>();
        for (int i = 0; i < months.length; i++) {
            Calendar cal = Calendar.getInstance();
            SimpleDateFormat month_date = new SimpleDateFormat("MMMM");
            cal.set(Calendar.MONTH, months[i]);
            String month_name = month_date.format(cal.getTime());
            ActivityData activityData = new ActivityData();
            activityData.setMonthname(month_name);
            newmonthViseActivityArrayList.add(activityData);
        }

        for (int i = 0; i < newmonthViseActivityArrayList.size(); i++) {
            try {
                for (int j = 0; j < monthViseActivityArrayList.size(); j++) {
                    String activityDate = monthViseActivityArrayList.get(j).getActivity_date();
                    SimpleDateFormat gsdf = new SimpleDateFormat("yyyy-MM-dd");
                    Date dt1 = gsdf.parse(activityDate);
                    String monthString = (String) DateFormat.format("MMMM", dt1);

                    if (newmonthViseActivityArrayList.get(i).getMonthname().equals(monthString)) {

                        monthViseActivityArrayList.get(j).setMonthname(monthString);
                        newmonthViseActivityArrayList.set(i, monthViseActivityArrayList.get(j));
                    }
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        if (newmonthViseActivityArrayList != null && newmonthViseActivityArrayList.size()!= 0) {
            for (int i = 0; i < newmonthViseActivityArrayList.size(); i++) {
                try {
                    String activityDate = newmonthViseActivityArrayList.get(i).getMonthname();
                    activityTabLayout.addTab(activityTabLayout.newTab().setText(activityDate.toUpperCase()));

                    SimpleDateFormat dateFormat = new SimpleDateFormat("MMMM");
                    Date date = new Date();

                    if (activityDate.toString().contains(dateFormat.format(date).toString())) {
                        postionTab = i;
                    }


                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            try {
                new Handler().postDelayed(
                        new Runnable() {
                            @Override
                            public void run() {
                                activityTabLayout.getTabAt(postionTab).select();
                                getMonthViseChartData(postionTab);
                            }
                        }, 100);
            } catch (Exception e) {
                e.printStackTrace();
            }


            activityTabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
                @Override
                public void onTabSelected(TabLayout.Tab tab) {
                    activityPager.setCurrentItem(tab.getPosition());
                    getMonthViseChartData(tab.getPosition());

                }

                @Override
                public void onTabUnselected(TabLayout.Tab tab) {
                }

                @Override
                public void onTabReselected(TabLayout.Tab tab) {

                }
            });

        }

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    public void getMonthViseChartData(int pos) {
        try {

            String activityMonthStart = newmonthViseActivityArrayList.get(pos).getWeekStart();
            String activityMonthEnd = newmonthViseActivityArrayList.get(pos).getWeekEnd();
            if (activityMonthStart != null && activityMonthEnd != null) {

                selectChartMonthViseActivityRecord(activityMonthStart, activityMonthEnd);
            } else {

                monthChartViseActivityArrayList.clear();
                mAdapter = new ActivityChartPagerAdapter(getActivity(), activityTabLayout.getTabCount(), 2, monthChartViseActivityArrayList);
                activityPager.setAdapter(mAdapter);
                mAdapter.notifyDataSetChanged();
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void selectChartMonthViseActivityRecord(String varMonthstart, String varMonthEnd) {

        Cursor cursor;
        cursor = commonDatabaseHelper.getMonthChartViseActivityRecord(varMonthstart, varMonthEnd);

        monthChartViseActivityArrayList.clear();
        if (cursor != null && cursor.moveToFirst()) {
            while (!cursor.isAfterLast()) {

                ActivityData activityData = new ActivityData();
                activityData.setActivity_id(cursor.getInt(cursor.getColumnIndex(CommonDatabaseHelper.KEY_ACTIVITY_ID)));
                activityData.setActivity_date(cursor.getString(cursor.getColumnIndex(CommonDatabaseHelper.KEY_ACTIVITY_DATE)));
                activityData.setActivity_hour(cursor.getInt(cursor.getColumnIndex(CommonDatabaseHelper.KEY_ACTIVITY_HOUR)));
                activityData.setActivity_dayOfWeek(cursor.getInt(cursor.getColumnIndex(CommonDatabaseHelper.KEY_ACTIVITY_DAYOFWEEK)));
                activityData.setActivity_dayOfMonth(cursor.getInt(cursor.getColumnIndex(CommonDatabaseHelper.KEY_ACTIVITY_MONTH)));
                activityData.setActivity_dayOfYear(cursor.getInt(cursor.getColumnIndex(CommonDatabaseHelper.KEY_ACTIVITY_YEAR)));
                activityData.setActivity_DayMonth(cursor.getInt(cursor.getColumnIndex(CommonDatabaseHelper.KEY_ACTIVITY_DAYOFMONTH)));
                activityData.setActivity_goal_point(cursor.getInt(cursor.getColumnIndex(CommonDatabaseHelper.KEY_ACTIVITY_GOAL_POINT)));

                monthChartViseActivityArrayList.add(activityData);
                cursor.moveToNext();
            }

            mAdapter = new ActivityChartPagerAdapter(getActivity(), activityTabLayout.getTabCount(), 2, monthChartViseActivityArrayList);
            activityPager.setAdapter(mAdapter);
            int activityGoalPoint = monthViseActivityArrayList.get(0).getActivity_goal_point();
            int arrayListSize = monthViseActivityArrayList.size();
            ActivityScreen.getInstance().day_strike(txtActivitydayStreaks, txtActivityReadingMinute, txtActivityDailyGoalpts, txtAvgGoalPoint, activityTotalGoalspoint, String.valueOf(activityGoalPoint), arrayListSize, txtActivityAchivementMinute);
            txtAvglabel.setText(R.string.avg_monthly);
        }

    }

    public static ActivityMonthPagerTabFragment getInstance() {
        return activityMonthPagerTabFragment;
    }
}
