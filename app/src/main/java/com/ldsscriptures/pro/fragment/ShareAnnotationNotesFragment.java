package com.ldsscriptures.pro.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.ldsscriptures.pro.R;
import com.ldsscriptures.pro.activity.ShareAnnotationsActivity;
import com.ldsscriptures.pro.adapter.ShareAnnotationNotesAdapter;
import com.ldsscriptures.pro.utils.Global;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;


public class ShareAnnotationNotesFragment extends BaseFragment {

    Unbinder unbinder;
    @BindView(R.id.tvNoData)
    TextView tvNoData;

    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;

    private ShareAnnotationNotesAdapter shareAnnotationNotesAdapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_notes_share_annotation, container, false);
        unbinder = ButterKnife.bind(this, view);
        setContents();
        return view;
    }

    public void setContents() {
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
        if (ShareAnnotationsActivity.notesDataArrayList.size() != 0) {
            recyclerView.setVisibility(View.VISIBLE);
            tvNoData.setVisibility(View.GONE);


            shareAnnotationNotesAdapter = new ShareAnnotationNotesAdapter(getActivity(), ShareAnnotationsActivity.notesDataArrayList);
            recyclerView.setAdapter(shareAnnotationNotesAdapter);
        } else {
            recyclerView.setVisibility(View.GONE);
            tvNoData.setVisibility(View.VISIBLE);
            tvNoData.setText(getString(R.string.no_notes_found));
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }
}