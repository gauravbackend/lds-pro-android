package com.ldsscriptures.pro.fragment;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.ldsscriptures.pro.R;
import com.ldsscriptures.pro.activity.socialmenu.LeaderBoardActivity;
import com.ldsscriptures.pro.adapter.FollowingListAdapter;
import com.ldsscriptures.pro.model.FollowersMainData;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class LeaderboardMonthTabFragment extends BaseFragment {


    @BindView(R.id.txtsearch)
    TextView txtsearch;
    @BindView(R.id.txtsee_which)
    TextView txtseeWhich;
    @BindView(R.id.txtalready_here)
    TextView txtalreadyHere;
    @BindView(R.id.btn_chkList)
    Button btnChkList;
    @BindView(R.id.lay_firstLoad)
    LinearLayout layFirstLoad;

    ArrayList<FollowersMainData> leaderboardArraylist = new ArrayList<>();
    FollowingListAdapter mAdapter;

    Unbinder unbinder;
    @BindView(R.id.tvNoData)
    TextView tvNoData;
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;

    private View view;
    private LinearLayout lin_main_layout;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
         view = inflater.inflate(R.layout.followingtabfragment, container, false);
        unbinder = ButterKnife.bind(this, view);
        setContents();
        return view;
    }

    public void setContents() {
        lin_main_layout=(LinearLayout)view.findViewById(R.id.lin_main_layout);

        recyclerView.setVisibility(View.VISIBLE);
        layFirstLoad.setVisibility(View.GONE);
        lin_main_layout.setVisibility(View.VISIBLE);

        recyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(layoutManager);

        leaderboardArraylist = LeaderBoardActivity.leaderboardArraylist;

        Collections.sort(leaderboardArraylist, new Comparator<FollowersMainData>() {
            @Override
            public int compare(FollowersMainData lhs, FollowersMainData rhs) {

                int i1 = Integer.parseInt(lhs.getFollowing_points_month());
                int i2 = Integer.parseInt(rhs.getFollowing_points_month());
                return Integer.valueOf(i2).compareTo(i1);
            }
        });

        if (leaderboardArraylist.size() != 0) {
            recyclerView.setVisibility(View.VISIBLE);
            tvNoData.setVisibility(View.GONE);

            mAdapter = new FollowingListAdapter(getActivity(), leaderboardArraylist, 1, 2);
            recyclerView.setAdapter(mAdapter);
        } else {
            recyclerView.setVisibility(View.GONE);
            tvNoData.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

}
