package com.ldsscriptures.pro.dbClass;

import android.content.Context;
import android.database.Cursor;

import com.ldsscriptures.pro.activity.ReadingActivity;
import com.ldsscriptures.pro.database.SubDataBaseHelper;
import com.ldsscriptures.pro.model.FootNotesData;

import org.jsoup.Jsoup;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class GetFootnotesFromDB {
    private final SubDataBaseHelper databasehelper;
    ArrayList<FootNotesData> footNotesArrayList = new ArrayList<>();
    private String firstLetter;
    private String posID;
    int scrollPosition;

    public GetFootnotesFromDB(Context context, int indexID, char firstLetter, String posID, String lsId) {
        this.firstLetter = String.valueOf(firstLetter);
        this.posID = posID;
        databasehelper = new SubDataBaseHelper(context, lsId);
        getFootnoteData(indexID);

    }

    private void getFootnoteData(int indexId) {

        footNotesArrayList = new ArrayList<>();

        Cursor cursor;
        cursor = databasehelper.getFootNotes(Integer.parseInt(String.valueOf(indexId)));

        if (cursor.moveToFirst()) {
            while (!cursor.isAfterLast()) {
                FootNotesData footNotesData = new FootNotesData();
                footNotesData.setId(cursor.getString(cursor.getColumnIndex(SubDataBaseHelper.KEY_ID)));
                footNotesData.setSubitem_id(cursor.getString(cursor.getColumnIndex(SubDataBaseHelper.KEY_SUBITEM_ID)));
                footNotesData.setRef_id(cursor.getString(cursor.getColumnIndex(SubDataBaseHelper.KEY_FT_REF_ID)));
                footNotesData.setLabel_html(cursor.getString(cursor.getColumnIndex(SubDataBaseHelper.KEY_FT_LABEL_HTML)));
                footNotesData.setOrigin_id(cursor.getString(cursor.getColumnIndex(SubDataBaseHelper.KEY_FT_ORIGIN_ID)));
                footNotesData.setContent_html(cursor.getString(cursor.getColumnIndex(SubDataBaseHelper.KEY_FT_CONTENT_HTML)));
                footNotesData.setWord_offset(cursor.getInt(cursor.getColumnIndex(SubDataBaseHelper.KEY_FT_WORD_OFFSET)));
                footNotesData.setByte_location(cursor.getInt(cursor.getColumnIndex(SubDataBaseHelper.KEY_FT_BYTE_LOCATION)));
                footNotesArrayList.add(footNotesData);

                cursor.moveToNext();
            }

            Collections.sort(footNotesArrayList, new Comparator<FootNotesData>() {
                public int compare(FootNotesData s1, FootNotesData s2) {
                    String i1 = getOnlyString(html2text(s1.getLabel_html()));
                    String i2 = getOnlyString(html2text(s2.getLabel_html()));
                    return i1.compareTo(i2);
                }
            });


            Collections.sort(footNotesArrayList, new Comparator<FootNotesData>() {
                public int compare(FootNotesData s1, FootNotesData s2) {
                    Integer i1 = getOnlyNumerics(html2text(s1.getLabel_html()));
                    Integer i2 = getOnlyNumerics(html2text(s2.getLabel_html()));
                    return i1.compareTo(i2);
                }
            });
        }

        for (int i = 0; i < footNotesArrayList.size(); i++) {
            if (footNotesArrayList.get(i).getRef_id().contains("note" + posID + firstLetter)) {
                scrollPosition = i;
            }
        }

        if (cursor != null) {
            cursor.close();
        }

        if (ReadingActivity.getInstance() != null) {
            ReadingActivity.getInstance().showFootnotesLayout(footNotesArrayList, scrollPosition);
        }
    }

    public static String getOnlyString(String str) {
        if (str == null) {
            return null;
        }

        StringBuffer strBuff = new StringBuffer();
        char c;

        for (int i = 0; i < str.length(); i++) {
            c = str.charAt(i);

            if (!Character.isDigit(c)) {
                strBuff.append(c);
            }
        }

        return strBuff.toString();
    }

    public static int getOnlyNumerics(String str) {
        if (str == null) {
            return 0;
        }

        StringBuffer strBuff = new StringBuffer();
        char c;

        for (int i = 0; i < str.length(); i++) {
            c = str.charAt(i);

            if (Character.isDigit(c)) {
                strBuff.append(c);
            } else {
                break;
            }
        }

        int retunvalue = 0;

        if (!strBuff.toString().equals("")) {
            retunvalue = Integer.parseInt(strBuff.toString());
        }

        return retunvalue;
    }

    public static String html2text(String html) {
        return Jsoup.parse(html).text();
    }

}
