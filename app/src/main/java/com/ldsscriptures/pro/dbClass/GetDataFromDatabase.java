package com.ldsscriptures.pro.dbClass;

import android.content.Context;
import android.database.Cursor;
import android.text.Html;

import com.ldsscriptures.pro.database.SubDataBaseHelper;
import com.ldsscriptures.pro.model.IndexWrapper;
import com.ldsscriptures.pro.model.LsFullIndexData;
import com.ldsscriptures.pro.model.LsIndex;
import com.ldsscriptures.pro.model.LsIndexPreviewData;
import com.ldsscriptures.pro.model.LsMainIndexData;
import com.ldsscriptures.pro.model.LsSubIndexData;
import com.ldsscriptures.pro.model.LsTitleIndexData;
import com.ldsscriptures.pro.model.ReadingViewpagerData;
import com.ldsscriptures.pro.model.VerseData;
import com.ldsscriptures.pro.utils.Global;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.util.ArrayList;
import java.util.List;

public class GetDataFromDatabase {

    private final SubDataBaseHelper subDataBaseHelper;
    private final String lsId;
    Context context;

    private ArrayList<LsIndex> lsIndexArray = new ArrayList<>();
    private ArrayList<LsFullIndexData> lsFullIndexDataArray = new ArrayList<>();
    private ArrayList<LsMainIndexData> itemMainIndexArray = new ArrayList<>();
    private ArrayList<LsTitleIndexData> lsTitleIndexDataArray;
    private ArrayList<LsSubIndexData> lsSubIndexDataArray = new ArrayList<>();
    private ArrayList<LsIndexPreviewData> lsIndexPreviewDataArray = new ArrayList<>();
    public static String HEADERCONTENT;
    private String headerContent = "";
    final ArrayList<ReadingViewpagerData> readingViewpagerDataArrayList = new ArrayList<>();

    public GetDataFromDatabase(Context context, String lsId) {
        this.context = context;
        this.lsId = lsId;
        subDataBaseHelper = new SubDataBaseHelper(context, lsId);
    }

    public ArrayList<LsFullIndexData> GetMainDataFromDB() {

        // id of Library Collection item For example, Old testament,new testament
        Cursor cursor = subDataBaseHelper.getLsIndexDataS2(getIndexDataFromDB(lsId));

        if (cursor != null && cursor.moveToFirst()) {
            while (!cursor.isAfterLast()) {
                LsFullIndexData lsFullIndexData = new LsFullIndexData();
                lsFullIndexData.setId(cursor.getInt(cursor.getColumnIndex(SubDataBaseHelper.KEY_ID)));
                lsFullIndexData.setNav_collection_id(cursor.getString(cursor.getColumnIndex(SubDataBaseHelper.KEY_NAV_COLLECTION_ID)));
                lsFullIndexData.setPosition(cursor.getInt(cursor.getColumnIndex(SubDataBaseHelper.KEY_POSITION)));
                lsFullIndexData.setTitle(cursor.getString(cursor.getColumnIndex(SubDataBaseHelper.KEY_TITLE)));
                lsFullIndexData.setIndent_level(cursor.getString(cursor.getColumnIndex(SubDataBaseHelper.KEY_INDENT_LEVEL)));

                // get data of content list data of LibraryIndexActivity.
                // 2.1) SELECT * FROM nav_collection where nav_section_id=1 order by position
                lsFullIndexData.setItemMainIndexArray(GetMainIndexDataFromDB(lsFullIndexData.getId()));

                // get data of header/title list data of LibraryIndexActivity.
                // 2.2) SELECT * FROM nav_item where nav_section_id=1 order by position
                lsFullIndexData.setTitleIndexDatas(GetTitleDataFromDB(lsFullIndexData.getId()));

                lsFullIndexDataArray.add(lsFullIndexData);
                cursor.moveToNext();
            }
        }
        subDataBaseHelper.close();
        if (cursor != null) {

            cursor.close();
        }
        return lsFullIndexDataArray;
    }

    public int getIndexDataFromDB(String lsId) {

        Cursor cursor;
        cursor = subDataBaseHelper.getLsIndexDataS1();
        if (cursor != null && cursor.moveToFirst()) {
            while (!cursor.isAfterLast()) {

                LsIndex lsIndex = new LsIndex();
                lsIndex.setId(cursor.getInt(cursor.getColumnIndex(SubDataBaseHelper.KEY_ID)));
                lsIndex.setNav_section_id(cursor.getInt(cursor.getColumnIndex(SubDataBaseHelper.KEY_NAV_SECTION_ID)));
                lsIndex.setPosition(cursor.getInt(cursor.getColumnIndex(SubDataBaseHelper.KEY_POSITION)));
                lsIndex.setImage_rendation(cursor.getString(cursor.getColumnIndex(SubDataBaseHelper.KEY_IMAGE_RENDITIONS)));
                lsIndex.setTitle_html(cursor.getString(cursor.getColumnIndex(SubDataBaseHelper.KEY_TITLE_HTML)));
                lsIndex.setSubtitle(cursor.getString(cursor.getColumnIndex(SubDataBaseHelper.KEY_SUB_TITLE)));
                lsIndex.setUri(cursor.getString(cursor.getColumnIndex(SubDataBaseHelper.KEY_URI)));

                lsIndexArray.add(lsIndex);
                cursor.moveToNext();
            }
        }
        subDataBaseHelper.close();
        if (cursor != null) {
            cursor.close();
        }

        if (lsIndexArray != null && lsIndexArray.size() > 0) {
            return lsIndexArray.get(0).getId();
        } else {
            return -1;
        }
    }

    public ArrayList<LsMainIndexData> GetMainIndexDataFromDB(int nextId) {
        itemMainIndexArray = new ArrayList<>();

        Cursor cursor = subDataBaseHelper.getLsIndexDataS3(nextId);
        if (cursor != null && cursor.moveToFirst()) {
            while (!cursor.isAfterLast()) {

                LsMainIndexData mainIndexData = new LsMainIndexData();
                mainIndexData.setId(cursor.getInt(cursor.getColumnIndex(SubDataBaseHelper.KEY_ID)));
                mainIndexData.setNav_section_id(cursor.getInt(cursor.getColumnIndex(SubDataBaseHelper.KEY_NAV_SECTION_ID)));
                mainIndexData.setPosition(cursor.getInt(cursor.getColumnIndex(SubDataBaseHelper.KEY_POSITION)));
                mainIndexData.setImage_renditions(cursor.getString(cursor.getColumnIndex(SubDataBaseHelper.KEY_IMAGE_RENDITIONS)));
                mainIndexData.setTitle_html(cursor.getString(cursor.getColumnIndex(SubDataBaseHelper.KEY_TITLE_HTML)));
                mainIndexData.setSubtitle(cursor.getString(cursor.getColumnIndex(SubDataBaseHelper.KEY_SUB_TITLE)));
                mainIndexData.setUri(cursor.getString(cursor.getColumnIndex(SubDataBaseHelper.KEY_URI)));

                // get Sub-Index data of LibrarIndexActivity content data.
                // 2.1.1)SELECT * FROM nav_section where nav_collection_id=%@ order by position
                mainIndexData.setLsSubIndexDatas(GetSubIndexDataFromDB(cursor.getInt(cursor.getColumnIndex(SubDataBaseHelper.KEY_ID))));

                itemMainIndexArray.add(mainIndexData);
                cursor.moveToNext();
            }
        }
        subDataBaseHelper.close();
        if (cursor != null) {
            cursor.close();
        }
        return itemMainIndexArray;
    }

    public ArrayList<LsTitleIndexData> GetTitleDataFromDB(int nextId) {

        lsTitleIndexDataArray = new ArrayList<>();
        Cursor cursor = subDataBaseHelper.getLsIndexDataS4(nextId);


        if (cursor != null && cursor.moveToFirst()) {
            while (!cursor.isAfterLast()) {

                LsTitleIndexData indexData = new LsTitleIndexData();
                indexData.setId(cursor.getInt(cursor.getColumnIndex(SubDataBaseHelper.KEY_ID)));
                indexData.setNav_section_id(cursor.getInt(cursor.getColumnIndex(SubDataBaseHelper.KEY_NAV_SECTION_ID)));
                indexData.setPosition(cursor.getInt(cursor.getColumnIndex(SubDataBaseHelper.KEY_POSITION)));
                indexData.setImage_rendition(cursor.getString(cursor.getColumnIndex(SubDataBaseHelper.KEY_IMAGE_RENDITIONS)));

                indexData.setTitle_html(cursor.getString(cursor.getColumnIndex(SubDataBaseHelper.KEY_TITLE_HTML)));
                indexData.setSubtitle(cursor.getString(cursor.getColumnIndex(SubDataBaseHelper.KEY_SUB_TITLE)));
                indexData.setPreview(cursor.getString(cursor.getColumnIndex(SubDataBaseHelper.KEY_PREVIEW)));
                indexData.setUri(cursor.getString(cursor.getColumnIndex(SubDataBaseHelper.KEY_URI)));
                indexData.setSubitem_id(cursor.getInt(cursor.getColumnIndex(SubDataBaseHelper.KEY_SUBITEM_ID)));

                lsTitleIndexDataArray.add(indexData);
                cursor.moveToNext();
            }
        }
        subDataBaseHelper.close();
        if (cursor != null) {
            cursor.close();
        }
        return lsTitleIndexDataArray;
    }

    public ArrayList<LsSubIndexData> GetSubIndexDataFromDB(int nextId) {
        Cursor cursor = subDataBaseHelper.getLsIndexDataS5(nextId);
        if (cursor != null && cursor.moveToFirst()) {
            while (!cursor.isAfterLast()) {

                LsSubIndexData lsSubIndexData = new LsSubIndexData();
                lsSubIndexData.setId(cursor.getInt(cursor.getColumnIndex(SubDataBaseHelper.KEY_ID)));
                lsSubIndexData.setNav_collection_id(cursor.getString(cursor.getColumnIndex(SubDataBaseHelper.KEY_NAV_COLLECTION_ID)));
                lsSubIndexData.setPosition(cursor.getInt(cursor.getColumnIndex(SubDataBaseHelper.KEY_POSITION)));
                lsSubIndexData.setIndent_level(cursor.getInt(cursor.getColumnIndex(SubDataBaseHelper.KEY_INDENT_LEVEL)));
                lsSubIndexData.setTitle(cursor.getString(cursor.getColumnIndex(SubDataBaseHelper.KEY_TITLE)));

                // get the preview data of SubIndex data of SubIndexActivity.
                // - 2.1.1.1 ����SELECT * FROM nav_item where nav_section_id=%@ order by position
                lsSubIndexData.setLsIndexPreviewDatas(GetIndexPreviewDataFromDB(cursor.getInt(cursor.getColumnIndex(SubDataBaseHelper.KEY_ID))));
                lsSubIndexDataArray.add(lsSubIndexData);
                cursor.moveToNext();
            }
        }
        subDataBaseHelper.close();
        if (cursor != null) {
            cursor.close();
        }
        return lsSubIndexDataArray;
    }

    public ArrayList<LsIndexPreviewData> GetIndexPreviewDataFromDB(int nextId) {
        Cursor cursor = subDataBaseHelper.getLsIndexDataS6(nextId);
        if (cursor != null && cursor.moveToFirst()) {
            while (!cursor.isAfterLast()) {

                LsIndexPreviewData indexDataS6 = new LsIndexPreviewData();
                indexDataS6.setId(cursor.getInt(cursor.getColumnIndex(SubDataBaseHelper.KEY_ID)));
                indexDataS6.setNav_section_id(cursor.getString(cursor.getColumnIndex(SubDataBaseHelper.KEY_NAV_SECTION_ID)));
                indexDataS6.setPosition(cursor.getInt(cursor.getColumnIndex(SubDataBaseHelper.KEY_ID)));
                indexDataS6.setImage_renditions(cursor.getString(cursor.getColumnIndex(SubDataBaseHelper.KEY_IMAGE_RENDITIONS)));
                indexDataS6.setHtml_title(cursor.getString(cursor.getColumnIndex(SubDataBaseHelper.KEY_TITLE_HTML)));
                indexDataS6.setSubtitle(cursor.getString(cursor.getColumnIndex(SubDataBaseHelper.KEY_SUB_TITLE)));
                indexDataS6.setPreview(cursor.getString(cursor.getColumnIndex(SubDataBaseHelper.KEY_PREVIEW)));
                indexDataS6.setUri(cursor.getString(cursor.getColumnIndex(SubDataBaseHelper.KEY_URI)));
                indexDataS6.setSubitem_id(cursor.getInt(cursor.getColumnIndex(SubDataBaseHelper.KEY_SUBITEM_ID)));
//                indexDataS6.setVerseDatas(getSubItemHtmlData(cursor.getInt(cursor.getColumnIndex(SubDataBaseHelper.KEY_ID))));
                lsIndexPreviewDataArray.add(indexDataS6);
                cursor.moveToNext();
            }
        }
        return lsIndexPreviewDataArray;
    }

    public String getWebViewData(int nextId) {
        Cursor cursor = subDataBaseHelper.getReadingHtmlData(nextId);
        String readingData = null;
        if (cursor != null && cursor.moveToFirst()) {
            while (!cursor.isAfterLast()) {

                byte[] blob = cursor.getBlob(cursor.getColumnIndex(SubDataBaseHelper.KEY_SUB_ITEM_HTML_C1CONTENT_HTML));

                readingData = new String(blob);
                cursor.moveToNext();
            }
        }
        subDataBaseHelper.close();
        if (cursor != null) {
            cursor.close();
        }

        return readingData;
    }

    public String getHeaderString(int nextId) {
        Cursor cursor = subDataBaseHelper.getReadingHtmlData(nextId);
        if (cursor != null && cursor.moveToFirst()) {
            Elements studySummary = null;

            while (!cursor.isAfterLast()) {

                byte[] blob = cursor.getBlob(cursor.getColumnIndex(SubDataBaseHelper.KEY_SUB_ITEM_HTML_C1CONTENT_HTML));

                String readingData = new String(blob);
                Document doc = Jsoup.parse(readingData);

                studySummary = doc.select("header").select("p");

                for (int i = 0; i < studySummary.size(); i++) {

                    String name_class = studySummary.get(i).attr("class");

                    if (name_class.equals("study-intro")) {
                        headerContent = studySummary.get(i).text();
                    } else if (name_class.equals("study-summary")) {
                        headerContent = headerContent + studySummary.get(i).text();
                    } else if (name_class.equals("studyIntro")) {
                        headerContent = headerContent + studySummary.get(i).text();
                    } else if (name_class.equals("kicker")) {
                        headerContent = headerContent + studySummary.get(i).text();
                    } else if (name_class.equals("intro")) {
                        headerContent = headerContent + studySummary.get(i).text();
                    } else if (name_class.equals("authorName")) {
                        headerContent = headerContent + studySummary.get(i).text();
                    }
                }

                cursor.moveToNext();
            }

        }
        subDataBaseHelper.close();
        if (cursor != null) {
            cursor.close();
        }

        return headerContent;
    }

    public ArrayList<VerseData> getSubItemHtmlData(int nextId) {

        final ArrayList<VerseData> stringArrayList = new ArrayList<>();
        ArrayList<String> selectedArrayList = new ArrayList<>();
        Cursor cursor = subDataBaseHelper.getReadingHtmlData(nextId);
        if (cursor != null && cursor.moveToFirst()) {
            while (!cursor.isAfterLast()) {

                byte[] blob = cursor.getBlob(cursor.getColumnIndex(SubDataBaseHelper.KEY_SUB_ITEM_HTML_C1CONTENT_HTML));
                String readingData = new String(blob);
                Document doc = Jsoup.parse(readingData);

                Elements tvVersemain = doc.select("div[class^=body-block]");
                Elements tvVerse = tvVersemain.select("p[class^=verse]");

                if (!tvVerse.isEmpty() && tvVerse != null && !tvVerse.equals("null")) {

                    Elements tvVerseNo2 = tvVerse.select("span[class^=verse-number]"); //output.. <span class="verse-number">1 </span>
                    for (int i = 0; i < tvVerseNo2.size(); i++) {

                        Element element = tvVerseNo2.get(i);
                        Element element2 = tvVerse.get(i);

                        Elements tvVerseSpan = element2.select("span[class^=study-note-ref]");
                        for (int j = 0; j < tvVerseSpan.size(); j++) {
                            String e2 = tvVerseSpan.get(j).text();
                            selectedArrayList.add(e2);
                        }

                        List<IndexWrapper> indexWrapperList = Global.findIndexesForKeyword(element2.outerHtml(), "data-aid");

                        String[] dataAid = new String[0];
                        for (int k = 0; k < indexWrapperList.size(); k++) {
                            String substring = element2.outerHtml().substring(indexWrapperList.get(k).getEnd() + 2);
                            dataAid = substring.split("\"");
                        }

                        VerseData verseData = new VerseData();
                        verseData.setId(element.text());  //.. output: 1,2,..
                        verseData.setStringSentense(element2.outerHtml()); // output: <p class="verse" data-aid="128421397" id="p1"><span class="verse-number">1 </span>Thus the heavens and the <span class="study-note-ref" data-ref="note1a"><sup class="marker">a</sup>earth</span> were finished, and all the <span class="study-note-ref" data-ref="note1b"><sup class="marker">b</sup>host</span> of them.</p>
                        verseData.setFootnoteArray(selectedArrayList); //output: data-ref="abegginning", "bGod".......
                        verseData.setData_aid(dataAid[0]); // output: <p class="verse" data-aid="128421334"
                        verseData.setLayoutflag(1);
                        stringArrayList.add(verseData);
                    }
                } else {

                    Elements twoElements = tvVersemain.select("p,video,figure");

                    for (int i = 0; i < twoElements.size(); i++) {

                        String ptag = twoElements.get(i).tagName();
                        VerseData verseData = new VerseData();

                        if (ptag.equals("p")) {

                            String pid = twoElements.get(i).select("p").attr("id");

                            if (pid.startsWith("p")) {
                                verseData.setId(pid.substring(1));
                            } else {
                                verseData.setId("");
                            }

                            verseData.setStringSentense(twoElements.get(i).outerHtml().toString());
                            verseData.setFootnoteArray(null);
                            verseData.setData_aid(twoElements.get(i).attr("data-aid"));
                            verseData.setLayoutflag(1);

                        } else if (ptag.equals("video")) {

                            Elements source_url = twoElements.get(i).select("video").select("source");
                            ArrayList<String> videoUrlArray = new ArrayList<>();

                            for (int j = 0; j < source_url.size(); j++) {

                                String innervideo_src = source_url.get(j).attr("type");

                                if (innervideo_src.equals("video/mp4")) {
                                    String video_src = source_url.get(j).attr("src");
                                    videoUrlArray.add(video_src.trim());
                                }
                            }

                            verseData.setVideoUrlArray(videoUrlArray);
                            verseData.setLayoutflag(2);

                        } else if (ptag.equals("figure")) {

                            Elements source_url = twoElements.get(i).select("figure").select("a").select("img");
                            ArrayList<String> imageUrlArray = new ArrayList<>();

                            for (int j = 0; j < source_url.size(); j++) {
                                String image_src = source_url.get(j).attr("src");
                                imageUrlArray.add(image_src.trim());
                            }

                            verseData.setImageUrlArray(imageUrlArray);
                            verseData.setLayoutflag(3);
                        } else {
                            verseData.setLayoutflag(0);
                        }

                        stringArrayList.add(verseData);

                    }


                }
                cursor.moveToNext();
            }
        }
        subDataBaseHelper.close();
        if (cursor != null) {
            cursor.close();
        }

        return stringArrayList;
    }

    public ArrayList<VerseData> getCrossRefSubItemHtmlData(int nextId, String uri) {
        final ArrayList<VerseData> stringArrayList = new ArrayList<>();
        ArrayList<String> selectedArrayList = new ArrayList<>();
        Cursor cursor = subDataBaseHelper.getCrossRefReadingHtmlData(nextId);
        if (cursor != null && cursor.moveToFirst()) {
            while (!cursor.isAfterLast()) {

                byte[] blob = cursor.getBlob(cursor.getColumnIndex(SubDataBaseHelper.KEY_SUB_ITEM_HTML_C1CONTENT_HTML));

                String readingData = new String(blob);
                Document doc = Jsoup.parse(readingData);

                Elements tvVersemain = doc.select("div[class^=body-block]");
                Elements tvVerse = tvVersemain.select("p[class^=verse]");

                if (!tvVerse.isEmpty() && tvVerse != null && !tvVerse.equals("null")) {

                    Elements tvVerseNo2 = tvVerse.select("span[class^=verse-number]"); //output.. <span class="verse-number">1 </span>
                    for (int i = 0; i < tvVerseNo2.size(); i++) {
                        Element element = tvVerseNo2.get(i);
                        Element element2 = tvVerse.get(i);
                        Elements tvVerseSpan = element2.select("span[class^=study-note-ref]");
                        for (int j = 0; j < tvVerseSpan.size(); j++) {
                            String e2 = tvVerseSpan.get(j).text();
                            selectedArrayList.add(e2);
                        }

                        List<IndexWrapper> indexWrapperList = Global.findIndexesForKeyword(element2.outerHtml(), "data-aid");
                        String[] dataAid = new String[0];
                        for (int k = 0; k < indexWrapperList.size(); k++) {
                            String substring = element2.outerHtml().substring(indexWrapperList.get(k).getEnd() + 2);
                            dataAid = substring.split("\"");
                        }

                        VerseData verseData = new VerseData();
                        verseData.setId(element.text());  //.. output: 1,2,..
                        verseData.setStringSentense(element2.outerHtml()); // output: <p class="verse" data-aid="128421397" id="p1"><span class="verse-number">1 </span>Thus the heavens and the <span class="study-note-ref" data-ref="note1a"><sup class="marker">a</sup>earth</span> were finished, and all the <span class="study-note-ref" data-ref="note1b"><sup class="marker">b</sup>host</span> of them.</p>
                        verseData.setFootnoteArray(selectedArrayList); //output: data-ref="abegginning", "bGod".......
                        verseData.setData_aid(dataAid[0]); // output: <p class="verse" data-aid="128421334"
                        verseData.setUri(uri); //... BHOOMI
                        stringArrayList.add(verseData);
                    }

                } else {

                    Elements tvVerse1 = tvVersemain.select("p");


                    for (int i = 0; i < tvVerse1.size(); i++) {

                        VerseData verseData = new VerseData();
                        String verseID = "";
                        String pid = tvVerse1.get(i).select("p").attr("id");
                        if (pid.startsWith("p")) {
                            verseID = pid.substring(1);
                        }

                        verseData.setId(verseID);
                        verseData.setStringSentense(String.valueOf(Html.fromHtml(tvVerse1.get(i).outerHtml())));
                        verseData.setFootnoteArray(null);
                        verseData.setData_aid(tvVerse1.get(i).attr("data-aid"));

                        stringArrayList.add(verseData);

                    }


                }
                cursor.moveToNext();
            }
        }
        subDataBaseHelper.close();
        if (cursor != null) {
            cursor.close();
        }

        return stringArrayList;
    }

    public String getTitleOnUrl(String uri) {
        String title = "";
        Cursor cursor = subDataBaseHelper.getTitleOnUrl(uri);
        if (cursor != null && cursor.getCount() > 0) {
            title = cursor.getString(cursor.getColumnIndex("title_html"));
        }
        subDataBaseHelper.close();
        if (cursor != null) {
            cursor.close();
        }

        return title;
    }


}
