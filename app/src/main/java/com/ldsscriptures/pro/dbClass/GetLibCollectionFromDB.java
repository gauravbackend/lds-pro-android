package com.ldsscriptures.pro.dbClass;

import android.app.Activity;
import android.content.Context;
import android.database.Cursor;
import android.util.Log;

import com.ldsscriptures.pro.database.DataBaseHelper;
import com.ldsscriptures.pro.database.SubDataBaseHelper;
import com.ldsscriptures.pro.model.GlobalGroupSearch;
import com.ldsscriptures.pro.model.GlobalSearchModel;
import com.ldsscriptures.pro.model.IndexWrapper;
import com.ldsscriptures.pro.model.Language;
import com.ldsscriptures.pro.model.LcData;
import com.ldsscriptures.pro.model.LsData;
import com.ldsscriptures.pro.model.NavItemData;
import com.ldsscriptures.pro.model.SerachData;
import com.ldsscriptures.pro.utils.Global;
import com.ldsscriptures.pro.utils.SessionManager;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class GetLibCollectionFromDB {
    private final DataBaseHelper databasehelper;
    Context context;
    private SessionManager sessionManager;

    public GetLibCollectionFromDB(Context context) {
        this.context = context;
        databasehelper = new DataBaseHelper(context);
        sessionManager = new SessionManager(context);
    }

    public ArrayList<LcData> getLcArrayListFromDB() {
        ArrayList<LcData> lcDataArrayList = new ArrayList<>();
        Cursor cursor;
        try {
            cursor = databasehelper.getLCData();
            if (cursor != null && cursor.moveToFirst()) {
                while (!cursor.isAfterLast()) {
                    LcData lcData = new LcData();

                    lcData.setId(cursor.getString(cursor.getColumnIndex(DataBaseHelper.KEY_ID)));
                    lcData.setExternal_id(cursor.getString(cursor.getColumnIndex(DataBaseHelper.KEY_EXTERNAL_ID)));
                    lcData.setLibrary_section_id(cursor.getInt(cursor.getColumnIndex(DataBaseHelper.KEY_LC_SECTION_ID)));
                    lcData.setLibrary_section_external_id(cursor.getString(cursor.getColumnIndex(DataBaseHelper.KEY_LC_LIB_SECTION_EXTERNAL_ID)));
                    lcData.setPosition(cursor.getInt(cursor.getColumnIndex(DataBaseHelper.KEY_POSITION)));
                    lcData.setTitle(cursor.getString(cursor.getColumnIndex(DataBaseHelper.KEY_LC_TITLE_HTML)));
                    lcData.setCover_renditions(cursor.getString(cursor.getColumnIndex(DataBaseHelper.KEY_LC_COVER_RENDITIONS)));
                    lcData.setType_id(cursor.getInt(cursor.getColumnIndex(DataBaseHelper.KEY_LC_TYPE_ID)));
                    lcData.setLsDatas(getLsDataFromDb(lcData.getId()));
                    lcDataArrayList.add(lcData);
                    cursor.moveToNext();
                }
            }
            if (cursor != null) {
                cursor.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return lcDataArrayList;

    }

    public ArrayList<LsData> getLsDataFromDb(String itemId) {

        Cursor cursor;
        cursor = databasehelper.getLSData(Integer.parseInt(itemId));

        ArrayList<LsData> lsDataArrayList = new ArrayList<>();
        if (cursor != null && cursor.moveToFirst()) {
            while (!cursor.isAfterLast()) {

                LsData lsData = new LsData();
                lsData.setId(cursor.getInt(cursor.getColumnIndex(DataBaseHelper.KEY_ID)));
                lsData.setExternal_id(cursor.getString(cursor.getColumnIndex(DataBaseHelper.KEY_EXTERNAL_ID)));
                lsData.setLibrary_section_id(cursor.getInt(cursor.getColumnIndex(DataBaseHelper.KEY_LS_RESULT_LIBRARY_SECTION_ID)));
                lsData.setLibrary_section_external_id(cursor.getString(cursor.getColumnIndex(DataBaseHelper.KEY_LS_RESULT_LS_EXTERNAL_ID)));
                lsData.setPosition(cursor.getInt(cursor.getColumnIndex(DataBaseHelper.KEY_POSITION)));
                lsData.setTitle(cursor.getString(cursor.getColumnIndex(DataBaseHelper.KEY_LS_RESULT_TITLE_HTML)));
                lsData.setIs_obsolete(cursor.getString(cursor.getColumnIndex(DataBaseHelper.KEY_LS_RESULT_IS_OBSOLETE)));
                lsData.setItem_id(cursor.getInt(cursor.getColumnIndex(DataBaseHelper.KEY_LS_RESULT_ITEM_ID)));
                lsData.setItem_external_id(cursor.getString(cursor.getColumnIndex(DataBaseHelper.KEY_LS_RESULT_ITEM_EXTERNAL_ID)));
                lsData.setItem_cover_renditions(cursor.getString(cursor.getColumnIndex(DataBaseHelper.KEY_LS_RESULT_ITEM_COVER_RENDITIONS)));
                lsData.setUri(cursor.getString(cursor.getColumnIndex(DataBaseHelper.KEY_LS_RESULT_URI)));
                lsData.setItemid(cursor.getInt(cursor.getColumnIndex(DataBaseHelper.KEY_LS_RESULT_ITEMID)));

                lsDataArrayList.add(lsData);
                cursor.moveToNext();
            }
        }
        return lsDataArrayList;
    }

    public ArrayList<LsData> getLsDataFromDbFirst(String itemId) {

        Cursor cursor;
        cursor = databasehelper.getLSDataFirst(Integer.parseInt(itemId));

        ArrayList<LsData> lsDataArrayList = new ArrayList<>();
        if (cursor != null && cursor.moveToFirst()) {
            while (!cursor.isAfterLast()) {

                LsData lsData = new LsData();
                lsData.setId(cursor.getInt(cursor.getColumnIndex(DataBaseHelper.KEY_ID)));
                lsData.setExternal_id(cursor.getString(cursor.getColumnIndex(DataBaseHelper.KEY_EXTERNAL_ID)));
                lsData.setLibrary_section_id(cursor.getInt(cursor.getColumnIndex(DataBaseHelper.KEY_LS_RESULT_LIBRARY_SECTION_ID)));
                lsData.setLibrary_section_external_id(cursor.getString(cursor.getColumnIndex(DataBaseHelper.KEY_LS_RESULT_LS_EXTERNAL_ID)));
                lsData.setPosition(cursor.getInt(cursor.getColumnIndex(DataBaseHelper.KEY_POSITION)));
                lsData.setTitle(cursor.getString(cursor.getColumnIndex(DataBaseHelper.KEY_LS_RESULT_TITLE_HTML)));
                lsData.setItem_cover_renditions(cursor.getString(cursor.getColumnIndex(DataBaseHelper.KEY_LS_RESULT_ITEM_COVER_RENDITIONS)));

                lsDataArrayList.add(lsData);
                cursor.moveToNext();
            }
        }
        return lsDataArrayList;
    }

    public ArrayList<Language> getLanguageListFromDB() {
        ArrayList<Language> languageArrayList = new ArrayList<>();
        Cursor cursor;
        cursor = databasehelper.getLanguageData();
        SessionManager sessionManager = new SessionManager((Activity) context);
        if (cursor != null && cursor.moveToFirst()) {

            while (!cursor.isAfterLast()) {
                Language language = new Language();
                language.setId(cursor.getString(cursor.getColumnIndex(DataBaseHelper.KEY_ID)));
                if (sessionManager.getLanguageID().equals(language.getId()))
                    language.setSelected(true);
                else language.setSelected(false);
                language.setName(cursor.getString(cursor.getColumnIndex(DataBaseHelper.KEY_NAME)));
                language.setLang_code(cursor.getString(cursor.getColumnIndex("iso639_3")));
                language.setRootLibCollectionID(cursor.getString(cursor.getColumnIndex("root_library_collection_id")));
                languageArrayList.add(language);
                cursor.moveToNext();
            }
        }
        if (cursor != null) {
            cursor.close();
        }
        return languageArrayList;

    }

    public ArrayList<LcData> getSerachLcArrayListFromDB(int start_limit) {
        ArrayList<LcData> lcDataArrayList = new ArrayList<>();
        Cursor cursor;
        try {
            cursor = databasehelper.getLCData();

            if (cursor != null && cursor.moveToFirst()) {
                while (!cursor.isAfterLast()) {
                    LcData lcData = new LcData();

                    lcData.setId(cursor.getString(cursor.getColumnIndex(DataBaseHelper.KEY_ID)));
                    lcData.setExternal_id(cursor.getString(cursor.getColumnIndex(DataBaseHelper.KEY_EXTERNAL_ID)));
                    lcData.setLibrary_section_id(cursor.getInt(cursor.getColumnIndex(DataBaseHelper.KEY_LC_SECTION_ID)));
                    lcData.setLibrary_section_external_id(cursor.getString(cursor.getColumnIndex(DataBaseHelper.KEY_LC_LIB_SECTION_EXTERNAL_ID)));
                    lcData.setPosition(cursor.getInt(cursor.getColumnIndex(DataBaseHelper.KEY_POSITION)));
                    lcData.setTitle(cursor.getString(cursor.getColumnIndex(DataBaseHelper.KEY_LC_TITLE_HTML)));
                    lcData.setCover_renditions(cursor.getString(cursor.getColumnIndex(DataBaseHelper.KEY_LC_COVER_RENDITIONS)));
                    lcData.setType_id(cursor.getInt(cursor.getColumnIndex(DataBaseHelper.KEY_LC_TYPE_ID)));


                    lcData.setLsDatas(getSerachLsDataFromDb(lcData.getId(), start_limit));
                    lcDataArrayList.add(lcData);
                    cursor.moveToNext();
                    System.gc();
                }
            }
            if (cursor != null) {
                cursor.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return lcDataArrayList;

    }

    public ArrayList<LsData> getSerachLsDataFromDb(String itemId, int start_limit) {
        Cursor cursor;
        cursor = databasehelper.getLSData(Integer.parseInt(itemId));
        ArrayList<LsData> lsDataArrayList = new ArrayList<>();
        if (cursor != null && cursor.moveToFirst()) {
            while (!cursor.isAfterLast()) {

                LsData lsData = new LsData();
                lsData.setId(cursor.getInt(cursor.getColumnIndex(DataBaseHelper.KEY_ID)));
                lsData.setExternal_id(cursor.getString(cursor.getColumnIndex(DataBaseHelper.KEY_EXTERNAL_ID)));
                lsData.setLibrary_section_id(cursor.getInt(cursor.getColumnIndex(DataBaseHelper.KEY_LS_RESULT_LIBRARY_SECTION_ID)));
                lsData.setLibrary_section_external_id(cursor.getString(cursor.getColumnIndex(DataBaseHelper.KEY_LS_RESULT_LS_EXTERNAL_ID)));
                lsData.setPosition(cursor.getInt(cursor.getColumnIndex(DataBaseHelper.KEY_POSITION)));
                lsData.setTitle(cursor.getString(cursor.getColumnIndex(DataBaseHelper.KEY_LS_RESULT_TITLE_HTML)));
                lsData.setIs_obsolete(cursor.getString(cursor.getColumnIndex(DataBaseHelper.KEY_LS_RESULT_IS_OBSOLETE)));
                lsData.setItem_id(cursor.getInt(cursor.getColumnIndex(DataBaseHelper.KEY_LS_RESULT_ITEM_ID)));
                lsData.setItem_external_id(cursor.getString(cursor.getColumnIndex(DataBaseHelper.KEY_LS_RESULT_ITEM_EXTERNAL_ID)));
                lsData.setItem_cover_renditions(cursor.getString(cursor.getColumnIndex(DataBaseHelper.KEY_LS_RESULT_ITEM_COVER_RENDITIONS)));

                String fullVerseUri = cursor.getString(cursor.getColumnIndex(DataBaseHelper.KEY_LS_RESULT_URI));
                int Lsid = cursor.getInt(cursor.getColumnIndex(DataBaseHelper.KEY_LS_RESULT_ITEMID));

                lsData.setUri(fullVerseUri);
                lsData.setItemid(Lsid);

                if (isFolderExists(String.valueOf(Lsid))) {
                    lsData.setCombineIndexDataArray(getVerseIndexDataFromDB(fullVerseUri, Lsid, start_limit));
                }

                lsDataArrayList.add(lsData);
                cursor.moveToNext();
                System.gc();
            }
        }
        return lsDataArrayList;
    }

    public ArrayList<NavItemData> getVerseIndexDataFromDB(String fullVerseUri, int LsId, int start_limit) {

        SubDataBaseHelper subDataBaseHelper;
        subDataBaseHelper = new SubDataBaseHelper(context, String.valueOf(LsId));

        Cursor cursor;
        cursor = subDataBaseHelper.getserachVerseTextFromURI(fullVerseUri, start_limit);

        ArrayList<NavItemData> navItemDataArrayList = new ArrayList<>();

        if (cursor != null && cursor.moveToFirst()) {
            while (!cursor.isAfterLast()) {

                String LsHrmlTextItem = cursor.getString(cursor.getColumnIndex(DataBaseHelper.KEY_LS_RESULT_TITLE_HTML));
                byte[] blob = cursor.getBlob(cursor.getColumnIndex(SubDataBaseHelper.KEY_SUB_ITEM_HTML_C1CONTENT_HTML));
                String idForReading = cursor.getString(cursor.getColumnIndex(SubDataBaseHelper.KEY_ID));
                String newHtmlVerseArray = new String(blob);
                String readinguri = cursor.getString(cursor.getColumnIndex("uri"));

                NavItemData navItemData = new NavItemData();
                navItemData.setId(Integer.parseInt(idForReading));
                navItemData.setTitle(LsHrmlTextItem);
                navItemData.setUri(readinguri);

                navItemData.setVerseTextArraylist(getSubItemHtmlData(newHtmlVerseArray));
                navItemDataArrayList.add(navItemData);

                cursor.moveToNext();

                System.gc();
            }
        }
        subDataBaseHelper.close();
        if (cursor != null) {
            cursor.close();
        }
        return navItemDataArrayList;
    }

    public ArrayList<SerachData> getSubItemHtmlData(String newHtmlVerseArray) {

        final ArrayList<SerachData> stringArrayList = new ArrayList<>();
        ArrayList<String> selectedArrayList = new ArrayList<>();
        Document doc = Jsoup.parse(newHtmlVerseArray);

        Elements tvVersemain = doc.select("div[class^=body-block]");
        Elements tvVerse = tvVersemain.select("p[class^=verse]");

        if (!tvVerse.isEmpty() && tvVerse != null && !tvVerse.equals("null")) {

            Elements tvVerseNo2 = tvVerse.select("span[class^=verse-number]");

            for (int i = 0; i < tvVerseNo2.size(); i++) {

                Element element = tvVerseNo2.get(i);
                Element element2 = tvVerse.get(i);

                Elements tvVerseSpan = element2.select("span[class^=study-note-ref]");
                for (int j = 0; j < tvVerseSpan.size(); j++) {
                    String e2 = tvVerseSpan.get(j).text();
                    selectedArrayList.add(e2);
                }

                List<IndexWrapper> indexWrapperList = Global.findIndexesForKeyword(element2.outerHtml(), "data-aid");

                String[] dataAid = new String[0];
                for (int k = 0; k < indexWrapperList.size(); k++) {
                    String substring = element2.outerHtml().substring(indexWrapperList.get(k).getEnd() + 2);
                    dataAid = substring.split("\"");
                }

                SerachData serachData = new SerachData();
                serachData.setStringId(element.text());
                serachData.setStringSentense(element2.outerHtml());
                serachData.setSetData_aid(dataAid[0]);
                stringArrayList.add(serachData);

                System.gc();
            }
        } else {

            Elements tvVerse1 = tvVersemain.select("p");

            for (int i = 0; i < tvVerse1.size(); i++) {

                SerachData serachData = new SerachData();
                serachData.setStringId(String.valueOf(i + 1));
                serachData.setStringSentense(tvVerse1.get(i).outerHtml());
                serachData.setSetData_aid(tvVerse1.get(i).attr("data-aid"));
                stringArrayList.add(serachData);

                System.gc();
            }

        }

        return stringArrayList;
    }

    private boolean isFolderExists(String id) {
        File destDir = new File(Global.AppFolderPath + id);

        if (!destDir.exists()) {
            return false;
        } else {
            if (destDir.isDirectory()) {
                String[] files = destDir.list();
                if (files == null)
                    return false;

                if (files.length == 0) {
                    //directory is empty
                    return false;
                } else
                    return true;
            }
        }
        return false;
    }

}

