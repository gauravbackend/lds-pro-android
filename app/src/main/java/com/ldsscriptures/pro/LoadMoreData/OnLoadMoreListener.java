package com.ldsscriptures.pro.LoadMoreData;

/**
 * Created by IBL InfoTech on 9/8/2017.
 */

public interface OnLoadMoreListener {
    void onLoadMore();
}