package com.ldsscriptures.pro.model;

public class TabsData {

    String path;
    String tag;

    String subitemName;

    String folderName;

    int iPosition;

    public int getIndexForReading() {
        return indexForReading;
    }

    public void setIndexForReading(int indexForReading) {
        this.indexForReading = indexForReading;
    }

    int indexForReading;

    public int getiPosition() {
        return iPosition;
    }

    public void setiPosition(int iPosition) {
        this.iPosition = iPosition;
    }

    public String getIndexTitle() {
        return indexTitle;
    }

    public void setIndexTitle(String indexTitle) {
        this.indexTitle = indexTitle;
    }

    String indexTitle;


    public String getSubitemName() {
        return subitemName;
    }

    public void setSubitemName(String subitemName) {
        this.subitemName = subitemName;
    }

    public String getFolderName() {
        return folderName;
    }

    public void setFolderName(String folderName) {
        this.folderName = folderName;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }
}
