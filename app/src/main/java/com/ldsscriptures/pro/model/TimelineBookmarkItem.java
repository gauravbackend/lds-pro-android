package com.ldsscriptures.pro.model;

/**
 * Created by IBL InfoTech on 8/5/2017.
 */

public class TimelineBookmarkItem {
    private String id;

    private String bookmark_nephi;

    private String bookmark_time_ago;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getBookmark_nephi() {
        return bookmark_nephi;
    }

    public void setBookmark_nephi(String bookmark_nephi) {
        this.bookmark_nephi = bookmark_nephi;
    }

    public String getBookmark_time_ago() {
        return bookmark_time_ago;
    }

    public void setBookmark_time_ago(String bookmark_time_ago) {
        this.bookmark_time_ago = bookmark_time_ago;
    }

    @Override
    public String toString() {
        return "TimelineBookmarkItem{" +
                "id='" + id + '\'' +
                ", bookmark_nephi='" + bookmark_nephi + '\'' +
                ", bookmark_time_ago='" + bookmark_time_ago + '\'' +
                '}';
    }
}
