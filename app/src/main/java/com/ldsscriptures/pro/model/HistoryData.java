package com.ldsscriptures.pro.model;

import java.util.ArrayList;

public class HistoryData {
    public String date;
    public ArrayList<HistorySubData> historyDataArrayList=new ArrayList<>();

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public ArrayList<HistorySubData> getHistoryDataArrayList() {
        return historyDataArrayList;
    }

    public void setHistoryDataArrayList(ArrayList<HistorySubData> historyDataArrayList) {
        this.historyDataArrayList = historyDataArrayList;
    }
}

