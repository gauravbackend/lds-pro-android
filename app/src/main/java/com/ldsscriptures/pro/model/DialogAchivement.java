package com.ldsscriptures.pro.model;

public class DialogAchivement {
    int drwable;
    String varTitle;
    int flag;

    public int getDrwable() {
        return drwable;
    }

    public void setDrwable(int drwable) {
        this.drwable = drwable;
    }

    public String getVarTitle() {
        return varTitle;
    }

    public void setVarTitle(String varTitle) {
        this.varTitle = varTitle;
    }

    public int getFlag() {
        return flag;
    }

    public void setFlag(int flag) {
        this.flag = flag;
    }


    @Override
    public String toString() {
        return "DialogAchivement{" +
                "drwable=" + drwable +
                ", varTitle='" + varTitle + '\'' +
                ", flag=" + flag +
                '}';
    }
}