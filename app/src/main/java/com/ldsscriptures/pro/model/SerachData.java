package com.ldsscriptures.pro.model;


import java.io.Serializable;
import java.util.ArrayList;

public class SerachData implements Serializable {

    String stringSentense;
    String stringlsTitle;
    String stringlcTitle;
    String stringSubTitle;
    String stringId;
    String setData_aid;
    String setData_URL;
    String setLcCount;

    public String getSetLcCount() {
        return setLcCount;
    }

    public void setSetLcCount(String setLcCount) {
        this.setLcCount = setLcCount;
    }

    public String getStringlcTitle() {
        return stringlcTitle;
    }

    public void setStringlcTitle(String stringlcTitle) {
        this.stringlcTitle = stringlcTitle;
    }

    public String getSetData_URL() {
        return setData_URL;
    }

    public void setSetData_URL(String setData_URL) {
        this.setData_URL = setData_URL;
    }

    public String getStringId() {
        return stringId;
    }

    public void setStringId(String stringId) {
        this.stringId = stringId;
    }

    public String getSetData_aid() {
        return setData_aid;
    }

    public void setSetData_aid(String setData_aid) {
        this.setData_aid = setData_aid;
    }

    public String getStringlsTitle() {
        return stringlsTitle;
    }

    public void setStringlsTitle(String stringlsTitle) {
        this.stringlsTitle = stringlsTitle;
    }

    public String getStringSubTitle() {
        return stringSubTitle;
    }

    public void setStringSubTitle(String stringSubTitle) {
        this.stringSubTitle = stringSubTitle;
    }

    public String getStringSentense() {
        return stringSentense;
    }

    public void setStringSentense(String stringSentense) {
        this.stringSentense = stringSentense;
    }


    @Override
    public String toString() {
        return "SerachData{" +
                "stringSentense='" + stringSentense + '\'' +
                '}';
    }
}
