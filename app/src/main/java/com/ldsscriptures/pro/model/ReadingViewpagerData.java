package com.ldsscriptures.pro.model;

import java.util.ArrayList;

/**
 * Created by iblinfotech on 13/07/17.
 */

public class ReadingViewpagerData {

    String image_rendition;
    String preview;
    int subitem_id;
    String title_html;
    String image_renditions;
    String subtitle;
    String uri;
    int id;
    int position;
    boolean isHeader;
    int nav_section_id;


    public boolean isHeader() {
        return isHeader;
    }

    public void setHeader(boolean header) {
        isHeader = header;
    }

    public ArrayList<LsSubIndexData> getLsSubIndexDatas() {
        return lsSubIndexDatas;
    }

    public void setLsSubIndexDatas(ArrayList<LsSubIndexData> lsSubIndexDatas) {
        this.lsSubIndexDatas = lsSubIndexDatas;
    }

    ArrayList<LsSubIndexData> lsSubIndexDatas;

    public String getImage_rendition() {
        return image_rendition;
    }

    public void setImage_rendition(String image_rendition) {
        this.image_rendition = image_rendition;
    }

    public String getPreview() {
        return preview;
    }

    public void setPreview(String preview) {
        this.preview = preview;
    }

    public int getSubitem_id() {
        return subitem_id;
    }

    public void setSubitem_id(int subitem_id) {
        this.subitem_id = subitem_id;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public int getNav_section_id() {
        return nav_section_id;
    }

    public void setNav_section_id(int nav_section_id) {
        this.nav_section_id = nav_section_id;
    }

    public String getTitle_html() {
        return title_html;
    }

    public void setTitle_html(String title_html) {
        this.title_html = title_html;
    }

    public String getImage_renditions() {
        return image_renditions;
    }

    public void setImage_renditions(String image_renditions) {
        this.image_renditions = image_renditions;
    }

    public String getSubtitle() {
        return subtitle;
    }

    public void setSubtitle(String subtitle) {
        this.subtitle = subtitle;
    }

    public String getUri() {
        return uri;
    }

    public void setUri(String uri) {
        this.uri = uri;
    }

    @Override
    public String toString() {
        return "ReadingViewpagerData{" +
                "preview='" + preview + '\'' +
                ", subitem_id=" + subitem_id +
                ", title_html='" + title_html + '\'' +
                ", id=" + id +
                ", position=" + position +
                '}';
    }
}
