package com.ldsscriptures.pro.model;

import java.util.ArrayList;

public class LsSubIndexData {

    int id;
    String nav_collection_id;
    int position;
    String title;
    int indent_level;

    ArrayList<LsIndexPreviewData> lsIndexPreviewDatas;

    public ArrayList<LsIndexPreviewData> getLsIndexPreviewDatas() {
        return lsIndexPreviewDatas;
    }

    public void setLsIndexPreviewDatas(ArrayList<LsIndexPreviewData> lsIndexPreviewDatas) {
        this.lsIndexPreviewDatas = lsIndexPreviewDatas;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNav_collection_id() {
        return nav_collection_id;
    }

    public void setNav_collection_id(String nav_collection_id) {
        this.nav_collection_id = nav_collection_id;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getIndent_level() {
        return indent_level;
    }

    public void setIndent_level(int indent_level) {
        this.indent_level = indent_level;
    }
}
