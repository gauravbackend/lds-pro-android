package com.ldsscriptures.pro.model;


import java.util.ArrayList;

public class SyncResponse<T> {

    private String last_sync_date;

    private ArrayList<SyncItems> items;

    private String sync_again;

    private String user_id;

    public String getLast_sync_date() {
        return last_sync_date;
    }

    public void setLast_sync_date(String last_sync_date) {
        this.last_sync_date = last_sync_date;
    }

    public ArrayList<SyncItems> getItems() {
        return items;
    }

    public void setItems(ArrayList<SyncItems> items) {
        this.items = items;
    }

    public String getSync_again() {
        return sync_again;
    }

    public void setSync_again(String sync_again) {
        this.sync_again = sync_again;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }
}
