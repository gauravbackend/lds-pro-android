package com.ldsscriptures.pro.model;


import java.util.ArrayList;

public class SpanData {

    private String verseNumber;
//    private String spannedText;

    private String verse_aid;
    private String verseText;

    private ArrayList<SpannedTextData> spannedTextDatas = new ArrayList<>();

    public ArrayList<SpannedTextData> getSpannedTextDatas() {
        return spannedTextDatas;
    }

    public void setSpannedTextDatas(ArrayList<SpannedTextData> spannedTextDatas) {
        this.spannedTextDatas = spannedTextDatas;
    }


    public String getVerseNumber() {
        return verseNumber;
    }

    public void setVerseNumber(String verseNumber) {
        this.verseNumber = verseNumber;
    }


    public String getVerse_aid() {
        return verse_aid;
    }

    public void setVerse_aid(String verse_aid) {
        this.verse_aid = verse_aid;
    }

    public String getVerseText() {
        return verseText;
    }

    public void setVerseText(String verseText) {
        this.verseText = verseText;
    }

    @Override
    public String toString() {
        return "SpanData{" +
                "verseText='" + spannedTextDatas.toString() + '\'' +
                '}';
    }
}
