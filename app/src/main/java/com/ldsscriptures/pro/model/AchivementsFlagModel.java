package com.ldsscriptures.pro.model;

/**
 * Created by IBL InfoTech on 7/25/2017.
 */

public class AchivementsFlagModel {
    private int achivements_id;
    private Boolean achivements_flag;
    private Boolean achivements_dialogflag;
    private String Identifier;
    private String serverid;
    private String achivements_date;

    public AchivementsFlagModel() {
    }

    public AchivementsFlagModel(int achivements_id, Boolean achivements_flag, Boolean achivements_dialogflag, String Identifier, String serverid, String achivements_date) {
        this.achivements_id = achivements_id;
        this.achivements_flag = achivements_flag;
        this.achivements_dialogflag = achivements_dialogflag;
        this.Identifier = Identifier;
        this.serverid = serverid;
        this.achivements_date = achivements_date;
    }

    public String getServerid() {
        return serverid;
    }

    public void setServerid(String serverid) {
        this.serverid = serverid;
    }

    public int getAchivements_id() {
        return achivements_id;
    }

    public void setAchivements_id(int achivements_id) {
        this.achivements_id = achivements_id;
    }

    public Boolean getAchivements_flag() {
        return achivements_flag;
    }

    public void setAchivements_flag(Boolean achivements_flag) {
        this.achivements_flag = achivements_flag;
    }

    public Boolean getAchivements_dialogflag() {
        return achivements_dialogflag;
    }

    public void setAchivements_dialogflag(Boolean achivements_dialogflag) {
        this.achivements_dialogflag = achivements_dialogflag;
    }

    public String getIdentifier() {
        return Identifier;
    }

    public void setIdentifier(String identifier) {
        Identifier = identifier;
    }

    public String getAchivements_date() {
        return achivements_date;
    }

    public void setAchivements_date(String achivements_date) {
        this.achivements_date = achivements_date;
    }

    @Override
    public String toString() {
        return "AchivementsFlagModel{" +
                "achivements_id=" + achivements_id +
                ", achivements_flag=" + achivements_flag +
                ", achivements_dialogflag=" + achivements_dialogflag +
                ", Identifier='" + Identifier + '\'' +
                ", serverid='" + serverid + '\'' +
                ", achivements_date='" + achivements_date + '\'' +
                '}';
    }
}
