package com.ldsscriptures.pro.model;


public class VerseTagsData {
    private String serverid;
    private String verseaid;
    private String chapterUri;
    private String itemuri;
    private String bookuri;
    private String tagTitle;
    private String verseNumber;
    private String verseText;
    private boolean isSelected;
    private String tagname;
    private String verse;
    private String deleted;
    private String modified;

    public String getDeleted() {
        return deleted;
    }

    public void setDeleted(String deleted) {
        this.deleted = deleted;
    }

    public String getModified() {
        return modified;
    }

    public void setModified(String modified) {
        this.modified = modified;
    }

    public String getVerse() {
        return verse;
    }

    public void setVerse(String verse) {
        this.verse = verse;
    }

    public String getTagname() {
        return tagname;
    }

    public void setTagname(String tagname) {
        this.tagname = tagname;
    }

    public String getServerid() {
        return serverid;
    }

    public void setServerid(String serverid) {
        this.serverid = serverid;
    }

    public String getVerseNumber() {
        return verseNumber;
    }

    public void setVerseNumber(String verseNumber) {
        this.verseNumber = verseNumber;
    }

    public String getVerseText() {
        return verseText;
    }

    public void setVerseText(String verseText) {
        this.verseText = verseText;
    }

    public String getVerseaid() {
        return verseaid;
    }

    public void setVerseaid(String verseaid) {
        this.verseaid = verseaid;
    }

    public String getChapterUri() {
        return chapterUri;
    }

    public void setChapterUri(String chapterUri) {
        this.chapterUri = chapterUri;
    }

    public String getItemuri() {
        return itemuri;
    }

    public void setItemuri(String itemuri) {
        this.itemuri = itemuri;
    }

    public String getBookuri() {
        return bookuri;
    }

    public void setBookuri(String bookuri) {
        this.bookuri = bookuri;
    }

    public String getTagTitle() {
        return tagTitle;
    }

    public void setTagTitle(String tagTitle) {
        this.tagTitle = tagTitle;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean isSelected) {
        this.isSelected = isSelected;
    }

    @Override
    public String toString() {
        return "VerseTagsData{" +
                "serverid='" + serverid + '\'' +
                ", verseaid='" + verseaid + '\'' +
                ", chapterUri='" + chapterUri + '\'' +
                ", itemuri='" + itemuri + '\'' +
                ", bookuri='" + bookuri + '\'' +
                ", tagTitle='" + tagTitle + '\'' +
                ", verseNumber='" + verseNumber + '\'' +
                ", verseText='" + verseText + '\'' +
                ", isSelected=" + isSelected +
                ", tagname='" + tagname + '\'' +
                ", verse='" + verse + '\'' +
                ", deleted='" + deleted + '\'' +
                ", modified='" + modified + '\'' +
                '}';
    }
}
