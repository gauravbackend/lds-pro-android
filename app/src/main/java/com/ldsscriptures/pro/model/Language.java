package com.ldsscriptures.pro.model;

/**
 * Created on 17/08/17 by iblinfotech.
 */

public class Language {
    String id, name,lang_code,rootLibCollectionID;
    boolean isSelected;

    public String getRootLibCollectionID() {
        return rootLibCollectionID;
    }

    public void setRootLibCollectionID(String rootLibCollectionID) {
        this.rootLibCollectionID = rootLibCollectionID;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public String getLang_code() {
        return lang_code;
    }

    public void setLang_code(String lang_code) {
        this.lang_code = lang_code;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
