package com.ldsscriptures.pro.model;

import java.util.ArrayList;

/**
 * Created by IBL InfoTech on 7/25/2017.
 */

public class FollowingMainModel<T>{

    private String message;

    private ArrayList<FollowersMainData> followers;

    private String following_user_id;

    private String followed_by;

    private String user_id;

    private String success;

    public String getMessage ()
    {
        return message;
    }

    public void setMessage (String message)
    {
        this.message = message;
    }

    public ArrayList<FollowersMainData> getFollowers() {
        return followers;
    }

    public void setFollowers(ArrayList<FollowersMainData> followers) {
        this.followers = followers;
    }

    public String getFollowing_user_id ()
    {
        return following_user_id;
    }

    public void setFollowing_user_id (String following_user_id)
    {
        this.following_user_id = following_user_id;
    }

    public String getFollowed_by ()
    {
        return followed_by;
    }

    public void setFollowed_by (String followed_by)
    {
        this.followed_by = followed_by;
    }

    public String getUser_id ()
    {
        return user_id;
    }

    public void setUser_id (String user_id)
    {
        this.user_id = user_id;
    }

    public String getSuccess ()
    {
        return success;
    }

    public void setSuccess (String success)
    {
        this.success = success;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [message = "+message+", followers = "+followers+", following_user_id = "+following_user_id+", followed_by = "+followed_by+", user_id = "+user_id+", success = "+success+"]";
    }

}
