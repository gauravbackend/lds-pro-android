package com.ldsscriptures.pro.model;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created on 11/08/17 by iblinfotech.
 */

public class GlobalSearchModel implements Serializable{

   private String nav_col_id;
   private String nav_sel_id;
    private String nav_sel_title;
    private String uri;
    private String subTitle;

    private ArrayList<GlobalSearchVerseModel> globalSearchModels=new ArrayList<>();

    public String getSubTitle() {
        return subTitle;
    }

    public void setSubTitle(String subTitle) {
        this.subTitle = subTitle;
    }

    public String getNav_col_id() {
        return nav_col_id;
    }

    public void setNav_col_id(String nav_col_id) {
        this.nav_col_id = nav_col_id;
    }

    public String getUri() {
        return uri;
    }

    public void setUri(String uri) {/**/
        this.uri = uri;
    }

    public String getItemid() {
        return itemid;
    }

    public void setItemid(String itemid) {
        this.itemid = itemid;
    }

    private String itemid;

    public String getNav_sel_id() {
        return nav_sel_id;
    }

    public void setNav_sel_id(String nav_sel_id) {
        this.nav_sel_id = nav_sel_id;
    }

    public String getNav_sel_title() {
        return nav_sel_title;
    }

    public void setNav_sel_title(String nav_sel_title) {
        this.nav_sel_title = nav_sel_title;
    }

    public ArrayList<GlobalSearchVerseModel> getGlobalSearchModels() {
        return globalSearchModels;
    }

    public void setGlobalSearchModels(ArrayList<GlobalSearchVerseModel> globalSearchModels) {
        this.globalSearchModels = globalSearchModels;
    }


    @Override
    public String toString() {
        return "GlobalSearchModel{" +
                "nav_col_id='" + nav_col_id + '\'' +
                ", nav_sel_id='" + nav_sel_id + '\'' +
                ", nav_sel_title='" + nav_sel_title + '\'' +
                ", uri='" + uri + '\'' +
                ", subTitle='" + subTitle + '\'' +
                ", globalSearchModels=" + globalSearchModels +
                ", itemid='" + itemid + '\'' +
                '}';
    }
}
