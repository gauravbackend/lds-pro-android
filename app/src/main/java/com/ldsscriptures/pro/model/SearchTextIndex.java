package com.ldsscriptures.pro.model;

/**
 * Created by iblinfotech on 20/06/17.
 */

public class SearchTextIndex {
    int searchIndex;
    String string;

    public int getSearchIndex() {
        return searchIndex;
    }

    public void setSearchIndex(int searchIndex) {
        this.searchIndex = searchIndex;
    }

    public String getString() {
        return string;
    }

    public void setString(String string) {
        this.string = string;
    }
}
