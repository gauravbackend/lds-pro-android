package com.ldsscriptures.pro.model;

public class BookmarkData {
    private int id;
    private String Serverid;
    private String verseaid;
    private String chapterUri;
    private String itemuri;
    private String currant_date;
    private String bookuri;
    private String title;
    private String verseNumber;
    private String verseText;
    private String indexfromID;
    private String lcid;
    private String lsid;
    private String deleted;
    private String modified;
    private String sort_order;

    public String getSort_order() {
        return sort_order;
    }

    public void setSort_order(String sort_order) {
        this.sort_order = sort_order;
    }

    public String getDeleted() {
        return deleted;
    }

    public void setDeleted(String deleted) {
        this.deleted = deleted;
    }

    public String getModified() {
        return modified;
    }

    public void setModified(String modified) {
        this.modified = modified;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getServerid() {
        return Serverid;
    }

    public void setServerid(String serverid) {
        Serverid = serverid;
    }

    public String getIndexfromID() {
        return indexfromID;
    }

    public void setIndexfromID(String indexfromID) {
        this.indexfromID = indexfromID;
    }

    public String getLcid() {
        return lcid;
    }

    public void setLcid(String lcid) {
        this.lcid = lcid;
    }

    public String getLsid() {
        return lsid;
    }

    public void setLsid(String lsid) {
        this.lsid = lsid;
    }

    public String getCurrant_date() {
        return currant_date;
    }

    public void setCurrant_date(String currant_date) {
        this.currant_date = currant_date;
    }


    public String getVerseaid() {
        return verseaid;
    }

    public void setVerseaid(String verseaid) {
        this.verseaid = verseaid;
    }

    public String getChapterUri() {
        return chapterUri;
    }

    public void setChapterUri(String chapterUri) {
        this.chapterUri = chapterUri;
    }

    public String getItemuri() {
        return itemuri;
    }

    public void setItemuri(String itemuri) {
        this.itemuri = itemuri;
    }

    public String getBookuri() {
        return bookuri;
    }

    public void setBookuri(String bookuri) {
        this.bookuri = bookuri;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getVerseNumber() {
        return verseNumber;
    }

    public void setVerseNumber(String verseNumber) {
        this.verseNumber = verseNumber;
    }

    public String getVerseText() {
        return verseText;
    }

    public void setVerseText(String verseText) {
        this.verseText = verseText;
    }

    @Override
    public String toString() {
        return "BookmarkData{" +
                "title='" + title + '\'' +
                ", sort_order='" + sort_order + '\'' +
                '}';
    }
}
