package com.ldsscriptures.pro.model;


import java.util.ArrayList;

public class LsIndexPreviewData {

    int id;
    int subitem_id;
    String nav_section_id;
    int position;
    String image_renditions;
    String html_title;
    String subtitle;
    String uri;
    String preview;
    boolean isBookmarked;
    ArrayList<VerseData> verseDatas = new ArrayList<>();

    public ArrayList<VerseData> getVerseDatas() {
        return verseDatas;
    }

    public void setVerseDatas(ArrayList<VerseData> verseDatas) {
        this.verseDatas = verseDatas;
    }

    public boolean isBookmarked() {
        return isBookmarked;
    }

    public void setBookmarked(boolean bookmarked) {
        isBookmarked = bookmarked;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getSubitem_id() {
        return subitem_id;
    }

    public void setSubitem_id(int subitem_id) {
        this.subitem_id = subitem_id;
    }

    public String getNav_section_id() {
        return nav_section_id;
    }

    public void setNav_section_id(String nav_section_id) {
        this.nav_section_id = nav_section_id;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public String getImage_renditions() {
        return image_renditions;
    }

    public void setImage_renditions(String image_renditions) {
        this.image_renditions = image_renditions;
    }

    public String getHtml_title() {
        return html_title;
    }

    public void setHtml_title(String html_title) {
        this.html_title = html_title;
    }

    public String getSubtitle() {
        return subtitle;
    }

    public void setSubtitle(String subtitle) {
        this.subtitle = subtitle;
    }

    public String getUri() {
        return uri;
    }

    public void setUri(String uri) {
        this.uri = uri;
    }

    public String getPreview() {
        return preview;
    }

    public void setPreview(String preview) {
        this.preview = preview;
    }
}
