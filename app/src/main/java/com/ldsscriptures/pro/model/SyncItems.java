package com.ldsscriptures.pro.model;


public class SyncItems {

    private String link_aid;
    private String link_verse;
    private String link_uri;
    private String presented;
    private String id;
    private String title;
    private String verse;
    private String aid;
    private String type;
    private String lesson_id;
    private String uri;
    private String url;
    private String note;
    private String item_type;
    private String points;
    private String minutes;
    private String start_index;
    private String created;
    private String color;
    private String highlight_type;
    private String end_index;
    private String tag;
    private String deleted;
    private String modified;
    private String sort_order;

    public String getSort_order() {
        return sort_order;
    }

    public void setSort_order(String sort_order) {
        this.sort_order = sort_order;
    }

    public String getDeleted() {
        return deleted;
    }

    public void setDeleted(String deleted) {
        this.deleted = deleted;
    }

    public String getModified() {
        return modified;
    }

    public void setModified(String modified) {
        this.modified = modified;
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public String getStart_index() {
        return start_index;
    }

    public void setStart_index(String start_index) {
        this.start_index = start_index;
    }

    public String getCreated() {
        return created;
    }

    public void setCreated(String created) {
        this.created = created;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getHighlight_type() {
        return highlight_type;
    }

    public void setHighlight_type(String highlight_type) {
        this.highlight_type = highlight_type;
    }

    public String getEnd_index() {
        return end_index;
    }

    public void setEnd_index(String end_index) {
        this.end_index = end_index;
    }

    public String getPoints() {
        return points;
    }

    public void setPoints(String points) {
        this.points = points;
    }

    public String getMinutes() {
        return minutes;
    }

    public void setMinutes(String minutes) {
        this.minutes = minutes;
    }

    public String getLink_aid() {
        return link_aid;
    }

    public void setLink_aid(String link_aid) {
        this.link_aid = link_aid;
    }

    public String getLink_verse() {
        return link_verse;
    }

    public void setLink_verse(String link_verse) {
        this.link_verse = link_verse;
    }

    public String getLink_uri() {
        return link_uri;
    }

    public void setLink_uri(String link_uri) {
        this.link_uri = link_uri;
    }

    private String audience;

    public String getAudience() {
        return audience;
    }

    public void setAudience(String audience) {
        this.audience = audience;
    }

    public String getPresented() {
        return presented;
    }

    public void setPresented(String presented) {
        this.presented = presented;
    }

    public String getAchievement_id() {
        return achievement_id;
    }

    public void setAchievement_id(String achievement_id) {
        this.achievement_id = achievement_id;
    }

    private String achievement_id;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getVerse() {
        return verse;
    }

    public void setVerse(String verse) {
        this.verse = verse;
    }

    public String getAid() {
        return aid;
    }

    public void setAid(String aid) {
        this.aid = aid;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getLesson_id() {
        return lesson_id;
    }

    public void setLesson_id(String lesson_id) {
        this.lesson_id = lesson_id;
    }

    public String getUri() {
        return uri;
    }

    public void setUri(String uri) {
        this.uri = uri;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getItem_type() {
        return item_type;
    }

    public void setItem_type(String item_type) {
        this.item_type = item_type;
    }
}
