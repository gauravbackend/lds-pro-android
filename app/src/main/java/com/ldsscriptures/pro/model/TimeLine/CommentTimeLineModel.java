package com.ldsscriptures.pro.model.TimeLine;

import java.util.ArrayList;

/**
 * Created by IBL InfoTech on 9/4/2017.
 */

public class CommentTimeLineModel {

    private String id;

    private String img_url;

    private String uid;

    private String username;

    private String created;

    private ArrayList<LikesTimeLineModel> likes;

    private String comment;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getImg_url() {
        return img_url;
    }

    public void setImg_url(String img_url) {
        this.img_url = img_url;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getCreated() {
        return created;
    }

    public void setCreated(String created) {
        this.created = created;
    }

    public ArrayList<LikesTimeLineModel> getLikes() {
        return likes;
    }

    public void setLikes(ArrayList<LikesTimeLineModel> likes) {
        this.likes = likes;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    @Override
    public String toString() {
        return "ClassPojo [id = " + id + ", img_url = " + img_url + ", uid = " + uid + ", username = " + username + ", created = " + created + ", likes = " + likes + ", comment = " + comment + "]";
    }

    public CommentTimeLineModel(String img_url, String uid, String username, String created, String comment) {
        this.img_url = img_url;
        this.uid = uid;
        this.username = username;
        this.created = created;
        this.comment = comment;
    }

    public CommentTimeLineModel() {

    }

}
