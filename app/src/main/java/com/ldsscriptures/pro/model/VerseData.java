package com.ldsscriptures.pro.model;


import java.util.ArrayList;

public class VerseData {

    String uri;
    String stringSentense;
    String id;
    String data_aid;
    ArrayList<String> footnoteArray = new ArrayList<>();
    String citationCount;
    String versetitle;
    String LsID;
    int NextID;
    int layoutflag;
    String videoUrl;
    String imageUrl;
    ArrayList<String> videoUrlArray = new ArrayList<>();
    ArrayList<String> imageUrlArray = new ArrayList<>();


    public ArrayList<String> getImageUrlArray() {
        return imageUrlArray;
    }

    public void setImageUrlArray(ArrayList<String> imageUrlArray) {
        this.imageUrlArray = imageUrlArray;
    }

    public ArrayList<String> getVideoUrlArray() {
        return videoUrlArray;
    }

    public void setVideoUrlArray(ArrayList<String> videoUrlArray) {
        this.videoUrlArray = videoUrlArray;
    }

    public String getVideoUrl() {
        return videoUrl;
    }

    public void setVideoUrl(String videoUrl) {
        this.videoUrl = videoUrl;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public int getLayoutflag() {
        return layoutflag;
    }

    public void setLayoutflag(int layoutflag) {
        this.layoutflag = layoutflag;
    }

    public int getNextID() {
        return NextID;
    }

    public void setNextID(int nextID) {
        NextID = nextID;
    }

    public String getLsID() {
        return LsID;
    }

    public void setLsID(String lsID) {
        LsID = lsID;
    }

    public String getVersetitle() {
        return versetitle;
    }

    public void setVersetitle(String versetitle) {
        this.versetitle = versetitle;
    }

    public String getCitationCount() {
        return citationCount;
    }

    public void setCitationCount(String citationCount) {
        this.citationCount = citationCount;
    }

    boolean isSelected = false;

    public String getUri() {
        return uri;
    }

    public void setUri(String uri) {
        this.uri = uri;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }

    public ArrayList<String> getFootnoteArray() {
        return footnoteArray;
    }

    public void setFootnoteArray(ArrayList<String> footnoteArray) {
        this.footnoteArray = footnoteArray;
    }

    public String getData_aid() {
        return data_aid;
    }

    public void setData_aid(String data_aid) {
        this.data_aid = data_aid;
    }

    public String getStringSentense() {
        return stringSentense;
    }

    public void setStringSentense(String stringSentense) {
        this.stringSentense = stringSentense;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }


    @Override
    public String toString() {
        return "VerseData{" +
                "uri='" + uri + '\'' +
                ", stringSentense='" + stringSentense + '\'' +
                ", id='" + id + '\'' +
                ", data_aid='" + data_aid + '\'' +
                ", footnoteArray=" + footnoteArray +
                ", citationCount='" + citationCount + '\'' +
                ", versetitle='" + versetitle + '\'' +
                ", LsID='" + LsID + '\'' +
                ", NextID=" + NextID +
                ", isSelected=" + isSelected +
                '}';
    }
}
