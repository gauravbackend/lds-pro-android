package com.ldsscriptures.pro.model;

/**
 * Created by iblinfotech on 27/07/17.
 */

public class JournalItemData {

    String serverid;
    String journaltitle;
    String journaltext;
    private String deleted;
    private String modified;
    private int id;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDeleted() {
        return deleted;
    }

    public void setDeleted(String deleted) {
        this.deleted = deleted;
    }

    public String getModified() {
        return modified;
    }

    public void setModified(String modified) {
        this.modified = modified;
    }

    public String getServerid() {
        return serverid;
    }

    public void setServerid(String serverid) {
        this.serverid = serverid;
    }

    public String getJournaltitle() {
        return journaltitle;
    }

    public void setJournaltitle(String journaltitle) {
        this.journaltitle = journaltitle;
    }

    public String getJournaltext() {
        return journaltext;
    }

    public void setJournaltext(String journaltext) {
        this.journaltext = journaltext;
    }


}
