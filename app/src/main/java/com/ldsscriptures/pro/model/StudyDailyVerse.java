package com.ldsscriptures.pro.model;

public class StudyDailyVerse {

    private String showDate;

    private String uri;

    private String verseID;

    public StudyDailyVerse() {
    }

    public StudyDailyVerse(String uri, String showDate) {
        this.uri = uri;
        this.showDate = showDate;
    }


    public String getVerseID() {
        return verseID;
    }

    public void setVerseID(String verseID) {
        this.verseID = verseID;
    }

    public String getShowDate() {
        return showDate;
    }

    public void setShowDate(String showDate) {
        this.showDate = showDate;
    }

    public String getUri() {
        return uri;
    }

    public void setUri(String uri) {
        this.uri = uri;
    }

    @Override
    public String toString() {
        return "ClassPojo [showDate = " + showDate + ", uri = " + uri + "]";
    }

}
