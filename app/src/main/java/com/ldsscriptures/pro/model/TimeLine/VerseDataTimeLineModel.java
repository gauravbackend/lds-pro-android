package com.ldsscriptures.pro.model.TimeLine;

/**
 * Created by IBL InfoTech on 9/12/2017.
 */

public class VerseDataTimeLineModel {

    String lcId;
    String LsId;
    String indexForReading;
    String versePath;
    String vereseText;
    String verseFullPath;
    String verseAid;

    public String getVerseAid() {
        return verseAid;
    }

    public void setVerseAid(String verseAid) {
        this.verseAid = verseAid;
    }

    public String getIndexForReading() {
        return indexForReading;
    }

    public void setIndexForReading(String indexForReading) {
        this.indexForReading = indexForReading;
    }

    public String getLcId() {
        return lcId;
    }

    public void setLcId(String lcId) {
        this.lcId = lcId;
    }

    public String getLsId() {
        return LsId;
    }

    public void setLsId(String lsId) {
        LsId = lsId;
    }

    public String getVerseFullPath() {
        return verseFullPath;
    }

    public void setVerseFullPath(String verseFullPath) {
        this.verseFullPath = verseFullPath;
    }

    public String getVersePath() {
        return versePath;
    }

    public void setVersePath(String versePath) {
        this.versePath = versePath;
    }

    public String getVereseText() {
        return vereseText;
    }

    public void setVereseText(String vereseText) {
        this.vereseText = vereseText;
    }


    @Override
    public String toString() {
        return "VerseDataTimeLineModel{" +
                "lcId='" + lcId + '\'' +
                ", LsId='" + LsId + '\'' +
                ", indexForReading='" + indexForReading + '\'' +
                ", versePath='" + versePath + '\'' +
                ", vereseText='" + vereseText + '\'' +
                ", verseFullPath='" + verseFullPath + '\'' +
                ", verseAid='" + verseAid + '\'' +
                '}';
    }
}
