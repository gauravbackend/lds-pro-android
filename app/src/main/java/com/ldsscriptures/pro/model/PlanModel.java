package com.ldsscriptures.pro.model;

/**
 * Created by IBL InfoTech on 7/25/2017.
 */

public class PlanModel {

    private String plan_id;
    private String plan_name;
    private String plan_Main_LCID;
    private String plan_Sub_LSID;
    private String plan_days;
    private String plan_flag;
    private String plan_reading_minute;
    private String plan_created_date;

    public String getPlan_created_date() {
        return plan_created_date;
    }

    public void setPlan_created_date(String plan_created_date) {
        this.plan_created_date = plan_created_date;
    }

    public String getPlan_reading_minute() {
        return plan_reading_minute;
    }

    public void setPlan_reading_minute(String plan_reading_minute) {
        this.plan_reading_minute = plan_reading_minute;
    }

    public String getPlan_id() {
        return plan_id;
    }

    public void setPlan_id(String plan_id) {
        this.plan_id = plan_id;
    }

    public String getPlan_name() {
        return plan_name;
    }

    public void setPlan_name(String plan_name) {
        this.plan_name = plan_name;
    }

    public String getPlan_Main_LCID() {
        return plan_Main_LCID;
    }

    public void setPlan_Main_LCID(String plan_Main_LCID) {
        this.plan_Main_LCID = plan_Main_LCID;
    }

    public String getPlan_Sub_LSID() {
        return plan_Sub_LSID;
    }

    public void setPlan_Sub_LSID(String plan_Sub_LSID) {
        this.plan_Sub_LSID = plan_Sub_LSID;
    }

    public String getPlan_days() {
        return plan_days;
    }

    public void setPlan_days(String plan_days) {
        this.plan_days = plan_days;
    }

    public String getPlan_flag() {
        return plan_flag;
    }

    public void setPlan_flag(String plan_flag) {
        this.plan_flag = plan_flag;
    }


    @Override
    public String toString() {
        return "PlanModel{" +
                "plan_id='" + plan_id + '\'' +
                ", plan_name='" + plan_name + '\'' +
                ", plan_Main_LCID='" + plan_Main_LCID + '\'' +
                ", plan_Sub_LSID='" + plan_Sub_LSID + '\'' +
                ", plan_days='" + plan_days + '\'' +
                ", plan_flag='" + plan_flag + '\'' +
                '}';
    }
}
