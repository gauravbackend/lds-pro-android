package com.ldsscriptures.pro.model;

/**
 * Created on 30/05/17 by iblinfotech.
 */

public class UserDetail {
    private String session_id;
    private String achievements;
    private String day_streak;
    private String points;
    private String plan_expire_date;
    private String img_url;
    private String fb_id;
    private String email;
    private String min_reading;
    private String confirmed;
    private String success;
    private String msg;
    private String goal_points;
    private String fb_user;
    private String username;
    private String plan_type;
    private String annotations;
    private String goal_hit;
    private String subscribed;
    private String user_id;
    private String private_status;

    public String getPrivate_status() {
        return private_status;
    }

    public void setPrivate_status(String private_status) {
        this.private_status = private_status;
    }

    public String getSession_id() {
        return session_id;
    }

    public void setSession_id(String session_id) {
        this.session_id = session_id;
    }

    public String getAchievements() {
        return achievements;
    }

    public void setAchievements(String achievements) {
        this.achievements = achievements;
    }

    public String getDay_streak() {
        return day_streak;
    }

    public void setDay_streak(String day_streak) {
        this.day_streak = day_streak;
    }

    public String getPoints() {
        return points;
    }

    public void setPoints(String points) {
        this.points = points;
    }

    public String getPlan_expire_date() {
        return plan_expire_date;
    }

    public void setPlan_expire_date(String plan_expire_date) {
        this.plan_expire_date = plan_expire_date;
    }

    public String getImg_url() {
        return img_url;
    }

    public void setImg_url(String img_url) {
        this.img_url = img_url;
    }

    public String getFb_id() {
        return fb_id;
    }

    public void setFb_id(String fb_id) {
        this.fb_id = fb_id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getMin_reading() {
        return min_reading;
    }

    public void setMin_reading(String min_reading) {
        this.min_reading = min_reading;
    }

    public String getConfirmed() {
        return confirmed;
    }

    public void setConfirmed(String confirmed) {
        this.confirmed = confirmed;
    }

    public String getSuccess() {
        return success;
    }

    public void setSuccess(String success) {
        this.success = success;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getGoal_points() {
        return goal_points;
    }

    public void setGoal_points(String goal_points) {
        this.goal_points = goal_points;
    }

    public String getFb_user() {
        return fb_user;
    }

    public void setFb_user(String fb_user) {
        this.fb_user = fb_user;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getAnnotations() {
        return annotations;
    }

    public void setAnnotations(String annotations) {
        this.annotations = annotations;
    }

    public String getGoal_hit() {
        return goal_hit;
    }

    public void setGoal_hit(String goal_hit) {
        this.goal_hit = goal_hit;
    }

    public String getSubscribed() {
        return subscribed;
    }

    public void setSubscribed(String subscribed) {
        this.subscribed = subscribed;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getPlan_type() {
        return plan_type;
    }

    public void setPlan_type(String plan_type) {
        this.plan_type = plan_type;
    }

    @Override
    public String toString() {
        return "UserDetail{" +
                "goal_points='" + goal_points + '\'' +
                '}';
    }
}
