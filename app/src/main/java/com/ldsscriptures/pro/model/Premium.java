package com.ldsscriptures.pro.model;

/**
 * Created on 17/08/17 by iblinfotech.
 */

public class Premium {

    private String title, subTitle;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSubTitle() {
        return subTitle;
    }

    public void setSubTitle(String subTitle) {
        this.subTitle = subTitle;
    }
}
