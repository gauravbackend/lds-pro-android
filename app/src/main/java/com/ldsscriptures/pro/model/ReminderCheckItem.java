package com.ldsscriptures.pro.model;

/**
 * Created on 31/05/17 by iblinfotech.
 */

public class ReminderCheckItem {
    private String planid;
    private boolean planFlag;
    private String planDate;

    public ReminderCheckItem() {
    }

    public ReminderCheckItem(String planid, boolean planFlag, String planDate) {
        this.planid = planid;
        this.planFlag = planFlag;
        this.planDate = planDate;
    }

    public String getPlanDate() {
        return planDate;
    }

    public void setPlanDate(String planDate) {
        this.planDate = planDate;
    }

    public String getPlanid() {
        return planid;
    }

    public void setPlanid(String planid) {
        this.planid = planid;
    }

    public boolean isPlanFlag() {
        return planFlag;
    }

    public void setPlanFlag(boolean planFlag) {
        this.planFlag = planFlag;
    }
}
