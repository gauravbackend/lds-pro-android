package com.ldsscriptures.pro.model;

public class MoreOptionMenuData {

    public MoreOptionMenuData() {
    }

    // lib selection parameter
    String lcTitle;
    String lcID;

    public MoreOptionMenuData(String lcID, String lcTitle) {
        this.lcTitle = lcTitle;
        this.lcID = lcID;
    }

    // lib index parameter
    //    String lcID;
    String lsId;
    String lsTitle;

    public MoreOptionMenuData(String lcID, String lsId, String lsTitle) {
        this.lcID = lcID;
        this.lsId = lsId;
        this.lsTitle = lsTitle;
    }

    // lib sub index parameter
    int libSubPosition;
    String libIndexTitle;


    public MoreOptionMenuData(String lcID, String lsId, int libSubPosition, String libIndexTitle) {
        this.lcID = lcID;
        this.lsId = lsId;
        this.libSubPosition = libSubPosition;
        this.libIndexTitle = libIndexTitle;
    }

    // lib reading parameter
    int indexForReading;
    boolean isHistory;
    boolean isFromIndex;
    int postion;

    public MoreOptionMenuData(String lcID, String lsId, int indexForReading, boolean isHistory, boolean isFromIndex, int pos) {
        this.lcID = lcID;
        this.lsId = lsId;
        this.indexForReading = indexForReading;
        this.isHistory = isHistory;
        this.isFromIndex = isFromIndex;
        this.postion = pos;
    }


    public int getPostion() {
        return postion;
    }

    public void setPostion(int postion) {
        this.postion = postion;
    }

    public String getLsId() {
        return lsId;
    }

    public void setLsId(String lsId) {
        this.lsId = lsId;
    }

    public String getLcTitle() {
        return lcTitle;
    }

    public void setLcTitle(String lcTitle) {
        this.lcTitle = lcTitle;
    }

    public String getLcID() {
        return lcID;
    }

    public void setLcID(String lcID) {
        this.lcID = lcID;
    }

    public String getLsTitle() {
        return lsTitle;
    }

    public void setLsTitle(String lsTitle) {
        this.lsTitle = lsTitle;
    }

    public int getLibSubPosition() {
        return libSubPosition;
    }

    public void setLibSubPosition(int libSubPosition) {
        this.libSubPosition = libSubPosition;
    }

    public String getLibIndexTitle() {
        return libIndexTitle;
    }

    public void setLibIndexTitle(String libIndexTitle) {
        this.libIndexTitle = libIndexTitle;
    }

    public int getIndexForReading() {
        return indexForReading;
    }

    public void setIndexForReading(int indexForReading) {
        this.indexForReading = indexForReading;
    }

    public boolean isHistory() {
        return isHistory;
    }

    public void setHistory(boolean history) {
        isHistory = history;
    }

    public boolean isFromIndex() {
        return isFromIndex;
    }

    public void setFromIndex(boolean fromIndex) {
        isFromIndex = fromIndex;
    }

}
