package com.ldsscriptures.pro.model;

/**
 * Created on 08/08/17 by iblinfotech.
 */

public class CitationList {

    private String id, name, cnt, ref_uri, source_group,citation_count;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCnt() {
        return cnt;
    }

    public void setCnt(String cnt) {
        this.cnt = cnt;
    }

    public String getRef_uri() {
        return ref_uri;
    }

    public void setRef_uri(String ref_uri) {
        this.ref_uri = ref_uri;
    }

    public String getSource_group() {
        return source_group;
    }

    public void setSource_group(String source_group) {
        this.source_group = source_group;
    }

    public String getCitation_count() {
        return citation_count;
    }

    public void setCitation_count(String citation_count) {
        this.citation_count = citation_count;
    }
}
