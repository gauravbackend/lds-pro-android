package com.ldsscriptures.pro.model;

public class SubMenu {
    public int submenuIcon;
    public String submenuTitle;

    public SubMenu(int imageResource, String modelName) {
        this.submenuIcon = imageResource;
        this.submenuTitle = modelName;
    }

}
