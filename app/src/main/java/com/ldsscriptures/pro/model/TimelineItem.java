package com.ldsscriptures.pro.model;

/**
 * Created by IBL InfoTech on 8/5/2017.
 */

public class TimelineItem
{
    private String for_uid;

    private String id;

    private String uid;

    private String share_level;

    private String created;

    private String likes;

    private String uri;

    private String note;

    private String comments;

    private  String contact_image_url;

    private  String timeline_user_name;

    private  String timeline_user_time_ago;

    private  String timeline_nephi_time;

    private  String timeline_nephi_content;

    private  String timeline_content;

    private  String txt_fav_timeline;

    private  String txt_comment_timeline;

    public String getContact_image_url() {
        return contact_image_url;
    }

    public void setContact_image_url(String contact_image_url) {
        this.contact_image_url = contact_image_url;
    }

    public String getTimeline_user_name() {
        return timeline_user_name;
    }

    public void setTimeline_user_name(String timeline_user_name) {
        this.timeline_user_name = timeline_user_name;
    }

    public String getTimeline_user_time_ago() {
        return timeline_user_time_ago;
    }

    public void setTimeline_user_time_ago(String timeline_user_time_ago) {
        this.timeline_user_time_ago = timeline_user_time_ago;
    }

    public String getTimeline_nephi_time() {
        return timeline_nephi_time;
    }

    public void setTimeline_nephi_time(String timeline_nephi_time) {
        this.timeline_nephi_time = timeline_nephi_time;
    }

    public String getTimeline_nephi_content() {
        return timeline_nephi_content;
    }

    public void setTimeline_nephi_content(String timeline_nephi_content) {
        this.timeline_nephi_content = timeline_nephi_content;
    }

    public String getTimeline_content() {
        return timeline_content;
    }

    public void setTimeline_content(String timeline_content) {
        this.timeline_content = timeline_content;
    }

    public String getTxt_fav_timeline() {
        return txt_fav_timeline;
    }

    public void setTxt_fav_timeline(String txt_fav_timeline) {
        this.txt_fav_timeline = txt_fav_timeline;
    }

    public String getTxt_comment_timeline() {
        return txt_comment_timeline;
    }

    public void setTxt_comment_timeline(String txt_comment_timeline) {
        this.txt_comment_timeline = txt_comment_timeline;
    }

    public String getFor_uid() {
        return for_uid;
    }

    public void setFor_uid(String for_uid) {
        this.for_uid = for_uid;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getShare_level() {
        return share_level;
    }

    public void setShare_level(String share_level) {
        this.share_level = share_level;
    }

    public String getCreated() {
        return created;
    }

    public void setCreated(String created) {
        this.created = created;
    }

    public String getLikes() {
        return likes;
    }

    public void setLikes(String likes) {
        this.likes = likes;
    }

    public String getUri() {
        return uri;
    }

    public void setUri(String uri) {
        this.uri = uri;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    @Override
    public String toString() {
        return "ClassPojo [for_uid = " + for_uid + ", id = " + id + ", uid = " + uid + ", share_level = " + share_level + ", created = " + created + ", likes = " + likes + ", uri = " + uri + ", note = " + note + ", comments = " + comments + "]";
    }
}
