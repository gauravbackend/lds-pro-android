package com.ldsscriptures.pro.model;

/**
 * Created by iblinfotech on 01/09/17.
 */

public class PurchaseData {

    private String plan_expire_date;

    private String packageName;

    private String user_id;

    private String purchase_date;

    private String msg;

    private String success;

    private String subscriptionId;

    private String plan_type;

    public String getPlan_expire_date() {
        return plan_expire_date;
    }

    public void setPlan_expire_date(String plan_expire_date) {
        this.plan_expire_date = plan_expire_date;
    }

    public String getPackageName() {
        return packageName;
    }

    public void setPackageName(String packageName) {
        this.packageName = packageName;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getPurchase_date() {
        return purchase_date;
    }

    public void setPurchase_date(String purchase_date) {
        this.purchase_date = purchase_date;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getSuccess() {
        return success;
    }

    public void setSuccess(String success) {
        this.success = success;
    }

    public String getSubscriptionId() {
        return subscriptionId;
    }

    public void setSubscriptionId(String subscriptionId) {
        this.subscriptionId = subscriptionId;
    }

    public String getPlan_type() {
        return plan_type;
    }

    public void setPlan_type(String plan_type) {
        this.plan_type = plan_type;
    }
}
