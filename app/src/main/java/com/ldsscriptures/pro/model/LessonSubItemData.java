package com.ldsscriptures.pro.model;

/**
 * Created by iblinfotech on 26/07/17.
 */

public class LessonSubItemData {

    int _id;
    String itemtype;
    String serverid;
    String lessonid;
    String notetext;
    int lessonType;
    String verseID;
    String verseString;
    String verseAid;
    String urltitle;
    String url;
    String verseTitle;
    private String deleted;
    private String modified;
    String sort_order;
    String uri;

    public String getUri() {
        return uri;
    }

    public void setUri(String uri) {
        this.uri = uri;
    }

    public String getSort_order() {
        return sort_order;
    }

    public void setSort_order(String sort_order) {
        this.sort_order = sort_order;
    }

    public String getDeleted() {
        return deleted;
    }

    public void setDeleted(String deleted) {
        this.deleted = deleted;
    }

    public String getModified() {
        return modified;
    }

    public void setModified(String modified) {
        this.modified = modified;
    }

    public String getVerseTitle() {
        return verseTitle;
    }

    public void setVerseTitle(String verseTitle) {
        this.verseTitle = verseTitle;
    }

    public String getVerseID() {
        return verseID;
    }

    public void setVerseID(String verseID) {
        this.verseID = verseID;
    }

    public String getVerseString() {
        return verseString;
    }

    public void setVerseString(String verseString) {
        this.verseString = verseString;
    }

    public String getVerseAid() {
        return verseAid;
    }

    public void setVerseAid(String verseAid) {
        this.verseAid = verseAid;
    }


    public int getLessonType() {
        return lessonType;
    }

    public void setLessonType(int lessonType) {
        this.lessonType = lessonType;
    }

    public int get_id() {
        return _id;
    }

    public void set_id(int _id) {
        this._id = _id;
    }

    public String getItemtype() {
        return itemtype;
    }

    public void setItemtype(String itemtype) {
        this.itemtype = itemtype;
    }

    public String getServerid() {
        return serverid;
    }

    public void setServerid(String serverid) {
        this.serverid = serverid;
    }

    public String getLessonid() {
        return lessonid;
    }

    public void setLessonid(String lessonid) {
        this.lessonid = lessonid;
    }

    public String getNotetext() {
        return notetext;
    }

    public void setNotetext(String notetext) {
        this.notetext = notetext;
    }

    public String getUrltitle() {
        return urltitle;
    }

    public void setUrltitle(String urltitle) {
        this.urltitle = urltitle;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    @Override
    public String toString() {
        return "LessonSubItemData{" +
                "_id=" + _id +
                ", itemtype='" + itemtype + '\'' +
                ", serverid='" + serverid + '\'' +
                ", lessonid='" + lessonid + '\'' +
                ", notetext='" + notetext + '\'' +
                ", lessonType=" + lessonType +
                ", verseID='" + verseID + '\'' +
                ", verseString='" + verseString + '\'' +
                ", verseAid='" + verseAid + '\'' +
                ", urltitle='" + urltitle + '\'' +
                ", url='" + url + '\'' +
                ", verseTitle='" + verseTitle + '\'' +
                ", deleted='" + deleted + '\'' +
                ", modified='" + modified + '\'' +
                ", sort_order='" + sort_order + '\'' +
                '}';
    }
}
