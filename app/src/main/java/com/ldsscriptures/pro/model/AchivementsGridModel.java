package com.ldsscriptures.pro.model;

/**
 * Created by IBL InfoTech on 7/25/2017.
 */

public class AchivementsGridModel {
    private String achivements_id;
    private String achivements_title;
    private Integer achivements_image;

    public AchivementsGridModel() {
    }

    public AchivementsGridModel(String achivements_id, String achivements_title, Integer achivements_image) {
        this.achivements_id = achivements_id;
        this.achivements_title = achivements_title;
        this.achivements_image = achivements_image;
    }

    public String getAchivements_id() {
        return achivements_id;
    }

    public void setAchivements_id(String achivements_id) {
        this.achivements_id = achivements_id;
    }

    public Integer getAchivements_image() {
        return achivements_image;
    }

    public void setAchivements_image(Integer achivements_image) {
        this.achivements_image = achivements_image;
    }

    public String getAchivements_title() {
        return achivements_title;
    }

    public void setAchivements_title(String achivements_title) {
        this.achivements_title = achivements_title;
    }


}
