package com.ldsscriptures.pro.model;

/**
 * Created by IBL InfoTech on 7/28/2017.
 */

public class EmailFriendData {

    private Boolean friend_flag;

    private String friend_fb_id;

    private String friend_img_url;

    private String following;

    private String status;

    private String followed_by;

    private String friend_fb_user;

    private String friend_user_id;

    private String friend_name;

    private String friend_level;

    private String friend_username;

    private String friend_email;

    private Boolean friend_invite_flag;

    public Boolean getFriend_invite_flag() {
        return friend_invite_flag;
    }

    public void setFriend_invite_flag(Boolean friend_invite_flag) {
        this.friend_invite_flag = friend_invite_flag;
    }

    public Boolean getFriend_flag() {
        return friend_flag;
    }

    public void setFriend_flag(Boolean friend_flag) {
        this.friend_flag = friend_flag;
    }

    public String getFriend_fb_id() {
        return friend_fb_id;
    }

    public void setFriend_fb_id(String friend_fb_id) {
        this.friend_fb_id = friend_fb_id;
    }

    public String getFriend_img_url() {
        return friend_img_url;
    }

    public void setFriend_img_url(String friend_img_url) {
        this.friend_img_url = friend_img_url;
    }

    public String getFollowing() {
        return following;
    }

    public void setFollowing(String following) {
        this.following = following;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getFollowed_by() {
        return followed_by;
    }

    public void setFollowed_by(String followed_by) {
        this.followed_by = followed_by;
    }

    public String getFriend_fb_user() {
        return friend_fb_user;
    }

    public void setFriend_fb_user(String friend_fb_user) {
        this.friend_fb_user = friend_fb_user;
    }

    public String getFriend_user_id() {
        return friend_user_id;
    }

    public void setFriend_user_id(String friend_user_id) {
        this.friend_user_id = friend_user_id;
    }

    public String getFriend_name() {
        return friend_name;
    }

    public void setFriend_name(String friend_name) {
        this.friend_name = friend_name;
    }

    public String getFriend_level() {
        return friend_level;
    }

    public void setFriend_level(String friend_level) {
        this.friend_level = friend_level;
    }

    public String getFriend_username() {
        return friend_username;
    }

    public void setFriend_username(String friend_username) {
        this.friend_username = friend_username;
    }

    public String getFriend_email() {
        return friend_email;
    }

    public void setFriend_email(String friend_email) {
        this.friend_email = friend_email;
    }

    @Override
    public String toString() {
        return "ClassPojo [friend_fb_id = " + friend_fb_id + ", friend_img_url = " + friend_img_url + ", following = " + following + ", status = " + status + ", followed_by = " + followed_by + ", friend_fb_user = " + friend_fb_user + ", friend_user_id = " + friend_user_id + ", friend_name = " + friend_name + ", friend_level = " + friend_level + ", friend_username = " + friend_username + ", friend_email = " + friend_email + "]";
    }
}
