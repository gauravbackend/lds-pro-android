package com.ldsscriptures.pro.model;


public class ReadingAudioData {
    String id;
    String subitem_id;
    String media_url;
    String file_size;
    String duration;
    String realted_audio_voice_id;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getSubitem_id() {
        return subitem_id;
    }

    public void setSubitem_id(String subitem_id) {
        this.subitem_id = subitem_id;
    }

    public String getMedia_url() {
        return media_url;
    }

    public void setMedia_url(String media_url) {
        this.media_url = media_url;
    }

    public String getFile_size() {
        return file_size;
    }

    public void setFile_size(String file_size) {
        this.file_size = file_size;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public String getRealted_audio_voice_id() {
        return realted_audio_voice_id;
    }

    public void setRealted_audio_voice_id(String realted_audio_voice_id) {
        this.realted_audio_voice_id = realted_audio_voice_id;
    }
}
