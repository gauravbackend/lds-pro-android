package com.ldsscriptures.pro.model;


public class LsTitleIndexData {

    int id;
    int nav_section_id;
    int position;
    String image_rendition;
    String title_html;
    String subtitle;
    String preview;
    int subitem_id;
    String uri;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getNav_section_id() {
        return nav_section_id;
    }

    public void setNav_section_id(int nav_section_id) {
        this.nav_section_id = nav_section_id;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public String getImage_rendition() {
        return image_rendition;
    }

    public void setImage_rendition(String image_rendition) {
        this.image_rendition = image_rendition;
    }

    public String getTitle_html() {
        return title_html;
    }

    public void setTitle_html(String title_html) {
        this.title_html = title_html;
    }

    public String getSubtitle() {
        return subtitle;
    }

    public void setSubtitle(String subtitle) {
        this.subtitle = subtitle;
    }

    public String getPreview() {
        return preview;
    }

    public void setPreview(String preview) {
        this.preview = preview;
    }

    public int getSubitem_id() {
        return subitem_id;
    }

    public void setSubitem_id(int subitem_id) {
        this.subitem_id = subitem_id;
    }

    public String getUri() {
        return uri;
    }

    public void setUri(String uri) {
        this.uri = uri;
    }
}
