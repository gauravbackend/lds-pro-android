package com.ldsscriptures.pro.model;


import java.util.ArrayList;

public class LsFullIndexData {

    int id;
    String nav_collection_id;
    int position;
    String title;
    String indent_level;
    ArrayList<LsMainIndexData> itemMainIndexArray = new ArrayList<>();
    ArrayList<LsTitleIndexData> titleIndexDatas = new ArrayList<>();

    public ArrayList<LsTitleIndexData> getTitleIndexDatas() {
        return titleIndexDatas;
    }

    public void setTitleIndexDatas(ArrayList<LsTitleIndexData> titleIndexDatas) {
        this.titleIndexDatas = titleIndexDatas;
    }

    public ArrayList<LsMainIndexData> getItemMainIndexArray() {
        return itemMainIndexArray;
    }

    public void setItemMainIndexArray(ArrayList<LsMainIndexData> itemMainIndexArray) {
        this.itemMainIndexArray = itemMainIndexArray;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNav_collection_id() {
        return nav_collection_id;
    }

    public void setNav_collection_id(String nav_collection_id) {
        this.nav_collection_id = nav_collection_id;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getIndent_level() {
        return indent_level;
    }

    public void setIndent_level(String indent_level) {
        this.indent_level = indent_level;
    }

    @Override
    public String toString() {
        return "LsFullIndexData{" +
                "itemMainIndexArray=" + itemMainIndexArray +
                '}';
    }
}
