package com.ldsscriptures.pro.model;


public class LsIndex {

    private int position;
    private int id;
    private int nav_section_id;
    private String image_rendation;
    private String title_html;
    private String subtitle;
    private String uri;

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getNav_section_id() {
        return nav_section_id;
    }

    public void setNav_section_id(int nav_section_id) {
        this.nav_section_id = nav_section_id;
    }

    public String getImage_rendation() {
        return image_rendation;
    }

    public void setImage_rendation(String image_rendation) {
        this.image_rendation = image_rendation;
    }

    public String getTitle_html() {
        return title_html;
    }

    public void setTitle_html(String title_html) {
        this.title_html = title_html;
    }

    public String getSubtitle() {
        return subtitle;
    }

    public void setSubtitle(String subtitle) {
        this.subtitle = subtitle;
    }

    public String getUri() {
        return uri;
    }

    public void setUri(String uri) {
        this.uri = uri;
    }
}
