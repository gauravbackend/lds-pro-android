package com.ldsscriptures.pro.model;

import java.util.ArrayList;

/**
 * Created by iblinfotech on 27/07/17.
 */

public class JournalData {

    String groupingdate;
    ArrayList<JournalItemData> journalItemDataArrayList = new ArrayList<>();

    public String getGroupingdate() {
        return groupingdate;
    }

    public void setGroupingdate(String groupingdate) {
        this.groupingdate = groupingdate;
    }

    public ArrayList<JournalItemData> getJournalItemDataArrayList() {
        return journalItemDataArrayList;
    }

    public void setJournalItemDataArrayList(ArrayList<JournalItemData> journalItemDataArrayList) {
        this.journalItemDataArrayList = journalItemDataArrayList;
    }
}
