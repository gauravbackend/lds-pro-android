package com.ldsscriptures.pro.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by IBL InfoTech on 8/2/2017.
 */

public class FollowersMainData
{
    private String following_img_url;

    private String following_fb_id;

    private String following_back;

    private String following_user_id;

    private String following_points_week;

    private String following_points_year;

    private String following_fb_user;

    private String following_points;

    private String following_level;

    private String following_username;

    private String following_points_month;

    private String following_name;

    private String following_email;

    public String getFollowing_img_url ()
    {
        return following_img_url;
    }

    public void setFollowing_img_url (String following_img_url)
    {
        this.following_img_url = following_img_url;
    }

    public String getFollowing_fb_id ()
{
    return following_fb_id;
}

    public void setFollowing_fb_id (String following_fb_id)
    {
        this.following_fb_id = following_fb_id;
    }

    public String getFollowing_back ()
    {
        return following_back;
    }

    public void setFollowing_back (String following_back)
    {
        this.following_back = following_back;
    }

    public String getFollowing_user_id ()
    {
        return following_user_id;
    }

    public void setFollowing_user_id (String following_user_id)
    {
        this.following_user_id = following_user_id;
    }

    public String getFollowing_points_week ()
    {
        return following_points_week;
    }

    public void setFollowing_points_week (String following_points_week)
    {
        this.following_points_week = following_points_week;
    }

    public String getFollowing_points_year ()
    {
        return following_points_year;
    }

    public void setFollowing_points_year (String following_points_year)
    {
        this.following_points_year = following_points_year;
    }

    public String getFollowing_fb_user ()
{
    return following_fb_user;
}

    public void setFollowing_fb_user (String following_fb_user)
    {
        this.following_fb_user = following_fb_user;
    }

    public String getFollowing_points ()
    {
        return following_points;
    }

    public void setFollowing_points (String following_points)
    {
        this.following_points = following_points;
    }

    public String getFollowing_level ()
    {
        return following_level;
    }

    public void setFollowing_level (String following_level)
    {
        this.following_level = following_level;
    }

    public String getFollowing_username ()
    {
        return following_username;
    }

    public void setFollowing_username (String following_username)
    {
        this.following_username = following_username;
    }

    public String getFollowing_points_month ()
    {
        return following_points_month;
    }

    public void setFollowing_points_month (String following_points_month)
    {
        this.following_points_month = following_points_month;
    }

    public String getFollowing_name ()
    {
        return following_name;
    }

    public void setFollowing_name (String following_name)
    {
        this.following_name = following_name;
    }

    public String getFollowing_email ()
    {
        return following_email;
    }

    public void setFollowing_email (String following_email)
    {
        this.following_email = following_email;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [following_img_url = "+following_img_url+", following_fb_id = "+following_fb_id+", following_back = "+following_back+", following_user_id = "+following_user_id+", following_points_week = "+following_points_week+", following_points_year = "+following_points_year+", following_fb_user = "+following_fb_user+", following_points = "+following_points+", following_level = "+following_level+", following_username = "+following_username+", following_points_month = "+following_points_month+", following_name = "+following_name+", following_email = "+following_email+"]";
    }


    public FollowersMainData(String following_img_url) {
        this.following_img_url = following_img_url;
    }
}
