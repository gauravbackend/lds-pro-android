package com.ldsscriptures.pro.model;

/**
 * Created by iblinfotech on 09/08/17.
 */

public class GetVerseDataFromUri {
    String verseId;
    String verseText;

    public String getVerseId() {
        return verseId;
    }

    public void setVerseId(String verseId) {
        this.verseId = verseId;
    }

    public String getVerseText() {
        return verseText;
    }

    public void setVerseText(String verseText) {
        this.verseText = verseText;
    }
}
