package com.ldsscriptures.pro.model;


import java.io.Serializable;
import java.util.ArrayList;

public class NavItemData implements Serializable{

    private int id;
    private String uri;
    private String title;
    private ArrayList<SerachData> verseTextArraylist;

    public ArrayList<SerachData> getVerseTextArraylist() {
        return verseTextArraylist;
    }

    public void setVerseTextArraylist(ArrayList<SerachData> verseTextArraylist) {
        this.verseTextArraylist = verseTextArraylist;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUri() {
        return uri;
    }

    public void setUri(String uri) {
        this.uri = uri;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
