package com.ldsscriptures.pro.model;

import java.io.Serializable;

/**
 * Created on 11/08/17 by iblinfotech.
 */

public class GlobalSearchVerseModel implements Serializable{

    private String nav_col_id,nav_col_sel_id,nav_col_position,nav_col_title,nav_col_uri;
    private String nav_sel_id,nav_sel_col_id,nav_sel_position,nav_sel_indent_level,nav_sel_title;
    private String nav_item_id,nav_item_sec_id,nav_item_position,nav_item_title,nav_item_uri;
    private String subitem_docid,subitem_c0subitem_id,subitem_c1content_html;

    public String getNav_col_id() {
        return nav_col_id;
    }

    public void setNav_col_id(String nav_col_id) {
        this.nav_col_id = nav_col_id;
    }

    public String getNav_col_sel_id() {
        return nav_col_sel_id;
    }

    public void setNav_col_sel_id(String nav_col_sel_id) {
        this.nav_col_sel_id = nav_col_sel_id;
    }

    public String getNav_col_position() {
        return nav_col_position;
    }

    public void setNav_col_position(String nav_col_position) {
        this.nav_col_position = nav_col_position;
    }

    public String getNav_col_title() {
        return nav_col_title;
    }

    public void setNav_col_title(String nav_col_title) {
        this.nav_col_title = nav_col_title;
    }

    public String getNav_col_uri() {
        return nav_col_uri;
    }

    public void setNav_col_uri(String nav_col_uri) {
        this.nav_col_uri = nav_col_uri;
    }

    public String getNav_sel_id() {
        return nav_sel_id;
    }

    public void setNav_sel_id(String nav_sel_id) {
        this.nav_sel_id = nav_sel_id;
    }

    public String getNav_sel_col_id() {
        return nav_sel_col_id;
    }

    public void setNav_sel_col_id(String nav_sel_col_id) {
        this.nav_sel_col_id = nav_sel_col_id;
    }

    public String getNav_sel_position() {
        return nav_sel_position;
    }

    public void setNav_sel_position(String nav_sel_position) {
        this.nav_sel_position = nav_sel_position;
    }

    public String getNav_sel_indent_level() {
        return nav_sel_indent_level;
    }

    public void setNav_sel_indent_level(String nav_sel_indent_level) {
        this.nav_sel_indent_level = nav_sel_indent_level;
    }

    public String getNav_sel_title() {
        return nav_sel_title;
    }

    public void setNav_sel_title(String nav_sel_title) {
        this.nav_sel_title = nav_sel_title;
    }

    public String getNav_item_id() {
        return nav_item_id;
    }

    public void setNav_item_id(String nav_item_id) {
        this.nav_item_id = nav_item_id;
    }

    public String getNav_item_sec_id() {
        return nav_item_sec_id;
    }

    public void setNav_item_sec_id(String nav_item_sec_id) {
        this.nav_item_sec_id = nav_item_sec_id;
    }

    public String getNav_item_position() {
        return nav_item_position;
    }

    public void setNav_item_position(String nav_item_position) {
        this.nav_item_position = nav_item_position;
    }

    public String getNav_item_title() {
        return nav_item_title;
    }

    public void setNav_item_title(String nav_item_title) {
        this.nav_item_title = nav_item_title;
    }

    public String getNav_item_uri() {
        return nav_item_uri;
    }

    public void setNav_item_uri(String nav_item_uri) {
        this.nav_item_uri = nav_item_uri;
    }

    public String getSubitem_docid() {
        return subitem_docid;
    }

    public void setSubitem_docid(String subitem_docid) {
        this.subitem_docid = subitem_docid;
    }

    public String getSubitem_c0subitem_id() {
        return subitem_c0subitem_id;
    }

    public void setSubitem_c0subitem_id(String subitem_c0subitem_id) {
        this.subitem_c0subitem_id = subitem_c0subitem_id;
    }

    public String getSubitem_c1content_html() {
        return subitem_c1content_html;
    }

    public void setSubitem_c1content_html(String subitem_c1content_html) {
        this.subitem_c1content_html = subitem_c1content_html;
    }
}
