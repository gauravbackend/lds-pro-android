package com.ldsscriptures.pro.model;

import java.util.ArrayList;

/**
 * Created by IBL InfoTech on 8/5/2017.
 */

public class TimelineData
{
    private String sync_again_date;
    private String sync_again;
    private String user_id;

    private ArrayList<TimelineItem> items;

    public String getSync_again_date ()
    {
        return sync_again_date;
    }

    public void setSync_again_date (String sync_again_date)
    {
        this.sync_again_date = sync_again_date;
    }

    public ArrayList<TimelineItem> getItems ()
    {
        return items;
    }

    public void setItems (ArrayList<TimelineItem> items)
    {
        this.items = items;
    }

    public String getSync_again ()
    {
        return sync_again;
    }

    public void setSync_again (String sync_again)
    {
        this.sync_again = sync_again;
    }

    public String getUser_id ()
    {
        return user_id;
    }

    public void setUser_id (String user_id)
    {
        this.user_id = user_id;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [sync_again_date = "+sync_again_date+", items = "+items+", sync_again = "+sync_again+", user_id = "+user_id+"]";
    }
}
