package com.ldsscriptures.pro.model.TimeLine;

/**
 * Created by IBL InfoTech on 9/4/2017.
 */

public class LikesTimeLineModel {
    private String id;

    private String img_url;

    private String uid;

    private String username;

    private String created;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getImg_url() {
        return img_url;
    }

    public void setImg_url(String img_url) {
        this.img_url = img_url;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getCreated() {
        return created;
    }

    public void setCreated(String created) {
        this.created = created;
    }

    @Override
    public String toString() {
        return "ClassPojo [id = " + id + ", img_url = " + img_url + ", uid = " + uid + ", username = " + username + ", created = " + created + "]";
    }

    public LikesTimeLineModel(String uid) {
        this.uid = uid;
    }

    public LikesTimeLineModel() {

    }
}

