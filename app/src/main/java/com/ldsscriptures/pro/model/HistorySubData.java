package com.ldsscriptures.pro.model;

import com.ldsscriptures.pro.R;

public class HistorySubData {
    String title;
    String date;
    String tag;
    int readingIndex;
    boolean isFromIndex;
    String lcId;
    String lsId;
    String itemURI;
    String verseID;


    public String getVerseID() {
        return verseID;
    }

    public void setVerseID(String verseID) {
        this.verseID = verseID;
    }

    public String getItemURI() {
        return itemURI;
    }

    public void setItemURI(String itemURI) {
        this.itemURI = itemURI;
    }

    public boolean isFromHistory() {
        return isFromHistory;
    }

    public void setFromHistory(boolean fromHistory) {
        isFromHistory = fromHistory;
    }

    boolean isFromHistory;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public int getReadingIndex() {
        return readingIndex;
    }

    public void setReadingIndex(int readingIndex) {
        this.readingIndex = readingIndex;
    }

    public boolean isFromIndex() {
        return isFromIndex;
    }

    public void setFromIndex(boolean fromIndex) {
        isFromIndex = fromIndex;
    }

    public String getLcId() {
        return lcId;
    }

    public void setLcId(String lcId) {
        this.lcId = lcId;
    }

    public String getLsId() {
        return lsId;
    }

    public void setLsId(String lsId) {
        this.lsId = lsId;
    }


    @Override
    public String toString() {
        return "HistorySubData{" +
                "itemURI='" + itemURI + '\'' +
                '}';
    }
}
