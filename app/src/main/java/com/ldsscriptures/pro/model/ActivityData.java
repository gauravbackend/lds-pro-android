package com.ldsscriptures.pro.model;

public class ActivityData {
    int activity_id;
    String activity_date;
    int activity_hour;
    int activity_dayOfWeek;
    int activity_dayOfMonth;
    int activity_dayOfYear;
    int activity_DayMonth;
    int activity_goal_point;
    String WeekStart;
    String WeekEnd;
    String monthname;


    public String getMonthname() {
        return monthname;
    }

    public void setMonthname(String monthname) {
        this.monthname = monthname;
    }

    public String getWeekStart() {
        return WeekStart;
    }

    public void setWeekStart(String weekStart) {
        WeekStart = weekStart;
    }

    public String getWeekEnd() {
        return WeekEnd;
    }

    public void setWeekEnd(String weekEnd) {
        WeekEnd = weekEnd;
    }

    public int getActivity_id() {
        return activity_id;
    }

    public void setActivity_id(int activity_id) {
        this.activity_id = activity_id;
    }

    public String getActivity_date() {
        return activity_date;
    }

    public void setActivity_date(String activity_date) {
        this.activity_date = activity_date;
    }

    public int getActivity_hour() {
        return activity_hour;
    }

    public void setActivity_hour(int activity_hour) {
        this.activity_hour = activity_hour;
    }

    public int getActivity_dayOfWeek() {
        return activity_dayOfWeek;
    }

    public void setActivity_dayOfWeek(int activity_dayOfWeek) {
        this.activity_dayOfWeek = activity_dayOfWeek;
    }

    public int getActivity_goal_point() {
        return activity_goal_point;
    }

    public void setActivity_goal_point(int activity_goal_point) {
        this.activity_goal_point = activity_goal_point;
    }

    public int getActivity_dayOfMonth() {
        return activity_dayOfMonth;
    }

    public void setActivity_dayOfMonth(int activity_dayOfMonth) {
        this.activity_dayOfMonth = activity_dayOfMonth;
    }

    public int getActivity_dayOfYear() {
        return activity_dayOfYear;
    }

    public void setActivity_dayOfYear(int activity_dayOfYear) {
        this.activity_dayOfYear = activity_dayOfYear;
    }

    public int getActivity_DayMonth() {
        return activity_DayMonth;
    }

    public void setActivity_DayMonth(int activity_DayMonth) {
        this.activity_DayMonth = activity_DayMonth;
    }

    @Override
    public String toString() {
        return "ActivityData{" +
                "activity_hour=" + activity_hour +
                ", activity_goal_point=" + activity_goal_point +
                '}';
    }
}
