package com.ldsscriptures.pro.model.ReadingNow;

/**
 * Created by iblinfotech on 01/08/17.
 */

public class LibSubIndex {
    String tag;
    String title;
    int index;

    public String getLcId() {
        return lcId;
    }

    public void setLcId(String lcId) {
        this.lcId = lcId;
    }

    public String getLsId() {
        return lsId;
    }

    public void setLsId(String lsId) {
        this.lsId = lsId;
    }

    String lcId;
    String lsId;

    public LibSubIndex(String tag, String title, int index,String lcId,String lsId) {
        this.tag = tag;
        this.title = title;
        this.index = index;
        this.lcId = lcId;
        this.lsId = lsId;
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }
}
