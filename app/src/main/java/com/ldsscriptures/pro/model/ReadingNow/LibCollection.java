package com.ldsscriptures.pro.model.ReadingNow;

public class LibCollection {
    String tag;

    public LibCollection(String tag) {
        this.tag = tag;
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

}
