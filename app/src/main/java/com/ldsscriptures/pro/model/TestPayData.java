package com.ldsscriptures.pro.model;

/**
 * Created by iblinfotech on 01/09/17.
 */

public class TestPayData {


    private String developerPayload;

    private String packageName;

    private String purchaseToken;

    private String purchaseState;

    private String orderId;

    private String purchaseTime;

    private String productId;

    public String getDeveloperPayload ()
    {
        return developerPayload;
    }

    public void setDeveloperPayload (String developerPayload)
    {
        this.developerPayload = developerPayload;
    }

    public String getPackageName ()
    {
        return packageName;
    }

    public void setPackageName (String packageName)
    {
        this.packageName = packageName;
    }

    public String getPurchaseToken ()
    {
        return purchaseToken;
    }

    public void setPurchaseToken (String purchaseToken)
    {
        this.purchaseToken = purchaseToken;
    }

    public String getPurchaseState ()
    {
        return purchaseState;
    }

    public void setPurchaseState (String purchaseState)
    {
        this.purchaseState = purchaseState;
    }

    public String getOrderId ()
    {
        return orderId;
    }

    public void setOrderId (String orderId)
    {
        this.orderId = orderId;
    }

    public String getPurchaseTime ()
    {
        return purchaseTime;
    }

    public void setPurchaseTime (String purchaseTime)
    {
        this.purchaseTime = purchaseTime;
    }

    public String getProductId ()
    {
        return productId;
    }

    public void setProductId (String productId)
    {
        this.productId = productId;
    }
}



