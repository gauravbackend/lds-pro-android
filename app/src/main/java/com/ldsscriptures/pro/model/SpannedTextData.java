package com.ldsscriptures.pro.model;


public class SpannedTextData {

    private String spannedText;
    private String serverid;
    private String color;
    private boolean isUnderLine;
    private int startIndex;
    private int endIndex;
    private int verseAId;
    private int highlightflag;
    private String deleted;
    private String modified;
    private String verseId;
    private String verseURI;

    public SpannedTextData() {
    }

    public SpannedTextData(int verseAId, int startIndex, int endIndex, String spannedText, String color, int highlightflag, String serverid, String deleted, String modified, String verseId, String verseURI) {
        this.verseAId = verseAId;
        this.startIndex = startIndex;
        this.endIndex = endIndex;
        this.spannedText = spannedText;
        this.color = color;
        this.highlightflag = highlightflag;
        this.serverid = serverid;
        this.deleted = deleted;
        this.modified = modified;
        this.verseId = verseId;
        this.verseURI = verseURI;
    }


    public String getVerseId() {
        return verseId;
    }

    public void setVerseId(String verseId) {
        this.verseId = verseId;
    }

    public String getVerseURI() {
        return verseURI;
    }

    public void setVerseURI(String verseURI) {
        this.verseURI = verseURI;
    }

    public String getDeleted() {
        return deleted;
    }

    public void setDeleted(String deleted) {
        this.deleted = deleted;
    }

    public String getModified() {
        return modified;
    }

    public void setModified(String modified) {
        this.modified = modified;
    }

    public String getServerid() {
        return serverid;
    }

    public void setServerid(String serverid) {
        this.serverid = serverid;
    }

    public int getHighlightflag() {
        return highlightflag;
    }

    public void setHighlightflag(int highlightflag) {
        this.highlightflag = highlightflag;
    }

    public int getVerseAId() {
        return verseAId;
    }

    public void setVerseAId(int verseAId) {
        this.verseAId = verseAId;
    }

    public int getStartIndex() {
        return startIndex;
    }

    public void setStartIndex(int startIndex) {
        this.startIndex = startIndex;
    }

    public int getEndIndex() {
        return endIndex;
    }

    public void setEndIndex(int endIndex) {
        this.endIndex = endIndex;
    }

    public boolean isUnderLine() {
        return isUnderLine;
    }

    public void setUnderLine(boolean underLine) {
        isUnderLine = underLine;
    }
//    private  BackgroundColorSpan backgroundColorSpan;
//    private  UnderlineSpan underlineSpan;


    public String getSpannedText() {
        return spannedText;
    }

    public void setSpannedText(String spannedText) {
        this.spannedText = spannedText;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    @Override
    public String toString() {
        return "SpannedTextData{" +
                "verseAId='" + verseAId + '\'' +
                '}';
    }
}
