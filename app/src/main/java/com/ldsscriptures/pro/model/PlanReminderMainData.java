package com.ldsscriptures.pro.model;

import java.util.ArrayList;

public class PlanReminderMainData {

    public String planID;
    public String planTime;
    public int planSelectHour;
    public int planSelectMinute;
    public boolean planDaysFlag;
    private ArrayList<PlanReminderData> planReminderDataArrayList = new ArrayList<>();

    public PlanReminderMainData() {

    }

    public PlanReminderMainData(String planID, String planTime, int planSelectHour, int planSelectMinute, boolean planDaysFlag, ArrayList<PlanReminderData> planReminderDataArrayList) {
        this.planID = planID;
        this.planTime = planTime;
        this.planSelectHour = planSelectHour;
        this.planSelectMinute = planSelectMinute;
        this.planDaysFlag = planDaysFlag;
        this.planReminderDataArrayList = planReminderDataArrayList;
    }


    public int getPlanSelectHour() {
        return planSelectHour;
    }

    public void setPlanSelectHour(int planSelectHour) {
        this.planSelectHour = planSelectHour;
    }

    public int getPlanSelectMinute() {
        return planSelectMinute;
    }

    public void setPlanSelectMinute(int planSelectMinute) {
        this.planSelectMinute = planSelectMinute;
    }

    public String getPlanTime() {
        return planTime;
    }

    public void setPlanTime(String planTime) {
        this.planTime = planTime;
    }

    public Boolean getPlanDaysFlag() {
        return planDaysFlag;
    }

    public void setPlanDaysFlag(boolean planDaysFlag) {
        this.planDaysFlag = planDaysFlag;
    }

    public String getPlanID() {
        return planID;
    }

    public void setPlanID(String planID) {
        this.planID = planID;
    }

    public ArrayList<PlanReminderData> getPlanReminderDataArrayList() {
        return planReminderDataArrayList;
    }

    public void setPlanReminderDataArrayList(ArrayList<PlanReminderData> planReminderDataArrayList) {
        this.planReminderDataArrayList = planReminderDataArrayList;
    }

    @Override
    public String toString() {
        return "PlanReminderMainData{" +
                "planID='" + planID + '\'' +
                '}';
    }
}