package com.ldsscriptures.pro.model;

import com.ldsscriptures.pro.model.ReadingNow.LibCollection;
import com.ldsscriptures.pro.model.ReadingNow.LibIndex;
import com.ldsscriptures.pro.model.ReadingNow.LibSelection;
import com.ldsscriptures.pro.model.ReadingNow.LibSubIndex;
import com.ldsscriptures.pro.model.ReadingNow.Reading;

import java.util.ArrayList;

public class ReadingNowData {

    public static ReadingNowData readingNowData;
    public LibCollection libCollection;
    public LibSelection libSelection;
    public LibIndex libIndex;
    public LibSubIndex libSubIndex;
    public Reading reading;
    public String activityIdentifier;
    public String tag;
    public String path;
    public boolean isBookmarked;

    public boolean isBookmarked() {
        return isBookmarked;
    }

    public void setBookmarked(boolean bookmarked) {
        isBookmarked = bookmarked;
    }


    public String getActivityIdentifier() {
        return activityIdentifier;
    }

    public void setActivityIdentifier(String activityIdentifier) {
        this.activityIdentifier = activityIdentifier;
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public LibCollection getLibCollection() {
        return libCollection;
    }

    public void setLibCollection(LibCollection libCollection) {
        this.libCollection = libCollection;
    }

    public LibSelection getLibSelection() {
        return libSelection;
    }

    public void setLibSelection(LibSelection libSelection) {
        this.libSelection = libSelection;
    }

    public LibIndex getLibIndex() {
        return libIndex;
    }

    public void setLibIndex(LibIndex libIndex) {
        this.libIndex = libIndex;
    }

    public LibSubIndex getLibSubIndex() {
        return libSubIndex;
    }

    public void setLibSubIndex(LibSubIndex libSubIndex) {
        this.libSubIndex = libSubIndex;
    }

    public Reading getReading() {
        return reading;
    }

    public void setReading(Reading reading) {
        this.reading = reading;
    }

    public static ArrayList<String> tagArrayList = new ArrayList<>();


    public ArrayList<String> getTagArrayList() {
        return tagArrayList;
    }

    public void setTagArrayList(ArrayList<String> tagArrayList) {
        this.tagArrayList = tagArrayList;
    }


    @Override
    public String toString() {
        return "ReadingNowData{" +
                "libCollection=" + libCollection +
                ", libSelection=" + libSelection +
                ", libIndex=" + libIndex +
                ", libSubIndex=" + libSubIndex +
                ", reading=" + reading +
                ", activityIdentifier='" + activityIdentifier + '\'' +
                ", tag='" + tag + '\'' +
                ", path='" + path + '\'' +
                ", isBookmarked=" + isBookmarked +
                '}';
    }
}

