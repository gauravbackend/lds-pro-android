package com.ldsscriptures.pro.model;


import java.io.Serializable;
import java.util.ArrayList;

public class LcData implements Serializable{

    private String id;
    private int library_section_id;
    private int position;
    private int type_id;
    private String library_section_external_id;
    private String title;
    private String cover_renditions;
    private String external_id;
    private ArrayList<LsData> lsSubIndexDatas;

    public ArrayList<LsData> getLsDatas() {
        return lsSubIndexDatas;
    }

    public void setLsDatas(ArrayList<LsData> lsSubIndexDatas) {
        this.lsSubIndexDatas = lsSubIndexDatas;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getLibrary_section_id() {
        return library_section_id;
    }

    public void setLibrary_section_id(int library_section_id) {
        this.library_section_id = library_section_id;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public int getType_id() {
        return type_id;
    }

    public void setType_id(int type_id) {
        this.type_id = type_id;
    }

    public String getLibrary_section_external_id() {
        return library_section_external_id;
    }

    public void setLibrary_section_external_id(String library_section_external_id) {
        this.library_section_external_id = library_section_external_id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getCover_renditions() {
        return cover_renditions;
    }

    public void setCover_renditions(String cover_renditions) {
        this.cover_renditions = cover_renditions;
    }

    public String getExternal_id() {
        return external_id;
    }

    public void setExternal_id(String external_id) {
        this.external_id = external_id;
    }
}
