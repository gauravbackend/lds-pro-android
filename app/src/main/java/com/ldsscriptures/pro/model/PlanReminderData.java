package com.ldsscriptures.pro.model;

public class PlanReminderData {

    public String planReminderId;
    public String planReminderText;
    public String planReminderFullText;
    public Boolean planReminderFlag;

    public PlanReminderData() {

    }

    public PlanReminderData(String planReminderId, String planReminderText, String planReminderFullText, Boolean planReminderFlag) {
        this.planReminderId = planReminderId;
        this.planReminderText = planReminderText;
        this.planReminderFullText = planReminderFullText;
        this.planReminderFlag = planReminderFlag;
    }

    public String getPlanReminderFullText() {
        return planReminderFullText;
    }

    public void setPlanReminderFullText(String planReminderFullText) {
        this.planReminderFullText = planReminderFullText;
    }

    public String getPlanReminderId() {
        return planReminderId;
    }

    public void setPlanReminderId(String planReminderId) {
        this.planReminderId = planReminderId;
    }

    public String getPlanReminderText() {
        return planReminderText;
    }

    public void setPlanReminderText(String planReminderText) {
        this.planReminderText = planReminderText;
    }

    public Boolean getPlanReminderFlag() {
        return planReminderFlag;
    }

    public void setPlanReminderFlag(Boolean planReminderFlag) {
        this.planReminderFlag = planReminderFlag;
    }

    @Override
    public String toString() {
        return "PlanReminderData{" +
                "planReminderId='" + planReminderId + '\'' +
                ", planReminderText='" + planReminderText + '\'' +
                ", planReminderFullText='" + planReminderFullText + '\'' +
                ", planReminderFlag=" + planReminderFlag +
                '}';
    }
}