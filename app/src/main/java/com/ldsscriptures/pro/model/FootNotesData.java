package com.ldsscriptures.pro.model;


public class FootNotesData {

    private String id;
    private String subitem_id;
    private String ref_id;
    private String label_html;
    private String origin_id;
    private String content_html;
    private int word_offset;
    private int byte_location;
    private String origin_uri;

    public String getOrigin_uri() {
        return origin_uri;
    }

    public void setOrigin_uri(String origin_uri) {
        this.origin_uri = origin_uri;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getSubitem_id() {
        return subitem_id;
    }

    public void setSubitem_id(String subitem_id) {
        this.subitem_id = subitem_id;
    }

    public String getRef_id() {
        return ref_id;
    }

    public void setRef_id(String ref_id) {
        this.ref_id = ref_id;
    }

    public String getLabel_html() {
        return label_html;
    }

    public void setLabel_html(String label_html) {
        this.label_html = label_html;
    }

    public String getOrigin_id() {
        return origin_id;
    }

    public void setOrigin_id(String origin_id) {
        this.origin_id = origin_id;
    }

    public String getContent_html() {
        return content_html;
    }

    public void setContent_html(String content_html) {
        this.content_html = content_html;
    }

    public int getWord_offset() {
        return word_offset;
    }

    public void setWord_offset(int word_offset) {
        this.word_offset = word_offset;
    }

    public int getByte_location() {
        return byte_location;
    }

    public void setByte_location(int byte_location) {
        this.byte_location = byte_location;
    }

    @Override
    public String toString() {
        return "FootNotesData{" +
                "id='" + id + '\'' +
                ", subitem_id='" + subitem_id + '\'' +
                ", ref_id='" + ref_id + '\'' +
                ", label_html='" + label_html + '\'' +
                ", origin_id='" + origin_id + '\'' +
                ", content_html='" + content_html + '\'' +
                ", word_offset=" + word_offset +
                ", byte_location=" + byte_location +
                ", origin_uri='" + origin_uri + '\'' +
                '}';
    }
}
