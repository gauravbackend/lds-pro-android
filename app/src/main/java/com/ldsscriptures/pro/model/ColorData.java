package com.ldsscriptures.pro.model;

public class ColorData {
    public String color;
    public boolean isSelected;

    public ColorData(String color, boolean isSelected) {
        this.isSelected = isSelected;
        this.color = color;
    }
}