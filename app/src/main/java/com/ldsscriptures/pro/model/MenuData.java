package com.ldsscriptures.pro.model;

import java.util.ArrayList;
import java.util.List;

public class MenuData {

    public String menuName;
    public List<SubMenu> subMenus = new ArrayList<SubMenu>();

    public MenuData(String brandName, List<SubMenu> mobiles) {
        this.menuName = brandName;
        this.subMenus = mobiles;
    }

}
