package com.ldsscriptures.pro.model;

/**
 * Created by IBL InfoTech on 7/25/2017.
 */

public class LevelModel {

    private int level_point;

    public LevelModel(int level_point) {
        this.level_point = level_point;
    }

    public int getLevel_point() {
        return level_point;
    }

    public void setLevel_point(int level_point) {
        this.level_point = level_point;
    }
}
