package com.ldsscriptures.pro.model;


import java.util.ArrayList;

public class LsMainIndexData {

    int id;
    int position;
    int nav_section_id;
    String title_html;
    String image_renditions;
    String subtitle;
    String uri;


    ArrayList<LsSubIndexData> lsSubIndexDatas;

    public ArrayList<LsSubIndexData> getLsSubIndexDatas() {
        return lsSubIndexDatas;
    }

    public void setLsSubIndexDatas(ArrayList<LsSubIndexData> lsSubIndexDatas) {
        this.lsSubIndexDatas = lsSubIndexDatas;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public int getNav_section_id() {
        return nav_section_id;
    }

    public void setNav_section_id(int nav_section_id) {
        this.nav_section_id = nav_section_id;
    }

    public String getTitle_html() {
        return title_html;
    }

    public void setTitle_html(String title_html) {
        this.title_html = title_html;
    }

    public String getImage_renditions() {
        return image_renditions;
    }

    public void setImage_renditions(String image_renditions) {
        this.image_renditions = image_renditions;
    }

    public String getSubtitle() {
        return subtitle;
    }

    public void setSubtitle(String subtitle) {
        this.subtitle = subtitle;
    }

    public String getUri() {
        return uri;
    }

    public void setUri(String uri) {
        this.uri = uri;
    }


}
