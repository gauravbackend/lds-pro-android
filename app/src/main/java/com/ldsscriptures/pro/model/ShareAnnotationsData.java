package com.ldsscriptures.pro.model;

import java.util.ArrayList;


public class ShareAnnotationsData {

    private ArrayList<TagsData> tags = new ArrayList<>();

    private ArrayList<NotesData> notes = new ArrayList<>();

    private ArrayList<CrossRefrenceData> cross_refs = new ArrayList<>();

    public ArrayList<TagsData> getTags() {
        return tags;
    }

    public void setTags(ArrayList<TagsData> tags) {
        this.tags = tags;
    }

    public ArrayList<NotesData> getNotes() {
        return notes;
    }

    public void setNotes(ArrayList<NotesData> notes) {
        this.notes = notes;
    }

    public ArrayList<CrossRefrenceData> getCross_refs() {
        return cross_refs;
    }

    public void setCross_refs(ArrayList<CrossRefrenceData> cross_refs) {
        this.cross_refs = cross_refs;
    }
}
