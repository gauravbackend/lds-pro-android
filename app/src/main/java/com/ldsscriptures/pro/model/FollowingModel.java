package com.ldsscriptures.pro.model;

import java.util.ArrayList;

/**
 * Created by IBL InfoTech on 7/25/2017.
 */

public class FollowingModel <T>{

    private String msg;

    private String success;

    private String email;

    private String following_user_id;

    private String action;

    private ArrayList<EmailFriendData> email_friend;

    private ArrayList<EmailFriendData> fb_friend;

    public ArrayList<EmailFriendData> getFb_friend() {
        return fb_friend;
    }

    public void setFb_friend(ArrayList<EmailFriendData> fb_friend) {
        this.fb_friend = fb_friend;
    }

    public String getFollowing_user_id() {
        return following_user_id;
    }

    public void setFollowing_user_id(String following_user_id) {
        this.following_user_id = following_user_id;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getSuccess() {
        return success;
    }

    public void setSuccess(String success) {
        this.success = success;
    }

    public ArrayList<EmailFriendData> getEmail_friend() {
        return email_friend;
    }

    public void setEmail_friend(ArrayList<EmailFriendData> email_friend) {
        this.email_friend = email_friend;
    }

    @Override
    public String toString() {
        return "ClassPojo [msg = " + msg + ", success = " + success + ", email_friend = " + email_friend + "]";
    }


}
