package com.ldsscriptures.pro.model;

import java.util.ArrayList;

public class SettingPlanReminderMainData {


    public String planTime;
    public int planSelectHour;
    public int planSelectMinute;
    public String planDate;
    private ArrayList<PlanReminderData> planReminderDataArrayList = new ArrayList<>();

    public SettingPlanReminderMainData() {

    }


    public SettingPlanReminderMainData(String planTime, int planSelectHour, int planSelectMinute, String planDate, ArrayList<PlanReminderData> planReminderDataArrayList) {
        this.planTime = planTime;
        this.planSelectHour = planSelectHour;
        this.planSelectMinute = planSelectMinute;
        this.planDate = planDate;
        this.planReminderDataArrayList = planReminderDataArrayList;
    }

    public String getPlanTime() {
        return planTime;
    }

    public void setPlanTime(String planTime) {
        this.planTime = planTime;
    }

    public int getPlanSelectHour() {
        return planSelectHour;
    }

    public void setPlanSelectHour(int planSelectHour) {
        this.planSelectHour = planSelectHour;
    }

    public int getPlanSelectMinute() {
        return planSelectMinute;
    }

    public void setPlanSelectMinute(int planSelectMinute) {
        this.planSelectMinute = planSelectMinute;
    }

    public String getPlanDate() {
        return planDate;
    }

    public void setPlanDate(String planDate) {
        this.planDate = planDate;
    }

    public ArrayList<PlanReminderData> getPlanReminderDataArrayList() {
        return planReminderDataArrayList;
    }

    public void setPlanReminderDataArrayList(ArrayList<PlanReminderData> planReminderDataArrayList) {
        this.planReminderDataArrayList = planReminderDataArrayList;
    }


    @Override
    public String toString() {
        return "SettingPlanReminderMainData{" +
                "planTime='" + planTime + '\'' +
                ", planSelectHour=" + planSelectHour +
                ", planSelectMinute=" + planSelectMinute +
                ", planDate='" + planDate + '\'' +
                ", planReminderDataArrayList=" + planReminderDataArrayList +
                '}';
    }
}