package com.ldsscriptures.pro.model;

/**
 * Created by iblinfotech on 08/08/17.
 */

public class CrossRefrenceData {

    private String title;

    private String count;

    private String locale;

    private String link_uri;

    private String uri;

    public String getTitle ()
    {
        return title;
    }

    public void setTitle (String title)
    {
        this.title = title;
    }

    public String getCount ()
    {
        return count;
    }

    public void setCount (String count)
    {
        this.count = count;
    }

    public String getLocale ()
    {
        return locale;
    }

    public void setLocale (String locale)
    {
        this.locale = locale;
    }

    public String getLink_uri ()
    {
        return link_uri;
    }

    public void setLink_uri (String link_uri)
    {
        this.link_uri = link_uri;
    }

    public String getUri ()
    {
        return uri;
    }

    public void setUri (String uri)
    {
        this.uri = uri;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [title = "+title+", count = "+count+", locale = "+locale+", link_uri = "+link_uri+", uri = "+uri+"]";
    }
}
