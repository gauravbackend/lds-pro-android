package com.ldsscriptures.pro.model;

import java.io.Serializable;
import java.util.ArrayList;

public class GlobalGroupSearch implements Serializable {

    private String title;
    private ArrayList<GlobalSearchModel> globalSearchModels = new ArrayList<>();

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public ArrayList<GlobalSearchModel> getGlobalSearchModels() {
        return globalSearchModels;
    }

    public void setGlobalSearchModels(ArrayList<GlobalSearchModel> globalSearchModels) {
        this.globalSearchModels = globalSearchModels;
    }


    @Override
    public String toString() {
        return "GlobalGroupSearch{" +
                "title='" + title + '\'' +
                ", globalSearchModels=" + globalSearchModels +
                '}';
    }
}
