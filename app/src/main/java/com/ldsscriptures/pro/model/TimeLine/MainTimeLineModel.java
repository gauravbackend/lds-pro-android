package com.ldsscriptures.pro.model.TimeLine;

import java.util.ArrayList;

/**
 * Created by IBL InfoTech on 9/4/2017.
 */

public class MainTimeLineModel<T> {

    private String sync_again_date;

    private ArrayList<T> items;

    private String sync_again;

    private String user_id;

    public String getSync_again_date() {
        return sync_again_date;
    }

    public void setSync_again_date(String sync_again_date) {
        this.sync_again_date = sync_again_date;
    }

    public ArrayList<T> getItems() {
        return items;
    }

    public void setItems(ArrayList<T> items) {
        this.items = items;
    }

    public String getSync_again() {
        return sync_again;
    }

    public void setSync_again(String sync_again) {
        this.sync_again = sync_again;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    @Override
    public String toString() {
        return "ClassPojo [sync_again_date = " + sync_again_date + ", items = " + items + ", sync_again = " + sync_again + ", user_id = " + user_id + "]";
    }
}

