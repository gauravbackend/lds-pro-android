package com.ldsscriptures.pro.model.TimeLine;

import java.util.ArrayList;

/**
 * Created by IBL InfoTech on 9/4/2017.
 */

public class InnerTimeLineModel {

    private String uid;

    private String verse;

    private String aid;

    private String uri;

    private String profile_img;

    private String for_uid;

    private String share_level;

    private String id;

    private String created;

    private String name;

    private ArrayList<LikesTimeLineModel> likes;

    private ArrayList<CommentTimeLineModel> comments;

    private String note;

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getVerse() {
        return verse;
    }

    public void setVerse(String verse) {
        this.verse = verse;
    }

    public String getAid() {
        return aid;
    }

    public void setAid(String aid) {
        this.aid = aid;
    }

    public String getUri() {
        return uri;
    }

    public void setUri(String uri) {
        this.uri = uri;
    }

    public String getProfile_img() {
        return profile_img;
    }

    public void setProfile_img(String profile_img) {
        this.profile_img = profile_img;
    }

    public String getFor_uid() {
        return for_uid;
    }

    public void setFor_uid(String for_uid) {
        this.for_uid = for_uid;
    }

    public String getShare_level() {
        return share_level;
    }

    public void setShare_level(String share_level) {
        this.share_level = share_level;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCreated() {
        return created;
    }

    public void setCreated(String created) {
        this.created = created;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ArrayList<LikesTimeLineModel> getLikes() {
        return likes;
    }

    public void setLikes(ArrayList<LikesTimeLineModel> likes) {
        this.likes = likes;
    }

    public ArrayList<CommentTimeLineModel> getComments() {
        return comments;
    }

    public void setComments(ArrayList<CommentTimeLineModel> comments) {
        this.comments = comments;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    @Override
    public String toString() {
        return "ClassPojo [uid = " + uid + ", verse = " + verse + ", aid = " + aid + ", uri = " + uri + ", profile_img = " + profile_img + ", for_uid = " + for_uid + ", share_level = " + share_level + ", id = " + id + ", created = " + created + ", name = " + name + ", likes = " + likes + ", comments = " + comments + ", note = " + note + "]";
    }
}

