package com.ldsscriptures.pro.model.ReadingNow;

/**
 * Created by iblinfotech on 01/08/17.
 */

public class LibSelection {    String tag;
    String title;
    String lcId;

    public LibSelection(String tag, String title, String lcId) {
        this.tag = tag;
        this.title = title;
        this.lcId = lcId;
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getLcId() {
        return lcId;
    }

    public void setLcId(String lcId) {
        this.lcId = lcId;
    }
}
