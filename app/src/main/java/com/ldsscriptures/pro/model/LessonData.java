package com.ldsscriptures.pro.model;

/**
 * Created by iblinfotech on 25/07/17.
 */

public class LessonData {
    private String serverid;
    private String lessonname;
    private String lessondate;
    private String lessondesc;
    private int id;
    private String deleted;
    private String modified;


    public String getDeleted() {
        return deleted;
    }

    public void setDeleted(String deleted) {
        this.deleted = deleted;
    }

    public String getModified() {
        return modified;
    }

    public void setModified(String modified) {
        this.modified = modified;
    }

    public String getServerid() {
        return serverid;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setServerid(String serverid) {
        this.serverid = serverid;
    }

    public String getLessonname() {
        return lessonname;
    }

    public void setLessonname(String lessonname) {
        this.lessonname = lessonname;
    }

    public String getLessondate() {
        return lessondate;
    }

    public void setLessondate(String lessondate) {
        this.lessondate = lessondate;
    }

    public String getLessondesc() {
        return lessondesc;
    }

    public void setLessondesc(String lessondesc) {
        this.lessondesc = lessondesc;
    }
}
