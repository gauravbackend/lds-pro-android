package com.ldsscriptures.pro.model;


import java.io.Serializable;
import java.util.ArrayList;

public class LsData implements Serializable{

    int id;
    String external_id;
    int library_section_id;
    String library_section_external_id;
    int position;
    String title;
    String is_obsolete;
    int item_id;
    String item_external_id;
    String item_cover_renditions;
    String uri;
    int itemid;

    ArrayList<NavItemData> combineIndexDataArray;

    boolean isDownloaded = false;

    public ArrayList<NavItemData> getCombineIndexDataArray() {
        return combineIndexDataArray;
    }

    public void setCombineIndexDataArray(ArrayList<NavItemData> combineIndexDataArray) {
        this.combineIndexDataArray = combineIndexDataArray;
    }

    public boolean isDownloaded() {
        return isDownloaded;
    }

    public void setDownloaded(boolean downloaded) {
        isDownloaded = downloaded;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getExternal_id() {
        return external_id;
    }

    public void setExternal_id(String external_id) {
        this.external_id = external_id;
    }

    public int getLibrary_section_id() {
        return library_section_id;
    }

    public void setLibrary_section_id(int library_section_id) {
        this.library_section_id = library_section_id;
    }

    public String getLibrary_section_external_id() {
        return library_section_external_id;
    }

    public void setLibrary_section_external_id(String library_section_external_id) {
        this.library_section_external_id = library_section_external_id;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getIs_obsolete() {
        return is_obsolete;
    }

    public void setIs_obsolete(String is_obsolete) {
        this.is_obsolete = is_obsolete;
    }

    public int getItem_id() {
        return item_id;
    }

    public void setItem_id(int item_id) {
        this.item_id = item_id;
    }

    public String getItem_external_id() {
        return item_external_id;
    }

    public void setItem_external_id(String item_external_id) {
        this.item_external_id = item_external_id;
    }

    public String getItem_cover_renditions() {
        return item_cover_renditions;
    }

    public void setItem_cover_renditions(String item_cover_renditions) {
        this.item_cover_renditions = item_cover_renditions;
    }

    public String getUri() {
        return uri;
    }

    public void setUri(String uri) {
        this.uri = uri;
    }

    public int getItemid() {
        return itemid;
    }

    public void setItemid(int itemid) {
        this.itemid = itemid;
    }

    @Override
    public String toString() {
        return "LsData{" +
                "id=" + id +
                ", external_id='" + external_id + '\'' +
                ", library_section_id=" + library_section_id +
                ", library_section_external_id='" + library_section_external_id + '\'' +
                ", position=" + position +
                ", title='" + title + '\'' +
                ", is_obsolete='" + is_obsolete + '\'' +
                ", item_id=" + item_id +
                ", item_external_id='" + item_external_id + '\'' +
                ", item_cover_renditions='" + item_cover_renditions + '\'' +
                ", uri='" + uri + '\'' +
                ", itemid=" + itemid +
                ", combineIndexDataArray=" + combineIndexDataArray +
                ", isDownloaded=" + isDownloaded +
                '}';
    }
}
