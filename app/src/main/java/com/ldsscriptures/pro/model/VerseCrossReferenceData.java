package com.ldsscriptures.pro.model;

/**
 * Created by iblinfotech on 07/07/17.
 */

public class VerseCrossReferenceData {
    String refTitle1;
    String refIndex1;
    String refAid1;
    String refTitle2;
    String refIndex2;
    String refAid2;
    String serverId;
    String crosstitle;
    String link_title;

    public String getCrosstitle() {
        return crosstitle;
    }

    public void setCrosstitle(String crosstitle) {
        this.crosstitle = crosstitle;
    }

    public String getLink_title() {
        return link_title;
    }

    public void setLink_title(String link_title) {
        this.link_title = link_title;
    }

    public String getServerId() {
        return serverId;
    }

    public void setServerId(String serverId) {
        this.serverId = serverId;
    }

    public String getRefTitle1() {
        return refTitle1;
    }

    public void setRefTitle1(String refTitle1) {
        this.refTitle1 = refTitle1;
    }

    public String getRefIndex1() {
        return refIndex1;
    }

    public void setRefIndex1(String refIndex1) {
        this.refIndex1 = refIndex1;
    }

    public String getRefAid1() {
        return refAid1;
    }

    public void setRefAid1(String refAid1) {
        this.refAid1 = refAid1;
    }

    public String getRefTitle2() {
        return refTitle2;
    }

    public void setRefTitle2(String refTitle2) {
        this.refTitle2 = refTitle2;
    }

    public String getRefIndex2() {
        return refIndex2;
    }

    public void setRefIndex2(String refIndex2) {
        this.refIndex2 = refIndex2;
    }

    public String getRefAid2() {
        return refAid2;
    }

    public void setRefAid2(String refAid2) {
        this.refAid2 = refAid2;
    }

    @Override
    public String toString() {
        return "VerseCrossReferenceData{" +
                "refTitle1='" + refTitle1 + '\'' +
                ", refIndex1='" + refIndex1 + '\'' +
                ", refAid1='" + refAid1 + '\'' +
                ", refTitle2='" + refTitle2 + '\'' +
                ", refIndex2='" + refIndex2 + '\'' +
                ", refAid2='" + refAid2 + '\'' +
                ", serverId='" + serverId + '\'' +
                ", title='" + crosstitle + '\'' +
                ", link_title='" + link_title + '\'' +
                '}';
    }
}
