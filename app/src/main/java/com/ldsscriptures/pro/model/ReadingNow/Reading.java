package com.ldsscriptures.pro.model.ReadingNow;

/**
 * Created by iblinfotech on 01/08/17.
 */

public class Reading {
    public boolean isFromIndex() {
        return isFromIndex;
    }

    public void setFromIndex(boolean fromIndex) {
        isFromIndex = fromIndex;
    }

    String tag;
    int index;

    public String getLcId() {
        return lcId;
    }

    public void setLcId(String lcId) {
        this.lcId = lcId;
    }

    public String getLsId() {
        return lsId;
    }

    public void setLsId(String lsId) {
        this.lsId = lsId;
    }

    boolean isFromIndex;
    String lcId;
    String lsId;
    boolean isHistory;


    public boolean isHistory() {
        return isHistory;
    }

    public void setHistory(boolean history) {
        isHistory = history;
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public Reading(String tag, int index, boolean isHistory, boolean isFromIndex, String lcId, String lsId) {

        this.tag = tag;
        this.index = index;
        this.isHistory = isHistory;
        this.isFromIndex = isFromIndex;
        this.lcId = lcId;
        this.lsId = lsId;
    }
}
