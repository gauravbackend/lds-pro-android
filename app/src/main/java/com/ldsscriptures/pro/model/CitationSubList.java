package com.ldsscriptures.pro.model;

/**
 * Created on 08/08/17 by iblinfotech.
 */

public class CitationSubList {

    private String id, aid, source_title, source_subtitle, source_group, source_uri, ref_uri, verseText;


    public String getVerseText() {
        return verseText;
    }

    public void setVerseText(String verseText) {
        this.verseText = verseText;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getAid() {
        return aid;
    }

    public void setAid(String aid) {
        this.aid = aid;
    }

    public String getSource_title() {
        return source_title;
    }

    public void setSource_title(String source_title) {
        this.source_title = source_title;
    }

    public String getSource_subtitle() {
        return source_subtitle;
    }

    public void setSource_subtitle(String source_subtitle) {
        this.source_subtitle = source_subtitle;
    }

    public String getSource_group() {
        return source_group;
    }

    public void setSource_group(String source_group) {
        this.source_group = source_group;
    }

    public String getSource_uri() {
        return source_uri;
    }

    public void setSource_uri(String source_uri) {
        this.source_uri = source_uri;
    }

    public String getRef_uri() {
        return ref_uri;
    }

    public void setRef_uri(String ref_uri) {
        this.ref_uri = ref_uri;
    }
}
