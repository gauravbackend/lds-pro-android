package com.ldsscriptures.pro.activity.verses;

import android.content.ContentValues;
import android.os.Bundle;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.ldsscriptures.pro.R;
import com.ldsscriptures.pro.activity.BaseActivity;
import com.ldsscriptures.pro.database.CommonDatabaseHelper;
import com.ldsscriptures.pro.model.BookmarkData;
import com.ldsscriptures.pro.utils.Global;
import com.ldsscriptures.pro.utils.PushToServerData;
import com.ldsscriptures.pro.utils.SessionManager;
import com.ldsscriptures.pro.utils.Validator;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class VerseNewBookmarkActivity extends BaseActivity {

    @BindView(R.id.tvVerseId)
    TextView tvVerseId;

    @BindView(R.id.tvVerseName)
    TextView tvVerseName;

    @BindView(R.id.tvTitle)
    TextView tvTitle;

    @BindView(R.id.ivBack)
    ImageView ivBack;

    @BindView(R.id.edtEnterBookTitle)
    EditText edtEnterBookTitle;

    private String bookmarkTitle;
    private SessionManager sessionManager;
    private String verseText;
    private String verseId;
    private String verseAid;
    private String itemUri;
    private String citation_itemUri;
    private int indexId, versePos;
    private CommonDatabaseHelper commonDatabaseHelper;
    BookmarkData bookmarkData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_verse_new_bookmark);
        setContent();
    }

    private void setContent() {
        ButterKnife.bind(this);
        commonDatabaseHelper = new CommonDatabaseHelper(VerseNewBookmarkActivity.this);
        sessionManager = new SessionManager(VerseNewBookmarkActivity.this);

        verseText = getIntent().getStringExtra(getString(R.string.verseText));
        verseId = getIntent().getStringExtra(getString(R.string.verseId));
        verseAid = getIntent().getStringExtra(getString(R.string.verseAId));
        indexId = getIntent().getIntExtra(getString(R.string.indexId), 0);
        versePos = getIntent().getIntExtra(getString(R.string.versePos), 0);
        itemUri = getIntent().getStringExtra(getString(R.string.itemUri));
        showText(tvTitle, getString(R.string.title_new_bookmark));
        edtEnterBookTitle.setSelection(edtEnterBookTitle.getText().length());
        tvVerseId.setText(verseId);
        tvVerseName.setText(verseText);
    }

    @OnClick(R.id.ivBack)
    public void finishActivity1() {
        finish();
    }

    @OnClick(R.id.tvDone)
    public void addNewBookmark() {
        if (validateFields()) {

            String currentDateTimeString = new SimpleDateFormat("dd-MMM-yyyy hh:mm:ss").format(Calendar.getInstance().getTime());

            ContentValues contentValues = new ContentValues();
            contentValues.put("serverid", "0");
            contentValues.put("verseaid", verseAid);
            contentValues.put("chapteruri", Global.bookmarkData.getChapterUri());
            contentValues.put("itemuri", Global.bookmarkData.getItemuri());
            contentValues.put("bookuri", Global.bookmarkData.getBookuri());
            contentValues.put("versetitle", edtEnterBookTitle.getText().toString().trim());
            contentValues.put("versenumber", verseId);
            contentValues.put("versetext", verseText);
            contentValues.put("createdAt", currentDateTimeString);
            contentValues.put("deleted", "");
            contentValues.put("modified", "0");
            contentValues.put("sort_order", "1");

            commonDatabaseHelper.insertData(CommonDatabaseHelper.TABLE_BOOKMARK_MARSTER, contentValues);
            sessionManager.setUserDailyPoints(100);
            sessionManager.setBookmarkCount(1);
            finish();
            if (VerseHighlightActivity.getInstance() != null) {
                VerseHighlightActivity.getInstance().goToReadingView();
            }
            new PushToServerData(this);
        }
    }

    private boolean validateFields() {
        if (!Validator.checkEmpty(edtEnterBookTitle)) {
            Toast.makeText(VerseNewBookmarkActivity.this, getString(R.string.error_bookmark), Toast.LENGTH_LONG).show();
            return false;
        }
        return true;
    }
}
