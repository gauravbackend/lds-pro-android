package com.ldsscriptures.pro.activity.socialmenu;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.ldsscriptures.pro.R;
import com.ldsscriptures.pro.activity.BaseActivity;
import com.ldsscriptures.pro.activity.MainActivity;
import com.ldsscriptures.pro.adapter.LeaderBoardPagerAdapter;
import com.ldsscriptures.pro.model.FollowersMainData;
import com.ldsscriptures.pro.model.FollowingMainModel;
import com.ldsscriptures.pro.retroutils.APIClient;
import com.ldsscriptures.pro.retroutils.APIInterface;
import com.ldsscriptures.pro.utils.SessionManager;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class LeaderBoardActivity extends BaseActivity {

    @BindView(R.id.following_tab_layout)
    TabLayout followingTabLayout;
    @BindView(R.id.followingpager)
    ViewPager followingpager;
    @BindView(R.id.ivMenu)
    ImageView ivMenu;
    @BindView(R.id.ivBack)
    ImageView ivBack;
    @BindView(R.id.tvTitle)
    TextView tvTitle;
    @BindView(R.id.ivSearch)
    ImageView ivSearch;
    @BindView(R.id.ivMore)
    ImageView ivMore;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.main_layout)
    RelativeLayout mainLayout;

    LeaderBoardPagerAdapter mAdapter;
    static LeaderBoardActivity leaderBoardActivity;

    public static ArrayList<FollowersMainData> leaderboardArraylist = new ArrayList<>();
    String paraUserId, paraSessionId;
    @BindView(R.id.ivTabs)
    ImageView ivTabs;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_following);
        ButterKnife.bind(this);
        setContents();
    }

    public void setContents() {

        leaderBoardActivity = this;
        showText(tvTitle, getString(R.string.txt_leaderboard));

        setToolbar(toolbar);
        setVisibilityGone(ivMore);
        setVisibilityGone(ivTabs);
        setVisibilityGone(ivSearch);

        followingTabLayout.addTab(followingTabLayout.newTab().setText(R.string.week));
        followingTabLayout.addTab(followingTabLayout.newTab().setText(R.string.month));
        followingTabLayout.addTab(followingTabLayout.newTab().setText(R.string.year));

        if (LeaderBoardActivity.getInstance().getUserInfo(SessionManager.KEY_USER_ID) != null) {
            paraUserId = LeaderBoardActivity.getInstance().getUserInfo(SessionManager.KEY_USER_ID);
        }
        if (LeaderBoardActivity.getInstance().getUserInfo(SessionManager.KEY_USER_SESSION_ID) != null) {
            paraSessionId = LeaderBoardActivity.getInstance().getUserInfo(SessionManager.KEY_USER_SESSION_ID);
        }
        getLeaderboardFriendAPI(paraUserId, paraSessionId);
    }

    @OnClick(R.id.ivMenu)
    public void openMenu() {
        openSlideMenu();
    }

    public static LeaderBoardActivity getInstance() {
        return leaderBoardActivity;
    }

    private void getLeaderboardFriendAPI(String paraUserId, String paraSessionId) {
        showprogress();
        leaderboardArraylist.clear();
        APIInterface apiService = APIClient.getClient().create(APIInterface.class);
        String url = APIClient.BASE_URL;

        url += "listUsers.pl?user_id=" + paraUserId +
                "&session_id=" + paraSessionId +
                "&apiauth=" + APIClient.AUTH_KEY +
                "&followed_by=" + "1";

        Call<FollowingMainModel<FollowersMainData>> callObject = apiService.getFollowingData(url.replaceAll(" ", "%20"));
        callObject.enqueue(new Callback<FollowingMainModel<FollowersMainData>>() {

            @Override
            public void onResponse(Call<FollowingMainModel<FollowersMainData>> call, Response<FollowingMainModel<FollowersMainData>> response) {

                if (response.body() != null && !response.body().toString().trim().isEmpty()) {
                    if (response.body().getSuccess().equals("true")) {
                        leaderboardArraylist = response.body().getFollowers();
                        Collections.sort(leaderboardArraylist, new Comparator<FollowersMainData>() {
                            @Override
                            public int compare(FollowersMainData lhs, FollowersMainData rhs) {
                                int i1 = Integer.parseInt(lhs.getFollowing_points_week());
                                int i2 = Integer.parseInt(rhs.getFollowing_points_week());
                                return Integer.valueOf(i2).compareTo(i1);
                            }
                        });

                        mAdapter = new LeaderBoardPagerAdapter(getSupportFragmentManager(), followingTabLayout.getTabCount());
                        followingpager.setAdapter(mAdapter);

                        followingpager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(followingTabLayout));
                        followingTabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
                            @Override
                            public void onTabSelected(TabLayout.Tab tab) {
                                followingpager.setCurrentItem(tab.getPosition());
                            }

                            @Override
                            public void onTabUnselected(TabLayout.Tab tab) {

                            }

                            @Override
                            public void onTabReselected(TabLayout.Tab tab) {

                            }
                        });
                    }
                    dismissprogress();
                }
            }

            @Override
            public void onFailure(Call<FollowingMainModel<FollowersMainData>> call, Throwable t) {
                dismissprogress();
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menuBack:
                finish();
                break;
            case R.id.menuNext:
                Toast.makeText(getApplicationContext(), "No Item Found", Toast.LENGTH_LONG).show();
                break;
            case R.id.menuBookmark:
                Intent bookmarkIntent = new Intent(getApplicationContext(), MainActivity.class);
                bookmarkIntent.putExtra(getString(R.string.menuSelection), "Bookmarks");
                bookmarkIntent.putExtra(getString(R.string.isFromReading), true);
                startActivityWithAnimation(bookmarkIntent);
                break;
            case R.id.menuHistory:
                Intent historyIntent = new Intent(getApplicationContext(), MainActivity.class);
                historyIntent.putExtra(getString(R.string.menuSelection), "History");
                historyIntent.putExtra(getString(R.string.isFromReading), true);
                startActivityWithAnimation(historyIntent);
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}
