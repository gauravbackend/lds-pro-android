package com.ldsscriptures.pro.activity.verses;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.TextView;

import com.ldsscriptures.pro.R;
import com.ldsscriptures.pro.activity.BaseActivity;
import com.ldsscriptures.pro.adapter.TagListAdapter;
import com.ldsscriptures.pro.model.VerseTagsData;
import com.ldsscriptures.pro.utils.SessionManager;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class VerseTagActivity extends BaseActivity {

    private String verseText;
    private String verseId;
    private String verseAId;
    private String stringTag;

    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;

    @BindView(R.id.tvTitle)
    TextView tvTitle;

    @BindView(R.id.tvNoData)
    TextView tvNoData;

    private SessionManager sessionManager;
    private TagListAdapter tagListAdapter;
    private LinearLayoutManager layoutManager;
    String title;
    ArrayList<VerseTagsData> verseTagsArrayList = new ArrayList<VerseTagsData>();
    ArrayList<VerseTagsData> oldverseTagsArrayList = new ArrayList<VerseTagsData>();
    static VerseTagActivity verseTagActivity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_verse_tag);
    }

    private void setContents() {
        ButterKnife.bind(this);
        showText(tvTitle, getString(R.string.txt_tags));
        verseTagActivity = this;
        sessionManager = new SessionManager(VerseTagActivity.this);
        verseText = getIntent().getStringExtra(getString(R.string.verseText));
        verseId = getIntent().getStringExtra(getString(R.string.verseId));
        verseAId = getIntent().getStringExtra(getString(R.string.verseAId));
        stringTag = getIntent().getStringExtra("stringTag");
        title = getIntent().getStringExtra("title");

        setRecyclerView();
    }

    private void setRecyclerView() {

        verseTagsArrayList = sessionManager.getPreferencesTagsArrayList(VerseTagActivity.this);
        oldverseTagsArrayList = sessionManager.getPreferencesTagsArrayList(VerseTagActivity.this);

        if (verseTagsArrayList != null && verseTagsArrayList.size() > 0) {

            removeDuplicates(verseTagsArrayList);

            ArrayList<VerseTagsData> tagDataArrayListnew = new ArrayList<>();
            for (int i = 0; i < verseTagsArrayList.size(); i++) {
                if (verseTagsArrayList.get(i).getDeleted().isEmpty()) {
                    tagDataArrayListnew.add(verseTagsArrayList.get(i));
                }
            }
            if (tagDataArrayListnew != null) {
                if (tagDataArrayListnew.size() > 0) {
                    tagListAdapter = new TagListAdapter(VerseTagActivity.this, tagDataArrayListnew, oldverseTagsArrayList, verseText, verseId, verseAId, stringTag, title);
                    layoutManager = new LinearLayoutManager(VerseTagActivity.this, RecyclerView.VERTICAL, false);
                    recyclerView.setLayoutManager(layoutManager);
                    recyclerView.setAdapter(tagListAdapter);
                    setVisibilityVisible(recyclerView);
                    setVisibilityGone(tvNoData);
                } else {
                    setVisibilityVisible(tvNoData);
                    setVisibilityGone(recyclerView);
                    showText(tvNoData, getString(R.string.no_tag_found));
                }
            }

        } else {
            setVisibilityVisible(tvNoData);
            setVisibilityGone(recyclerView);
            showText(tvNoData, getString(R.string.no_tag_found));
        }
    }

    @OnClick(R.id.fab)
    public void AdBookmark() {
        Intent intent = new Intent(VerseTagActivity.this, VerseNewTagActivity.class);
        intent.putExtra(getString(R.string.verseId), verseId);
        intent.putExtra(getString(R.string.verseText), verseText);
        intent.putExtra(getString(R.string.verseAId), verseAId);
        intent.putExtra("title", title);

        startActivityWithAnimation(intent);
    }

    @OnClick(R.id.ivBack)
    public void finishActivity() {
        finish();
    }

    @Override
    protected void onResume() {
        super.onResume();
        setContents();
    }

    private void removeDuplicates(ArrayList<VerseTagsData> verseTagsArrayList) {
        int count = verseTagsArrayList.size();

        for (int i = 0; i < count; i++) {
            for (int j = i + 1; j < count; j++) {

                if (verseTagsArrayList.get(i).getTagname().equals(verseTagsArrayList.get(j).getTagname())) {

                    verseTagsArrayList.remove(j--);
                    count--;
                }

            }
        }
    }

    public static VerseTagActivity getInstance() {
        return verseTagActivity;
    }

    public void visibleHideView() {
        setVisibilityVisible(tvNoData);
        setVisibilityGone(recyclerView);
        showText(tvNoData, getString(R.string.no_tag_found));
    }
}
