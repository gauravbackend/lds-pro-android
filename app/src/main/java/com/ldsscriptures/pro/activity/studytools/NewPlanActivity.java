package com.ldsscriptures.pro.activity.studytools;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;

import com.ldsscriptures.pro.R;
import com.ldsscriptures.pro.activity.BaseActivity;
import com.ldsscriptures.pro.adapter.LcGridAdapter;
import com.ldsscriptures.pro.dbClass.GetLibCollectionFromDB;
import com.ldsscriptures.pro.model.LcData;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnItemClick;

/**
 * Created by IBL InfoTech on 8/5/2017.
 */

public class NewPlanActivity extends BaseActivity {

    @BindView(R.id.ivMenu)
    ImageView ivMenu;
    @BindView(R.id.ivBack)
    ImageView ivBack;
    @BindView(R.id.tvTitle)
    TextView tvTitle;
    @BindView(R.id.ivSearch)
    ImageView ivSearch;
    @BindView(R.id.ivMore)
    ImageView ivMore;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.gridView)
    GridView gridView;

    static NewPlanActivity newPlanActivity;
    @BindView(R.id.ivTabs)
    ImageView ivTabs;
    private ArrayList<LcData> lcDataArrayList = new ArrayList<>();
    LcGridAdapter lcGridAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_plan);
        ButterKnife.bind(this);
        setContents();

    }

    public void setContents() {
        newPlanActivity = this;
        showText(tvTitle, getString(R.string.txt_new_plan));

        ivMenu.setVisibility(View.GONE);
        ivBack.setVisibility(View.VISIBLE);

        setVisibilityGone(ivTabs);
        setVisibilityGone(ivSearch);

        lcDataArrayList = new GetLibCollectionFromDB(this).getLcArrayListFromDB();
        lcGridAdapter = new LcGridAdapter(this, lcDataArrayList);
        gridView.setAdapter(lcGridAdapter);
    }

    public static NewPlanActivity getInstance() {
        return newPlanActivity;
    }

    @OnClick(R.id.ivBack)
    public void onViewClicked() {
        finish();
    }

    @OnItemClick(R.id.gridView)
    public void openLSActivity(int position) {
        Intent intent = new Intent(getApplicationContext(), NewPlanSubListActivity.class);
        intent.putExtra(getString(R.string.lcID), lcDataArrayList.get(position).getId());
        startActivity(intent);
    }
}
