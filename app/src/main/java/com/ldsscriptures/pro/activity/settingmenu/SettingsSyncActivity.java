package com.ldsscriptures.pro.activity.settingmenu;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.Switch;
import android.widget.TextView;

import com.ldsscriptures.pro.R;
import com.ldsscriptures.pro.activity.BaseActivity;
import com.ldsscriptures.pro.database.CommonDatabaseHelper;
import com.ldsscriptures.pro.model.SyncItems;
import com.ldsscriptures.pro.service.SyncEventReceiver;
import com.ldsscriptures.pro.utils.Global;
import com.ldsscriptures.pro.utils.SessionManager;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


public class SettingsSyncActivity extends BaseActivity {
    @BindView(R.id.ivBack)
    ImageView ivBack;
    @BindView(R.id.ivMenu)
    ImageView ivMenu;
    @BindView(R.id.tvTitle)
    TextView tvTitle;
    @BindView(R.id.ivDropDownMenu)
    ImageView ivDropDownMenu;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.switchSync)
    Switch switchSync;
    @BindView(R.id.switchSyncOnWifi)
    Switch switchSyncOnWifi;
    private CommonDatabaseHelper commonDatabaseHelper;
    private SessionManager sessionManager;
    private ArrayList<SyncItems> syncItemsArrayList = new ArrayList<SyncItems>();
    private String TAG = getClass().getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings_sync);
        ButterKnife.bind(this);
        setContent();
    }

    private void setContent() {

        ButterKnife.bind(this);
        setToolbar(toolbar);

        commonDatabaseHelper = new CommonDatabaseHelper(SettingsSyncActivity.this);
        sessionManager = new SessionManager(SettingsSyncActivity.this);

        showText(tvTitle, getResources().getString(R.string.sync));
        setVisibilityVisible(ivBack);
        setVisibilityGone(ivMenu);

        switchSync.setChecked(sessionManager.getSyncStatus());
        switchSyncOnWifi.setChecked(sessionManager.getSyncWifiStatus());

        switchSync.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                sessionManager.setSyncStatus(isChecked);
                if (isChecked) {
                    boolean isOnWifi = sessionManager.getSyncWifiStatus();
                    if (isOnWifi) {
                        if (checkWifi()) {
                            SyncEventReceiver.setupAlarm(getApplicationContext());
                        }
                    } else {
                        SyncEventReceiver.setupAlarm(getApplicationContext());
                    }
                } else {
                    SyncEventReceiver.cancelAlarm(getApplicationContext());
                }
            }
        });

        switchSyncOnWifi.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                sessionManager.setSyncWifiStatus(isChecked);

                if (isChecked) {
                    boolean isSync = sessionManager.getSyncStatus();

                    if (isSync) {
                        if (checkWifi()) {
                            SyncEventReceiver.setupAlarm(getApplicationContext());
                        }
                    }
                } else {
                    SyncEventReceiver.cancelAlarm(getApplicationContext());
                }
            }
        });
    }

    private boolean checkWifi() {
        ConnectivityManager connManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo mWifi = connManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);

        if (mWifi.isConnected()) {
            // Do whatever
            Global.printLog(TAG, "switchSync -checkWifi--true ");

            return true;
        }
        return false;
    }

    @OnClick(R.id.ivBack)
    public void onViewClicked() {
        finish();
    }
}
