package com.ldsscriptures.pro.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.Profile;
import com.facebook.ProfileTracker;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.ldsscriptures.pro.BuildConfig;
import com.ldsscriptures.pro.R;
import com.ldsscriptures.pro.model.UserDetail;
import com.ldsscriptures.pro.retroutils.APIClient;
import com.ldsscriptures.pro.retroutils.APIInterface;
import com.ldsscriptures.pro.utils.Global;

import org.json.JSONException;
import org.json.JSONObject;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.Arrays;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.ldsscriptures.pro.retroutils.APIClient.DEVICE_KEY;

/**
 * Created on 30/05/17 by iblinfotech.
 */

public class LoginTwoActivity extends BaseActivity {

    @BindView(R.id.btnFacebook)
    Button btnFacebook;

    @BindView(R.id.btnLds)
    Button btnLds;

    @BindView(R.id.tv_signUp)
    TextView tvSignUp;

    @BindView(R.id.imageView_background)
    ImageView imageView_background;

    private CallbackManager callbackManager;
    private ProfileTracker profileTracker;
    private boolean isFbLoginSuccess = false;
    private String fbUserEmail = "";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_two);
        setContents();
    }

    private void setContents() {
        ButterKnife.bind(this);

        setBackground(R.drawable.bg_splash_blur, imageView_background);

        FacebookSdk.sdkInitialize(LoginTwoActivity.this);
        callbackManager = CallbackManager.Factory.create();

        LoginManager.getInstance().registerCallback(callbackManager,
                new FacebookCallback<LoginResult>() {
                    @Override
                    public void onSuccess(LoginResult loginResult) {
                        isFbLoginSuccess = true;

                        String accessToken = loginResult.getAccessToken().getToken();
                        GraphRequest request = GraphRequest.newMeRequest(loginResult.getAccessToken(), new GraphRequest.GraphJSONObjectCallback() {

                            @Override
                            public void onCompleted(JSONObject object, GraphResponse response) {
                                if (Global.isNetworkAvailable(LoginTwoActivity.this)) {
                                    getFacebookData(object);
                                } else {
                                    Global.showNetworkAlert(LoginTwoActivity.this);
                                }
                            }
                        });

                        Bundle parameters = new Bundle();
                        parameters.putString("fields", "id,name, first_name, last_name, email,gender, birthday, location"); // Parámetros que pedimos a facebook
                        request.setParameters(parameters);
                        request.executeAsync();
                    }

                    @Override
                    public void onCancel() {
                        isFbLoginSuccess = false;
                        updateUI();
                    }

                    @Override
                    public void onError(FacebookException exception) {
                        isFbLoginSuccess = false;
                        updateUI();
                    }
                });

        LoginManager.getInstance().registerCallback(callbackManager, fbLoginCallback);

    }

    private final FacebookCallback fbLoginCallback = new FacebookCallback<LoginResult>() {
        @Override
        public void onSuccess(final LoginResult loginResult) {
            isFbLoginSuccess = true;
            String accessToken = loginResult.getAccessToken().getToken();
            GraphRequest request = GraphRequest.newMeRequest(loginResult.getAccessToken(), new GraphRequest.GraphJSONObjectCallback() {

                @Override
                public void onCompleted(JSONObject object, GraphResponse response) {
                    updateUI();
                }
            });

            Bundle parameters = new Bundle();
            parameters.putString("fields", "id,name, first_name, last_name, email,gender, birthday, location,friends"); // Parámetros que pedimos a facebook
            request.setParameters(parameters);
            request.executeAsync();
        }

        @Override
        public void onCancel() {
            isFbLoginSuccess = false;
            updateUI();
        }

        @Override
        public void onError(FacebookException e) {
            isFbLoginSuccess = false;
            updateUI();
        }
    };

    private Bundle getFacebookData(JSONObject object) {
        if (object != null) {
            Bundle bundle = new Bundle();
            try {
                String id = object.getString("id");
                URL profile_pic;
                try {
                    profile_pic = new URL("https://graph.facebook.com/" + id + "/picture?width=720&height=720");
                    Global.printLog("profile_pic", profile_pic + "");
                } catch (MalformedURLException e) {
                    e.printStackTrace();
                    return null;
                }

                fbUserEmail = object.getString("email");
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return bundle;
        } else {
            Toast.makeText(LoginTwoActivity.this, getString(R.string.please_try_again), Toast.LENGTH_SHORT).show();
        }
        return null;
    }

    private void updateUI() {
        boolean enableButtons = AccessToken.getCurrentAccessToken() != null;

        Profile profile = Profile.getCurrentProfile();
        if (isFbLoginSuccess) {
            isFbLoginSuccess = false;
            if (profile != null) {
                signUpFBUser(profile);
            }
        }
    }

    @OnClick(R.id.btnFacebook)
    public void facebookLogIn() {

        if (Global.isNetworkAvailable(LoginTwoActivity.this)) {
            List<String> permissionNeeds = Arrays.asList("public_profile", "user_friends", "user_birthday", "email");
            LoginManager.getInstance().logInWithReadPermissions(LoginTwoActivity.this, permissionNeeds);
        } else {
            Global.showNetworkAlert(LoginTwoActivity.this);
        }
    }

    @OnClick(R.id.btnLds)
    public void ldsScriptures() {
        startActivity(new Intent(LoginTwoActivity.this, LoginActivity.class));
        overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
    }

    @OnClick(R.id.tv_signUp)
    public void signUp() {
        startActivity(new Intent(LoginTwoActivity.this, SignUpActivity.class));
        overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
    }

    private void getLoggedIn(Profile profile) {

        // profilePictureView.setProfileId(profile.getId());
//            SharedData.fb_user_id = profile.getId();
//            SharedData.fb_user_f_name = profile.getFirstName();
//            SharedData.fb_user_l_name = profile.getLastName();
//            SharedData.fb_photo_url = "" + profile.getProfilePictureUri(720, 720);
//            if (CheckNetwork.isNetworkAvailable(LoginActivity.this))
//                registerfacebookUser("");
////                registerfacebookUser(object.getString("friends")); // facebook friend list String
//            else
//                CheckNetwork.displayAlert(LoginActivity.this);

        APIInterface apiService = APIClient.getClient().create(APIInterface.class);

//        DialogUtils.displayAlert(LoginActivity.this);

        String url = APIClient.BASE_URL;

        url += "login.pl?username=" + profile.getFirstName() +
                "&password=" +
                "&device=" + DEVICE_KEY +
                "&fb_id=" + profile.getId() +
                "&fb_user=" + profile.getName() +
                "&appversion=" + BuildConfig.VERSION_NAME +
                "&apiauth=" + APIClient.AUTH_KEY;


        Global.printLog("url==========", "" + url);
        Call<UserDetail> callObject = apiService.updateUserInfo(url);

        callObject.enqueue(new Callback<UserDetail>() {
            @Override
            public void onResponse(Call<UserDetail> call, Response<UserDetail> response) {

                Global.printLog("FB Login response", "=====" + response);
                Global.printLog("FB Login response.body()", "=====" + response.body());

                if (response.body() != null && !response.body().toString().trim().isEmpty()) {

                    if (Integer.parseInt(response.body().getSuccess()) == 0) {
                        Global.showToast(LoginTwoActivity.this, response.body().getMsg());
                    } else {

                    }
                }
            }

            @Override
            public void onFailure(Call<UserDetail> call, Throwable t) {
                call.cancel();
                Global.printLog("====onFailure====", "======" + t.getLocalizedMessage());
//                DialogUtils.dismissProgressDialog();
            }
        });
    }

    private void signUpFBUser(Profile profile) {
        APIInterface apiService = APIClient.getClient().create(APIInterface.class);
        String url = APIClient.BASE_URL;

        url += "addUser.pl?" +
                "&fb_id=" + profile.getId() +
                "&username=" + profile.getName() +
                "&password=" +
                "&name=" + profile.getFirstName() +
                "&email=" + fbUserEmail +
                "&img_url=" + profile.getProfilePictureUri(720, 720) +
                "&device=" + DEVICE_KEY +
                "&app_version=" + BuildConfig.VERSION_NAME +
                "&device_token=1234123412341234" +
                "&apiauth=" + APIClient.AUTH_KEY;

        Call<UserDetail> callObject = apiService.updateUserInfo(url);

        callObject.enqueue(new Callback<UserDetail>() {
            @Override
            public void onResponse(Call<UserDetail> call, Response<UserDetail> response) {
                if (response.body() != null && !response.body().toString().trim().isEmpty()) {
                    if (Integer.parseInt(response.body().getSuccess()) == 0) {
                        Global.showToast(LoginTwoActivity.this, response.body().getMsg());
                    } else {

                    }
                }
            }

            @Override
            public void onFailure(Call<UserDetail> call, Throwable t) {
                call.cancel();
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }
}
