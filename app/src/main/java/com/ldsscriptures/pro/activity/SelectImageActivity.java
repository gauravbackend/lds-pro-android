package com.ldsscriptures.pro.activity;

import android.Manifest;
import android.annotation.TargetApi;
import android.content.ActivityNotFoundException;
import android.content.ContentUris;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.ldsscriptures.pro.BuildConfig;
import com.ldsscriptures.pro.R;
import com.ldsscriptures.pro.utils.RealPathUtil;
import com.yanzhenjie.permission.AndPermission;
import com.yanzhenjie.permission.PermissionNo;
import com.yanzhenjie.permission.PermissionYes;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class SelectImageActivity extends AppCompatActivity {
    public static final int RESULT_PERMISSION = 999;
    public static final int RESULT_FROM_CAMERA = 99;
    public static final int RESULT_FROM_GALLERY = 98;
    public static final String TEMP_PHOTO_FILE_NAME = "temp_photo.jpeg";
    private File mFileTemp;

    public static File mSelectedFileTemp;

    @BindView(R.id.tvSkip)
    TextView tvSkip;

    @BindView(R.id.tvSelectPic)
    TextView tvSelectPic;

    @BindView(R.id.tvTakePhoto)
    TextView tvTakePhoto;

    @BindView(R.id.ivProfileImage)
    ImageView ivProfileImage;

    public static Bitmap mBitmap;
    public static Bitmap croppedBitmap;

    public static String path;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_image);
        setContents();
    }

    private void setContents() {
        ButterKnife.bind(this);
        String state = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED.equals(state)) {
            mFileTemp = new File(Environment.getExternalStorageDirectory(), TEMP_PHOTO_FILE_NAME);
        } else {
            mFileTemp = new File(getFilesDir(), TEMP_PHOTO_FILE_NAME);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (croppedBitmap != null) {
            ivProfileImage.setPadding(0, 0, 0, 0);
            Glide.with(SelectImageActivity.this)
                    .load(bitmapToByte(croppedBitmap)).asBitmap()
                    .fitCenter()
                    .into(ivProfileImage);
        }
    }

    private byte[] bitmapToByte(Bitmap bitmap) {
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, stream);
        byte[] byteArray = stream.toByteArray();
        return byteArray;
    }

    public Uri getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 20, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "Title", null);
        return Uri.parse(path);
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            if (requestCode == RESULT_FROM_GALLERY) {
                /*Uri selectedImageUri = data.getData();

                if (selectedImageUri != null) {
                    Bitmap bm = null;
                    try {
                        bm = MediaStore.Images.Media.getBitmap(this.getContentResolver(), selectedImageUri);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    path = getFilePathFromUri(this, selectedImageUri);
                    mSelectedFileTemp = new File(path);
                    mBitmap = bm;
                    Intent intent = new Intent(SelectImageActivity.this, CropActivity.class);
                    startActivity(intent);
                    finish();
                    overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
                }*/


                Uri temp = data.getData();

                if (temp.toString().startsWith("content://com.google.android.apps")) {
                    try {
                        InputStream is = getContentResolver().openInputStream(temp);
                        if (is != null) {
                            Bitmap pictureBitmap = BitmapFactory.decodeStream(is);
                            String realPath = RealPathUtil.getRealPath(this, getImageUri(this, pictureBitmap));
                            mSelectedFileTemp = new File(realPath);
                            mBitmap = pictureBitmap;
                            Intent intent = new Intent(SelectImageActivity.this, CropActivity.class);
                            startActivity(intent);
                            finish();
                            overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
                        }
                    } catch (FileNotFoundException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                } else {
                    Bitmap bm = null;
                    try {
                        bm = MediaStore.Images.Media.getBitmap(this.getContentResolver(), temp);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    path = getFilePathFromUri(this, temp);
                    mSelectedFileTemp = new File(path);
                    mBitmap = bm;
                    Intent intent = new Intent(SelectImageActivity.this, CropActivity.class);
                    startActivity(intent);
                    finish();
                    overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
                }


            } else if (requestCode == RESULT_FROM_CAMERA) {
                mBitmap = BitmapFactory.decodeFile(mFileTemp.getPath());


                Uri tempUri = getImageUri(SelectImageActivity.this, mBitmap);
                System.out.println(tempUri);

                path = getFilePathFromUri(this, tempUri);
                mSelectedFileTemp = new File(path);

                Intent intent = new Intent(SelectImageActivity.this, CropActivity.class);
                startActivity(intent);
                overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
                finish();
            }
        }
    }

    public String getRealPathFromURI(Uri contentUri) {
        Cursor cursor = null;
        try {
            String[] proj = {MediaStore.Images.Media.DATA};
            cursor = getContentResolver().query(contentUri, proj, null, null, null);
            int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            cursor.moveToFirst();
            return cursor.getString(column_index);
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
    }

    /**
     * Get the file path from the given Uri.
     *
     * @param context The activity of the calling activity.
     * @param uri     The Uri whose file path is returned.
     */
    @TargetApi(Build.VERSION_CODES.KITKAT)
    public static String getFilePathFromUri(final Context context, final Uri uri) {

        final boolean isKitKat = Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT;

        // DocumentProvider
        if (isKitKat && DocumentsContract.isDocumentUri(context, uri)) {
            // ExternalStorageProvider
            if (isExternalStorageDocument(uri)) {

                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];

                if ("primary".equalsIgnoreCase(type)) {
                    return Environment.getExternalStorageDirectory() + "/" + split[1];
                }
            }
            // DownloadsProvider
            else if (isDownloadsDocument(uri)) {

                final String id = DocumentsContract.getDocumentId(uri);
                final Uri contentUri = ContentUris.withAppendedId(
                        Uri.parse("content://downloads/public_downloads"), Long.valueOf(id));

                return getDataColumn(context, contentUri, null, null);
            }
            // MediaProvider
            else if (isMediaDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];

                Uri contentUri = null;
                if ("image".equals(type)) {
                    contentUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
                } else if ("video".equals(type)) {
                    contentUri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
                } else if ("audio".equals(type)) {
                    contentUri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
                }

                final String selection = "_id=?";
                final String[] selectionArgs = new String[]{split[1]};

                return getDataColumn(context, contentUri, selection, selectionArgs);
            }
        }
        // MediaStore (and general)
        else if ("content".equalsIgnoreCase(uri.getScheme())) {
            return getDataColumn(context, uri, null, null);
        }
        // File
        else if ("file".equalsIgnoreCase(uri.getScheme())) {
            return uri.getPath();
        }

        return null;
    }

    /**
     * Get the value of the data column for this Uri. This is useful for
     * MediaStore Uris, and other file-based ContentProviders.
     *
     * @param context       The activity.
     * @param uri           The Uri to query.
     * @param selection     (Optional) Filter used in the query.
     * @param selectionArgs (Optional) Selection arguments used in the query.
     * @return The value of the _data column, which is typically a file path.
     */
    public static String getDataColumn(Context context, Uri uri, String selection,
                                       String[] selectionArgs) {

        Cursor cursor = null;
        final String column = "_data";
        final String[] projection = {column};

        try {

            cursor = context.getContentResolver()
                    .query(uri, projection, selection, selectionArgs, null);
            if (cursor != null && cursor.moveToFirst()) {

                final int column_index = cursor.getColumnIndexOrThrow(column);
                return cursor.getString(column_index);
            }
        } finally {

            if (cursor != null) {
                cursor.close();
            }
        }
        return null;
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is ExternalStorageProvider.
     */
    public static boolean isExternalStorageDocument(Uri uri) {
        return "com.android.externalstorage.documents".equals(uri.getAuthority());
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is DownloadsProvider.
     */
    public static boolean isDownloadsDocument(Uri uri) {
        return "com.android.providers.downloads.documents".equals(uri.getAuthority());
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is MediaProvider.
     */
    public static boolean isMediaDocument(Uri uri) {
        return "com.android.providers.media.documents".equals(uri.getAuthority());
    }


    @PermissionYes(RESULT_PERMISSION)
    private void getPermissionYes(List<String> grantedPermissions) {
        if (grantedPermissions.contains(Manifest.permission.CAMERA)) {
            cameraIntent();
        } else {
            Intent intent = new Intent();
            intent.setType("image/*");
            intent.setAction("android.intent.action.GET_CONTENT");
            intent.addCategory("android.intent.category.OPENABLE");
            startActivityForResult(Intent.createChooser(intent, "Choose Image"), RESULT_FROM_GALLERY);
        }
    }

    private void cameraIntent() {

        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        try {
            Uri mImageCaptureUri = null;
            String state = Environment.getExternalStorageState();
            if (Environment.MEDIA_MOUNTED.equals(state)) {
                mImageCaptureUri = FileProvider.getUriForFile(SelectImageActivity.this,
                        BuildConfig.APPLICATION_ID + ".provider",
                        mFileTemp);
            } else {
            }
            intent.putExtra(MediaStore.EXTRA_OUTPUT, mImageCaptureUri);
            intent.putExtra("return-data", true);
            intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);

            String key = "Done";
            String title = intent.getStringExtra(key);
            setTitle(title);

            startActivityForResult(intent, RESULT_FROM_CAMERA);
        } catch (ActivityNotFoundException e) {
            Toast.makeText(SelectImageActivity.this, "cannot take picture", Toast.LENGTH_SHORT).show();
        }
    }


    @PermissionNo(RESULT_PERMISSION)
    private void getPermissionNo(List<String> deniedPermissions) {

    }

    @OnClick(R.id.tvSkip)
    public void Skip() {
        SignUpActivity.getInstance().signUpNewUSer("");
        finish();
    }

    @OnClick(R.id.tvSelectPic)
    public void SelectPic() {
        AndPermission.with(SelectImageActivity.this)
                .requestCode(RESULT_PERMISSION)
                .permission(Manifest.permission.WRITE_EXTERNAL_STORAGE)
                .callback(SelectImageActivity.this)
                .start();
    }

    @OnClick(R.id.tvTakePhoto)
    public void TakePhoto() {
        AndPermission.with(SelectImageActivity.this)
                .requestCode(RESULT_PERMISSION)
                .permission(Manifest.permission.CAMERA)
                .callback(SelectImageActivity.this)
                .start();
    }
}
