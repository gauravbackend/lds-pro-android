package com.ldsscriptures.pro.activity.content;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.ldsscriptures.pro.R;
import com.ldsscriptures.pro.fragment.BaseFragment;

import butterknife.BindView;
import butterknife.ButterKnife;

public class LyricsFragment extends BaseFragment {
    @BindView(R.id.webView)
    WebView webView;

    @BindView(R.id.textView)
    TextView textView;

    private String pageUrl;

    public ProgressDialog pd;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_layrics, container, false);
        LinearLayout lnr_root = (LinearLayout) view.findViewById(R.id.lnr_root);
        ButterKnife.bind(this, lnr_root);
        setContents();

        return view;
    }

    private void setContents() {
        // this load page url in webview
        pageUrl = getArguments().getString(getResources().getString(R.string.page_url));
        if (pageUrl != null) {
            pd = new ProgressDialog(getActivity());
            pd.setMessage("loading...");
            pd.show();
            webView.setWebViewClient(new MyWebViewClient());
            webView.loadUrl(pageUrl + "/?m=0");
        }
    }

    public class MyWebViewClient extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            view.loadUrl(url);
            if (!pd.isShowing()) {
                pd.show();
            }
            return true;
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            if (pd.isShowing()) {
                pd.dismiss();
            }
        }

        @Override
        public void onReceivedError(WebView view, WebResourceRequest request, WebResourceError error) {
            super.onReceivedError(view, request, error);
            if (pd.isShowing()) {
                pd.dismiss();
            }
        }
    }
}

