package com.ldsscriptures.pro.activity.studytools;

import android.content.Intent;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.ldsscriptures.pro.R;
import com.ldsscriptures.pro.activity.BaseActivity;
import com.ldsscriptures.pro.activity.MainActivity;
import com.ldsscriptures.pro.adapter.LessonListAdapter;
import com.ldsscriptures.pro.database.CommonDatabaseHelper;
import com.ldsscriptures.pro.model.LessonData;
import com.ldsscriptures.pro.utils.Global;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class LessonsActivity extends BaseActivity {

    @BindView(R.id.ivMenu)
    ImageView ivMenu;
    @BindView(R.id.tvTitle)
    TextView tvTitle;
    @BindView(R.id.ivSearch)
    ImageView ivSearch;
    @BindView(R.id.ivMore)
    ImageView ivMore;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.rvUpcoming)
    RecyclerView rvUpcoming;
    @BindView(R.id.rvPrevious)
    RecyclerView rvPrevious;
    @BindView(R.id.fab)
    FloatingActionButton fab;
    @BindView(R.id.txtNoDataUpcoming)
    TextView txtNoDataUpcoming;
    @BindView(R.id.txtNoDataPrevious)
    TextView txtNoDataPrevious;
    @BindView(R.id.ivBack)
    ImageView ivBack;
    @BindView(R.id.ivTabs)
    ImageView ivTabs;
    private CommonDatabaseHelper commonDatabaseHelper;
    private ArrayList<LessonData> lessonDataArrayList = new ArrayList<>();

    public static LessonsActivity lessonsActivity;

    public static LessonsActivity getInstance() {
        return lessonsActivity;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lessons);
        ButterKnife.bind(this);
        setContents();
    }

    private void setContents() {
        ButterKnife.bind(this);
        lessonsActivity = this;
        showText(tvTitle, getResources().getString(R.string.lessons));
        setToolbar(toolbar);
        setVisibilityGone(ivMore);
        setVisibilityGone(ivTabs);
        setVisibilityGone(ivSearch);

        commonDatabaseHelper = new CommonDatabaseHelper(LessonsActivity.this);

        // set managerLayout for recyclerView
        rvPrevious.setLayoutManager(new LinearLayoutManager(LessonsActivity.this));
        rvUpcoming.setLayoutManager(new LinearLayoutManager(LessonsActivity.this));

        setData();
    }

    public void setData() {
        //get the category from the local database
        new GetLessonList().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, "");
    }

    @OnClick({R.id.ivMenu, R.id.ivSearch, R.id.ivMore, R.id.fab})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.ivMenu:
                openSlideMenu();
                break;
            case R.id.ivSearch:
                break;
            case R.id.ivMore:
                break;
            case R.id.fab:
                Intent tagIntent = new Intent(LessonsActivity.this, AddLessonsActivity.class);
                startActivity(tagIntent);
                overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
                break;
        }
    }

    private class GetLessonList extends AsyncTask<String, Void, String> {

        @Override
        protected void onPreExecute() {
            Global.showProgressDialog(LessonsActivity.this);
            txtNoDataUpcoming.setText("loading...");
            txtNoDataPrevious.setText("loading...");
        }

        @Override
        protected String doInBackground(String... params) {
            Cursor cursor;
            cursor = commonDatabaseHelper.getLessonList();
            lessonDataArrayList = new ArrayList<>();

            if (cursor != null && cursor.moveToFirst()) {
                while (!cursor.isAfterLast()) {
                    String deleted = cursor.getString(cursor.getColumnIndex(CommonDatabaseHelper.KEY_DELETED));
                    if (deleted == null) {
                        deleted = "";
                    }
                    if (deleted.isEmpty()) {
                        LessonData lessonData = new LessonData();
                        lessonData.setId(cursor.getInt(cursor.getColumnIndex(CommonDatabaseHelper.KEY_ID)));
                        lessonData.setServerid(cursor.getString(cursor.getColumnIndex(CommonDatabaseHelper.KEY_SERVERID)));
                        lessonData.setLessonname(cursor.getString(cursor.getColumnIndex(CommonDatabaseHelper.KEY_LESSONNAME)));
                        lessonData.setLessondate(cursor.getString(cursor.getColumnIndex(CommonDatabaseHelper.KEY_LESSONDATE)));
                        lessonData.setLessondesc(cursor.getString(cursor.getColumnIndex(CommonDatabaseHelper.KEY_LESSONDESC)));
                        lessonData.setDeleted(deleted);
                        lessonData.setModified(cursor.getString(cursor.getColumnIndex(CommonDatabaseHelper.KEY_MODIFIED)));
                        lessonDataArrayList.add(lessonData);
                    }
                    cursor.moveToNext();
                }
            }
            commonDatabaseHelper.close();

            if (cursor != null) {
                cursor.close();
            }
            return "";
        }

        @Override
        protected void onPostExecute(String result) {
            ArrayList<LessonData> upcomingLessonDataArrayList = new ArrayList<>();
            ArrayList<LessonData> previousLessonDataArrayList = new ArrayList<>();
            for (int i = 0; i < lessonDataArrayList.size(); i++) {
                try {
                    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.US); // here set the pattern as you date in string was containing like date/month/year
                    Date date = dateFormat.parse(lessonDataArrayList.get(i).getLessondate());

                    if (new Date().after(date)) {
                        previousLessonDataArrayList.add(lessonDataArrayList.get(i));
                    }
                    if (new Date().before(date)) {
                        upcomingLessonDataArrayList.add(lessonDataArrayList.get(i));
                    }
                    if (new Date().equals(date)) {
                        upcomingLessonDataArrayList.add(lessonDataArrayList.get(i));
                    }

                } catch (ParseException ex) {
                    // handle parsing exception if date string was different from the pattern applying into the SimpleDateFormat contructor
                }
            }
            Collections.sort(upcomingLessonDataArrayList, new Comparator<LessonData>() {
                public int compare(LessonData one, LessonData other) {
                    Date dateOne = null;
                    Date dateOther = null;
                    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.US); // here set the pattern as you date in string was containing like date/month/year
                    try {
                        dateOne = dateFormat.parse(one.getLessondate());
                        dateOther = dateFormat.parse(other.getLessondate());
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                    assert dateOne != null;
                    assert dateOther != null;
                    return dateOther.compareTo(dateOne);
                }
            });
            Collections.sort(previousLessonDataArrayList, new Comparator<LessonData>() {
                public int compare(LessonData one, LessonData other) {
                    Date dateOne = null;
                    Date dateOther = null;
                    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.US); // here set the pattern as you date in string was containing like date/month/year
                    try {
                        dateOne = dateFormat.parse(one.getLessondate());
                        dateOther = dateFormat.parse(other.getLessondate());
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                    assert dateOne != null;
                    assert dateOther != null;
                    return dateOne.compareTo(dateOther);
                }
            });
            Global.dismissProgressDialog();
            txtNoDataUpcoming.setText(getResources().getString(R.string.no_upcoming_lesson_found));
            txtNoDataPrevious.setText(getResources().getString(R.string.no_previous_lesson_found));

            if (upcomingLessonDataArrayList.size() != 0) {
                setVisibilityVisible(rvUpcoming);
                setVisibilityGone(txtNoDataUpcoming);
                LessonListAdapter upcomingLessonListAdapter = new LessonListAdapter(LessonsActivity.this, upcomingLessonDataArrayList, 0);
                rvUpcoming.setAdapter(upcomingLessonListAdapter);
            } else {
                setVisibilityVisible(txtNoDataUpcoming);
                setVisibilityGone(rvUpcoming);
            }

            if (previousLessonDataArrayList.size() != 0) {
                setVisibilityVisible(rvPrevious);
                setVisibilityGone(txtNoDataPrevious);
                LessonListAdapter previoudLessonListAdapter = new LessonListAdapter(LessonsActivity.this, previousLessonDataArrayList, 1);
                rvPrevious.setAdapter(previoudLessonListAdapter);
            } else {
                setVisibilityVisible(txtNoDataPrevious);
                setVisibilityGone(rvPrevious);
            }
        }
    }

    public void upcomingView() {
        setVisibilityVisible(txtNoDataUpcoming);
        setVisibilityGone(rvUpcoming);
    }

    public void previousView() {
        setVisibilityVisible(txtNoDataPrevious);
        setVisibilityGone(rvPrevious);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menuBack:
                finish();
                break;
            case R.id.menuNext:
                Toast.makeText(getApplicationContext(), "No Item Found", Toast.LENGTH_LONG).show();
                break;
            case R.id.menuBookmark:
                Intent bookmarkIntent = new Intent(getApplicationContext(), MainActivity.class);
                bookmarkIntent.putExtra(getString(R.string.menuSelection), "Bookmarks");
                bookmarkIntent.putExtra(getString(R.string.isFromReading), true);
                startActivityWithAnimation(bookmarkIntent);
                break;
            case R.id.menuHistory:
                Intent historyIntent = new Intent(getApplicationContext(), MainActivity.class);
                historyIntent.putExtra(getString(R.string.menuSelection), "History");
                historyIntent.putExtra(getString(R.string.isFromReading), true);
                startActivityWithAnimation(historyIntent);
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}

