package com.ldsscriptures.pro.activity;

import android.app.AlarmManager;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.app.UiModeManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.text.format.DateFormat;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;

import com.ldsscriptures.pro.R;
import com.ldsscriptures.pro.activity.activitymenu.AchievementsDetailsActivity;
import com.ldsscriptures.pro.activity.menuscreens.SlideMenuActivity;
import com.ldsscriptures.pro.database.DataBaseHelper;
import com.ldsscriptures.pro.database.SubDataBaseHelper;
import com.ldsscriptures.pro.model.AchivementsFlagModel;
import com.ldsscriptures.pro.model.DialogAchivement;
import com.ldsscriptures.pro.model.IndexWrapper;
import com.ldsscriptures.pro.model.TimeLine.VerseDataTimeLineModel;
import com.ldsscriptures.pro.utils.Font;
import com.ldsscriptures.pro.utils.Global;
import com.ldsscriptures.pro.utils.PushToServerData;
import com.ldsscriptures.pro.utils.SessionManager;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.TimeUnit;

import permission.auron.com.marshmallowpermissionhelper.ActivityManagePermission;

public class BaseActivity extends ActivityManagePermission {

    SessionManager sessionManager;
    ProgressDialog progressDoalog;
    String currant_date;
    ArrayList<AchivementsFlagModel> achivementsFlagarraylist = new ArrayList<>();
    private DialogAchivement dialogachivement;
    AlarmManager alarmManager;
    static BaseActivity baseActivity;
    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;
    private static final String PREF_NAME = "LDS_Session";
    int PRIVATE_MODE = 0;
    Dialog achivementDialog;
    DataBaseHelper databasehelper;
    VerseDataTimeLineModel verseDataTimeLineModel = new VerseDataTimeLineModel();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        sessionManager = new SessionManager(this);
        databasehelper = new DataBaseHelper(this);
        currant_date = new SimpleDateFormat("dd/MM/yyyy").format(Calendar.getInstance().getTime());

        UiModeManager uiManager = (UiModeManager) getSystemService(Context.UI_MODE_SERVICE);

        if (sessionManager.isNightMode()) {
            uiManager.setNightMode(UiModeManager.MODE_NIGHT_YES);
        } else {
            uiManager.setNightMode(UiModeManager.MODE_NIGHT_NO);
        }
        if (achivementsFlagarraylist != null) {
            achivementsFlagarraylist = sessionManager.getAchivementsFlagArrayList();
        }

        alarmManager = (AlarmManager) getSystemService(ALARM_SERVICE);

        progressDoalog = new ProgressDialog(BaseActivity.this);
        progressDoalog.setMessage("Loading...");
        progressDoalog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDoalog.setCancelable(false);

        baseActivity = this;
        sharedPreferences = getApplicationContext().getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        editor = sharedPreferences.edit();

    }

    public void showprogress() {
        try {
            if (!isFinishing() && !progressDoalog.isShowing()) {
                progressDoalog.show();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void dismissprogress() {
        if (progressDoalog != null) {
            if (progressDoalog.isShowing())
                progressDoalog.dismiss();
        }
    }

    void setFont() {
        try {
            ViewGroup group = (ViewGroup) getWindow().getDecorView().findViewById(android.R.id.content);
            Font.setAllTextView(this, group, getString(R.string.font_roboto_regular));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    void setBackground(int image, ImageView imageView_background) {
        Global.SetBackground(this, imageView_background, image);
    }

    public void setToolbar(Toolbar toolbar) {
        setSupportActionBar(toolbar);
        toolbar.hideOverflowMenu();
    }

    public void showText(TextView tvTitle, String sTitle) {
        tvTitle.setText(sTitle);
    }

    public void setVisibilityVisible(View ivBack) {
        ivBack.setVisibility(View.VISIBLE);
    }

    public void setVisibilityGone(View ivBack) {
        ivBack.setVisibility(View.GONE);
    }

    public String getUserInfo(String getStr) {
        String string = null;
        if (sessionManager.isLoggedIn()) {
            HashMap<String, String> hashMap = sessionManager.getLoginDetails();
            string = hashMap.get(getStr);
        }
        return string;
    }

    public void openSlideMenu() {
        startActivity(new Intent(this, SlideMenuActivity.class));
        overridePendingTransition(R.anim.left_to_right, android.R.anim.fade_out);
    }

    public void manageFloatingBtn() {
        startActivityWithAnimation(new Intent(this, DashboardSetGoalActivity.class));
    }

    public void startActivityWithAnimation(Intent intent) {
        startActivity(intent);
        overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
    }

    @Override
    protected void onResume() {
        super.onResume();

        sessionManager.setLevelArrayList();
        sessionManager.setDailyDate(currant_date);
        sessionManager.setGoalHit();
        if (achivementsFlagarraylist == null) {
            sessionManager.setAchivementsFlagArrayList();
        } else {
            getAchivementsPointsCategory1();
            getAchivementsPointsCategory2();
            getAchivementsPointsCategory3();
            getAchivementsPointsCategory4();
            getAchivementsPointsCategory5();
            getAchivementsPointsCategory6();
            getAchivementsPointsCategory7();
            getAchivementsPointsCategory8();

            openAchivementDialog();
        }

        getAchivementsDaysCalculation();

    }


    public static long getDateDiff(SimpleDateFormat format, String oldDate, String newDate) {
        try {
            if (oldDate.equals("")) {
                oldDate = newDate;
            }
            return TimeUnit.DAYS.convert(format.parse(newDate).getTime() - format.parse(oldDate).getTime(), TimeUnit.MILLISECONDS);
        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        }
    }

    public void getAchivementsDaysCalculation() {

        String currantDate = currant_date;
        String lastDate = sessionManager.getLastDate();
        int goalHit = Integer.parseInt(sessionManager.getGoalHit());

        int dateDifference = (int) getDateDiff(new SimpleDateFormat("dd/MM/yyyy"), lastDate, currantDate);

        if (dateDifference == 1) {
            sessionManager.setDateCounter(1);

            if (goalHit == 100) {
                sessionManager.setDateCounterCategory2(1);
            }
        } else {
            if (!currantDate.equals(lastDate)) {
                sessionManager.setResetDateCounter(0);
            }
        }
        sessionManager.setLastDate(currantDate);
    }

    public void getAchivementsPointsCategory1() {
        int dayCounter = sessionManager.getDateCounter();

        switch (dayCounter) {
            case 3:
                checkAchivementFlagCategory(101, 100, 0, true);
                break;
            case 5:
                checkAchivementFlagCategory(102, 200, 0, true);
                break;
            case 7:
                checkAchivementFlagCategory(103, 300, 0, true);
                break;
            case 10:
                checkAchivementFlagCategory(104, 400, 0, true);
                break;
            case 14:
                checkAchivementFlagCategory(105, 500, sessionManager.IS_ACHIVEMENTS_LEVEL_2, true);
                break;
            case 30:
                checkAchivementFlagCategory(106, 1000, sessionManager.IS_ACHIVEMENTS_LEVEL_3, false);
                break;
            case 100:
                checkAchivementFlagCategory(107, 1000, sessionManager.IS_ACHIVEMENTS_LEVEL_5, false);
                break;
            case 365:
                checkAchivementFlagCategory(108, 2000, sessionManager.IS_ACHIVEMENTS_LEVEL_7, false);
                break;
        }
    }

    public void getAchivementsPointsCategory2() {

        String currantDate = currant_date;
        String lastDate = sessionManager.getLastDate();
        int goalHit = Integer.parseInt(sessionManager.getGoalHit());
        int dateDifference = (int) getDateDiff(new SimpleDateFormat("dd/MM/yyyy"), lastDate, currantDate);
        int dayCounter = sessionManager.getDateCounterCategory2();

        if (dateDifference == 0) {
            switch (goalHit) {
                case 100:
                    checkAchivementFlagCategory(201, 50, 0, true);
                    break;
                case 150:
                    checkAchivementFlagCategory(202, 100, 0, true);
                    break;
                case 200:
                    checkAchivementFlagCategory(203, 200, sessionManager.IS_ACHIVEMENTS_LEVEL_5, true);
                    break;
                case 300:
                    checkAchivementFlagCategory(204, 300, sessionManager.IS_ACHIVEMENTS_LEVEL_8, false);
                    break;
                case 400:
                    checkAchivementFlagCategory(205, 400, sessionManager.IS_ACHIVEMENTS_LEVEL_10, false);
                    break;
                case 500:
                    checkAchivementFlagCategory(206, 500, sessionManager.IS_ACHIVEMENTS_LEVEL_15, false);
                    break;
            }
        }

        switch (dayCounter) {
            case 3:
                checkAchivementFlagCategory(2011, 100, 0, true);
                break;
            case 7:
                checkAchivementFlagCategory(2021, 200, sessionManager.IS_ACHIVEMENTS_LEVEL_5, false);
                break;
            case 14:
                checkAchivementFlagCategory(2031, 400, sessionManager.IS_ACHIVEMENTS_LEVEL_7, false);
                break;
            case 50:
                checkAchivementFlagCategory(2041, 1000, sessionManager.IS_ACHIVEMENTS_LEVEL_10, false);
                break;
            case 100:
                checkAchivementFlagCategory(2051, 2000, sessionManager.IS_ACHIVEMENTS_LEVEL_15, false);
                break;
            case 365:
                checkAchivementFlagCategory(2061, 5000, sessionManager.IS_ACHIVEMENTS_LEVEL_20, false);
                break;
        }
    }

    public void getAchivementsPointsCategory3() {

        int dayCounter = sessionManager.getDateCounterCategory2();

        switch (dayCounter) {
            case 3:
                checkAchivementFlagCategory(2011, 100, 0, true);
                break;
            case 7:
                checkAchivementFlagCategory(2021, 200, sessionManager.IS_ACHIVEMENTS_LEVEL_5, false);
                break;
            case 14:
                checkAchivementFlagCategory(2031, 400, sessionManager.IS_ACHIVEMENTS_LEVEL_7, false);
                break;
            case 50:
                checkAchivementFlagCategory(2041, 1000, sessionManager.IS_ACHIVEMENTS_LEVEL_10, false);
                break;
            case 100:
                checkAchivementFlagCategory(2051, 2000, sessionManager.IS_ACHIVEMENTS_LEVEL_15, false);
                break;
            case 365:
                checkAchivementFlagCategory(2061, 5000, sessionManager.IS_ACHIVEMENTS_LEVEL_20, false);
                break;
        }
    }

    public void getAchivementsPointsCategory4() {

        String currantDate = currant_date;
        String lastDate = sessionManager.getLastDate();
        int UserDailyPoints = sessionManager.getUserDailyPoints();
        int dateDifference = (int) getDateDiff(new SimpleDateFormat("dd/MM/yyyy"), lastDate, currantDate);

        if (dateDifference == 0) {
            switch (UserDailyPoints) {
                case 5000:
                    checkAchivementFlagCategory(301, 500, 0, true);
                    break;
                case 6000:
                    checkAchivementFlagCategory(302, 600, 0, true);
                    break;
                case 7000:
                    checkAchivementFlagCategory(303, 700, sessionManager.IS_ACHIVEMENTS_LEVEL_3, true);
                    break;
                case 10000:
                    checkAchivementFlagCategory(304, 1000, sessionManager.IS_ACHIVEMENTS_LEVEL_5, false);
                    break;
                case 15000:
                    checkAchivementFlagCategory(305, 1500, sessionManager.IS_ACHIVEMENTS_LEVEL_5, false);
                    break;
                case 20000:
                    checkAchivementFlagCategory(306, 2000, sessionManager.IS_ACHIVEMENTS_LEVEL_10, false);
                    break;
                case 30000:
                    checkAchivementFlagCategory(307, 3000, sessionManager.IS_ACHIVEMENTS_LEVEL_10, false);
                    break;
                case 40000:
                    checkAchivementFlagCategory(308, 4000, sessionManager.IS_ACHIVEMENTS_LEVEL_15, false);
                    break;
                case 50000:
                    checkAchivementFlagCategory(309, 5000, sessionManager.IS_ACHIVEMENTS_LEVEL_15, false);
                    break;
                case 75000:
                    checkAchivementFlagCategory(3010, 7500, sessionManager.IS_ACHIVEMENTS_LEVEL_20, false);
                    break;

            }
        }
    }

    public void getAchivementsPointsCategory5() {

        String currantDate = currant_date;
        String lastDate = sessionManager.getLastDate();
        int goalHit = Integer.parseInt(sessionManager.getGoalHit());
        int dateDifference = (int) getDateDiff(new SimpleDateFormat("dd/MM/yyyy"), lastDate, currantDate);

        try {
            SimpleDateFormat gsdf = new SimpleDateFormat("dd/MM/yyyy");
            Date dt1 = gsdf.parse(currantDate);
            String weekStartString = (String) DateFormat.format("MMMM dd", dt1);

            if (dateDifference == 0) {

                if (goalHit >= 100) {

                    for (int i = 0; i < achivementsFlagarraylist.size(); i++) {

                        int achivement_id = achivementsFlagarraylist.get(i).getAchivements_id();

                        if (achivementsFlagarraylist.get(i).getAchivements_date().equals(weekStartString)) {
                            checkAchivementFlagCategory(achivement_id, 500, 0, true);
                        }

                    }

                }
            }

        } catch (ParseException e) {
            e.printStackTrace();
        }


    }

    public void getAchivementsPointsCategory6() {

        String currantDate = currant_date;
        String lastDate = sessionManager.getLastDate();
        int hightLightCount = sessionManager.getHightLightCount();
        int dateDifference = (int) getDateDiff(new SimpleDateFormat("dd/MM/yyyy"), lastDate, currantDate);

        if (dateDifference == 0) {

            switch (hightLightCount) {
                case 100:
                    checkAchivementFlagCategory(409, 100, 0, true);
                    break;
                case 500:
                    checkAchivementFlagCategory(4010, 200, 0, true);
                    break;
                case 1000:
                    checkAchivementFlagCategory(4011, 300, 0, true);
                    break;
            }
        }
    }

    public void getAchivementsPointsCategory7() {

        String currantDate = currant_date;
        String lastDate = sessionManager.getLastDate();
        int tagCount = sessionManager.getTagCount();
        int dateDifference = (int) getDateDiff(new SimpleDateFormat("dd/MM/yyyy"), lastDate, currantDate);

        if (dateDifference == 0) {

            switch (tagCount) {
                case 100:
                    checkAchivementFlagCategory(4012, 100, 0, true);
                    break;
                case 500:
                    checkAchivementFlagCategory(4013, 200, 0, true);
                    break;
                case 1000:
                    checkAchivementFlagCategory(4014, 300, 0, true);
                    break;
            }
        }
    }

    public void getAchivementsPointsCategory8() {

        String currantDate = currant_date;
        String lastDate = sessionManager.getLastDate();
        int tagCount = sessionManager.getBookmarkCount();
        int dateDifference = (int) getDateDiff(new SimpleDateFormat("dd/MM/yyyy"), lastDate, currantDate);

        if (dateDifference == 0) {

            switch (tagCount) {
                case 100:
                    checkAchivementFlagCategory(4015, 100, 0, true);
                    break;
                case 500:
                    checkAchivementFlagCategory(4016, 200, 0, true);
                    break;
                case 1000:
                    checkAchivementFlagCategory(4017, 300, 0, true);
                    break;
            }
        }
    }

    public void checkAchivementFlagCategory(int id, int userPoint, int achivementsLevel, boolean onetime) {

        for (int i = 0; i < achivementsFlagarraylist.size(); i++) {
            int achivementId = achivementsFlagarraylist.get(i).getAchivements_id();

            if (achivementId == id) {
                Boolean acFlag = achivementsFlagarraylist.get(i).getAchivements_flag();
                if (onetime == true) {
                    if (acFlag == false) {
                        setAchivementPointCatecory(i, userPoint, achivementsLevel);
                    }
                } else {
                    setAchivementPointCatecory(i, userPoint, achivementsLevel);
                }

            }
        }
        sessionManager.saveAchivementsFlagArraylist(achivementsFlagarraylist);
    }

    public void setAchivementPointCatecory(int pos, int userPoint, int achivementsLevel) {
        if (achivementsLevel == 0) {
            achivementsFlagarraylist.get(pos).setAchivements_flag(true);
            sessionManager.setUserDailyPoints(userPoint);
        } else {
            int dailyPoint = sessionManager.getUserDailyPoints();
            if (dailyPoint >= achivementsLevel) {
                achivementsFlagarraylist.get(pos).setAchivements_flag(true);
                sessionManager.setUserDailyPoints(userPoint);
            }
        }
    }

    public void openAchivementDialog() {

        for (int i = 0; i < achivementsFlagarraylist.size(); i++) {

            final Boolean achivementFlag = achivementsFlagarraylist.get(i).getAchivements_flag();
            final Boolean achivementdialogFlag = achivementsFlagarraylist.get(i).getAchivements_dialogflag();
            final int achivementId = achivementsFlagarraylist.get(i).getAchivements_id();
            final String serverID = achivementsFlagarraylist.get(i).getServerid();
            final int pos = i;

            if (achivementFlag && !achivementdialogFlag) {

                achivementDialog = new Dialog(BaseActivity.this);
                achivementDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                achivementDialog.setContentView(R.layout.activity_achievements_dialog);

                final ImageView imageGetAchivement1 = (ImageView) achivementDialog.findViewById(R.id.imageGetAchivement1);
                final TextView txtGetAchivement1 = (TextView) achivementDialog.findViewById(R.id.txtGetAchivement1);
                final TextView txtviewAchivement = (TextView) achivementDialog.findViewById(R.id.txtviewAchivement);
                final TextView txtdone1Achivement = (TextView) achivementDialog.findViewById(R.id.txtdone1Achivement);

                setAchivementsImages(achivementId, imageGetAchivement1, txtGetAchivement1);
                achivementDialog.show();

                txtdone1Achivement.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        achivementsFlagarraylist.get(pos).setAchivements_dialogflag(true);
                        sessionManager.saveAchivementsFlagArraylist(achivementsFlagarraylist);
                        achivementDialog.dismiss();

                        new PushToServerData(BaseActivity.this);
                    }
                });

                txtviewAchivement.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        viewDialogClick(pos);
                        achivementDialog.dismiss();
                    }
                });

            }
        }
    }

    public void setAchivementsImages(int achivements_id, ImageView achievements_images, TextView achievements_titles) {
        switch (achivements_id) {
            case 101:
                checkAchivementFlag(1, achivements_id, achievements_images, R.drawable.oval_4, achievements_titles, getString(R.string.three_days_streak));
                break;
            case 102:
                checkAchivementFlag(1, achivements_id, achievements_images, R.drawable.rectangle_4_18, achievements_titles, getString(R.string.five_days_streak));
                break;
            case 103:
                checkAchivementFlag(1, achivements_id, achievements_images, R.drawable.polygon_15, achievements_titles, getString(R.string.seven_days_streak));
                break;
            case 104:
                checkAchivementFlag(1, achivements_id, achievements_images, R.drawable.polygon_14, achievements_titles, getString(R.string.ten_days_streak));
                break;
            case 105:
                checkAchivementFlag(1, achivements_id, achievements_images, R.drawable.oval_3, achievements_titles, getString(R.string.fortnightstreak));
                break;
            case 106:
                checkAchivementFlag(1, achivements_id, achievements_images, R.drawable.rectangle_4_17, achievements_titles, getString(R.string.monthStreak));
                break;
            case 107:
                checkAchivementFlag(1, achivements_id, achievements_images, R.drawable.polygon_13, achievements_titles, getString(R.string.centuryStreak));
                break;
            case 108:
                checkAchivementFlag(1, achivements_id, achievements_images, R.drawable.polygon_12, achievements_titles, getString(R.string.yearStreak));
                break;
            case 201:
                checkAchivementFlag(2, achivements_id, achievements_images, R.drawable.rectangle_4_16, achievements_titles, getString(R.string.hundred_perecent_goal_hit));
                break;
            case 202:
                checkAchivementFlag(2, achivements_id, achievements_images, R.drawable.polygon_11, achievements_titles, getString(R.string.one_hundred_fifty_goal_hit));
                break;
            case 203:
                checkAchivementFlag(2, achivements_id, achievements_images, R.drawable.polygon_10, achievements_titles, getString(R.string.goalHit200));
                break;
            case 204:
                checkAchivementFlag(2, achivements_id, achievements_images, R.drawable.rectangle_4_15, achievements_titles, getString(R.string.goalHit300));
                break;
            case 205:
                checkAchivementFlag(2, achivements_id, achievements_images, R.drawable.polygon_9, achievements_titles, getString(R.string.goalHit400));
                break;
            case 206:
                checkAchivementFlag(2, achivements_id, achievements_images, R.drawable.polygon_8, achievements_titles, getString(R.string.goalHit500));
                break;
            case 2011:
                checkAchivementFlag(2, achivements_id, achievements_images, R.drawable.rectangle_4_14, achievements_titles, getString(R.string.daysGoalStreak3));
                break;
            case 2021:
                checkAchivementFlag(2, achivements_id, achievements_images, R.drawable.polygon_7, achievements_titles, getString(R.string.daysGoalStreak7));
                break;
            case 2031:
                checkAchivementFlag(2, achivements_id, achievements_images, R.drawable.polygon_6, achievements_titles, getString(R.string.fortnightGoalStreak));
                break;
            case 2041:
                checkAchivementFlag(2, achivements_id, achievements_images, R.drawable.rectangle_4_13, achievements_titles, getString(R.string.halfCenturyGoalStreak));
                break;
            case 2051:
                checkAchivementFlag(2, achivements_id, achievements_images, R.drawable.polygon_5, achievements_titles, getString(R.string.centuryGoalStreak));
                break;
            case 2061:
                checkAchivementFlag(2, achivements_id, achievements_images, R.drawable.polygon_14, achievements_titles, getString(R.string.yearGoalStreak));
                break;
            case 301:
                checkAchivementFlag(3, achivements_id, achievements_images, R.drawable.rectangle_4_12, achievements_titles, getString(R.string.oneDay5k));
                break;
            case 302:
                checkAchivementFlag(3, achivements_id, achievements_images, R.drawable.rectangle_4_11, achievements_titles, getString(R.string.oneDay6k));
                break;
            case 303:
                checkAchivementFlag(3, achivements_id, achievements_images, R.drawable.rectangle_4_10, achievements_titles, getString(R.string.oneDay7k));
                break;
            case 304:
                checkAchivementFlag(3, achivements_id, achievements_images, R.drawable.rectangle_4_9, achievements_titles, getString(R.string.oneDay10k));
                break;
            case 305:
                checkAchivementFlag(3, achivements_id, achievements_images, R.drawable.rectangle_4_8, achievements_titles, getString(R.string.oneDay15k));
                break;
            case 306:
                checkAchivementFlag(3, achivements_id, achievements_images, R.drawable.rectangle_4_7, achievements_titles, getString(R.string.oneDay20k));
                break;
            case 307:
                checkAchivementFlag(3, achivements_id, achievements_images, R.drawable.rectangle_4_6, achievements_titles, getString(R.string.oneDay30k));
                break;
            case 308:
                checkAchivementFlag(3, achivements_id, achievements_images, R.drawable.rectangle_4_5, achievements_titles, getString(R.string.oneDay40k));
                break;
            case 309:
                checkAchivementFlag(3, achivements_id, achievements_images, R.drawable.rectangle_4_4, achievements_titles, getString(R.string.oneDay50k));
                break;
            case 3010:
                checkAchivementFlag(3, achivements_id, achievements_images, R.drawable.rectangle_4_3, achievements_titles, getString(R.string.oneDay75k));
                break;
            case 401:
                checkAchivementFlag(4, achivements_id, achievements_images, R.drawable.ac_31_s, achievements_titles, getString(R.string.hidden_11));
                break;
            case 402:
                checkAchivementFlag(4, achivements_id, achievements_images, R.drawable.ac_32_s, achievements_titles, getString(R.string.hidden_214));
                break;
            case 403:
                checkAchivementFlag(4, achivements_id, achievements_images, R.drawable.ac_33_s, achievements_titles, getString(R.string.hidden_46));
                break;
            case 404:
                checkAchivementFlag(4, achivements_id, achievements_images, R.drawable.ac_34_s, achievements_titles, getString(R.string.hidden_515));
                break;
            case 405:
                checkAchivementFlag(4, achivements_id, achievements_images, R.drawable.ac_35_s, achievements_titles, getString(R.string.hidden_724));
                break;
            case 406:
                checkAchivementFlag(4, achivements_id, achievements_images, R.drawable.ac_36_s, achievements_titles, getString(R.string.hidden_922));
                break;
            case 407:
                checkAchivementFlag(4, achivements_id, achievements_images, R.drawable.ac_37_s, achievements_titles, getString(R.string.hidden_1223));
                break;
            case 408:
                checkAchivementFlag(4, achivements_id, achievements_images, R.drawable.ac_38_s, achievements_titles, getString(R.string.hidden_1225));
                break;
            case 409:
                checkAchivementFlag(4, achivements_id, achievements_images, R.drawable.ac_39_s, achievements_titles, getString(R.string.hidden_100h));
                break;
            case 4010:
                checkAchivementFlag(4, achivements_id, achievements_images, R.drawable.ac_40_s, achievements_titles, getString(R.string.hidden_500h));
                break;
            case 4011:
                checkAchivementFlag(4, achivements_id, achievements_images, R.drawable.ac_41_s, achievements_titles, getString(R.string.hidden_1000h));
                break;
            case 4012:
                checkAchivementFlag(4, achivements_id, achievements_images, R.drawable.ac_42_s, achievements_titles, getString(R.string.hidden_100t));
                break;
            case 4013:
                checkAchivementFlag(4, achivements_id, achievements_images, R.drawable.ac_43_s, achievements_titles, getString(R.string.hidden_500t));
                break;
            case 4014:
                checkAchivementFlag(4, achivements_id, achievements_images, R.drawable.ac_44_s, achievements_titles, getString(R.string.hidden_1000t));
                break;
            case 4015:
                checkAchivementFlag(4, achivements_id, achievements_images, R.drawable.ac_45_s, achievements_titles, getString(R.string.hidden_100b));
                break;
            case 4016:
                checkAchivementFlag(4, achivements_id, achievements_images, R.drawable.ac_46_s, achievements_titles, getString(R.string.hidden_500b));
                break;
            case 4017:
                checkAchivementFlag(4, achivements_id, achievements_images, R.drawable.ac_47_s, achievements_titles, getString(R.string.hidden_1000b));
                break;
        }
    }

    public void checkAchivementFlag(final int flag, int id, ImageView achivementsImage, final int drwable, TextView achievements_titles, final String varTitle) {
        for (int i = 0; i < achivementsFlagarraylist.size(); i++) {

            int achivementId = achivementsFlagarraylist.get(i).getAchivements_id();
            if (achivementId == id) {
                Boolean acFlag = achivementsFlagarraylist.get(i).getAchivements_flag();
                if (acFlag == true) {
                    achivementsImage.setImageResource(drwable);
                    achievements_titles.setText(varTitle);

                    dialogachivement = new DialogAchivement();
                    dialogachivement.setDrwable(drwable);
                    dialogachivement.setVarTitle(achievements_titles.getText().toString());
                    dialogachivement.setFlag(flag);
                }
            }
        }
    }

    public void viewDialogClick(int pos) {
        Intent i = new Intent(BaseActivity.this, AchievementsDetailsActivity.class);
        i.putExtra("achievementsDetailsActivity", 0);
        i.putExtra("achievementsImage", dialogachivement.getDrwable());
        i.putExtra("achievementsTitle", dialogachivement.getVarTitle());
        i.putExtra("achievementsflag", dialogachivement.getFlag());
        startActivity(i);

        achivementsFlagarraylist.get(pos).setAchivements_dialogflag(true);
        sessionManager.saveAchivementsFlagArraylist(achivementsFlagarraylist);

    }

    public static BaseActivity getInstance() {
        return baseActivity;
    }

    public void day_strike(TextView dayStrike, TextView txtActivityReadingMinute, TextView txtActivityDailyGoalpts, TextView txtAvgGoalPoint, TextView txtview_point, String varpoint, int arrayListSize, TextView txtActivityAchivementMinute) {
        String currentDateTimeString = new SimpleDateFormat("dd/MM/yyyy").format(Calendar.getInstance().getTime());

        String date1 = currentDateTimeString;
        String date2 = sharedPreferences.getString(sessionManager.getLastDate(), null);
        int dateDifference = (int) getDateDiff(new SimpleDateFormat("dd/MM/yyyy"), date2, date1);
        editor.putString(sessionManager.KEY_USER_DAY_STREAK, String.valueOf(dateDifference));
        editor.commit();
        dayStrike.setText(sharedPreferences.getString(sessionManager.KEY_USER_DAY_STREAK, null));

        int x1 = Integer.parseInt(sessionManager.getUserMinReading());
        // set reading minute
        if (sessionManager.getUserMinReading() != null) {
            if (x1 >= 10000) {
                int point = (int) Math.round((x1 / 1000));
                DecimalFormat form = new DecimalFormat("#,##");
                txtActivityReadingMinute.setText(form.format(point) + "k");

            } else {
                txtActivityReadingMinute.setText(sessionManager.getUserMinReading());
            }
        }

        if (sessionManager.getUserGoal() != null)
            txtActivityDailyGoalpts.setText(sessionManager.getUserGoal() + " " + "PTS");

        int x = Integer.valueOf(varpoint);
        double avgPts = Math.ceil(x / arrayListSize);

        if (x >= 10000) {
            int point = (int) Math.round((x / 1000));
            DecimalFormat form = new DecimalFormat("#,##");
            txtview_point.setText(form.format(point) + "k");

        } else {
            txtview_point.setText(varpoint);
        }

        int x2 = (int) avgPts;

        if (avgPts >= 10000) {
            txtAvgGoalPoint.setText(String.valueOf(x2) + "k" + " " + "PTS");
        } else {
            txtAvgGoalPoint.setText(String.valueOf(x2) + " " + "PTS");
        }

        if (sessionManager.getAchivementCount() != null) {
            txtActivityAchivementMinute.setText(sessionManager.getAchivementCount());
        }
    }

    public VerseDataTimeLineModel getVerseText(String fullVerseUri, String var_timeline_Verse_Id) {

        Cursor cursor;
        String LcId = "";
        try {

            String[] URI_TXT = fullVerseUri.split("\\/");
            String URI_TXT_First = URI_TXT[1];
            String URI_TXT_Second = URI_TXT[2];

            cursor = databasehelper.getVerseTextAndVersePath(URI_TXT[1]);

            if (cursor != null && cursor.moveToFirst()) {

                while (!cursor.isAfterLast()) {
                    LcId = cursor.getString(cursor.getColumnIndex(DataBaseHelper.KEY_ID));
                    cursor.moveToNext();
                }
            }
            if (cursor != null) {
                cursor.close();
            }

            String URI_TXT_First1 = URI_TXT_First.substring(0, 1).toUpperCase() + URI_TXT_First.substring(1);


            verseDataTimeLineModel = getLsDataFromDb(LcId, URI_TXT_Second, fullVerseUri, var_timeline_Verse_Id, URI_TXT_First1);
            verseDataTimeLineModel.setLcId(LcId);


        } catch (Exception e) {
            e.printStackTrace();
        }
        return verseDataTimeLineModel;
    }

    public VerseDataTimeLineModel getLsDataFromDb(String LcId, String verseUri, String fullVerseUri, String var_timeline_Verse_Id, String URI_TXT_First) {

        Cursor cursor = null;
        String LsItemId = "";
        String LsItemTitle = "";

        if (LcId.isEmpty()) {
            int index = fullVerseUri.lastIndexOf('/');
            cursor = databasehelper.getVerseLSPathEmpty(fullVerseUri.substring(0, index));
        } else {
            cursor = databasehelper.getVerseLSPath(Integer.parseInt(LcId), verseUri);
        }

        if (cursor != null && cursor.moveToFirst()) {
            while (!cursor.isAfterLast()) {
                LsItemId = cursor.getString(cursor.getColumnIndex(DataBaseHelper.KEY_LS_RESULT_ITEM_ID));
                LsItemTitle = cursor.getString(cursor.getColumnIndex(DataBaseHelper.KEY_LS_RESULT_TITLE_HTML));

                URI_TXT_First = URI_TXT_First + " / " + LsItemTitle;
                verseDataTimeLineModel = getVerseIndexDataFromDB(LsItemId, fullVerseUri, var_timeline_Verse_Id, URI_TXT_First);
                verseDataTimeLineModel.setLsId(LsItemId);
                cursor.moveToNext();
            }
        }

        return verseDataTimeLineModel;
    }

    public VerseDataTimeLineModel getVerseIndexDataFromDB(String LsItemId, String fullVerseUri, String var_timeline_Verse_Id, String URI_TXT_First) {
        SubDataBaseHelper subDataBaseHelper;
        subDataBaseHelper = new SubDataBaseHelper(this, LsItemId);

        Cursor cursor;
        cursor = subDataBaseHelper.getVerseTextFromURI(fullVerseUri);

        if (cursor != null && cursor.moveToFirst()) {
            while (!cursor.isAfterLast()) {

                String LsHrmlTextItem = cursor.getString(cursor.getColumnIndex(DataBaseHelper.KEY_LS_RESULT_TITLE_HTML));
                byte[] blob = cursor.getBlob(cursor.getColumnIndex(SubDataBaseHelper.KEY_SUB_ITEM_HTML_C1CONTENT_HTML));
                String idForReading = cursor.getString(cursor.getColumnIndex(SubDataBaseHelper.KEY_ID));
                String postionForReading = cursor.getString(cursor.getColumnIndex(SubDataBaseHelper.KEY_FT_POSITION));

                String newHtmlVerseArray = new String(blob);

                verseDataTimeLineModel = getCrossRefSubItemHtmlData(newHtmlVerseArray, var_timeline_Verse_Id);
                verseDataTimeLineModel.setVersePath(LsHrmlTextItem);
                verseDataTimeLineModel.setVerseFullPath(URI_TXT_First + " / " + LsHrmlTextItem);
                verseDataTimeLineModel.setIndexForReading(idForReading);

                cursor.moveToNext();
            }
        }
        subDataBaseHelper.close();
        if (cursor != null) {
            cursor.close();
        }
        return verseDataTimeLineModel;
    }

    public VerseDataTimeLineModel getCrossRefSubItemHtmlData(String newHtmlVerseArray, String verse_Id) {
        String verseText = "";
        ArrayList<String> selectedArrayList = new ArrayList<>();
        Document doc = Jsoup.parse(newHtmlVerseArray);
        Elements tvVersemain = doc.select("div[class^=body-block]");
        Elements tvVerse = tvVersemain.select("p[class^=verse]");

        if (!tvVerse.isEmpty() && tvVerse != null && !tvVerse.equals("null")) {

            Element element2 = tvVerse.get(Integer.parseInt(verse_Id) - 1);
            Elements tvVerseSpan = element2.select("span[class^=study-note-ref]");
            for (int j = 0; j < tvVerseSpan.size(); j++) {
                String e2 = tvVerseSpan.get(j).text();
                selectedArrayList.add(e2);
            }

            List<IndexWrapper> indexWrapperList = Global.findIndexesForKeyword(element2.outerHtml(), "data-aid");
            String[] dataAid = new String[0];
            for (int k = 0; k < indexWrapperList.size(); k++) {
                String substring = element2.outerHtml().substring(indexWrapperList.get(k).getEnd() + 2);
                dataAid = substring.split("\"");
            }
            verseText = Html.fromHtml(element2.outerHtml()).toString();
            for (int i = 0; i < selectedArrayList.size(); i++) {
                verseText = verseText.replace(selectedArrayList.get(i), selectedArrayList.get(i).substring(1));
            }
            verseText = verseText.replace(verseText, verseText.substring(verse_Id.length()).trim());
            verseDataTimeLineModel.setVereseText(verseText);
            verseDataTimeLineModel.setVerseAid(dataAid[0]);
        } else {

            Elements tvVerse1 = tvVersemain.select("p");

            for (int i = 0; i < tvVerse1.size(); i++) {

                String pid = tvVerse1.get(i).select("p").attr("id");
                verseText = tvVerse1.get(i).outerHtml().toString();
                String verseAid = tvVerse1.get(i).attr("data-aid");
                String verseID = "";

                if (pid.startsWith("p")) {
                    verseID = pid.substring(1);
                }

                if (verseID.equals(verse_Id)) {

                    verseDataTimeLineModel.setVereseText(String.valueOf(Html.fromHtml(verseText)));
                    verseDataTimeLineModel.setVerseAid(verseAid);
                }
            }
        }

        return verseDataTimeLineModel;
    }
}


