package com.ldsscriptures.pro.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.ldsscriptures.pro.R;
import com.ldsscriptures.pro.adapter.SearchVerseAdapter;
import com.ldsscriptures.pro.model.SerachData;
import com.ldsscriptures.pro.utils.DataHolder;

import java.lang.reflect.Type;
import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class SearchSubActivity extends BaseActivity {

    @BindView(R.id.ivBack)
    ImageView ivBack;
    @BindView(R.id.searchView)
    SearchView searchView;
    @BindView(R.id.ivMore)
    ImageView ivMore;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.layout_toolbar_serach1)
    LinearLayout layoutToolbarSerach1;
    @BindView(R.id.Search_recyclerView)
    RecyclerView SearchRecyclerView;
    @BindView(R.id.textsubtitleserach)
    TextView textsubtitleserach;
    @BindView(R.id.fab)
    FloatingActionButton fab;

    static SearchSubActivity searchSubActivity;
    SearchVerseAdapter searchLCAdapter;
    private ArrayList<SerachData> lcDataArrayListString = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);
        ButterKnife.bind(this);
        setContents();
    }

    private void setContents() {
        ButterKnife.bind(this);

        setToolbar(toolbar);

        searchSubActivity = this;
        SearchRecyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        SearchRecyclerView.setLayoutManager(layoutManager);


        String lstitle = getIntent().getStringExtra("lstitle");
        String serachQuery = getIntent().getStringExtra("serachQuery");

        if (DataHolder.hasData1()) {
            lcDataArrayListString = DataHolder.getData1();
        }

        textsubtitleserach.setVisibility(View.VISIBLE);
        searchView.setVisibility(View.GONE);
        SearchRecyclerView.setVisibility(View.VISIBLE);

        textsubtitleserach.setText(lstitle);

        searchLCAdapter = new SearchVerseAdapter(SearchSubActivity.this, lcDataArrayListString);
        SearchRecyclerView.setAdapter(searchLCAdapter);
        searchLCAdapter.filter(serachQuery, lstitle);
    }

    public ArrayList<SerachData> convertArrayListToString(String content) {
        Gson gson = new Gson();
        Type type = new TypeToken<ArrayList<SerachData>>() {
        }.getType();
        ArrayList<SerachData> arrayList = gson.fromJson(content, type);
        return arrayList;
    }

    public static SearchSubActivity getInstance() {
        return searchSubActivity;
    }

    @OnClick(R.id.ivBack)
    public void onViewClicked() {
        finish();
    }

    @OnClick(R.id.fab)
    public void manageFloatingButton() {
        manageFloatingBtn();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menuBack:
                finish();
                break;
            case R.id.menuNext:
                Toast.makeText(getApplicationContext(), "No Item Found", Toast.LENGTH_LONG).show();
                break;
            case R.id.menuBookmark:
                Intent bookmarkIntent = new Intent(getApplicationContext(), MainActivity.class);
                bookmarkIntent.putExtra(getString(R.string.menuSelection), "Bookmarks");
                bookmarkIntent.putExtra(getString(R.string.isFromReading), true);
                startActivityWithAnimation(bookmarkIntent);
                break;
            case R.id.menuHistory:
                Intent historyIntent = new Intent(getApplicationContext(), MainActivity.class);
                historyIntent.putExtra(getString(R.string.menuSelection), "History");
                historyIntent.putExtra(getString(R.string.isFromReading), true);
                startActivityWithAnimation(historyIntent);
                break;
        }
        return super.onOptionsItemSelected(item);
    }

}
