package com.ldsscriptures.pro.activity.studytools;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.ldsscriptures.pro.R;
import com.ldsscriptures.pro.activity.BaseActivity;
import com.ldsscriptures.pro.adapter.NewPlanSubListAdapter;
import com.ldsscriptures.pro.dbClass.GetLibCollectionFromDB;
import com.ldsscriptures.pro.model.LsData;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by IBL InfoTech on 8/5/2017.
 */

public class NewPlanSubListActivity extends BaseActivity {


    @BindView(R.id.ivMenu)
    ImageView ivMenu;
    @BindView(R.id.ivBack)
    ImageView ivBack;
    @BindView(R.id.tvTitle)
    TextView tvTitle;
    @BindView(R.id.ivSearch)
    ImageView ivSearch;
    @BindView(R.id.ivMore)
    ImageView ivMore;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.tvNoData)
    TextView tvNoData;
    @BindView(R.id.recyclerView)
    RecyclerView newPlanSubListrecyclerView;
    String LcID;
    @BindView(R.id.ivTabs)
    ImageView ivTabs;

    private ArrayList<LsData> newPlanSubListArrayList = new ArrayList<>();
    private NewPlanSubListAdapter newPlanSubListAdapter;
    static NewPlanSubListActivity newPlanSubListActivity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_plan_sub_list);
        ButterKnife.bind(this);
        setContents();
    }

    public void setContents() {
        newPlanSubListActivity = this;
        showText(tvTitle, getString(R.string.txt_new_plan));

        ivMenu.setVisibility(View.GONE);
        ivBack.setVisibility(View.VISIBLE);
        setVisibilityGone(ivTabs);
        setVisibilityGone(ivSearch);

        Intent intent = getIntent();
        LcID = intent.getStringExtra(getString(R.string.lcID));

        newPlanSubListArrayList = new GetLibCollectionFromDB(this).getLsDataFromDb(LcID);

        newPlanSubListrecyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        newPlanSubListrecyclerView.setLayoutManager(layoutManager);
        newPlanSubListAdapter = new NewPlanSubListAdapter(NewPlanSubListActivity.this, newPlanSubListArrayList, LcID);
        newPlanSubListrecyclerView.setAdapter(newPlanSubListAdapter);
    }

    public static NewPlanSubListActivity getInstance() {
        return newPlanSubListActivity;
    }

    @OnClick(R.id.ivBack)
    public void onViewClicked() {
        finish();
    }

}
