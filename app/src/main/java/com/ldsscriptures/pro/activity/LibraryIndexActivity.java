package com.ldsscriptures.pro.activity;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.Toast;

import com.ldsscriptures.pro.R;
import com.ldsscriptures.pro.adapter.LibraryIndexFirstLevelMenuAdapter;
import com.ldsscriptures.pro.adapter.LibraryIndexSecondLevelMenuAdapter;
import com.ldsscriptures.pro.adapter.LsIndexAdapter;
import com.ldsscriptures.pro.dbClass.GetDataFromDatabase;
import com.ldsscriptures.pro.dbClass.GetLibCollectionFromDB;
import com.ldsscriptures.pro.model.LsData;
import com.ldsscriptures.pro.model.LsFullIndexData;
import com.ldsscriptures.pro.model.LsMainIndexData;
import com.ldsscriptures.pro.model.ReadingNow.LibIndex;
import com.ldsscriptures.pro.model.ReadingNowData;
import com.ldsscriptures.pro.utils.Global;
import com.ldsscriptures.pro.utils.SessionManager;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.ldsscriptures.pro.model.ReadingNowData.readingNowData;

public class LibraryIndexActivity extends BaseActivity {

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.tvTitle)
    TextView tvTitle;
    @BindView(R.id.ivBack)
    ImageView ivBack;
    @BindView(R.id.ivTabs)
    ImageView ivTabs;
    @BindView(R.id.ivDropDown)
    ImageView ivDropDown;
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    @BindView(R.id.linDropDown)
    LinearLayout linDropDown;
    @BindView(R.id.ivMenu)
    ImageView ivMenu;
    @BindView(R.id.ivSearch)
    ImageView ivSearch;
    @BindView(R.id.ivMore)
    ImageView ivMore;
    @BindView(R.id.tvNoData)
    TextView tvNoData;
    @BindView(R.id.fab)
    FloatingActionButton fab;

    private RecyclerView rvSecondLevel;
    private LibraryIndexSecondLevelMenuAdapter libraryIndexSecondLevelMenuAdapter;

    private String lsTitle;
    private SessionManager sessionManager;
    public static ArrayList<LsMainIndexData> combineIndexDataArray = new ArrayList<>();
    public String lcID;
    static LibraryIndexActivity libraryIndexActivity;
    private String lsId;

    public static LibraryIndexActivity getInstance() {
        return libraryIndexActivity;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_library_index);
        ButterKnife.bind(this);
        setContents();
    }

    private void setContents() {
        ButterKnife.bind(this);

        libraryIndexActivity = this;

        ivMenu.setVisibility(View.VISIBLE);
        ivDropDown.setVisibility(View.VISIBLE);
        ivMore.setVisibility(View.GONE);
        ivBack.setVisibility(View.GONE);

        // set the dropdown option into toolbar
        lcID = getIntent().getStringExtra(getString(R.string.lcID));
        lsTitle = getIntent().getStringExtra(getString(R.string.lsTitle));
        lsId = getIntent().getStringExtra(getString(R.string.lsId));

        sessionManager = new SessionManager(LibraryIndexActivity.this);
        sessionManager.setoffset(0);

        ReadingNowData.tagArrayList.add("LibIndex");
        if (readingNowData != null)
            readingNowData.setLibIndex(new LibIndex(this.getClass().getName(), lsTitle, lcID, lsId));

        setToolbar(toolbar);
        showText(tvTitle, lsTitle);
        setRecyclerView(lsId);
    }

    @OnClick(R.id.fab)
    public void manageFloatingButton() {
        manageFloatingBtn();
    }

    @OnClick(R.id.linDropDown)
    public void openDropDownMenu() {
        initiateTypePopUp(linDropDown);
    }

    @OnClick(R.id.ivTabs)
    public void openTabs() {
        ArrayList<ReadingNowData> readingNowDataArrayList = sessionManager.getPreferencesArrayList(LibraryIndexActivity.this);
        if (readingNowDataArrayList == null) {
            readingNowDataArrayList = new ArrayList<>();
            ReadingNowData readingNowData = new ReadingNowData();
            if (Global.getPrefrenceString(LibraryIndexActivity.this, Global.ADD_NEW_TAB_PATH, "") != null &&
                    Global.getPrefrenceString(LibraryIndexActivity.this, Global.ADD_NEW_TAB_CLASS_NAME, "") != null) {
                readingNowData.setPath(Global.getPrefrenceString(LibraryIndexActivity.this, Global.ADD_NEW_TAB_PATH, ""));
                readingNowData.setTag(Global.getPrefrenceString(LibraryIndexActivity.this, Global.ADD_NEW_TAB_CLASS_NAME, ""));
            }

            readingNowData.setActivityIdentifier(getString(R.string.tabs_lib_collection));
            readingNowDataArrayList.add(0, readingNowData);
            sessionManager.setPreferencesArrayList(LibraryIndexActivity.this, readingNowDataArrayList);
        }

        ReadingNowData readingNowData = new ReadingNowData();
        readingNowData.setPath(Global.takeScreenshot(this, 24));
        readingNowData.setTag(this.getClass().getName());
        readingNowData.setBookmarked(readingNowDataArrayList.get(0).isBookmarked());
        readingNowData.setActivityIdentifier(getString(R.string.tabs_lib_index));

        readingNowData.setLibIndex(new LibIndex(this.getClass().getName(), lsTitle, lcID, lsId));
        readingNowDataArrayList.set(0, readingNowData);
        sessionManager.setPreferencesArrayList(LibraryIndexActivity.this, readingNowDataArrayList);

        startActivity(new Intent(this, TabsActivity.class));
        overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
    }

    @OnClick(R.id.ivBack)
    public void finishAct() {
        onBackPressed();
    }

    private void setRecyclerView(String lsId) {

        ArrayList<LsFullIndexData> lsFullIndexDataArray = new GetDataFromDatabase(LibraryIndexActivity.this, lsId).GetMainDataFromDB();

        combineIndexDataArray = new ArrayList<>();
        if (lsFullIndexDataArray != null) {
            for (int i = 0; i < lsFullIndexDataArray.size(); i++) {
                for (int j = 0; j < lsFullIndexDataArray.get(i).getTitleIndexDatas().size(); j++) {
                    LsMainIndexData mainIndexData = new LsMainIndexData();
                    mainIndexData.setId(lsFullIndexDataArray.get(i).getTitleIndexDatas().get(j).getId());

                    mainIndexData.setNav_section_id(lsFullIndexDataArray.get(i).getTitleIndexDatas().get(j).getNav_section_id());
                    mainIndexData.setPosition(lsFullIndexDataArray.get(i).getTitleIndexDatas().get(j).getPosition());
                    mainIndexData.setImage_renditions(lsFullIndexDataArray.get(i).getTitleIndexDatas().get(j).getImage_rendition());
                    mainIndexData.setTitle_html(lsFullIndexDataArray.get(i).getTitleIndexDatas().get(j).getTitle_html());
                    mainIndexData.setSubtitle(lsFullIndexDataArray.get(i).getTitleIndexDatas().get(j).getSubtitle());
                    mainIndexData.setUri(lsFullIndexDataArray.get(i).getTitleIndexDatas().get(j).getUri());
                    mainIndexData.setLsSubIndexDatas(null);
                    combineIndexDataArray.add(mainIndexData);
                }
                combineIndexDataArray.addAll(lsFullIndexDataArray.get(i).getItemMainIndexArray());
            }

            Collections.sort(combineIndexDataArray, new Comparator<LsMainIndexData>() {
                @Override
                public int compare(LsMainIndexData lhs, LsMainIndexData rhs) {
                    return Integer.valueOf(lhs.getPosition()).compareTo(rhs.getPosition());
                }
            });


        }
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(LibraryIndexActivity.this);
        recyclerView.setLayoutManager(mLayoutManager);

        if (combineIndexDataArray != null)
            recyclerView.setAdapter(new LsIndexAdapter(LibraryIndexActivity.this, combineIndexDataArray, lcID, lsId));
    }

    @OnClick(R.id.ivSearch)
    public void openSearch() {
        startActivityWithAnimation(new Intent(LibraryIndexActivity.this, SearchActivity.class));
    }

    private void initiateTypePopUp(LinearLayout linDropDown) {
        LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        LinearLayout layout = (LinearLayout) inflater.inflate(R.layout.popup_drop_down, null);

        final PopupWindow pw = new PopupWindow(layout, ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT, true);
        pw.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        pw.setTouchable(true);
        pw.setFocusable(true);
        pw.setOutsideTouchable(false);
        pw.setHeight(ViewGroup.LayoutParams.WRAP_CONTENT);
        pw.setWidth(ViewGroup.LayoutParams.MATCH_PARENT);

        pw.setTouchInterceptor(new View.OnTouchListener() {

            public boolean onTouch(View v, MotionEvent event) {
                // TODO Auto-generated method stub
                if (event.getAction() == MotionEvent.ACTION_OUTSIDE) {
                    pw.dismiss();
                    return true;
                }
                return false;
            }
        });

        pw.setOnDismissListener(new PopupWindow.OnDismissListener() {

            @Override
            public void onDismiss() {
                // TODO Auto-generated method stub
            }
        });

        pw.setContentView(layout);

        int x = (int) linDropDown.getX();
        int y = (int) linDropDown.getY() - 9;

        pw.showAsDropDown(linDropDown, x, y);

        RecyclerView rvFirstLevel = (RecyclerView) layout.findViewById(R.id.rvFirstLevel);
        rvSecondLevel = (RecyclerView) layout.findViewById(R.id.rvSecondLevel);
        TextView txtLibSel, txtLibColl;
        LinearLayout linLib;

        linLib = (LinearLayout) layout.findViewById(R.id.linLib);
        txtLibSel = (TextView) layout.findViewById(R.id.txtLibSel);
        txtLibColl = (TextView) layout.findViewById(R.id.txtLibColl);

        linLib.setVisibility(View.VISIBLE);
        txtLibSel.setVisibility(View.GONE);
        txtLibColl.setVisibility(View.GONE);

        rvFirstLevel.setLayoutManager(new LinearLayoutManager(LibraryIndexActivity.this, LinearLayoutManager.HORIZONTAL, false));
        rvFirstLevel.setAdapter(new LibraryIndexFirstLevelMenuAdapter(LibraryIndexActivity.this, new GetLibCollectionFromDB(LibraryIndexActivity.this).getLcArrayListFromDB(), lcID));

        ArrayList<LsData> lsDataArrayList = new ArrayList<>();
        lsDataArrayList = new GetLibCollectionFromDB(LibraryIndexActivity.this).getLsDataFromDb(lcID);
        setSecondLevel(lsDataArrayList, lcID);
    }

    public void setSecondLevel(ArrayList<LsData> lsDataArrayList, String lcID) {
        if (lsDataArrayList != null && lsDataArrayList.size() > 0) {
            rvSecondLevel.setLayoutManager(new LinearLayoutManager(LibraryIndexActivity.this, LinearLayoutManager.HORIZONTAL, false));
            libraryIndexSecondLevelMenuAdapter = new LibraryIndexSecondLevelMenuAdapter(LibraryIndexActivity.this, lsDataArrayList, lcID);
            rvSecondLevel.setAdapter(libraryIndexSecondLevelMenuAdapter);

            int scrollToPos = 0;
            for (int i = 0; i < lsDataArrayList.size(); i++) {
                if (lsTitle.equals(lsDataArrayList.get(i).getTitle())) {
                    scrollToPos = i;
                }
            }
            rvSecondLevel.scrollToPosition(scrollToPos);
            libraryIndexSecondLevelMenuAdapter.notifyDataSetChanged();
        }
    }

    public void finishActivity() {
        finish();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menuBack:
                finishActivity();
                break;
            case R.id.menuNext:
                if (Global.moreOptionMenuDatas.get(2).getLcID() != null) {
                    Intent intent = new Intent(getApplicationContext(), SubIndexActivity.class);
                    intent.putExtra(getString(R.string.indexTitle), Global.moreOptionMenuDatas.get(2).getLibIndexTitle());
                    intent.putExtra(getString(R.string.SubIndexPosition), Global.moreOptionMenuDatas.get(2).getLibSubPosition());
                    intent.putExtra(getString(R.string.lcID), Global.moreOptionMenuDatas.get(2).getLcID());
                    intent.putExtra(getString(R.string.lsId), Global.moreOptionMenuDatas.get(2).getLsId());
                    startActivityWithAnimation(intent);
                } else {
                    Toast.makeText(getApplicationContext(), "No Item Found", Toast.LENGTH_LONG).show();
                }
                break;
            case R.id.menuBookmark:
                Intent bookmarkIntent = new Intent(getApplicationContext(), MainActivity.class);
                bookmarkIntent.putExtra(getString(R.string.menuSelection), "Bookmarks");
                bookmarkIntent.putExtra(getString(R.string.isFromReading), true);
                startActivityWithAnimation(bookmarkIntent);
                break;
            case R.id.menuHistory:
                Intent historyIntent = new Intent(getApplicationContext(), MainActivity.class);
                historyIntent.putExtra(getString(R.string.menuSelection), "History");
                historyIntent.putExtra(getString(R.string.isFromReading), true);
                startActivityWithAnimation(historyIntent);
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @OnClick(R.id.ivMenu)
    public void openMenu() {
        openSlideMenu();
    }
}
