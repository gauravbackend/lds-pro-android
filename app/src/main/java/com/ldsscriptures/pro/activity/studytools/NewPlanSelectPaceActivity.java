package com.ldsscriptures.pro.activity.studytools;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.icu.util.Calendar;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v7.widget.AppCompatRadioButton;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.ldsscriptures.pro.R;
import com.ldsscriptures.pro.activity.BaseActivity;
import com.ldsscriptures.pro.database.CommonDatabaseHelper;

import java.text.SimpleDateFormat;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


public class NewPlanSelectPaceActivity extends BaseActivity {

    @BindView(R.id.ivMenu)
    ImageView ivMenu;
    @BindView(R.id.ivBack)
    ImageView ivBack;
    @BindView(R.id.tvTitle)
    TextView tvTitle;
    @BindView(R.id.ivSearch)
    ImageView ivSearch;
    @BindView(R.id.ivMore)
    ImageView ivMore;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.days_30)
    AppCompatRadioButton days30;
    @BindView(R.id.days_60)
    AppCompatRadioButton days60;
    @BindView(R.id.days_90)
    AppCompatRadioButton days90;
    @BindView(R.id.days_180)
    AppCompatRadioButton days180;
    @BindView(R.id.days_365)
    AppCompatRadioButton days365;
    @BindView(R.id.radiobuttoncustom)
    AppCompatRadioButton radiobuttoncustom;
    @BindView(R.id.myRadioGroup)
    RadioGroup myRadioGroup;
    @BindView(R.id.btn_cancel_plan)
    Button btnCancelPlan;
    @BindView(R.id.btn_create_plan)
    Button btnCreatePlan;

    static NewPlanSelectPaceActivity newPlanSelectPaceActivity;
    @BindView(R.id.ivTabs)
    ImageView ivTabs;
    @BindView(R.id.textView5)
    TextView textView5;

    private Calendar calendar;
    private int year, month, day;
    private String currant_date, plan_Sub_Title;
    private int dateDifference, plan_Sub_LsID, plan_Main_LcID;
    CommonDatabaseHelper commonDatabaseHelper;
    boolean idDataFound = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_plan_select_pace);
        ButterKnife.bind(this);
        setContents();
    }

    public void setContents() {
        showText(tvTitle, getString(R.string.txt_new_plan));

        ivMenu.setVisibility(View.GONE);
        ivBack.setVisibility(View.VISIBLE);

        setVisibilityGone(ivTabs);
        setVisibilityGone(ivSearch);

        commonDatabaseHelper = new CommonDatabaseHelper(this);
        Intent mIntent = getIntent();
        int intValue = mIntent.getIntExtra("planDetailsActivity", 0);

        if (intValue == 0) {
            plan_Sub_LsID = mIntent.getIntExtra("plan_Sub_LsID", 0);
            plan_Main_LcID = mIntent.getIntExtra("plan_Main_LcID", 0);
            plan_Sub_Title = mIntent.getStringExtra("plan_Sub_Title");
        }
        currant_date = new SimpleDateFormat("dd/MM/yyyy").format(java.util.Calendar.getInstance().getTime());
        radiobuttoncustom.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onClick(View v) {

                calendar = Calendar.getInstance();
                year = calendar.get(Calendar.YEAR);
                month = calendar.get(Calendar.MONTH);
                day = calendar.get(Calendar.DAY_OF_MONTH);
                showDialog(999);
            }
        });
    }

    @Override
    protected Dialog onCreateDialog(int id) {
        // TODO Auto-generated method stub
        if (id == 999) {
            DatePickerDialog datePickerDialog = new DatePickerDialog(this, myDateListener, year, month, day);
            datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
            return datePickerDialog;
        }
        return null;
    }

    private DatePickerDialog.OnDateSetListener myDateListener = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker arg0, int arg1, int arg2, int arg3) {
            // TODO Auto-generated method stub
                     /*arg1 = year
                     arg2 = month
                     arg3 = day*/
            showDate(arg1, arg2 + 1, arg3);
        }
    };

    public static NewPlanSelectPaceActivity getInstance() {
        return newPlanSelectPaceActivity;
    }

    @OnClick({R.id.ivBack, R.id.btn_cancel_plan, R.id.btn_create_plan})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.ivBack:
                finish();
                break;
            case R.id.btn_cancel_plan:
                finish();
                break;
            case R.id.btn_create_plan:
                new CreatePlanAsync().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, "");
                break;
        }
    }

    private void showDate(int year, int month, int day) {
        StringBuilder select_date = new StringBuilder().append(day).append("/").append(month).append("/").append(year);
        dateDifference = Math.abs((int) getDateDiff(new SimpleDateFormat("dd/MM/yyyy"), select_date.toString(), currant_date));
        radiobuttoncustom.setText(dateDifference + " " + "days");
    }

    private class CreatePlanAsync extends AsyncTask<String, Void, String> {

        @Override
        protected void onPreExecute() {
        }

        @Override
        protected String doInBackground(String... params) {

            fetchPlanData();
            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            insertPlanData();
        }
    }

    public void fetchPlanData() {
        Cursor cursor;
        cursor = commonDatabaseHelper.getPlanList();

        if (cursor != null && cursor.moveToFirst()) {
            while (!cursor.isAfterLast()) {

                String plan_Main_LCID1 = cursor.getString(cursor.getColumnIndex(CommonDatabaseHelper.KEY_PLAN_MAIN_LCID));
                String plan_Sub_LSID1 = cursor.getString(cursor.getColumnIndex(CommonDatabaseHelper.KEY_PLAN_SUB_LSID));

                if (plan_Main_LCID1.equals(String.valueOf(plan_Main_LcID)) && plan_Sub_LSID1.equals(String.valueOf(plan_Sub_LsID))) {
                    idDataFound = true;
                    break;
                }
                cursor.moveToNext();
            }
        }
    }

    public void insertPlanData() {


        if (!idDataFound) {
            String radiovalue = ((RadioButton) findViewById(myRadioGroup.getCheckedRadioButtonId())).getText().toString();
            ContentValues contentValues = new ContentValues();
            contentValues.put(CommonDatabaseHelper.KEY_PLAN_NAME, plan_Sub_Title.toString());
            contentValues.put(CommonDatabaseHelper.KEY_PLAN_MAIN_LCID, plan_Main_LcID);
            contentValues.put(CommonDatabaseHelper.KEY_PLAN_SUB_LSID, plan_Sub_LsID);
            contentValues.put(CommonDatabaseHelper.KEY_PLAN_DAYS, radiovalue.toString());
            contentValues.put(CommonDatabaseHelper.KEY_PLAN_FLAG, false);
            contentValues.put(CommonDatabaseHelper.KEY_PLAN_READING_MIN, 0);
            contentValues.put(CommonDatabaseHelper.KEY_PLAN_CREATED_DATE, currant_date);


            commonDatabaseHelper.insertData(CommonDatabaseHelper.PLAN_MASTER, contentValues);
            startActivity(new Intent(getApplicationContext(), PlanActivity.class));
            finish();
        } else {
            Toast.makeText(getApplicationContext(), "Already Created Plan", Toast.LENGTH_LONG).show();
        }
    }
}
