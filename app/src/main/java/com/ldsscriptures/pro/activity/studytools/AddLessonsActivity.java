package com.ldsscriptures.pro.activity.studytools;

import android.app.DatePickerDialog;
import android.content.ContentValues;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.ldsscriptures.pro.R;
import com.ldsscriptures.pro.activity.BaseActivity;
import com.ldsscriptures.pro.database.CommonDatabaseHelper;
import com.ldsscriptures.pro.utils.Global;
import com.ldsscriptures.pro.utils.PushToServerData;
import com.ldsscriptures.pro.utils.SessionManager;
import com.ldsscriptures.pro.utils.Validator;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class AddLessonsActivity extends BaseActivity {
    @BindView(R.id.ivBack)
    ImageView ivBack;
    @BindView(R.id.tvTitle)
    TextView tvTitle;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.edAddLesson)
    EditText edAddLesson;
    @BindView(R.id.tvDone)
    TextView tvDone;
    @BindView(R.id.edDate)
    EditText edDate;
    @BindView(R.id.edLessonDesc)
    EditText edLessonDesc;
    @BindView(R.id.linDate)
    LinearLayout linDate;
    private CommonDatabaseHelper commonDatabaseHelper;
    SessionManager sessionManager;
    String Lessonname = "";
    String Lessondate = "";
    String Lessondesc = "";
    int Lessonid = 0;
    private boolean isEdit = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_lessons);
        ButterKnife.bind(this);
        setContent();
    }

    private void setContent() {
        ButterKnife.bind(this);
        //set toolbar
        setToolbar(toolbar);

        sessionManager = new SessionManager(this);
        commonDatabaseHelper = new CommonDatabaseHelper(AddLessonsActivity.this);

        Intent intent = getIntent();
        Bundle extras = intent.getExtras();
        if (extras != null) {
            if (extras.containsKey("Lessonname")) {
                Lessonname = getIntent().getStringExtra("Lessonname");
            }
            if (extras.containsKey("Lessondate")) {
                Lessondate = getIntent().getStringExtra("Lessondate");
            }
            if (extras.containsKey("Lessondesc")) {
                Lessondesc = getIntent().getStringExtra("Lessondesc");
            }
            if (extras.containsKey("Lessonid")) {
                Lessonid = getIntent().getIntExtra("Lessonid", 0);
            }

            if (Lessonid != 0 && Lessonname != null && Lessondate != null) {
                showText(tvTitle, getString(R.string.edit_lesson));
                edAddLesson.setText(Lessonname);
                edDate.setText(Global.convertViewFormat(Lessondate));
                edLessonDesc.setText(Lessondesc);
                isEdit = true;
            }
        } else {
            //set default current date into date field.
            Calendar c = Calendar.getInstance();
            SimpleDateFormat df = new SimpleDateFormat("MMM dd, yyyy", Locale.US);
            edDate.setText(df.format(c.getTime()));
            showText(tvTitle, getString(R.string.new_lesson));
            isEdit = false;
        }

    }

    @OnClick({R.id.ivBack, R.id.linDate, R.id.tvDone})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.ivBack:
                finish();
                onBackPressed();
                break;
            case R.id.linDate:
                // TODO Auto-generated method stub
                final Calendar myCalendar = Calendar.getInstance();
                final DatePickerDialog dialog = new DatePickerDialog(AddLessonsActivity.this, new DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear,
                                          int dayOfMonth) {
                        // TODO Auto-generated method stub
                        edDate.setText("");
                        final Calendar myCalendar = Calendar.getInstance();
                        myCalendar.set(Calendar.YEAR, year);
                        myCalendar.set(Calendar.MONTH, monthOfYear + 1);
                        myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                        final String selectedDate = year + "-" + (monthOfYear + 1) + "-" + dayOfMonth;


                        edDate.setText(convertDateFormat(selectedDate));
                    }
                }, myCalendar.get(Calendar.YEAR), myCalendar.get(Calendar.MONTH), myCalendar.get(Calendar.DAY_OF_MONTH));
                dialog.getDatePicker().setMinDate(System.currentTimeMillis());
                dialog.show();
                break;
            case R.id.tvDone:
                if (validateFields()) {

                    if (isEdit) {

                        ContentValues contentValues = new ContentValues();
                        contentValues.put("lessonname", edAddLesson.getText().toString());
                        contentValues.put("lessondate", Global.convertDBFormat(edDate.getText().toString()));
                        contentValues.put("lessondesc", edLessonDesc.getText().toString());
                        contentValues.put("modified", "1");

                        commonDatabaseHelper.updateRowData(CommonDatabaseHelper.TABLE_LESSONS, contentValues,
                                CommonDatabaseHelper.KEY_ID + " = ?",
                                new String[]{String.valueOf(Lessonid)});

                    } else {
                        ContentValues contentValues = new ContentValues();
                        contentValues.put("serverid", 0);
                        contentValues.put("lessonname", edAddLesson.getText().toString());
                        contentValues.put("lessondate", Global.convertDBFormat(edDate.getText().toString()));
                        contentValues.put("lessondesc", edLessonDesc.getText().toString());
                        contentValues.put("deleted", "");
                        contentValues.put("modified", "");
                        commonDatabaseHelper.insertData(CommonDatabaseHelper.TABLE_LESSONS, contentValues);
                    }

                    new PushToServerData(this);

                    if (LessonsActivity.getInstance() != null) {
                        LessonsActivity.getInstance().setData();
                    }
                    finish();
                }
                break;
        }
    }

    public String convertDateFormat(String inputDate) {
        SimpleDateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
        SimpleDateFormat outputFormat = new SimpleDateFormat("MMM dd, yyyy", Locale.US);
        Date outputDate = null;
        try {
            outputDate = inputFormat.parse(inputDate);
            return outputFormat.format(outputDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return outputFormat.format(outputDate);
    }


    private boolean validateFields() {
        if (!Validator.checkEmpty(edAddLesson)) {
            Global.showSnackBar(AddLessonsActivity.this, getString(R.string.enter_lesson_name));
            return false;
        }
        if (!Validator.checkEmpty(edDate)) {
            Global.showSnackBar(AddLessonsActivity.this, getString(R.string.select_lesson_date));
            return false;
        }
        if (!Validator.checkEmpty(edLessonDesc)) {
            Global.showSnackBar(AddLessonsActivity.this, getString(R.string.enter_lesson_description));
            return false;
        }
        return true;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if (LessonsActivity.getInstance() != null) {
            LessonsActivity.getInstance().setData();
        }
    }
}
