package com.ldsscriptures.pro.activity;

import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.design.widget.TextInputLayout;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.ldsscriptures.pro.BuildConfig;
import com.ldsscriptures.pro.R;
import com.ldsscriptures.pro.database.CommonDatabaseHelper;
import com.ldsscriptures.pro.retroutils.APIClient;
import com.ldsscriptures.pro.retroutils.APIInterface;
import com.ldsscriptures.pro.service.SyncEventReceiver;
import com.ldsscriptures.pro.utils.Global;
import com.ldsscriptures.pro.utils.SessionManager;
import com.ldsscriptures.pro.utils.Validator;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends BaseActivity {

    @BindView(R.id.tvEmail)
    TextInputLayout tvEmail;

    @BindView(R.id.edtEmail)
    EditText edtEmail;

    @BindView(R.id.edtPassword)
    EditText edtPassword;

    @BindView(R.id.btnLogIn)
    Button btnLogIn;

    @BindView(R.id.imageView_background)
    ImageView imageView_background;

    @BindView(R.id.tvRecoverPass)
    TextView tvRecoverPass;

    String currantDate;
    int currantDayHour;
    Calendar calendar;

    private SessionManager sessionManager;

    public static LoginActivity loginActivity;
    private CommonDatabaseHelper commonDatabaseHelper;

    public static LoginActivity getInstance() {
        return loginActivity;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);
        setContents();
    }

    private void setContents() {
        ButterKnife.bind(this);
        commonDatabaseHelper = new CommonDatabaseHelper(LoginActivity.this);
        loginActivity = this;

        currantDate = new SimpleDateFormat("yyyy-MM-dd").format(Calendar.getInstance().getTime());
        calendar = Calendar.getInstance();
        currantDayHour = calendar.get(Calendar.HOUR);

        sessionManager = new SessionManager(LoginActivity.this);
        setBackground(R.drawable.bg_splash_blur, imageView_background);

        edtEmail.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                tvEmail.setHint(getString(R.string.hint_email));
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }
        });

    }

    @OnClick({R.id.btnLogIn, R.id.tvRecoverPass, R.id.txtForgotPassword})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.btnLogIn:
                if (validateFields()) {
                    if (Global.isNetworkAvailable(this)) {
                        getLoggedIn();
                    } else {
                        Global.showNetworkAlert(this);
                    }
                }
                break;
            case R.id.tvRecoverPass:
                startActivity(new Intent(LoginActivity.this, ForgotPasswordEmailActivity.class));
                break;
        }
    }

    public void passwordRecovered() {
        tvRecoverPass.setText(R.string.recover);
    }

    private void getLoggedIn() {
        APIInterface apiService = APIClient.getClient().create(APIInterface.class);
        String url = APIClient.BASE_URL;

        url += "login.pl?email=" + edtEmail.getText().toString().trim() +
                "&password=" + edtPassword.getText().toString().trim() +
                "&device=" + APIClient.DEVICE_KEY +
                "&appversion=" + BuildConfig.VERSION_NAME +
                "&apiauth=" + APIClient.AUTH_KEY;

        Call<JsonObject> callObject = apiService.getLogin(url.replaceAll(" ", "%20"));

        callObject.enqueue(new Callback<JsonObject>() {
            @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                try {
                    if (response.body() != null && !response.body().toString().trim().isEmpty()) {

                        JsonParser jsonParser = new JsonParser();
                        JSONObject jsonObject = new JSONObject(response.body().toString());

                        if (Integer.parseInt(jsonObject.getString("success")) == 0) {

                            String mess = jsonObject.getString("msg");
                            Toast.makeText(getApplicationContext(), mess, Toast.LENGTH_LONG).show();

                        } else {

                            String session_id = jsonObject.getString("session_id");
                            String plan_expire_date = jsonObject.getString("plan_expire_date");
                            String confirmed = jsonObject.getString("confirmed");
                            String subscribed = jsonObject.getString("subscribed");
                            String user_achievements = jsonObject.getString("achievements");
                            String user_goal_points = jsonObject.getString("goal_points");

                            String user_day_streak = jsonObject.getString("day_streak");
                            String user_fb_user = jsonObject.getString("fb_user");
                            String user_points = jsonObject.getString("points");
                            String user_img_url = jsonObject.getString("img_url");

                            String user_annotations = jsonObject.getString("annotations");
                            String user_fb_id = jsonObject.getString("fb_id");
                            String user_username = jsonObject.getString("username");
                            String user_email = jsonObject.getString("email");
                            String user_goal_hit = jsonObject.getString("goal_hit");
                            String user_min_reading = jsonObject.getString("min_reading");
                            String user_id = jsonObject.getString("user_id");
                            String private_status = jsonObject.getString("private");
                            String plan_type = jsonObject.getString("plan_type");
                            String time_zone = jsonObject.getString("server_time_zone");

                            ContentValues contentValues = new ContentValues();
                            contentValues.put(SessionManager.KEY_USER_SESSION_ID, session_id);
                            contentValues.put(SessionManager.KEY_USER_ID, user_id);
                            contentValues.put(SessionManager.KEY_USER_USERNAME, user_username);
                            contentValues.put(SessionManager.KEY_USER_EMAIL, user_email);
                            contentValues.put(SessionManager.KEY_USR_IMG_URL, user_img_url);
                            contentValues.put(SessionManager.KEY_USER_FB_ID, user_fb_id);
                            contentValues.put(SessionManager.KEY_USER_FB_USER, user_fb_user);
                            contentValues.put(SessionManager.KEY_USER_PLAN_EXPIRE_DATE, plan_expire_date);
                            contentValues.put(SessionManager.KEY_USER_CONFIRMED, confirmed);
                            contentValues.put(SessionManager.KEY_USER_SUBSCRIBED, subscribed);
                            contentValues.put(SessionManager.KEY_USER_ACHIEVEMENTS, user_achievements);
                            contentValues.put(SessionManager.KEY_USER_GOAL_POINTS, user_goal_points);
                            contentValues.put(SessionManager.KEY_USER_DAY_STREAK, user_day_streak);
                            contentValues.put(SessionManager.KEY_USER_POINTS, user_points);
                            contentValues.put(SessionManager.KEY_USER_ANNOTATIONS, user_annotations);
                            contentValues.put(SessionManager.KEY_USER_GOAL_HIT, user_goal_hit);
                            contentValues.put(SessionManager.KEY_USER_MIN_READING, user_min_reading);
                            commonDatabaseHelper.insertDataIfNotExists(CommonDatabaseHelper.USER_MASTER, contentValues);

                            sessionManager.createLoginSession(session_id, plan_expire_date, confirmed, subscribed, user_achievements, user_goal_points, user_day_streak, user_fb_user, user_points, user_img_url, user_annotations, user_fb_id, user_username, user_email, user_goal_hit, user_min_reading, user_id, private_status);
                            sessionManager.setPremiumSelected(true);
                            sessionManager.checkGoalSet();
                            sessionManager.setUserDailyPoints(Integer.parseInt(user_points));
                            sessionManager.setUserMinReading(user_min_reading);
                            sessionManager.setSubscribeType(plan_type);
                            sessionManager.setKeyTimeZone(time_zone);
                            sessionManager.setUserID(user_id);
                            sessionManager.setUserSessionId(session_id);

                            if (subscribed.isEmpty() || subscribed == null || subscribed.equals("") || subscribed.equals("null")) {
                                subscribed = "0";
                            }
                            sessionManager.setKeySubscribed(subscribed);

                            insertDailyPointData(currantDate, currantDayHour, Integer.parseInt(user_min_reading), Integer.parseInt(user_points), user_id);
                            Global.UserId = user_id;

                            sessionManager.setSyncStatus(true);
                            SyncEventReceiver.setupAlarm(getApplicationContext());

                            Cursor cursor = commonDatabaseHelper.getLastSyncDate(user_id);
                            if (cursor != null && cursor.moveToFirst()) {
                                sessionManager.setLastSyncDate(cursor.getString(cursor.getColumnIndex(SessionManager.KEY_USER_LAST_SYNC_DATE)));
                            }
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                call.cancel();
            }
        });
    }

    private boolean validateFields() {
        if (!Validator.checkEmpty(edtEmail)) {
            tvEmail.setHint(getString(R.string.error_validEmail));
            return false;
        }
        if (!Validator.checkEmail(edtEmail)) {
            tvEmail.setHint(getString(R.string.error_validEmail));
            return false;
        }

        if (!Validator.checkEmpty(edtPassword)) {
            edtPassword.setError(getString(R.string.error_enterPassword));
            return false;
        }
        return true;
    }

    public void insertDailyPointData(String currantDate, int currantDayHour, int minutes, int points, String user_id) {

        ContentValues contentValues = new ContentValues();
        contentValues.put(CommonDatabaseHelper.KEY_DAILY_LOCAL_USE_DATE, currantDate);
        contentValues.put(CommonDatabaseHelper.KEY_DAILY_LOCAL_USE_HOUR, currantDayHour);
        contentValues.put(CommonDatabaseHelper.KEY_DAILY_MINUTES, minutes);
        contentValues.put(CommonDatabaseHelper.KEY_DAILY_POINTS, points);
        contentValues.put(CommonDatabaseHelper.KEY_DAILY_USER_ID, user_id);

        commonDatabaseHelper.insertData(CommonDatabaseHelper.USER_DAILY, contentValues);
    }


}
