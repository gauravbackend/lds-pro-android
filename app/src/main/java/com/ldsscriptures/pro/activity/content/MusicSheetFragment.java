package com.ldsscriptures.pro.activity.content;


import android.annotation.SuppressLint;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.ConsoleMessage;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.ldsscriptures.pro.R;
import com.ldsscriptures.pro.fragment.BaseFragment;
import com.ldsscriptures.pro.utils.Global;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MusicSheetFragment extends BaseFragment {
    @BindView(R.id.webView)
    WebView webView;
    @BindView(R.id.txtPageNumber)
    TextView txtPageNumber;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_music_sheet, container, false);
        RelativeLayout lnr_root = (RelativeLayout) view.findViewById(R.id.lnr_root);
        ButterKnife.bind(this, lnr_root);
        setContents();
        return view;
    }

    @SuppressLint("SetJavaScriptEnabled")
    public void setContents() {
        // this load page url in webview
        String pdfUrl = getArguments().getString(getResources().getString(R.string.pdf_url));
        if (pdfUrl != null) {
            webView.setWebViewClient(new myWebViewClient());
            webView.getSettings().setJavaScriptEnabled(true);
            webView.loadUrl("http://drive.google.com/viewerng/viewer?embedded=true&url=" + pdfUrl);
        }
    }


    public class myWebViewClient extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            view.loadUrl(url);
            return true;
        }

        public boolean onConsoleMessage(ConsoleMessage cm) {
            Global.printLog("MusicSheetFragment", cm.message() + " -- From line "
                    + cm.lineNumber() + " of "
                    + cm.sourceId());
            return true;
        }
    }
}
