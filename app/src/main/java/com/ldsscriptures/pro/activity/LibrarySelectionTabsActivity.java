package com.ldsscriptures.pro.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.ldsscriptures.pro.R;
import com.ldsscriptures.pro.adapter.LibrarySelectionPagerAdapter;
import com.ldsscriptures.pro.model.ReadingNow.LibSelection;
import com.ldsscriptures.pro.model.ReadingNowData;
import com.ldsscriptures.pro.utils.Global;
import com.ldsscriptures.pro.utils.SessionManager;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.ldsscriptures.pro.model.ReadingNowData.readingNowData;

public class LibrarySelectionTabsActivity extends BaseActivity implements TabLayout.OnTabSelectedListener {

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.tabLayout)
    TabLayout tabLayout;
    @BindView(R.id.viewPager)
    ViewPager viewPager;
    @BindView(R.id.tvTitle)
    TextView tvTitle;
    @BindView(R.id.ivTabs)
    ImageView ivTabs;
    @BindView(R.id.ivMore)
    ImageView ivMore;

    static LibrarySelectionTabsActivity librarySelectionTabsActivity;
    private SessionManager sessionManager;
    private String LcID;
    private String lcTitle;

    public static LibrarySelectionTabsActivity getInstance() {
        return librarySelectionTabsActivity;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_library_selection);
        ButterKnife.bind(this);
        setContents();
    }

    private void setContents() {
        ButterKnife.bind(this);

        librarySelectionTabsActivity = this;
        sessionManager = new SessionManager(LibrarySelectionTabsActivity.this);
        ivMore.setVisibility(View.GONE);
        lcTitle = getIntent().getStringExtra(getString(R.string.lcname));
        LcID = getIntent().getStringExtra(getString(R.string.lcID));

        setToolbar(toolbar);
        showText(tvTitle, lcTitle);

        ReadingNowData.tagArrayList.add("LibSelection");
        if (readingNowData != null)
            readingNowData.setLibSelection(new LibSelection(this.getClass().getName(), getIntent().getStringExtra(getString(R.string.lcname)), LcID));

        tabLayout.addTab(tabLayout.newTab().setText(getString(R.string.library).toUpperCase()));
        tabLayout.addTab(tabLayout.newTab().setText(getString(R.string.bookmarks).toUpperCase()));
        tabLayout.addTab(tabLayout.newTab().setText(getString(R.string.history).toUpperCase()));

        LibrarySelectionPagerAdapter adapter = new LibrarySelectionPagerAdapter(this, getSupportFragmentManager(), tabLayout.getTabCount(), LcID);
        viewPager.setAdapter(adapter);
        tabLayout.setOnTabSelectedListener(this);
        viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            public void onPageScrollStateChanged(int state) {
            }

            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            public void onPageSelected(int position) {
                tabLayout.getTabAt(position).select();
            }
        });
    }

    @OnClick(R.id.fab)
    public void manageFloatingButton() {
        manageFloatingBtn();
    }

    @OnClick(R.id.ivMenu)
    public void openMenu() {
        openSlideMenu();
    }

    @Override
    public void onTabSelected(TabLayout.Tab tab) {
        viewPager.setCurrentItem(tab.getPosition());
    }

    @Override
    public void onTabUnselected(TabLayout.Tab tab) {
    }

    @Override
    public void onTabReselected(TabLayout.Tab tab) {
    }

    public void finishActivity() {
        finish();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @OnClick(R.id.ivSearch)
    public void openSearch() {
        startActivityWithAnimation(new Intent(LibrarySelectionTabsActivity.this, SearchActivity.class));
    }

    @OnClick(R.id.ivTabs)
    public void openTab() {
        ArrayList<ReadingNowData> readingNowDataArrayList = sessionManager.getPreferencesArrayList(LibrarySelectionTabsActivity.this);
        if (readingNowDataArrayList == null) {
            readingNowDataArrayList = new ArrayList<>();
            ReadingNowData readingNowData = new ReadingNowData();
            if (Global.getPrefrenceString(LibrarySelectionTabsActivity.this, Global.ADD_NEW_TAB_PATH, "") != null &&
                    Global.getPrefrenceString(LibrarySelectionTabsActivity.this, Global.ADD_NEW_TAB_CLASS_NAME, "") != null) {
                readingNowData.setPath(Global.getPrefrenceString(LibrarySelectionTabsActivity.this, Global.ADD_NEW_TAB_PATH, ""));
                readingNowData.setTag(Global.getPrefrenceString(LibrarySelectionTabsActivity.this, Global.ADD_NEW_TAB_CLASS_NAME, ""));
            }

            readingNowData.setActivityIdentifier(getString(R.string.tabs_lib_collection));
            readingNowDataArrayList.add(0, readingNowData);
            sessionManager.setPreferencesArrayList(LibrarySelectionTabsActivity.this, readingNowDataArrayList);
        }

        ReadingNowData readingNowData = new ReadingNowData();
        readingNowData.setPath(Global.takeScreenshot(this, 24));
        readingNowData.setTag(this.getClass().getName());
        readingNowData.setBookmarked(readingNowDataArrayList.get(0).isBookmarked());
        readingNowData.setActivityIdentifier(getString(R.string.tabs_lib_selection));

        readingNowData.setLibSelection(new LibSelection(this.getClass().getName(), lcTitle, LcID));
        readingNowDataArrayList.set(0, readingNowData);
        sessionManager.setPreferencesArrayList(LibrarySelectionTabsActivity.this, readingNowDataArrayList);

        startActivity(new Intent(this, TabsActivity.class));
        overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menuBack:
                finishActivity();
                break;
            case R.id.menuNext:
                if (Global.moreOptionMenuDatas.get(1).getLcID() != null) {
                    Intent intent = new Intent(getApplicationContext(), LibraryIndexActivity.class);
                    intent.putExtra(getString(R.string.lcID), Global.moreOptionMenuDatas.get(1).getLcID());
                    intent.putExtra(getString(R.string.lsTitle), Global.moreOptionMenuDatas.get(1).getLsTitle());
                    intent.putExtra(getString(R.string.lsId), Global.moreOptionMenuDatas.get(1).getLsId());
                    startActivityWithAnimation(intent);
                } else {
                    Toast.makeText(getApplicationContext(), "No Item Found", Toast.LENGTH_LONG).show();
                }
                break;
            case R.id.menuBookmark:
                Intent bookmarkIntent = new Intent(getApplicationContext(), MainActivity.class);
                bookmarkIntent.putExtra(getString(R.string.menuSelection), "Bookmarks");
                bookmarkIntent.putExtra(getString(R.string.isFromReading), true);
                startActivityWithAnimation(bookmarkIntent);
                break;
            case R.id.menuHistory:
                Intent historyIntent = new Intent(getApplicationContext(), MainActivity.class);
                historyIntent.putExtra(getString(R.string.menuSelection), "History");
                historyIntent.putExtra(getString(R.string.isFromReading), true);
                startActivityWithAnimation(historyIntent);
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}
