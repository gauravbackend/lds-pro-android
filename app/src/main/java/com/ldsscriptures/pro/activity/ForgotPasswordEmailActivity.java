package com.ldsscriptures.pro.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.ldsscriptures.pro.R;
import com.ldsscriptures.pro.model.ForgotPasswordData;
import com.ldsscriptures.pro.retroutils.APIClient;
import com.ldsscriptures.pro.retroutils.APIInterface;
import com.ldsscriptures.pro.utils.Global;
import com.ldsscriptures.pro.utils.Validator;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class ForgotPasswordEmailActivity extends BaseActivity {

    @BindView(R.id.ivBack)
    ImageView ivBack;
    @BindView(R.id.tvCancel)
    TextView tvCancel;
    @BindView(R.id.tvSend)
    TextView tvSend;
    @BindView(R.id.edEmail)
    EditText edEmail;
    @BindView(R.id.tvEmail)
    TextInputLayout tvEmail;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password_email);
        ButterKnife.bind(this);
        setContents();
    }

    private void setContents() {
        ButterKnife.bind(this);

    }

    @OnClick({R.id.ivBack, R.id.tvCancel, R.id.tvSend})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.ivBack:
                finish();
                break;
            case R.id.tvCancel:
                finish();
                break;
            case R.id.tvSend:
                if (validateFields()) {
                    recoverPassword(edEmail.getText().toString().trim());
                }
                break;
        }
    }

    private void recoverPassword(String email) {
        APIInterface apiService = APIClient.getClient().create(APIInterface.class);
        String url = APIClient.BASE_URL;
        url += "forgotPassword.pl?email=" + email +
                "&apiauth=" + APIClient.AUTH_KEY;
        Call<ForgotPasswordData> callObject = apiService.recoverPassword(url.replaceAll(" ", "%20"));
        callObject.enqueue(new Callback<ForgotPasswordData>() {
            @Override
            public void onResponse(Call<ForgotPasswordData> call, Response<ForgotPasswordData> response) {
                 if (response.body() != null && !response.body().toString().trim().isEmpty()) {
                    if (Integer.parseInt(response.body().getSuccess()) == 1) {
                        startActivity(new Intent(ForgotPasswordEmailActivity.this, ForgotPasswordConfirmActivity.class));
                        Global.showSnackBar(ForgotPasswordEmailActivity.this, response.body().getMsg());
                        finish();
                    }
                }
            }

            @Override
            public void onFailure(Call<ForgotPasswordData> call, Throwable t) {
                call.cancel();
            }
        });
    }

    private boolean validateFields() {
        if (!Validator.checkEmpty(edEmail)) {
            tvEmail.setHint(getString(R.string.error_validEmail));
            return false;
        }
        if (!Validator.checkEmail(edEmail)) {
            tvEmail.setHint(getString(R.string.error_validEmail));
            return false;
        }
        return true;
    }

}
