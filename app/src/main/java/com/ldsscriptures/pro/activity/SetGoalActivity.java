package com.ldsscriptures.pro.activity;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.JsonObject;
import com.ldsscriptures.pro.R;
import com.ldsscriptures.pro.activity.subscriptionmenu.PremiumActivity;
import com.ldsscriptures.pro.retroutils.APIClient;
import com.ldsscriptures.pro.retroutils.APIInterface;
import com.ldsscriptures.pro.utils.Global;
import com.ldsscriptures.pro.utils.SessionManager;

import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SetGoalActivity extends BaseActivity {

    @BindView(R.id.ivBack)
    ImageView ivBack;

    @BindView(R.id.ivSearch)
    ImageView ivSearch;

    @BindView(R.id.ivMenu)
    ImageView ivMenu;

    @BindView(R.id.tvTitle)
    TextView tvTitle;

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.iv300)
    ImageView iv300;

    @BindView(R.id.iv1000)
    ImageView iv1000;

    @BindView(R.id.iv1500)
    ImageView iv1500;

    @BindView(R.id.iv4000)
    ImageView iv4000;

    @BindView(R.id.btnSetGoal)
    Button btnSetGoal;

    private SessionManager sessionManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_set_goal);
        setContents();
    }

    private void setContents() {

        ButterKnife.bind(this);
        sessionManager = new SessionManager(SetGoalActivity.this);

        ivSearch.setVisibility(View.GONE);

        setToolbar(toolbar);
        showText(tvTitle, getString(R.string.set_daily_goal));
        setVisibilityVisible(ivBack);
        setVisibilityGone(ivMenu);

        // set the Set Daily Goal base on user Set Goal points.
        if (getUserInfo(SessionManager.KEY_USER_GOAL_POINTS) != null)
            switch (getUserInfo(SessionManager.KEY_USER_GOAL_POINTS)) {
                case "4000":
                    getSelectedItem(iv4000);
                    break;
                case "1000":
                    getSelectedItem(iv1000);
                    break;
                case "1500":
                    getSelectedItem(iv1500);
                    break;
                default:
                    getSelectedItem(iv300);
            }
        else {
            getSelectedItem(iv1500);
        }
    }

    @OnClick(R.id.ivBack)
    public void finishOnBack() {
        finish();
    }

    @OnClick(R.id.iv4000)
    public void changeiv4000() {
        getSelectedItem(iv4000);
    }

    @OnClick(R.id.iv1000)
    public void changeiv1000() {
        getSelectedItem(iv1000);
    }

    @OnClick(R.id.iv1500)
    public void changeiv1500() {
        getSelectedItem(iv1500);
    }

    @OnClick(R.id.iv300)
    public void changeiv300() {
        getSelectedItem(iv300);
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    @OnClick(R.id.btnSetGoal)
    public void setGoal() {
        sessionManager.setUserGoal(getSelectedGoal());

        if (getSelectedGoal() != null) {
            updateUSer();
            sessionManager.setGoalSelected();
        }
        startActivity(new Intent(getApplicationContext(), PremiumActivity.class));
    }

    private String getSelectedGoal() {
        if (iv300.getTag().equals(true)) {
            return "300";
        } else if (iv1000.getTag().equals(true)) {
            return "1000";
        } else if (iv1500.getTag().equals(true)) {
            return "1500";
        } else if (iv4000.getTag().equals(true)) {
            return "4000";
        }
        return "4000";
    }

    private void getSelectedItem(ImageView imageView) {
        iv300.setBackgroundResource(R.drawable.bg_filled_gray);
        iv1500.setBackgroundResource(R.drawable.bg_filled_gray);
        iv1000.setBackgroundResource(R.drawable.bg_filled_gray);
        iv4000.setBackgroundResource(R.drawable.bg_filled_gray);

        iv300.setTag(false);
        iv1500.setTag(false);
        iv1000.setTag(false);
        iv4000.setTag(false);

        imageView.setBackgroundResource(R.drawable.bg_gradient);
        imageView.setTag(true);
    }

    private void updateUSer() {

        APIInterface apiService = APIClient.getClient().create(APIInterface.class);

        String url = APIClient.BASE_URL;

        url += "updateUser.pl?user_id=" +
                getUserInfo(SessionManager.KEY_USER_ID) +
                "&session_id=" +
                getUserInfo(SessionManager.KEY_USER_SESSION_ID) +
                "&apiauth=" + APIClient.AUTH_KEY + "&goal_points=" + sessionManager.getUserGoal();

        Call<JsonObject> callObject = apiService.updateUserInfo1(url.replaceAll(" ", "%20"));

        callObject.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {

                try {
                    if (response.body() != null && !response.body().toString().trim().isEmpty()) {


                        JSONObject jsonObject = new JSONObject(response.body().toString());

                        if (Integer.parseInt(jsonObject.getString("success")) == 0) {
                            Global.showToast(SetGoalActivity.this, jsonObject.getString("msg"));
                        } else {

                            String user_achievements = jsonObject.getString("achievements");
                            String user_goal_points = jsonObject.getString("goal_points");

                            String user_day_streak = jsonObject.getString("day_streak");
                            String user_fb_user = jsonObject.getString("fb_user");
                            String user_points = jsonObject.getString("points");
                            String user_img_url = jsonObject.getString("img_url");

                            String user_annotations = jsonObject.getString("annotations");
                            String user_fb_id = jsonObject.getString("fb_id");
                            String user_username = jsonObject.getString("username");
                            String user_min_reading = jsonObject.getString("min_reading");
                            String user_id = jsonObject.getString("user_id");
                            String private_status = jsonObject.getString("private");
                            String newsletter = jsonObject.getString("newsletter");

                            sessionManager.setUserDailyPoints(Integer.parseInt(user_points));
                        }

                        dismissprogress();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                call.cancel();
                Global.dismissProgressDialog();

            }
        });
    }
}
