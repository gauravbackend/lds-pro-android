package com.ldsscriptures.pro.activity.settingmenu;

import android.app.UiModeManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.annotation.Nullable;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.SeekBar;
import android.widget.Switch;
import android.widget.TextView;

import com.ldsscriptures.pro.R;
import com.ldsscriptures.pro.activity.BaseActivity;
import com.ldsscriptures.pro.adapter.ViewPagerIndexAdapter;
import com.ldsscriptures.pro.service.AutoNightModeEventReceiver;
import com.ldsscriptures.pro.utils.SessionManager;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class SettingsViewOptionActivity extends BaseActivity implements SeekBar.OnSeekBarChangeListener, RadioGroup.OnCheckedChangeListener {

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.tbNightMode)
    Switch tbNightMode;

    @BindView(R.id.tbAutoNightMode)
    Switch tbAutoNightMode;

    @BindView(R.id.tvTitle)
    TextView tvTitle;
    @BindView(R.id.ivBack)
    ImageView ivBack;
    @BindView(R.id.ivMenu)
    ImageView ivMenu;
    @BindView(R.id.seekBar)
    SeekBar seekBar;
    @BindView(R.id.txtNumber)
    TextView txtNumber;
    @BindView(R.id.txtText)
    TextView txtText;
    @BindView(R.id.rbFiraSans)
    RadioButton rbFiraSans;
    @BindView(R.id.rbAlegreya)
    RadioButton rbAlegreya;
    @BindView(R.id.rbRobotoSlab)
    RadioButton rbRobotoSlab;
    @BindView(R.id.rbSourceSerif)
    RadioButton rbSourceSerif;
    @BindView(R.id.rbCambria)
    RadioButton rbCambria;
    @BindView(R.id.rbSourceSans)
    RadioButton rbSourceSans;
    @BindView(R.id.radioGroup)
    RadioGroup radioGroup;

    private SessionManager sessionManager;

    private Typeface tfFiraSans;
    private Typeface tfAlegreya;
    private Typeface tfRobotoSlab;
    private Typeface tfSourceSerif;
    private Typeface tfCambria;
    private Typeface tfSourceSans;

    public static SettingsViewOptionActivity settingsViewOptionActivity;

    public static SettingsViewOptionActivity getInstance() {
        return settingsViewOptionActivity;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_option);
        ButterKnife.bind(this);
        setContents();
    }

    private void setContents() {
        ButterKnife.bind(this);

        settingsViewOptionActivity = this;

        // set the back icon
        setVisibilityGone(ivMenu);
        setVisibilityVisible(ivBack);

        // set toolbar
        setToolbar(toolbar);
        showText(tvTitle, getString(R.string.view_option));
        sessionManager = new SessionManager(SettingsViewOptionActivity.this);

        // set font style
        tfFiraSans = Typeface.createFromAsset(getAssets(), getResources().getString(R.string.font_fira_sans));
        rbFiraSans.setTypeface(tfFiraSans);

        tfAlegreya = Typeface.createFromAsset(getAssets(), getResources().getString(R.string.font_alegreya));
        rbAlegreya.setTypeface(tfAlegreya);

        tfRobotoSlab = Typeface.createFromAsset(getAssets(), getResources().getString(R.string.font_roboto_slab));
        rbRobotoSlab.setTypeface(tfRobotoSlab);

        tfSourceSerif = Typeface.createFromAsset(getAssets(), getResources().getString(R.string.font_source_serif));
        rbSourceSerif.setTypeface(tfSourceSerif);

        tfCambria = Typeface.createFromAsset(getAssets(), getResources().getString(R.string.font_cambria));
        rbCambria.setTypeface(tfCambria);

        tfSourceSans = Typeface.createFromAsset(getAssets(), getResources().getString(R.string.font_source_sans));
        rbSourceSans.setTypeface(tfSourceSans);

        // set default font size
        seekBar.setMax(16);
        if (sessionManager.getFontStyle() == null) {
            sessionManager.setKeyFontStyle(getResources().getString(R.string.font_fira_sans));
        } else {
            if (sessionManager.getFontStyle().equalsIgnoreCase(getResources().getString(R.string.font_source_sans))) {
                txtNumber.setTypeface(tfSourceSans);
                txtText.setTypeface(tfSourceSans);
                rbSourceSans.setChecked(true);
            } else if (sessionManager.getFontStyle().equalsIgnoreCase(getResources().getString(R.string.font_alegreya))) {
                txtNumber.setTypeface(tfFiraSans);
                txtText.setTypeface(tfFiraSans);
                rbAlegreya.setChecked(true);
            } else if (sessionManager.getFontStyle().equalsIgnoreCase(getResources().getString(R.string.font_roboto_slab))) {
                txtNumber.setTypeface(tfRobotoSlab);
                txtText.setTypeface(tfRobotoSlab);
                rbRobotoSlab.setChecked(true);
            } else if (sessionManager.getFontStyle().equalsIgnoreCase(getResources().getString(R.string.font_source_serif))) {
                txtNumber.setTypeface(tfSourceSerif);
                txtText.setTypeface(tfSourceSerif);
                rbSourceSerif.setChecked(true);
            } else if (sessionManager.getFontStyle().equalsIgnoreCase(getResources().getString(R.string.font_cambria))) {
                txtNumber.setTypeface(tfCambria);
                txtText.setTypeface(tfCambria);
                rbCambria.setChecked(true);
            } else {
                txtNumber.setTypeface(tfFiraSans);
                txtText.setTypeface(tfFiraSans);
                rbFiraSans.setChecked(true);
            }
        }

        if (sessionManager.getFontSize() == 0) {
            txtNumber.setTextSize(16);
            txtNumber.setTextSize(16);
            sessionManager.setKeyFontSize(16);
            seekBar.setProgress(4);
        } else {
            // defalut text size and font style
            txtNumber.setTextSize(sessionManager.getFontSize());
            txtText.setTextSize(sessionManager.getFontSize());
            seekBar.setProgress(sessionManager.getFontSize());
            seekBar.setProgress(sessionManager.getFontSize() - 12);
        }


        // code for night mode
        if (sessionManager.isNightMode()) {
            tbNightMode.setChecked(true);
        } else {
            tbNightMode.setChecked(false);
        }

        tbNightMode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                UiModeManager uiManager = (UiModeManager) getSystemService(Context.UI_MODE_SERVICE);
                if (sessionManager.isNightMode()) {
                    uiManager.setNightMode(UiModeManager.MODE_NIGHT_NO);
                    SessionManager.setNightModeOff();
                    recreate();
                } else {
                    uiManager.setNightMode(UiModeManager.MODE_NIGHT_YES);
                    SessionManager.setNightModeOn();
                    recreate();
                }
            }
        });

        // code for auto night mode
        if (sessionManager.isAutoNightMode()) {
            tbAutoNightMode.setChecked(true);
        } else {
            tbAutoNightMode.setChecked(false);
        }

        tbAutoNightMode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (sessionManager.isAutoNightMode()) {
                    AutoNightModeEventReceiver.cancelAlarm(SettingsViewOptionActivity.this);
                    SessionManager.setAutoNightModeOn(false);
                } else {
                    AutoNightModeEventReceiver.setupAlarm(SettingsViewOptionActivity.this);
                    SessionManager.setAutoNightModeOn(true);
                }
            }
        });


        seekBar.setOnSeekBarChangeListener(this);
        radioGroup.setOnCheckedChangeListener(this);
    }

    public void changeMode(final boolean isOn) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                UiModeManager uiManager = (UiModeManager) getSystemService(Context.UI_MODE_SERVICE);
                if (isOn) {
                    SessionManager.setNightModeOn();
                    uiManager.setNightMode(UiModeManager.MODE_NIGHT_YES);
                    recreate();
                } else {
                    SessionManager.setNightModeOff();
                    uiManager.setNightMode(UiModeManager.MODE_NIGHT_NO);
                    recreate();
                }
            }
        });
    }

    @OnClick(R.id.ivBack)
    public void onViewClicked() {
        onBackPressed();
    }

    @Override
    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
        txtNumber.setTextSize(progress + 12);
        txtText.setTextSize(progress + 12);
        sessionManager.setKeyFontSize(progress + 12);
    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {
    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {
    }

    @Override
    public void onCheckedChanged(RadioGroup group, @IdRes int checkedId) {
        switch (checkedId) {
            case R.id.rbFiraSans:
                txtNumber.setTypeface(tfFiraSans);
                txtText.setTypeface(tfFiraSans);
                rbFiraSans.setChecked(true);
                sessionManager.setKeyFontStyle(getResources().getString(R.string.font_fira_sans));
                break;
            case R.id.rbAlegreya:
                txtNumber.setTypeface(tfAlegreya);
                txtText.setTypeface(tfAlegreya);
                rbAlegreya.setChecked(true);
                sessionManager.setKeyFontStyle(getResources().getString(R.string.font_alegreya));
                break;
            case R.id.rbRobotoSlab:
                txtNumber.setTypeface(tfRobotoSlab);
                txtText.setTypeface(tfRobotoSlab);
                rbRobotoSlab.setChecked(true);
                sessionManager.setKeyFontStyle(getResources().getString(R.string.font_roboto_slab));
                break;
            case R.id.rbSourceSerif:
                txtNumber.setTypeface(tfSourceSerif);
                txtText.setTypeface(tfSourceSerif);
                rbSourceSerif.setChecked(true);
                sessionManager.setKeyFontStyle(
                        getResources().getString(R.string.font_source_serif));
                break;
            case R.id.rbCambria:
                txtNumber.setTypeface(tfCambria);
                txtText.setTypeface(tfCambria);
                rbCambria.setChecked(true);
                sessionManager.setKeyFontStyle(getResources().getString(R.string.font_cambria));
                break;
            case R.id.rbSourceSans:
                txtNumber.setTypeface(tfSourceSans);
                txtText.setTypeface(tfSourceSans);
                rbSourceSans.setChecked(true);
                sessionManager.setKeyFontStyle(getResources().getString(R.string.font_source_sans));
                break;
        }
    }

    @Override
    public void onBackPressed() {
        if (ViewPagerIndexAdapter.getInstance() != null)
            ViewPagerIndexAdapter.getInstance().notifyFontStyleChange();
        super.onBackPressed();
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        setIntent(intent);
    }

    @Override
    protected void onResumeFragments() {
        super.onResumeFragments();
    }
}
