package com.ldsscriptures.pro.activity.studytools;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.SpannableStringBuilder;
import android.text.TextPaint;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.ldsscriptures.pro.R;
import com.ldsscriptures.pro.activity.BaseActivity;
import com.ldsscriptures.pro.activity.MainActivity;
import com.ldsscriptures.pro.activity.ReadingActivity;
import com.ldsscriptures.pro.activity.horizontalcalendar.HorizontalCalendar;
import com.ldsscriptures.pro.activity.horizontalcalendar.HorizontalCalendarListener;
import com.ldsscriptures.pro.activity.horizontalcalendar.HorizontalCalendarView;
import com.ldsscriptures.pro.database.HighlightDatabaseHelper;
import com.ldsscriptures.pro.model.StudyDailyVerse;
import com.ldsscriptures.pro.model.TimeLine.VerseDataTimeLineModel;
import com.ldsscriptures.pro.retroutils.APIClient;
import com.ldsscriptures.pro.retroutils.APIInterface;
import com.ldsscriptures.pro.utils.Global;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class StudyDailyActivity extends BaseActivity {

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.tvTitle)
    TextView tvTitle;

    @BindView(R.id.txt_study_daily_verse)
    TextView txtStudyDailyVerse;

    @BindView(R.id.txt_study_daily_verse_id)
    TextView txtStudyDailyVerseId;

    @BindView(R.id.txt_string_path)
    TextView txtStringPath;

    @BindView(R.id.txt_date_year)
    TextView txtDateYear;

    @BindView(R.id.layout_set_verse)
    LinearLayout layoutSetVerse;
    @BindView(R.id.txt_study_daily_no_verse)
    TextView txtStudyDailyNoVerse;
    @BindView(R.id.layout_set_no_verse)
    LinearLayout layoutSetNoVerse;
    @BindView(R.id.ivMenu)
    ImageView ivMenu;
    @BindView(R.id.ivBack)
    ImageView ivBack;
    @BindView(R.id.ivSearch)
    ImageView ivSearch;
    @BindView(R.id.ivTabs)
    ImageView ivTabs;
    @BindView(R.id.ivMore)
    ImageView ivMore;
    @BindView(R.id.calendarView)
    HorizontalCalendarView calendarView;

    private ArrayList<StudyDailyVerse> studyDailyArrayList = new ArrayList<>();
    private ArrayList<Date> dateArrayList = new ArrayList<>();

    HighlightDatabaseHelper highlightDb;
    String todayURI, currentDateTimeString, currentDate;
    HorizontalCalendar horizontalCalendar;
    String month = "0", dd = "0", yer = "0";
    Date startDate1, endDate1, getDate;
    VerseDataTimeLineModel verseDataTimeLineModel;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_studydaily);
        ButterKnife.bind(this);
        setContents();
    }

    private void setContents() {
        highlightDb = new HighlightDatabaseHelper(this);
        setToolbar(toolbar);
        setVisibilityGone(ivMore);
        setVisibilityGone(ivTabs);
        setVisibilityGone(ivSearch);

        showText(tvTitle, getResources().getString(R.string.DailyVerse));
        currentDate = new SimpleDateFormat("yyyy-MM-dd").format(Calendar.getInstance().getTime());
        currentDateTimeString = new SimpleDateFormat("yyyy-MM-dd").format(Calendar.getInstance().getTime());

        studyDailyArrayList.clear();
        studyDailyArrayList = highlightDb.getAllStudyDailyVerseData();

        if (studyDailyArrayList != null && studyDailyArrayList.size() != 0) {
            for (int i = 0; i < studyDailyArrayList.size(); i++) {
                setDateInArraylist(i);
            }
            firsSetCalendar();

        } else {
            if (Global.isNetworkAvailable(StudyDailyActivity.this)) {
                showprogress();
                getStudyDailyVerse_API();
            } else {

                Global.showNetworkAlert(this);

            }
        }
    }

    private void setDateInArraylist(int pos) {
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            getDate = sdf.parse(studyDailyArrayList.get(pos).getShowDate().toString());
        } catch (ParseException e) {
            e.printStackTrace();
        }

        dateArrayList.add(getDate);
    }

    private void getStudyDailyVerse_API() {

        studyDailyArrayList.clear();

        APIInterface apiService = APIClient.getClient().create(APIInterface.class);
        String url = APIClient.BASE_URL;

        url += "dailyVerse.pl?apiauth=" + APIClient.AUTH_KEY +
                "&lang=" + "eng";

        Call<ArrayList<StudyDailyVerse>> callObject = apiService.getStudyDailyVerse(url.replaceAll(" ", "%20"));

        callObject.enqueue(new Callback<ArrayList<StudyDailyVerse>>() {
            @Override
            public void onResponse(Call<ArrayList<StudyDailyVerse>> call, Response<ArrayList<StudyDailyVerse>> response) {


                if (response.body() != null && !response.body().toString().trim().isEmpty()) {

                    studyDailyArrayList = response.body();

                    for (int i = 0; i < studyDailyArrayList.size(); i++) {
                        setDateInArraylist(i);
                        highlightDb.addStudyDailyRecord(new StudyDailyVerse(studyDailyArrayList.get(i).getUri(), studyDailyArrayList.get(i).getShowDate()));
                    }

                    firsSetCalendar();
                    dismissprogress();
                }
            }

            @Override
            public void onFailure(Call<ArrayList<StudyDailyVerse>> call, Throwable t) {
                call.cancel();
                dismissprogress();
            }
        });
    }

    public void firsSetCalendar() {

        showprogress();
        try {
            Collections.sort(dateArrayList);
            SimpleDateFormat sdf = new SimpleDateFormat("EEE MMM dd HH:mm:ss z yyyy");
            SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd");
            startDate1 = sdf.parse(String.valueOf(dateArrayList.get(0)));
            endDate1 = sdf1.parse(currentDate);

        } catch (ParseException e) {
            e.printStackTrace();
        }

        final Calendar defaultDate = Calendar.getInstance();
        defaultDate.add(Calendar.YEAR, -1);
        defaultDate.add(Calendar.DAY_OF_WEEK, +5);

        horizontalCalendar = new HorizontalCalendar.Builder(StudyDailyActivity.this, R.id.calendarView)
                .startDate(startDate1)
                .endDate(endDate1)
                .datesNumberOnScreen(5)
                .dayNameFormat("EEE")
                .dayNumberFormat("dd")
                .monthFormat("MMM")
                .showDayName(true)
                .showMonthName(true)
                .defaultSelectedDate(defaultDate.getTime())
                .textColor(getResources().getColor(R.color.sub_item), getResources().getColor(R.color.textColor))
                .selectedDateBackground(Color.TRANSPARENT)
                .build();

        horizontalCalendar.goToday(true);
        setCalendar();

    }

    public void setCalendar() {

        horizontalCalendar.setCalendarListener(new HorizontalCalendarListener() {
            @Override
            public void onDateSelected(Date date, int position) {

                try {
                    currentDateTimeString = DateFormat.getDateInstance().format(date);
                    DateFormat srcDf = new SimpleDateFormat("EEE MMM dd HH:mm:ss z yyyy");
                    Date date1 = srcDf.parse(String.valueOf(date));
                    DateFormat destDf = new SimpleDateFormat("yyyy-MM-dd");
                    currentDateTimeString = destDf.format(date1);
                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                    Date currantDate = sdf.parse(currentDateTimeString);
                    Calendar cal = Calendar.getInstance();
                    cal.setTime(currantDate);
                    month = checkDigit(cal.get(Calendar.MONTH) + 1);
                    dd = checkDigit(cal.get(Calendar.DATE));
                    yer = checkDigit(cal.get(Calendar.YEAR));
                    txtDateYear.setText(yer);


                    for (int i = 0; i < studyDailyArrayList.size(); i++) {

                        if (currentDateTimeString.equals(studyDailyArrayList.get(i).getShowDate().toString())) {

                            todayURI = studyDailyArrayList.get(i).getUri().toString();
                            String[] fn = todayURI.split("\\.");

                            if (fn[1].trim().contains("title")) {
                                layoutSetVerse.setVisibility(View.GONE);
                                layoutSetNoVerse.setVisibility(View.VISIBLE);
                            } else {
                                verseDataTimeLineModel = new VerseDataTimeLineModel();

                                String verseId = fn[1].trim();//13-14
                                if (verseId.contains("-")) {
                                    String verseNoArr[] = verseId.split("-");
                                    if (verseNoArr.length != 0)
                                        verseId = verseNoArr[0];
                                }


                                verseDataTimeLineModel = getVerseText(fn[0], verseId);
                                String verseText = verseDataTimeLineModel.getVereseText();
                                String versepath = verseDataTimeLineModel.getVerseFullPath();
                                final String verseLcId = verseDataTimeLineModel.getLcId();
                                final String verseLsId = verseDataTimeLineModel.getLsId();
                                final String indexForReading = verseDataTimeLineModel.getIndexForReading();
                                final String verseAid = verseDataTimeLineModel.getVerseAid();

                                if (verseText != null || verseText.length() != 0) {
                                    layoutSetVerse.setVisibility(View.VISIBLE);
                                    layoutSetNoVerse.setVisibility(View.GONE);

                                    txtStudyDailyVerse.setText("");
                                    txtStudyDailyVerse.setText(verseText);

                                    if (versepath != null || versepath.length() != 0) {

                                        txtStudyDailyVerseId.setText(verseId.trim());
                                        versepath = versepath + ":" + verseId;

                                        int index = versepath.lastIndexOf("/");
                                        String imgName = versepath.substring(0, index) + "/";
                                        String substring2 = versepath.substring(index + 1, versepath.length());
                                        SpannableStringBuilder spanTxt = new SpannableStringBuilder(imgName);
                                        spanTxt.append(substring2);

                                        spanTxt.setSpan(new MyClickableSpan(verseLcId, verseLsId, indexForReading, verseAid, verseId), spanTxt.length() - substring2.length(), spanTxt.length(), 0);
                                        txtStringPath.setMovementMethod(LinkMovementMethod.getInstance());
                                        txtStringPath.setText(spanTxt, TextView.BufferType.SPANNABLE);
                                        txtStringPath.setHighlightColor(getResources().getColor(android.R.color.transparent));
                                    }

                                    final String finalverseAid = verseAid;
                                    final String finalVerseId = verseId;
                                    txtStudyDailyVerse.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            redirectReadingActivity(verseLcId, verseLsId, indexForReading, finalverseAid, finalVerseId);
                                        }
                                    });

                                } else {
                                    layoutSetVerse.setVisibility(View.GONE);
                                    layoutSetNoVerse.setVisibility(View.VISIBLE);
                                }
                            }
                        }
                    }


                    dismissprogress();

                } catch (Exception e) {
                    dismissprogress();
                    e.printStackTrace();
                }
            }
        });
    }

    public String checkDigit(int number) {
        return number <= 9 ? "0" + number : String.valueOf(number);
    }

    @OnClick(R.id.ivMenu)
    public void openMenu() {
        openSlideMenu();
    }

    private class MyClickableSpan extends ClickableSpan {

        String verseLcId;
        String verseLsId;
        String indexForReading;
        String verseAid;
        String verseId;

        public MyClickableSpan(String verseLcId, String verseLsId, String indexForReading, String verseAid, String verseId) {
            super();
            this.verseLcId = verseLcId;
            this.verseLsId = verseLsId;
            this.indexForReading = indexForReading;
            this.verseAid = verseAid;
            this.verseId = verseId;
        }

        public void onClick(View widget) {

            redirectReadingActivity(verseLcId, verseLsId, indexForReading, verseAid, verseId);
            TextView tv = (TextView) widget;
            tv.setHighlightColor(getResources().getColor(android.R.color.transparent));
        }

        public void updateDrawState(TextPaint ds) {// override updateDrawState
            ds.setUnderlineText(false); // set to false to remove underline
            ds.setColor(getResources().getColor(R.color.sand)); // set to false to remove underline
        }
    }

    public void redirectReadingActivity(String verseLcId, String verseLsId, String indexForReading, String verseAid, String verseId) {
        if (verseLcId != null || verseLsId != null || indexForReading != null) {

            Intent intent = new Intent(getApplicationContext(), ReadingActivity.class);
            intent.putExtra(getString(R.string.indexForReading), Integer.parseInt(indexForReading));
            intent.putExtra(getString(R.string.isFromIndex), true);
            intent.putExtra(getString(R.string.is_history), false);
            intent.putExtra(getString(R.string.lcID), verseLcId);
            intent.putExtra(getString(R.string.lsId), verseLsId);
            intent.putExtra(getString(R.string.verseAId), Integer.parseInt(verseAid));
            intent.putExtra(getString(R.string.verseId), Integer.parseInt(verseId));
            startActivity(intent);
            overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menuBack:
                finish();
                break;
            case R.id.menuNext:
                Toast.makeText(getApplicationContext(), "No Item Found", Toast.LENGTH_LONG).show();
                break;
            case R.id.menuBookmark:
                Intent bookmarkIntent = new Intent(getApplicationContext(), MainActivity.class);
                bookmarkIntent.putExtra(getString(R.string.menuSelection), "Bookmarks");
                bookmarkIntent.putExtra(getString(R.string.isFromReading), true);
                startActivityWithAnimation(bookmarkIntent);
                break;
            case R.id.menuHistory:
                Intent historyIntent = new Intent(getApplicationContext(), MainActivity.class);
                historyIntent.putExtra(getString(R.string.menuSelection), "History");
                historyIntent.putExtra(getString(R.string.isFromReading), true);
                startActivityWithAnimation(historyIntent);
                break;
        }
        return super.onOptionsItemSelected(item);
    }


}
