package com.ldsscriptures.pro.activity.studytools;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.ldsscriptures.pro.R;
import com.ldsscriptures.pro.activity.BaseActivity;
import com.ldsscriptures.pro.adapter.PlanListAdapter;
import com.ldsscriptures.pro.database.CommonDatabaseHelper;
import com.ldsscriptures.pro.model.PlanModel;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class PlanActivity extends BaseActivity {
    @BindView(R.id.ivMenu)
    ImageView ivMenu;
    @BindView(R.id.ivBack)
    ImageView ivBack;
    @BindView(R.id.tvTitle)
    TextView tvTitle;
    @BindView(R.id.ivSearch)
    ImageView ivSearch;
    @BindView(R.id.ivMore)
    ImageView ivMore;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    @BindView(R.id.followingfab)
    FloatingActionButton followingfab;
    @BindView(R.id.tvNoData)
    TextView tvNoData;
    @BindView(R.id.ivTabs)
    ImageView ivTabs;

    private ArrayList<PlanModel> newPlanArrayList = new ArrayList<>();
    CommonDatabaseHelper commonDatabaseHelper;
    private PlanListAdapter planListAdapter;
    static PlanActivity planActivity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_following_main);
        ButterKnife.bind(this);
        setContents();
    }

    public void setContents() {

        setVisibilityGone(ivTabs);
        setVisibilityVisible(ivSearch);

        commonDatabaseHelper = new CommonDatabaseHelper(this);

        planActivity = this;
        showText(tvTitle, getString(R.string.txt_myplans));

        Cursor cursor;
        cursor = commonDatabaseHelper.getPlanList();
        if (cursor != null && cursor.moveToFirst()) {
            while (!cursor.isAfterLast()) {

                PlanModel planModel = new PlanModel();
                planModel.setPlan_id(cursor.getString(cursor.getColumnIndex(CommonDatabaseHelper.KEY_PLAN_ID)));
                planModel.setPlan_name(cursor.getString(cursor.getColumnIndex(CommonDatabaseHelper.KEY_PLAN_NAME)));
                planModel.setPlan_Main_LCID(cursor.getString(cursor.getColumnIndex(CommonDatabaseHelper.KEY_PLAN_MAIN_LCID)));
                planModel.setPlan_Sub_LSID(cursor.getString(cursor.getColumnIndex(CommonDatabaseHelper.KEY_PLAN_SUB_LSID)));
                planModel.setPlan_days(cursor.getString(cursor.getColumnIndex(CommonDatabaseHelper.KEY_PLAN_DAYS)));
                planModel.setPlan_flag(cursor.getString(cursor.getColumnIndex(CommonDatabaseHelper.KEY_PLAN_FLAG)));
                planModel.setPlan_reading_minute(cursor.getString(cursor.getColumnIndex(CommonDatabaseHelper.KEY_PLAN_READING_MIN)));
                planModel.setPlan_created_date(cursor.getString(cursor.getColumnIndex(CommonDatabaseHelper.KEY_PLAN_CREATED_DATE)));
                newPlanArrayList.add(planModel);
                cursor.moveToNext();
            }
        }

        recyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);

        if (newPlanArrayList.size() != 0) {
            recyclerView.setVisibility(View.VISIBLE);
            tvNoData.setVisibility(View.GONE);

            planListAdapter = new PlanListAdapter(PlanActivity.this, newPlanArrayList);
            recyclerView.setAdapter(planListAdapter);
        } else {
            recyclerView.setVisibility(View.GONE);
            tvNoData.setVisibility(View.VISIBLE);
        }
    }

    @OnClick(R.id.ivMenu)
    public void openMenu() {
        openSlideMenu();
    }

    @OnClick(R.id.followingfab)
    public void onViewClicked() {
        startActivity(new Intent(getApplicationContext(), NewPlanActivity.class));
    }

    public static PlanActivity getInstance() {
        return planActivity;
    }
}
