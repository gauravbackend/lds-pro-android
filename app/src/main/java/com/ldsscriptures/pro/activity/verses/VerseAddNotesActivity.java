package com.ldsscriptures.pro.activity.verses;

import android.content.ContentValues;
import android.database.Cursor;
import android.os.Bundle;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.ldsscriptures.pro.R;
import com.ldsscriptures.pro.activity.BaseActivity;
import com.ldsscriptures.pro.database.CommonDatabaseHelper;
import com.ldsscriptures.pro.model.BookmarkData;
import com.ldsscriptures.pro.utils.Global;
import com.ldsscriptures.pro.utils.PushToServerData;
import com.ldsscriptures.pro.utils.SessionManager;
import com.ldsscriptures.pro.utils.Validator;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class VerseAddNotesActivity extends BaseActivity {

    @BindView(R.id.tvTitle)
    TextView tvTitle;
    @BindView(R.id.ivBack)
    ImageView ivBack;
    @BindView(R.id.tvDone)
    TextView tvDone;
    @BindView(R.id.edtNote)
    EditText edtNote;

    private String verseText;
    private String verseId;
    private String verseAId;
    private SessionManager sessionManager;
    ArrayList<BookmarkData> notesArrayList = new ArrayList<>();


    CommonDatabaseHelper commonDatabaseHelper;
    private BookmarkData notes;
    private int position;
    private boolean isEdit = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_verse_add_note);
        setContent();
    }

    private void setContent() {
        ButterKnife.bind(this);
        showText(tvTitle, getString(R.string.title_add_notes));
        sessionManager = new SessionManager(VerseAddNotesActivity.this);
        commonDatabaseHelper = new CommonDatabaseHelper(VerseAddNotesActivity.this);

        verseId = getIntent().getStringExtra(getString(R.string.verseId));
        verseText = getIntent().getStringExtra(getString(R.string.verseText));
        verseAId = getIntent().getStringExtra(getString(R.string.verseAId));

        notesArrayList = getNotes(verseAId);
        edtNote.setText(getNote());
        edtNote.setSelection(edtNote.getText().length());
    }

    private String getNote() {
        if (notesArrayList != null) {
            for (int i = 0; i < notesArrayList.size(); i++) {
                if (notesArrayList.get(i).getVerseText().equalsIgnoreCase(verseText)) {
                    String isDeleted = notesArrayList.get(i).getDeleted();
                    if (isDeleted.isEmpty()) {
                        showText(tvTitle, getString(R.string.title_edit_notes));
                        isEdit = true;
                        notes = notesArrayList.get(i);
                        position = i;
                        return notes.getTitle();
                    }
                }
            }
        }
        return "";
    }

    @OnClick(R.id.ivBack)
    public void finishActivity() {
        finish();
    }

    @OnClick(R.id.tvDone)
    public void addNote() {
        if (validateFields()) {
            String chapterUri = Global.bookmarkData.getChapterUri();
            String itemuri = Global.bookmarkData.getItemuri();
            String bookuri = Global.bookmarkData.getBookuri();
            String noteTitle = edtNote.getText().toString();

            if (isEdit) {

                notes.setTitle(noteTitle);
                notes.setVerseText(noteTitle);

                ContentValues contentValues = new ContentValues();
                contentValues.put("notetext", noteTitle);
                contentValues.put("versetitle", noteTitle);
                contentValues.put("modified", "1");

                commonDatabaseHelper.updateRowData(CommonDatabaseHelper.TABLE_NOTE_MARSTER, contentValues, CommonDatabaseHelper.KEY_VERSEAID + " = ? AND " + CommonDatabaseHelper.KEY_ITEMURI + " = ?",
                        new String[]{String.valueOf(notes.getVerseaid()), String.valueOf(notes.getItemuri())});

            } else {
                ContentValues contentValues = new ContentValues();
                contentValues.put("notetext", noteTitle);
                contentValues.put("versetitle", noteTitle);
                contentValues.put("versenumber", verseId);
                contentValues.put("versetext", verseText);
                contentValues.put("serverid", "0");
                contentValues.put("verseaid", verseAId);
                contentValues.put("chapteruri", chapterUri);
                contentValues.put("itemuri", itemuri);
                contentValues.put("bookuri", bookuri);
                contentValues.put("createdAt", Global.getCurrentDate());
                contentValues.put("deleted", "");
                contentValues.put("modified", "0");
                commonDatabaseHelper.insertData(CommonDatabaseHelper.TABLE_NOTE_MARSTER, contentValues);
                // add points
                sessionManager.setUserDailyPoints(50);
            }
            new PushToServerData(this);
            finish();
            if (VerseHighlightActivity.getInstance() != null) {
                VerseHighlightActivity.getInstance().goToReadingView();
            }
        }
    }

    private boolean validateFields() {
        if (!Validator.checkEmpty(edtNote)) {
            Global.showToast(VerseAddNotesActivity.this, getString(R.string.error_add_note));
            return false;
        }
        return true;
    }

    private ArrayList<BookmarkData> getNotes(String data_aid) {
        Cursor cursor;

        notesArrayList.clear();

        cursor = commonDatabaseHelper.getNotesList(data_aid);
        if (cursor != null && cursor.moveToFirst()) {
            while (!cursor.isAfterLast()) {

                BookmarkData noteskData = new BookmarkData();

                noteskData.setServerid(cursor.getString(cursor.getColumnIndex("serverid")));
                noteskData.setVerseaid(cursor.getString(cursor.getColumnIndex("verseaid")));
                noteskData.setChapterUri(cursor.getString(cursor.getColumnIndex("chapteruri")));
                noteskData.setItemuri(cursor.getString(cursor.getColumnIndex("itemuri")));
                noteskData.setBookuri(cursor.getString(cursor.getColumnIndex("bookuri")));
                noteskData.setTitle(cursor.getString(cursor.getColumnIndex("versetitle")));
                noteskData.setVerseNumber(cursor.getString(cursor.getColumnIndex("versenumber")));
                noteskData.setVerseText(cursor.getString(cursor.getColumnIndex("versetext")));
                noteskData.setDeleted(cursor.getString(cursor.getColumnIndex("deleted")));
                noteskData.setModified(cursor.getString(cursor.getColumnIndex("modified")));

                notesArrayList.add(noteskData);
                cursor.moveToNext();
            }
        }
        return notesArrayList;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finishActivity();
    }
}
