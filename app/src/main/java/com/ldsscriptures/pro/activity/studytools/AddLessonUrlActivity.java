package com.ldsscriptures.pro.activity.studytools;

import android.content.ContentValues;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.ldsscriptures.pro.R;
import com.ldsscriptures.pro.activity.BaseActivity;
import com.ldsscriptures.pro.database.CommonDatabaseHelper;
import com.ldsscriptures.pro.utils.Global;
import com.ldsscriptures.pro.utils.PushToServerData;
import com.ldsscriptures.pro.utils.SessionManager;
import com.ldsscriptures.pro.utils.Validator;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class AddLessonUrlActivity extends BaseActivity {
    @BindView(R.id.ivBack)
    ImageView ivBack;
    @BindView(R.id.tvTitle)
    TextView tvTitle;
    @BindView(R.id.ivMore)
    ImageView ivMore;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.edUrl)
    EditText edUrl;
    @BindView(R.id.edUrlTitle)
    EditText edUrlTitle;
    @BindView(R.id.tvDone)
    TextView tvDone;
    private CommonDatabaseHelper commonDatabaseHelper;
    private int lessonId;
    SessionManager sessionManager;
    // fix value assign from web-service
    int lessonType = 3;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_lessons_url);
        ButterKnife.bind(this);
        setContent();
    }

    private void setContent() {
        ButterKnife.bind(this);

        lessonId = getIntent().getIntExtra(getString(R.string.lesson_id), 0);

        setVisibilityVisible(ivMore);
        sessionManager = new SessionManager(this);
        //set toolbar
        setToolbar(toolbar);
        showText(tvTitle, getString(R.string.add_url));
        commonDatabaseHelper = new CommonDatabaseHelper(AddLessonUrlActivity.this);
    }

    private boolean validateFields() {
        if (!Validator.checkEmpty(edUrlTitle)) {
            Global.showSnackBar(AddLessonUrlActivity.this, getString(R.string.enter_title));
            return false;
        }
        if (!Validator.checkEmpty(edUrl)) {
            Global.showSnackBar(AddLessonUrlActivity.this, getString(R.string.enter_url));
            return false;
        }
        if (!Validator.isValidUrl(edUrl)) {
            Global.showSnackBar(AddLessonUrlActivity.this, getString(R.string.enter_valid_url));
            return false;
        }
        return true;
    }

    @OnClick({R.id.ivBack, R.id.tvDone})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.ivBack:
                finish();
                break;
            case R.id.tvDone:
                if (validateFields()) {
                    ContentValues contentValues = new ContentValues();
                    contentValues.put(CommonDatabaseHelper.KEY_LESSONID, lessonId);
                    contentValues.put(CommonDatabaseHelper.KEY_LESSONTYPE, lessonType);
                    contentValues.put(CommonDatabaseHelper.KEY_URLTITLE, edUrlTitle.getText().toString());
                    contentValues.put(CommonDatabaseHelper.KEY_URL, edUrl.getText().toString());
                    contentValues.put(CommonDatabaseHelper.KEY_SERVERID, "0");
                    contentValues.put(CommonDatabaseHelper.KEY_SORT_ORDER, "1");
                    contentValues.put("deleted", "");
                    contentValues.put("modified", "");

                    commonDatabaseHelper.insertData(CommonDatabaseHelper.TABLE_SUB_ITEM_LESSONS, contentValues);

                    new PushToServerData(this);

                    if (LessonSubItemActivity.getInstance() != null) {
                        LessonSubItemActivity.getInstance().setData();
                    }
                    finish();
                }
                break;
        }
    }


}
