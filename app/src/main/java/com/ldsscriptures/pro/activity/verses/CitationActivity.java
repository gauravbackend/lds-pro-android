package com.ldsscriptures.pro.activity.verses;

import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.widget.ImageView;
import android.widget.TextView;

import com.ldsscriptures.pro.R;
import com.ldsscriptures.pro.activity.BaseActivity;
import com.ldsscriptures.pro.adapter.CitationListAdapter;
import com.ldsscriptures.pro.database.CitationsDatabaseHelper;
import com.ldsscriptures.pro.model.CitationList;
import com.ldsscriptures.pro.utils.Global;
import com.ldsscriptures.pro.utils.SessionManager;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class CitationActivity extends BaseActivity {

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.tvTitle)
    TextView tvTitle;

    @BindView(R.id.ivBack)
    ImageView ivBack;

    @BindView(R.id.ivMore)
    ImageView ivMore;

    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;

    @BindView(R.id.tvNoData)
    TextView tvNoData;

    @BindView(R.id.fab)
    FloatingActionButton fab;

    private CitationsDatabaseHelper citationsDatabaseHelper;
    private ArrayList<CitationList> citationLists = new ArrayList<>();
    public String citation_itemUri, verseId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_citation);
        ButterKnife.bind(this);
        setContents();
    }

    private void setContents() {
        ButterKnife.bind(this);

        // set the dropdown option into toolbar
        citation_itemUri = getIntent().getStringExtra(getString(R.string.citation_uri));
        verseId = getIntent().getStringExtra(getString(R.string.verseId));

        citationsDatabaseHelper = new CitationsDatabaseHelper(CitationActivity.this);

        setToolbar(toolbar);

        showText(tvTitle, getString(R.string.citations));
        setVisibilityVisible(ivBack);

        setRecyclerView();
    }

    @OnClick(R.id.fab)
    public void manageFloatingButton() {
        manageFloatingBtn();
    }

    @OnClick(R.id.ivMore)
    public void showMore() {

    }

    @OnClick(R.id.ivBack)
    public void finishAct() {
        onBackPressed();
    }

    private void setRecyclerView() {
        new GetCitationList().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, "");
    }

    public void finishActivity() {
        finish();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    private class GetCitationList extends AsyncTask<String, Void, String> {
        @Override
        protected void onPreExecute() {
            Global.showProgressDialog(CitationActivity.this);
        }

        @Override
        protected String doInBackground(String... params) {
            Cursor cursor;
            cursor = citationsDatabaseHelper.getCitationList(citation_itemUri, verseId);

            citationLists = new ArrayList<>();
            if (cursor != null && cursor.moveToFirst()) {
                while (!cursor.isAfterLast()) {
                    CitationList citationList = new CitationList();

                    citationList.setId(cursor.getString(cursor.getColumnIndex(CitationsDatabaseHelper.KEY_ID)));
                    citationList.setName(cursor.getString(cursor.getColumnIndex(CitationsDatabaseHelper.KEY_NAME)));
                    citationList.setCnt(cursor.getString(cursor.getColumnIndex(CitationsDatabaseHelper.KEY_CNT)));
                    citationList.setRef_uri(cursor.getString(cursor.getColumnIndex(CitationsDatabaseHelper.KEY_REF_URI)));
                    citationList.setSource_group(cursor.getString(cursor.getColumnIndex(CitationsDatabaseHelper.KEY_SOURCE_GROUP)));

                    citationLists.add(citationList);
                    cursor.moveToNext();
                }
            }
            citationsDatabaseHelper.close();
            if (cursor != null) {
                cursor.close();
            }
            return "";
        }

        @Override
        protected void onPostExecute(String result) {
            Global.dismissProgressDialog();
            if (citationLists.size() != 0) {

                setVisibilityGone(tvNoData);
                setVisibilityVisible(recyclerView);
                RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(CitationActivity.this);
                recyclerView.setLayoutManager(mLayoutManager);

                CitationListAdapter journalsListAdapter = new CitationListAdapter(CitationActivity.this, citationLists, citation_itemUri,verseId);
                recyclerView.setAdapter(journalsListAdapter);
            } else {
                setVisibilityGone(recyclerView);
                setVisibilityVisible(tvNoData);
            }
        }
    }
}
