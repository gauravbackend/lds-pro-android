package com.ldsscriptures.pro.activity.subscriptionmenu;

import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewPager;

import com.ldsscriptures.pro.R;
import com.ldsscriptures.pro.activity.BaseActivity;
import com.ldsscriptures.pro.activity.subscriptionmenu.util.IabHelper;
import com.ldsscriptures.pro.activity.subscriptionmenu.util.IabResult;
import com.ldsscriptures.pro.activity.subscriptionmenu.util.Inventory;
import com.ldsscriptures.pro.activity.subscriptionmenu.util.Purchase;
import com.ldsscriptures.pro.adapter.PremiumPagerAdapter;
import com.ldsscriptures.pro.model.Premium;
import com.ldsscriptures.pro.utils.CirclePageIndicator;
import com.ldsscriptures.pro.utils.Global;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created on 17/08/17 by iblinfotech.
 */

public class PremiumActivity extends BaseActivity {

    @BindView(R.id.pager)
    ViewPager pager;
    @BindView(R.id.indicator)
    CirclePageIndicator indicator;

    private ArrayList<Premium> arrayList;
    static PremiumActivity premiumActivity;
    PremiumPagerAdapter pagerAdapter;

    static final String TAG = "SubscriptionTAG";
    static final String SKU_GET_YEAR = "yearly";
    static final String SKU_GET_MONTH = "monthly";

    /*static final String SKU_GET_YEAR = "android.test.purchased";
    static final String SKU_GET_MONTH = "android.test.purchased";*/
    static final int RC_REQUEST1 = 501;
    static final int RC_REQUEST2 = 502;

    boolean isPurchaseYear = false;
    boolean isPurchaseMonth = false;

    IabHelper mHelper;
    String payload = "";


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_premium);
        ButterKnife.bind(this);
        setContents();
    }

    private void setContents() {

        premiumActivity = this;
        String base64EncodedPublicKey = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAnu7nbbHFxIyTPsfZjcK7i8XhevxSr794y8jZt7zOV/lZgKo3gu7eYpXUJyYVL3Ur6EcWABJMfnZ86HrCnJR59bHbG4iPT0pOkYtPbPwsXp8xkmzHI1l7/2eds8dzCrbZ1ETlUF2vV3JsHKx3e7zXHdR/J/n1navwCrdeyz+f+p5vhJ6SS2GWOHTI/zeMRlPOZhkUgKFUhdIp1b2Pk1doLs03U8qjGLmVkIKjic8yJmtPHawoOQJI59j4A6EC/v6tem3WGCLlGekV2ZEI+4VH1AkCVE1Rk5O5wU7WSz9+2GzQmORO83dbbBLi1+OqSFydDTZU1qUvoFrGPFUPEcjYTQIDAQAB";

        mHelper = new IabHelper(PremiumActivity.this, base64EncodedPublicKey);
        mHelper.enableDebugLogging(true);
        mHelper.startSetup(new IabHelper.OnIabSetupFinishedListener() {
            public void onIabSetupFinished(IabResult result) {
                if (!result.isSuccess()) {
                    complain("Problem setting up in-app billing: " + result);
                    return;
                }

                if (mHelper == null) return;
                mHelper.queryInventoryAsync(mGotInventoryListener);
            }
        });

        arrayList = new ArrayList<>();

        Premium premium = new Premium();
        premium.setTitle(getString(R.string.premium_title_1));
        premium.setSubTitle(getString(R.string.premium_subtitle_1));
        arrayList.add(premium);

        premium = new Premium();
        premium.setTitle(getString(R.string.premium_title_2));
        premium.setSubTitle(getString(R.string.premium_subtitle_2));
        arrayList.add(premium);

        premium = new Premium();
        premium.setTitle(getString(R.string.premium_title_3));
        premium.setSubTitle(getString(R.string.premium_subtitle_3));
        arrayList.add(premium);

        premium = new Premium();
        premium.setTitle(getString(R.string.premium_title_4));
        premium.setSubTitle(getString(R.string.premium_subtitle_4));
        arrayList.add(premium);

        premium = new Premium();
        premium.setTitle(getString(R.string.premium_title_5));
        premium.setSubTitle(getString(R.string.premium_subtitle_5));
        arrayList.add(premium);

        pagerAdapter = new PremiumPagerAdapter(PremiumActivity.this, arrayList);
        pager.setAdapter(pagerAdapter);
        indicator.setViewPager(pager);
    }

    // Called when consumption is complete
    IabHelper.OnConsumeFinishedListener consumeFinishedListener = new IabHelper.OnConsumeFinishedListener() {
        public void onConsumeFinished(Purchase purchase, IabResult result) {
            //Global.printLog(TAG, "Consumption finished. Purchase: " + purchase + ", result: " + result);

            // if we were disposed of in the meantime, quit.
            if (mHelper == null) return;

            // We know this is the "gas" sku because it's the only one we consume,
            // so we don't check which sku was consumed. If you have more than one
            // sku, you probably should check...
            if (result.isSuccess()) {
                // successfully consumed, so we apply the effects of the item in our
                // game world's logic, which in our case means filling the gas tank a bit
                //Global.printLog(TAG, "Consumption successful. Provisioning.");
            } else {
                complain("Error while consuming: " + result);
            }
            //updateUi();
            //Global.printLog(TAG, "End consumption flow.");
        }
    };

    // Listener that's called when we finish querying the items and subscriptions we own
    IabHelper.QueryInventoryFinishedListener mGotInventoryListener = new IabHelper.QueryInventoryFinishedListener() {
        public void onQueryInventoryFinished(IabResult result, Inventory inventory) {
            //Global.printLog(TAG, "Query inventory finished.");

            // Have we been disposed of in the meantime? If so, quit.
            if (mHelper == null) return;

            // Is it a failure?
            if (result.isFailure()) {
                complain("Failed to query inventory: " + result);
                return;
            }

            //Global.printLog(TAG, "Query inventory was successful.");

            Purchase getYearPurchase = inventory.getPurchase(SKU_GET_YEAR);
            isPurchaseYear = (getYearPurchase != null && verifyDeveloperPayload(getYearPurchase));
            //Global.printLog(TAG, "User " + (isPurchaseYear ? "HAS" : "DOES NOT HAVE") + " Purchase Yearly Subsription.");
            //Global.printLog("getYearPurchase====================", "" + getYearPurchase);
            if (getYearPurchase != null && verifyDeveloperPayload(getYearPurchase)) {
                mHelper.consumeAsync(getYearPurchase, consumeFinishedListener);
            }

            Purchase getMonthPurchase = inventory.getPurchase(SKU_GET_MONTH);
            isPurchaseMonth = (getMonthPurchase != null && verifyDeveloperPayload(getMonthPurchase));
            //Global.printLog(TAG, "User " + (isPurchaseMonth ? "HAS" : "DOES NOT HAVE") + " Purchase Monthly Subscription.");
            //Global.printLog("getMonthPurchase====================", "" + getMonthPurchase);
            if (getMonthPurchase != null && verifyDeveloperPayload(getMonthPurchase)) {
                mHelper.consumeAsync(getMonthPurchase, consumeFinishedListener);
            }

/*
            if (inventory.hasPurchase(SKU_GET_MONTH)) {

                mHelper.consumeAsync(inventory.getPurchase(SKU_GET_MONTH), null);
            }*/
            //Global.printLog(TAG, "Initial inventory query finished; enabling main UI.");
        }
    };

    // Callback for when a purchase is finished
    public IabHelper.OnIabPurchaseFinishedListener purchaseFinishedListener = new IabHelper.OnIabPurchaseFinishedListener() {
        public void onIabPurchaseFinished(IabResult result, Purchase purchase) {
            //Global.printLog(TAG, "Purchase finished: " + result + ", purchase: " + purchase);

            // if we were disposed of in the meantime, quit.
            if (mHelper == null) return;

            if (result.isFailure()) {
                complain("Error purchasing: " + result);
                return;
            }

            if (!verifyDeveloperPayload(purchase)) {
                complain("Error purchasing. Authenticity verification failed.");
                return;
            }

            mHelper.consumeAsync(purchase, consumeFinishedListener);
        }
    };

    // Verifies the developer payload of a purchase.
    boolean verifyDeveloperPayload(Purchase purchase) {
        String payload = purchase.getDeveloperPayload();

        /*
         * TODO: verify that the developer payload of the purchase is correct. It will be
         * the same one that you sent when initiating the purchase.
         *
         * WARNING: Locally generating a random string when starting a purchase and
         * verifying it here might seem like a good approach, but this will fail in the
         * case where the user purchases an item on one device and then uses your app on
         * a different device, because on the other device you will not have access to the
         * random string you originally generated.
         *
         * So a good developer payload has these characteristics:
         *
         * 1. If two different users purchase an item, the payload is different between them,
         *    so that one user's purchase can't be replayed to another user.
         *
         * 2. The payload must be such that you can verify it even when the app wasn't the
         *    one who initiated the purchase flow (so that items purchased by the user on
         *    one device work on other devices owned by the user).
         *
         * Using your own server to store and verify developer payloads across app
         * installations is recommended.
         */

        return true;
    }

    // Enables or disables the "please wait" screen.
    void complain(String message) {
        Global.printLog(TAG, "**** TrivialDrive Error: " + message);
        Global.printLog("PremiumActivity", "Error: " + message);
    }

    void alert(String message) {
        AlertDialog.Builder bld = new AlertDialog.Builder(PremiumActivity.this);
        bld.setMessage(message);
        bld.setNeutralButton("OK", null);
        Global.printLog(TAG, "Showing alert dialog: " + message);
        bld.create().show();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        Global.printLog(TAG, "Destroying helper.");
        if (mHelper != null) {
            try {
                mHelper.dispose();
            } catch (IllegalArgumentException ex) {
                ex.printStackTrace();
            } finally {
            }
            mHelper = null;
        }
    }

    public static PremiumActivity getInstance() {
        return premiumActivity;
    }

    public void purchaseYear() {

        try {
            if (mHelper != null) {
                mHelper.flagEndAsync();
                //mHelper.launchPurchaseFlow(PremiumActivity.this, SKU_GET_YEAR, RC_REQUEST1, purchaseFinishedListener, payload);
                mHelper.launchSubscriptionPurchaseFlow(PremiumActivity.this, SKU_GET_YEAR, RC_REQUEST1, purchaseFinishedListener, payload);

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void purchaseMonth() {
        try {
            if (mHelper != null) {
                mHelper.flagEndAsync();
                //mHelper.launchPurchaseFlow(PremiumActivity.this, SKU_GET_YEAR, RC_REQUEST1, purchaseFinishedListener, payload);
                mHelper.launchSubscriptionPurchaseFlow(PremiumActivity.this, SKU_GET_MONTH, RC_REQUEST2, purchaseFinishedListener, payload);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (mHelper != null && !mHelper.handleActivityResult(requestCode, resultCode, data)) {
            Global.printLog("on Activity", "this is on activity result --- with data finsh");

            super.onActivityResult(requestCode, resultCode, data);
        } else {
            Global.printLog(TAG, "onActivityResult handled by IABUtil.");
        }
    }
}
