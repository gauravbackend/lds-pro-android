package com.ldsscriptures.pro.activity.verses;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.text.SpannableString;
import android.text.style.BackgroundColorSpan;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.ldsscriptures.pro.R;
import com.ldsscriptures.pro.activity.BaseActivity;
import com.ldsscriptures.pro.activity.ReadingActivity;
import com.ldsscriptures.pro.activity.ShareAnnotationsActivity;
import com.ldsscriptures.pro.adapter.BottomVerseEditAdapter;
import com.ldsscriptures.pro.adapter.FillColorAdapter;
import com.ldsscriptures.pro.database.HighlightDatabaseHelper;
import com.ldsscriptures.pro.model.ColorData;
import com.ldsscriptures.pro.model.SpannedTextData;
import com.ldsscriptures.pro.model.SubMenu;
import com.ldsscriptures.pro.utils.Global;
import com.ldsscriptures.pro.utils.PushToServerData;
import com.ldsscriptures.pro.utils.SessionManager;
import com.ldsscriptures.pro.view.ColoredUnderlineSpan;
import com.ldsscriptures.pro.view.SelectableTextViewNew.ObservableScrollView;
import com.ldsscriptures.pro.view.SelectableTextViewNew.SelectableTextViewNew;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class VerseHighlightActivity extends BaseActivity {
    @BindView(R.id.ivBack)
    ImageView ivBack;
    @BindView(R.id.ivMenu)
    ImageView ivMenu;
    @BindView(R.id.tvTitle)
    TextView tvTitle;
    @BindView(R.id.ivCheck)
    ImageView ivCheck;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.tvVerseId)
    TextView tvVerseId;
    @BindView(R.id.tvVerseNameNew)
    SelectableTextViewNew tvVerseNameNew;
    @BindView(R.id.scroller)
    ObservableScrollView scroller;
    @BindView(R.id.tvPreview)
    TextView tvPreview;
    @BindView(R.id.rcColors)
    RecyclerView rcColors;
    @BindView(R.id.tvHighlight)
    TextView tvHighlight;
    @BindView(R.id.tvUnderline)
    TextView tvUnderline;
    @BindView(R.id.tvClearAll)
    TextView tvClearAll;
    @BindView(R.id.ivCitation)
    ImageView ivCitation;
    @BindView(R.id.tvSharedAnnotation)
    TextView tvSharedAnnotation;
    @BindView(R.id.rcBottomMenus)
    RecyclerView rcBottomMenus;
    @BindView(R.id.linBottomMenu)
    LinearLayout linBottomMenu;
    @BindView(R.id.lnr_citation)
    LinearLayout lnrCitation;
    @BindView(R.id.lnr_sharedAnnonation)
    LinearLayout lnrSharedAnnonation;
    @BindView(R.id.ivEdit)
    ImageView ivEdit;
    @BindView(R.id.ivMore)
    ImageView ivMore;
    @BindView(R.id.txtCitation_Count)
    TextView txtCitationCount;
    boolean highendindexFlag = false;
    boolean underlineendindexFlag = false;

    private int mTouchX;
    private int mTouchY;
    private final static int DEFAULT_SELECTION_LEN = 5;
    static VerseHighlightActivity verseEditActivity;
    private SpannableString myStringNew;
    private int cursorStart;
    private int cursorEnd;
    ArrayList<BackgroundColorSpan> bgColorSpanArrayList = new ArrayList<>();
    ArrayList<ColoredUnderlineSpan> underLineSpanArrayList = new ArrayList<>();

    private String verseText;
    private String verseId;
    private String verseAId;
    private String citation_itemUri;
    private String citationCount;
    private String title, stringTag;
    private int indexId, versePos;
    SessionManager sessionManager;
    HighlightDatabaseHelper highlightDb;
    String myString;
    private SpannedTextData spannedTextData;
    private String verseText1, readingtitle;

    private int highlight_cursorStart;
    private int highlight_cursorEnd;
    private int underline_cursorStart;
    private int underline_cursorEnd;

    private int serverhighlight_cursorStart;
    private int serverhighlight_cursorEnd;
    private int serverunderline_cursorStart;
    private int serverunderline_cursorEnd;
    ArrayList<SpannedTextData> spanArrayList = new ArrayList<>();

    public static VerseHighlightActivity getInstance() {
        return verseEditActivity;
    }

    @Override
    protected void onCreate(Bundle savedInsstanceState) {
        super.onCreate(savedInsstanceState);
        setContentView(R.layout.activity_verse_highlight);
        ButterKnife.bind(this);
        setContents();
    }

    private void setContents() {

        ButterKnife.bind(this);
        verseEditActivity = this;

        verseText = getIntent().getStringExtra(getString(R.string.verseText));
        verseId = getIntent().getStringExtra(getString(R.string.verseId));
        verseAId = getIntent().getStringExtra(getString(R.string.verseAId));
        indexId = getIntent().getIntExtra(getString(R.string.indexId), 0);
        versePos = getIntent().getIntExtra(getString(R.string.versePos), 0);
        citation_itemUri = getIntent().getStringExtra(getString(R.string.itemUri));
        citationCount = getIntent().getStringExtra(getString(R.string.citationCount));
        stringTag = getIntent().getStringExtra("stringTag");
        readingtitle = getIntent().getStringExtra("readingtitle");
        title = readingtitle + ":" + verseId;

        sessionManager = new SessionManager(VerseHighlightActivity.this);
        highlightDb = new HighlightDatabaseHelper(this);

        if (citationCount != null && !citationCount.isEmpty() && !citationCount.equals("null") && !citationCount.equals("0")) {
            txtCitationCount.setVisibility(View.VISIBLE);
            txtCitationCount.setText(citationCount);
        } else {
            txtCitationCount.setVisibility(View.GONE);
        }

        setBottomMenus();
        setColors();
        showText(tvVerseId, verseId);
        showText(tvTitle, readingtitle);

        myString = verseText.replace(verseId, "");
        myStringNew = new SpannableString(Html.fromHtml(myString) + " ");

        spanArrayList = highlightDb.getAllHighLightData();
        ShowHighlightUnderline();

        tvVerseNameNew.setDefaultSelectionColor(getResources().getColor(R.color.sky));
        tvVerseNameNew.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                showSelectionCursors(mTouchX, mTouchY);
                return true;
            }
        });
        tvVerseNameNew.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tvVerseNameNew.hideCursor();

                tvVerseNameNew.getCursorSelection().setStart(0);
                tvVerseNameNew.getCursorSelection().setEnd(0);
            }
        });
        tvVerseNameNew.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                mTouchX = (int) event.getX();
                mTouchY = (int) event.getY();
                return false;
            }
        });
    }

    private void showSelectionCursors(int x, int y) {

        try {
            int start = tvVerseNameNew.getPreciseOffset(x, y);

            if (start > -1 && start < tvVerseNameNew.getText().length()) {
                start = WordSelect(start);
                int end = start + DEFAULT_SELECTION_LEN;

                end = tvVerseNameNew.getText().toString().indexOf(" ", end);

                if (end >= tvVerseNameNew.getText().length()) {
                    end = tvVerseNameNew.getText().length() - 1;
                }

                tvVerseNameNew.removeSelection();
                tvVerseNameNew.hideCursor();
                tvVerseNameNew.showSelectionControls(start, end);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private int WordSelect(int start) {

        try {
            if (!(tvVerseNameNew.getText().toString().substring(start, start + 1).equalsIgnoreCase(" "))) {
                for (int i = start; i >= 0; i--) {
                    if (tvVerseNameNew.getText().toString().substring(i, i + 1).equalsIgnoreCase(" ")) {
                        return i + 1;
                    }
                }
            } else {
                return WordSelect(start - 1);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return 0;
    }

    private void setBottomMenus() {
        List<SubMenu> bookMarkIcons = new ArrayList<SubMenu>();
        bookMarkIcons.add(new SubMenu(R.drawable.bookmarks, getString(R.string.bookmarkVerse)));
        bookMarkIcons.add(new SubMenu(R.drawable.edit, getString(R.string.addNote)));
        bookMarkIcons.add(new SubMenu(R.drawable.tag, getString(R.string.tagVerse)));
        bookMarkIcons.add(new SubMenu(R.drawable.link, getString(R.string.crossReference)));
        bookMarkIcons.add(new SubMenu(R.drawable.tabs, getString(R.string.copyVerse)));
        bookMarkIcons.add(new SubMenu(R.drawable.timeline_share, getString(R.string.timeline_share)));
        bookMarkIcons.add(new SubMenu(R.drawable.share, getString(R.string.share)));

        rcBottomMenus.setLayoutManager(new LinearLayoutManager(VerseHighlightActivity.this, LinearLayoutManager.HORIZONTAL, false));
        verseText1 = verseText.replaceAll("&nbsp;", "");
        rcBottomMenus.setAdapter(new BottomVerseEditAdapter(VerseHighlightActivity.this, bookMarkIcons, verseId, verseText1, verseAId, readingtitle, citation_itemUri, indexId, versePos, citation_itemUri, stringTag));
    }

    private void setColors() {
        ArrayList<ColorData> colorsArray = new ArrayList<>();
        colorsArray.add(new ColorData("#d83a35", false));
        colorsArray.add(new ColorData("#Ec7235", false));
        colorsArray.add(new ColorData("#Fbf351", true));
        colorsArray.add(new ColorData("#98c256", false));
        colorsArray.add(new ColorData("#4faaef", false));
        colorsArray.add(new ColorData("#27539f", false));
        colorsArray.add(new ColorData("#7f63a2", false));
        colorsArray.add(new ColorData("#E79dbd", false));
        colorsArray.add(new ColorData("#Bd9c72", false));
        colorsArray.add(new ColorData("#b6b6b6", false));

        rcColors.setLayoutManager(new LinearLayoutManager(VerseHighlightActivity.this, LinearLayoutManager.HORIZONTAL, false));
        FillColorAdapter fillColorAdapter = new FillColorAdapter(VerseHighlightActivity.this, colorsArray);
        rcColors.setAdapter(fillColorAdapter);
    }

    @OnClick(R.id.ivBack)
    void backButton() {
        onBackPressed();
    }

    @OnClick(R.id.tvClearAll)
    void clearAll() {

        for (int i = 0; i < bgColorSpanArrayList.size(); i++) {
            myStringNew.removeSpan(bgColorSpanArrayList.get(i));
        }
        for (int i = 0; i < underLineSpanArrayList.size(); i++) {
            myStringNew.removeSpan(underLineSpanArrayList.get(i));
        }
        myStringNew = new SpannableString(Html.fromHtml(myString));
        tvVerseNameNew.setText(myStringNew);
        tvVerseNameNew.removeSelection();
        tvVerseNameNew.hideCursor();
        underLineSpanArrayList.clear();
        bgColorSpanArrayList.clear();
        tvVerseNameNew.getCursorSelection().setStart(0);
        tvVerseNameNew.getCursorSelection().setEnd(0);
        highlight_cursorStart = 0;
        highlight_cursorEnd = 0;
        underline_cursorStart = 0;
        underline_cursorEnd = 0;
        serverhighlight_cursorStart = 0;
        serverhighlight_cursorEnd = 0;
        serverunderline_cursorStart = 0;
        serverunderline_cursorEnd = 0;

        highlightDb.deleteHighlightData(verseAId);

        new PushToServerData(this);
    }

    private boolean isTextSelected() {
        cursorStart = tvVerseNameNew.getCursorSelection().getStart();
        cursorEnd = tvVerseNameNew.getCursorSelection().getEnd();
        if (cursorStart >= 0 && cursorEnd > 0) {
            return true;
        } else {
            return false;
        }
    }

    @OnClick(R.id.tvUnderline)
    void underline() {
        try {
            if (isTextSelected()) {
                underlineProcess(myStringNew, cursorStart, cursorEnd);
            } else {
                if (myStringNew.toString().substring(myStringNew.length() - 1, myStringNew.length()).equalsIgnoreCase(" ")) {
                    underlineendindexFlag = true;
                    underlineProcess(myStringNew, 0, myStringNew.length() - 1);

                } else {
                    underlineendindexFlag = true;
                    underlineProcess(myStringNew, 0, myStringNew.length());

                }
            }
        } catch (Exception e) {

        }
    }

    @OnClick(R.id.tvHighlight)
    void setHighlight() {
        try {
            if (isTextSelected()) {
                highlightProcess(myStringNew, cursorStart, cursorEnd);
            } else {
                if (myStringNew.toString().substring(myStringNew.length() - 1, myStringNew.length()).equalsIgnoreCase(" ")) {
                    highendindexFlag = true;
                    highlightProcess(myStringNew, 0, myStringNew.length() - 1);

                } else {
                    highendindexFlag = true;
                    highlightProcess(myStringNew, 0, myStringNew.length());
                }

            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void highlightProcess(SpannableString myStringNew, int cursorStart, int cursorEnd) {

        if (cursorStart > cursorEnd) {

            cursorStart = cursorStart + cursorEnd;  // x now becomes 15
            cursorEnd = cursorStart - cursorEnd;  // y becomes 10
            cursorStart = cursorStart - cursorEnd;  // x becomes 5
        }

        highlight_cursorStart = cursorStart;
        highlight_cursorEnd = cursorEnd;

        if (FillColorAdapter.selectedColor != null) {

            BackgroundColorSpan backgroundColorSpan = new BackgroundColorSpan(Color.parseColor(Global.addLetter(FillColorAdapter.selectedColor, "80", 1)));
            bgColorSpanArrayList.add(backgroundColorSpan);
            myStringNew.setSpan(backgroundColorSpan, highlight_cursorStart, highlight_cursorEnd, 0);
            tvVerseNameNew.setText(myStringNew);
            tvVerseNameNew.hideCursor();

            String selectedString = "";
            selectedString = myStringNew.toString().substring(highlight_cursorStart, highlight_cursorEnd).trim();
            String[] selectedWordsArray = selectedString.split("\\s+");

            String beforeselectedString = "";
            beforeselectedString = myStringNew.toString().substring(0, highlight_cursorStart).trim();
            String[] beforeselectedWordsArray = beforeselectedString.split("\\s+");

            serverhighlight_cursorStart = beforeselectedWordsArray.length;
            serverhighlight_cursorEnd = ((selectedWordsArray.length) + (beforeselectedWordsArray.length - 1));

            if (highendindexFlag == true) {

                spannedTextData = new SpannedTextData(
                        Integer.parseInt(verseAId),
                        0,
                        -1,
                        title,
                        FillColorAdapter.selectedColor,
                        0, "0", "", "", verseId, Global.bookmarkData.getItemuri());

            } else {

                spannedTextData = new SpannedTextData(
                        Integer.parseInt(verseAId),
                        serverhighlight_cursorStart,
                        serverhighlight_cursorEnd,
                        title,
                        FillColorAdapter.selectedColor, 0, "0", "", "", verseId, Global.bookmarkData.getItemuri());
            }
            highlightDb.addHighLightData(spannedTextData);
            highendindexFlag = false;
            new PushToServerData(this);

            if (sessionManager.isHighlight == false) {
                sessionManager.setUserDailyPoints(50);
                sessionManager.setHighLightCount(1);
            }

        } else {
            Global.showToast(VerseHighlightActivity.this, getString(R.string.select_color_first));
        }
    }

    public void underlineProcess(SpannableString myStringNew, int cursorStart, int cursorEnd) {

        if (cursorStart > cursorEnd) {

            cursorStart = cursorStart + cursorEnd;  // x now becomes 15
            cursorEnd = cursorStart - cursorEnd;  // y becomes 10
            cursorStart = cursorStart - cursorEnd;  // x becomes 5
        }

        underline_cursorStart = cursorStart;
        underline_cursorEnd = cursorEnd;

        if (FillColorAdapter.selectedColor != null) {

            ColoredUnderlineSpan underlineSpan = new ColoredUnderlineSpan(Color.parseColor(FillColorAdapter.selectedColor));
            underLineSpanArrayList.add(underlineSpan);
            myStringNew.setSpan(underlineSpan, underline_cursorStart, underline_cursorEnd, 0);
            tvVerseNameNew.setText(myStringNew);
            tvVerseNameNew.hideCursor();

            String selectedString = "";
            selectedString = myStringNew.toString().substring(underline_cursorStart, underline_cursorEnd).trim();
            String[] selectedWordsArray = selectedString.split("\\s+");

            String beforeselectedString = "";
            beforeselectedString = myStringNew.toString().substring(0, underline_cursorStart).trim();
            String[] beforeselectedWordsArray = beforeselectedString.split("\\s+");

            serverunderline_cursorStart = beforeselectedWordsArray.length;
            serverunderline_cursorEnd = ((selectedWordsArray.length) + (beforeselectedWordsArray.length - 1));

            if (underlineendindexFlag == true) {

                spannedTextData = new SpannedTextData(
                        Integer.parseInt(verseAId),
                        0,
                        -1,
                        title,
                        FillColorAdapter.selectedColor,
                        1, "0", "", "", verseId, Global.bookmarkData.getItemuri());

            } else {

                spannedTextData = new SpannedTextData(
                        Integer.parseInt(verseAId),
                        serverunderline_cursorStart,
                        serverunderline_cursorEnd,
                        title,
                        FillColorAdapter.selectedColor,
                        1, "0", "", "", verseId, Global.bookmarkData.getItemuri());
            }

            highlightDb.addHighLightData(spannedTextData);
            underlineendindexFlag = false;
            new PushToServerData(this);

        } else {
            Global.showToast(VerseHighlightActivity.this, getString(R.string.select_color_first));
        }
    }

    public void ShowHighlightUnderline() {

        if (spanArrayList != null) {
            for (int i = 0; i < spanArrayList.size(); i++) {

                if (verseAId.contains(String.valueOf(spanArrayList.get(i).getVerseAId()))) {

                    String isDeleted = spanArrayList.get(i).getDeleted();
                    if (isDeleted == null) {
                        isDeleted = "";
                    }
                    if (isDeleted.equalsIgnoreCase("")) {

                        if (spanArrayList.get(i).getHighlightflag() == 0) {

                            String[] allStringWordsArray = myStringNew.toString().split("\\s+");
                            int startIndex = 0;
                            startIndex = spanArrayList.get(i).getStartIndex();
                            int endIndex = 0;
                            endIndex = spanArrayList.get(i).getEndIndex();

                            String finalString = "";

                            if (startIndex == 0 && endIndex == -1) {
                                myStringNew.setSpan(new BackgroundColorSpan(Color.parseColor(Global.addLetter(spanArrayList.get(i).getColor(), "80", 1))), 0, myStringNew.length(), 0);
                            } else {

                                if (startIndex == 1) {

                                    startIndex = 0;
                                    endIndex = endIndex - 1;

                                    for (int j = 0; j < allStringWordsArray.length; j++) {

                                        if (j >= startIndex) {
                                            if (j <= endIndex) {
                                                finalString = finalString + " " + allStringWordsArray[j].toString();
                                            }
                                        }
                                    }

                                    if (finalString != null) {

                                        String[] mysplitarray = finalString.toString().split("\\s+");
                                        int startpos = myStringNew.toString().indexOf(mysplitarray[0]);
                                        int endpos = startpos + finalString.length();
                                        myStringNew.setSpan(new BackgroundColorSpan(Color.parseColor(Global.addLetter(spanArrayList.get(i).getColor(), "80", 1))), startpos, endpos, 0);
                                    }
                                } else {
                                    for (int j = 0; j < allStringWordsArray.length; j++) {

                                        if (j >= startIndex) {
                                            if (j <= endIndex) {
                                                finalString = finalString + " " + allStringWordsArray[j].toString();
                                            }
                                        }
                                    }

                                    if (finalString != null) {

                                        Matcher match = Pattern.compile(Pattern.quote(finalString.trim()), Pattern.UNICODE_CASE).matcher(myStringNew.toString());
                                        while (match.find()) {
                                            myStringNew.setSpan(new BackgroundColorSpan(Color.parseColor(Global.addLetter(spanArrayList.get(i).getColor(), "80", 1))), match.start(), (match.end()), 0);
                                        }
                                    }
                                }
                            }

                        } else {
                            String[] allStringWordsArray = myStringNew.toString().split("\\s+");
                            int startIndex = 0;
                            startIndex = spanArrayList.get(i).getStartIndex();
                            int endIndex = 0;
                            endIndex = spanArrayList.get(i).getEndIndex();

                            String finalString = "";

                            if (startIndex == 0 && endIndex == -1) {
                                myStringNew.setSpan(new ColoredUnderlineSpan(Color.parseColor(Global.addLetter(spanArrayList.get(i).getColor(), "", 1))), 0, myStringNew.length(), 0);
                            } else {

                                if (startIndex == 1) {

                                    startIndex = 0;
                                    endIndex = endIndex - 1;

                                    for (int j = 0; j < allStringWordsArray.length; j++) {

                                        if (j >= startIndex) {
                                            if (j <= endIndex) {
                                                finalString = finalString + " " + allStringWordsArray[j].toString();
                                            }
                                        }

                                    }

                                    if (finalString != null) {
                                        String[] mysplitarray = finalString.toString().split("\\s+");
                                        int startpos = myStringNew.toString().indexOf(mysplitarray[0]);
                                        int endpos = startpos + finalString.length();
                                        myStringNew.setSpan(new ColoredUnderlineSpan(Color.parseColor(Global.addLetter(spanArrayList.get(i).getColor(), "", 1))), startpos, endpos, 0);
                                    }
                                } else {
                                    for (int j = 0; j < allStringWordsArray.length; j++) {

                                        if (j >= startIndex) {
                                            if (j <= endIndex) {
                                                finalString = finalString + " " + allStringWordsArray[j].toString();
                                            }
                                        }
                                    }

                                    if (finalString != null) {

                                        Matcher match = Pattern.compile(Pattern.quote(finalString.trim()), Pattern.UNICODE_CASE).matcher(myStringNew.toString());
                                        while (match.find()) {
                                            myStringNew.setSpan(new ColoredUnderlineSpan(Color.parseColor(Global.addLetter(spanArrayList.get(i).getColor(), "", 1))), match.start(), (match.end()), 0);
                                        }
                                    }
                                }
                            }
                        }
                        sessionManager.isHighlight = true;
                    } else {
                        sessionManager.isHighlight = false;
                    }
                }

            }
            tvVerseNameNew.setText(myStringNew);
        } else {
            tvVerseNameNew.setText(myStringNew);
        }
    }
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        goToReadingView();
    }

    @OnClick({R.id.lnr_citation, R.id.lnr_sharedAnnonation})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.lnr_citation:
                Intent i = new Intent(getApplicationContext(), CitationActivity.class);
                i.putExtra(getString(R.string.citation_uri), citation_itemUri);
                i.putExtra(getString(R.string.verseId), verseId);
                startActivityWithAnimation(i);
                break;

            case R.id.lnr_sharedAnnonation:
                Intent intent = new Intent(VerseHighlightActivity.this, ShareAnnotationsActivity.class);
                intent.putExtra(getString(R.string.verseId), verseId);
                intent.putExtra(getString(R.string.verseAId), verseAId);
                startActivityWithAnimation(intent);
                break;
        }
    }

    public void goToReadingView() {
        ReadingActivity.getInstance().setContents(Integer.valueOf(indexId));
        Global.recycleviewpos = Integer.parseInt(verseId);
        finish();
    }
}
