package com.ldsscriptures.pro.activity.verses;

import android.os.Bundle;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.ldsscriptures.pro.R;
import com.ldsscriptures.pro.activity.BaseActivity;
import com.ldsscriptures.pro.model.VerseTagsData;
import com.ldsscriptures.pro.utils.Global;
import com.ldsscriptures.pro.utils.PushToServerData;
import com.ldsscriptures.pro.utils.SessionManager;
import com.ldsscriptures.pro.utils.Validator;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class VerseNewTagActivity extends BaseActivity {

    @BindView(R.id.tvVerseId)
    TextView tvVerseId;

    @BindView(R.id.tvVerseName)
    TextView tvVerseName;

    @BindView(R.id.tvDone)
    TextView tvDone;

    @BindView(R.id.tvTitle)
    TextView tvTitle;

    @BindView(R.id.ivCheck)
    ImageView ivCheck;

    @BindView(R.id.ivBack)
    ImageView ivBack;

    @BindView(R.id.edtTag)
    EditText edtTag;
    private SessionManager sessionManager;
    ArrayList<VerseTagsData> verseTagArrayList = new ArrayList<>();

    private String verseText;
    private String verseId;
    private String verseAid;
    private String tagTitle;
    private String title;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_verse_new_tag);
        setContent();
    }

    private void setContent() {
        ButterKnife.bind(this);

        sessionManager = new SessionManager(VerseNewTagActivity.this);

        verseId = getIntent().getStringExtra(getString(R.string.verseId));
        verseText = getIntent().getStringExtra(getString(R.string.verseText));
        verseAid = getIntent().getStringExtra(getString(R.string.verseAId));
        title = getIntent().getStringExtra("title");
        showText(tvTitle, getString(R.string.title_new_tag));
    }

    @OnClick(R.id.ivBack)
    public void finishActivity() {
        finish();
    }

    @OnClick(R.id.tvDone)
    public void addNewTag() {
        if (validateFields()) {
            verseTagArrayList = sessionManager.getPreferencesTagsArrayList(VerseNewTagActivity.this);

            tagTitle = edtTag.getText().toString();

            String TagTitle = title + ":" + verseId;

            VerseTagsData verseTagsData = new VerseTagsData();
            verseTagsData.setVerseaid(verseAid);
            verseTagsData.setTagname(tagTitle);
            verseTagsData.setVerseNumber(verseId);
            verseTagsData.setTagTitle(TagTitle);
            verseTagsData.setVerseText(verseText);
            verseTagsData.setChapterUri(Global.bookmarkData.getChapterUri());
            verseTagsData.setItemuri(Global.bookmarkData.getItemuri());
            verseTagsData.setBookuri(Global.bookmarkData.getBookuri());
            verseTagsData.setDeleted("");
            verseTagsData.setModified("");
            verseTagsData.setServerid("0");

            if (getTag().length() > 0) {
                if (verseTagArrayList != null) {
                    for (int i = 0; i < verseTagArrayList.size(); i++) {
                        if (verseTagArrayList.get(i).getTagTitle().equals(getTag())) {
                            Global.showToast(this, "Already Added");
                            break;
                        }
                    }
                }
            } else {
                if (verseTagArrayList != null) {
                    verseTagArrayList.add(verseTagsData);
                } else {
                    verseTagArrayList = new ArrayList<>();
                    verseTagArrayList.add(verseTagsData);
                }

                sessionManager.setUserDailyPoints(50);
                sessionManager.setTagCount(1);
            }

            sessionManager.setPreferencesTagsArrayList(VerseNewTagActivity.this, verseTagArrayList);
            finish();
            new PushToServerData(this);
        }

    }

    private String getTag() {
        if (verseTagArrayList != null) {
            for (int i = 0; i < verseTagArrayList.size(); i++) {
                if (verseTagArrayList.get(i).getTagTitle().equalsIgnoreCase(tagTitle)) {
                    return verseTagArrayList.get(i).getTagTitle();
                }
            }
        }
        return "";
    }

    private boolean validateFields() {
        if (!Validator.checkEmpty(edtTag)) {
            Toast.makeText(VerseNewTagActivity.this, getString(R.string.error_bookmark), Toast.LENGTH_LONG).show();
            return false;
        }
        return true;
    }


}
