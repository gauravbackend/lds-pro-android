package com.ldsscriptures.pro.activity;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.widget.TextView;

import com.ldsscriptures.pro.R;
import com.ldsscriptures.pro.adapter.ShareAnnotationsPagerAdapter;
import com.ldsscriptures.pro.model.CrossRefrenceData;
import com.ldsscriptures.pro.model.NotesData;
import com.ldsscriptures.pro.model.ShareAnnotationsData;
import com.ldsscriptures.pro.model.TagsData;
import com.ldsscriptures.pro.retroutils.APIClient;
import com.ldsscriptures.pro.retroutils.APIInterface;
import com.ldsscriptures.pro.utils.Global;
import com.ldsscriptures.pro.utils.SessionManager;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class ShareAnnotationsActivity extends BaseActivity {

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.tabLayout)
    TabLayout tabLayout;

    @BindView(R.id.viewPager)
    ViewPager viewPager;
    @BindView(R.id.tvTitle)
    TextView tvTitle;

    private String verseId;
    private String verseAId;

    public static ArrayList<TagsData> tagsDataArrayList = new ArrayList<>();
    public static ArrayList<NotesData> notesDataArrayList = new ArrayList<>();
    public static ArrayList<CrossRefrenceData> crossRefrenceDataArrayList = new ArrayList<>();

    public static ShareAnnotationsActivity shareAnnotationsActivity;

    public static ShareAnnotationsActivity getInstance() {
        return shareAnnotationsActivity;
    }

    public ArrayList<TagsData> getTagsDataArrayList() {
        return tagsDataArrayList;
    }

    public void setTagsDataArrayList(ArrayList<TagsData> tagsDataArrayList) {
        this.tagsDataArrayList = tagsDataArrayList;
    }

    public ArrayList<NotesData> getNotesDataArrayList() {
        return notesDataArrayList;
    }

    public void setNotesDataArrayList(ArrayList<NotesData> notesDataArrayList) {
        this.notesDataArrayList = notesDataArrayList;
    }

    public ArrayList<CrossRefrenceData> getCrossRefrenceDataArrayList() {
        return crossRefrenceDataArrayList;
    }

    public void setCrossRefrenceDataArrayList(ArrayList<CrossRefrenceData> crossRefrenceDataArrayList) {
        this.crossRefrenceDataArrayList = crossRefrenceDataArrayList;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_share_annotations);
        ButterKnife.bind(this);
        setContents();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    private void setContents() {
        ButterKnife.bind(this);
        shareAnnotationsActivity = this;

        verseId = getIntent().getStringExtra(getString(R.string.verseId));
        verseAId = getIntent().getStringExtra(getString(R.string.verseAId));

        setToolbar(toolbar);

        showText(tvTitle, ReadingActivity.title + ":" + verseId);

        tabLayout.addTab(tabLayout.newTab().setText(getString(R.string.notes).toUpperCase()));
        tabLayout.addTab(tabLayout.newTab().setText(getString(R.string.txt_tags).toUpperCase()));
        tabLayout.addTab(tabLayout.newTab().setText(getString(R.string.cross_references).toUpperCase()));


        if (Global.isNetworkAvailable(this)) {
            getShareAnnotationData();
        } else {
            Global.showNetworkAlert(ShareAnnotationsActivity.this);
        }
    }

    private void getShareAnnotationData() {
        APIInterface apiService = APIClient.getClient().create(APIInterface.class);
        String url = APIClient.BASE_URL;

        url += "community.pl?user_id=" + getUserInfo(SessionManager.KEY_USER_ID) +
                "&session_id=" + getUserInfo(SessionManager.KEY_USER_SESSION_ID) +
                "&locale=" + Global.LANGUAGE +
                "&apiauth=" + APIClient.AUTH_KEY +
                "&aid=" + verseAId +
                "&verse=" + verseId +
                "&uri=" + Global.bookmarkData.getItemuri();

        Call<ShareAnnotationsData> callObject = apiService.getShareAnnotationData(url.replaceAll(" ", "%20"));
        callObject.enqueue(new Callback<ShareAnnotationsData>() {
            @Override
            public void onResponse(Call<ShareAnnotationsData> call, Response<ShareAnnotationsData> response) {
                setNotesDataArrayList(response.body().getNotes());
                setTagsDataArrayList(response.body().getTags());
                setCrossRefrenceDataArrayList(response.body().getCross_refs());

                ShareAnnotationsPagerAdapter adapter = new ShareAnnotationsPagerAdapter(getSupportFragmentManager(), tabLayout.getTabCount());
                viewPager.setAdapter(adapter);
                tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
                    @Override
                    public void onTabSelected(TabLayout.Tab tab) {
                        viewPager.setCurrentItem(tab.getPosition());
                    }

                    @Override
                    public void onTabUnselected(TabLayout.Tab tab) {

                    }

                    @Override
                    public void onTabReselected(TabLayout.Tab tab) {

                    }
                });

                viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
                    public void onPageScrollStateChanged(int state) {
                    }

                    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                    }

                    public void onPageSelected(int position) {
                        tabLayout.getTabAt(position).select();
                    }
                });
            }

            @Override
            public void onFailure(Call<ShareAnnotationsData> call, Throwable t) {
                call.cancel();
            }
        });
    }

    @OnClick(R.id.ivBack)
    public void onViewClicked() {
        finish();
    }
}
