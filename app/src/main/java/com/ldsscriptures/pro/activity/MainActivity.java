package com.ldsscriptures.pro.activity;


import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.crashlytics.android.Crashlytics;
import com.google.gson.JsonObject;
import com.ldsscriptures.pro.BuildConfig;
import com.ldsscriptures.pro.R;
import com.ldsscriptures.pro.adapter.LibraryCollectionPagerAdapter;
import com.ldsscriptures.pro.database.DataBaseHelper;
import com.ldsscriptures.pro.model.Item;
import com.ldsscriptures.pro.model.LcData;
import com.ldsscriptures.pro.model.ReadingNow.LibCollection;
import com.ldsscriptures.pro.model.ReadingNowData;
import com.ldsscriptures.pro.retroutils.APIClient;
import com.ldsscriptures.pro.retroutils.APIInterface;
import com.ldsscriptures.pro.retroutils.RetrieveDBListResponse;
import com.ldsscriptures.pro.service.ActivityEventReceiver;
import com.ldsscriptures.pro.service.PointEventReceiver;
import com.ldsscriptures.pro.utils.Global;
import com.ldsscriptures.pro.utils.SessionManager;

import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Set;
import java.util.TreeSet;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.fabric.sdk.android.Fabric;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.ldsscriptures.pro.model.ReadingNowData.readingNowData;


public class MainActivity extends BaseActivity implements TabLayout.OnTabSelectedListener {

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.tabLayout)
    TabLayout tabLayout;
    @BindView(R.id.viewPager)
    ViewPager viewPager;
    @BindView(R.id.ivTabs)
    ImageView ivTabs;
    @BindView(R.id.ivMenu)
    ImageView ivMenu;
    @BindView(R.id.ivBack)
    ImageView ivBack;
    @BindView(R.id.tvTitle)
    TextView tvTitle;
    @BindView(R.id.ivSearch)
    ImageView ivSearch;
    @BindView(R.id.ivMore)
    ImageView ivMore;
    @BindView(R.id.fab)
    FloatingActionButton fab;
    @BindView(R.id.lin_main_root)
    RelativeLayout linMainRoot;
    private ArrayList<Item> dbCatelogArraylist = new ArrayList<>();

    private DataBaseHelper databasehelper;
    private boolean doubleBackToExitPressedOnce = false;
    private boolean isFromReading;
    private SessionManager sessionManager;
    private ArrayList<LcData> lcDataArrayList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        setContents();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    private void setContents() {

        ButterKnife.bind(this);
        sessionManager = new SessionManager(MainActivity.this);
        sessionManager.setPremiumSelected(true);
        databasehelper = new DataBaseHelper(this);
        setToolbar(toolbar);
        ivMore.setVisibility(View.GONE);
        Global.IS_FROM_READING_NOW = false;
        ReadingNowData.tagArrayList.clear();
        ReadingNowData.tagArrayList.add("LibCollection");
        readingNowData = new ReadingNowData();
        readingNowData.setLibCollection(new LibCollection(this.getClass().getName()));

        tabLayout.addTab(tabLayout.newTab().setText(getString(R.string.library).toUpperCase()));
        tabLayout.addTab(tabLayout.newTab().setText(getString(R.string.bookmarks).toUpperCase()));
        tabLayout.addTab(tabLayout.newTab().setText(getString(R.string.history).toUpperCase()));

        LibraryCollectionPagerAdapter adapter = new LibraryCollectionPagerAdapter(getSupportFragmentManager(), tabLayout.getTabCount());
        viewPager.setAdapter(adapter);
        tabLayout.setOnTabSelectedListener(this);
        viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            public void onPageScrollStateChanged(int state) {
            }

            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            public void onPageSelected(int position) {
                tabLayout.getTabAt(position).select();
            }
        });

        if (getIntent() != null)
            if (getIntent().getBooleanExtra(getString(R.string.isFromReading), false))
                isFromReading = true;
            else
                isFromReading = false;

        // this method is to handle menu item selection
        if (getIntent() != null) {
            if (getIntent().getExtras() != null) {
                String menuSelection = getIntent().getExtras().getString(getString(R.string.menuSelection));
                if (menuSelection != null) {
                    switch (menuSelection) {
                        case "Reading Now":
                            viewPager.setCurrentItem(0);
                            break;
                        case "Library":
                            viewPager.setCurrentItem(0);
                            break;
                        case "Bookmarks":
                            viewPager.setCurrentItem(1);
                            break;
                        case "History":
                            viewPager.setCurrentItem(2);
                            break;
                        default:
                            viewPager.setCurrentItem(0);
                    }
                }
            }
        }

        takeScreenshot();
        PointEventReceiver.setupAlarm(getApplicationContext());
        ActivityEventReceiver.setupAlarm(getApplicationContext());
        getDatabaseVersion();
    }

    @OnClick(R.id.fab)
    public void manageFloatingButton() {
        manageFloatingBtn();
    }

    public void takeScreenshot() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                capture();
            }
        }, 2500);
    }

    private void capture() {
        try {
            String state = Environment.getExternalStorageState();
            File mFileTemp;
            if (Environment.MEDIA_MOUNTED.equals(state)) {
                mFileTemp = new File(Environment.getExternalStorageDirectory(), "Main Screen.jpg");
            } else {
                mFileTemp = new File(getFilesDir(), "Main Screen.jpg");
            }
            String mPath = mFileTemp.getAbsolutePath();

            View v1 = getWindow().getDecorView().findViewById(R.id.lin_main_root);

            v1.setDrawingCacheEnabled(true);
            v1.buildDrawingCache(true);
            Bitmap bitmap = Bitmap.createBitmap(v1.getDrawingCache());
            v1.setDrawingCacheEnabled(false);
            File imageFile = new File(mPath);
            FileOutputStream outputStream = new FileOutputStream(imageFile);
            int quality = 100;
            bitmap.compress(Bitmap.CompressFormat.JPEG, quality, outputStream);
            outputStream.flush();
            outputStream.close();
            Global global = new Global(MainActivity.this);
            Global.setPrefrenceString(MainActivity.this, Global.ADD_NEW_TAB_PATH, mPath);
            Global.setPrefrenceString(MainActivity.this, Global.ADD_NEW_TAB_CLASS_NAME, this.getClass().getName());

            ArrayList<ReadingNowData> readingNowDataArrayList = sessionManager.getPreferencesArrayList(MainActivity.this);

            if (readingNowDataArrayList == null) {

                readingNowDataArrayList = new ArrayList<>();

                ReadingNowData readingNowData = new ReadingNowData();
                if (Global.getPrefrenceString(MainActivity.this, Global.ADD_NEW_TAB_PATH, "") != null && Global.getPrefrenceString(MainActivity.this, Global.ADD_NEW_TAB_CLASS_NAME, "") != null) {
                    readingNowData.setPath(Global.getPrefrenceString(MainActivity.this, Global.ADD_NEW_TAB_PATH, ""));
                    readingNowData.setTag(Global.getPrefrenceString(MainActivity.this, Global.ADD_NEW_TAB_CLASS_NAME, ""));
                }

                readingNowData.setActivityIdentifier(getString(R.string.tabs_lib_collection));
                readingNowDataArrayList.add(0, readingNowData);

            } else {
                for (int i = 0; i < readingNowDataArrayList.size(); i++) {

                    if (readingNowDataArrayList.get(i).getTag().equals(this.getClass().getName())) {

                    } else {
                        ReadingNowData readingNowData = new ReadingNowData();
                        readingNowData.setPath(Global.takeScreenshot(this, 24));
                        readingNowData.setTag(this.getClass().getName());
                        readingNowData.setBookmarked(readingNowDataArrayList.get(0).isBookmarked());
                        readingNowData.setActivityIdentifier(getString(R.string.tabs_lib_collection));
                        readingNowDataArrayList.set(0, readingNowData);
                    }
                }
            }

            sessionManager.setPreferencesArrayList(MainActivity.this, readingNowDataArrayList);

        } catch (Throwable e) {
            e.printStackTrace();
        }
    }

    @OnClick(R.id.ivMenu)
    public void openMenu() {
        openSlideMenu();
    }

    @Override
    public void onTabSelected(TabLayout.Tab tab) {
        viewPager.setCurrentItem(tab.getPosition());
    }

    @Override
    public void onTabUnselected(TabLayout.Tab tab) {
    }

    @Override
    public void onTabReselected(TabLayout.Tab tab) {
    }

    @Override
    public void onBackPressed() {
        if (!isFromReading) {
            if (doubleBackToExitPressedOnce) {
                super.onBackPressed();
                return;
            }
            this.doubleBackToExitPressedOnce = true;

            Toast.makeText(getApplicationContext(), R.string.press_to_exit, Toast.LENGTH_LONG).show();
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    doubleBackToExitPressedOnce = false;
                }
            }, 2000);
        } else {
            finish();
        }
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        setIntent(intent);
    }

    @Override
    protected void onResumeFragments() {
        super.onResumeFragments();
    }

    @OnClick({R.id.ivSearch})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.ivSearch:
                startActivityWithAnimation(new Intent(MainActivity.this, SearchActivity.class));
                break;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menuBack:
                finish();
                break;
            case R.id.menuNext:
                if (Global.moreOptionMenuDatas.get(0).getLcID() != null) {
                    Intent intent = new Intent(getApplicationContext(), LibrarySelectionTabsActivity.class);
                    intent.putExtra(getString(R.string.lcID), Global.moreOptionMenuDatas.get(0).getLcID());
                    intent.putExtra(getString(R.string.lcname), Global.moreOptionMenuDatas.get(0).getLcTitle());
                    startActivityWithAnimation(intent);
                } else {
                    Toast.makeText(getApplicationContext(), "No Item Found", Toast.LENGTH_LONG).show();
                }
                break;
            case R.id.menuBookmark:
                Intent bookmarkIntent = new Intent(getApplicationContext(), MainActivity.class);
                bookmarkIntent.putExtra(getString(R.string.menuSelection), "Bookmarks");
                bookmarkIntent.putExtra(getString(R.string.isFromReading), true);
                startActivityWithAnimation(bookmarkIntent);
                break;
            case R.id.menuHistory:
                Intent historyIntent = new Intent(getApplicationContext(), MainActivity.class);
                historyIntent.putExtra(getString(R.string.menuSelection), "History");
                historyIntent.putExtra(getString(R.string.isFromReading), true);
                startActivityWithAnimation(historyIntent);
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void getDatabaseVersion() {

        APIInterface apiService = APIClient.getClient().create(APIInterface.class);
        String url = APIClient.BASE_URL;

        url += "latestDB.pl?device=" + APIClient.DEVICE_KEY +
                "&appversion=" + BuildConfig.VERSION_NAME +
                "&apiauth=" + APIClient.AUTH_KEY;

        Call<JsonObject> callObject = apiService.getDBVersion(url);

        callObject.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {

                if (response.body() != null && !response.body().toString().trim().isEmpty()) {
                    Global.DB_VERSION = response.body().get("dbversion").toString();
                    retriveDatabse();
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                call.cancel();
            }
        });
    }

    private void retriveDatabse() {

        dbCatelogArraylist.clear();

        APIInterface apiService = APIClient.getClient().create(APIInterface.class);

        String url = APIClient.BASE_URL;

        url += "prodb3.pl?catalog=" + Global.DB_VERSION +
                "&lang=eng" +
                "&apiauth=" + APIClient.AUTH_KEY;

        Call<RetrieveDBListResponse<Item>> callObject = apiService.retrieveDB(url);

        callObject.enqueue(new Callback<RetrieveDBListResponse<Item>>() {
            @Override
            public void onResponse(Call<RetrieveDBListResponse<Item>> call, Response<RetrieveDBListResponse<Item>> response) {

                if (response.body() != null && !response.body().toString().trim().isEmpty()) {
                    dbCatelogArraylist = response.body().getItem();
                    dbCatelogArraylist = sortValues(dbCatelogArraylist);
                    Global.saveListToSharedPreferneces(getApplicationContext(), dbCatelogArraylist);
                }
            }

            @Override
            public void onFailure(Call<RetrieveDBListResponse<Item>> call, Throwable t) {
                call.cancel();
            }
        });
    }

    public ArrayList<Item> sortValues(ArrayList<Item> list) {
        Set set = new TreeSet(new Comparator() {

            @Override
            public int compare(Object o1, Object o2) {
                return ((Item) o1).getId().compareTo(((Item) o2).getId());
            }
        });
        set.addAll(list);

        final ArrayList newList = new ArrayList(set);
        return newList;
    }

    @OnClick(R.id.ivTabs)
    public void openTabs() {

        ArrayList<ReadingNowData> readingNowDataArrayList = sessionManager.getPreferencesArrayList(MainActivity.this);

        if (readingNowDataArrayList == null) {

            readingNowDataArrayList = new ArrayList<>();

            ReadingNowData readingNowData = new ReadingNowData();
            if (Global.getPrefrenceString(MainActivity.this, Global.ADD_NEW_TAB_PATH, "") != null && Global.getPrefrenceString(MainActivity.this, Global.ADD_NEW_TAB_CLASS_NAME, "") != null) {
                readingNowData.setPath(Global.getPrefrenceString(MainActivity.this, Global.ADD_NEW_TAB_PATH, ""));
                readingNowData.setTag(Global.getPrefrenceString(MainActivity.this, Global.ADD_NEW_TAB_CLASS_NAME, ""));
            }

            readingNowData.setActivityIdentifier(getString(R.string.tabs_lib_collection));
            readingNowDataArrayList.add(0, readingNowData);

        } else {
            for (int i = 0; i < readingNowDataArrayList.size(); i++) {

                if (readingNowDataArrayList.get(i).getTag().equals(this.getClass().getName())) {

                } else {

                    ReadingNowData readingNowData = new ReadingNowData();
                    readingNowData.setPath(Global.takeScreenshot(this, 24));
                    readingNowData.setTag(this.getClass().getName());
                    readingNowData.setBookmarked(readingNowDataArrayList.get(0).isBookmarked());
                    readingNowData.setActivityIdentifier(getString(R.string.tabs_lib_collection));
                    readingNowDataArrayList.set(0, readingNowData);

                }
            }
        }

        sessionManager.setPreferencesArrayList(MainActivity.this, readingNowDataArrayList);
        startActivity(new Intent(this, TabsActivity.class));
        overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
    }
}
