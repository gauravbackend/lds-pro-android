package com.ldsscriptures.pro.activity.activitymenu;

import android.content.Intent;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.ldsscriptures.pro.R;
import com.ldsscriptures.pro.activity.BaseActivity;
import com.ldsscriptures.pro.activity.MainActivity;
import com.ldsscriptures.pro.adapter.ActivityPagerAdapter;
import com.ldsscriptures.pro.database.CommonDatabaseHelper;
import com.ldsscriptures.pro.model.ActivityData;
import com.ldsscriptures.pro.utils.Global;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


public class ActivityScreen extends BaseActivity {

    static ActivityScreen activityScreen;

    ActivityPagerAdapter mAdapter;
    @BindView(R.id.ivMenu)
    ImageView ivMenu;
    @BindView(R.id.tvTitle)
    TextView tvTitle;
    @BindView(R.id.ivadjustment)
    ImageView ivadjustment;
    @BindView(R.id.ivMore)
    ImageView ivMore;
    @BindView(R.id.layout_actitivity)
    LinearLayout layoutActitivity;
    @BindView(R.id.following_tab_layout)
    TabLayout followingTabLayout;
    @BindView(R.id.followingpager)
    ViewPager followingpager;
    @BindView(R.id.main_layout)
    RelativeLayout mainLayout;
    CommonDatabaseHelper commonDatabaseHelper;
    ArrayList<ActivityData> dayViseActivityArrayList = new ArrayList<>();
    ArrayList<ActivityData> weekViseActivityArrayList = new ArrayList<>();
    ArrayList<ActivityData> monthViseActivityArrayList = new ArrayList<>();
    ArrayList<ActivityData> yearViseActivityArrayList = new ArrayList<>();
    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_screen_pager_layout);
        ButterKnife.bind(this);
        setContents();
    }

    public void setContents() {
        activityScreen = this;
        setToolbar(toolbar);
        setVisibilityGone(ivMore);
        showText(tvTitle, getResources().getString(R.string.txt_activity));

        followingTabLayout.addTab(followingTabLayout.newTab().setText(R.string.day));
        followingTabLayout.addTab(followingTabLayout.newTab().setText(R.string.week));
        followingTabLayout.addTab(followingTabLayout.newTab().setText(R.string.month));
        followingTabLayout.addTab(followingTabLayout.newTab().setText(R.string.year));

        commonDatabaseHelper = new CommonDatabaseHelper(this);
        new GetReadingData().execute();
    }

    public class GetReadingData extends AsyncTask<String, Void, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showprogress();
        }

        @Override
        protected String doInBackground(String... params) {

            selectDayViseActivityRecord();
            selectWeekViseActivityRecord();
            selectMonthViseActivityRecord();
            selectYearViseActivityRecord();

            return "";
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            mAdapter = new ActivityPagerAdapter(getSupportFragmentManager(), followingTabLayout.getTabCount());
            followingpager.setAdapter(mAdapter);
            followingpager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(followingTabLayout));
            followingTabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
                @Override
                public void onTabSelected(TabLayout.Tab tab) {
                    followingpager.setCurrentItem(tab.getPosition());
                }

                @Override
                public void onTabUnselected(TabLayout.Tab tab) {

                }

                @Override
                public void onTabReselected(TabLayout.Tab tab) {

                }
            });
            dismissprogress();
        }
    }

    @OnClick(R.id.ivMenu)
    public void openMenu() {
        openSlideMenu();
    }

    public static ActivityScreen getInstance() {
        return activityScreen;
    }

    public void selectDayViseActivityRecord() {

        dayViseActivityArrayList.clear();
        Cursor cursor;
        cursor = commonDatabaseHelper.getDayViseActivityRecord();
        if (cursor != null && cursor.moveToFirst()) {
            while (!cursor.isAfterLast()) {
                ActivityData activityData = new ActivityData();
                activityData.setActivity_id(cursor.getInt(cursor.getColumnIndex(CommonDatabaseHelper.KEY_ACTIVITY_ID)));
                activityData.setActivity_date(cursor.getString(cursor.getColumnIndex(CommonDatabaseHelper.KEY_ACTIVITY_DATE)));
                activityData.setActivity_hour(cursor.getInt(cursor.getColumnIndex(CommonDatabaseHelper.KEY_ACTIVITY_HOUR)));
                activityData.setActivity_dayOfWeek(cursor.getInt(cursor.getColumnIndex(CommonDatabaseHelper.KEY_ACTIVITY_DAYOFWEEK)));
                activityData.setActivity_dayOfMonth(cursor.getInt(cursor.getColumnIndex(CommonDatabaseHelper.KEY_ACTIVITY_MONTH)));
                activityData.setActivity_dayOfYear(cursor.getInt(cursor.getColumnIndex(CommonDatabaseHelper.KEY_ACTIVITY_YEAR)));
                activityData.setActivity_goal_point(cursor.getInt(cursor.getColumnIndex(CommonDatabaseHelper.KEY_ACTIVITY_GOAL_POINT)));
                dayViseActivityArrayList.add(activityData);
                cursor.moveToNext();
            }
        }
    }

    public void selectWeekViseActivityRecord() {
        weekViseActivityArrayList.clear();
        Cursor cursor;
        cursor = commonDatabaseHelper.getWeekViseActivityRecord();
        if (cursor != null && cursor.moveToFirst()) {
            while (!cursor.isAfterLast()) {
                ActivityData activityData = new ActivityData();
                activityData.setActivity_id(cursor.getInt(cursor.getColumnIndex(CommonDatabaseHelper.KEY_ACTIVITY_ID)));
                activityData.setActivity_date(cursor.getString(cursor.getColumnIndex(CommonDatabaseHelper.KEY_ACTIVITY_DATE)));
                activityData.setActivity_hour(cursor.getInt(cursor.getColumnIndex(CommonDatabaseHelper.KEY_ACTIVITY_HOUR)));
                activityData.setActivity_dayOfWeek(cursor.getInt(cursor.getColumnIndex(CommonDatabaseHelper.KEY_ACTIVITY_DAYOFWEEK)));
                activityData.setActivity_dayOfMonth(cursor.getInt(cursor.getColumnIndex(CommonDatabaseHelper.KEY_ACTIVITY_MONTH)));
                activityData.setActivity_dayOfYear(cursor.getInt(cursor.getColumnIndex(CommonDatabaseHelper.KEY_ACTIVITY_YEAR)));
                activityData.setActivity_goal_point(cursor.getInt(cursor.getColumnIndex(CommonDatabaseHelper.KEY_ACTIVITY_GOAL_POINT)));
                activityData.setWeekStart(cursor.getString(cursor.getColumnIndex(CommonDatabaseHelper.KEY_ACTIVITY_WEEKSTART)));
                activityData.setWeekEnd(cursor.getString(cursor.getColumnIndex(CommonDatabaseHelper.KEY_ACTIVITY_WEEKEND)));
                weekViseActivityArrayList.add(activityData);
                cursor.moveToNext();
            }
        }
    }

    public void selectMonthViseActivityRecord() {
        monthViseActivityArrayList.clear();
        Cursor cursor;
        cursor = commonDatabaseHelper.getMonthViseActivityRecord();
        if (cursor != null && cursor.moveToFirst()) {
            while (!cursor.isAfterLast()) {

                ActivityData activityData = new ActivityData();
                activityData.setActivity_id(cursor.getInt(cursor.getColumnIndex(CommonDatabaseHelper.KEY_ACTIVITY_ID)));
                activityData.setActivity_date(cursor.getString(cursor.getColumnIndex(CommonDatabaseHelper.KEY_ACTIVITY_DATE)));
                activityData.setActivity_hour(cursor.getInt(cursor.getColumnIndex(CommonDatabaseHelper.KEY_ACTIVITY_HOUR)));
                activityData.setActivity_dayOfWeek(cursor.getInt(cursor.getColumnIndex(CommonDatabaseHelper.KEY_ACTIVITY_DAYOFWEEK)));
                activityData.setActivity_dayOfMonth(cursor.getInt(cursor.getColumnIndex(CommonDatabaseHelper.KEY_ACTIVITY_MONTH)));
                activityData.setActivity_dayOfYear(cursor.getInt(cursor.getColumnIndex(CommonDatabaseHelper.KEY_ACTIVITY_YEAR)));
                activityData.setActivity_goal_point(cursor.getInt(cursor.getColumnIndex(CommonDatabaseHelper.KEY_ACTIVITY_GOAL_POINT)));
                activityData.setWeekStart(cursor.getString(cursor.getColumnIndex(CommonDatabaseHelper.KEY_ACTIVITY_WEEKSTART)));
                activityData.setWeekEnd(cursor.getString(cursor.getColumnIndex(CommonDatabaseHelper.KEY_ACTIVITY_WEEKEND)));

                monthViseActivityArrayList.add(activityData);
                cursor.moveToNext();
            }
        }

    }

    public void selectYearViseActivityRecord() {
        yearViseActivityArrayList.clear();
        Cursor cursor;
        cursor = commonDatabaseHelper.getYearViseActivityRecord();
        if (cursor != null && cursor.moveToFirst()) {
            while (!cursor.isAfterLast()) {

                ActivityData activityData = new ActivityData();
                activityData.setActivity_id(cursor.getInt(cursor.getColumnIndex(CommonDatabaseHelper.KEY_ACTIVITY_ID)));
                activityData.setActivity_date(cursor.getString(cursor.getColumnIndex(CommonDatabaseHelper.KEY_ACTIVITY_DATE)));
                activityData.setActivity_hour(cursor.getInt(cursor.getColumnIndex(CommonDatabaseHelper.KEY_ACTIVITY_HOUR)));
                activityData.setActivity_dayOfWeek(cursor.getInt(cursor.getColumnIndex(CommonDatabaseHelper.KEY_ACTIVITY_DAYOFWEEK)));
                activityData.setActivity_dayOfMonth(cursor.getInt(cursor.getColumnIndex(CommonDatabaseHelper.KEY_ACTIVITY_MONTH)));
                activityData.setActivity_dayOfYear(cursor.getInt(cursor.getColumnIndex(CommonDatabaseHelper.KEY_ACTIVITY_YEAR)));
                activityData.setActivity_goal_point(cursor.getInt(cursor.getColumnIndex(CommonDatabaseHelper.KEY_ACTIVITY_GOAL_POINT)));

                yearViseActivityArrayList.add(activityData);
                cursor.moveToNext();
            }
        }

    }

    public ArrayList<ActivityData> getdayViseActivityArrayList() {
        return dayViseActivityArrayList;
    }

    public ArrayList<ActivityData> getWeekViseActivityArrayList() {
        return weekViseActivityArrayList;
    }

    public ArrayList<ActivityData> getmonthViseActivityArrayList() {
        return monthViseActivityArrayList;
    }

    public ArrayList<ActivityData> getyearViseActivityArrayList() {
        return yearViseActivityArrayList;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menuBack:
                if (followingpager.getCurrentItem() > 0)
                    followingpager.setCurrentItem(followingpager.getCurrentItem() - 1);
                else
                    Toast.makeText(getApplicationContext(), "No Item Found", Toast.LENGTH_LONG).show();
                finish();
                break;
            case R.id.menuNext:
                if (followingpager.getCurrentItem() != (followingTabLayout.getTabCount() - 1))
                    followingpager.setCurrentItem(followingpager.getCurrentItem() + 1);
                else
                    //finish();
                    Toast.makeText(getApplicationContext(), "No Item Found", Toast.LENGTH_LONG).show();
                break;
            case R.id.menuBookmark:
                Intent bookmarkIntent = new Intent(ActivityScreen.this, MainActivity.class);
                bookmarkIntent.putExtra(getString(R.string.menuSelection), getString(R.string.intent_bookmarks));
                bookmarkIntent.putExtra(getString(R.string.isFromReading), true);
                startActivity(bookmarkIntent);
                overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
                break;
            case R.id.menuHistory:
                Intent historyIntent = new Intent(ActivityScreen.this, MainActivity.class);
                historyIntent.putExtra(getString(R.string.menuSelection), getString(R.string.intent_history));
                historyIntent.putExtra(getString(R.string.isFromReading), true);
                startActivity(historyIntent);
                overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}
