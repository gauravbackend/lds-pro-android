package com.ldsscriptures.pro.activity.settingmenu;

import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;

import com.ldsscriptures.pro.R;
import com.ldsscriptures.pro.activity.BaseActivity;
import com.ldsscriptures.pro.model.UserDetail;
import com.ldsscriptures.pro.retroutils.APIClient;
import com.ldsscriptures.pro.retroutils.APIInterface;
import com.ldsscriptures.pro.utils.Global;
import com.ldsscriptures.pro.utils.SessionManager;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class SettingsGoalActivity extends BaseActivity {
    @BindView(R.id.ivBack)
    ImageView ivBack;
    @BindView(R.id.ivMenu)
    ImageView ivMenu;
    @BindView(R.id.tvTitle)
    TextView tvTitle;
    @BindView(R.id.ivDropDownMenu)
    ImageView ivDropDownMenu;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.seekBar)
    SeekBar seekBar;
    @BindView(R.id.ivMore)
    ImageView ivMore;
    @BindView(R.id.txtSetGoal)
    TextView txtSetGoal;
    @BindView(R.id.txtAboutDaysCount)
    TextView txtAboutDaysCount;

    int minValue = 50;
    int setseekgoal;
    double progressValue;
    double divistionValue = 60;
    SessionManager sessionManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings_goal);
        ButterKnife.bind(this);
        setContent();
    }

    private void setContent() {
        ButterKnife.bind(this);
        setToolbar(toolbar);

        showText(tvTitle, getResources().getString(R.string.set_daily_goal));
        setVisibilityVisible(ivBack);
        setVisibilityGone(ivMenu);
        sessionManager = new SessionManager(this);

        if (sessionManager.getUserGoal() != null && sessionManager.getUserGoal().length() != 0) {
            int getGoal = Integer.parseInt(sessionManager.getUserGoal());
            if (getGoal != 0) {
                seekBar.setProgress(getGoal);
                txtSetGoal.setText(String.valueOf(getGoal));
                txtAboutDaysCount.setText("About" + " " + Math.round(Math.ceil(getGoal / divistionValue)) + " " + "min per Day");
            }
        }

        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {

            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                txtSetGoal.setText(Integer.toString(progress + minValue));
            }

            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            public void onStopTrackingTouch(SeekBar seekBar) {
                setseekgoal = seekBar.getProgress() + minValue;
                sessionManager.setUserGoal(String.valueOf(setseekgoal));
                progressValue = setseekgoal;
                txtAboutDaysCount.setText("About" + " " + Math.round(Math.ceil(progressValue / divistionValue)) + " " + "min per Day");
                updateUSer();
            }
        });
    }

    @OnClick(R.id.ivBack)
    public void onViewClicked() {
        finish();
    }

    private void updateUSer() {

        APIInterface apiService = APIClient.getClient().create(APIInterface.class);
        String url = APIClient.BASE_URL;

        url += "updateUser.pl?" +
                "user_id=" + getUserInfo(SessionManager.KEY_USER_ID) +
                "&session_id=" + getUserInfo(SessionManager.KEY_USER_SESSION_ID) +
                "&apiauth=" + APIClient.AUTH_KEY +
                "&goal_points=" + sessionManager.getUserGoal();

        Call<UserDetail> callObject = apiService.updateUserInfo(url.replaceAll(" ", "%20"));

        callObject.enqueue(new Callback<UserDetail>() {
            @Override
            public void onResponse(Call<UserDetail> call, Response<UserDetail> response) {

                if (response.body() != null && !response.body().toString().trim().isEmpty()) {

                }
            }

            @Override
            public void onFailure(Call<UserDetail> call, Throwable t) {
                call.cancel();
                Global.dismissProgressDialog();
            }
        });
    }

}
