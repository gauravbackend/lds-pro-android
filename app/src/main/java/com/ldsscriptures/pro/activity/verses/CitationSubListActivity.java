package com.ldsscriptures.pro.activity.verses;

import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.widget.ImageView;
import android.widget.TextView;

import com.ldsscriptures.pro.R;
import com.ldsscriptures.pro.activity.BaseActivity;
import com.ldsscriptures.pro.adapter.CitationSubListAdapter;
import com.ldsscriptures.pro.database.CitationsDatabaseHelper;
import com.ldsscriptures.pro.model.CitationSubList;
import com.ldsscriptures.pro.model.TimeLine.VerseDataTimeLineModel;
import com.ldsscriptures.pro.utils.Global;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class CitationSubListActivity extends BaseActivity {
    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.tvTitle)
    TextView tvTitle;

    @BindView(R.id.ivBack)
    ImageView ivBack;

    @BindView(R.id.ivMore)
    ImageView ivMore;

    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;

    @BindView(R.id.tvNoData)
    TextView tvNoData;

    @BindView(R.id.fab)
    FloatingActionButton fab;

    private CitationsDatabaseHelper citationsDatabaseHelper;
    private ArrayList<CitationSubList> citationLists = new ArrayList<>();

    public String citation_itemUri, source_group, verseId;
    static CitationSubListActivity citationSubListActivity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_citation);
        ButterKnife.bind(this);
        setContents();
    }

    private void setContents() {
        ButterKnife.bind(this);

        // set the dropdown option into toolbar
        citation_itemUri = getIntent().getStringExtra(getString(R.string.citation_uri));
        source_group = getIntent().getStringExtra(getString(R.string.source_group));
        verseId = getIntent().getStringExtra(getString(R.string.verseId));

        citationSubListActivity = this;

        citationsDatabaseHelper = new CitationsDatabaseHelper(CitationSubListActivity.this);

        setToolbar(toolbar);

        showText(tvTitle, getString(R.string.citations));
        setVisibilityVisible(ivBack);

        setRecyclerView();
    }

    @OnClick(R.id.fab)
    public void manageFloatingButton() {
        manageFloatingBtn();
    }

    @OnClick(R.id.ivMore)
    public void showMore() {

    }

    @OnClick(R.id.ivBack)
    public void finishAct() {
        onBackPressed();
    }

    private void setRecyclerView() {
        new GetCitationSubList().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, "");
    }

    public void finishActivity() {
        finish();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    private class GetCitationSubList extends AsyncTask<String, Void, String> {
        @Override
        protected void onPreExecute() {
            citationLists.clear();
            Global.showProgressDialog(CitationSubListActivity.this);
        }

        @Override
        protected String doInBackground(String... params) {
            Cursor cursor;
            cursor = citationsDatabaseHelper.getCitationSubList(citation_itemUri, source_group, verseId);

            citationLists = new ArrayList<>();
            if (cursor != null && cursor.moveToFirst()) {
                while (!cursor.isAfterLast()) {

                    CitationSubList citationList = new CitationSubList();
                    citationList.setId(cursor.getString(cursor.getColumnIndex("id")));
                    citationList.setAid(cursor.getString(cursor.getColumnIndex("source_aid")));
                    citationList.setSource_title(cursor.getString(cursor.getColumnIndex("source_title")));
                    citationList.setSource_subtitle(cursor.getString(cursor.getColumnIndex(CitationsDatabaseHelper.KEY_SOURCE_SUBTITLE)));
                    citationList.setSource_group(cursor.getString(cursor.getColumnIndex("source_group")));
                    citationList.setSource_uri(cursor.getString(cursor.getColumnIndex("source_uri")));
                    citationList.setRef_uri(cursor.getString(cursor.getColumnIndex("ref_uri")));

                    String verseURI = cursor.getString(cursor.getColumnIndex("ref_uri")) + "." + cursor.getString(cursor.getColumnIndex("ref_verse"));
                    VerseDataTimeLineModel verseDataTimeLineModel;
                    verseDataTimeLineModel = new VerseDataTimeLineModel();
                    verseDataTimeLineModel = getVerseText(verseURI, verseId);
                    String verseText = verseDataTimeLineModel.getVereseText();
                    citationList.setVerseText(verseText);

                    citationLists.add(citationList);
                    cursor.moveToNext();
                }
            }
            if (cursor != null) {
                cursor.close();
            }
            citationsDatabaseHelper.close();

            return "";
        }

        @Override
        protected void onPostExecute(String result) {
            Global.dismissProgressDialog();
            if (citationLists.size() != 0) {

                setVisibilityGone(tvNoData);
                setVisibilityVisible(recyclerView);
                RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(CitationSubListActivity.this);
                recyclerView.setLayoutManager(mLayoutManager);

                CitationSubListAdapter subListAdapter = new CitationSubListAdapter(CitationSubListActivity.this, citationLists);
                recyclerView.setAdapter(subListAdapter);
            } else {
                setVisibilityGone(recyclerView);
                setVisibilityVisible(tvNoData);
            }
        }
    }

    public static CitationSubListActivity getInstance() {
        return citationSubListActivity;
    }

}
