package com.ldsscriptures.pro.activity.verses;

import android.content.ContentValues;
import android.os.Bundle;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.ldsscriptures.pro.R;
import com.ldsscriptures.pro.activity.BaseActivity;
import com.ldsscriptures.pro.database.CommonDatabaseHelper;
import com.ldsscriptures.pro.utils.Global;
import com.ldsscriptures.pro.utils.PushToServerData;
import com.ldsscriptures.pro.utils.SessionManager;
import com.ldsscriptures.pro.utils.Validator;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class VerseEditBookmarkActivity extends BaseActivity {

    @BindView(R.id.tvVerseId)
    TextView tvVerseId;

    @BindView(R.id.tvVerseName)
    TextView tvVerseName;

    @BindView(R.id.tvTitle)
    TextView tvTitle;

    @BindView(R.id.ivBack)
    ImageView ivBack;

    @BindView(R.id.edtEnterBookTitle)
    EditText edtEnterBookTitle;

    private String bookmarkTitle;
    private SessionManager sessionManager;
    private String verseText;
    private String verseId;
    private String verseAid;
    private CommonDatabaseHelper commonDatabaseHelper;
    private String serverId;
    private String itemUri;
    private int position;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_verse_new_bookmark);
        setContent();
    }

    private void setContent() {
        ButterKnife.bind(this);

        sessionManager = new SessionManager(VerseEditBookmarkActivity.this);
        commonDatabaseHelper = new CommonDatabaseHelper(VerseEditBookmarkActivity.this);

        verseId = getIntent().getStringExtra(getString(R.string.verseId));
        verseText = getIntent().getStringExtra(getString(R.string.verseText));
        verseAid = getIntent().getStringExtra(getString(R.string.verseAId));
        serverId = getIntent().getStringExtra(getString(R.string.serverId));
        bookmarkTitle = getIntent().getStringExtra(getString(R.string.bookmarkTitle));
        itemUri = getIntent().getStringExtra(getString(R.string.itemUri));
        position = getIntent().getIntExtra(getString(R.string.position), 0);
        showText(tvTitle, getString(R.string.update_bookmark));

        edtEnterBookTitle.setText(bookmarkTitle);
        edtEnterBookTitle.setSelection(edtEnterBookTitle.getText().length());
        tvVerseId.setText(verseId);
        tvVerseName.setText(verseText);
    }

    @OnClick(R.id.ivBack)
    public void finishActivity() {
        if (VerseHighlightActivity.getInstance() != null) {
            VerseHighlightActivity.getInstance().goToReadingView();
        }
        finish();
    }

    @OnClick(R.id.tvDone)
    public void addNewBookmark() {
        if (validateFields()) {

            ContentValues contentValues = new ContentValues();
            contentValues.put("versetitle", edtEnterBookTitle.getText().toString());
            contentValues.put("modified", "1");

            commonDatabaseHelper.updateRowData(CommonDatabaseHelper.TABLE_BOOKMARK_MARSTER, contentValues, CommonDatabaseHelper.KEY_VERSEAID + " = ? AND " + CommonDatabaseHelper.KEY_ITEMURI + " = ?",
                    new String[]{String.valueOf(verseAid), String.valueOf(Global.bookmarkData.getItemuri())});

            new PushToServerData(this);

            finish();
        }
    }


    private boolean validateFields() {
        if (!Validator.checkEmpty(edtEnterBookTitle)) {
            Toast.makeText(VerseEditBookmarkActivity.this, getString(R.string.error_bookmark), Toast.LENGTH_LONG).show();
            return false;
        }
        return true;
    }
}
