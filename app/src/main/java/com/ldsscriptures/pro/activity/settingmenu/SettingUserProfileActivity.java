package com.ldsscriptures.pro.activity.settingmenu;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.ldsscriptures.pro.R;
import com.ldsscriptures.pro.activity.BaseActivity;
import com.ldsscriptures.pro.adapter.ProfileFollowingListAdapter;
import com.ldsscriptures.pro.model.FollowersMainData;
import com.ldsscriptures.pro.model.FollowingMainModel;
import com.ldsscriptures.pro.retroutils.APIClient;
import com.ldsscriptures.pro.retroutils.APIInterface;
import com.ldsscriptures.pro.utils.SessionManager;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by IBL InfoTech on 8/28/2017.
 */

public class SettingUserProfileActivity extends BaseActivity {

    @BindView(R.id.ivmenu_Profle)
    ImageView ivmenuProfle;
    @BindView(R.id.setting_profile_image)
    CircleImageView settingProfileImage;
    @BindView(R.id.ivSetting_Profile)
    ImageView ivSettingProfile;
    @BindView(R.id.profile_total_PTS)
    TextView profileTotalPTS;
    @BindView(R.id.profile_level_up)
    TextView profileLevelUp;
    @BindView(R.id.profile_reading_time)
    TextView profileReadingTime;
    @BindView(R.id.profile_achivement)
    ImageView profileAchivement;
    @BindView(R.id.profile_Artifacts)
    ImageView profileArtifacts;
    @BindView(R.id.setting_profile_username)
    TextView settingProfileUsername;
    @BindView(R.id.progress_profile_image)
    ProgressBar progressProfileImage;
    @BindView(R.id.achivement_count)
    TextView achivementCount;
    @BindView(R.id.artifacts_count)
    TextView artifactsCount;
    @BindView(R.id.imageView4)
    ImageView imageView4;
    @BindView(R.id.rvParticipateUser)
    RecyclerView rvParticipateUser;
    @BindView(R.id.remainCount)
    TextView remainCount;
    @BindView(R.id.linRemainCount)
    LinearLayout linRemainCount;
    @BindView(R.id.no_following_found)
    TextView noFollowingFound;
    @BindView(R.id.txt_level_up_count)
    TextView txtLevelUpCount;

    int deviceWidth;

    ArrayList<FollowersMainData> followingarraylist = new ArrayList<>();
    SessionManager sessionManager;

    String paraUserId, paraSessionId;
    int rv_width = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting_user_profile);
        ButterKnife.bind(this);
        setContents();
    }

    private void setContents() {
        ButterKnife.bind(this);
        sessionManager = new SessionManager(SettingUserProfileActivity.this);


        showText(settingProfileUsername, getUserInfo(SessionManager.KEY_USER_USERNAME));

        String imageUrl = getUserInfo(SessionManager.KEY_USR_IMG_URL);
        if (imageUrl != null) {
            Glide.with(getBaseContext())
                    .load(getUserInfo(SessionManager.KEY_USR_IMG_URL))
                    .listener(new RequestListener<String, GlideDrawable>() {
                        @Override
                        public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                            progressProfileImage.setVisibility(View.GONE);
                            return false;
                        }

                        @Override
                        public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                            progressProfileImage.setVisibility(View.GONE);
                            return false;
                        }
                    })
                    .centerCrop()
                    .fitCenter()
                    .into(settingProfileImage);
        } else {
            progressProfileImage.setVisibility(View.GONE);
            settingProfileImage.setImageResource(R.drawable.profile_lg);
        }

        // set reading minute
        if (sessionManager.getUserMinReading() != null)
            profileReadingTime.setText(sessionManager.getUserMinReading());

        // set the daily point
        if (sessionManager.getUserDailyPoints() != 0) {
            profileTotalPTS.setText(String.valueOf(sessionManager.getUserDailyPoints()));
        }
        if (getUserInfo(SessionManager.KEY_USER_ID) != null) {
            paraUserId = getUserInfo(SessionManager.KEY_USER_ID);
        }
        if (getUserInfo(SessionManager.KEY_USER_SESSION_ID) != null) {
            paraSessionId = getUserInfo(SessionManager.KEY_USER_SESSION_ID);
        }
        if (sessionManager.getAchivementCount() != null) {
            achivementCount.setText(sessionManager.getAchivementCount());
        }

        if (sessionManager.getLevelCount() != null) {
            txtLevelUpCount.setText(sessionManager.getLevelCount());
        }

        getFollowingFriendAPI(paraUserId, paraSessionId);
    }

    private void getFollowingFriendAPI(String paraUserId, String paraSessionId) {
        followingarraylist.clear();
        showprogress();

        APIInterface apiService = APIClient.getClient().create(APIInterface.class);
        String url = APIClient.BASE_URL;

        url += "listUsers.pl?user_id=" + paraUserId +
                "&session_id=" + paraSessionId +
                "&apiauth=" + APIClient.AUTH_KEY +
                "&followed_by=" + "1";

        Call<FollowingMainModel<FollowersMainData>> callObject = apiService.getFollowingData(url.replaceAll(" ", "%20"));
        callObject.enqueue(new Callback<FollowingMainModel<FollowersMainData>>() {

            @Override
            public void onResponse(Call<FollowingMainModel<FollowersMainData>> call, Response<FollowingMainModel<FollowersMainData>> response) {

                if (response.body() != null && !response.body().toString().trim().isEmpty()) {

                    if (response.body().getSuccess().equals("true")) {
                        followingarraylist = response.body().getFollowers();

                        if (followingarraylist.size() != 0) {

                            int item = 4;
                            float itemWidth = (deviceWidth) / item;
                            DisplayMetrics dm = getResources().getDisplayMetrics();
                            int densityDpi = dm.densityDpi;
                            DisplayMetrics displayMetrics1 = getResources().getDisplayMetrics();
                            float density = getResources().getDisplayMetrics().density;
                            float itemWidthDp;
                            if (density >= 4.0) {
                                itemWidthDp = itemWidth / (displayMetrics1.ydpi / DisplayMetrics.DENSITY_XXXHIGH);
                            }
                            if (density >= 3.0) {
                                itemWidthDp = itemWidth / (displayMetrics1.ydpi / DisplayMetrics.DENSITY_XXHIGH);
                            }
                            if (density >= 2.0) {
                                itemWidthDp = itemWidth / (displayMetrics1.ydpi / DisplayMetrics.DENSITY_XHIGH);
                            }
                            if (density >= 1.5) {
                                itemWidthDp = itemWidth / (displayMetrics1.ydpi / DisplayMetrics.DENSITY_HIGH);
                            }
                            if (density >= 1.0) {
                                itemWidthDp = itemWidth / (displayMetrics1.ydpi / DisplayMetrics.DENSITY_MEDIUM);
                            } else {
                                itemWidthDp = itemWidth / (displayMetrics1.ydpi / DisplayMetrics.DENSITY_DEFAULT);
                            }
                            int dispCount = followingarraylist.size() - item;
                            int width = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, itemWidthDp, getResources().getDisplayMetrics());

                            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(width, width);
                            linRemainCount.setLayoutParams(layoutParams);
                            remainCount.setText(dispCount + "+");

                            rvParticipateUser.setLayoutManager(new LinearLayoutManager(SettingUserProfileActivity.this, LinearLayoutManager.HORIZONTAL, false));

                            if (followingarraylist.size() <= 7) {

                                linRemainCount.setVisibility(View.GONE);

                                ViewTreeObserver vto = rvParticipateUser.getViewTreeObserver();
                                vto.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                                    @Override
                                    public void onGlobalLayout() {
                                        rvParticipateUser.getViewTreeObserver().removeGlobalOnLayoutListener(this);
                                        rv_width = (int) (rvParticipateUser.getMeasuredWidth() / 7);

                                        ProfileFollowingListAdapter mAdapter = new ProfileFollowingListAdapter(SettingUserProfileActivity.this, followingarraylist, rv_width);
                                        rvParticipateUser.setAdapter(mAdapter);

                                    }
                                });

                            } else {

                                linRemainCount.setVisibility(View.VISIBLE);

                                ViewTreeObserver vto = rvParticipateUser.getViewTreeObserver();
                                vto.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                                    @Override
                                    public void onGlobalLayout() {
                                        rvParticipateUser.getViewTreeObserver().removeGlobalOnLayoutListener(this);
                                        rv_width = (int) (rvParticipateUser.getMeasuredWidth() / 7);
                                        linRemainCount.getLayoutParams().height = rv_width;
                                        linRemainCount.getLayoutParams().width = rv_width;
                                        remainCount.setText("+\n" + (followingarraylist.size() - 6));

                                        ArrayList<FollowersMainData> tempFollowingarraylist = new ArrayList<>();

                                        for (int i = 0; i < 6; i++) {
                                            tempFollowingarraylist.add(followingarraylist.get(i));
                                        }

                                        ProfileFollowingListAdapter mAdapter = new ProfileFollowingListAdapter(SettingUserProfileActivity.this, tempFollowingarraylist, rv_width);
                                        rvParticipateUser.setAdapter(mAdapter);
                                    }
                                });


                            }
                        } else {
                            linRemainCount.setVisibility(View.GONE);
                            noFollowingFound.setVisibility(View.VISIBLE);
                            rvParticipateUser.setVisibility(View.GONE);
                        }
                    }
                    dismissprogress();
                }
            }

            @Override
            public void onFailure(Call<FollowingMainModel<FollowersMainData>> call, Throwable t) {
                dismissprogress();
            }
        });
    }

    @OnClick({R.id.ivmenu_Profle, R.id.ivSetting_Profile, R.id.linRemainCount})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.ivmenu_Profle:
                openSlideMenu();
                break;
            case R.id.ivSetting_Profile:
                startActivity(new Intent(getApplicationContext(), SettingsActivity.class));
                overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
                break;
            case R.id.linRemainCount:
                linRemainCount.setVisibility(View.GONE);
                ViewTreeObserver vto = rvParticipateUser.getViewTreeObserver();
                vto.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                    @Override
                    public void onGlobalLayout() {
                        rvParticipateUser.getViewTreeObserver().removeGlobalOnLayoutListener(this);
                        rv_width = (int) (rvParticipateUser.getMeasuredWidth() / 7);
                        linRemainCount.getLayoutParams().height = rv_width;
                        linRemainCount.getLayoutParams().width = rv_width;
                        remainCount.setText((followingarraylist.size() - 7) + "+");

                        ProfileFollowingListAdapter addParticipateUserImageAdapter = new ProfileFollowingListAdapter(SettingUserProfileActivity.this, followingarraylist, rv_width);
                        rvParticipateUser.setAdapter(addParticipateUserImageAdapter);
                    }
                });
                break;

        }
    }
}
