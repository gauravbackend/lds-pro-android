package com.ldsscriptures.pro.activity.studytools;

import android.content.ContentValues;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.ldsscriptures.pro.R;
import com.ldsscriptures.pro.activity.BaseActivity;
import com.ldsscriptures.pro.adapter.CrossReferenceLcListAdapter;
import com.ldsscriptures.pro.adapter.CrossReferenceLibIndexAdapter;
import com.ldsscriptures.pro.adapter.CrossReferenceLibSubIndexAdapter;
import com.ldsscriptures.pro.adapter.CrossReferenceLsListAdapter;
import com.ldsscriptures.pro.adapter.CrossReferenceReadingHtmlDataAdapter;
import com.ldsscriptures.pro.database.CommonDatabaseHelper;
import com.ldsscriptures.pro.dbClass.GetDataFromDatabase;
import com.ldsscriptures.pro.dbClass.GetLibCollectionFromDB;
import com.ldsscriptures.pro.model.Item;
import com.ldsscriptures.pro.model.LcData;
import com.ldsscriptures.pro.model.LsData;
import com.ldsscriptures.pro.model.LsFullIndexData;
import com.ldsscriptures.pro.model.LsIndexPreviewData;
import com.ldsscriptures.pro.model.LsMainIndexData;
import com.ldsscriptures.pro.model.VerseData;
import com.ldsscriptures.pro.utils.DownloadSubDBZipFile;
import com.ldsscriptures.pro.utils.Global;
import com.ldsscriptures.pro.utils.PushToServerData;
import com.ldsscriptures.pro.utils.SessionManager;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


public class AddLessonScripture extends BaseActivity {

    @BindView(R.id.ivBack)
    ImageView ivBack;
    @BindView(R.id.ivMenu)
    ImageView ivMenu;
    @BindView(R.id.tvTitle)
    TextView tvTitle;
    @BindView(R.id.ivCheck)
    ImageView ivCheck;
    @BindView(R.id.ivEdit)
    ImageView ivEdit;
    @BindView(R.id.ivMore)
    ImageView ivMore;
    @BindView(R.id.rvLc)
    RecyclerView rvLc;
    @BindView(R.id.linLc)
    LinearLayout linLc;
    @BindView(R.id.txtLSBack)
    TextView txtLSBack;
    @BindView(R.id.rvLS)
    RecyclerView rvLS;
    @BindView(R.id.linLS)
    LinearLayout linLS;
    @BindView(R.id.txtLibIndexBack)
    TextView txtLibIndexBack;
    @BindView(R.id.txtNavPathLibIndex)
    TextView txtNavPathLibIndex;
    @BindView(R.id.rvLibIndex)
    RecyclerView rvLibIndex;
    @BindView(R.id.linLibIndex)
    LinearLayout linLibIndex;
    @BindView(R.id.txtSubIndexBack)
    TextView txtSubIndexBack;
    @BindView(R.id.txtNavPathSubIndex)
    TextView txtNavPathSubIndex;
    @BindView(R.id.rvLibSubIndex)
    RecyclerView rvLibSubIndex;
    @BindView(R.id.linLibSubIndex)
    LinearLayout linLibSubIndex;
    @BindView(R.id.txtReadingHtmlBack)
    TextView txtReadingHtmlBack;
    @BindView(R.id.txtReadingHtmlDone)
    TextView txtReadingHtmlDone;
    @BindView(R.id.txtNavPathReadingHtml)
    TextView txtNavPathReadingHtml;
    @BindView(R.id.rvReadingHtml)
    RecyclerView rvReadingHtml;
    @BindView(R.id.linReadingHtml)
    LinearLayout linReadingHtml;
    @BindView(R.id.llFootNotes)
    RelativeLayout llFootNotes;
    private LinearLayoutManager mLayoutManager;

    private ArrayList<LsData> lsDataArrayList = new ArrayList<>();
    private ArrayList<LsData> lsDataArrayList_new = new ArrayList<>();
    private ArrayList<LsMainIndexData> combineIndexDataArray = new ArrayList<>();
    private ArrayList<LsIndexPreviewData> newIndexArray = new ArrayList<>();

    SessionManager sessionManager;

    static AddLessonScripture addLessonScripture;
    private CommonDatabaseHelper commonDatabaseHelper;
    private int lessonId;
    int lessonType = 2;
    private ArrayList<Item> dbCatelogArraylist = new ArrayList<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lesson_scripture);
        setContents();
    }

    private void setContents() {
        ButterKnife.bind(this);
        addLessonScripture = this;
        mLayoutManager = new LinearLayoutManager(AddLessonScripture.this);
        sessionManager = new SessionManager(this);
        commonDatabaseHelper = new CommonDatabaseHelper(AddLessonScripture.this);
        lessonId = getIntent().getIntExtra(getString(R.string.lesson_id), 0);
        dbCatelogArraylist = Global.getListSharedPreferneces(this);
        setLcList();
    }

    private void setLcList() {
        linLc.setVisibility(View.VISIBLE);
        ArrayList<LcData> lcDataArrayList = new GetLibCollectionFromDB(AddLessonScripture.this).getLcArrayListFromDB();
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(AddLessonScripture.this);
        rvLc.setLayoutManager(mLayoutManager);
        if (lcDataArrayList != null)
            rvLc.setAdapter(new CrossReferenceLcListAdapter(AddLessonScripture.this, lcDataArrayList, "1"));
    }

    public void setLsData(String LcID) {
        linLS.setVisibility(View.VISIBLE);


        lsDataArrayList = new GetLibCollectionFromDB(AddLessonScripture.this).getLsDataFromDb(LcID);
        lsDataArrayList_new = new GetLibCollectionFromDB(AddLessonScripture.this).getLsDataFromDbFirst(LcID);
        lsDataArrayList.addAll(lsDataArrayList_new);

        rvLS.setLayoutManager(mLayoutManager);
        if (lsDataArrayList != null)
            rvLS.setAdapter(new CrossReferenceLsListAdapter(AddLessonScripture.this, lsDataArrayList, "1"));
    }

    public void setLibraryIndex(String LsId, String navPath) {
        if (dbCatelogArraylist != null) {
            for (int i = 0; i < dbCatelogArraylist.size(); i++) {
                if (dbCatelogArraylist.get(i).getId().equals(LsId)) {
                    if (!isFolderExists(dbCatelogArraylist.get(i).getId())) {
                        new DownloadFileFromURL().execute(dbCatelogArraylist.get(i).getUrl(), dbCatelogArraylist.get(i).getId(), navPath);
                    } else {
                        linLibIndex.setVisibility(View.VISIBLE);
                        txtNavPathLibIndex.setText(navPath);

                        ArrayList<LsFullIndexData> lsFullIndexDataArray = new GetDataFromDatabase(AddLessonScripture.this, LsId).GetMainDataFromDB();

                        combineIndexDataArray = new ArrayList<>();
                        if (lsFullIndexDataArray != null)
                            for (int i1 = 0; i1 < lsFullIndexDataArray.size(); i1++) {
                                combineIndexDataArray.addAll(lsFullIndexDataArray.get(i1).getItemMainIndexArray());
                            }

                        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(AddLessonScripture.this);
                        rvLibIndex.setLayoutManager(mLayoutManager);
                        if (combineIndexDataArray != null)
                            rvLibIndex.setAdapter(new CrossReferenceLibIndexAdapter(AddLessonScripture.this, combineIndexDataArray, LsId, "1"));
                    }
                }
            }
        }

    }

    public void setLibSubIndex(int position, int clickedId, String navPath, String lsId) {
        linLibSubIndex.setVisibility(View.VISIBLE);

        txtNavPathSubIndex.setText(txtNavPathLibIndex.getText().toString() + " / " + navPath);

        ArrayList<LsIndexPreviewData> indexArray = new ArrayList<>();

        for (int i = 0; i < combineIndexDataArray.get(position).getLsSubIndexDatas().size(); i++) {
            if (clickedId == combineIndexDataArray.get(position).getLsSubIndexDatas().get(i).getId()) {
                indexArray = combineIndexDataArray.get(position).getLsSubIndexDatas().get(i).getLsIndexPreviewDatas();
            }
        }

        newIndexArray = new ArrayList<>();
        for (int i = 0; i < indexArray.size(); i++) {
            if (indexArray.get(i).getNav_section_id().equals(String.valueOf(clickedId))) {
                newIndexArray.add(indexArray.get(i));
            }
        }

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(AddLessonScripture.this);
        rvLibSubIndex.setLayoutManager(mLayoutManager);
        if (newIndexArray != null)
            rvLibSubIndex.setAdapter(new CrossReferenceLibSubIndexAdapter(AddLessonScripture.this, newIndexArray, lsId, "1"));
    }

    public void setLibraryReadingHtml(int position, String html_title, String lsId) {

        linReadingHtml.setVisibility(View.VISIBLE);
        txtNavPathReadingHtml.setText(txtNavPathSubIndex.getText().toString() + " / " + html_title);
        String uri = newIndexArray.get(position).getUri();

        int indexID = newIndexArray.get(position).getId();
        ArrayList<VerseData> stringArrayList = new GetDataFromDatabase(AddLessonScripture.this, lsId).getCrossRefSubItemHtmlData(indexID, uri);

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(AddLessonScripture.this);
        rvReadingHtml.setLayoutManager(mLayoutManager);
        if (stringArrayList != null)
            rvReadingHtml.setAdapter(new CrossReferenceReadingHtmlDataAdapter(AddLessonScripture.this, stringArrayList, html_title, "0", uri, "1"));
    }

    public void visibleDone(boolean isVisible) {
        if (isVisible) {
            txtReadingHtmlDone.setVisibility(View.VISIBLE);
        } else {
            txtReadingHtmlDone.setVisibility(View.GONE);
        }
    }

    @OnClick(R.id.txtReadingHtmlDone)
    public void storeCrossRefDone() {

        if (CrossReferenceReadingHtmlDataAdapter.getInstance() != null) {
            ArrayList<VerseData> verseDataArrayList = new ArrayList<>();
            verseDataArrayList = CrossReferenceReadingHtmlDataAdapter.getInstance().selectedScriptureArray();
            String verseNumber = "";

            for (int i = 0; i < verseDataArrayList.size(); i++) {
                verseNumber = verseNumber + verseDataArrayList.get(i).getId() + ", ";
            }

            String[] separated = verseNumber.split(",");
            ArrayList<Integer> list = convert(separated);

            int start = 0;
            List<String> ranges = new ArrayList<>();
            for (int i = 1; i < list.size(); i++) {

                if (list.get(i - 1) + 1 != list.get(i)) {
                    if (list.get(start) == list.get(i - 1)) {
                        ranges.add("" + list.get(start));
                    } else {
                        ranges.add(list.get(start) + "-" + list.get(i - 1));
                    }
                    start = i;
                }
            }

            if (list.get(start) == list.get(list.size() - 1)) {
                ranges.add("" + list.get(start));
            } else {
                ranges.add(list.get(start) + "-" + list.get(list.size() - 1));
            }

            if (!verseNumber.equals("")) {

                for (int i = 0; i < verseDataArrayList.size(); i++) {

                    ContentValues contentValues = new ContentValues();

                    contentValues.put(CommonDatabaseHelper.KEY_LESSONID, lessonId);
                    contentValues.put(CommonDatabaseHelper.KEY_LESSONTYPE, 2);
                    contentValues.put(CommonDatabaseHelper.KEY_VERSEID, ranges.toString().trim().replace("[", "").replace("]", ""));
                    contentValues.put(CommonDatabaseHelper.KEY_VERSETEXT1, verseDataArrayList.get(i).getStringSentense());
                    contentValues.put(CommonDatabaseHelper.KEY_VERSEAID1, verseDataArrayList.get(i).getData_aid());
                    contentValues.put(CommonDatabaseHelper.KEY_URI, verseDataArrayList.get(i).getUri());
                    contentValues.put(CommonDatabaseHelper.KEY_VERSETITLE1, verseDataArrayList.get(i).getVersetitle());
                    contentValues.put(CommonDatabaseHelper.KEY_SERVERID, "0");
                    contentValues.put(CommonDatabaseHelper.KEY_SORT_ORDER, "1");
                    contentValues.put("deleted", "");
                    contentValues.put("modified", "");

                    commonDatabaseHelper.insertData(CommonDatabaseHelper.TABLE_SUB_ITEM_LESSONS, contentValues);

                    break;
                }

                new PushToServerData(this);

                if (LessonSubItemActivity.getInstance() != null) {
                    LessonSubItemActivity.getInstance().setData();
                }

                finish();
            }

        }
    }

    private ArrayList<Integer> convert(String[] string) {
        ArrayList<Integer> number = new ArrayList<Integer>();
        for (int i = 0; i < string.length; i++) {
            if (string[i].trim().length() != 0) {
                number.add(Integer.parseInt(string[i].trim()));
            }
        }
        return number;
    }

    @OnClick(R.id.txtLSBack)
    public void mangeLSBack() {
        linLS.setVisibility(View.GONE);
    }

    @OnClick(R.id.txtLibIndexBack)
    public void mangeLibIndexBack() {
        linLibIndex.setVisibility(View.GONE);
    }

    @OnClick(R.id.txtSubIndexBack)
    public void mangeSubIndexBack() {
        linLibSubIndex.setVisibility(View.GONE);
    }

    @OnClick(R.id.txtReadingHtmlBack)
    public void mangeReadingHtmlBack() {
        linReadingHtml.setVisibility(View.GONE);
    }

    @OnClick(R.id.ivBack)
    public void finishActivity() {
        finish();
    }

    public static AddLessonScripture getInstance() {
        return addLessonScripture;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if (LessonSubItemActivity.getInstance() != null) {
            LessonSubItemActivity.getInstance().setData();
        }
    }

    private boolean isFolderExists(String id) {
        File destDir = new File(Global.AppFolderPath + id);

        if (!destDir.exists()) {
            return false;
        } else {
            if (destDir.isDirectory()) {
                String[] files = destDir.list();
                if (files == null)
                    return false;

                if (files.length == 0) {
                    //directory is empty
                    return false;
                } else
                    return true;
            }
        }
        return false;
    }

    class DownloadFileFromURL extends AsyncTask<String, String, String> {

        String ItemID = "";
        String navPath = "";

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showprogress();
        }

        @Override
        protected String doInBackground(String... f_url) {
            int count;
            try {

                ItemID = f_url[1];
                navPath = f_url[2];

                new DownloadSubDBZipFile(AddLessonScripture.this, f_url[0], ItemID);

                URL url = new URL(f_url[0]);
                URLConnection conection = url.openConnection();
                conection.connect();
                // getting file length
                int lenghtOfFile = conection.getContentLength();

                // input stream to read file - with 8k buffer
                InputStream input = new BufferedInputStream(url.openStream(), 8192);

                // Output stream to write file
                OutputStream output = new FileOutputStream("/sdcard/downloadedfile.jpg");

                byte data[] = new byte[1024];

                long total = 0;

                while ((count = input.read(data)) != -1) {
                    total += count;
                    // publishing the progress....
                    // After this onProgressUpdate will be called
                    publishProgress("" + (int) ((total * 100) / lenghtOfFile));

                    // writing data to file
                    output.write(data, 0, count);
                }

                // flushing output
                output.flush();

                // closing streams
                output.close();
                input.close();

            } catch (Exception e) {
                Log.e("Error: ", e.getMessage());
            }

            return null;
        }

        /**
         * Updating progress bar
         */
        protected void onProgressUpdate(String... progress) {
            // setting progress percentage
            //pDialog.setProgress(Integer.parseInt(progress[0]));
        }

        /**
         * After completing background task
         * Dismiss the progress dialog
         **/
        @Override
        protected void onPostExecute(String file_url) {
            // dismiss the dialog after the file was downloaded
            setLibraryIndex(ItemID, navPath);
            dismissprogress();
        }

    }


}
