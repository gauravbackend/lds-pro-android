package com.ldsscriptures.pro.activity;

import android.content.ContentValues;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.text.Editable;
import android.text.TextWatcher;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.ldsscriptures.pro.BuildConfig;
import com.ldsscriptures.pro.R;
import com.ldsscriptures.pro.database.CommonDatabaseHelper;
import com.ldsscriptures.pro.retroutils.APIClient;
import com.ldsscriptures.pro.retroutils.APIInterface;
import com.ldsscriptures.pro.service.SyncEventReceiver;
import com.ldsscriptures.pro.utils.Global;
import com.ldsscriptures.pro.utils.SessionManager;
import com.ldsscriptures.pro.utils.Validator;
import com.ldsscriptures.pro.utils.WordUtils;

import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class SignUpActivity extends BaseActivity {

    @BindView(R.id.edtName)
    EditText edtName;
    @BindView(R.id.edtEmail)
    EditText edtEmail;
    @BindView(R.id.edtPassword)
    EditText edtPassword;
    @BindView(R.id.edtConfirmPass)
    EditText edtConfirmPass;
    @BindView(R.id.btnSignUp)
    Button btnSignUp;
    @BindView(R.id.imageView_background)
    ImageView imageView_background;

    static SignUpActivity signUpActivity;
    private SessionManager sessionManager;
    private CommonDatabaseHelper commonDatabaseHelper;

    String currantDate;
    int currantDayHour;
    Calendar calendar;


    public static SignUpActivity getInstance() {
        return signUpActivity;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);
        setContents();
    }

    private void setContents() {
        signUpActivity = this;

        sessionManager.setPremiumSelected(false);
        commonDatabaseHelper = new CommonDatabaseHelper(this);

        ButterKnife.bind(this);
        setBackground(R.drawable.bg_splash_blur, imageView_background);
        sessionManager = new SessionManager(SignUpActivity.this);

        setCapitalizeTextWatcher(edtName);
        addTextWatcher(edtName);
        addTextWatcher(edtEmail);
        addTextWatcher(edtPassword);
        addTextWatcher(edtConfirmPass);


        currantDate = new SimpleDateFormat("yyyy-MM-dd").format(Calendar.getInstance().getTime());
        calendar = Calendar.getInstance();
        currantDayHour = calendar.get(Calendar.HOUR);
    }

    private void addTextWatcher(final EditText edtText) {
        edtText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                edtText.setError(null);
            }
        });
    }

    private boolean validateFields() {
        if (!Validator.checkEmpty(edtName)) {
            edtName.setError(getString(R.string.error_enterName));
            return false;
        }

        if (!Validator.checkEmpty(edtEmail)) {
            edtEmail.setError(getString(R.string.error_EnterEmail));
            return false;
        }
        if (!Validator.checkEmail(edtEmail)) {
            edtEmail.setError(getString(R.string.error_validEmail));
            return false;
        }
        if (!Validator.checkEmpty(edtPassword)) {
            edtPassword.setError(getString(R.string.error_enterPassword));
            return false;
        }

        if (!Validator.checkLength(edtPassword)) {
            edtPassword.setError(getString(R.string.error_passwordLenght));
            return false;
        }
        if (!Validator.checkEmpty(edtConfirmPass)) {
            edtConfirmPass.setError(getString(R.string.error_enterConfirmPassword));
            return false;
        }
        if (!Validator.comparePWD(edtPassword, edtConfirmPass)) {
            edtConfirmPass.setError(getString(R.string.password_didnot_match));
            return false;
        }
        return true;
    }

    @OnClick(R.id.btnSignUp)
    public void signUp() {
        if (validateFields()) {
            if (Global.isNetworkAvailable(SignUpActivity.this)) {
                SelectImageActivity.croppedBitmap = null;
                startActivity(new Intent(SignUpActivity.this, SelectImageActivity.class));
                overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
            } else {
                Global.showNetworkAlert(this);
            }
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent = new Intent(SignUpActivity.this, LoginActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
        overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
        finish();
    }

    public void signUpNewUSer(String imageUrl) {

        showprogress();
        APIInterface apiService = APIClient.getClient().create(APIInterface.class);
        String url = APIClient.BASE_URL;

        url += "addUser.pl?username=" + edtName.getText().toString().trim() +
                "&password=" + edtPassword.getText().toString().trim() +
                "&email=" + edtEmail.getText().toString().trim() +
                "&device=" + APIClient.DEVICE_KEY +
                "&appversion=" + BuildConfig.VERSION_NAME +
                "&newsletter=1" +
                "&udid=" + Global.GCMInstanceID +
                "&img_url=" + imageUrl +
                "&img_url=" + imageUrl +
                "&apiauth=" + APIClient.AUTH_KEY;

        Call<JsonObject> callObject = apiService.getLogin(url.replaceAll(" ", "%20"));

        callObject.enqueue(new Callback<JsonObject>() {
            @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
                try {
                    if (response.body() != null && !response.body().toString().trim().isEmpty()) {

                        JsonParser jsonParser = new JsonParser();
                        JSONObject jsonObject = new JSONObject(response.body().toString());

                        if (Integer.parseInt(jsonObject.getString("success")) == 0) {

                            String mess = jsonObject.getString("msg");
                            Toast.makeText(getApplicationContext(), mess, Toast.LENGTH_LONG).show();

                        } else {

                            String session_id = jsonObject.getString("session_id");
                            String user_achievements = jsonObject.getString("achievements");
                            String user_goal_points = jsonObject.getString("goal_points");

                            String user_day_streak = jsonObject.getString("day_streak");
                            String user_fb_user = jsonObject.getString("fb_user");
                            String user_points = jsonObject.getString("points");
                            String user_img_url = jsonObject.getString("img_url");

                            String user_annotations = jsonObject.getString("annotations");
                            String user_fb_id = jsonObject.getString("fb_id");
                            String user_username = jsonObject.getString("username");
                            String user_email = jsonObject.getString("email");
                            String user_goal_hit = jsonObject.getString("goal_hit");
                            String user_min_reading = jsonObject.getString("min_reading");
                            String user_id = jsonObject.getString("user_id");
                            String private_status = jsonObject.getString("private");
                            String time_zone = jsonObject.getString("server_time_zone");

                            sessionManager.createLoginSession(session_id, null, null, null, user_achievements, user_goal_points, user_day_streak, user_fb_user, user_points, user_img_url, user_annotations, user_fb_id, user_username, user_email, user_goal_hit, user_min_reading, user_id, private_status);
                            sessionManager.checkGoalSet();
                            sessionManager.setUserDailyPoints(Integer.parseInt(user_points));
                            sessionManager.setUserMinReading(user_min_reading);
                            sessionManager.setUserID(user_id);
                            sessionManager.setUserSessionId(session_id);
                            sessionManager.setKeyTimeZone(time_zone);

                            insertDailyPointData(currantDate, currantDayHour, Integer.parseInt(user_min_reading), Integer.parseInt(user_points), user_id);
                            Global.UserId = user_id;

                            sessionManager.setSyncStatus(true);
                            SyncEventReceiver.setupAlarm(getApplicationContext());

                        }

                        dismissprogress();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                call.cancel();
                dismissprogress();
            }
        });
    }

    public static void setCapitalizeTextWatcher(final EditText editText) {
        final TextWatcher textWatcher = new TextWatcher() {

            int mStart = 0;

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                mStart = start + count;
            }

            @Override
            public void afterTextChanged(Editable s) {
                //Use WordUtils.capitalizeFully if you only want the first letter of each word to be capitalized
                String capitalizedText = WordUtils.capitalize(editText.getText().toString());
                if (!capitalizedText.equals(editText.getText().toString())) {
                    editText.addTextChangedListener(new TextWatcher() {
                        @Override
                        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                        }

                        @Override
                        public void onTextChanged(CharSequence s, int start, int before, int count) {

                        }

                        @Override
                        public void afterTextChanged(Editable s) {
                            editText.setSelection(mStart);
                            editText.removeTextChangedListener(this);
                        }
                    });
                    editText.setText(capitalizedText);
                }
            }
        };

        editText.addTextChangedListener(textWatcher);
    }

    public void insertDailyPointData(String currantDate, int currantDayHour, int minutes, int points, String user_id) {

        ContentValues contentValues = new ContentValues();
        contentValues.put(CommonDatabaseHelper.KEY_DAILY_LOCAL_USE_DATE, currantDate);
        contentValues.put(CommonDatabaseHelper.KEY_DAILY_LOCAL_USE_HOUR, currantDayHour);
        contentValues.put(CommonDatabaseHelper.KEY_DAILY_MINUTES, minutes);
        contentValues.put(CommonDatabaseHelper.KEY_DAILY_POINTS, points);
        contentValues.put(CommonDatabaseHelper.KEY_DAILY_USER_ID, user_id);

        commonDatabaseHelper.insertData(CommonDatabaseHelper.USER_DAILY, contentValues);
    }

}
