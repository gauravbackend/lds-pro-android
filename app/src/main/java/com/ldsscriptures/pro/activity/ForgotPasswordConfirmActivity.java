package com.ldsscriptures.pro.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.widget.TextView;

import com.ldsscriptures.pro.R;
import com.ldsscriptures.pro.utils.Global;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


public class ForgotPasswordConfirmActivity extends BaseActivity {
    @BindView(R.id.tvDone)
    TextView tvDone;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password_conform_msg);
        ButterKnife.bind(this);
        setContents();
    }

    private void setContents() {
        ButterKnife.bind(this);
    }

    @OnClick(R.id.tvDone)
    public void onViewClicked() {
        if (LoginActivity.getInstance() != null) {
            LoginActivity.getInstance().passwordRecovered();
        }
        finish();
    }
}
