package com.ldsscriptures.pro.activity;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.View;
import android.widget.ImageView;

import com.ldsscriptures.pro.R;
import com.ldsscriptures.pro.adapter.TabsViewPagerAdapter;
import com.ldsscriptures.pro.model.ReadingNowData;
import com.ldsscriptures.pro.utils.Global;
import com.ldsscriptures.pro.utils.SessionManager;
import com.lsjwzh.widget.recyclerviewpager.RecyclerViewPager;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class TabsActivity extends BaseActivity {

    @BindView(R.id.imageView_background)
    ImageView imageView_background;

    @BindView(R.id.viewPager)
    RecyclerViewPager recyclerView;

    @BindView(R.id.ivAddTab)
    ImageView ivAddTab;

    private ArrayList<ReadingNowData> tabsDataArrayList = new ArrayList<>();
    private SessionManager sessionManager;
    private TabsViewPagerAdapter tabsRecyclerviewAdapter;
    private LinearLayoutManager layoutManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tabs);
        setContents();
    }

    private void setContents() {
        ButterKnife.bind(this);
        sessionManager = new SessionManager(TabsActivity.this);
        setRecyerview();

        tabsDataArrayList = sessionManager.getPreferencesArrayList(TabsActivity.this);


        if (sessionManager.getPreferencesArrayList(TabsActivity.this) != null) {
            if (sessionManager.getPreferencesArrayList(TabsActivity.this).size() != 0) {
                tabsDataArrayList = sessionManager.getPreferencesArrayList(TabsActivity.this);
                tabsRecyclerviewAdapter = new TabsViewPagerAdapter(TabsActivity.this, tabsDataArrayList);
                layoutManager = new LinearLayoutManager(TabsActivity.this, RecyclerView.HORIZONTAL, false);
                recyclerView.setLayoutManager(layoutManager);
                recyclerView.setHasFixedSize(true);
                recyclerView.setAdapter(tabsRecyclerviewAdapter);
            }
        }

        //Swipe to Delete
        ItemTouchHelper swipeToDismissTouchHelper = new ItemTouchHelper(new ItemTouchHelper.SimpleCallback(
                ItemTouchHelper.UP | ItemTouchHelper.DOWN, ItemTouchHelper.UP | ItemTouchHelper.DOWN) {
            @Override
            public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
                return false;
            }

            @Override
            public int getMovementFlags(RecyclerView recyclerView, RecyclerView.ViewHolder holder) {
                int position = holder.getAdapterPosition();
                int dragFlags = 0; // whatever your dragFlags need to be

                int swipeFlags = position == tabsDataArrayList.size() - 1 ? 0 : ItemTouchHelper.UP | ItemTouchHelper.DOWN;

                return makeMovementFlags(dragFlags, swipeFlags);
            }

            @Override
            public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {
                int swipedPosition = viewHolder.getAdapterPosition();
                tabsDataArrayList.remove(swipedPosition);
                sessionManager.setPreferencesArrayList(TabsActivity.this, tabsDataArrayList);

                tabsRecyclerviewAdapter = new TabsViewPagerAdapter(TabsActivity.this, tabsDataArrayList);
                recyclerView.setAdapter(tabsRecyclerviewAdapter);

                recyclerView.scrollToPosition(swipedPosition);
            }

        });
        swipeToDismissTouchHelper.attachToRecyclerView(recyclerView);
    }

    private void setRecyerview() {
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int scrollState) {
//                updateState(scrollState);
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int i, int i2) {
//                mPositionText.setText("First: " + mRecyclerViewPager.getFirstVisiblePosition());
                int childCount = TabsActivity.this.recyclerView.getChildCount();
                int width = TabsActivity.this.recyclerView.getChildAt(0).getWidth();
                int padding = (TabsActivity.this.recyclerView.getWidth() - width) / 2;
//                mCountText.setText("Count: " + childCount);

                for (int j = 0; j < childCount; j++) {
                    View v = recyclerView.getChildAt(j);
                    float rate = 0;
                    if (v.getLeft() <= padding) {
                        if (v.getLeft() >= padding - v.getWidth()) {
                            rate = (padding - v.getLeft()) * 1f / v.getWidth();
                        } else {
                            rate = 1;
                        }
                        v.setScaleY(1 - rate * 0.1f);
                        v.setScaleX(1 - rate * 0.1f);

                    } else {
                        if (v.getLeft() <= recyclerView.getWidth() - padding) {
                            rate = (recyclerView.getWidth() - padding - v.getLeft()) * 1f / v.getWidth();
                        }
                        v.setScaleY(0.9f + rate * 0.1f);
                        v.setScaleX(0.9f + rate * 0.1f);
                    }
                }
            }
        });

        recyclerView.addOnPageChangedListener(new RecyclerViewPager.OnPageChangedListener() {
            @Override
            public void OnPageChanged(int oldPosition, int newPosition) {
                Global.printLog("test", "oldPosition:" + oldPosition + " newPosition:" + newPosition);
            }
        });

        recyclerView.addOnLayoutChangeListener(new View.OnLayoutChangeListener() {
            @Override
            public void onLayoutChange(View v, int left, int top, int right, int bottom, int oldLeft, int oldTop, int oldRight, int oldBottom) {
                if (recyclerView.getChildCount() < 3) {
                    if (recyclerView.getChildAt(1) != null) {
                        if (recyclerView.getCurrentPosition() == 0) {
                            View v1 = recyclerView.getChildAt(1);
                            v1.setScaleY(0.9f);
                            v1.setScaleX(0.9f);
                        } else {
                            View v1 = recyclerView.getChildAt(0);
                            v1.setScaleY(0.9f);
                            v1.setScaleX(0.9f);
                        }
                    }
                } else {
                    if (recyclerView.getChildAt(0) != null) {
                        View v0 = recyclerView.getChildAt(0);
                        v0.setScaleY(0.9f);
                        v0.setScaleX(0.9f);
                    }
                    if (recyclerView.getChildAt(2) != null) {
                        View v2 = recyclerView.getChildAt(2);
                        v2.setScaleY(0.9f);
                        v2.setScaleX(0.9f);
                    }
                }
            }
        });
    }

    @OnClick(R.id.ivClose)
    public void closeTabs() {
        finish();
    }

    @OnClick(R.id.ivAddTab)
    public void addNewTab() {
        ArrayList<ReadingNowData> readingNowDataArrayList = sessionManager.getPreferencesArrayList(TabsActivity.this);
        if (readingNowDataArrayList == null) {
            readingNowDataArrayList = new ArrayList<>();
        }
        ReadingNowData readingNowData = new ReadingNowData();
        if (Global.getPrefrenceString(TabsActivity.this, Global.ADD_NEW_TAB_PATH, "") != null &&
                Global.getPrefrenceString(TabsActivity.this, Global.ADD_NEW_TAB_CLASS_NAME, "") != null) {
            readingNowData.setPath(Global.getPrefrenceString(TabsActivity.this, Global.ADD_NEW_TAB_PATH, ""));
            readingNowData.setTag(Global.getPrefrenceString(TabsActivity.this, Global.ADD_NEW_TAB_CLASS_NAME, ""));
        }

        readingNowData.setActivityIdentifier(getString(R.string.tabs_lib_collection));
        readingNowDataArrayList.add(0, readingNowData);
        sessionManager.setPreferencesArrayList(TabsActivity.this, readingNowDataArrayList);
        tabsDataArrayList = sessionManager.getPreferencesArrayList(TabsActivity.this);

        tabsRecyclerviewAdapter = new TabsViewPagerAdapter(TabsActivity.this, tabsDataArrayList);
        recyclerView.setAdapter(tabsRecyclerviewAdapter);
    }
}
