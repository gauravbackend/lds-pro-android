package com.ldsscriptures.pro.activity.socialmenu;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.ldsscriptures.pro.R;
import com.ldsscriptures.pro.activity.BaseActivity;
import com.ldsscriptures.pro.activity.MainActivity;
import com.ldsscriptures.pro.adapter.FollowingFindFriendPagerAdapter;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class FollowingFindFriendActivity extends BaseActivity {

    @BindView(R.id.following_tab_layout)
    TabLayout followingTabLayout;
    @BindView(R.id.followingpager)
    ViewPager followingpager;
    @BindView(R.id.ivMenu)
    ImageView ivMenu;
    @BindView(R.id.ivBack)
    ImageView ivBack;
    @BindView(R.id.tvTitle)
    TextView tvTitle;
    @BindView(R.id.ivSearch)
    ImageView ivSearch;
    @BindView(R.id.ivMore)
    ImageView ivMore;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.main_layout)
    RelativeLayout mainLayout;
    @BindView(R.id.ivTabs)
    ImageView ivTabs;

    FollowingFindFriendPagerAdapter mAdapter;
    static FollowingFindFriendActivity followingFindFriendActivity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_following);
        ButterKnife.bind(this);
        setContents();
    }

    public void setContents() {

        followingFindFriendActivity = this;
        showText(tvTitle, getString(R.string.find_people));

        setToolbar(toolbar);
        setVisibilityGone(ivMore);
        setVisibilityGone(ivTabs);
        setVisibilityGone(ivSearch);
        ivMenu.setVisibility(View.GONE);
        ivBack.setVisibility(View.VISIBLE);

        followingTabLayout.addTab(followingTabLayout.newTab().setText(getResources().getString(R.string.facebook).toUpperCase()));
        followingTabLayout.addTab(followingTabLayout.newTab().setText(R.string.contacts));

        mAdapter = new FollowingFindFriendPagerAdapter(getSupportFragmentManager(), followingTabLayout.getTabCount());
        followingpager.setAdapter(mAdapter);
        followingpager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(followingTabLayout));
        followingTabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                followingpager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }

    @OnClick(R.id.ivMenu)
    public void openMenu() {
        openSlideMenu();
    }

    @OnClick({R.id.ivBack})
    public void onViewClicked(View view) {
        finish();
    }

    public static FollowingFindFriendActivity getInstance() {
        return followingFindFriendActivity;
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menuBack:
                finish();
                break;
            case R.id.menuNext:
                Toast.makeText(getApplicationContext(), "No Item Found", Toast.LENGTH_LONG).show();
                break;
            case R.id.menuBookmark:
                Intent bookmarkIntent = new Intent(getApplicationContext(), MainActivity.class);
                bookmarkIntent.putExtra(getString(R.string.menuSelection), "Bookmarks");
                bookmarkIntent.putExtra(getString(R.string.isFromReading), true);
                startActivityWithAnimation(bookmarkIntent);
                break;
            case R.id.menuHistory:
                Intent historyIntent = new Intent(getApplicationContext(), MainActivity.class);
                historyIntent.putExtra(getString(R.string.menuSelection), "History");
                historyIntent.putExtra(getString(R.string.isFromReading), true);
                startActivityWithAnimation(historyIntent);
                break;
        }
        return super.onOptionsItemSelected(item);
    }

}
