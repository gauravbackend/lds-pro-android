package com.ldsscriptures.pro.activity.socialmenu;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.ldsscriptures.pro.LoadMoreData.OnLoadMoreListener;
import com.ldsscriptures.pro.LoadMoreData.RecyclerViewLoadMoreScroll;
import com.ldsscriptures.pro.R;
import com.ldsscriptures.pro.activity.BaseActivity;
import com.ldsscriptures.pro.activity.MainActivity;
import com.ldsscriptures.pro.adapter.TimelineListAdapter;
import com.ldsscriptures.pro.model.TimeLine.InnerTimeLineModel;
import com.ldsscriptures.pro.model.TimeLine.MainTimeLineModel;
import com.ldsscriptures.pro.retroutils.APIClient;
import com.ldsscriptures.pro.retroutils.APIInterface;
import com.ldsscriptures.pro.utils.SessionManager;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class TimelineActivity extends BaseActivity {

    @BindView(R.id.ivMenu)
    ImageView ivMenu;
    @BindView(R.id.ivBack)
    ImageView ivBack;
    @BindView(R.id.tvTitle)
    TextView tvTitle;
    @BindView(R.id.ivSearch)
    ImageView ivSearch;
    @BindView(R.id.ivMore)
    ImageView ivMore;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.main_layout)
    RelativeLayout mainLayout;
    @BindView(R.id.timeline_recycler_view)
    RecyclerView timelineRecyclerView;
    @BindView(R.id.ivTabs)
    ImageView ivTabs;
    @BindView(R.id.timeline_progress)
    ProgressBar timelineProgress;
    @BindView(R.id.txt_no_timeline_data_found)
    TextView txtNoTimelineDataFound;

    String last_timeline_date = "";
    String getSyncAgain = "";

    static TimelineActivity timelineActivity;
    TimelineListAdapter mAdapter;

    private RecyclerViewLoadMoreScroll scrollListener;
    private ArrayList<InnerTimeLineModel> timelineItemArrayList;

    String paraUserId = "";
    String paraUserImage = "";
    String paraUserName = "";
    String paraSessionId = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_timeline);
        ButterKnife.bind(this);
        setContents();
    }

    public void setContents() {
        timelineActivity = this;

        showText(tvTitle, getString(R.string.txt_timeline));
        setToolbar(toolbar);
        setVisibilityGone(ivMore);
        setVisibilityGone(ivTabs);
        setVisibilityGone(ivSearch);

        paraUserId = getUserInfo(SessionManager.KEY_USER_ID);
        paraUserImage = getUserInfo(SessionManager.KEY_USR_IMG_URL);
        paraUserName = getUserInfo(SessionManager.KEY_USER_USERNAME);
        paraSessionId = getUserInfo(SessionManager.KEY_USER_SESSION_ID);

        timelineItemArrayList = new ArrayList<>();
        timelineItemArrayList.clear();
        timelineRecyclerView.setHasFixedSize(true);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        timelineRecyclerView.setLayoutManager(linearLayoutManager);
        mAdapter = new TimelineListAdapter(TimelineActivity.this, timelineItemArrayList, 0, paraUserId, paraUserImage, paraUserName, paraSessionId);
        timelineRecyclerView.setAdapter(mAdapter);

        scrollListener = new RecyclerViewLoadMoreScroll(linearLayoutManager);
        scrollListener.setOnLoadMoreListener(new OnLoadMoreListener() {
            @Override
            public void onLoadMore() {
                if (getSyncAgain.equals("1")) {
                    setVisibilityVisible(timelineProgress);
                    getTimeLineAPI(1);
                } else {
                    setVisibilityGone(timelineProgress);
                }
            }
        });

        timelineRecyclerView.addOnScrollListener(scrollListener);
        getTimeLineAPI(0);
    }

    public static TimelineActivity getInstance() {
        return timelineActivity;
    }

    @OnClick(R.id.ivMenu)
    public void openMenu() {
        openSlideMenu();
    }

    private void getTimeLineAPI(final int refreshFlag) {

        if (refreshFlag == 0) {
            showprogress();
        }
        String paraUserId = "", paraSessionId = "";

        if (getUserInfo(SessionManager.KEY_USER_ID) != null) {
            paraUserId = getUserInfo(SessionManager.KEY_USER_ID);
        }

        if (getUserInfo(SessionManager.KEY_USER_SESSION_ID) != null) {
            paraSessionId = getUserInfo(SessionManager.KEY_USER_SESSION_ID);
        }

        APIInterface apiService = APIClient.getClient().create(APIInterface.class);
        String url = APIClient.BASE_URL;

        url += "timelineSync.pl?user_id=" + paraUserId +
                "&session_id=" + paraSessionId +
                "&apiauth=" + APIClient.AUTH_KEY +
                "&last_timeline_date=" + last_timeline_date;

        Call<MainTimeLineModel<InnerTimeLineModel>> callObject = apiService.getTimeLineData(url.replaceAll(" ", "%20"));

        callObject.enqueue(new Callback<MainTimeLineModel<InnerTimeLineModel>>() {

            @Override
            public void onResponse(Call<MainTimeLineModel<InnerTimeLineModel>> call, Response<MainTimeLineModel<InnerTimeLineModel>> response) {

                if (response.body() != null && !response.body().toString().trim().isEmpty()) {

                    last_timeline_date = response.body().getSync_again_date();
                    getSyncAgain = response.body().getSync_again();
                    if (response.body().getItems() != null) {
                        if ((response.body().getItems().size() != 0)) {
                            timelineItemArrayList.addAll(response.body().getItems());
                        }
                    }
                    mAdapter.notifyDataSetChanged();
                    scrollListener.setLoaded();
                    setVisibilityGone(timelineProgress);

                    if (refreshFlag == 0) {
                        dismissprogress();
                    }

                    if (timelineItemArrayList.size() == 0) {
                        txtNoTimelineDataFound.setText(getString(R.string.nothing_shared_yet));
                        txtNoTimelineDataFound.setVisibility(View.VISIBLE);
                        timelineRecyclerView.setVisibility(View.GONE);
                    } else {
                        txtNoTimelineDataFound.setVisibility(View.GONE);
                        timelineRecyclerView.setVisibility(View.VISIBLE);
                    }
                }
            }

            @Override
            public void onFailure(Call<MainTimeLineModel<InnerTimeLineModel>> call, Throwable t) {
                if (refreshFlag == 0) {
                    dismissprogress();
                }
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menuBack:
                finish();
                break;
            case R.id.menuNext:
                Toast.makeText(getApplicationContext(), "No Item Found", Toast.LENGTH_LONG).show();
                break;
            case R.id.menuBookmark:
                Intent bookmarkIntent = new Intent(getApplicationContext(), MainActivity.class);
                bookmarkIntent.putExtra(getString(R.string.menuSelection), "Bookmarks");
                bookmarkIntent.putExtra(getString(R.string.isFromReading), true);
                startActivityWithAnimation(bookmarkIntent);
                break;
            case R.id.menuHistory:
                Intent historyIntent = new Intent(getApplicationContext(), MainActivity.class);
                historyIntent.putExtra(getString(R.string.menuSelection), "History");
                historyIntent.putExtra(getString(R.string.isFromReading), true);
                startActivityWithAnimation(historyIntent);
                break;
        }
        return super.onOptionsItemSelected(item);
    }

}
