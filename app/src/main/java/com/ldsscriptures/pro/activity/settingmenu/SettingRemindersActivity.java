package com.ldsscriptures.pro.activity.settingmenu;

import android.app.TimePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.ldsscriptures.pro.R;
import com.ldsscriptures.pro.activity.BaseActivity;
import com.ldsscriptures.pro.model.PlanReminderData;
import com.ldsscriptures.pro.model.SettingPlanReminderMainData;
import com.ldsscriptures.pro.service.PlanNotificationEventReceiver;
import com.ldsscriptures.pro.utils.SessionManager;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by IBL InfoTech on 8/16/2017.
 */

public class SettingRemindersActivity extends BaseActivity implements CompoundButton.OnCheckedChangeListener {

    @BindView(R.id.ivMenu)
    ImageView ivMenu;
    @BindView(R.id.tvTitle)
    TextView tvTitle;
    @BindView(R.id.ivadjustment)
    ImageView ivadjustment;
    @BindView(R.id.ivMore)
    ImageView ivMore;
    @BindView(R.id.layout_actitivity)
    LinearLayout layoutActitivity;
    @BindView(R.id.txtSettingSelectTime)
    TextView txtSettingSelectTime;
    @BindView(R.id.switch_Time)
    Switch switchTime;
    @BindView(R.id.SettingbtnLayoutWeekly)
    LinearLayout SettingbtnLayoutWeekly;
    @BindView(R.id.main_layout)
    RelativeLayout mainLayout;
    private ArrayList<PlanReminderData> planReminderDataArrayList = new ArrayList<>();
    private ArrayList<SettingPlanReminderMainData> planReminderMainDataArrayList = new ArrayList<>();
    static SettingRemindersActivity settingRemindersActivity;

    int var_selectedHour, var_selectedMinute;
    String var_select_Time = "";
    String currant_date;
    Button[] btn;
    boolean flagchecked = false;
    SessionManager sessionManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.setting_reminders_layout);
        ButterKnife.bind(this);
        setContents();
    }

    public void setContents() {

        sessionManager = new SessionManager(this);
        showText(tvTitle, getResources().getString(R.string.reminders));
        switchTime.setOnCheckedChangeListener(this);
        currant_date = new SimpleDateFormat("dd/MM/yyyy").format(Calendar.getInstance().getTime());
        planReminderDataArrayList.add(new PlanReminderData("1", "M", "Mo", false));
        planReminderDataArrayList.add(new PlanReminderData("2", "T", "Tu", false));
        planReminderDataArrayList.add(new PlanReminderData("3", "W", "We", false));
        planReminderDataArrayList.add(new PlanReminderData("4", "T", "Th", false));
        planReminderDataArrayList.add(new PlanReminderData("5", "F", "Fr", false));
        planReminderDataArrayList.add(new PlanReminderData("6", "S", "Sa", false));
        planReminderDataArrayList.add(new PlanReminderData("7", "S", "Su", false));

        LinearLayout.LayoutParams param = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT, 1.0f);
        param.setMargins(5, 0, 5, 0);
        btn = new Button[planReminderDataArrayList.size()];
        for (int i = 0; i < planReminderDataArrayList.size(); i++) {
            btn[i] = new Button(getApplicationContext());
            btn[i].setText(planReminderDataArrayList.get(i).getPlanReminderText());
            btn[i].setTextColor(getResources().getColor(R.color.textColor));
            btn[i].setBackgroundResource(0);
            btn[i].setTextSize(16);
            btn[i].setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT));
            btn[i].setLayoutParams(param);
            btn[i].setPadding(0, 0, 0, 0);
            SettingbtnLayoutWeekly.addView(btn[i]);
            final int btnReminderFlag = i;

            btn[i].setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Boolean reminderFlag = planReminderDataArrayList.get(btnReminderFlag).getPlanReminderFlag();
                    if (!reminderFlag) {
                        v.setBackgroundResource(R.drawable.selectedroundedbutton);
                        planReminderDataArrayList.get(btnReminderFlag).setPlanReminderFlag(true);
                        btn[btnReminderFlag].setTextColor(getResources().getColor(R.color.textColorWhite));
                    } else {
                        v.setBackgroundResource(0);
                        planReminderDataArrayList.get(btnReminderFlag).setPlanReminderFlag(false);
                        btn[btnReminderFlag].setTextColor(getResources().getColor(R.color.textColor));
                    }
                }
            });
        }


        ArrayList<SettingPlanReminderMainData> planReminderMainDataArrayList = new ArrayList<>();
        ArrayList<PlanReminderData> planReminderDatas = new ArrayList<>();
        planReminderMainDataArrayList = sessionManager.getSettingPlanReminderData();
        if (planReminderMainDataArrayList != null) {
            for (int i = 0; i < planReminderMainDataArrayList.size(); i++) {
                planReminderDatas = planReminderMainDataArrayList.get(i).getPlanReminderDataArrayList();
                var_select_Time = planReminderMainDataArrayList.get(i).getPlanTime();

                for (int j = 0; j < planReminderDatas.size(); j++) {

                    boolean reminderflag = planReminderDatas.get(j).getPlanReminderFlag();

                    if (reminderflag == true) {
                        btn[j].setBackgroundResource(R.drawable.selectedroundedbutton);
                        txtSettingSelectTime.setText(var_select_Time);
                        switchTime.setChecked(true);
                        flagchecked = true;

                    }
                }
            }
        }

    }

    public static SettingRemindersActivity getInstance() {
        return settingRemindersActivity;
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

        if (isChecked) {
            saveReminder();
        } else {
            PlanNotificationEventReceiver.cancelAlarm(getApplicationContext());
            planReminderMainDataArrayList = new ArrayList<>();
            sessionManager.setSettingPlanReminderData(planReminderMainDataArrayList);
        }
    }

    @OnClick({R.id.ivMenu, R.id.txtSettingSelectTime})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.ivMenu:
                finish();
                break;
            case R.id.txtSettingSelectTime:
                openTimePikerDialog();
                break;
        }
    }

    public void openTimePikerDialog() {

        Calendar mcurrentTime = Calendar.getInstance();
        int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
        int minute = mcurrentTime.get(Calendar.MINUTE);
        TimePickerDialog mTimePicker;
        mTimePicker = new TimePickerDialog(SettingRemindersActivity.this, new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {

                String status = "AM";
                int hour_of_12_hour_format;
                if (selectedHour > 11) {
                    status = "PM";
                }
                if (selectedHour > 11) {
                    hour_of_12_hour_format = selectedHour - 12;
                } else {
                    hour_of_12_hour_format = selectedHour;
                }

                var_select_Time = hour_of_12_hour_format + ":" + selectedMinute + "  " + status;
                var_selectedHour = hour_of_12_hour_format;
                var_selectedMinute = selectedMinute;
                txtSettingSelectTime.setText(var_select_Time);

            }
        }, hour, minute, true);//Yes 24 hour time
        mTimePicker.show();
    }

    public void saveReminder() {

        if (flagchecked == false) {
            if (!var_select_Time.equals("")) {
                reminderData();
            } else {
                Toast.makeText(getApplicationContext(), "Please Select Time", Toast.LENGTH_LONG).show();
            }
        } else {
            reminderData();
        }

    }

    public void reminderData() {

        planReminderMainDataArrayList = sessionManager.getSettingPlanReminderData();

        if (planReminderMainDataArrayList != null) {
            setPlanReminderDataArrayList();
        } else {
            planReminderMainDataArrayList = new ArrayList<>();
            setPlanReminderDataArrayList();
        }
    }

    public void setPlanReminderDataArrayList() {
        planReminderMainDataArrayList.add(new SettingPlanReminderMainData(var_select_Time.toString(), var_selectedHour, var_selectedMinute, currant_date, planReminderDataArrayList));
        sessionManager.setSettingPlanReminderData(planReminderMainDataArrayList);
        PlanNotificationEventReceiver.setupAlarm(getApplicationContext());
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        setIntent(intent);
    }

    @Override
    protected void onResumeFragments() {
        super.onResumeFragments();
    }
}
