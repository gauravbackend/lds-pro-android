package com.ldsscriptures.pro.activity.studytools;

import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.ldsscriptures.pro.R;
import com.ldsscriptures.pro.activity.BaseActivity;
import com.ldsscriptures.pro.adapter.LessonSubListAdapter;
import com.ldsscriptures.pro.database.CommonDatabaseHelper;
import com.ldsscriptures.pro.floatinbutton.FloatingActionButton;
import com.ldsscriptures.pro.floatinbutton.FloatingActionMenu;
import com.ldsscriptures.pro.model.LessonSubItemData;
import com.ldsscriptures.pro.utils.EditItemTouchHelperCallback;
import com.ldsscriptures.pro.utils.Global;
import com.ldsscriptures.pro.utils.PushToServerData;
import com.ldsscriptures.pro.utils.SessionManager;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class LessonSubItemActivity extends BaseActivity {

    @BindView(R.id.ivBack)
    ImageView ivBack;
    @BindView(R.id.tvTitle)
    TextView tvTitle;
    @BindView(R.id.ivEdit)
    ImageView ivEdit;
    @BindView(R.id.ivMore)
    ImageView ivMore;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    @BindView(R.id.fabUrl)
    FloatingActionButton fabUrl;
    @BindView(R.id.fabScripture)
    FloatingActionButton fabScripture;
    @BindView(R.id.fabNote)
    FloatingActionButton fabNote;
    @BindView(R.id.fabBtn)
    FloatingActionMenu fabBtn;
    @BindView(R.id.tvNoData)
    TextView tvNoData;
    @BindView(R.id.ivMenu)
    ImageView ivMenu;
    @BindView(R.id.ivCheck)
    ImageView ivCheck;
    SessionManager sessionManager;
    private int lessonId;
    private CommonDatabaseHelper commonDatabaseHelper;
    private ArrayList<LessonSubItemData> lessonSubItemDataArrayList;
    public static LessonSubItemActivity lessonSubItemActivity;
    LessonSubListAdapter lessonSubListAdapter;
    ArrayList<LessonSubItemData> saveLessionSortOrderArraylist;

    public static LessonSubItemActivity getInstance() {
        return lessonSubItemActivity;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lessons_sub_item);
        ButterKnife.bind(this);
        setContents();
    }

    private void setContents() {
        ButterKnife.bind(this);
        sessionManager = new SessionManager(this);
        lessonSubItemActivity = this;
        commonDatabaseHelper = new CommonDatabaseHelper(LessonSubItemActivity.this);

        String title = getIntent().getStringExtra(getResources().getString(R.string.title));
        lessonId = getIntent().getIntExtra(getResources().getString(R.string.lesson_id), 0);

        setVisibilityGone(ivCheck);

        //set toolbar
        setToolbar(toolbar);
        showText(tvTitle, title);

        fabBtn.setMenuButtonColorNormalResId(R.color.sand);
        fabBtn.setMenuButtonColorPressedResId(R.color.sand);

        setLabelColor(fabUrl);
        setLabelColor(fabScripture);
        setLabelColor(fabNote);

        // set managerLayout for recyclerView
        recyclerView.setLayoutManager(new LinearLayoutManager(LessonSubItemActivity.this));
        saveLessionSortOrderArraylist = new ArrayList<>();
        setData();
    }

    public void setLabelColor(FloatingActionButton floatingActionButton) {
        String labelBg = "#" + Integer.toHexString(ContextCompat.getColor(LessonSubItemActivity.this, R.color.windowBg));
        String labelText = "#" + Integer.toHexString(ContextCompat.getColor(LessonSubItemActivity.this, R.color.gray_light));

        floatingActionButton.setLabelColors(Color.parseColor(labelBg), Color.parseColor(labelBg), Color.parseColor(labelBg));
        floatingActionButton.setLabelTextColor(Color.parseColor(labelText));
    }

    public void setData() {
        //get the category from the local database
        new GetLessonSubList().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, "");
    }

    @OnClick({R.id.ivBack, R.id.ivEdit, R.id.fabUrl, R.id.fabScripture, R.id.fabNote, R.id.fabBtn, R.id.ivCheck})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.ivBack:
                finish();
                break;
            case R.id.ivEdit:
                lessonSubListAdapter.isSetFalg(true);
                lessonSubListAdapter.notifyDataSetChanged();
                setVisibilityGone(ivEdit);
                setVisibilityVisible(ivCheck);
                break;
            case R.id.ivCheck:
                lessonSubListAdapter.isSetFalg(false);
                lessonSubListAdapter.notifyDataSetChanged();
                setVisibilityVisible(ivEdit);
                setVisibilityGone(ivCheck);

                saveLessionSortOrderArraylist = sessionManager.getLessionSortOrderArraylist();
                if (saveLessionSortOrderArraylist != null) {
                    if (saveLessionSortOrderArraylist.size() != 0) {

                        for (int i = 0; i < saveLessionSortOrderArraylist.size(); i++) {
                            String serverid = saveLessionSortOrderArraylist.get(i).getServerid();
                            String sort_order = saveLessionSortOrderArraylist.get(i).getSort_order();
                            String Lessonid = saveLessionSortOrderArraylist.get(i).getLessonid();
                            int LessonType = saveLessionSortOrderArraylist.get(i).getLessonType();
                            String Notetext = saveLessionSortOrderArraylist.get(i).getNotetext();
                            String Urltitle = saveLessionSortOrderArraylist.get(i).getUrltitle();
                            String setUrl = saveLessionSortOrderArraylist.get(i).getUrl();
                            String setUri = saveLessionSortOrderArraylist.get(i).getUri();
                            String VerseID = saveLessionSortOrderArraylist.get(i).getVerseID();
                            String VerseAid = saveLessionSortOrderArraylist.get(i).getVerseAid();
                            int id = saveLessionSortOrderArraylist.get(i).get_id();

                            ContentValues contentValues = new ContentValues();
                            contentValues.put(CommonDatabaseHelper.KEY_SORT_ORDER, sort_order);
                            contentValues.put(CommonDatabaseHelper.KEY_MODIFIED, "1");
                            commonDatabaseHelper.updateRowData(CommonDatabaseHelper.TABLE_SUB_ITEM_LESSONS, contentValues, CommonDatabaseHelper.KEY_ID + " = ?",
                                    new String[]{String.valueOf(id)});
                        }

                        saveLessionSortOrderArraylist = new ArrayList<>();
                        sessionManager.saveLessionSortOrderArraylist(saveLessionSortOrderArraylist);
                        new PushToServerData(this);
                    }
                }

                break;
            case R.id.fabUrl:
                fabBtn.toggle(false);
                startActivity(new Intent(LessonSubItemActivity.this, AddLessonUrlActivity.class).putExtra(getString(R.string.lesson_id), lessonId));
                break;
            case R.id.fabScripture:
                fabBtn.toggle(false);
                startActivity(new Intent(LessonSubItemActivity.this, AddLessonScripture.class).putExtra(getString(R.string.lesson_id), lessonId));
                break;
            case R.id.fabNote:
                fabBtn.toggle(false);
                startActivity(new Intent(LessonSubItemActivity.this, AddLessonNoteActivity.class).putExtra(getString(R.string.lesson_id), lessonId));
                break;
            case R.id.fabBtn:
                break;
        }
    }

    private class GetLessonSubList extends AsyncTask<String, Void, String> {

        @Override
        protected void onPreExecute() {
            Global.showProgressDialog(LessonSubItemActivity.this);
        }

        @Override
        protected String doInBackground(String... params) {
            Cursor cursor;
            cursor = commonDatabaseHelper.getLessonSubList(String.valueOf(lessonId));
            lessonSubItemDataArrayList = new ArrayList<>();
            if (cursor != null && cursor.moveToFirst()) {
                while (!cursor.isAfterLast()) {

                    String deleted = cursor.getString(cursor.getColumnIndex("deleted"));
                    if (deleted == null) {
                        deleted = "";
                    }
                    if (deleted.isEmpty()) {

                        LessonSubItemData lessonSubItemData = new LessonSubItemData();
                        lessonSubItemData.set_id(cursor.getInt(cursor.getColumnIndex(CommonDatabaseHelper.KEY_ID)));
                        lessonSubItemData.setLessonid(cursor.getString(cursor.getColumnIndex(CommonDatabaseHelper.KEY_LESSONID)));
                        lessonSubItemData.setServerid(cursor.getString(cursor.getColumnIndex(CommonDatabaseHelper.KEY_SERVERID)));
                        lessonSubItemData.setLessonType(cursor.getInt(cursor.getColumnIndex(CommonDatabaseHelper.KEY_LESSONTYPE)));
                        lessonSubItemData.setNotetext(cursor.getString(cursor.getColumnIndex(CommonDatabaseHelper.KEY_NOTETEXT)));
                        lessonSubItemData.setUrltitle(cursor.getString(cursor.getColumnIndex(CommonDatabaseHelper.KEY_URLTITLE)));
                        lessonSubItemData.setUrl(cursor.getString(cursor.getColumnIndex(CommonDatabaseHelper.KEY_URL)));
                        lessonSubItemData.setUri(cursor.getString(cursor.getColumnIndex(CommonDatabaseHelper.KEY_URI)));
                        lessonSubItemData.setVerseID(cursor.getString(cursor.getColumnIndex(CommonDatabaseHelper.KEY_VERSEID)));
                        lessonSubItemData.setVerseAid(cursor.getString(cursor.getColumnIndex(CommonDatabaseHelper.KEY_VERSEAID1)));
                        lessonSubItemData.setVerseString(cursor.getString(cursor.getColumnIndex(CommonDatabaseHelper.KEY_VERSETEXT1)));
                        lessonSubItemData.setVerseTitle(cursor.getString(cursor.getColumnIndex(CommonDatabaseHelper.KEY_VERSETITLE1)));
                        lessonSubItemData.setDeleted(deleted);
                        lessonSubItemData.setModified(cursor.getString(cursor.getColumnIndex("modified")));
                        lessonSubItemData.setSort_order(cursor.getString(cursor.getColumnIndex("sort_order")));
                        lessonSubItemDataArrayList.add(lessonSubItemData);
                    }
                    cursor.moveToNext();
                }
            }
            commonDatabaseHelper.close();
            if (cursor != null) {
                cursor.close();
            }
            return "";
        }

        @Override
        protected void onPostExecute(String result) {
            Global.dismissProgressDialog();

            if (lessonSubItemDataArrayList.size() != 0) {
                setVisibilityVisible(recyclerView);
                setVisibilityVisible(ivEdit);
                setVisibilityGone(tvNoData);
                lessonSubListAdapter = new LessonSubListAdapter(LessonSubItemActivity.this, lessonSubItemDataArrayList);
                ItemTouchHelper.Callback callback = new EditItemTouchHelperCallback(lessonSubListAdapter);
                ItemTouchHelper mItemTouchHelper = new ItemTouchHelper(callback);
                mItemTouchHelper.attachToRecyclerView(recyclerView);
                recyclerView.setAdapter(lessonSubListAdapter);

            } else {
                tvNoData.setText(R.string.no_lesson_data_found);
                setVisibilityVisible(tvNoData);
                setVisibilityGone(recyclerView);
                setVisibilityGone(ivEdit);
            }
        }
    }

}
