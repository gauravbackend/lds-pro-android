package com.ldsscriptures.pro.activity.menuscreens;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.TextView;

import com.ldsscriptures.pro.R;
import com.ldsscriptures.pro.activity.BaseActivity;
import com.ldsscriptures.pro.adapter.MenuViewTagListAdapter;
import com.ldsscriptures.pro.model.VerseTagsData;
import com.ldsscriptures.pro.utils.SessionManager;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MenuViewTagActivity extends BaseActivity {
    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;

    @BindView(R.id.tvTitle)
    TextView tvTitle;
    String Tagname;
    private SessionManager sessionManager;
    private ArrayList<VerseTagsData> tagDataArrayList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu_view_tag);
    }

    private void setContents() {
        ButterKnife.bind(this);

        Intent intent = getIntent();
        Tagname = intent.getStringExtra("Tagname");

        showText(tvTitle, Tagname.toString());
        sessionManager = new SessionManager(MenuViewTagActivity.this);
        setRecyclerView();
    }

    private void setRecyclerView() {

        tagDataArrayList = sessionManager.getPreferencesTagsArrayList(MenuViewTagActivity.this);

        ArrayList<VerseTagsData> verseTagsDatasList = new ArrayList<VerseTagsData>();
        if (tagDataArrayList != null) {
            if (tagDataArrayList.size() > 0) {

                for (int i = 0; i < tagDataArrayList.size(); i++) {
                    if (tagDataArrayList.get(i).getTagname().contains(Tagname)) {
                        if (tagDataArrayList.get(i).getDeleted().isEmpty()) {
                            VerseTagsData verseMainDatas = new VerseTagsData();

                            verseMainDatas.setServerid(tagDataArrayList.get(i).getServerid());
                            verseMainDatas.setVerseaid(tagDataArrayList.get(i).getVerseaid());
                            verseMainDatas.setItemuri(tagDataArrayList.get(i).getItemuri());
                            verseMainDatas.setTagTitle(tagDataArrayList.get(i).getTagTitle());
                            verseMainDatas.setVerseNumber(tagDataArrayList.get(i).getVerseNumber());
                            verseMainDatas.setTagname(tagDataArrayList.get(i).getTagname());
                            verseMainDatas.setDeleted(tagDataArrayList.get(i).getDeleted());
                            verseMainDatas.setModified(tagDataArrayList.get(i).getModified());

                            verseTagsDatasList.add(verseMainDatas);
                        }
                    }
                }

                MenuViewTagListAdapter tagListAdapter = new MenuViewTagListAdapter(MenuViewTagActivity.this, verseTagsDatasList);
                LinearLayoutManager layoutManager = new LinearLayoutManager(MenuViewTagActivity.this, RecyclerView.VERTICAL, false);
                recyclerView.setLayoutManager(layoutManager);
                recyclerView.setAdapter(tagListAdapter);
            }
        }
    }

    @OnClick(R.id.ivBack)
    public void finishActivity() {
        finish();
    }


    @Override
    protected void onResume() {
        super.onResume();
        setContents();
    }
}
