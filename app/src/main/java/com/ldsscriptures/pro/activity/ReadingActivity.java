package com.ldsscriptures.pro.activity;

import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.media.MediaPlayer;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.SeekBar;
import android.widget.TextView;

import com.ldsscriptures.pro.R;
import com.ldsscriptures.pro.adapter.FootnoteAdapter;
import com.ldsscriptures.pro.adapter.ReadingActivityFirstLevelMenuAdapter;
import com.ldsscriptures.pro.adapter.ReadingActivitySecondLevelMenuAdapter;
import com.ldsscriptures.pro.adapter.ViewPagerIndexAdapter;
import com.ldsscriptures.pro.database.CommonDatabaseHelper;
import com.ldsscriptures.pro.database.SubDataBaseHelper;
import com.ldsscriptures.pro.dbClass.GetDataFromDatabase;
import com.ldsscriptures.pro.fragment.HistoryFragment;
import com.ldsscriptures.pro.model.FootNotesData;
import com.ldsscriptures.pro.model.HistorySubData;
import com.ldsscriptures.pro.model.LsFullIndexData;
import com.ldsscriptures.pro.model.LsIndexPreviewData;
import com.ldsscriptures.pro.model.LsMainIndexData;
import com.ldsscriptures.pro.model.ReadingAudioData;
import com.ldsscriptures.pro.model.ReadingNow.Reading;
import com.ldsscriptures.pro.model.ReadingNowData;
import com.ldsscriptures.pro.model.ReadingViewpagerData;
import com.ldsscriptures.pro.service.ActivityEventReceiver;
import com.ldsscriptures.pro.utils.Global;
import com.ldsscriptures.pro.utils.SessionManager;
import com.ldsscriptures.pro.utils.Utilities;
import com.ldsscriptures.pro.utils.ViewDialog;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ReadingActivity extends BaseActivity {

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.viewpager)
    ViewPager viewpager;

    @BindView(R.id.seekBar)
    SeekBar seekBar;

    @BindView(R.id.ivPlaybackFwd)
    ImageView ivPlaybackFwd;

    @BindView(R.id.ivPlaybackBwd)
    ImageView ivPlaybackBwd;

    @BindView(R.id.ivClose)
    ImageView ivClose;

    @BindView(R.id.txtDuration)
    TextView txtDuration;

    @BindView(R.id.ll_header)
    LinearLayout ll_header;

    @BindView(R.id.llFootNotes_reading)
    LinearLayout llFootNotesReading;

    @BindView(R.id.fabPlay)
    FloatingActionButton fabPlay;

    @BindView(R.id.tvTitle)
    TextView tvTitle;

    @BindView(R.id.txtPlayingSpeed)
    TextView txtPlayingSpeed;

    @BindView(R.id.ivMore)
    ImageView ivMore;

    @BindView(R.id.linDropDown)
    LinearLayout linDropDown;

    @BindView(R.id.ivDropDown)
    ImageView ivDropDown;

    @BindView(R.id.txtReadingTime)
    TextView txtReadingTime;

    @BindView(R.id.ivMenu)
    ImageView ivMenu;

    @BindView(R.id.ivBack)
    ImageView ivBack;

    @BindView(R.id.ivSearch)
    ImageView ivSearch;

    @BindView(R.id.ivTabs)
    ImageView ivTabs;

    @BindView(R.id.fab)
    FloatingActionButton fab;

    @BindView(R.id.recyclerView_footnot)
    RecyclerView recyclerViewFootnot;

    @BindView(R.id.somemediatitle)
    TextView somemediatitle;

    @BindView(R.id.main_reading_layout)
    LinearLayout mainReadingLayout;

    public static LinearLayout mainSwipeLayout;
    public static View hide_view;


    private SessionManager sessionManager;
    static ReadingActivity readingActivity;
    private boolean isHistory;
    private boolean isFromIndex;
    private String lsId;
    private String lcId;
    private int indexForReading = 0;
    private int verseAId = 0;
    private int verseId = 0;
    private int postion = 0;
    private String verseIdArrayStrig = "";

    private ReadingActivitySecondLevelMenuAdapter readingActivitySecondLevelMenuAdapter;
    private RecyclerView rvSecondLevel;
    public static String title;
    private ViewPagerIndexAdapter viewPagerIndexAdapter;
    private ArrayList<LsMainIndexData> combineIndexDataArray;
    private int selectedItem = 0;
    private ArrayList<ReadingViewpagerData> readingViewpagerDataArrayList = new ArrayList<>();

    public MediaPlayer mp;
    private Utilities utils;

    private int seekForwardTime = 15000; // 15000 milliseconds
    private int seekBackwardTime = 15000; // 15000 milliseconds

    // Handler to update UI timer, progress bar etc,.
    private Handler mHandler = new Handler();
    private ArrayList<ReadingAudioData> readingAudioDataArrayList = new ArrayList<>();
    private AsyncDownloadAudio asyncDownloadAudio;
    public File mAudioFileTemp;
    public static final String TEMP_AUDIO_FILE_NAME = "readingAudio.mp3";
    double[] playingSpeedArr = {0.5, 1, 1.25, 2};
    int selectedPlayingSpeed = 1;

    // set for the count reading time.
    private Handler mHandlerReadingCount = new Handler();
    private int readingMinute = 0;
    private String AppFolderPath;

    // ProgressDialog pd;
    CommonDatabaseHelper commonDatabaseHelper;
    SubDataBaseHelper subDataBaseHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        readingActivity = this;
        setContentView(R.layout.main_coordinatorlayout);
        ButterKnife.bind(this);
        setToolbar(toolbar);
        mainSwipeLayout = (LinearLayout) findViewById(R.id.main_swipe_layout);
        hide_view = (View) findViewById(R.id.hide_view);
        sessionManager = new SessionManager(ReadingActivity.this);
        commonDatabaseHelper = new CommonDatabaseHelper(this);

        indexForReading = getIntent().getIntExtra(getString(R.string.indexForReading), 0);
        isHistory = getIntent().getBooleanExtra(getString(R.string.is_history), false);
        isFromIndex = getIntent().getBooleanExtra(getString(R.string.isFromIndex), false);
        lcId = getIntent().getStringExtra(getString(R.string.lcID));
        lsId = getIntent().getStringExtra(getString(R.string.lsId));

        subDataBaseHelper = new SubDataBaseHelper(this, lsId);

        Intent intent = getIntent();
        Bundle extras = intent.getExtras();
        if (extras != null) {
            if (extras.containsKey(getString(R.string.verseAId))) {
                verseAId = getIntent().getIntExtra(getString(R.string.verseAId), 0);
            }
            if (extras.containsKey(getString(R.string.verseId))) {
                verseId = getIntent().getIntExtra(getString(R.string.verseId), 0);
            }
            if (extras.containsKey(getString(R.string.postion))) {
                postion = getIntent().getIntExtra(getString(R.string.postion), 0);
            }
            if (extras.containsKey("verseIdArrayStrig")) {
                verseIdArrayStrig = getIntent().getStringExtra("verseIdArrayStrig");
            }
        }
        selectedItem = indexForReading;

        if (!isFromIndex) {
            ivMenu.setVisibility(View.VISIBLE);
            ivBack.setVisibility(View.GONE);

        } else {
            ivMenu.setVisibility(View.VISIBLE);
            ivBack.setVisibility(View.GONE);
        }

        setContents(selectedItem);
    }

    public void setContents(int selectedItem1) {

        ButterKnife.bind(this);
        readingActivity = this;
        selectedItem = selectedItem1;
        Global.audioFlag = 0;
        ivDropDown.setVisibility(View.VISIBLE);
        ivMore.setVisibility(View.GONE);

        if (getUserInfo(SessionManager.KEY_USER_MIN_READING) != null) {
            readingMinute = Integer.parseInt(getUserInfo(SessionManager.KEY_USER_MIN_READING));
        } else {
            readingMinute = 0;
        }
        mHandlerReadingCount.postDelayed(runnableReadingTime, 1000 * 60);
        AppFolderPath = Environment.getExternalStorageDirectory() + "/LDS/";
        String destPath = AppFolderPath + TEMP_AUDIO_FILE_NAME;
        mAudioFileTemp = new File(destPath);

        utils = new Utilities();
        mp = new MediaPlayer();

        if (ll_header.getVisibility() == View.VISIBLE) {
            setVisibilityGone(fabPlay);
            setVisibilityGone(ll_header);
        }

        setVisibilityGone(llFootNotesReading);

        try {
            new GetReadingData().execute();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        // set for the count Reading Time.

        if (getUserInfo(SessionManager.KEY_USER_MIN_READING) != null) {
            readingMinute = Integer.parseInt(getUserInfo(SessionManager.KEY_USER_MIN_READING));
        } else {
            readingMinute = 0;
        }
        mHandlerReadingCount.postDelayed(runnableReadingTime, 1000 * 60);
    }

    public class GetReadingData extends AsyncTask<String, Void, String> {

        ViewDialog alert = new ViewDialog();

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            alert.showDialog(ReadingActivity.this, "Loading...");
        }

        @Override
        protected String doInBackground(String... params) {

            readingViewpagerDataArrayList = new ArrayList<>();

            Cursor cursor = null;
            try {
                cursor = subDataBaseHelper.getReadingData();

                if (cursor != null && cursor.moveToFirst()) {

                    while (!cursor.isAfterLast()) {

                        ReadingViewpagerData readingViewpagerData = new ReadingViewpagerData();
                        readingViewpagerData.setId(cursor.getInt(cursor.getColumnIndex(SubDataBaseHelper.KEY_ID)));
                        readingViewpagerData.setPosition(cursor.getInt(cursor.getColumnIndex(SubDataBaseHelper.KEY_POSITION)));
                        readingViewpagerData.setTitle_html(cursor.getString(cursor.getColumnIndex(SubDataBaseHelper.KEY_TITLE_HTML)));
                        readingViewpagerData.setSubtitle(null);
                        readingViewpagerData.setUri(cursor.getString(cursor.getColumnIndex(SubDataBaseHelper.KEY_URI)));
                        readingViewpagerData.setLsSubIndexDatas(null);
                        readingViewpagerData.setHeader(false);

                        readingViewpagerDataArrayList.add(readingViewpagerData);

                        cursor.moveToNext();
                    }
                }

            } catch (Exception e) {

            } finally {
                if (cursor != null) {
                    cursor.close();
                }
                subDataBaseHelper.close();
            }

            return "";
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            try {
                if (readingViewpagerDataArrayList != null && readingViewpagerDataArrayList.size() != 0) {

                    viewPagerIndexAdapter = new ViewPagerIndexAdapter(ReadingActivity.this, readingViewpagerDataArrayList, lcId, lsId, verseAId, verseId, indexForReading, title, verseIdArrayStrig);
                    viewpager.setAdapter(viewPagerIndexAdapter);
                    viewpager.setCurrentItem((selectedItem - 1));
                    viewpager.setOffscreenPageLimit(0);
                    viewpager.invalidate();
                    Global.bookmarkData.setItemuri(readingViewpagerDataArrayList.get(selectedItem - 1).getUri());
                    getSubItemAudioData(readingViewpagerDataArrayList.get(selectedItem - 1).getId());
                    try {
                        sessionManager.setReadingNow(ReadingNowData.readingNowData);
                        setHistory((selectedItem), readingViewpagerDataArrayList.get(selectedItem - 1).getTitle_html(), readingViewpagerDataArrayList.get(selectedItem - 1).getUri());
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    viewpager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
                        public void onPageScrollStateChanged(int state) {
                            sessionManager.setoffset(0);
                            mainSwipeLayout.setVisibility(View.VISIBLE);
                            viewPagerIndexAdapter.getRecyclerViewNew().scrollToPosition(0);
                        }

                        public void onPageScrolled(final int position, float positionOffset, int positionOffsetPixels) {
                            //Global.printLog("AUDIODATA", "======onPageScrolled====");

                            title = readingViewpagerDataArrayList.get(position).getTitle_html();
                            showText(tvTitle, title);
                            showText(somemediatitle, title);
                            mp.release();
                            mp = new MediaPlayer();

                            if (ll_header.getVisibility() == View.VISIBLE) {
                                setVisibilityGone(fabPlay);
                                setVisibilityGone(ll_header);
                                viewPagerIndexAdapter.notifyDataSetChanged();
                            }
                            mp.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                                @Override
                                public void onCompletion(MediaPlayer mp1) {
                                    Global.audioFlag = 1;
                                    getSubItemAudioData(readingViewpagerDataArrayList.get(position + 1).getId());
                                }
                            });
                        }

                        public void onPageSelected(final int position) {

                            //Global.printLog("AUDIODATA", "======onPageSelected====");

                            setHistory(readingViewpagerDataArrayList.get(position).getId(), readingViewpagerDataArrayList.get(position).getTitle_html(), readingViewpagerDataArrayList.get(position).getUri());
                            ReadingNowData readingNowData = sessionManager.getReadingNow();
                            Global.bookmarkData.setItemuri(readingViewpagerDataArrayList.get(position).getUri());
                            sessionManager.setReadingNow(readingNowData);
                            mHandler.removeCallbacks(runnable);
                            String audioMin = txtDuration.getText().toString();
                            String[] separated = audioMin.split(":");
                            int audiomin = Integer.parseInt(separated[0]);
                            if (audiomin != 0) {
                                calculateAudioMin(audiomin);
                            }
                            showText(txtDuration, "0:00");
                            getSubItemAudioData(readingViewpagerDataArrayList.get(position).getId());
                        }
                    });
                }

                seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
                    @Override
                    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

                    }

                    @Override
                    public void onStartTrackingTouch(SeekBar seekBar) {
                        // remove message Handler from updating progress bar
                    }

                    @Override
                    public void onStopTrackingTouch(SeekBar seekBar) {

                        int totalDuration = mp.getDuration();
                        int currentPosition = utils.progressToTimer(seekBar.getProgress(), totalDuration);
                        mp.seekTo(currentPosition);

                    }
                });

                viewpager.post(new Runnable() {
                    @Override
                    public void run() {
                        alert.dimissDialog(ReadingActivity.this);
                    }
                });

            } catch (Exception e) {

            }
        }
    }

    private void setHistory(int selectedItem, String title, String itemURI) {

        // remove past history of same page.
        if (sessionManager.getPreferencesHistoryArrayList(ReadingActivity.this) != null) {
            ArrayList<HistorySubData> historySubDataArrayList = sessionManager.getPreferencesHistoryArrayList(ReadingActivity.this);
            if (historySubDataArrayList.size() != 0) {
                for (int i = 0; i < historySubDataArrayList.size(); i++) {
                    if (historySubDataArrayList.get(i).getTitle().equalsIgnoreCase(title)) {
                        historySubDataArrayList.remove(i);
                    }
                }
                sessionManager.setPreferencesHistoryArrayList(ReadingActivity.this, historySubDataArrayList);
            }
        }

        ArrayList<HistorySubData> historyDataArrayList;
        if (sessionManager.getPreferencesHistoryArrayList(this) != null) {
            historyDataArrayList = sessionManager.getPreferencesHistoryArrayList(this);
        } else {
            historyDataArrayList = new ArrayList<>();
        }


        HistorySubData historySubData = new HistorySubData();
        historySubData.setTitle(title);
        historySubData.setTag(this.getClass().getName());
        historySubData.setDate(Global.getCurrentDate());
        historySubData.setReadingIndex(selectedItem);
        historySubData.setFromIndex(isFromIndex);
        historySubData.setFromHistory(true);
        historySubData.setLcId(lcId);
        historySubData.setLsId(lsId);
        historySubData.setItemURI(itemURI);

        historyDataArrayList.add(0, historySubData);

        // set maximum 100 data into history
        if (historyDataArrayList.size() > 100)
            for (int i = historyDataArrayList.size() - 1; i >= 100; i--) {
                historyDataArrayList.remove(i);
            }

        sessionManager.setPreferencesHistoryArrayList(this, historyDataArrayList);
    }

    @OnClick(R.id.linDropDown)
    public void openDropDownMenu() {
        initiateTypePopUp(linDropDown);
    }

    @OnClick(R.id.ivMenu)

    public void openMenu() {
        openSlideMenu();
        ActivityEventReceiver.cancelAlarm(getApplicationContext());
    }

    @OnClick(R.id.txtPlayingSpeed)
    public void setPlayingSpeed() {
        if (Build.VERSION.SDK_INT >= 23) {
            double speed = 1;
            if (mp != null && mp.isPlaying()) {
                if (selectedPlayingSpeed != playingSpeedArr.length) {
                    speed = playingSpeedArr[selectedPlayingSpeed];
                } else {
                    selectedPlayingSpeed = 0;
                    speed = playingSpeedArr[selectedPlayingSpeed];
                }
                mp.setPlaybackParams(mp.getPlaybackParams().setSpeed((float) speed));
                showText(txtPlayingSpeed, playingSpeedArr[selectedPlayingSpeed] + "X");
                selectedPlayingSpeed++;
            }
        } else {
            Global.showToast(ReadingActivity.this, getString(R.string.playing_speed_message));
        }
    }

    private Runnable runnable = new Runnable() {
        @Override
        public void run() {
            long totalDuration = mp.getDuration();
            long currentDuration = mp.getCurrentPosition();

            txtDuration.setText("" + utils.milliSecondsToTimer(currentDuration));
            final int progress = (int) (utils.getProgressPercentage(currentDuration, totalDuration));
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    seekBar.setProgress(progress);
                }
            });
            mHandler.postDelayed(this, 1000);
        }

    };

    private Runnable runnableReadingTime = new Runnable() {
        @Override
        public void run() {
            readingMinute++;
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    txtReadingTime.setText("Timer Count : " + readingMinute);
                    sessionManager.setUserDailyPoints(60);
                    fetchPlanData(readingMinute);
                }
            });
            mHandlerReadingCount.postDelayed(this, 1000 * 60);
        }
    };

    @Override
    public void onBackPressed() {
        if (isHistory) {
            HistoryFragment.getInstance().setRecyclerView();
        }
        super.onBackPressed();
        Global.recycleviewpos = 0;
        Global.recycleviewOffset = 0;
        sessionManager.setoffset(0);
        ActivityEventReceiver.cancelAlarm(getApplicationContext());
    }

    @Override
    protected void onPause() {
        super.onPause();
        // when the ReadingActivity close that time reading time store into SessionManager.
        sessionManager.setUserMinReading(String.valueOf(readingMinute));
        mHandlerReadingCount.removeCallbacks(runnableReadingTime);
        ActivityEventReceiver.cancelAlarm(getApplicationContext());

    }

    public void playSong(SeekBar seekBar) {
        // Play song
        try {
            mp.reset();
            mp.setDataSource(mAudioFileTemp.getAbsolutePath());
            mp.prepare();
            // set Progress bar values
            seekBar.setProgress(0);
            seekBar.setMax(100);
        } catch (IllegalArgumentException | IllegalStateException | IOException e) {
            e.printStackTrace();
        }
    }

    public void showHideLayout() {
        if (ll_header.getVisibility() == View.VISIBLE) {
            // By default play first song
            playSong(seekBar);
            setVisibilityGone(fabPlay);
            setVisibilityGone(ll_header);
        } else {
            // By default play first song
            playSong(seekBar);
            fabPlay.setImageResource(R.drawable.play);
            setVisibilityVisible(fabPlay);
            setVisibilityVisible(ll_header);
        }
    }

    public void HideLayout() {
        if (ll_header.getVisibility() == View.VISIBLE) {
            playSong(seekBar);
            setVisibilityGone(fabPlay);
            setVisibilityGone(ll_header);
        }
    }

    public void showFootnotesLayout(ArrayList<FootNotesData> footNotesArrayList, int scrollPosition) {

        setVisibilityVisible(llFootNotesReading);
        if (footNotesArrayList != null && footNotesArrayList.size() != 0) {

            FootnoteAdapter footnoteAdapter = new FootnoteAdapter(getApplicationContext(), ReadingActivity.this, footNotesArrayList);
            recyclerViewFootnot.setAdapter(footnoteAdapter);
            recyclerViewFootnot.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
            recyclerViewFootnot.setHasFixedSize(true);
            recyclerViewFootnot.scrollToPosition(scrollPosition);
            footnoteAdapter.notifyDataSetChanged();
        }

    }

    private void getSubItemAudioData(int nextId) {

        readingAudioDataArrayList = new ArrayList<>();
        SubDataBaseHelper subDataBaseHelper = new SubDataBaseHelper(ReadingActivity.this, lsId);
        Cursor cursor = subDataBaseHelper.getReadingAudioFile(nextId);
        if (cursor != null && cursor.moveToFirst()) {
            while (!cursor.isAfterLast()) {
                ReadingAudioData readingAudioData = new ReadingAudioData();
                readingAudioData.setId(cursor.getString(cursor.getColumnIndex(SubDataBaseHelper.KEY_ID)));
                readingAudioData.setSubitem_id(cursor.getString(cursor.getColumnIndex(SubDataBaseHelper.KEY_AUDIO_SUBITEM_ID)));
                readingAudioData.setMedia_url(cursor.getString(cursor.getColumnIndex(SubDataBaseHelper.KEY_AUDIO_MEDIA_URL)));
                readingAudioData.setFile_size(cursor.getString(cursor.getColumnIndex(SubDataBaseHelper.KEY_AUDIO_FILE_SIZE)));
                readingAudioData.setDuration(cursor.getString(cursor.getColumnIndex(SubDataBaseHelper.KEY_AUDIO_DURATION)));
                readingAudioData.setRealted_audio_voice_id(cursor.getString(cursor.getColumnIndex(SubDataBaseHelper.KEY_AUDIO_REALTED_AUDIO_VOICE_ID)));
                readingAudioDataArrayList.add(readingAudioData);
                cursor.moveToNext();
            }
        }
        subDataBaseHelper.close();
        if (cursor != null) {
            cursor.close();
        }
        if (readingAudioDataArrayList.size() != 0) {
            asyncDownloadAudio = new AsyncDownloadAudio(ReadingActivity.this, readingAudioDataArrayList.get(0).getMedia_url());
            asyncDownloadAudio.execute();
        }
    }

    @OnClick(R.id.ivSearch)
    public void openSearch() {
        startActivityWithAnimation(new Intent(ReadingActivity.this, SearchActivity.class));
    }

    @OnClick(R.id.ivTabs)
    public void openTabs() {
        ArrayList<ReadingNowData> readingNowDataArrayList = sessionManager.getPreferencesArrayList(ReadingActivity.this);
        if (readingNowDataArrayList == null) {
            readingNowDataArrayList = new ArrayList<>();
            ReadingNowData readingNowData = new ReadingNowData();
            if (Global.getPrefrenceString(ReadingActivity.this, Global.ADD_NEW_TAB_PATH, "") != null && Global.getPrefrenceString(ReadingActivity.this, Global.ADD_NEW_TAB_CLASS_NAME, "") != null) {
                readingNowData.setPath(Global.getPrefrenceString(ReadingActivity.this, Global.ADD_NEW_TAB_PATH, ""));
                readingNowData.setTag(Global.getPrefrenceString(ReadingActivity.this, Global.ADD_NEW_TAB_CLASS_NAME, ""));
            }

            readingNowData.setActivityIdentifier(getString(R.string.tabs_lib_collection));
            readingNowDataArrayList.add(0, readingNowData);

            sessionManager.setPreferencesArrayList(ReadingActivity.this, readingNowDataArrayList);
        }

        ReadingNowData readingNowData = new ReadingNowData();
        readingNowData.setPath(Global.takeScreenshot(this, 24));
        readingNowData.setTag(this.getClass().getName());
        readingNowData.setBookmarked(readingNowDataArrayList.get(0).isBookmarked());
        readingNowData.setActivityIdentifier(getString(R.string.tabs_reading));

        readingNowData.setReading(new Reading(this.getClass().getName(), indexForReading, isHistory, isFromIndex, lcId, lsId));
        readingNowDataArrayList.set(0, readingNowData);
        sessionManager.setPreferencesArrayList(ReadingActivity.this, readingNowDataArrayList);

        startActivity(new Intent(this, TabsActivity.class));
        overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
    }

    @OnClick(R.id.ivPlaybackFwd)
    public void playBackForward() {
        // get current song positionƒ
        int currentPosition = mp.getCurrentPosition();
        // check if seekForward time is lesser than song duration
        if (currentPosition + seekForwardTime <= mp.getDuration()) {
            // forward song
            mp.seekTo(currentPosition + seekForwardTime);
        } else {
            // forward to end position
            mp.seekTo(mp.getDuration());
        }
    }

    @OnClick(R.id.ivPlaybackBwd)
    public void playBackBackword() {
        // get current song position
        int currentPosition = mp.getCurrentPosition();
        // check if seekBackward time is greater than 0 sec
        if (currentPosition - seekBackwardTime >= 0) {
            // forward song
            mp.seekTo(currentPosition - seekBackwardTime);
        } else {
            // backward to starting position
            mp.seekTo(0);
        }
    }

    @OnClick(R.id.fabPlay)
    public void mangeFab() {

        if (mp != null) {
            if (mp.isPlaying()) {
                if (mp != null) {
                    mp.pause();
                    // Changing button image to play button
                    fabPlay.setImageResource(R.drawable.play);
                }
            } else {
                mHandler.postDelayed(runnable, 1000);
                // Resume song
                if (mp != null) {
                    mp.start();
                    // Changing button image to pause button
                    fabPlay.setImageResource(R.drawable.pause);
                }
            }
        }
    }

    @OnClick(R.id.fab)
    public void manageFloatingButton() {
        mHandlerReadingCount.removeCallbacks(runnableReadingTime);
        manageFloatingBtn();
    }

    @OnClick(R.id.ivBack)
    public void finishAct() {
        onBackPressed();
    }

    @OnClick(R.id.ivClose)
    public void ivClose() {
        setVisibilityGone(llFootNotesReading);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mp != null) {
            mp.release();
        }
        mHandler.removeCallbacks(runnable);
        sessionManager.setUserMinReading(String.valueOf(readingMinute));
        mHandlerReadingCount.removeCallbacks(runnableReadingTime);
        ActivityEventReceiver.cancelAlarm(getApplicationContext());

        String audioMin = txtDuration.getText().toString();
        String[] separated = audioMin.split(":");
        int audiomin = Integer.parseInt(separated[0]);
        if (audiomin != 0) {
            calculateAudioMin(audiomin);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menuBack:
                if (viewpager.getCurrentItem() > 0)
                    viewpager.setCurrentItem(viewpager.getCurrentItem() - 1);
                else
                    finish();
                break;
            case R.id.menuNext:
                if (viewpager.getCurrentItem() != (readingViewpagerDataArrayList.size() - 1))
                    viewpager.setCurrentItem(viewpager.getCurrentItem() + 1);
                else
                    finish();
                break;
            case R.id.menuBookmark:
                Intent bookmarkIntent = new Intent(ReadingActivity.this, MainActivity.class);
                bookmarkIntent.putExtra(getString(R.string.menuSelection), "Bookmarks");
                bookmarkIntent.putExtra(getString(R.string.isFromReading), true);
                startActivity(bookmarkIntent);
                overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
                break;
            case R.id.menuHistory:
                Intent historyIntent = new Intent(ReadingActivity.this, MainActivity.class);
                historyIntent.putExtra(getString(R.string.menuSelection), "History");
                historyIntent.putExtra(getString(R.string.isFromReading), true);
                startActivity(historyIntent);
                overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void initiateTypePopUp(LinearLayout linDropDown) {
        LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        LinearLayout layout = (LinearLayout) inflater.inflate(R.layout.popup_drop_down, null);

        final PopupWindow pw = new PopupWindow(layout, ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT, true);

        pw.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        pw.setTouchable(true);
        pw.setFocusable(true);
        pw.setOutsideTouchable(false);
        pw.setHeight(ViewGroup.LayoutParams.WRAP_CONTENT);
        pw.setWidth(ViewGroup.LayoutParams.MATCH_PARENT);

        pw.setTouchInterceptor(new View.OnTouchListener() {

            public boolean onTouch(View v, MotionEvent event) {

                if (event.getAction() == MotionEvent.ACTION_OUTSIDE) {
                    pw.dismiss();
                    return true;
                }
                return false;
            }
        });

        pw.setOnDismissListener(new PopupWindow.OnDismissListener() {

            @Override
            public void onDismiss() {
            }
        });


        pw.setContentView(layout);

        int x = (int) linDropDown.getX();
        int y = (int) linDropDown.getY();

        pw.showAsDropDown(linDropDown, x, y);

        RecyclerView rvFirstLevel = (RecyclerView) layout.findViewById(R.id.rvFirstLevel);
        rvSecondLevel = (RecyclerView) layout.findViewById(R.id.rvSecondLevel);

        TextView txtLibSel = (TextView) layout.findViewById(R.id.txtLibSel);
        TextView txtLibColl = (TextView) layout.findViewById(R.id.txtLibColl);
        LinearLayout linLib = (LinearLayout) layout.findViewById(R.id.linLib);

        ArrayList<LsMainIndexData> arrayList = new ArrayList<>();

        // get all the data for the fill into ViewPager.
        ArrayList<LsFullIndexData> lsFullIndexDataArray = new GetDataFromDatabase(ReadingActivity.this, lsId).GetMainDataFromDB();

        // get library index data
        combineIndexDataArray = new ArrayList<>();
        if (lsFullIndexDataArray != null) {

            for (int i = 0; i < lsFullIndexDataArray.size(); i++) {
                for (int j = 0; j < lsFullIndexDataArray.get(i).getTitleIndexDatas().size(); j++) {

                    LsMainIndexData mainIndexData = new LsMainIndexData();
                    mainIndexData.setId(lsFullIndexDataArray.get(i).getTitleIndexDatas().get(j).getId());
                    mainIndexData.setNav_section_id(lsFullIndexDataArray.get(i).getTitleIndexDatas().get(j).getNav_section_id());
                    mainIndexData.setPosition(lsFullIndexDataArray.get(i).getTitleIndexDatas().get(j).getPosition());
                    mainIndexData.setImage_renditions(lsFullIndexDataArray.get(i).getTitleIndexDatas().get(j).getImage_rendition());
                    mainIndexData.setTitle_html(lsFullIndexDataArray.get(i).getTitleIndexDatas().get(j).getTitle_html());
                    mainIndexData.setSubtitle(lsFullIndexDataArray.get(i).getTitleIndexDatas().get(j).getSubtitle());
                    mainIndexData.setUri(lsFullIndexDataArray.get(i).getTitleIndexDatas().get(j).getUri());
                    mainIndexData.setLsSubIndexDatas(null);

                    combineIndexDataArray.add(mainIndexData);
                }
                combineIndexDataArray.addAll(lsFullIndexDataArray.get(i).getItemMainIndexArray());
            }
        }

        final ArrayList<LsIndexPreviewData> indexArray = new ArrayList<>();
        for (int i = 0; i < combineIndexDataArray.size(); i++) {
            if (combineIndexDataArray.get(i).getLsSubIndexDatas() != null) {
                arrayList.add(combineIndexDataArray.get(i));
            }
        }
        setFirstLevel(arrayList, rvFirstLevel);

        if (Global.selectedLibCollection != null)
            txtLibColl.setText(Global.selectedLibCollection);

        if (Global.selectedLibSelection != null)
            txtLibSel.setText(Global.selectedLibSelection);

        txtLibSel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ReadingActivity.this, LibraryIndexActivity.class);
                intent.putExtra(getString(R.string.lcID), lcId);
                intent.putExtra(getString(R.string.lsTitle), Global.selectedLibSelection);
                intent.putExtra(getString(R.string.lsId), lsId);
                startActivityWithAnimation(intent);
            }
        });

        txtLibColl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ReadingActivity.this, LibrarySelectionTabsActivity.class);
                intent.putExtra(getString(R.string.lcID), lcId);
                intent.putExtra(getString(R.string.lcname), Global.selectedLibCollection);
                startActivityWithAnimation(intent);
            }
        });

        linLib.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivityWithAnimation(new Intent(ReadingActivity.this, MainActivity.class));
            }
        });
    }

    public void setFirstLevel(ArrayList<LsMainIndexData> arrayList, RecyclerView rvFirstLevel) {
        if (arrayList != null && arrayList.size() > 0) {
            rvFirstLevel.setLayoutManager(new LinearLayoutManager(ReadingActivity.this, LinearLayoutManager.HORIZONTAL, false));
            rvFirstLevel.setAdapter(new ReadingActivityFirstLevelMenuAdapter(ReadingActivity.this, arrayList, lsId));

            int scrollToPos = 0;
            int getID = 0;

            for (int i = 0; i < arrayList.size(); i++) {
                if (title.contains(arrayList.get(i).getTitle_html())) {
                    scrollToPos = i;
                    getID = arrayList.get(i).getId();
                }
            }

            rvFirstLevel.scrollToPosition(scrollToPos);
            ArrayList<LsIndexPreviewData> subArrayList = new GetDataFromDatabase(ReadingActivity.this, lsId).GetIndexPreviewDataFromDB(getID);
            setSecondLevel(subArrayList);
        }
    }

    public void setSecondLevel(ArrayList<LsIndexPreviewData> subArrayList) {
        if (subArrayList != null && subArrayList.size() > 0) {
            rvSecondLevel.setLayoutManager(new LinearLayoutManager(ReadingActivity.this, LinearLayoutManager.HORIZONTAL, false));
            readingActivitySecondLevelMenuAdapter = new ReadingActivitySecondLevelMenuAdapter(ReadingActivity.this, subArrayList, lcId, lsId);
            rvSecondLevel.setAdapter(readingActivitySecondLevelMenuAdapter);

            int scrollToPos = 0;
            for (int i = 0; i < subArrayList.size(); i++) {
                if (title.equals(subArrayList.get(i).getHtml_title())) {
                    scrollToPos = i;
                }
            }

            rvSecondLevel.scrollToPosition(scrollToPos);
            readingActivitySecondLevelMenuAdapter.notifyDataSetChanged();
        }
    }

    public void fetchPlanData(int readingMinute) {
        Cursor cursor;
        cursor = commonDatabaseHelper.getPlanList();
        if (cursor != null && cursor.moveToFirst()) {
            while (!cursor.isAfterLast()) {

                String plan_Main_LCID1 = cursor.getString(cursor.getColumnIndex(CommonDatabaseHelper.KEY_PLAN_MAIN_LCID));
                String plan_Sub_LSID1 = cursor.getString(cursor.getColumnIndex(CommonDatabaseHelper.KEY_PLAN_SUB_LSID));
                String plan_id = cursor.getString(cursor.getColumnIndex(CommonDatabaseHelper.KEY_PLAN_ID));

                if (plan_Main_LCID1.equals(lcId) && plan_Sub_LSID1.equals(lsId)) {
                    ContentValues contentValues = new ContentValues();
                    contentValues.put(CommonDatabaseHelper.KEY_PLAN_READING_MIN, readingMinute);
                    commonDatabaseHelper.updateRowData(CommonDatabaseHelper.PLAN_MASTER, contentValues, commonDatabaseHelper.KEY_PLAN_ID + " = ?",
                            new String[]{String.valueOf(plan_id)});

                }
                cursor.moveToNext();
            }
        }
    }

    public static ReadingActivity getInstance() {
        return readingActivity;
    }

    public void calculateAudioMin(int tempAudioMin) {
        int audioMinute = 0;
        int audioPoint = 0;

        audioMinute = sessionManager.getKeyUserMinAudio();
        audioPoint = sessionManager.getUserMinAudioPoints();

        if (audioMinute != 0) {
            audioMinute = audioMinute + tempAudioMin;
        } else {
            audioMinute = tempAudioMin;
        }

        if (audioPoint != 0) {
            audioPoint = audioPoint + (tempAudioMin * 60);
        } else {
            audioPoint = tempAudioMin * 60;
        }

        Global.printLog("AUDIODATA", "======audioMinute====0000===" + audioMinute +
                "======audioPoint=====0000===" + audioPoint +
                "======tempAudioMin=====0000===" + tempAudioMin);

        sessionManager.setUserMinAudio(audioMinute);
        sessionManager.setKeyUserMinAudioPoint(audioPoint);
    }

    public class AsyncDownloadAudio extends AsyncTask<String, String, String> {

        private Context context;
        private String filepath;
        private String mediaUrl;
        private String AppFolderPath;

        public AsyncDownloadAudio(Context context, String mediaUrl) {
            this.context = context;
            this.mediaUrl = mediaUrl;
        }

        @Override
        protected void onPreExecute() {
            //showprogress();
        }

        @Override
        protected String doInBackground(String... URL) {
            try {
                java.net.URL url = new URL(mediaUrl);
                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setDoInput(true);
                conn.connect();
                AppFolderPath = Environment.getExternalStorageDirectory() + "/LDS/";
                String destPath = AppFolderPath + "readingAudio.mp3";
                File file = new File(destPath);
                try {
                    if (file.createNewFile()) {
                        file.createNewFile();
                    }
                } catch (Exception e) {

                }

                FileOutputStream fileOutput = new FileOutputStream(file);
                InputStream inputStream = conn.getInputStream();
                int totalSize = conn.getContentLength();
                int downloadedSize = 0;
                byte[] buffer = new byte[1024];
                int bufferLength;
                while ((bufferLength = inputStream.read(buffer)) > 0) {
                    fileOutput.write(buffer, 0, bufferLength);
                    downloadedSize += bufferLength;
                }
                fileOutput.close();
                if (downloadedSize == totalSize) filepath = file.getPath();
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                filepath = null;
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String result) {
            if (Global.audioFlag == 1) {
                viewpager.setCurrentItem(viewpager.getCurrentItem() + 1);
                viewPagerIndexAdapter.notifyDataSetChanged();
                Global.audioFlag = 0;
                showText(txtDuration, "0:00");
                showHideLayout();
                mangeFab();
                //dismissprogress();
            } else {
                //dismissprogress();
            }
        }
    }

}
