package com.ldsscriptures.pro.activity.menuscreens;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.ldsscriptures.pro.R;
import com.ldsscriptures.pro.activity.BaseActivity;
import com.ldsscriptures.pro.activity.settingmenu.SettingsActivity;
import com.ldsscriptures.pro.adapter.MenuAdapter;
import com.ldsscriptures.pro.model.MenuData;
import com.ldsscriptures.pro.model.SubMenu;
import com.ldsscriptures.pro.utils.Global;
import com.ldsscriptures.pro.utils.SessionManager;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;


public class SlideMenuActivity extends BaseActivity {

    @BindView(R.id.listMenu)
    ExpandableListView listMenu;
    @BindView(R.id.ivClose)
    ImageView ivClose;
    @BindView(R.id.tvUserName)
    TextView tvUserName;
    @BindView(R.id.imageView_background)
    ImageView imageView_background;
    @BindView(R.id.ivSetting)
    ImageView ivSetting;
    @BindView(R.id.ivProfileImage)
    CircleImageView ivProfileImage;
    @BindView(R.id.progress_user_image)
    ProgressBar progressUserImage;
    @BindView(R.id.ivProfileImage_layout)
    FrameLayout ivProfileImageLayout;

    private ExpandableListAdapter menuAdapter;
    private ArrayList<MenuData> menuArray = new ArrayList<MenuData>();
    private int previousGroup;
    private SessionManager sessionManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_slide_menu);
        ButterKnife.bind(this);
        setContents();
    }

    private void setContents() {
        ButterKnife.bind(this);
        sessionManager = new SessionManager(SlideMenuActivity.this);
        Global.SetBackground(SlideMenuActivity.this, imageView_background, R.drawable.bg_splash_blur);

        setMenu();
        previousGroup = new Global(SlideMenuActivity.this).getPrefrenceInt(SlideMenuActivity.this, "groupPosition", 0);

        listMenu.expandGroup(previousGroup);

        listMenu.setOnGroupExpandListener(new ExpandableListView.OnGroupExpandListener() {

            @Override
            public void onGroupExpand(int groupPosition) {
                if ((previousGroup != -1) && (groupPosition != previousGroup)) {
                    listMenu.collapseGroup(previousGroup);
                    Global.setPrefrenceInt(SlideMenuActivity.this, "groupPosition", groupPosition);
                }
                previousGroup = groupPosition;
            }
        });
        showText(tvUserName, getUserInfo(SessionManager.KEY_USER_USERNAME));

        String imageUrl = getUserInfo(SessionManager.KEY_USR_IMG_URL);


        if (imageUrl != null) {
            Glide.with(getBaseContext())
                    .load(getUserInfo(SessionManager.KEY_USR_IMG_URL))
                    .listener(new RequestListener<String, GlideDrawable>() {
                        @Override
                        public boolean onException(Exception e, String model, Target<GlideDrawable> target, boolean isFirstResource) {
                            progressUserImage.setVisibility(View.GONE);
                            return false;
                        }

                        @Override
                        public boolean onResourceReady(GlideDrawable resource, String model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                            progressUserImage.setVisibility(View.GONE);
                                return false;
                        }
                    })
                    .centerCrop()
                    .fitCenter()
                    .into(ivProfileImage);
        } else {
            progressUserImage.setVisibility(View.GONE);
            ivProfileImage.setImageResource(R.drawable.profile_lg);
        }
    }

    @OnClick(R.id.ivClose)
    public void closeThis() {
        finish();
    }

    @OnClick(R.id.ivSetting)
    public void openSettings() {
        startActivity(new Intent(SlideMenuActivity.this, SettingsActivity.class));
        overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
    }

    private void setMenu() {
        List<SubMenu> reading = new ArrayList<SubMenu>();
        reading.add(new SubMenu(R.drawable.reading, getString(R.string.txt_readingNow)));
        reading.add(new SubMenu(R.drawable.library1, getString(R.string.txt_library)));
        reading.add(new SubMenu(R.drawable.history, getString(R.string.txt_history)));
        reading.add(new SubMenu(R.drawable.bookmarks, getString(R.string.txt_bookmarks)));
        MenuData Reading = new MenuData(getString(R.string.txt_reading), reading);

        List<SubMenu> content = new ArrayList<SubMenu>();
        content.add(new SubMenu(R.drawable.hymns, getString(R.string.txt_hymns)));
        MenuData Content = new MenuData(getString(R.string.txt_content), content);

        List<SubMenu> studytools = new ArrayList<SubMenu>();
        studytools.add(new SubMenu(R.drawable.lessons, getString(R.string.lessons)));
        studytools.add(new SubMenu(R.drawable.daily, getString(R.string.txt_daily)));
        studytools.add(new SubMenu(R.drawable.random, getString(R.string.txt_random)));
        studytools.add(new SubMenu(R.drawable.journal, getString(R.string.txt_journal)));
        MenuData Studytools = new MenuData(getString(R.string.txt_studyTools), studytools);

        List<SubMenu> social = new ArrayList<SubMenu>();
        social.add(new SubMenu(R.drawable.timeline, getString(R.string.txt_timeline)));
        social.add(new SubMenu(R.drawable.friends1, getString(R.string.txt_following)));
        social.add(new SubMenu(R.drawable.leaderboard, getString(R.string.txt_leaderboard)));
        MenuData Social = new MenuData(getString(R.string.txt_social), social);

        List<SubMenu> activity = new ArrayList<SubMenu>();
        activity.add(new SubMenu(R.drawable.activity, getString(R.string.txt_activity)));
        activity.add(new SubMenu(R.drawable.achievements, getString(R.string.txt_achievements)));
        MenuData Activity = new MenuData(getString(R.string.txt_activity), activity);

        List<SubMenu> data = new ArrayList<SubMenu>();
        data.add(new SubMenu(R.drawable.notes_sand, getString(R.string.txt_notes)));
        data.add(new SubMenu(R.drawable.tags, getString(R.string.txt_tags)));
        MenuData Data = new MenuData(getString(R.string.txt_data), data);

        menuArray.add(Reading);
        menuArray.add(Content);
        menuArray.add(Studytools);
        menuArray.add(Social);
        menuArray.add(Activity);
        menuArray.add(Data);

        menuAdapter = new MenuAdapter(this, menuArray);
        listMenu.setAdapter(menuAdapter);
    }
}
