package com.ldsscriptures.pro.activity.studytools;

import android.content.ContentValues;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.ldsscriptures.pro.R;
import com.ldsscriptures.pro.activity.BaseActivity;
import com.ldsscriptures.pro.database.CommonDatabaseHelper;
import com.ldsscriptures.pro.utils.Global;
import com.ldsscriptures.pro.utils.PushToServerData;
import com.ldsscriptures.pro.utils.SessionManager;
import com.ldsscriptures.pro.utils.Validator;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class AddJournalsActivity extends BaseActivity {
    @BindView(R.id.ivBack)
    ImageView ivBack;
    @BindView(R.id.tvTitle)
    TextView tvTitle;
    @BindView(R.id.ivMore)
    ImageView ivMore;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.edTitle)
    EditText edTitle;
    @BindView(R.id.edDetail)
    EditText edDetail;
    @BindView(R.id.tvDone)
    TextView tvDone;
    @BindView(R.id.ivMenu)
    ImageView ivMenu;
    @BindView(R.id.ivCheck)
    ImageView ivCheck;
    @BindView(R.id.ivEdit)
    ImageView ivEdit;
    @BindView(R.id.linTitle)
    LinearLayout linTitle;
    private CommonDatabaseHelper commonDatabaseHelper;
    SessionManager sessionManager;
    String Journaltitle = "";
    String Journaltext = "";
    int Journalid = 0;
    private boolean isEdit = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_journals);
        ButterKnife.bind(this);
        setContent();
    }

    private void setContent() {
        ButterKnife.bind(this);

        setToolbar(toolbar);
        setVisibilityGone(ivMore);
        sessionManager = new SessionManager(this);
        commonDatabaseHelper = new CommonDatabaseHelper(AddJournalsActivity.this);


        Intent intent = getIntent();
        Bundle extras = intent.getExtras();
        if (extras != null) {
            if (extras.containsKey("Journaltitle")) {
                Journaltitle = getIntent().getStringExtra("Journaltitle");
            }
            if (extras.containsKey("Journaltext")) {
                Journaltext = getIntent().getStringExtra("Journaltext");
            }
            if (extras.containsKey("Journalid")) {
                Journalid = getIntent().getIntExtra("Journalid", 0);
            }
            if (Journalid != 0 && Journaltitle != null && Journaltext != null) {
                showText(tvTitle, getString(R.string.edit_journal));
                edTitle.setText(Journaltitle);
                edDetail.setText(Journaltext);
                isEdit = true;
            }
        } else {
            showText(tvTitle, getString(R.string.new_journal));
        }

    }

    private boolean validateFields() {
        if (!Validator.checkEmpty(edTitle)) {
            Global.showSnackBar(AddJournalsActivity.this, getString(R.string.enter_journal_title));
            return false;
        }

        if (!Validator.checkEmpty(edDetail)) {
            Global.showSnackBar(AddJournalsActivity.this, getString(R.string.enter_journal_text));
            return false;
        }

        return true;
    }

    @OnClick({R.id.ivBack, R.id.tvDone})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.ivBack:
                finish();
                break;
            case R.id.tvDone:
                if (validateFields()) {

                    if (isEdit) {

                        ContentValues contentValues = new ContentValues();
                        contentValues.put(CommonDatabaseHelper.KEY_JOURNALTITLE, edTitle.getText().toString());
                        contentValues.put(CommonDatabaseHelper.KEY_JOURNALTEXT, edDetail.getText().toString());
                        contentValues.put(CommonDatabaseHelper.KEY_GROUPINGDATE, Global.getCurrentDate());
                        contentValues.put("modified", "1");

                        commonDatabaseHelper.updateRowData(CommonDatabaseHelper.TABLE_JOURNAL, contentValues,
                                CommonDatabaseHelper.KEY_ID + " = ?",
                                new String[]{String.valueOf(Journalid)});

                    } else {
                        ContentValues contentValues = new ContentValues();
                        contentValues.put(CommonDatabaseHelper.KEY_JOURNALTITLE, edTitle.getText().toString());
                        contentValues.put(CommonDatabaseHelper.KEY_JOURNALTEXT, edDetail.getText().toString());
                        contentValues.put(CommonDatabaseHelper.KEY_GROUPINGDATE, Global.getCurrentDate());
                        contentValues.put(CommonDatabaseHelper.KEY_SERVERID, "0");
                        contentValues.put("deleted", "");
                        contentValues.put("modified", "");
                        commonDatabaseHelper.insertData(CommonDatabaseHelper.TABLE_JOURNAL, contentValues);

                    }

                    new PushToServerData(this);
                    if (JournalsActivity.getInstance() != null) {
                        JournalsActivity.getInstance().setData();
                    }
                    finish();
                }

                break;
        }
    }
}
