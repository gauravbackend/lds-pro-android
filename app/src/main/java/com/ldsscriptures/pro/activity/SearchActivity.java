package com.ldsscriptures.pro.activity;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.text.SpannableStringBuilder;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.ldsscriptures.pro.R;
import com.ldsscriptures.pro.adapter.SearchCategoryAdapter;
import com.ldsscriptures.pro.database.CommonDatabaseHelper;
import com.ldsscriptures.pro.dbClass.GetLibCollectionFromDB;
import com.ldsscriptures.pro.model.LcData;
import com.ldsscriptures.pro.model.SerachData;
import com.ldsscriptures.pro.utils.DataHolder;
import com.ldsscriptures.pro.utils.RetriveMainDatabse;
import com.ldsscriptures.pro.utils.SessionManager;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class SearchActivity extends BaseActivity {

    @BindView(R.id.ivMore)
    ImageView ivMore;
    @BindView(R.id.searchView)
    SearchView searchView1;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.textsubtitleserach)
    TextView textsubtitleserach;
    @BindView(R.id.layout_toolbar_serach1)
    LinearLayout layoutToolbarSerach1;
    @BindView(R.id.fab)
    FloatingActionButton fab;
    @BindView(R.id.search_data_text)
    TextView searchDataText;
    @BindView(R.id.Search_recyclerView)
    RecyclerView SearchRecyclerView;
    @BindView(R.id.ivBack)
    ImageView ivBack;
    @BindView(R.id.txt_search_more_data)
    TextView txtSearchMoreData;
    SessionManager sessionManager;
    CommonDatabaseHelper commonDatabaseHelper;
    private EditText searchEditText;
    String searchQuery = "";
    SearchCategoryAdapter searchCategoryAdapter;
    private ArrayList<LcData> lcDataArrayList = new ArrayList<>();
    private ArrayList<SerachData> lcDataArrayListString = new ArrayList<>();
    private ArrayList<SerachData> categoryArrayList = new ArrayList<>();
    private ArrayList<SerachData> categoryMainArrayList = new ArrayList<>();
    static SearchActivity searchActivity;

    static {
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);
        ButterKnife.bind(this);
        setContents();

    }

    private void setContents() {
        sessionManager = new SessionManager(this);
        commonDatabaseHelper = new CommonDatabaseHelper(this);
        ButterKnife.bind(this);
        setToolbar(toolbar);
        searchActivity = this;
        SearchRecyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        SearchRecyclerView.setLayoutManager(layoutManager);
        searchView1 = customizeWhiteSearchView(searchView1, SearchActivity.this, "Search");
        searchView1.setIconified(false);
        searchView1.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                if (!query.trim().isEmpty()) {
                    sessionManager.setStartLimit(0);
                    searchQuery = query;
                    lcDataArrayList.clear();
                    lcDataArrayListString.clear();
                    categoryArrayList.clear();
                    searchView1.clearFocus();
                    new searchQueryFirstTime().execute(query);
                } else {
                    Toast.makeText(getApplicationContext(), "Please Enter a keyword", Toast.LENGTH_LONG).show();
                }
                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                searchQuery = newText.trim();
                setVisibilityGone(txtSearchMoreData);
                sessionManager.setStartLimit(0);
                return true;
            }
        });
    }

    public SearchView customizeWhiteSearchView(SearchView sv, Context context, String hintText) {

        searchEditText = (EditText) sv.findViewById(android.support.v7.appcompat.R.id.search_src_text);
        searchEditText.setTextColor(context.getResources().getColor(R.color.textColor));
        searchEditText.setPadding(0, 2, 0, 2);
        searchEditText.setHintTextColor(context.getResources().getColor(R.color.gray_light));
        searchEditText.setTextSize(20);

        ImageView imgViewSearchView = (ImageView) sv.findViewById(android.support.v7.appcompat.R.id.search_button);
        imgViewSearchView.setVisibility(View.VISIBLE);

        SpannableStringBuilder ssb = new SpannableStringBuilder("   ");
        if (hintText != null) {
            ssb.append(hintText);
        }
        searchEditText.setHint(ssb);

        return sv;
    }

    @OnClick(R.id.fab)
    public void manageFloatingButton() {
        manageFloatingBtn();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menuBack:
                finish();
                //Toast.makeText(getApplicationContext(), "No Item Found", Toast.LENGTH_LONG).show();
                break;
            case R.id.menuNext:
                Toast.makeText(getApplicationContext(), "No Item Found", Toast.LENGTH_LONG).show();
                break;
            case R.id.menuBookmark:
                Intent bookmarkIntent = new Intent(getApplicationContext(), MainActivity.class);
                bookmarkIntent.putExtra(getString(R.string.menuSelection), "Bookmarks");
                bookmarkIntent.putExtra(getString(R.string.isFromReading), true);
                startActivityWithAnimation(bookmarkIntent);
                break;
            case R.id.menuHistory:
                Intent historyIntent = new Intent(getApplicationContext(), MainActivity.class);
                historyIntent.putExtra(getString(R.string.menuSelection), "History");
                historyIntent.putExtra(getString(R.string.isFromReading), true);
                startActivityWithAnimation(historyIntent);
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @OnClick({R.id.ivBack, R.id.txt_search_more_data})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.ivBack:
                finish();
                break;
            case R.id.txt_search_more_data:
                if (!searchQuery.trim().isEmpty()) {
                    int start_limit = sessionManager.getStartLimit();
                    start_limit = start_limit + 1;
                    sessionManager.setStartLimit(start_limit);
                    searchView1.clearFocus();
                    new searchQueryFirstTime().execute(searchQuery);
                } else {
                    Toast.makeText(getApplicationContext(), "Please Enter a keyword", Toast.LENGTH_LONG).show();
                }
                break;
        }
    }

    private class searchQueryFirstTime extends AsyncTask<String, Void, String> {
        String serachQueryNew = "";

        @Override
        protected void onPreExecute() {
            showprogress();
        }

        @Override
        protected String doInBackground(final String... params) {

            categoryMainArrayList.clear();
            lcDataArrayListString.clear();
            categoryArrayList.clear();

            serachQueryNew = params[0];
            if (databasehelper.checkDataBase()) {
                lcDataArrayList.addAll(new GetLibCollectionFromDB(SearchActivity.this).getSerachLcArrayListFromDB(sessionManager.getStartLimit()));
                if (lcDataArrayList != null && lcDataArrayList.size() != 0) {
                    for (int i = 0; i < lcDataArrayList.size(); i++) {
                        for (int i1 = 0; i1 < lcDataArrayList.get(i).getLsDatas().size(); i1++) {
                            if (lcDataArrayList.get(i).getLsDatas().get(i1).getCombineIndexDataArray() != null) {
                                for (int i2 = 0; i2 < lcDataArrayList.get(i).getLsDatas().get(i1).getCombineIndexDataArray().size(); i2++) {
                                    for (int i3 = 0; i3 < lcDataArrayList.get(i).getLsDatas().get(i1).getCombineIndexDataArray().get(i2).getVerseTextArraylist().size(); i3++) {

                                        String stringSentense = lcDataArrayList.get(i).getLsDatas().get(i1).getCombineIndexDataArray().get(i2).getVerseTextArraylist().get(i3).getStringSentense();
                                        String stringlsTitle = lcDataArrayList.get(i).getLsDatas().get(i1).getTitle();
                                        String stringSubTitle = lcDataArrayList.get(i).getLsDatas().get(i1).getCombineIndexDataArray().get(i2).getTitle();
                                        String stringID = lcDataArrayList.get(i).getLsDatas().get(i1).getCombineIndexDataArray().get(i2).getVerseTextArraylist().get(i3).getStringId();
                                        String stringURL = lcDataArrayList.get(i).getLsDatas().get(i1).getCombineIndexDataArray().get(i2).getUri();
                                        String stringlcTitle = lcDataArrayList.get(i).getTitle();

                                        SerachData serachData = new SerachData();
                                        serachData.setStringSentense(stringSentense);
                                        serachData.setStringlsTitle(stringlsTitle);
                                        serachData.setStringSubTitle(stringSubTitle);
                                        serachData.setStringId(stringID);
                                        serachData.setSetData_URL(stringURL);
                                        serachData.setStringlcTitle(stringlcTitle);

                                        lcDataArrayListString.add(serachData);
                                        categoryArrayList.add(serachData);
                                        categoryMainArrayList.add(serachData);

                                        removeDuplicates(categoryArrayList);
                                        removeLCdataDuplicates(categoryMainArrayList);
                                    }
                                }
                            }
                        }
                    }

                    DataHolder.setData(categoryArrayList);
                    DataHolder.setData1(lcDataArrayListString);
                }
            } else {
                new RetriveMainDatabse(SearchActivity.this);
            }
            return "";
        }

        @Override
        protected void onPostExecute(String result) {

            if (lcDataArrayListString != null && lcDataArrayListString.size() > 0) {
                searchCategoryAdapter = new SearchCategoryAdapter(SearchActivity.this, categoryArrayList, serachQueryNew);
                SearchRecyclerView.setAdapter(searchCategoryAdapter);
                searchCategoryAdapter.filter(serachQueryNew);
                setVisibilityVisible(txtSearchMoreData);
                dismissprogress();
            }
        }
    }

    public void HideShow() {
        SearchRecyclerView.setVisibility(View.VISIBLE);
        searchDataText.setVisibility(View.GONE);
    }

    public void ShowHide() {
        searchDataText.setVisibility(View.VISIBLE);
        SearchRecyclerView.setVisibility(View.GONE);
        searchDataText.setText(R.string.no_data_found);
    }

    private void removeDuplicates(ArrayList<SerachData> categoryArrayList) {
        int count = categoryArrayList.size();

        for (int i = 0; i < count; i++) {
            for (int j = i + 1; j < count; j++) {
                if (categoryArrayList.get(i).getStringlsTitle().equals(categoryArrayList.get(j).getStringlsTitle())) {
                    categoryArrayList.remove(j--);
                    count--;
                }
            }
        }
    }

    private void removeLCdataDuplicates(ArrayList<SerachData> categoryMainArrayList) {
        int count = categoryMainArrayList.size();

        for (int i = 0; i < count; i++) {
            for (int j = i + 1; j < count; j++) {
                if (categoryMainArrayList.get(i).getStringlcTitle().equals(categoryMainArrayList.get(j).getStringlcTitle())) {
                    categoryMainArrayList.remove(j--);
                    count--;
                }
            }
        }
    }

    public static SearchActivity getInstance() {
        return searchActivity;
    }
}