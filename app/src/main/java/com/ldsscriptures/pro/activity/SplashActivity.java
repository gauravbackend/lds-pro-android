package com.ldsscriptures.pro.activity;

import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.RequiresApi;
import android.widget.ImageView;

import com.ldsscriptures.pro.R;
import com.ldsscriptures.pro.utils.Global;
import com.ldsscriptures.pro.utils.SessionManager;

import butterknife.BindView;
import butterknife.ButterKnife;
import permission.auron.com.marshmallowpermissionhelper.PermissionResult;
import permission.auron.com.marshmallowpermissionhelper.PermissionUtils;

public class SplashActivity extends BaseActivity {

    private static final int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;

    @BindView(R.id.imageView_background)
    ImageView imageView_background;

    private SessionManager sessionManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        ButterKnife.bind(this);
        sessionManager = new SessionManager(SplashActivity.this);
        Global.LANGUAGE = sessionManager.getLanguageCode();
        sessionManager = new SessionManager(SplashActivity.this);
        askCompactPermissions(new String[]{PermissionUtils.Manifest_CAMERA,
                PermissionUtils.Manifest_READ_EXTERNAL_STORAGE,
                PermissionUtils.Manifest_WRITE_EXTERNAL_STORAGE,
                PermissionUtils.Manifest_READ_CONTACTS}, new PermissionResult() {
            @Override
            public void permissionGranted() {
                new Handler().postDelayed(new Runnable() {
                    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
                    @Override
                    public void run() {
                        sessionManager.checkLogin();
                    }
                }, 2500);
            }

            @Override
            public void permissionDenied() {
            }

            @Override
            public void permissionForeverDenied() {
            }
        });
    }
}
