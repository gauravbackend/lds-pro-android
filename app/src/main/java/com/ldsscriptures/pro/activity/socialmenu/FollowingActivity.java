package com.ldsscriptures.pro.activity.socialmenu;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.ldsscriptures.pro.R;
import com.ldsscriptures.pro.activity.BaseActivity;
import com.ldsscriptures.pro.activity.MainActivity;
import com.ldsscriptures.pro.adapter.FollowingListAdapter;
import com.ldsscriptures.pro.model.FollowersMainData;
import com.ldsscriptures.pro.model.FollowingMainModel;
import com.ldsscriptures.pro.retroutils.APIClient;
import com.ldsscriptures.pro.retroutils.APIInterface;
import com.ldsscriptures.pro.utils.SessionManager;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FollowingActivity extends BaseActivity {

    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    @BindView(R.id.ivMenu)
    ImageView ivMenu;
    @BindView(R.id.ivBack)
    ImageView ivBack;
    @BindView(R.id.tvTitle)
    TextView tvTitle;
    @BindView(R.id.ivSearch)
    ImageView ivSearch;
    @BindView(R.id.ivMore)
    ImageView ivMore;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.followingfab)
    FloatingActionButton followingfab;
    @BindView(R.id.ivTabs)
    ImageView ivTabs;
    @BindView(R.id.tvNoData)
    TextView tvNoData;

    ArrayList<FollowersMainData> followingarraylist = new ArrayList<>();
    FollowingListAdapter mAdapter;
    String paraUserId, paraSessionId;
    static FollowingActivity followingActivity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_following_main);
        ButterKnife.bind(this);
        setContents();
    }

    public void setContents() {
        followingActivity = this;
        showText(tvTitle, getString(R.string.txt_following));

        setToolbar(toolbar);
        setVisibilityGone(ivMore);
        setVisibilityGone(ivTabs);
        setVisibilityGone(ivSearch);

        recyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);

        if (getUserInfo(SessionManager.KEY_USER_ID) != null) {
            paraUserId = getUserInfo(SessionManager.KEY_USER_ID);
        }
        if (getUserInfo(SessionManager.KEY_USER_SESSION_ID) != null) {
            paraSessionId = getUserInfo(SessionManager.KEY_USER_SESSION_ID);
        }

        getFollowingFriendAPI(paraUserId, paraSessionId);
    }

    private void getFollowingFriendAPI(String paraUserId, String paraSessionId) {
        followingarraylist.clear();
        showprogress();

        APIInterface apiService = APIClient.getClient().create(APIInterface.class);
        String url = APIClient.BASE_URL;

        url += "listUsers.pl?user_id=" + paraUserId +
                "&session_id=" + paraSessionId +
                "&apiauth=" + APIClient.AUTH_KEY +
                "&followed_by=" + "1";


        Call<FollowingMainModel<FollowersMainData>> callObject = apiService.getFollowingData(url.replaceAll(" ", "%20"));
        callObject.enqueue(new Callback<FollowingMainModel<FollowersMainData>>() {

            @Override
            public void onResponse(Call<FollowingMainModel<FollowersMainData>> call, Response<FollowingMainModel<FollowersMainData>> response) {

                if (response.body() != null && !response.body().toString().trim().isEmpty()) {

                    if (response.body().getSuccess().equals("true")) {
                        followingarraylist = response.body().getFollowers();
                        if (followingarraylist.size() != 0) {
                            recyclerView.setVisibility(View.VISIBLE);
                            tvNoData.setVisibility(View.GONE);

                            mAdapter = new FollowingListAdapter(FollowingActivity.this, followingarraylist, 0, 0);
                            recyclerView.setAdapter(mAdapter);
                        } else {
                            recyclerView.setVisibility(View.GONE);
                            tvNoData.setVisibility(View.VISIBLE);
                            tvNoData.setText(getResources().getString(R.string.press_to_connect_with_friend));
                        }

                    }
                    dismissprogress();

                }

            }

            @Override
            public void onFailure(Call<FollowingMainModel<FollowersMainData>> call, Throwable t) {
                dismissprogress();
            }
        });
    }

    public static FollowingActivity getInstance() {
        return followingActivity;
    }

    @OnClick(R.id.ivMenu)
    public void openMenu() {
        openSlideMenu();
    }

    @OnClick(R.id.followingfab)
    public void onViewClicked() {
        startActivity(new Intent(getApplicationContext(), FollowingFindFriendActivity.class));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menuBack:
                finish();
                break;
            case R.id.menuNext:
                Toast.makeText(getApplicationContext(), "No Item Found", Toast.LENGTH_LONG).show();
                break;
            case R.id.menuBookmark:
                Intent bookmarkIntent = new Intent(getApplicationContext(), MainActivity.class);
                bookmarkIntent.putExtra(getString(R.string.menuSelection), "Bookmarks");
                bookmarkIntent.putExtra(getString(R.string.isFromReading), true);
                startActivityWithAnimation(bookmarkIntent);
                break;
            case R.id.menuHistory:
                Intent historyIntent = new Intent(getApplicationContext(), MainActivity.class);
                historyIntent.putExtra(getString(R.string.menuSelection), "History");
                historyIntent.putExtra(getString(R.string.isFromReading), true);
                startActivityWithAnimation(historyIntent);
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}
