package com.ldsscriptures.pro.activity;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.Toast;

import com.ldsscriptures.pro.R;
import com.ldsscriptures.pro.adapter.IndexGridAdapter;
import com.ldsscriptures.pro.adapter.SubIndexAdapter;
import com.ldsscriptures.pro.adapter.SubIndexFirstLevelMenuAdapter;
import com.ldsscriptures.pro.adapter.SubIndexSecondLevelMenuAdapter;
import com.ldsscriptures.pro.dbClass.GetDataFromDatabase;
import com.ldsscriptures.pro.dbClass.GetLibCollectionFromDB;
import com.ldsscriptures.pro.model.LsIndexPreviewData;
import com.ldsscriptures.pro.model.LsMainIndexData;
import com.ldsscriptures.pro.model.MoreOptionMenuData;
import com.ldsscriptures.pro.model.ReadingNow.LibSubIndex;
import com.ldsscriptures.pro.model.ReadingNowData;
import com.ldsscriptures.pro.utils.Global;
import com.ldsscriptures.pro.utils.GridViewScrollable;
import com.ldsscriptures.pro.utils.SessionManager;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnItemClick;

import static com.ldsscriptures.pro.model.ReadingNowData.readingNowData;

public class SubIndexActivity extends BaseActivity {
    public static ArrayList<LsIndexPreviewData> newIndexArray = new ArrayList<>();
    @BindView(R.id.gridView)
    GridViewScrollable gridView;

    @BindView(R.id.ivMenu)
    ImageView ivMenu;

    @BindView(R.id.ivBack)
    ImageView ivBack;

    @BindView(R.id.ivTabs)
    ImageView ivTabs;

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.ivDropDown)
    ImageView ivDropDown;

    @BindView(R.id.linDropDown)
    LinearLayout linDropDown;

    @BindView(R.id.tvTitle)
    TextView tvTitle;

    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;

    @BindView(R.id.ivMore)
    ImageView ivMore;

    PopupWindow pw;

    private int iPosition;
    private ArrayList<LsIndexPreviewData> indexArray = new ArrayList<>();
    private SessionManager sessionManager;
    private String title;
    private String indexId;

    static SubIndexActivity subIndexActivity;
    private String lcId;
    private String lsId;

    private RecyclerView rvSecondLevel;
    private SubIndexSecondLevelMenuAdapter subIndexSecondLevelMenuAdapter;

    public static SubIndexActivity getInstance() {
        return subIndexActivity;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sub_index);
        setContents();
    }

    private void setContents() {
        ButterKnife.bind(this);

        subIndexActivity = this;

        iPosition = getIntent().getIntExtra(getString(R.string.SubIndexPosition), 1);
        title = getIntent().getStringExtra(getString(R.string.indexTitle));
        lcId = getIntent().getStringExtra(getString(R.string.lcID));
        lsId = getIntent().getStringExtra(getString(R.string.lsId));
        ivDropDown.setVisibility(View.VISIBLE);
        ivMore.setVisibility(View.GONE);

        sessionManager = new SessionManager(SubIndexActivity.this);
        sessionManager.setoffset(0);
        indexArray = new GetDataFromDatabase(SubIndexActivity.this, lsId).GetIndexPreviewDataFromDB(iPosition);

        newIndexArray = indexArray;

        // set toolbar title
        setToolbar(toolbar);
        showText(tvTitle, title);

        // set menu option in toolbar
        setVisibilityVisible(ivMenu);
        setVisibilityGone(ivBack);

        ReadingNowData.tagArrayList.add(this.getClass().getName());
        if (readingNowData != null)
            readingNowData.setLibSubIndex(new LibSubIndex(this.getClass().getName(), title, iPosition, lcId, lsId));


        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(SubIndexActivity.this);
        recyclerView.setLayoutManager(mLayoutManager);

        recyclerView.setAdapter(new SubIndexAdapter(SubIndexActivity.this, indexArray, lcId, lsId));
        recyclerView.setNestedScrollingEnabled(false);

        gridView.setAdapter(new IndexGridAdapter(SubIndexActivity.this, indexArray));
        setGridViewHeightBasedOnChildren(gridView);
    }

    @OnClick(R.id.fab)
    public void manageFloatingButton() {
        manageFloatingBtn();
    }

    @OnClick(R.id.ivTabs)
    public void openTabs() {
        ArrayList<ReadingNowData> readingNowDataArrayList = sessionManager.getPreferencesArrayList(SubIndexActivity.this);
        if (readingNowDataArrayList == null) {
            readingNowDataArrayList = new ArrayList<>();
            ReadingNowData readingNowData = new ReadingNowData();
            if (Global.getPrefrenceString(SubIndexActivity.this, Global.ADD_NEW_TAB_PATH, "") != null &&
                    Global.getPrefrenceString(SubIndexActivity.this, Global.ADD_NEW_TAB_CLASS_NAME, "") != null) {
                readingNowData.setPath(Global.getPrefrenceString(SubIndexActivity.this, Global.ADD_NEW_TAB_PATH, ""));
                readingNowData.setTag(Global.getPrefrenceString(SubIndexActivity.this, Global.ADD_NEW_TAB_CLASS_NAME, ""));
            }

            readingNowData.setActivityIdentifier(getString(R.string.tabs_lib_collection));
            readingNowDataArrayList.add(0, readingNowData);
            sessionManager.setPreferencesArrayList(SubIndexActivity.this, readingNowDataArrayList);
        }

        ReadingNowData readingNowData = new ReadingNowData();
        readingNowData.setPath(Global.takeScreenshot(this, 24));
        readingNowData.setTag(this.getClass().getName());
        readingNowData.setBookmarked(readingNowDataArrayList.get(0).isBookmarked());
        readingNowData.setActivityIdentifier(getString(R.string.tabs_lib_sub_index));

        readingNowData.setLibSubIndex(new LibSubIndex(this.getClass().getName(), title, iPosition, lcId, lsId));
        readingNowDataArrayList.set(0, readingNowData);
        sessionManager.setPreferencesArrayList(SubIndexActivity.this, readingNowDataArrayList);

        startActivity(new Intent(this, TabsActivity.class));
        overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
    }

    public void setGridViewHeightBasedOnChildren(GridView gridView) {
        ListAdapter listAdapter = gridView.getAdapter();
        if (listAdapter == null) {
            // pre-condition
            return;
        }
        int totalHeight = 0;
        int items = listAdapter.getCount();

        View listItem = listAdapter.getView(0, null, gridView);
        listItem.measure(0, 0);
        totalHeight = listItem.getMeasuredHeight();

        double row = items / 7;
        if (row > 6) {
            totalHeight *= 6;
        } else {
            if (items % 7 != 0)
                totalHeight *= Math.ceil(row) + 1;
            else
                totalHeight *= Math.ceil(row);
        }

        ViewGroup.LayoutParams params = gridView.getLayoutParams();
        params.height = totalHeight;
        gridView.setLayoutParams(params);
    }

    @OnClick(R.id.ivMenu)
    public void openMenu() {
        openSlideMenu();
    }


    @OnClick(R.id.linDropDown)
    public void openDropDownMenu() {
        initiateTypePopUp(linDropDown);
    }

    @OnClick(R.id.ivSearch)
    public void openSearch() {
        startActivityWithAnimation(new Intent(SubIndexActivity.this, SearchActivity.class));
    }


    @OnItemClick(R.id.gridView)
    public void openReadingActivity(int position) {

        Global.moreOptionMenuDatas.set(3, new MoreOptionMenuData(lcId, lsId, indexArray.get(position).getId(), false, false, position));

        Intent intent = new Intent(SubIndexActivity.this, ReadingActivity.class);
        intent.putExtra(getString(R.string.indexForReading), indexArray.get(position).getId());
        intent.putExtra(getString(R.string.position), position);
        intent.putExtra(getString(R.string.isFromIndex), false);
        intent.putExtra(getString(R.string.is_history), false);
        intent.putExtra(getString(R.string.lcID), lcId);
        intent.putExtra(getString(R.string.lsId), lsId);
        startActivity(intent);
        overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
    }

    private void initiateTypePopUp(LinearLayout linDropDown) {
        LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        // get the pop-up window i.e. drop-down layout
        LinearLayout layout = (LinearLayout) inflater.inflate(
                R.layout.popup_drop_down, null);

        pw = new PopupWindow(layout, ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.MATCH_PARENT, true);

        pw.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        // Pop-up window background cannot be null if we want the pop-up to
        // listen touch events outside its window
//        pw.setBackgroundDrawable(new BitmapDrawable());
        pw.setTouchable(true);
        pw.setFocusable(true);

        // let pop-up be informed about touch events outside its window. This
        // should be done before setting the content of pop-up
        pw.setOutsideTouchable(false);
        pw.setHeight(ViewGroup.LayoutParams.WRAP_CONTENT);
        pw.setWidth(ViewGroup.LayoutParams.MATCH_PARENT);
//         dismiss the pop-up i.e. drop-down when touched anywhere outside the
        // pop-up
        pw.setTouchInterceptor(new View.OnTouchListener() {

            public boolean onTouch(View v, MotionEvent event) {
                // TODO Auto-generated method stub
                if (event.getAction() == MotionEvent.ACTION_OUTSIDE) {
                    pw.dismiss();
                    return true;
                }
                return false;
            }
        });

        pw.setOnDismissListener(new PopupWindow.OnDismissListener() {

            @Override
            public void onDismiss() {
                // TODO Auto-generated method stub
            }
        });

        // provide the source layout for drop-down
        pw.setContentView(layout);

        int x = (int) linDropDown.getX();
        int y = (int) linDropDown.getY() - 9;

        // anchor the drop-down to bottom-left corner of 'layout1'
        pw.showAsDropDown(linDropDown, x, y);


        RecyclerView rvFirstLevel = (RecyclerView) layout.findViewById(R.id.rvFirstLevel);
        rvSecondLevel = (RecyclerView) layout.findViewById(R.id.rvSecondLevel);

        TextView txtLibSel, txtLibColl;
        LinearLayout linLib;

        linLib = (LinearLayout) layout.findViewById(R.id.linLib);
        txtLibSel = (TextView) layout.findViewById(R.id.txtLibSel);
        txtLibColl = (TextView) layout.findViewById(R.id.txtLibColl);

        linLib.setVisibility(View.VISIBLE);
        txtLibSel.setVisibility(View.GONE);
        txtLibColl.setVisibility(View.VISIBLE);

        if (Global.selectedLibCollection != null)
            txtLibColl.setText(Global.selectedLibCollection);

        rvFirstLevel.setLayoutManager(new LinearLayoutManager(SubIndexActivity.this, LinearLayoutManager.HORIZONTAL, false));
        rvFirstLevel.setAdapter(new SubIndexFirstLevelMenuAdapter(SubIndexActivity.this, new GetLibCollectionFromDB(SubIndexActivity.this).getLsDataFromDb(lcId), lcId));

        ArrayList<LsMainIndexData> combineIndexDataArray = new ArrayList<>();
        combineIndexDataArray = LibraryIndexActivity.combineIndexDataArray;

        setSecondLevel(combineIndexDataArray, lsId);
    }

    public void setSecondLevel(ArrayList<LsMainIndexData> combineIndexDataArray, String lsId) {
        if (combineIndexDataArray != null && combineIndexDataArray.size() > 0) {

            rvSecondLevel.setLayoutManager(new LinearLayoutManager(SubIndexActivity.this, LinearLayoutManager.HORIZONTAL, false));
            subIndexSecondLevelMenuAdapter = new SubIndexSecondLevelMenuAdapter(SubIndexActivity.this, combineIndexDataArray, lcId, lsId);
            rvSecondLevel.setAdapter(subIndexSecondLevelMenuAdapter);
            int scrollToPos = 0;
            for (int i = 0; i < combineIndexDataArray.size(); i++) {
                if (title.equals(combineIndexDataArray.get(i).getTitle_html())) {
                    scrollToPos = i;
                }
            }

            rvSecondLevel.scrollToPosition(scrollToPos);
            subIndexSecondLevelMenuAdapter.notifyDataSetChanged();
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (pw != null)
            pw.dismiss();
    }

    public void finishActivity() {
        finish();
    }

    @Override
    public void onBackPressed() {

        super.onBackPressed();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menuBack:
                finishActivity();
                break;
            case R.id.menuNext:
                if (Global.moreOptionMenuDatas.get(3).getLcID() != null) {
                    Intent intent = new Intent(getApplicationContext(), ReadingActivity.class);
                    intent.putExtra(getString(R.string.indexForReading), Global.moreOptionMenuDatas.get(3).getIndexForReading());
                    intent.putExtra(getString(R.string.position), Global.moreOptionMenuDatas.get(3).getPostion());
                    intent.putExtra(getString(R.string.isFromIndex), true);
                    intent.putExtra(getString(R.string.is_history), false);
                    intent.putExtra(getString(R.string.lcID), Global.moreOptionMenuDatas.get(3).getLcID());
                    intent.putExtra(getString(R.string.lsId), Global.moreOptionMenuDatas.get(3).getLsId());
                    startActivityWithAnimation(intent);
                } else {
                    Toast.makeText(getApplicationContext(), "No Item Found", Toast.LENGTH_LONG).show();
                }
                break;
            case R.id.menuBookmark:
                Intent bookmarkIntent = new Intent(getApplicationContext(), MainActivity.class);
                bookmarkIntent.putExtra(getString(R.string.menuSelection), "Bookmarks");
                bookmarkIntent.putExtra(getString(R.string.isFromReading), true);
                startActivityWithAnimation(bookmarkIntent);
                break;
            case R.id.menuHistory:
                Intent historyIntent = new Intent(getApplicationContext(), MainActivity.class);
                historyIntent.putExtra(getString(R.string.menuSelection), "History");
                historyIntent.putExtra(getString(R.string.isFromReading), true);
                startActivityWithAnimation(historyIntent);
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}
