package com.ldsscriptures.pro.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.ldsscriptures.pro.R;
import com.ldsscriptures.pro.service.DownloadSubDBService;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created on 30/05/17 by iblinfotech.
 */

public class LoginOneActivity extends BaseActivity {

    @BindView(R.id.btnLogIn)
    Button btnLogIn;

    @BindView(R.id.tv_signUp)
    TextView tvSignUp;

    @BindView(R.id.imageView_background)
    ImageView imageView_background;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_one);

        setContents();
        setFont();
    }


    private void setContents() {
        ButterKnife.bind(this);
        setBackground(R.drawable.bg_splash_blur, imageView_background);
    }

    @OnClick(R.id.btnLogIn)
    public void logIn() {
        startActivity(new Intent(LoginOneActivity.this, LoginActivity.class));
        overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
    }

    @OnClick(R.id.tv_signUp)
    public void signUp() {
        startActivity(new Intent(LoginOneActivity.this, SignUpActivity.class));
        overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
        finish();
    }
}
