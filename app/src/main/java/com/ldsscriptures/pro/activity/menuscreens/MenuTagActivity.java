package com.ldsscriptures.pro.activity.menuscreens;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.ldsscriptures.pro.R;
import com.ldsscriptures.pro.activity.BaseActivity;
import com.ldsscriptures.pro.activity.MainActivity;
import com.ldsscriptures.pro.adapter.MenuTagListAdapter;
import com.ldsscriptures.pro.model.VerseTagsData;
import com.ldsscriptures.pro.utils.SessionManager;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MenuTagActivity extends BaseActivity {

    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    @BindView(R.id.tvTitle)
    TextView tvTitle;
    @BindView(R.id.ivMenu)
    ImageView ivMenu;
    @BindView(R.id.ivBack)
    ImageView ivBack;
    @BindView(R.id.ivSearch)
    ImageView ivSearch;
    @BindView(R.id.ivMore)
    ImageView ivMore;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.tvNoData)
    TextView tvNoData;
    @BindView(R.id.ivTabs)
    ImageView ivTabs;

    private SessionManager sessionManager;
    private MenuTagListAdapter tagListAdapter;
    private LinearLayoutManager layoutManager;
    static MenuTagActivity menuTagActivity;
    private ArrayList<VerseTagsData> tagDataArrayList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu_tag);
        ButterKnife.bind(this);
    }

    private void setContents() {
        ButterKnife.bind(this);
        showText(tvTitle, getString(R.string.txt_tags));
        setVisibilityGone(ivMore);
        sessionManager = new SessionManager(MenuTagActivity.this);
        setRecyclerView();
        menuTagActivity = this;
        setToolbar(toolbar);
        setVisibilityGone(ivMore);
        setVisibilityGone(ivTabs);
        setVisibilityGone(ivSearch);
        setVisibilityVisible(ivMenu);
        setVisibilityGone(ivBack);
    }

    private void setRecyclerView() {
        tagDataArrayList = new SessionManager(MenuTagActivity.this).getPreferencesTagsArrayList(MenuTagActivity.this);
        if (tagDataArrayList != null) {
            if (tagDataArrayList.size() > 0) {

                removeDuplicates(tagDataArrayList);
                tvNoData.setVisibility(View.GONE);
                recyclerView.setVisibility(View.VISIBLE);
                ArrayList<VerseTagsData> tagDataArrayListnew = new ArrayList<>();
                for (int i = 0; i < tagDataArrayList.size(); i++) {
                    if (tagDataArrayList.get(i).getDeleted().isEmpty()) {
                        tagDataArrayListnew.add(tagDataArrayList.get(i));
                    }
                }
                if (tagDataArrayListnew != null) {
                    if (tagDataArrayListnew.size() > 0) {
                        tagListAdapter = new MenuTagListAdapter(MenuTagActivity.this, tagDataArrayListnew);
                        layoutManager = new LinearLayoutManager(MenuTagActivity.this, RecyclerView.VERTICAL, false);
                        recyclerView.setLayoutManager(layoutManager);
                        recyclerView.setAdapter(tagListAdapter);
                    } else {
                        tvNoData.setVisibility(View.VISIBLE);
                        tvNoData.setText(R.string.no_tag_found);
                        recyclerView.setVisibility(View.GONE);
                    }
                }
            } else {
                tvNoData.setVisibility(View.VISIBLE);
                tvNoData.setText(R.string.no_tag_found);
                recyclerView.setVisibility(View.GONE);
            }
        }
    }

    public void visibleHideView() {
        tvNoData.setVisibility(View.VISIBLE);
        tvNoData.setText(R.string.no_tag_found);
        recyclerView.setVisibility(View.GONE);
    }

    private void removeDuplicates(ArrayList<VerseTagsData> verseTagsArrayList) {
        int count = verseTagsArrayList.size();

        for (int i = 0; i < count; i++) {
            for (int j = i + 1; j < count; j++) {
                if (verseTagsArrayList.get(i).getTagname().equals(verseTagsArrayList.get(j).getTagname())) {
                    verseTagsArrayList.remove(j--);
                    count--;
                }
            }
        }
    }

    @OnClick(R.id.ivBack)
    public void finishActivity() {
        finish();
    }

    @Override
    protected void onResume() {
        super.onResume();
        setContents();
    }

    @OnClick(R.id.ivMenu)
    public void openMenu() {
        openSlideMenu();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menuBack:
                finish();
                break;
            case R.id.menuNext:
                Toast.makeText(getApplicationContext(), "No Item Found", Toast.LENGTH_LONG).show();
                break;
            case R.id.menuBookmark:
                Intent bookmarkIntent = new Intent(getApplicationContext(), MainActivity.class);
                bookmarkIntent.putExtra(getString(R.string.menuSelection), "Bookmarks");
                bookmarkIntent.putExtra(getString(R.string.isFromReading), true);
                startActivityWithAnimation(bookmarkIntent);
                break;
            case R.id.menuHistory:
                Intent historyIntent = new Intent(getApplicationContext(), MainActivity.class);
                historyIntent.putExtra(getString(R.string.menuSelection), "History");
                historyIntent.putExtra(getString(R.string.isFromReading), true);
                startActivityWithAnimation(historyIntent);
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    public static MenuTagActivity getInstance() {
        return menuTagActivity;
    }
}
