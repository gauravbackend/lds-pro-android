package com.ldsscriptures.pro.activity.settingmenu;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Switch;
import android.widget.TextView;

import com.ldsscriptures.pro.R;
import com.ldsscriptures.pro.activity.BaseActivity;
import com.ldsscriptures.pro.activity.LanguageActivity;
import com.ldsscriptures.pro.activity.subscriptionmenu.PremiumActivity;
import com.ldsscriptures.pro.dbClass.GetLibCollectionFromDB;
import com.ldsscriptures.pro.model.Language;
import com.ldsscriptures.pro.model.UserDetail;
import com.ldsscriptures.pro.retroutils.APIClient;
import com.ldsscriptures.pro.retroutils.APIInterface;
import com.ldsscriptures.pro.utils.Global;
import com.ldsscriptures.pro.utils.SessionManager;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class SettingsActivity extends BaseActivity implements CompoundButton.OnCheckedChangeListener {

    @BindView(R.id.tvSignOut)
    TextView tvSignOut;
    @BindView(R.id.tvUserName)
    TextView tvUserName;
    @BindView(R.id.tvEmailId)
    TextView tvEmailId;
    @BindView(R.id.linViewOption)
    LinearLayout linViewOption;
    @BindView(R.id.ivBack)
    ImageView ivBack;
    @BindView(R.id.ivMenu)
    ImageView ivMenu;
    @BindView(R.id.tvTitle)
    TextView tvTitle;
    @BindView(R.id.ivDropDownMenu)
    ImageView ivDropDownMenu;
    @BindView(R.id.ivMore)
    ImageView ivMore;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.textView4)
    TextView textView4;
    @BindView(R.id.linGoal)
    LinearLayout linGoal;
    @BindView(R.id.imageView2)
    ImageView imageView2;
    @BindView(R.id.linSync)
    LinearLayout linSync;
    @BindView(R.id.linReminders)
    LinearLayout linReminders;
    @BindView(R.id.textView3)
    TextView textView3;
    @BindView(R.id.linSubscription)
    LinearLayout linSubscription;
    @BindView(R.id.linLanguage)
    LinearLayout linLanguage;
    @BindView(R.id.textView1)
    TextView textView1;
    @BindView(R.id.switchPrivateAccount)
    Switch switchPrivateAccount;
    @BindView(R.id.linPrivacy)
    LinearLayout linPrivacy;
    @BindView(R.id.txtRestore)
    TextView txtRestore;
    @BindView(R.id.textView6)
    TextView textView6;
    @BindView(R.id.linAbout)
    LinearLayout linAbout;
    @BindView(R.id.linSupport)
    LinearLayout linSupport;
    @BindView(R.id.layout_setting_user_name)
    LinearLayout layoutSettingUserName;

    @BindView(R.id.txtSelLang)
    TextView txtSelLang;

    private SessionManager sessionManager;
    boolean switchflag;

    public static SettingsActivity settingsActivity;

    public static SettingsActivity getInstance() {
        return settingsActivity;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);
        ButterKnife.bind(this);
        setContent();
    }

    private void setContent() {
        ButterKnife.bind(this);

        settingsActivity = this;

        sessionManager = new SessionManager(this);
        setVisibilityGone(ivMore);

        //setm the user name and email id
        tvUserName.setText(getUserInfo(SessionManager.KEY_USER_USERNAME));
        tvEmailId.setText(getUserInfo(SessionManager.KEY_USER_EMAIL));
        if (getUserInfo(SessionManager.IS_PRIVATE_SET).equals("1")) {
            switchflag = true;
        } else {
            switchflag = false;
        }

        setLanguageLabel();
        switchPrivateAccount.setChecked(switchflag);
        switchPrivateAccount.setOnCheckedChangeListener(this);
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    @OnClick({R.id.ivMenu, R.id.ivDropDownMenu, R.id.linViewOption, R.id.tvSignOut, R.id.linGoal, R.id.linSync, R.id.linReminders, R.id.linSubscription, R.id.linLanguage, R.id.txtRestore, R.id.linAbout, R.id.linSupport, R.id.layout_setting_user_name})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.ivMenu:
                openSlideMenu();
                break;
            case R.id.ivDropDownMenu:
                break;
            case R.id.ivMore:
                break;
            case R.id.linViewOption:
                startActivityWithAnimation(new Intent(SettingsActivity.this, SettingsViewOptionActivity.class));
                break;
            case R.id.tvSignOut:
                new SessionManager(SettingsActivity.this).logoutUser();
                sessionManager.setSubscribeType("");
                break;
            case R.id.linGoal:
                startActivityWithAnimation(new Intent(SettingsActivity.this, SettingsGoalActivity.class));
                break;
            case R.id.linSync:
                startActivityWithAnimation(new Intent(SettingsActivity.this, SettingsSyncActivity.class));
                break;
            case R.id.linReminders:
                startActivityWithAnimation(new Intent(SettingsActivity.this, SettingRemindersActivity.class));
                break;
            case R.id.linSubscription:
                startActivityWithAnimation(new Intent(SettingsActivity.this, PremiumActivity.class));
                break;
            case R.id.linLanguage:
                startActivityWithAnimation(new Intent(SettingsActivity.this, LanguageActivity.class));
                break;
            case R.id.txtRestore:
                break;
            case R.id.linAbout:
                startActivityWithAnimation(new Intent(SettingsActivity.this, SettingsAboutActivity.class));
                break;
            case R.id.linSupport:
                startActivityWithAnimation(new Intent(SettingsActivity.this, SettingsSupportActivity.class));
                break;
            case R.id.layout_setting_user_name:
                startActivityWithAnimation(new Intent(SettingsActivity.this, SettingUserProfileActivity.class));
                break;
        }
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        if (isChecked) {
            updateUSer("1");
            sessionManager.setParameter(SessionManager.IS_PRIVATE_SET, "1");
        } else {
            updateUSer("false");
            sessionManager.setParameter(SessionManager.IS_PRIVATE_SET, "0");
        }
    }

    public void setLanguageLabel() {
        //set selected language
        ArrayList<Language> langArrayList = new GetLibCollectionFromDB(SettingsActivity.this).getLanguageListFromDB();
        for (int i = 0; i < langArrayList.size(); i++) {
            if (Global.LANGUAGE.equals(langArrayList.get(i).getLang_code())) {
                setVisibilityVisible(txtSelLang);
                txtSelLang.setText(langArrayList.get(i).getName());
            }
        }
    }

    private void updateUSer(String privateFlag) {

        APIInterface apiService = APIClient.getClient().create(APIInterface.class);
        String url = APIClient.BASE_URL;

        url += "updateUser.pl?" +
                "user_id=" + getUserInfo(SessionManager.KEY_USER_ID) +
                "&session_id=" + getUserInfo(SessionManager.KEY_USER_SESSION_ID) +
                "&apiauth=" + APIClient.AUTH_KEY +
                "&private=" + privateFlag;

        Call<UserDetail> callObject = apiService.updateUserInfo(url.replaceAll(" ", "%20"));

        callObject.enqueue(new Callback<UserDetail>() {
            @Override
            public void onResponse(Call<UserDetail> call, Response<UserDetail> response) {

                if (response.body() != null && !response.body().toString().trim().isEmpty()) {

                }
            }

            @Override
            public void onFailure(Call<UserDetail> call, Throwable t) {
                call.cancel();
                Global.dismissProgressDialog();
            }
        });
    }
}
