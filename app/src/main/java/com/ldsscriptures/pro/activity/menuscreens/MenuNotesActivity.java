package com.ldsscriptures.pro.activity.menuscreens;

import android.content.Intent;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.ldsscriptures.pro.R;
import com.ldsscriptures.pro.activity.BaseActivity;
import com.ldsscriptures.pro.activity.MainActivity;
import com.ldsscriptures.pro.adapter.MenuNotesListAdapter;
import com.ldsscriptures.pro.database.CommonDatabaseHelper;
import com.ldsscriptures.pro.model.BookmarkData;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MenuNotesActivity extends BaseActivity {

    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    @BindView(R.id.ivMenu)
    ImageView ivMenu;
    @BindView(R.id.ivBack)
    ImageView ivBack;
    @BindView(R.id.tvTitle)
    TextView tvTitle;
    @BindView(R.id.ivSearch)
    ImageView ivSearch;
    @BindView(R.id.ivMore)
    ImageView ivMore;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.tvNoData)
    TextView tvNoData;
    @BindView(R.id.ivTabs)
    ImageView ivTabs;
    private ArrayList<BookmarkData> notesArrayList = new ArrayList<>();
    private CommonDatabaseHelper commonDatabaseHelper;
    static MenuNotesActivity menuNotesActivity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu_tag);
        ButterKnife.bind(this);
        commonDatabaseHelper = new CommonDatabaseHelper(MenuNotesActivity.this);
    }

    private void setContents() {
        menuNotesActivity = this;
        showText(tvTitle, getString(R.string.txt_notes));
        setRecyclerView();
        setToolbar(toolbar);
        setVisibilityGone(ivMore);
        setVisibilityGone(ivTabs);
        setVisibilityGone(ivSearch);
    }

    private void setRecyclerView() {
        new GetNotes().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, "");
    }

    private class GetNotes extends AsyncTask<String, Void, String> {

        @Override
        protected void onPreExecute() {
            notesArrayList.clear();
        }

        @Override
        protected String doInBackground(String... params) {
            Cursor cursor;

            cursor = commonDatabaseHelper.getNotesList();
            if (cursor != null && cursor.moveToFirst()) {
                while (!cursor.isAfterLast()) {

                    String deleted = cursor.getString(cursor.getColumnIndex("deleted"));
                    if (deleted.isEmpty()) {

                        BookmarkData noteskData = new BookmarkData();
                        noteskData.setServerid(cursor.getString(cursor.getColumnIndex("serverid")));
                        noteskData.setVerseaid(cursor.getString(cursor.getColumnIndex("verseaid")));
                        noteskData.setChapterUri(cursor.getString(cursor.getColumnIndex("chapteruri")));
                        noteskData.setItemuri(cursor.getString(cursor.getColumnIndex("itemuri")));
                        noteskData.setBookuri(cursor.getString(cursor.getColumnIndex("bookuri")));
                        noteskData.setTitle(cursor.getString(cursor.getColumnIndex("versetitle")));
                        noteskData.setVerseNumber(cursor.getString(cursor.getColumnIndex("versenumber")));
                        noteskData.setVerseText(cursor.getString(cursor.getColumnIndex("versetext")));
                        noteskData.setDeleted(deleted);
                        noteskData.setModified(cursor.getString(cursor.getColumnIndex("modified")));
                        notesArrayList.add(noteskData);
                    }
                    cursor.moveToNext();
                }
            }
            commonDatabaseHelper.close();
            cursor.close();

            return "";
        }

        @Override
        protected void onPostExecute(String result) {

            if (notesArrayList != null) {
                if (notesArrayList.size() > 0) {
                    tvNoData.setVisibility(View.GONE);
                    recyclerView.setVisibility(View.VISIBLE);
                    MenuNotesListAdapter tagListAdapter = new MenuNotesListAdapter(MenuNotesActivity.this, notesArrayList);
                    LinearLayoutManager layoutManager = new LinearLayoutManager(MenuNotesActivity.this, RecyclerView.VERTICAL, false);
                    recyclerView.setLayoutManager(layoutManager);
                    recyclerView.setAdapter(tagListAdapter);
                } else {
                    tvNoData.setVisibility(View.VISIBLE);
                    recyclerView.setVisibility(View.GONE);
                    tvNoData.setText(R.string.no_notes_found);
                }
            }
        }
    }

    public void visibleHideView() {
        tvNoData.setVisibility(View.VISIBLE);
        recyclerView.setVisibility(View.GONE);
        tvNoData.setText(R.string.no_notes_found);
    }

    @OnClick(R.id.ivBack)
    public void finishActivity() {
        finish();
    }

    @Override
    protected void onResume() {
        super.onResume();
        setContents();
    }

    @OnClick(R.id.ivMenu)
    public void openMenu() {
        openSlideMenu();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menuBack:
                finish();
                break;
            case R.id.menuNext:
                Toast.makeText(getApplicationContext(), "No Item Found", Toast.LENGTH_LONG).show();
                break;
            case R.id.menuBookmark:
                Intent bookmarkIntent = new Intent(getApplicationContext(), MainActivity.class);
                bookmarkIntent.putExtra(getString(R.string.menuSelection), "Bookmarks");
                bookmarkIntent.putExtra(getString(R.string.isFromReading), true);
                startActivityWithAnimation(bookmarkIntent);
                break;
            case R.id.menuHistory:
                Intent historyIntent = new Intent(getApplicationContext(), MainActivity.class);
                historyIntent.putExtra(getString(R.string.menuSelection), "History");
                historyIntent.putExtra(getString(R.string.isFromReading), true);
                startActivityWithAnimation(historyIntent);
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    public static MenuNotesActivity getInstance() {
        return menuNotesActivity;
    }
}
