package com.ldsscriptures.pro.activity;


import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.ldsscriptures.pro.LoadMoreData.OnLoadMoreListener;
import com.ldsscriptures.pro.LoadMoreData.RecyclerViewLoadMoreScroll;
import com.ldsscriptures.pro.R;
import com.ldsscriptures.pro.activity.activitymenu.ActivityScreen;
import com.ldsscriptures.pro.activity.socialmenu.TimelineActivity;
import com.ldsscriptures.pro.adapter.TimelineListAdapter;
import com.ldsscriptures.pro.model.TimeLine.InnerTimeLineModel;
import com.ldsscriptures.pro.model.TimeLine.MainTimeLineModel;
import com.ldsscriptures.pro.retroutils.APIClient;
import com.ldsscriptures.pro.retroutils.APIInterface;
import com.ldsscriptures.pro.utils.SessionManager;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.concurrent.TimeUnit;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DashboardSetGoalActivity extends BaseActivity {

    @BindView(R.id.imageView_background)
    ImageView imageView_background;
    @BindView(R.id.txtGoalPoints)
    TextView txtGoalPoints;
    @BindView(R.id.txt_day_strike)
    TextView txt_day_strike;
    @BindView(R.id.txtReadingMinute)
    TextView txtReadingMinute;
    @BindView(R.id.txtDailyPoint)
    TextView txtDailyPoint;
    @BindView(R.id.goalHitTxt)
    TextView goalHitTxt;
    @BindView(R.id.pbRating)
    ProgressBar pbRating;
    @BindView(R.id.ivClose)
    ImageView ivClose;
    @BindView(R.id.tvUserName)
    TextView tvUserName;
    @BindView(R.id.adjustment)
    ImageView adjustment;
    @BindView(R.id.ivMore)
    ImageView ivMore;
    @BindView(R.id.linToolbar)
    LinearLayout linToolbar;
    @BindView(R.id.linGoalData)
    LinearLayout linGoalData;
    @BindView(R.id.imageView3)
    ImageView imageView3;
    @BindView(R.id.txt_achivement_dashboard)
    TextView txtAchivementDashboard;
    @BindView(R.id.linMenu)
    LinearLayout linMenu;
    @BindView(R.id.txt_more_status)
    TextView txtMoreStatus;
    @BindView(R.id.divider_more_status)
    TextView dividerMoreStatus;
    @BindView(R.id.txt_timelin_status)
    TextView txtTimelinStatus;
    @BindView(R.id.txt_no_timeline_data_found)
    TextView txtNoTimelineDataFound;
    @BindView(R.id.timeline_recycler_view)
    RecyclerView timelineRecyclerView;
    @BindView(R.id.timeline_progress)
    ProgressBar timelineProgress;

    private static final String PREF_NAME = "LDS_Session";
    int PRIVATE_MODE = 0;

    private SessionManager sessionManager;
    SharedPreferences sharedPreferences;

    public static SharedPreferences.Editor editor;

    String last_timeline_date = "";
    String getSyncAgain = "";

    static TimelineActivity timelineActivity;
    TimelineListAdapter mAdapter;

    private RecyclerViewLoadMoreScroll scrollListener;
    private ArrayList<InnerTimeLineModel> timelineItemArrayList;

    String paraUserId = "";
    String paraUserImage = "";
    String paraUserName = "";
    String paraSessionId = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard_daily_goal);
        ButterKnife.bind(this);

        sharedPreferences = getApplicationContext().getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        editor = sharedPreferences.edit();
        sessionManager = new SessionManager(this);
        setContents();
    }

    private void setContents() {

        ButterKnife.bind(this);
        setBackground(R.drawable.bg_splash_blur, imageView_background);

        paraUserId = getUserInfo(SessionManager.KEY_USER_ID);
        paraUserImage = getUserInfo(SessionManager.KEY_USR_IMG_URL);
        paraUserName = getUserInfo(SessionManager.KEY_USER_USERNAME);
        paraSessionId = getUserInfo(SessionManager.KEY_USER_SESSION_ID);
        day_strike();

        int x = Integer.parseInt(sessionManager.getUserMinReading());
        // set reading minute
        if (sessionManager.getUserMinReading() != null) {
            if (x >= 10000) {
                int point = (int) Math.round((x / 1000));
                DecimalFormat form = new DecimalFormat("#,##");
                txtReadingMinute.setText(form.format(point) + "k");
            } else {
                txtReadingMinute.setText(sessionManager.getUserMinReading());
            }
        }

        // set the selected point
        if (sessionManager.getUserGoal() != null)
            txtGoalPoints.setText(sessionManager.getUserGoal());

        // set the daily point
        int y = sessionManager.getUserDailyPoints();
        if (sessionManager.getUserDailyPoints() != 0) {
            if (y >= 10000) {
                int point = (int) Math.round((y / 1000));
                DecimalFormat form = new DecimalFormat("#,##");
                txtDailyPoint.setText(form.format(point) + "k");

            } else {
                txtDailyPoint.setText(String.valueOf(sessionManager.getUserDailyPoints()));
            }
        }

        if (sessionManager.getGoalHit() != null) {
            goalHitTxt.setText(sessionManager.getGoalHit() + " " + "%");
        }

        if (sessionManager.getAchivementCount() != null) {
            txtAchivementDashboard.setText(sessionManager.getAchivementCount());
        }

        pbRating.setProgress(Integer.parseInt(sessionManager.getGoalHit()));

        timelineItemArrayList = new ArrayList<>();
        timelineItemArrayList.clear();
        timelineRecyclerView.setHasFixedSize(true);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        timelineRecyclerView.setLayoutManager(linearLayoutManager);
        mAdapter = new TimelineListAdapter(DashboardSetGoalActivity.this, timelineItemArrayList, 1, paraUserId, paraUserImage, paraUserName, paraSessionId);
        timelineRecyclerView.setAdapter(mAdapter);

        scrollListener = new RecyclerViewLoadMoreScroll(linearLayoutManager);
        scrollListener.setOnLoadMoreListener(new OnLoadMoreListener() {
            @Override
            public void onLoadMore() {
                if (getSyncAgain.equals("1")) {
                    setVisibilityVisible(timelineProgress);
                    getTimeLineAPI(1);
                } else {
                    setVisibilityGone(timelineProgress);
                }
            }
        });

        timelineRecyclerView.addOnScrollListener(scrollListener);

        getTimeLineAPI(0);
    }

    @OnClick(R.id.ivClose)
    public void closeThis() {
        finish();
    }

    public void day_strike() {
        String currentDateTimeString = new SimpleDateFormat("dd/MM/yyyy").format(Calendar.getInstance().getTime());
        String date1 = currentDateTimeString;
        String date2 = sharedPreferences.getString(sessionManager.getLastDate(), null);
        int dateDifference = (int) getDateDiff(new SimpleDateFormat("dd/MM/yyyy"), date2, date1);
        editor.putString(sessionManager.KEY_USER_DAY_STREAK, String.valueOf(dateDifference));
        editor.commit();
        txt_day_strike.setText(sharedPreferences.getString(sessionManager.KEY_USER_DAY_STREAK, null));
    }

    public static long getDateDiff(SimpleDateFormat format, String oldDate, String newDate) {
        try {
            if (oldDate == null) {
                oldDate = newDate;
            }
            return TimeUnit.DAYS.convert(format.parse(newDate).getTime() - format.parse(oldDate).getTime(), TimeUnit.MILLISECONDS);
        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        }
    }

    private void getTimeLineAPI(final int refreshFlag) {

        if (refreshFlag == 0) {
            showprogress();
        }

        String paraUserId = "", paraSessionId = "";

        if (getUserInfo(SessionManager.KEY_USER_ID) != null) {
            paraUserId = getUserInfo(SessionManager.KEY_USER_ID);
        }
        if (getUserInfo(SessionManager.KEY_USER_SESSION_ID) != null) {
            paraSessionId = getUserInfo(SessionManager.KEY_USER_SESSION_ID);
        }

        APIInterface apiService = APIClient.getClient().create(APIInterface.class);
        String url = APIClient.BASE_URL;

        url += "timelineSync.pl?user_id=" + paraUserId +
                "&session_id=" + paraSessionId +
                "&apiauth=" + APIClient.AUTH_KEY +
                "&last_timeline_date=" + last_timeline_date;

        Call<MainTimeLineModel<InnerTimeLineModel>> callObject = apiService.getTimeLineData(url.replaceAll(" ", "%20"));

        callObject.enqueue(new Callback<MainTimeLineModel<InnerTimeLineModel>>() {

            @Override
            public void onResponse(Call<MainTimeLineModel<InnerTimeLineModel>> call, Response<MainTimeLineModel<InnerTimeLineModel>> response) {

                if (response.body() != null && !response.body().toString().trim().isEmpty()) {

                    last_timeline_date = response.body().getSync_again_date();
                    getSyncAgain = response.body().getSync_again();

                    if (response.body().getItems() != null) {
                        if ((response.body().getItems().size() != 0)) {
                            timelineItemArrayList.addAll(response.body().getItems());
                        }
                    }

                    mAdapter.notifyDataSetChanged();
                    scrollListener.setLoaded();
                    setVisibilityGone(timelineProgress);

                    if (refreshFlag == 0) {
                        dismissprogress();
                    }

                    if (timelineItemArrayList.size() == 0) {
                        txtNoTimelineDataFound.setVisibility(View.VISIBLE);
                        timelineRecyclerView.setVisibility(View.GONE);
                    } else {
                        txtNoTimelineDataFound.setVisibility(View.GONE);
                        timelineRecyclerView.setVisibility(View.VISIBLE);
                    }
                }

            }

            @Override
            public void onFailure(Call<MainTimeLineModel<InnerTimeLineModel>> call, Throwable t) {
                if (refreshFlag == 0) {
                    dismissprogress();
                }
            }
        });
    }

    @OnClick(R.id.txt_more_status)
    public void onViewClicked() {
        startActivity(new Intent(getApplicationContext(), ActivityScreen.class));
    }
}
