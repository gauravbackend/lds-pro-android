package com.ldsscriptures.pro.activity.content;

import android.content.Intent;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.ldsscriptures.pro.R;
import com.ldsscriptures.pro.activity.BaseActivity;
import com.ldsscriptures.pro.activity.MainActivity;
import com.ldsscriptures.pro.adapter.HymnsTopicContentListAdapter;
import com.ldsscriptures.pro.adapter.HymnsTopicsListAdapter;
import com.ldsscriptures.pro.database.HymnsDatabaseHelper;
import com.ldsscriptures.pro.model.HymnsTopicContentData;
import com.ldsscriptures.pro.model.HymnsTopicsData;
import com.turingtechnologies.materialscrollbar.AlphabetIndicator;
import com.turingtechnologies.materialscrollbar.CustomIndicator;
import com.turingtechnologies.materialscrollbar.DragScrollBar;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class HymnsActivity extends BaseActivity {

    @BindView(R.id.ivMenu)
    ImageView ivMenu;
    Spinner spinner;

    @BindView(R.id.ivSearch)
    ImageView ivSearch;

    @BindView(R.id.ivMore)
    ImageView ivMore;

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.txtSort)
    TextView txtSort;

    @BindView(R.id.relSort)
    RelativeLayout relSort;

    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;

    private HymnsDatabaseHelper hymnsDatabaseHelper;
    private HymnsTopicsListAdapter hymnsTopicsListAdapter;
    private ArrayList<HymnsTopicsData> hymnsTopicsDataArrayList = new ArrayList<>();
    private ArrayList<HymnsTopicContentData> hymnsTopicContentDataArrayList = new ArrayList<>();

    public static HymnsActivity hymnsActivity;
    private HymnsTopicContentListAdapter hymnsTopicContentListAdapter;

    public static HymnsActivity getInstance() {
        return hymnsActivity;
    }

    boolean lastSortStatus = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hymns);
        setContents();
    }

    private void setContents() {
        try {
            ButterKnife.bind(this);

            // assign value for getInstance
            hymnsActivity = this;

            setVisibilityGone(ivMore);
            setVisibilityGone(ivSearch);

            // assign databaseHelper object.
            hymnsDatabaseHelper = new HymnsDatabaseHelper(HymnsActivity.this);
            hymnsDatabaseHelper.openDataBase();

            //set toolbar
            setToolbar(toolbar);

            spinner = (Spinner) findViewById(R.id.spinner);

            // set managerLayout for recyclerView
            recyclerView.setLayoutManager(new LinearLayoutManager(HymnsActivity.this));

            if (lastSortStatus) {
                ((DragScrollBar) findViewById(R.id.dragScrollBar)).setIndicator(new AlphabetIndicator(this), true);
            } else {
                ((DragScrollBar) findViewById(R.id.dragScrollBar)).setIndicator(new CustomIndicator(this), true);
            }

            //get the category from the local database
            new HymnsActivity.GetHymnsTopics().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, "");

            displayAllHymnsData();

            spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    for (int i = 0; i < hymnsTopicsDataArrayList.size(); i++) {
                        if (i == position)
                            hymnsTopicsDataArrayList.get(i).setSelected(true);
                        else {
                            hymnsTopicsDataArrayList.get(i).setSelected(false);
                        }
                    }
                    if (position == 0) {
                        displayAllHymnsData();
                    } else {
                        new HymnsActivity.GetSpecificContent().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, position);
                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {
                }
            });
        } catch (Exception e) {
        }
    }

    @OnClick({R.id.relSort, R.id.ivMenu})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.ivMenu:
                openSlideMenu();
                break;
            case R.id.relSort:
                if (txtSort.getText().toString().equalsIgnoreCase(getString(R.string.sort_by_name))) {
                    sortList(true);
                } else {
                    sortList(false);
                }

                break;
        }
    }

    public void sortList(boolean sortByName) {
        if (sortByName) {
            lastSortStatus = true;
            Collections.sort(hymnsTopicContentDataArrayList, new Comparator<HymnsTopicContentData>() {
                public int compare(HymnsTopicContentData one, HymnsTopicContentData other) {
                    return one.getName().compareTo(other.getName());
                }
            });

            if (hymnsTopicContentListAdapter != null)
                hymnsTopicContentListAdapter.notifyDataSetChanged();

            txtSort.setText(getResources().getString(R.string.sort_by_number));
        } else {
            lastSortStatus = false;

            Collections.sort(hymnsTopicContentDataArrayList, new Comparator<HymnsTopicContentData>() {
                @Override
                public int compare(HymnsTopicContentData lhs, HymnsTopicContentData rhs) {
                    int i1 = Integer.parseInt(lhs.getId());
                    int i2 = Integer.parseInt(rhs.getId());
                    return Integer.valueOf(i1).compareTo(i2);
                }
            });

            hymnsTopicContentListAdapter.notifyDataSetChanged();
            txtSort.setText(getResources().getString(R.string.sort_by_name));
        }

        if (lastSortStatus) {
            ((DragScrollBar) findViewById(R.id.dragScrollBar))
                    .setIndicator(new AlphabetIndicator(this), true);
        } else {
            ((DragScrollBar) findViewById(R.id.dragScrollBar))
                    .setIndicator(new CustomIndicator(this), true);
        }
    }

    public void displayAllHymnsData() {
        new HymnsActivity.GetHymnsAllContent().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, "");
    }

    private class GetHymnsTopics extends AsyncTask<String, Void, String> {
        @Override
        protected void onPreExecute() {
        }

        @Override
        protected String doInBackground(String... params) {
            Cursor cursor;
            cursor = hymnsDatabaseHelper.getTopicsList();

            hymnsTopicsDataArrayList = new ArrayList<>();
            if (cursor != null && cursor.moveToFirst()) {
                while (!cursor.isAfterLast()) {
                    HymnsTopicsData hymnsTopicsData = new HymnsTopicsData();
                    hymnsTopicsData.setId(cursor.getString(cursor.getColumnIndex(HymnsDatabaseHelper.ID)));
                    hymnsTopicsData.setName(cursor.getString(cursor.getColumnIndex(HymnsDatabaseHelper.NAME)));
                    hymnsTopicsDataArrayList.add(hymnsTopicsData);
                    cursor.moveToNext();
                }
            }
            hymnsDatabaseHelper.close();
            if (cursor != null) {
                cursor.close();
            }
            return "";
        }

        @Override
        protected void onPostExecute(String result) {
            HymnsTopicsData hymnsTopicsData = new HymnsTopicsData();
            hymnsTopicsData.setName(getString(R.string.all_hymns));
            hymnsTopicsDataArrayList.add(0, hymnsTopicsData);

            hymnsTopicsListAdapter = new HymnsTopicsListAdapter(HymnsActivity.this, hymnsTopicsDataArrayList);
            spinner.setAdapter(hymnsTopicsListAdapter);
        }
    }

    private class GetHymnsAllContent extends AsyncTask<String, Void, String> {
        @Override
        protected void onPreExecute() {
        }

        @Override
        protected String doInBackground(String... params) {
            Cursor cursor;
            cursor = hymnsDatabaseHelper.getAllContentList();
            hymnsTopicContentDataArrayList = new ArrayList<>();
            if (cursor != null && cursor.moveToFirst()) {
                while (!cursor.isAfterLast()) {
                    HymnsTopicContentData hymnsTopicContentData = new HymnsTopicContentData();
                    hymnsTopicContentData.setId(cursor.getString(cursor.getColumnIndex(HymnsDatabaseHelper.ID)));
                    hymnsTopicContentData.setName(cursor.getString(cursor.getColumnIndex(HymnsDatabaseHelper.NAME)));
                    hymnsTopicContentData.setCopyrighted(cursor.getString(cursor.getColumnIndex(HymnsDatabaseHelper.CONTENT_COPYRIGHTED)));
                    hymnsTopicContentData.setPage_url(cursor.getString(cursor.getColumnIndex(HymnsDatabaseHelper.CONTENT_PAGE_URL)));
                    hymnsTopicContentData.setPdf_url(cursor.getString(cursor.getColumnIndex(HymnsDatabaseHelper.CONTENT_PDF_URL)));
                    hymnsTopicContentDataArrayList.add(hymnsTopicContentData);
                    cursor.moveToNext();
                }
                hymnsDatabaseHelper.close();
                if (cursor != null) {
                    cursor.close();
                }
            }

            return "";
        }

        @Override
        protected void onPostExecute(String result) {
            hymnsTopicContentListAdapter = new HymnsTopicContentListAdapter(HymnsActivity.this, hymnsTopicContentDataArrayList);
            recyclerView.setAdapter(hymnsTopicContentListAdapter);
            sortList(lastSortStatus);
        }
    }

    private class GetSpecificContent extends AsyncTask<Integer, Void, String> {
        @Override
        protected void onPreExecute() {
        }

        @Override
        protected String doInBackground(Integer... params) {

            Cursor cursor;
            cursor = hymnsDatabaseHelper.getSpecificContent(params[0]);

            hymnsTopicContentDataArrayList = new ArrayList<>();
            if (cursor != null && cursor.moveToFirst()) {
                while (!cursor.isAfterLast()) {
                    HymnsTopicContentData hymnsTopicContentData = new HymnsTopicContentData();
                    hymnsTopicContentData.setId(cursor.getString(cursor.getColumnIndex(HymnsDatabaseHelper.ID)));
                    hymnsTopicContentData.setName(cursor.getString(cursor.getColumnIndex(HymnsDatabaseHelper.NAME)));
                    hymnsTopicContentData.setCopyrighted(cursor.getString(cursor.getColumnIndex(HymnsDatabaseHelper.CONTENT_COPYRIGHTED)));
                    hymnsTopicContentData.setPage_url(cursor.getString(cursor.getColumnIndex(HymnsDatabaseHelper.CONTENT_PAGE_URL)));
                    hymnsTopicContentData.setPdf_url(cursor.getString(cursor.getColumnIndex(HymnsDatabaseHelper.CONTENT_PDF_URL)));
                    hymnsTopicContentDataArrayList.add(hymnsTopicContentData);
                    cursor.moveToNext();
                }
            }
            hymnsDatabaseHelper.close();
            if (cursor != null) {
                cursor.close();
            }
            return "";
        }

        @Override
        protected void onPostExecute(String result) {
            hymnsTopicContentListAdapter = new HymnsTopicContentListAdapter(HymnsActivity.this, hymnsTopicContentDataArrayList);
            recyclerView.setAdapter(hymnsTopicContentListAdapter);
            sortList(lastSortStatus);

        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menuBack:
                finish();
                break;
            case R.id.menuNext:
                Toast.makeText(getApplicationContext(), "No Item Found", Toast.LENGTH_LONG).show();
                break;
            case R.id.menuBookmark:
                Intent bookmarkIntent = new Intent(getApplicationContext(), MainActivity.class);
                bookmarkIntent.putExtra(getString(R.string.menuSelection), "Bookmarks");
                bookmarkIntent.putExtra(getString(R.string.isFromReading), true);
                startActivityWithAnimation(bookmarkIntent);
                break;
            case R.id.menuHistory:
                Intent historyIntent = new Intent(getApplicationContext(), MainActivity.class);
                historyIntent.putExtra(getString(R.string.menuSelection), "History");
                historyIntent.putExtra(getString(R.string.isFromReading), true);
                startActivityWithAnimation(historyIntent);
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}