package com.ldsscriptures.pro.activity.studytools;

import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Switch;
import android.widget.TextView;

import com.ldsscriptures.pro.R;
import com.ldsscriptures.pro.activity.BaseActivity;
import com.ldsscriptures.pro.database.CommonDatabaseHelper;
import com.ldsscriptures.pro.model.PlanReminderData;
import com.ldsscriptures.pro.model.PlanReminderMainData;
import com.ldsscriptures.pro.model.ReminderCheckItem;
import com.ldsscriptures.pro.service.PlanNotificationEventReceiver;
import com.ldsscriptures.pro.utils.Global;
import com.ldsscriptures.pro.utils.SessionManager;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by IBL InfoTech on 8/5/2017.
 */

public class PlanDetailsActivity extends BaseActivity implements CompoundButton.OnCheckedChangeListener {

    @BindView(R.id.ivMenu)
    ImageView ivMenu;
    @BindView(R.id.ivBack)
    ImageView ivBack;
    @BindView(R.id.tvTitle)
    TextView tvTitle;
    @BindView(R.id.ivSearch)
    ImageView ivSearch;
    @BindView(R.id.ivMore)
    ImageView ivMore;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.pbPlanDetails)
    ProgressBar pbPlanDetails;
    @BindView(R.id.txtCreatedDate)
    TextView txtCreatedDate;
    @BindView(R.id.txtCreatedYear)
    TextView txtCreatedYear;
    @BindView(R.id.txtPlanReadingDay)
    TextView txtPlanReadingDay;
    @BindView(R.id.txtPlanTotalDay)
    TextView txtPlanTotalDay;
    @BindView(R.id.txtDaysStatus)
    TextView txtDaysStatus;
    @BindView(R.id.txtFinishDate)
    TextView txtFinishDate;
    @BindView(R.id.txtFinishYear)
    TextView txtFinishYear;
    @BindView(R.id.createPlanReminder)
    TextView createPlanReminder;
    @BindView(R.id.createPlanReminderDays)
    TextView createPlanReminderDays;
    @BindView(R.id.switch_everyday)
    Switch switchEveryday;
    @BindView(R.id.lay_open_dialog)
    LinearLayout layOpenDialog;
    @BindView(R.id.img_notify)
    ImageView imgNotify;
    @BindView(R.id.catch_me_up)
    TextView catchMeUp;

    SessionManager sessionManager;
    static PlanDetailsActivity planDetailsActivity;
    String var_Plan_created_date, var_Plan_createDay, var_Plan_days, var_Plan_left_Days, var_Plan_finish_date,
            var_Plan_name, var_Plan_id, var_leftDaysPercentage, var_DaysName = "";
    CommonDatabaseHelper commonDatabaseHelper;
    @BindView(R.id.ivTabs)
    ImageView ivTabs;

    private ArrayList<PlanReminderMainData> planReminderMainDataArrayList = new ArrayList<>();
    private ArrayList<PlanReminderMainData> NewplanReminderMainDataArrayList = new ArrayList<>();
    private ArrayList<ReminderCheckItem> ReminderCheckItemArrayList = new ArrayList<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_plan_details);
        ButterKnife.bind(this);
    }

    public void setContents() {

        ivMenu.setVisibility(View.GONE);
        ivBack.setVisibility(View.VISIBLE);
        setVisibilityGone(ivTabs);
        setVisibilityGone(ivSearch);


        planDetailsActivity = this;
        commonDatabaseHelper = new CommonDatabaseHelper(this);

        sessionManager = new SessionManager(PlanDetailsActivity.this);
        planReminderMainDataArrayList = sessionManager.getPlanReminderData();
        ReminderCheckItemArrayList = sessionManager.getCheckedNotification();

        Intent intent = getIntent();
        var_Plan_created_date = intent.getStringExtra("var_Plan_created_date");
        var_Plan_createDay = intent.getStringExtra("var_Plan_createDay");
        var_Plan_days = intent.getStringExtra("var_Plan_days");
        var_Plan_left_Days = intent.getStringExtra("var_Plan_left_Days");
        var_Plan_finish_date = intent.getStringExtra("var_Plan_finish_date");
        var_Plan_name = intent.getStringExtra("var_Plan_name");
        var_Plan_id = intent.getStringExtra("var_Plan_id");
        var_leftDaysPercentage = intent.getStringExtra("var_leftDaysPercentage");

        String[] separatedCreatedDate = var_Plan_created_date.split(",");
        String[] separatedFinishDate = var_Plan_finish_date.split(",");

        txtCreatedDate.setText(separatedCreatedDate[0].trim().toString());
        txtCreatedYear.setText(separatedCreatedDate[1].trim().toString());
        txtFinishDate.setText(separatedFinishDate[0].trim().toString());
        txtFinishYear.setText(separatedFinishDate[1].trim().toString());
        txtPlanReadingDay.setText(var_Plan_createDay);
        txtPlanTotalDay.setText(var_Plan_days);
        txtDaysStatus.setText(var_Plan_left_Days);
        pbPlanDetails.setProgress(Integer.valueOf(var_leftDaysPercentage));


        if (ReminderCheckItemArrayList != null) {
            for (int i = 0; i < ReminderCheckItemArrayList.size(); i++) {
                Boolean checkFlag = ReminderCheckItemArrayList.get(i).isPlanFlag();
                String planID = ReminderCheckItemArrayList.get(i).getPlanid();
                if (planID.equals(var_Plan_id)) {
                    switchEveryday.setChecked(checkFlag);
                }
            }
        } else {
            switchEveryday.setChecked(false);
        }


        var_DaysName = "";
        if (planReminderMainDataArrayList != null) {
            for (int i = 0; i < planReminderMainDataArrayList.size(); i++) {

                String Plan_id = planReminderMainDataArrayList.get(i).getPlanID().toString();
                String Plan_ReminderTime = planReminderMainDataArrayList.get(i).getPlanTime().toString();
                int PlanHour = planReminderMainDataArrayList.get(i).getPlanSelectHour();
                int PlanMinute = planReminderMainDataArrayList.get(i).getPlanSelectMinute();
                Boolean PlanDaysFlag = planReminderMainDataArrayList.get(i).getPlanDaysFlag();

                ArrayList<PlanReminderData> planReminderDataArrayList = planReminderMainDataArrayList.get(i).getPlanReminderDataArrayList();

                if (Plan_id.equals(var_Plan_id)) {
                    createPlanReminder.setText(Plan_ReminderTime);
                    if (PlanDaysFlag) {
                        createPlanReminderDays.setText("Everyday");
                        NewplanReminderMainDataArrayList = planReminderMainDataArrayList;

                    } else {
                        for (int j = 0; j < planReminderDataArrayList.size(); j++) {
                            Boolean reminderFlag = planReminderDataArrayList.get(j).getPlanReminderFlag();
                            if (reminderFlag) {
                                var_DaysName = var_DaysName + "," + planReminderDataArrayList.get(j).getPlanReminderFullText();
                                NewplanReminderMainDataArrayList.add(new PlanReminderMainData(Plan_id, Plan_ReminderTime, PlanHour, PlanMinute, PlanDaysFlag, planReminderDataArrayList));

                            }
                        }

                        var_DaysName = var_DaysName.startsWith(",") ? var_DaysName.substring(1) : var_DaysName;
                        createPlanReminderDays.setText(var_DaysName);
                    }
                }
            }
        }

        showText(tvTitle, var_Plan_name);
        switchEveryday.setOnCheckedChangeListener(this);
    }

    public static PlanDetailsActivity getInstance() {
        return planDetailsActivity;
    }

    @OnClick({R.id.ivBack, R.id.lay_open_dialog, R.id.catch_me_up})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.ivBack:
                finish();
                break;
            case R.id.lay_open_dialog:
                Intent i = new Intent(getApplicationContext(), PlanReminderActivity.class);
                i.putExtra("var_Plan_id", var_Plan_id);
                startActivity(i);
                break;
            case R.id.catch_me_up:

                Cursor cursor;
                cursor = commonDatabaseHelper.getPlanList();
                if (cursor != null && cursor.moveToFirst()) {
                    while (!cursor.isAfterLast()) {

                        String plan_reading_min = cursor.getString(cursor.getColumnIndex(CommonDatabaseHelper.KEY_PLAN_READING_MIN));
                        String plan_createdDate = cursor.getString(cursor.getColumnIndex(CommonDatabaseHelper.KEY_PLAN_CREATED_DATE));

                        final int createDay = (Integer.valueOf(plan_reading_min) / 60) / 24;
                        final String currant_date = new SimpleDateFormat("dd/MM/yyyy").format(Calendar.getInstance().getTime());
                        final int dateDiff = Math.abs((int) getDateDiff(new SimpleDateFormat("dd/MM/yyyy"), plan_createdDate.toString(), currant_date));
                        final int leftDaysDifferent = dateDiff - createDay;

                        int left_Days = 0;
                        if (leftDaysDifferent >= 1) {
                            left_Days = Math.abs((int) leftDaysDifferent) * 1440 + Integer.parseInt(plan_reading_min);
                            ContentValues contentValues = new ContentValues();
                            contentValues.put(CommonDatabaseHelper.KEY_PLAN_READING_MIN, left_Days);
                            commonDatabaseHelper.updateRowData(CommonDatabaseHelper.PLAN_MASTER, contentValues, commonDatabaseHelper.KEY_PLAN_ID + " = ?",
                                    new String[]{String.valueOf(var_Plan_id)});
                        }

                        cursor.moveToNext();
                    }
                }
                startActivity(new Intent(getApplicationContext(), PlanActivity.class));

                break;
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        setContents();
    }

    @Override
    protected void onStart() {
        super.onStart();

        planDetailsActivity = this;
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        if (isChecked) {
            setDataSesstion(isChecked);
            sessionManager.setNotificationPlanReminderData(NewplanReminderMainDataArrayList);
            PlanNotificationEventReceiver.setupAlarm(getApplicationContext());
        } else {
            setDataSesstion(isChecked);
        }
    }

    public void setDataSesstion(boolean isChecked) {

        if (ReminderCheckItemArrayList != null) {
            for (int i = 0; i < ReminderCheckItemArrayList.size(); i++) {
                String planID = ReminderCheckItemArrayList.get(i).getPlanid();
                if (planID.equals(var_Plan_id)) {
                    ReminderCheckItemArrayList.remove(i);
                }
            }
            ReminderCheckItemArrayList.add(new ReminderCheckItem(var_Plan_id, isChecked, ""));
            sessionManager.setCheckedNotification(ReminderCheckItemArrayList);
        } else {
            ReminderCheckItemArrayList = new ArrayList<>();
            ReminderCheckItemArrayList.add(new ReminderCheckItem(var_Plan_id, isChecked, ""));
            sessionManager.setCheckedNotification(ReminderCheckItemArrayList);
        }

    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        setIntent(intent);
    }

    @Override
    protected void onResumeFragments() {
        super.onResumeFragments();
    }
}
