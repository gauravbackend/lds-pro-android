package com.ldsscriptures.pro.activity.activitymenu;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.ldsscriptures.pro.R;
import com.ldsscriptures.pro.activity.BaseActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class AchievementsDetailsActivity extends BaseActivity {

    @BindView(R.id.image_back_achiv_detail)
    ImageView imageBackAchivDetail;
    @BindView(R.id.imageGetAchivement)
    ImageView imageGetAchivement;
    @BindView(R.id.txtGetAchivement)
    TextView txtGetAchivement;
    @BindView(R.id.txtStaticAchivement)
    TextView txtStaticAchivement;
    @BindView(R.id.btnShareAchivement)
    Button btnShareAchivement;
    @BindView(R.id.txtdoneAchivement)
    TextView txtdoneAchivement;

    int achievementsImage;
    int achievementsflag;
    String achievementsTitle;

    static AchievementsDetailsActivity achievementsDetailsActivity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_achievements_detail);
        ButterKnife.bind(this);
        setContents();
    }

    public void setContents() {
        achievementsDetailsActivity = this;

        Intent mIntent = getIntent();
        int intValue = mIntent.getIntExtra("achievementsDetailsActivity", 0);

        if (intValue == 0) {
            achievementsImage = mIntent.getIntExtra("achievementsImage", 0);
            achievementsflag = mIntent.getIntExtra("achievementsflag", 0);
            achievementsTitle = mIntent.getStringExtra("achievementsTitle");
            imageGetAchivement.setBackgroundResource(achievementsImage);
            txtGetAchivement.setText(achievementsTitle.toString());

            if (achievementsflag == 1) {
                txtStaticAchivement.setText(R.string.achivedetail_txt1);
            } else if (achievementsflag == 2) {
                txtStaticAchivement.setText(R.string.achivedetail_txt2);
            } else if (achievementsflag == 3) {
                txtStaticAchivement.setText(R.string.achivedetail_txt3);
            }
        }

    }

    @OnClick({R.id.btnShareAchivement, R.id.txtdoneAchivement, R.id.image_back_achiv_detail})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.btnShareAchivement:
                shareContent(achievementsTitle);
                break;
            case R.id.txtdoneAchivement:
                finish();
                break;
            case R.id.image_back_achiv_detail:
                finish();
                break;
        }
    }

    public static AchievementsDetailsActivity getInstance() {
        return achievementsDetailsActivity;
    }

    public void shareContent(String achievementsTitle) {
        Intent sendIntent = new Intent();
        sendIntent.setAction(Intent.ACTION_SEND);
        sendIntent.putExtra(Intent.EXTRA_TEXT,
                "I just earned a new achievement in LDS Scriptures Pro: " + achievementsTitle + "\nhttps://ldsscriptures.com");
        sendIntent.setType("text/plain");
        startActivity(sendIntent);
    }
}
