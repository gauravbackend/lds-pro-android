package com.ldsscriptures.pro.activity.studytools;

import android.content.Intent;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.Toolbar;
import android.text.SpannableStringBuilder;
import android.text.TextPaint;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.ldsscriptures.pro.R;
import com.ldsscriptures.pro.activity.BaseActivity;
import com.ldsscriptures.pro.activity.MainActivity;
import com.ldsscriptures.pro.activity.ReadingActivity;
import com.ldsscriptures.pro.database.CitationsDatabaseHelper;
import com.ldsscriptures.pro.model.StudyDailyVerse;
import com.ldsscriptures.pro.model.TimeLine.VerseDataTimeLineModel;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


public class StudyRandomActivity extends BaseActivity {

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.tvTitle)
    TextView tvTitle;

    @BindView(R.id.txt_study_daily_verse)
    TextView txtStudyDailyVerse;

    @BindView(R.id.txt_study_daily_verse_id)
    TextView txtStudyDailyVerseId;

    @BindView(R.id.txt_string_path)
    TextView txtStringPath;

    @BindView(R.id.layout_set_verse)
    LinearLayout layoutSetVerse;

    @BindView(R.id.txt_study_daily_no_verse)
    TextView txtStudyDailyNoVerse;

    @BindView(R.id.layout_set_no_verse)
    LinearLayout layoutSetNoVerse;

    @BindView(R.id.rendomfab)
    FloatingActionButton rendomfab;
    @BindView(R.id.ivMenu)
    ImageView ivMenu;
    @BindView(R.id.ivBack)
    ImageView ivBack;
    @BindView(R.id.ivSearch)
    ImageView ivSearch;
    @BindView(R.id.ivTabs)
    ImageView ivTabs;
    @BindView(R.id.ivMore)
    ImageView ivMore;

    private ArrayList<StudyDailyVerse> studyDailyArrayList = new ArrayList<>();
    VerseDataTimeLineModel verseDataTimeLineModel;
    private CitationsDatabaseHelper citationsDatabaseHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_studyrandom);
        ButterKnife.bind(this);
        setContents();
    }

    private void setContents() {
        showText(tvTitle, getResources().getString(R.string.RandomVerse));
        setToolbar(toolbar);
        setVisibilityGone(ivMore);
        setVisibilityGone(ivTabs);
        setVisibilityGone(ivSearch);
        citationsDatabaseHelper = new CitationsDatabaseHelper(StudyRandomActivity.this);
        new GetSpecificContent().execute();
    }

    private class GetSpecificContent extends AsyncTask<String, Void, String> {

        @Override
        protected void onPreExecute() {
            showprogress();
        }

        @Override
        protected String doInBackground(String... params) {
            try {
                Cursor cursor;
                cursor = citationsDatabaseHelper.getSpecificContent();
                studyDailyArrayList.clear();

                if (cursor != null && cursor.moveToFirst()) {
                    while (!cursor.isAfterLast()) {

                        StudyDailyVerse studyDailyVerse = new StudyDailyVerse();
                        studyDailyVerse.setUri(cursor.getString(cursor.getColumnIndex("ref_uri")));
                        studyDailyVerse.setShowDate("");
                        studyDailyVerse.setVerseID(cursor.getString(cursor.getColumnIndex("ref_verse")));
                        studyDailyArrayList.add(studyDailyVerse);
                        cursor.moveToNext();
                    }
                }
                citationsDatabaseHelper.close();
                if (cursor != null) {
                    cursor.close();
                }
            } catch (Exception e) {

            }
            return null;
        }

        @Override
        protected void onPostExecute(String result) {

            try {
                for (int i = 0; i < studyDailyArrayList.size(); i++) {

                    String getVerseText = studyDailyArrayList.get(i).getUri() + "." + studyDailyArrayList.get(i).getVerseID();
                    String[] fn = getVerseText.split("\\.");

                    if (fn[1].trim().contains("title")) {
                        layoutSetVerse.setVisibility(View.GONE);
                        layoutSetNoVerse.setVisibility(View.VISIBLE);
                    } else {

                        verseDataTimeLineModel = new VerseDataTimeLineModel();

                        String verseId = fn[1].trim();
                        if (verseId.contains("-")) {
                            String verseNoArr[] = verseId.split("-");
                            if (verseNoArr.length != 0)
                                verseId = verseNoArr[0];
                        }

                        verseDataTimeLineModel = getVerseText(fn[0], verseId);
                        String verseText = verseDataTimeLineModel.getVereseText();
                        String versepath = verseDataTimeLineModel.getVerseFullPath();
                        final String verseLcId = verseDataTimeLineModel.getLcId();
                        final String verseLsId = verseDataTimeLineModel.getLsId();
                        final String indexForReading = verseDataTimeLineModel.getIndexForReading();
                        final String verseAid = verseDataTimeLineModel.getVerseAid();

                        if (verseText != null || verseText.length() != 0) {
                            layoutSetVerse.setVisibility(View.VISIBLE);
                            layoutSetNoVerse.setVisibility(View.GONE);

                            txtStudyDailyVerse.setText("");
                            txtStudyDailyVerse.setText(verseText);


                            if (versepath != null || versepath.length() != 0) {
                                txtStudyDailyVerseId.setText(verseId.trim());
                                versepath = versepath + ":" + verseId;

                                int index = versepath.lastIndexOf("/");
                                String imgName = versepath.substring(0, index) + "/";
                                String substring2 = versepath.substring(index + 1, versepath.length());
                                SpannableStringBuilder spanTxt = new SpannableStringBuilder(imgName);
                                spanTxt.append(substring2);

                                spanTxt.setSpan(new MyClickableSpan(verseLcId, verseLsId, indexForReading, verseAid, verseId), spanTxt.length() - substring2.length(), spanTxt.length(), 0);
                                txtStringPath.setMovementMethod(LinkMovementMethod.getInstance());
                                txtStringPath.setText(spanTxt, TextView.BufferType.SPANNABLE);
                                txtStringPath.setHighlightColor(getResources().getColor(android.R.color.transparent));

                            }

                            final String finalverseAid = verseAid;
                            final String finalVerseId = verseId;
                            txtStudyDailyVerse.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    redirectReadingActivity(verseLcId, verseLsId, indexForReading, finalverseAid, finalVerseId);
                                }
                            });

                        } else {
                            layoutSetVerse.setVisibility(View.GONE);
                            layoutSetNoVerse.setVisibility(View.VISIBLE);
                        }
                    }
                }
            } catch (Exception e) {

            }
            dismissprogress();

        }
    }

    @OnClick(R.id.ivMenu)
    public void openMenu() {
        openSlideMenu();
    }

    @OnClick(R.id.rendomfab)
    public void onViewClicked() {
        setContents();
    }

    private class MyClickableSpan extends ClickableSpan {

        String verseLcId;
        String verseLsId;
        String indexForReading;
        String verseAid;
        String verseId;

        public MyClickableSpan(String verseLcId, String verseLsId, String indexForReading, String verseAid, String verseId) {
            super();
            this.verseLcId = verseLcId;
            this.verseLsId = verseLsId;
            this.indexForReading = indexForReading;
            this.verseAid = verseAid;
            this.verseId = verseId;
        }

        public void onClick(View widget) {

            redirectReadingActivity(verseLcId, verseLsId, indexForReading, verseAid, verseId);
            TextView tv = (TextView) widget;
            tv.setHighlightColor(getResources().getColor(android.R.color.transparent));
        }

        public void updateDrawState(TextPaint ds) {
            ds.setUnderlineText(false);
            ds.setColor(getResources().getColor(R.color.sand));
        }
    }

    public void redirectReadingActivity(String verseLcId, String verseLsId, String indexForReading, String verseAid, String verseId) {
        if (verseLcId != null || verseLsId != null || indexForReading != null) {

            Intent intent = new Intent(getApplicationContext(), ReadingActivity.class);
            intent.putExtra(getString(R.string.indexForReading), Integer.parseInt(indexForReading));
            intent.putExtra(getString(R.string.isFromIndex), true);
            intent.putExtra(getString(R.string.is_history), false);
            intent.putExtra(getString(R.string.lcID), verseLcId);
            intent.putExtra(getString(R.string.lsId), verseLsId);
            intent.putExtra(getString(R.string.verseAId), Integer.parseInt(verseAid));
            intent.putExtra(getString(R.string.verseId), Integer.parseInt(verseId));
            startActivity(intent);
            overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menuBack:
                finish();
                break;
            case R.id.menuNext:
                Toast.makeText(getApplicationContext(), "No Item Found", Toast.LENGTH_LONG).show();
                break;
            case R.id.menuBookmark:
                Intent bookmarkIntent = new Intent(getApplicationContext(), MainActivity.class);
                bookmarkIntent.putExtra(getString(R.string.menuSelection), "Bookmarks");
                bookmarkIntent.putExtra(getString(R.string.isFromReading), true);
                startActivityWithAnimation(bookmarkIntent);
                break;
            case R.id.menuHistory:
                Intent historyIntent = new Intent(getApplicationContext(), MainActivity.class);
                historyIntent.putExtra(getString(R.string.menuSelection), "History");
                historyIntent.putExtra(getString(R.string.isFromReading), true);
                startActivityWithAnimation(historyIntent);
                break;
        }
        return super.onOptionsItemSelected(item);
    }

}
