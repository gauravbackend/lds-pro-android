package com.ldsscriptures.pro.activity.settingmenu;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.TextView;

import com.ldsscriptures.pro.R;
import com.ldsscriptures.pro.activity.BaseActivity;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


public class SettingsSupportActivity extends BaseActivity {


    @BindView(R.id.ivBack)
    ImageView ivBack;
    @BindView(R.id.ivMenu)
    ImageView ivMenu;
    @BindView(R.id.tvTitle)
    TextView tvTitle;
    @BindView(R.id.ivDropDownMenu)
    ImageView ivDropDownMenu;
    @BindView(R.id.ivMore)
    ImageView ivMore;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.webviewSupport)
    WebView webviewSupport;
    private ProgressDialog pd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting_support);
        ButterKnife.bind(this);
        setContent();
    }

    private void setContent() {
        showText(tvTitle, getResources().getString(R.string.support));
        setVisibilityGone(ivMore);
        setVisibilityGone(ivDropDownMenu);
        setVisibilityGone(ivMenu);
        setVisibilityVisible(ivBack);

        // this load page url in webView
        pd = new ProgressDialog(SettingsSupportActivity.this);
        pd.setMessage("loading...");
        pd.show();
        webviewSupport.setWebViewClient(new SettingsSupportActivity.MyWebViewClient());
        webviewSupport.getSettings().setJavaScriptEnabled(true); // enable javascript
        webviewSupport.loadUrl(getString(R.string.supportLink) + "/?m=0");
    }

    @OnClick(R.id.ivBack)
    public void onViewClicked() {
        finish();
    }

    public class MyWebViewClient extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            view.loadUrl(url);
            if (!pd.isShowing()) {
                pd.show();
            }
            return true;
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            if (pd.isShowing()) {
                pd.dismiss();
            }
        }

        @Override
        public void onReceivedError(WebView view, WebResourceRequest request, WebResourceError error) {
            super.onReceivedError(view, request, error);
            if (pd.isShowing()) {
                pd.dismiss();
            }
        }
    }
}
