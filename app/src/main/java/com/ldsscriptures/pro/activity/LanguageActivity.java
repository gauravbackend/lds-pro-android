package com.ldsscriptures.pro.activity;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.ldsscriptures.pro.R;
import com.ldsscriptures.pro.activity.settingmenu.SettingsActivity;
import com.ldsscriptures.pro.adapter.LanguageAdapter;
import com.ldsscriptures.pro.dbClass.GetLibCollectionFromDB;
import com.ldsscriptures.pro.model.Language;
import com.ldsscriptures.pro.utils.Global;
import com.ldsscriptures.pro.utils.RetriveMainDatabse;
import com.ldsscriptures.pro.utils.SessionManager;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.ldsscriptures.pro.adapter.LanguageAdapter.langCode;
import static com.ldsscriptures.pro.adapter.LanguageAdapter.langCollectionCode;
import static com.ldsscriptures.pro.adapter.LanguageAdapter.langId;

public class LanguageActivity extends BaseActivity {

    @BindView(R.id.ivBack)
    ImageView ivBack;

    @BindView(R.id.tvTitle)
    TextView tvTitle;

    @BindView(R.id.ivSelect)
    ImageView ivSelect;

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.tvNoData)
    TextView tvNoData;

    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;

    private String currentLanguageID = "";
    private SessionManager sessionManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_language);
        ButterKnife.bind(this);
        setContents();
    }

    private void setContents() {
        ButterKnife.bind(this);
        sessionManager = new SessionManager(LanguageActivity.this);
        currentLanguageID = sessionManager.getLanguageID();

        setToolbar(toolbar);

        showText(tvTitle, getString(R.string.language));
        setVisibilityVisible(ivBack);

        setRecyclerView();
    }

    private void setRecyclerView() {

        ArrayList<Language> langArrayList = new GetLibCollectionFromDB(LanguageActivity.this).getLanguageListFromDB();
        if (langArrayList.size() != 0) {

            setVisibilityGone(tvNoData);
            setVisibilityVisible(recyclerView);
            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(LanguageActivity.this);
            recyclerView.setLayoutManager(mLayoutManager);

            LanguageAdapter languageAdapter = new LanguageAdapter(LanguageActivity.this, langArrayList);
            recyclerView.setAdapter(languageAdapter);

        } else {
            setVisibilityGone(recyclerView);
            setVisibilityVisible(tvNoData);
        }
    }

    public void finishActivity() {
        finish();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @OnClick({R.id.ivBack, R.id.ivSelect})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.ivBack:
                onBackPressed();
                break;
            case R.id.ivSelect:

                if (!langId.isEmpty() && !langId.equalsIgnoreCase(currentLanguageID)) {

                    currentLanguageID = langId;

                    sessionManager.setLanguageID(langId);
                    sessionManager.setLanguageChanged(true);

                    if (!langCode.isEmpty())
                        sessionManager.setLanguageCode(langCode);

                    if (!langCollectionCode.isEmpty())
                        sessionManager.setLangCollID(langCollectionCode);


                    if (Global.isNetworkAvailable(LanguageActivity.this)) {
                        Global.LANGUAGE = langCode;
                        new RetriveMainDatabse(LanguageActivity.this, "");
                    } else {
                        Global.showNetworkAlert(this);
                    }

                    if (SettingsActivity.getInstance() != null) {
                        SettingsActivity.getInstance().setLanguageLabel();
                    }
                    finishActivity();
                }
                break;
        }
    }

}
