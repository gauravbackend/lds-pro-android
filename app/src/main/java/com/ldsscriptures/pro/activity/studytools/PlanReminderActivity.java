package com.ldsscriptures.pro.activity.studytools;

import android.app.TimePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.ldsscriptures.pro.R;
import com.ldsscriptures.pro.activity.BaseActivity;
import com.ldsscriptures.pro.model.PlanReminderData;
import com.ldsscriptures.pro.model.PlanReminderMainData;
import com.ldsscriptures.pro.utils.SessionManager;

import java.util.ArrayList;
import java.util.Calendar;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by IBL InfoTech on 8/5/2017.
 */

public class PlanReminderActivity extends BaseActivity implements CompoundButton.OnCheckedChangeListener {

    @BindView(R.id.ivMenu)
    ImageView ivMenu;
    @BindView(R.id.ivBack)
    ImageView ivBack;
    @BindView(R.id.tvTitle)
    TextView tvTitle;
    @BindView(R.id.ivSearch)
    ImageView ivSearch;
    @BindView(R.id.ivMore)
    ImageView ivMore;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.btnLayoutWeekly)
    LinearLayout btnLayoutWeekly;
    @BindView(R.id.switch_everyday)
    Switch switchEveryday;
    @BindView(R.id.set_button_cancle)
    Button setButtonCancle;
    @BindView(R.id.set_button_save)
    Button setButtonSave;
    @BindView(R.id.txtSelectTime)
    TextView txtSelectTime;

    static PlanReminderActivity planReminderActivity;
    boolean dayValueFlag = false;
    boolean var_dayValueFlag = false;
    String var_Plan_id, var_select_Time = "";
    @BindView(R.id.ivTabs)
    ImageView ivTabs;
    private ArrayList<PlanReminderData> planReminderDataArrayList = new ArrayList<>();
    private ArrayList<PlanReminderMainData> planReminderMainDataArrayList = new ArrayList<>();
    SessionManager sessionManager;
    int var_selectedHour, var_selectedMinute;
    Button[] btn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_plan_details_dialog);
        ButterKnife.bind(this);
        setContents();
    }

    public void setContents() {
        planReminderActivity = this;
        ivMenu.setVisibility(View.GONE);
        ivBack.setVisibility(View.VISIBLE);
        setVisibilityGone(ivTabs);
        setVisibilityGone(ivSearch);
        showText(tvTitle, getResources().getString(R.string.reminders));
        switchEveryday.setOnCheckedChangeListener(this);
        sessionManager = new SessionManager(PlanReminderActivity.this);
        Intent intent = getIntent();
        var_Plan_id = intent.getStringExtra("var_Plan_id");

        planReminderDataArrayList.add(new PlanReminderData("1", "M", "Mo", false));
        planReminderDataArrayList.add(new PlanReminderData("2", "T", "Tu", false));
        planReminderDataArrayList.add(new PlanReminderData("3", "W", "We", false));
        planReminderDataArrayList.add(new PlanReminderData("4", "T", "Th", false));
        planReminderDataArrayList.add(new PlanReminderData("5", "F", "Fr", false));
        planReminderDataArrayList.add(new PlanReminderData("6", "S", "Sa", false));
        planReminderDataArrayList.add(new PlanReminderData("7", "S", "Su", false));

        LinearLayout.LayoutParams param = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT, 1.0f);
        param.setMargins(5, 0, 5, 0);
        btn = new Button[planReminderDataArrayList.size()];
        for (int i = 0; i < planReminderDataArrayList.size(); i++) {
            btn[i] = new Button(getApplicationContext());
            btn[i].setText(planReminderDataArrayList.get(i).getPlanReminderText());
            btn[i].setTextColor(getResources().getColor(R.color.textColor));
            btn[i].setBackgroundResource(0);
            btn[i].setTextSize(16);
            btn[i].setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT));
            btn[i].setLayoutParams(param);
            btn[i].setPadding(0, 0, 0, 0);
            btnLayoutWeekly.addView(btn[i]);
            final int btnReminderFlag = i;

            btn[i].setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Boolean reminderFlag = planReminderDataArrayList.get(btnReminderFlag).getPlanReminderFlag();
                    if (!reminderFlag) {
                        v.setBackgroundResource(R.drawable.selectedroundedbutton);
                        planReminderDataArrayList.get(btnReminderFlag).setPlanReminderFlag(true);
                        btn[btnReminderFlag].setTextColor(getResources().getColor(R.color.textColorWhite));
                    } else {
                        v.setBackgroundResource(0);
                        planReminderDataArrayList.get(btnReminderFlag).setPlanReminderFlag(false);
                        btn[btnReminderFlag].setTextColor(getResources().getColor(R.color.textColor));
                    }
                }
            });
        }
    }

    public static PlanReminderActivity getInstance() {
        return planReminderActivity;
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        if (isChecked) {
            btnLayoutWeekly.setVisibility(View.GONE);
            dayValueFlag = true;
        } else {
            btnLayoutWeekly.setVisibility(View.VISIBLE);
            dayValueFlag = false;
        }
    }

    @OnClick({R.id.ivBack, R.id.set_button_cancle, R.id.set_button_save, R.id.txtSelectTime})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.ivBack:
                finish();
                break;
            case R.id.set_button_cancle:
                finish();
                break;
            case R.id.set_button_save:
                saveReminder();
                break;
            case R.id.txtSelectTime:
                openTimePikerDialog();
                break;
        }
    }

    public void openTimePikerDialog() {

        Calendar mcurrentTime = Calendar.getInstance();
        int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
        int minute = mcurrentTime.get(Calendar.MINUTE);
        TimePickerDialog mTimePicker;
        mTimePicker = new TimePickerDialog(PlanReminderActivity.this, new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {

                String status = "AM";
                int hour_of_12_hour_format;
                if (selectedHour > 11) {
                    status = "PM";
                }
                if (selectedHour > 11) {
                    hour_of_12_hour_format = selectedHour - 12;
                } else {
                    hour_of_12_hour_format = selectedHour;
                }

                var_select_Time = hour_of_12_hour_format + ":" + selectedMinute + "  " + status;
                var_selectedHour = hour_of_12_hour_format;
                var_selectedMinute = selectedMinute;
                txtSelectTime.setText(var_select_Time);
            }
        }, hour, minute, true);//Yes 24 hour time
        mTimePicker.show();
    }

    public void saveReminder() {

        for (int i = 0; i < planReminderDataArrayList.size(); i++) {
            if (planReminderDataArrayList.get(i).getPlanReminderFlag().equals(true)) {
                var_dayValueFlag = true;
                break;
            }
        }

        if (!var_select_Time.equals("")) {
            if (dayValueFlag == false && var_dayValueFlag == false) {
                Toast.makeText(getApplicationContext(), "Please Select Days", Toast.LENGTH_LONG).show();
            } else {
                reminderData();
            }
        } else {
            Toast.makeText(getApplicationContext(), "Please Select Time", Toast.LENGTH_LONG).show();
        }
    }

    public void reminderData() {

        planReminderMainDataArrayList = sessionManager.getPlanReminderData();

        if (planReminderMainDataArrayList != null) {
            for (int i = 0; i < planReminderMainDataArrayList.size(); i++) {
                String Plan_id = planReminderMainDataArrayList.get(i).getPlanID().toString();
                if (Plan_id.equals(var_Plan_id)) {
                    planReminderMainDataArrayList.remove(i);
                }
            }
            setPlanReminderDataArrayList();
        } else {
            planReminderMainDataArrayList = new ArrayList<>();
            setPlanReminderDataArrayList();
        }
    }

    public void setPlanReminderDataArrayList() {
        if (dayValueFlag) {

            ArrayList<PlanReminderData> planReminderDataArrayList = new ArrayList<>();
            planReminderDataArrayList.add(new PlanReminderData("1", "M", "Mo", true));
            planReminderDataArrayList.add(new PlanReminderData("2", "T", "Tu", true));
            planReminderDataArrayList.add(new PlanReminderData("3", "W", "We", true));
            planReminderDataArrayList.add(new PlanReminderData("4", "T", "Th", true));
            planReminderDataArrayList.add(new PlanReminderData("5", "F", "Fr", true));
            planReminderDataArrayList.add(new PlanReminderData("6", "S", "Sa", true));
            planReminderDataArrayList.add(new PlanReminderData("7", "S", "Su", true));


            planReminderMainDataArrayList.add(new PlanReminderMainData(var_Plan_id, var_select_Time.toString(), var_selectedHour, var_selectedMinute, dayValueFlag, planReminderDataArrayList));
            sessionManager.setPlanReminderData(planReminderMainDataArrayList);

        } else {

            planReminderMainDataArrayList.add(new PlanReminderMainData(var_Plan_id, var_select_Time.toString(), var_selectedHour, var_selectedMinute, dayValueFlag, planReminderDataArrayList));
            sessionManager.setPlanReminderData(planReminderMainDataArrayList);
        }
        finish();
    }
}
