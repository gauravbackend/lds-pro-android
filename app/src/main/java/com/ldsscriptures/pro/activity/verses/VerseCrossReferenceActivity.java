package com.ldsscriptures.pro.activity.verses;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.InputType;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.ldsscriptures.pro.R;
import com.ldsscriptures.pro.activity.BaseActivity;
import com.ldsscriptures.pro.adapter.CrossReferenceLcListAdapter;
import com.ldsscriptures.pro.adapter.CrossReferenceLibIndexAdapter;
import com.ldsscriptures.pro.adapter.CrossReferenceLibSubIndexAdapter;
import com.ldsscriptures.pro.adapter.CrossReferenceLsListAdapter;
import com.ldsscriptures.pro.adapter.CrossReferenceReadingHtmlDataAdapter;
import com.ldsscriptures.pro.dbClass.GetDataFromDatabase;
import com.ldsscriptures.pro.dbClass.GetLibCollectionFromDB;
import com.ldsscriptures.pro.model.Item;
import com.ldsscriptures.pro.model.LcData;
import com.ldsscriptures.pro.model.LsData;
import com.ldsscriptures.pro.model.LsFullIndexData;
import com.ldsscriptures.pro.model.LsIndexPreviewData;
import com.ldsscriptures.pro.model.LsMainIndexData;
import com.ldsscriptures.pro.model.VerseCrossReferenceData;
import com.ldsscriptures.pro.model.VerseData;
import com.ldsscriptures.pro.utils.DownloadSubDBZipFile;
import com.ldsscriptures.pro.utils.Global;
import com.ldsscriptures.pro.utils.PushToServerData;
import com.ldsscriptures.pro.utils.SessionManager;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


public class VerseCrossReferenceActivity extends BaseActivity {


    @BindView(R.id.tvVerseId)
    TextView tvVerseId;

    @BindView(R.id.tvVerseName)
    TextView tvVerseName;

    @BindView(R.id.tvTitle)
    TextView tvTitle;

    @BindView(R.id.linLibIndex)
    LinearLayout linLibIndex;

    @BindView(R.id.linLS)
    LinearLayout linLS;

    @BindView(R.id.linLc)
    LinearLayout linLc;

    @BindView(R.id.rvLS)
    RecyclerView rvLS;

    @BindView(R.id.linReadingHtml)
    LinearLayout linReadingHtml;

    @BindView(R.id.rvReadingHtml)
    RecyclerView rvReadingHtml;

    @BindView(R.id.rvLc)
    RecyclerView rvLc;

    @BindView(R.id.rvLibIndex)
    RecyclerView rvLibIndex;

    @BindView(R.id.linLibSubIndex)
    LinearLayout linLibSubIndex;

    @BindView(R.id.rvLibSubIndex)
    RecyclerView rvLibSubIndex;

    @BindView(R.id.txtLibIndexBack)
    TextView txtLibIndexBack;

    @BindView(R.id.txtReadingHtmlDone)
    TextView txtReadingHtmlDone;

    // set the navigation title value
    @BindView(R.id.txtNavPathLibIndex)
    TextView txtNavPathLibIndex;

    // set the navigation title value
    @BindView(R.id.txtNavPathSubIndex)
    TextView txtNavPathSubIndex;

    @BindView(R.id.txtNavPathReadingHtml)
    TextView txtNavPathReadingHtml;

    private ArrayList<LsData> lsDataArrayList = new ArrayList<>();
    private ArrayList<LsMainIndexData> combineIndexDataArray = new ArrayList<>();
    private ArrayList<LsIndexPreviewData> newIndexArray = new ArrayList<>();
    ArrayList<VerseCrossReferenceData> verseCrossReferenceDataArrayList = new ArrayList<>();
    private LinearLayoutManager mLayoutManager;

    static VerseCrossReferenceActivity verseCrossReferenceActivity;
    private String currentRefTitle = "";
    private String title = "";
    private String verseAId = "";
    private SessionManager sessionManager;
    private String verseId;
    private String verseText;
    private ArrayList<Item> dbCatelogArraylist = new ArrayList<>();

    public static VerseCrossReferenceActivity getInstance() {
        return verseCrossReferenceActivity;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_verse_cross_reference);
        setContents();
    }

    private void setContents() {
        ButterKnife.bind(this);

        verseCrossReferenceActivity = this;
        sessionManager = new SessionManager(VerseCrossReferenceActivity.this);

        tvTitle.setInputType(InputType.TYPE_TEXT_FLAG_CAP_SENTENCES);
        showText(tvTitle, getString(R.string.cross_reference));

        title = getIntent().getStringExtra("title");
        verseId = getIntent().getStringExtra(getString(R.string.verseId));
        verseAId = getIntent().getStringExtra(getString(R.string.verseAId));
        verseText = getIntent().getStringExtra(getString(R.string.verseText));

        tvVerseId.setText(verseId);
        tvVerseName.setText(verseText);

        mLayoutManager = new LinearLayoutManager(VerseCrossReferenceActivity.this);
        dbCatelogArraylist = Global.getListSharedPreferneces(this);
        setLcList();
    }

    private void setLcList() {
        linLc.setVisibility(View.VISIBLE);
        ArrayList<LcData> lcDataArrayList = new GetLibCollectionFromDB(VerseCrossReferenceActivity.this).getLcArrayListFromDB();
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(VerseCrossReferenceActivity.this);
        rvLc.setLayoutManager(mLayoutManager);
        if (lcDataArrayList != null)
            rvLc.setAdapter(new CrossReferenceLcListAdapter(VerseCrossReferenceActivity.this, lcDataArrayList, "0"));
    }

    public void setLsData(String LcID) {

        linLS.setVisibility(View.VISIBLE);
        lsDataArrayList = new GetLibCollectionFromDB(VerseCrossReferenceActivity.this).getLsDataFromDb(LcID);
        rvLS.setLayoutManager(mLayoutManager);
        if (lsDataArrayList != null)
            rvLS.setAdapter(new CrossReferenceLsListAdapter(VerseCrossReferenceActivity.this, lsDataArrayList, "0"));
    }

    public void setLibraryIndex(String LsId, String navPath) {

        if (dbCatelogArraylist != null) {
            for (int i = 0; i < dbCatelogArraylist.size(); i++) {
                if (dbCatelogArraylist.get(i).getId().equals(LsId)) {
                    if (!isFolderExists(dbCatelogArraylist.get(i).getId())) {
                        new DownloadFileFromURL().execute(dbCatelogArraylist.get(i).getUrl(), dbCatelogArraylist.get(i).getId(), navPath);
                    } else {
                        linLibIndex.setVisibility(View.VISIBLE);
                        txtNavPathLibIndex.setText(navPath);
                        ArrayList<LsFullIndexData> lsFullIndexDataArray = new GetDataFromDatabase(VerseCrossReferenceActivity.this, LsId).GetMainDataFromDB();
                        combineIndexDataArray = new ArrayList<>();
                        if (lsFullIndexDataArray != null)
                            for (int i1 = 0; i1 < lsFullIndexDataArray.size(); i1++) {
                                combineIndexDataArray.addAll(lsFullIndexDataArray.get(i1).getItemMainIndexArray());
                            }
                        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(VerseCrossReferenceActivity.this);
                        rvLibIndex.setLayoutManager(mLayoutManager);
                        if (combineIndexDataArray != null)
                            rvLibIndex.setAdapter(new CrossReferenceLibIndexAdapter(VerseCrossReferenceActivity.this, combineIndexDataArray, LsId, "0"));
                    }
                }
            }
        }
    }

    public void setLibSubIndex(int position, int clickedId, String navPath, String lsId) {
        linLibSubIndex.setVisibility(View.VISIBLE);

        txtNavPathSubIndex.setText(txtNavPathLibIndex.getText().toString() + " / " + navPath);

        ArrayList<LsIndexPreviewData> indexArray = new ArrayList<>();

        for (int i = 0; i < combineIndexDataArray.get(position).getLsSubIndexDatas().size(); i++) {
            if (clickedId == combineIndexDataArray.get(position).getLsSubIndexDatas().get(i).getId()) {
                indexArray = combineIndexDataArray.get(position).getLsSubIndexDatas().get(i).getLsIndexPreviewDatas();
            }
        }

        newIndexArray = new ArrayList<>();
        for (int i = 0; i < indexArray.size(); i++) {
            if (indexArray.get(i).getNav_section_id().equals(String.valueOf(clickedId))) {
                newIndexArray.add(indexArray.get(i));
            }
        }

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(VerseCrossReferenceActivity.this);
        rvLibSubIndex.setLayoutManager(mLayoutManager);
        if (newIndexArray != null)
            rvLibSubIndex.setAdapter(new CrossReferenceLibSubIndexAdapter(VerseCrossReferenceActivity.this, newIndexArray, lsId, "0"));
    }

    public void setLibraryReadingHtml(int position, String html_title, String lsId) {
        linReadingHtml.setVisibility(View.VISIBLE);

        currentRefTitle = html_title;

        txtNavPathReadingHtml.setText(txtNavPathSubIndex.getText().toString() + " / " + html_title);

        String uri = newIndexArray.get(position).getUri();
        int indexID = newIndexArray.get(position).getId();

        ArrayList<VerseData> stringArrayList = new GetDataFromDatabase(VerseCrossReferenceActivity.this, lsId).getCrossRefSubItemHtmlData(indexID, uri);

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(VerseCrossReferenceActivity.this);
        rvReadingHtml.setLayoutManager(mLayoutManager);
        if (stringArrayList != null)
            rvReadingHtml.setAdapter(new CrossReferenceReadingHtmlDataAdapter(VerseCrossReferenceActivity.this, stringArrayList, html_title, verseAId, uri, "0"));
    }

    public void visibleDone(boolean isVisible) {
        if (isVisible) {
            txtReadingHtmlDone.setVisibility(View.VISIBLE);
        } else {
            txtReadingHtmlDone.setVisibility(View.GONE);
        }
    }

    @OnClick(R.id.txtReadingHtmlDone)
    public void storeCrossRefDone() {

        verseCrossReferenceDataArrayList = sessionManager.getPreferencesCrossRefArrayList(VerseCrossReferenceActivity.this);

        if (CrossReferenceReadingHtmlDataAdapter.getInstance() != null) {
            ArrayList<VerseData> verseDataArrayList = new ArrayList<>();
            verseDataArrayList = CrossReferenceReadingHtmlDataAdapter.getInstance().selectedReferenceArray();
            String verseNumber = "";
            for (int i = 0; i < verseDataArrayList.size(); i++) {
                if (verseDataArrayList.get(i).isSelected()) {
                    verseNumber = verseNumber + verseDataArrayList.get(i).getId() + ", ";
                }
            }

            String[] separated = verseNumber.split(",");
            ArrayList<Integer> list = convert(separated);

            int start = 0;
            List<String> ranges = new ArrayList<>();
            for (int i = 1; i < list.size(); i++) {

                if (list.get(i - 1) + 1 != list.get(i)) {
                    if (list.get(start) == list.get(i - 1)) {
                        ranges.add("" + list.get(start));
                    } else {
                        ranges.add(list.get(start) + "-" + list.get(i - 1));
                    }
                    start = i;
                }
            }

            if (list.get(start) == list.get(list.size() - 1)) {
                ranges.add("" + list.get(start));
            } else {
                ranges.add(list.get(start) + "-" + list.get(list.size() - 1));
            }

            if (!verseNumber.equals("")) {

                for (int i = 0; i < verseDataArrayList.size(); i++) {

                    if (verseDataArrayList.get(i).isSelected()) {

                        VerseCrossReferenceData verseCrossReferenceData = new VerseCrossReferenceData();

                        verseCrossReferenceData.setRefTitle1(Global.bookmarkData.getItemuri());
                        verseCrossReferenceData.setRefIndex1(verseId);
                        verseCrossReferenceData.setRefAid1(verseAId);
                        verseCrossReferenceData.setRefTitle2(verseDataArrayList.get(i).getUri());
                        verseCrossReferenceData.setRefIndex2(ranges.toString().trim().replace("[", "").replace("]", ""));
                        verseCrossReferenceData.setRefAid2(verseDataArrayList.get(i).getData_aid());
                        verseCrossReferenceData.setServerId("0");
                        verseCrossReferenceData.setCrosstitle(title + ":" + verseId);
                        verseCrossReferenceData.setLink_title(currentRefTitle + ":" + ranges.toString().trim().replace("[", "").replace("]", ""));

                        if (verseCrossReferenceDataArrayList != null) {
                            verseCrossReferenceDataArrayList.add(verseCrossReferenceData);
                        } else {
                            verseCrossReferenceDataArrayList = new ArrayList<>();
                            verseCrossReferenceDataArrayList.add(verseCrossReferenceData);
                        }

                        sessionManager.setPreferencesCrossRefArrayList(VerseCrossReferenceActivity.this, verseCrossReferenceDataArrayList);
                        break;
                    }
                }
                new PushToServerData(this);
                finish();
                if (VerseHighlightActivity.getInstance() != null) {
                    VerseHighlightActivity.getInstance().goToReadingView();
                }
            }
        }


    }

    @OnClick(R.id.txtLSBack)
    public void mangeLSBack() {
        linLS.setVisibility(View.GONE);
    }

    @OnClick(R.id.txtLibIndexBack)
    public void mangeLibIndexBack() {
        linLibIndex.setVisibility(View.GONE);
    }

    @OnClick(R.id.txtSubIndexBack)
    public void mangeSubIndexBack() {
        linLibSubIndex.setVisibility(View.GONE);
    }

    @OnClick(R.id.txtReadingHtmlBack)
    public void mangeReadingHtmlBack() {
        linReadingHtml.setVisibility(View.GONE);
    }

    @OnClick(R.id.ivBack)
    public void finishActivity() {
        finish();
    }

    private boolean isFolderExists(String id) {
        File destDir = new File(Global.AppFolderPath + id);

        if (!destDir.exists()) {
            return false;
        } else {
            if (destDir.isDirectory()) {
                String[] files = destDir.list();
                if (files == null)
                    return false;

                if (files.length == 0) {
                    //directory is empty
                    return false;
                } else
                    return true;
            }
        }
        return false;
    }

    private ArrayList<Integer> convert(String[] string) {
        ArrayList<Integer> number = new ArrayList<Integer>();
        for (int i = 0; i < string.length; i++) {
            if (string[i].trim().length() != 0) {
                number.add(Integer.parseInt(string[i].trim()));
            }
        }
        return number;
    }

    class DownloadFileFromURL extends AsyncTask<String, String, String> {

        String ItemID = "";
        String navPath = "";

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showprogress();
        }

        @Override
        protected String doInBackground(String... f_url) {
            int count;
            try {

                ItemID = f_url[1];
                navPath = f_url[2];

                new DownloadSubDBZipFile(VerseCrossReferenceActivity.this, f_url[0], ItemID);

                URL url = new URL(f_url[0]);
                URLConnection conection = url.openConnection();
                conection.connect();
                // getting file length
                int lenghtOfFile = conection.getContentLength();

                // input stream to read file - with 8k buffer
                InputStream input = new BufferedInputStream(url.openStream(), 8192);

                // Output stream to write file
                OutputStream output = new FileOutputStream("/sdcard/downloadedfile.jpg");

                byte data[] = new byte[1024];

                long total = 0;

                while ((count = input.read(data)) != -1) {
                    total += count;
                    // publishing the progress....
                    // After this onProgressUpdate will be called
                    publishProgress("" + (int) ((total * 100) / lenghtOfFile));

                    // writing data to file
                    output.write(data, 0, count);
                }

                // flushing output
                output.flush();

                // closing streams
                output.close();
                input.close();

            } catch (Exception e) {
                Log.e("Error: ", e.getMessage());
            }

            return null;
        }

        /**
         * Updating progress bar
         */
        protected void onProgressUpdate(String... progress) {
            // setting progress percentage
            //pDialog.setProgress(Integer.parseInt(progress[0]));
        }

        /**
         * After completing background task
         * Dismiss the progress dialog
         **/
        @Override
        protected void onPostExecute(String file_url) {
            // dismiss the dialog after the file was downloaded
            setLibraryIndex(ItemID, navPath);
            dismissprogress();
        }

    }
}
