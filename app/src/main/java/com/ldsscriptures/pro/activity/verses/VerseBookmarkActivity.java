package com.ldsscriptures.pro.activity.verses;

import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.ldsscriptures.pro.R;
import com.ldsscriptures.pro.activity.BaseActivity;
import com.ldsscriptures.pro.adapter.BookmarkVerseListAdapter;
import com.ldsscriptures.pro.database.CommonDatabaseHelper;
import com.ldsscriptures.pro.model.BookmarkData;
import com.ldsscriptures.pro.utils.EditItemTouchHelperCallback;
import com.ldsscriptures.pro.utils.Global;
import com.ldsscriptures.pro.utils.PushToServerData;
import com.ldsscriptures.pro.utils.SessionManager;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class VerseBookmarkActivity extends BaseActivity {

    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    @BindView(R.id.tvTitle)
    TextView tvTitle;
    @BindView(R.id.tvNoData)
    TextView tvNoData;
    @BindView(R.id.fab)
    FloatingActionButton fab;
    @BindView(R.id.ivBack)
    ImageView ivBack;
    @BindView(R.id.ivMenu)
    ImageView ivMenu;
    @BindView(R.id.ivCheck)
    ImageView ivCheck;
    @BindView(R.id.ivEdit)
    ImageView ivEdit;
    @BindView(R.id.ivMore)
    ImageView ivMore;
    @BindView(R.id.toolbar)
    Toolbar toolbar;

    private String verseText;
    private String verseId;
    private String verseAId;
    private String itemUri;
    private int indexId, versePos;

    private ArrayList<BookmarkData> bookmarkDataArrayList = new ArrayList<>();
    private CommonDatabaseHelper commonDatabaseHelper;
    static VerseBookmarkActivity verseBookmarkActivity;
    BookmarkVerseListAdapter bookmarkVerseListAdapter;
    ArrayList<BookmarkData> savebookmarkDataArrayList;
    SessionManager sessionManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_verse_bookmark);
        ButterKnife.bind(this);
        setContents();
    }

    private void setContents() {
        ButterKnife.bind(this);
        showText(tvTitle, getString(R.string.title_bookmark_verse));
        verseBookmarkActivity = this;
        commonDatabaseHelper = new CommonDatabaseHelper(this);

        verseText = getIntent().getStringExtra(getString(R.string.verseText));
        verseId = getIntent().getStringExtra(getString(R.string.verseId));
        verseAId = getIntent().getStringExtra(getString(R.string.verseAId));
        indexId = getIntent().getIntExtra(getString(R.string.indexId), 0);
        versePos = getIntent().getIntExtra(getString(R.string.versePos), 0);
        itemUri = getIntent().getStringExtra(getString(R.string.itemUri));

        setVisibilityVisible(ivEdit);
        setVisibilityGone(ivCheck);

        sessionManager = new SessionManager(this);
        savebookmarkDataArrayList = new ArrayList<>();
    }

    private void setRecyclerView() {
        if (bookmarkDataArrayList != null) {
            if (bookmarkDataArrayList.size() > 0) {
                String paraUserId = "", paraSessionId = "";

                if (getUserInfo(SessionManager.KEY_USER_ID) != null) {
                    paraUserId = getUserInfo(SessionManager.KEY_USER_ID);
                }
                if (getUserInfo(SessionManager.KEY_USER_SESSION_ID) != null) {
                    paraSessionId = getUserInfo(SessionManager.KEY_USER_SESSION_ID);
                }

                bookmarkVerseListAdapter = new BookmarkVerseListAdapter(VerseBookmarkActivity.this, bookmarkDataArrayList, verseAId, verseText, verseId, indexId, versePos, itemUri, paraUserId, paraSessionId);
                LinearLayoutManager layoutManager = new LinearLayoutManager(VerseBookmarkActivity.this, RecyclerView.VERTICAL, false);
                recyclerView.setLayoutManager(layoutManager);
                recyclerView.setAdapter(bookmarkVerseListAdapter);

                ItemTouchHelper.Callback callback = new EditItemTouchHelperCallback(bookmarkVerseListAdapter);
                ItemTouchHelper mItemTouchHelper = new ItemTouchHelper(callback);
                mItemTouchHelper.attachToRecyclerView(recyclerView);

                setVisibilityVisible(recyclerView);
                setVisibilityGone(tvNoData);
            } else {
                setVisibilityVisible(tvNoData);
                setVisibilityGone(recyclerView);
                showText(tvNoData, getString(R.string.no_bookmarks));
            }
        }
    }

    public void visibleHideView() {
        tvNoData.setText(R.string.no_bookmarks);
        tvNoData.setVisibility(View.VISIBLE);
        recyclerView.setVisibility(View.GONE);
    }

    private boolean isBookmarked(String data_aid) {
        getBookmarks(data_aid);
        if (bookmarkDataArrayList.size() > 0) {
            return true;
        }
        return false;
    }

    private ArrayList<BookmarkData> getBookmarks(String data_aid) {
        Cursor cursor;

        bookmarkDataArrayList.clear();

        cursor = commonDatabaseHelper.getBookmarkList(data_aid);
        if (cursor != null) {
            while (!cursor.isAfterLast()) {
                BookmarkData noteskData = new BookmarkData();

                String deleted = cursor.getString(cursor.getColumnIndex("deleted"));
                if (deleted.isEmpty()) {
                    noteskData.setId(cursor.getInt(cursor.getColumnIndex("_id")));
                    noteskData.setServerid(cursor.getString(cursor.getColumnIndex("serverid")));
                    noteskData.setVerseaid(cursor.getString(cursor.getColumnIndex("verseaid")));
                    noteskData.setChapterUri(cursor.getString(cursor.getColumnIndex("chapteruri")));
                    noteskData.setItemuri(cursor.getString(cursor.getColumnIndex("itemuri")));
                    noteskData.setBookuri(cursor.getString(cursor.getColumnIndex("bookuri")));
                    noteskData.setTitle(cursor.getString(cursor.getColumnIndex("versetitle")));
                    noteskData.setVerseNumber(cursor.getString(cursor.getColumnIndex("versenumber")));
                    noteskData.setVerseText(cursor.getString(cursor.getColumnIndex("versetext")));
                    noteskData.setDeleted(deleted);
                    noteskData.setModified(cursor.getString(cursor.getColumnIndex("modified")));
                    noteskData.setSort_order(cursor.getString(cursor.getColumnIndex("sort_order")));
                    bookmarkDataArrayList.add(noteskData);
                }
                cursor.moveToNext();
            }
        }
        return bookmarkDataArrayList;
    }

    @Override
    protected void onResume() {
        super.onResume();
        new GetBookMarks().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, "");
    }

    @OnClick(R.id.fab)
    public void AdBookmark() {
        if (isBookmarked(verseAId)) {
            Global.showToast(VerseBookmarkActivity.this, "Bookmark already Added!");
        } else {
            Intent intent = new Intent(VerseBookmarkActivity.this, VerseNewBookmarkActivity.class);
            intent.putExtra(getString(R.string.verseText), verseText);
            intent.putExtra(getString(R.string.versePos), versePos);
            intent.putExtra(getString(R.string.verseId), verseId);
            intent.putExtra(getString(R.string.verseAId), verseAId);
            intent.putExtra(getString(R.string.indexId), indexId);
            intent.putExtra(getString(R.string.itemUri), itemUri);
            startActivityWithAnimation(intent);
            finishActivity();
        }
    }

    @OnClick(R.id.ivBack)
    public void finishActivity() {
        finish();
    }

    @OnClick({R.id.ivCheck, R.id.ivEdit})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.ivEdit:
                bookmarkVerseListAdapter.isSetFalg(true);
                bookmarkVerseListAdapter.notifyDataSetChanged();
                setVisibilityGone(ivEdit);
                setVisibilityVisible(ivCheck);
                break;
            case R.id.ivCheck:
                bookmarkVerseListAdapter.isSetFalg(false);
                bookmarkVerseListAdapter.notifyDataSetChanged();
                setVisibilityVisible(ivEdit);
                setVisibilityGone(ivCheck);

                savebookmarkDataArrayList = sessionManager.getBookmarkSortOrderArraylist();

                Global.printLog("BOOKMARKARRAY", "=========savebookmarkDataArrayList==========" + savebookmarkDataArrayList.toString());

                if (savebookmarkDataArrayList != null && savebookmarkDataArrayList.size() > 0) {
                    for (int i = 0; i < savebookmarkDataArrayList.size(); i++) {

                        String Sort_order = savebookmarkDataArrayList.get(i).getSort_order();
                        String Serverid = savebookmarkDataArrayList.get(i).getServerid();
                        String Verseaid = savebookmarkDataArrayList.get(i).getVerseaid();
                        String ChapterUri = savebookmarkDataArrayList.get(i).getChapterUri();
                        String Itemuri = savebookmarkDataArrayList.get(i).getItemuri();
                        String Bookuri = savebookmarkDataArrayList.get(i).getBookuri();
                        String Title = savebookmarkDataArrayList.get(i).getTitle();
                        String VerseText = savebookmarkDataArrayList.get(i).getVerseText();
                        String VerseNumber = savebookmarkDataArrayList.get(i).getVerseNumber();
                        int id = savebookmarkDataArrayList.get(i).getId();

                        ContentValues contentValues = new ContentValues();
                        contentValues.put(CommonDatabaseHelper.KEY_SORT_ORDER, Sort_order);
                        contentValues.put(CommonDatabaseHelper.KEY_MODIFIED, "1");

                        commonDatabaseHelper.updateRowData(CommonDatabaseHelper.TABLE_BOOKMARK_MARSTER, contentValues, CommonDatabaseHelper.KEY_ID + " = ?",
                                new String[]{String.valueOf(id)});
                    }

                    savebookmarkDataArrayList = new ArrayList<>();
                    sessionManager.saveBookmarkSortOrderArraylist(savebookmarkDataArrayList);
                    new PushToServerData(this);
                }
                break;
        }
    }

    private class GetBookMarks extends AsyncTask<String, Void, String> {

        @Override
        protected void onPreExecute() {
            Global.showProgressDialog(VerseBookmarkActivity.this);
            bookmarkDataArrayList.clear();
        }

        @Override
        protected String doInBackground(String... params) {
            Cursor cursor;

            cursor = commonDatabaseHelper.getBookmarkList();
            if (cursor != null && cursor.moveToFirst()) {
                while (!cursor.isAfterLast()) {

                    String deleted = cursor.getString(cursor.getColumnIndex("deleted"));
                    if (deleted.isEmpty()) {
                        BookmarkData bookmarkData = new BookmarkData();

                        bookmarkData.setId(cursor.getInt(cursor.getColumnIndex("_id")));
                        bookmarkData.setServerid(cursor.getString(cursor.getColumnIndex("serverid")));
                        bookmarkData.setVerseaid(cursor.getString(cursor.getColumnIndex("verseaid")));
                        bookmarkData.setChapterUri(cursor.getString(cursor.getColumnIndex("chapteruri")));
                        bookmarkData.setItemuri(cursor.getString(cursor.getColumnIndex("itemuri")));
                        bookmarkData.setBookuri(cursor.getString(cursor.getColumnIndex("bookuri")));
                        bookmarkData.setTitle(cursor.getString(cursor.getColumnIndex("versetitle")));
                        bookmarkData.setVerseNumber(cursor.getString(cursor.getColumnIndex("versenumber")));
                        bookmarkData.setVerseText(cursor.getString(cursor.getColumnIndex("versetext")));
                        bookmarkData.setDeleted(deleted);
                        bookmarkData.setModified(cursor.getString(cursor.getColumnIndex("modified")));
                        bookmarkData.setSort_order(cursor.getString(cursor.getColumnIndex("sort_order")));
                        bookmarkDataArrayList.add(bookmarkData);
                    }
                    cursor.moveToNext();
                }
            }

            return "";
        }

        @Override
        protected void onPostExecute(String result) {
            Global.dismissProgressDialog();
            setRecyclerView();
        }
    }

    public static VerseBookmarkActivity getInstance() {
        return verseBookmarkActivity;
    }

}
