package com.ldsscriptures.pro.activity.settingmenu;

import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.JsonObject;
import com.ldsscriptures.pro.BuildConfig;
import com.ldsscriptures.pro.R;
import com.ldsscriptures.pro.activity.BaseActivity;
import com.ldsscriptures.pro.retroutils.APIClient;
import com.ldsscriptures.pro.retroutils.APIInterface;
import com.ldsscriptures.pro.utils.Global;
import com.ldsscriptures.pro.utils.SessionManager;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class SettingsAboutActivity extends BaseActivity {

    @BindView(R.id.ivBack)
    ImageView ivBack;
    @BindView(R.id.ivMenu)
    ImageView ivMenu;
    @BindView(R.id.tvTitle)
    TextView tvTitle;
    @BindView(R.id.ivDropDownMenu)
    ImageView ivDropDownMenu;
    @BindView(R.id.ivMore)
    ImageView ivMore;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.txtAppVersion)
    TextView txtAppVersion;
    @BindView(R.id.txtAboutAppVersion)
    TextView txtAboutAppVersion;
    @BindView(R.id.txtdatabaseversion)
    TextView txtdatabaseversion;
    @BindView(R.id.txtAboutdatabaseversion)
    TextView txtAboutdatabaseversion;
    @BindView(R.id.txtusername)
    TextView txtusername;
    @BindView(R.id.txtAboutusername)
    TextView txtAboutusername;
    @BindView(R.id.txtlastsyncdate)
    TextView txtlastsyncdate;
    @BindView(R.id.txtAboutlastsyncdate)
    TextView txtAboutlastsyncdate;
    SessionManager sessionManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting_about);
        ButterKnife.bind(this);
        setContent();
    }

    private void setContent() {

        getDatabaseVersion();

        showText(tvTitle, getResources().getString(R.string.about));
        setVisibilityGone(ivMore);
        setVisibilityGone(ivDropDownMenu);
        setVisibilityGone(ivMenu);
        setVisibilityVisible(ivBack);

        sessionManager = new SessionManager(this);
        String VerstionName_Code = BuildConfig.VERSION_NAME + " (" + BuildConfig.VERSION_CODE + ")";

        txtAboutAppVersion.setText(VerstionName_Code);
        txtAboutusername.setText(getUserInfo(sessionManager.KEY_USER_USERNAME));
        String lastSyncDate = sessionManager.getLastSyncDate();
        String var_time_Zone = sessionManager.getKeyTimeZone();

        if (lastSyncDate.length() != 0) {

            if (var_time_Zone != null) {
                try {
                    String inputPattern = "yyyy-MM-dd hh:mm:ss";
                    String timezone = getFormatDateWithUTC(lastSyncDate, var_time_Zone, inputPattern, inputPattern);
                    txtAboutlastsyncdate.setText(timezone);

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private void getDatabaseVersion() {

        APIInterface apiService = APIClient.getClient().create(APIInterface.class);
        String url = APIClient.BASE_URL;

        url += "latestDB.pl?device=" + APIClient.DEVICE_KEY +
                "&appversion=" + BuildConfig.VERSION_NAME +
                "&apiauth=" + APIClient.AUTH_KEY;

        Call<JsonObject> callObject = apiService.getDBVersion(url);

        callObject.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {

                if (response.body() != null && !response.body().toString().trim().isEmpty()) {

                    Global.DB_VERSION = response.body().get("dbversion").toString();

                    txtAboutdatabaseversion.setText(Global.DB_VERSION);
                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                call.cancel();

            }
        });
    }

    @OnClick(R.id.ivBack)
    public void onViewClicked() {
        finish();
    }

    public String getFormatDateWithUTC(String date, String var_time_Zone, String inputPattern, String outputPattern) {
        try {
            SimpleDateFormat sdf3 = new SimpleDateFormat(inputPattern, Locale.ENGLISH);
            SimpleDateFormat sdfout = new SimpleDateFormat(outputPattern, Locale.ENGLISH);

            if (var_time_Zone.equals("PST")) {
                sdf3.setTimeZone(TimeZone.getTimeZone("PST8PDT"));
            } else if (var_time_Zone.equals("PDT")) {
                sdf3.setTimeZone(TimeZone.getTimeZone("PST8PDT"));
            } else {
                sdf3.setTimeZone(TimeZone.getTimeZone(var_time_Zone));
            }

            Date d1 = null;
            d1 = sdf3.parse(date);
            sdf3.setTimeZone(TimeZone.getDefault());
            assert d1 != null;
            return sdfout.format(d1);
        } catch (Exception ignored) {
        }
        return "";
    }
}
