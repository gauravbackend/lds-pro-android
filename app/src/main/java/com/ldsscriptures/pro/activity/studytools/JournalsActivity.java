package com.ldsscriptures.pro.activity.studytools;

import android.content.Intent;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.ldsscriptures.pro.R;
import com.ldsscriptures.pro.activity.BaseActivity;
import com.ldsscriptures.pro.activity.MainActivity;
import com.ldsscriptures.pro.adapter.JournalsListAdapter;
import com.ldsscriptures.pro.database.CommonDatabaseHelper;
import com.ldsscriptures.pro.model.JournalData;
import com.ldsscriptures.pro.model.JournalItemData;
import com.ldsscriptures.pro.utils.Global;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class JournalsActivity extends BaseActivity {

    @BindView(R.id.ivMenu)
    ImageView ivMenu;
    @BindView(R.id.tvTitle)
    TextView tvTitle;
    @BindView(R.id.ivSearch)
    ImageView ivSearch;
    @BindView(R.id.ivMore)
    ImageView ivMore;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.eListView)
    ExpandableListView eListView;
    @BindView(R.id.fab)
    FloatingActionButton fab;
    @BindView(R.id.txtNoData)
    TextView txtNoData;
    @BindView(R.id.ivBack)
    ImageView ivBack;
    @BindView(R.id.ivTabs)
    ImageView ivTabs;
    private CommonDatabaseHelper commonDatabaseHelper;
    public static JournalsActivity journalsActivity;
    ArrayList<JournalData> journalDataArrayList = new ArrayList<>();

    public static JournalsActivity getInstance() {
        return journalsActivity;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_journals);
        ButterKnife.bind(this);
        setContents();
        setData();
    }

    private void setContents() {
        ButterKnife.bind(this);
        journalsActivity = this;

        setToolbar(toolbar);
        setVisibilityGone(ivMore);
        setVisibilityGone(ivTabs);
        setVisibilityGone(ivSearch);
        showText(tvTitle, getString(R.string.journals));
        commonDatabaseHelper = new CommonDatabaseHelper(JournalsActivity.this);
    }

    public void setData() {
        //get the category from the local database
        new GetJournalList().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, "");
    }

    @OnClick({R.id.ivMenu, R.id.fab})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.ivMenu:
                openSlideMenu();
                break;
            case R.id.fab:
                startActivity(new Intent(JournalsActivity.this, AddJournalsActivity.class));
                break;
        }
    }

    public class GetJournalList extends AsyncTask<String, Void, String> {
        @Override
        protected void onPreExecute() {
            Global.showProgressDialog(JournalsActivity.this);
        }

        @Override
        protected String doInBackground(String... params) {
            Cursor cursor;
            cursor = commonDatabaseHelper.getJournalGroupList();

            journalDataArrayList = new ArrayList<>();
            if (cursor != null && cursor.moveToFirst()) {
                while (!cursor.isAfterLast()) {
                    JournalData journalData = new JournalData();
                    journalData.setGroupingdate(cursor.getString(cursor.getColumnIndex(CommonDatabaseHelper.KEY_GROUPINGDATE)));
                    journalData.setJournalItemDataArrayList(getJournalGroupItem(cursor.getString(cursor.getColumnIndex(CommonDatabaseHelper.KEY_GROUPINGDATE))));
                    journalDataArrayList.add(journalData);
                    cursor.moveToNext();
                }
            }
            commonDatabaseHelper.close();
            if (cursor != null) {
                cursor.close();
            }
            return "";
        }

        @Override
        protected void onPostExecute(String result) {
            Global.dismissProgressDialog();
            if (journalDataArrayList.size() != 0) {
                setVisibilityGone(txtNoData);
                setVisibilityVisible(eListView);
                JournalsListAdapter journalsListAdapter = new JournalsListAdapter(JournalsActivity.this, journalDataArrayList);
                eListView.setAdapter(journalsListAdapter);
                for (int i = 0; i < journalDataArrayList.size(); i++) {
                    eListView.expandGroup(i);
                }
            } else {
                setVisibilityGone(eListView);
                setVisibilityVisible(txtNoData);
            }
        }
    }


    public void visibleHideView() {
        setVisibilityGone(eListView);
        setVisibilityVisible(txtNoData);
    }

    public ArrayList<JournalItemData> getJournalGroupItem(String groupingDate) {
        Cursor cursor1;
        cursor1 = commonDatabaseHelper.getJournalList(groupingDate);
        ArrayList<JournalItemData> journalItemDataArrayList = new ArrayList<>();
        if (cursor1 != null && cursor1.moveToFirst()) {
            while (!cursor1.isAfterLast()) {

                String deleted = cursor1.getString(cursor1.getColumnIndex("deleted"));
                if (deleted == null) {
                    deleted = "";
                }
                if (deleted.isEmpty()) {
                    JournalItemData journalItemData = new JournalItemData();
                    journalItemData.setId(cursor1.getInt(cursor1.getColumnIndex(CommonDatabaseHelper.KEY_ID)));
                    journalItemData.setJournaltext(cursor1.getString(cursor1.getColumnIndex(CommonDatabaseHelper.KEY_JOURNALTEXT)));
                    journalItemData.setJournaltitle(cursor1.getString(cursor1.getColumnIndex(CommonDatabaseHelper.KEY_JOURNALTITLE)));
                    journalItemData.setServerid(cursor1.getString(cursor1.getColumnIndex(CommonDatabaseHelper.KEY_SERVERID)));
                    journalItemData.setDeleted(deleted);
                    journalItemData.setModified(cursor1.getString(cursor1.getColumnIndex("modified")));
                    journalItemDataArrayList.add(journalItemData);
                }
                cursor1.moveToNext();
            }
        }
        if (cursor1 != null) {
            cursor1.close();
        }
        return journalItemDataArrayList;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menuBack:
                finish();
                break;
            case R.id.menuNext:
                Toast.makeText(getApplicationContext(), "No Item Found", Toast.LENGTH_LONG).show();
                break;
            case R.id.menuBookmark:
                Intent bookmarkIntent = new Intent(getApplicationContext(), MainActivity.class);
                bookmarkIntent.putExtra(getString(R.string.menuSelection), "Bookmarks");
                bookmarkIntent.putExtra(getString(R.string.isFromReading), true);
                startActivityWithAnimation(bookmarkIntent);
                break;
            case R.id.menuHistory:
                Intent historyIntent = new Intent(getApplicationContext(), MainActivity.class);
                historyIntent.putExtra(getString(R.string.menuSelection), "History");
                historyIntent.putExtra(getString(R.string.isFromReading), true);
                startActivityWithAnimation(historyIntent);
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    public ArrayList<JournalData> journalArraylistMethod() {
        return journalDataArrayList;
    }
}

