package com.ldsscriptures.pro.activity.verses;

import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.ldsscriptures.pro.BuildConfig;
import com.ldsscriptures.pro.R;
import com.ldsscriptures.pro.activity.BaseActivity;
import com.ldsscriptures.pro.model.TimeLineShareSuccess;
import com.ldsscriptures.pro.retroutils.APIClient;
import com.ldsscriptures.pro.retroutils.APIInterface;
import com.ldsscriptures.pro.utils.Global;
import com.ldsscriptures.pro.utils.SessionManager;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by IBL InfoTech on 9/7/2017.
 */

public class VerseTimelineShareActivity extends BaseActivity {

    @BindView(R.id.ivBack)
    ImageView ivBack;
    @BindView(R.id.ivMenu)
    ImageView ivMenu;
    @BindView(R.id.tvTitle)
    TextView tvTitle;
    @BindView(R.id.ivCheck)
    ImageView ivCheck;
    @BindView(R.id.ivEdit)
    ImageView ivEdit;
    @BindView(R.id.ivMore)
    ImageView ivMore;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.edt_TimelineSharetext)
    EditText edtTimelineSharetext;
    @BindView(R.id.top_layout_timeline_share)
    LinearLayout topLayoutTimelineShare;
    @BindView(R.id.timeline_divider)
    TextView timelineDivider;
    @BindView(R.id.verse_timeline_share_path)
    TextView verseTimelineSharePath;
    @BindView(R.id.verse_timeline_sharetext)
    TextView verseTimelineSharetext;
    @BindView(R.id.tvDoneTimeline)
    TextView tvDoneTimeline;

    private SessionManager sessionManager;
    private String verseText;
    private String verseId;
    private String verseAid;
    private String title;
    private String versePath;
    private String citation_itemUri;

    static VerseTimelineShareActivity verseTimelineShareActivity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_verse_timeline_share);
        ButterKnife.bind(this);
        setContents();
    }

    private void setContents() {

        verseTimelineShareActivity = this;
        showText(tvTitle, getString(R.string.timeline_share));
        sessionManager = new SessionManager(this);
        setVisibilityVisible(ivMore);

        verseText = getIntent().getStringExtra(getString(R.string.verseText));
        verseId = getIntent().getStringExtra(getString(R.string.verseId));
        verseAid = getIntent().getStringExtra(getString(R.string.verseAId));
        title = getIntent().getStringExtra("title");
        citation_itemUri = Global.bookmarkData.getItemuri();

        versePath = title + ":" + verseId;

        verseTimelineSharePath.setText(versePath);
        verseTimelineSharetext.setText(verseText);
    }

    private void callTimelinePushApi(String para_uri, String paraVerseId, String paraVerseAId, String paraTimelineShareText) {

        showprogress();

        APIInterface apiService = APIClient.getClient().create(APIInterface.class);
        String url = APIClient.BASE_URL;

        String paraUserId = "", paraSessionId = "";

        if (getUserInfo(SessionManager.KEY_USER_ID) != null) {
            paraUserId = getUserInfo(SessionManager.KEY_USER_ID);
        }
        if (getUserInfo(SessionManager.KEY_USER_SESSION_ID) != null) {
            paraSessionId = getUserInfo(SessionManager.KEY_USER_SESSION_ID);
        }

        url += "timelinePush.pl?user_id=" + paraUserId +
                "&session_id=" + paraSessionId +
                "&action=" + "add" +
                "&type=" + "share" +
                "&device=" + APIClient.DEVICE_KEY +
                "&app_version=" + BuildConfig.VERSION_NAME +
                "&uri=" + para_uri +
                "&verse=" + paraVerseId +
                "&aid=" + paraVerseAId +
                "&note=" + paraTimelineShareText +
                "&share_level=" + "1" +
                "&for_uid=" +
                "&apiauth=" + APIClient.AUTH_KEY;


        Call<TimeLineShareSuccess> callObject = apiService.getTimeLineShareSuccess(url.replaceAll(" ", "%20"));
        callObject.enqueue(new Callback<TimeLineShareSuccess>() {

            @Override
            public void onResponse(Call<TimeLineShareSuccess> call, Response<TimeLineShareSuccess> response) {
                if (response.body() != null && !response.body().toString().trim().isEmpty()) {
                    dismissprogress();
                }
            }

            @Override
            public void onFailure(Call<TimeLineShareSuccess> call, Throwable t) {
                dismissprogress();
            }
        });
    }

    public void chkValidation() {

        String getTimelineShareText = edtTimelineSharetext.getText().toString().trim();
        if (getTimelineShareText.length() != 0) {
            String Subscribed = sessionManager.getKeySubscribed();
            if (Subscribed.equals("1")) {
                callTimelinePushApi(citation_itemUri, verseId, verseAid, getTimelineShareText);
            }
            finish();
        } else {
            Toast.makeText(getApplicationContext(), R.string.enter_timeline_Share, Toast.LENGTH_LONG).show();
        }
    }

    @OnClick({R.id.ivBack, R.id.tvDoneTimeline})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.ivBack:
                finish();
                break;
            case R.id.tvDoneTimeline:
                chkValidation();
                break;
        }
    }

    public static VerseTimelineShareActivity getInstance() {
        return verseTimelineShareActivity;
    }
}
