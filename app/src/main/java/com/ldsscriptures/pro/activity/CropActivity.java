package com.ldsscriptures.pro.activity;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.StrictMode;
import android.widget.Button;
import android.widget.Toast;

import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferListener;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferObserver;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferState;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferUtility;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.CannedAccessControlList;
import com.ldsscriptures.pro.R;
import com.ldsscriptures.pro.utils.Constants;
import com.theartofdev.edmodo.cropper.CropImageView;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class CropActivity extends BaseActivity {


    @BindView(R.id.cropImageView_new)
    CropImageView cropImageViewNew;
    @BindView(R.id.crop_cancle_button)
    Button cropCancleButton;
    @BindView(R.id.crop_done_button)
    Button cropDoneButton;

    AmazonS3Client s3;
    BasicAWSCredentials credentials;
    TransferUtility transferUtility;
    TransferObserver observer;

    String key = Constants.AWS_ACCESS_KEY;
    String secret = Constants.AWS_SECRET_KEY;
    String mFileName;
    String imageUrl;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_crop_layout);
        ButterKnife.bind(this);
        setContents();
    }

    private void setContents() {

        ButterKnife.bind(this);
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        cropImageViewNew.setImageBitmap(SelectImageActivity.mBitmap);
    }

    @OnClick(R.id.crop_done_button)
    public void GetImage() {

        SelectImageActivity.croppedBitmap = cropImageViewNew.getCroppedImage();

        credentials = new BasicAWSCredentials(key, secret);
        s3 = new AmazonS3Client(credentials);
        transferUtility = new TransferUtility(s3, CropActivity.this);

        mFileName = "LDSPro_" + System.currentTimeMillis() + ".png";
        observer = transferUtility.upload(
                Constants.BUCKET_NAME,
                "img/" + mFileName,
                bitmapToFile(SelectImageActivity.croppedBitmap),
                CannedAccessControlList.PublicRead
        );

        observer.setTransferListener(new TransferListener() {
            @Override
            public void onStateChanged(int id, TransferState state) {

                showprogress();
                if (state.COMPLETED.equals(observer.getState())) {
                    imageUrl = "https://s3-us-west-2.amazonaws.com/ldspro/img/" + mFileName;
                    SignUpActivity.getInstance().signUpNewUSer(imageUrl);
                    finish();
                }
            }

            @Override
            public void onProgressChanged(int id, long bytesCurrent, long bytesTotal) {

            }

            @Override
            public void onError(int id, Exception ex) {
                Toast.makeText(getApplicationContext(), "" + ex.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    @OnClick(R.id.crop_cancle_button)
    public void cancel() {
        finish();
        startActivity(new Intent(getApplicationContext(), SelectImageActivity.class));
        overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    public File bitmapToFile(Bitmap bmp) {
        try {
            int size = 0;
            ByteArrayOutputStream bos = new ByteArrayOutputStream(size);
            bmp.compress(Bitmap.CompressFormat.PNG, 80, bos);
            byte[] bArr = bos.toByteArray();
            bos.flush();
            bos.close();

            FileOutputStream fos = openFileOutput(mFileName, Context.MODE_PRIVATE);
            fos.write(bArr);
            fos.flush();
            fos.close();

            File mFile = new File(getFilesDir().getAbsolutePath(), mFileName);
            return mFile;
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return null;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }
}