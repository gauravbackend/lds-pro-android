package com.ldsscriptures.pro.activity.activitymenu;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.ldsscriptures.pro.R;
import com.ldsscriptures.pro.activity.BaseActivity;
import com.ldsscriptures.pro.activity.MainActivity;
import com.ldsscriptures.pro.adapter.AchievementsPagerAdapter;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


public class AchievementsActivity extends BaseActivity {

    @BindView(R.id.ivMenu)
    ImageView ivMenu;
    @BindView(R.id.ivBack)
    ImageView ivBack;
    @BindView(R.id.tvTitle)
    TextView tvTitle;
    @BindView(R.id.ivSearch)
    ImageView ivSearch;
    @BindView(R.id.ivMore)
    ImageView ivMore;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.pager)
    ViewPager pager;
    @BindView(R.id.ivTabs)
    ImageView ivTabs;

    int[] mResources = {1, 2, 3};

    AchievementsPagerAdapter achievementsPagerAdapter;
    static AchievementsActivity achievementsActivity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_achievements);
        ButterKnife.bind(this);
        setContents();
    }

    public void setContents() {
        achievementsActivity = this;

        setToolbar(toolbar);
        setVisibilityGone(ivMore);
        showText(tvTitle, getResources().getString(R.string.txt_achievements));
        achievementsPagerAdapter = new AchievementsPagerAdapter(this, mResources);
        pager.setAdapter(achievementsPagerAdapter);
        ivSearch.setVisibility(View.GONE);
        ivTabs.setVisibility(View.GONE);
    }

    @OnClick(R.id.ivMenu)
    public void openMenu() {
        openSlideMenu();
    }

    public static AchievementsActivity getInstance() {
        return achievementsActivity;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menuBack:
                if (pager.getCurrentItem() > 0)
                    pager.setCurrentItem(pager.getCurrentItem() - 1);
                else
                    Toast.makeText(getApplicationContext(), "No Item Found", Toast.LENGTH_LONG).show();
                //finish();
                break;
            case R.id.menuNext:
                if (pager.getCurrentItem() != (mResources.length - 1))
                    pager.setCurrentItem(pager.getCurrentItem() + 1);
                else
                    //finish();
                    Toast.makeText(getApplicationContext(), "No Item Found", Toast.LENGTH_LONG).show();
                break;
            case R.id.menuBookmark:
                Intent bookmarkIntent = new Intent(AchievementsActivity.this, MainActivity.class);
                bookmarkIntent.putExtra(getString(R.string.menuSelection), getString(R.string.intent_bookmarks));
                bookmarkIntent.putExtra(getString(R.string.isFromReading), true);
                startActivity(bookmarkIntent);
                overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
                break;
            case R.id.menuHistory:
                Intent historyIntent = new Intent(AchievementsActivity.this, MainActivity.class);
                historyIntent.putExtra(getString(R.string.menuSelection), getString(R.string.intent_history));
                historyIntent.putExtra(getString(R.string.isFromReading), true);
                startActivity(historyIntent);
                overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}
