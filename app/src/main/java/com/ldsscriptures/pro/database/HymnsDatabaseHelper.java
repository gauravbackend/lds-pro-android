package com.ldsscriptures.pro.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.ldsscriptures.pro.utils.Global;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class HymnsDatabaseHelper extends SQLiteOpenHelper {


    private static String DB_NAME = "hymns.sqlite";

    // parameter for hymns topics
    public static String ID = "id";
    public static String NAME = "name";

    // parameter for hymns topic content
    public static String CONTENT_PDF_URL = "pdf_url";
    public static String CONTENT_PAGE_URL = "page_url";
    public static String CONTENT_COPYRIGHTED = "copyrighted";

    private SQLiteDatabase myDataBase;
    private final Context myContext;

    public HymnsDatabaseHelper(Context context) {
        super(context, DB_NAME, null, Global.DATABASE_VERSION);
        this.myContext = context;
    }

    @Override
    public void onCreate(SQLiteDatabase arg0) {
        // TODO Auto-generated method stub
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // TODO Auto-generated method stub
    }

    public SQLiteDatabase openDataBase() throws SQLException {
        File dbFile = myContext.getDatabasePath(DB_NAME);
        if (!dbFile.exists()) {
            try {
                copyDataBase(dbFile);
            } catch (IOException e) {
                throw new RuntimeException("Error creating source database", e);
            }
        }

        return SQLiteDatabase.openDatabase(dbFile.getPath(), null, SQLiteDatabase.OPEN_READONLY);
    }

    private void copyDataBase(File dbFile) throws IOException {
        InputStream is = myContext.getAssets().open(DB_NAME);
        OutputStream os = new FileOutputStream(dbFile);

        byte[] buffer = new byte[1024];
        while (is.read(buffer) > 0) {
            os.write(buffer);
        }

        os.flush();
        os.close();
        is.close();
    }

    //Open database
    public HymnsDatabaseHelper openDatabase() throws java.sql.SQLException {
        myDataBase = this.getReadableDatabase();
        myDataBase = this.getWritableDatabase();

        if (!myDataBase.isReadOnly()) {
            myDataBase.execSQL("PRAGMA foreign_keys=ON;");
        }
        return this;
    }

    public synchronized void closeDataBase() throws java.sql.SQLException {
        if (myDataBase != null && myDataBase.isOpen())
            myDataBase.close();
        super.close();
    }

    public Cursor getTopicsList() {
        Cursor cursor = null;
        try {
            openDatabase();
            myDataBase = this.getReadableDatabase();

            String selectImageQuery = null;

            selectImageQuery = "SELECT * FROM topics";
            if (selectImageQuery != null) {
                cursor = myDataBase.rawQuery(selectImageQuery, null);
            }
            if (cursor != null && myDataBase != null && myDataBase.isOpen()) {
                cursor.moveToFirst();
            }

        } catch (java.sql.SQLException e) {
            e.printStackTrace();
        }

        return cursor;
    }

    public Cursor getAllContentList() {
        Cursor cursor = null;
        try {
            openDatabase();
            myDataBase = this.getReadableDatabase();
            String selectImageQuery = null;
            selectImageQuery = "select id,name,pdf_url,page_url,copyrighted from hymns";

            if (selectImageQuery != null) {
                cursor = myDataBase.rawQuery(selectImageQuery, null);
            }
            if (cursor != null && myDataBase != null && myDataBase.isOpen()) {
                cursor.moveToFirst();
            }

        } catch (java.sql.SQLException e) {
            e.printStackTrace();
        }

        return cursor;
    }

    public Cursor getSpecificContent(int topicId) {
        Cursor cursor = null;
        try {
            openDatabase();
            myDataBase = this.getReadableDatabase();
            String selectImageQuery = null;
            selectImageQuery = "select hm.* from hymns hm, topics tp, has_topic htp where tp.id=" + topicId + " and hm.id=htp.hymn_id and tp.id=htp.topic_id ORDER BY hm.id ASC";

            cursor = myDataBase.rawQuery(selectImageQuery, null);
            if (cursor != null && myDataBase != null && myDataBase.isOpen()) {
                cursor.moveToFirst();
            }
            closeDataBase();
        } catch (java.sql.SQLException e) {
            e.printStackTrace();
        }

        return cursor;
    }

    public boolean insertData(String tableName, ContentValues values) {
        try {
            openDatabase();
            myDataBase.insert(tableName, null, values);
            closeDataBase();
        } catch (java.sql.SQLException e) {
            e.printStackTrace();
        }


        return true;
    }

    public boolean updateRowData(String tableName, ContentValues values, String selection, String[] selectionArgs) {
        try {
            openDatabase();
            myDataBase.update(tableName, values, selection, selectionArgs);
            closeDataBase();
        } catch (java.sql.SQLException e) {
            e.printStackTrace();
        }
        return true;
    }
}
