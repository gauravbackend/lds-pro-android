package com.ldsscriptures.pro.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.ldsscriptures.pro.model.SpannedTextData;
import com.ldsscriptures.pro.model.StudyDailyVerse;
import com.ldsscriptures.pro.utils.Global;

import java.util.ArrayList;

/**
 * Created by IBL InfoTech on 7/18/2017.
 */

public class HighlightDatabaseHelper {

    Context context;
    private SQLiteDatabase sqLiteDb;

    public static final String DATABASE_NAME = "HighlightUnderlineDB";
    public static final String TABLE_CONTACTS = "HighlightUnderline";
    public static final String TABLE_studyDaily = "tablestudyDaily";
    public static final String KEY_ID = "id";
    public static final String KEY_verseAId = "verseAId";
    public static final String KEY_cursorStart = "cursorStart";
    public static final String KEY_cursorEnd = "cursorEnd";
    public static final String KEY_cursorSelection = "cursorSelection";
    public static final String KEY_selectedColor = "selectedColor";
    public static final String KEY_Highlightflag = "Highlightflag";
    public static final String KEY_uri = "uri";
    public static final String KEY_serverId = "serverid";
    public static final String KEY_showDate = "showDate";
    public static final String KEY_verseId = "verseId";
    public static final String KEY_VerseURI = "verseUri";
    public static final String KEY_VerseNO = "verseno";
    public static final String KEY_DELETED = "deleted";
    public static final String KEY_MODIFIED = "modified";

    String DATABASE_ALTER_HIGHLIGHT_1 = "ALTER TABLE " + TABLE_CONTACTS + " ADD COLUMN " + KEY_DELETED + " TEXT;";
    String DATABASE_ALTER_HIGHLIGHT_2 = "ALTER TABLE " + TABLE_CONTACTS + " ADD COLUMN " + KEY_MODIFIED + " TEXT;";
    String DATABASE_ALTER_HIGHLIGHT_3 = "ALTER TABLE " + TABLE_CONTACTS + " ADD COLUMN " + KEY_verseId + " TEXT;";
    String DATABASE_ALTER_HIGHLIGHT_4 = "ALTER TABLE " + TABLE_CONTACTS + " ADD COLUMN " + KEY_VerseURI + " TEXT;";


    public HighlightDatabaseHelper(Context context) {
        this.context = context;
    }

    private static final String CREATE_HIGHLIGHT_TABLE = "CREATE TABLE " + TABLE_CONTACTS + "("
            + KEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,"
            + KEY_verseAId + " INTEGER,"
            + KEY_serverId + " TEXT,"
            + KEY_verseId + " TEXT,"
            + KEY_VerseURI + " TEXT,"
            + KEY_cursorStart + " INTEGER,"
            + KEY_cursorEnd + " INTEGER,"
            + KEY_cursorSelection + " TEXT,"
            + KEY_selectedColor + " TEXT,"
            + KEY_DELETED + " TEXT,"
            + KEY_MODIFIED + " TEXT,"
            + KEY_Highlightflag + " INTEGER" + ")";

    private static final String CREATE_STUDYDAILY_TABLE = "CREATE TABLE " + TABLE_studyDaily + "("
            + KEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,"
            + KEY_uri + " TEXT,"
            + KEY_showDate + " TEXT" + ")";


    public boolean insertData(String tableName, ContentValues values) {
        try {
            open();
            sqLiteDb.insert(tableName, null, values);
            close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return true;
    }

    public boolean insertDataIfNotExists(String tableName, ContentValues values) {
        try {
            open();
            sqLiteDb.insertWithOnConflict(tableName, null, values, SQLiteDatabase.CONFLICT_IGNORE);
            close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return true;
    }

    public boolean updateRowData(String tableName, ContentValues values, String selection, String[] selectionArgs) {
        try {
            open();
            sqLiteDb.update(tableName, values, selection, selectionArgs);
            close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return true;
    }

    public boolean deleteRowData(String tableName, String selection, String[] selectionArgs) {
        try {
            open();
            sqLiteDb.delete(tableName, selection, selectionArgs);
            close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return true;
    }

    public boolean deleteData(String tableName) {
        try {
            open();
            sqLiteDb.delete(tableName, null, null);
            close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return true;
    }

    // ---Delete data from the table---
    public void clearDataFromTable() {
        try {
            deleteData(TABLE_CONTACTS);
            deleteData(TABLE_studyDaily);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    // code to add the new contact
    public void addHighLightData(SpannedTextData contact) {
        try {
            open();
            ContentValues values = new ContentValues();
            values.put(KEY_verseAId, contact.getVerseAId());
            values.put(KEY_cursorStart, contact.getStartIndex());
            values.put(KEY_cursorEnd, contact.getEndIndex());
            values.put(KEY_cursorSelection, contact.getSpannedText());
            values.put(KEY_selectedColor, contact.getColor());
            values.put(KEY_Highlightflag, contact.getHighlightflag());
            values.put(KEY_serverId, contact.getServerid());
            values.put(KEY_DELETED, contact.getDeleted());
            values.put(KEY_MODIFIED, contact.getModified());
            values.put(KEY_verseId, contact.getVerseId());
            values.put(KEY_VerseURI, contact.getVerseURI());
            // Inserting Row
            sqLiteDb.insert(TABLE_CONTACTS, null, values);
            close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    // ---opens the database---
    public HighlightDatabaseHelper open() throws SQLException {
        try {
            HighlightDatabaseHelper.DatabaseHelper dbHelper = new HighlightDatabaseHelper.DatabaseHelper(context);

            sqLiteDb = dbHelper.getWritableDatabase();
            sqLiteDb = dbHelper.getReadableDatabase();

            if (!sqLiteDb.isReadOnly()) {
                // Enable foreign key constraints
                sqLiteDb.execSQL("PRAGMA foreign_keys=ON;");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return this;
    }

    // ---closes the database---
    public void close() {
        try {
            if (sqLiteDb != null && sqLiteDb.isOpen()) {
//            dbHelper.close();
                sqLiteDb.close();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    // code to get all contacts in a list view
    public ArrayList<SpannedTextData> getAllHighLightData() {
        ArrayList<SpannedTextData> contactList = new ArrayList<SpannedTextData>();
        try {
            open();
            // Select All Query
            String selectQuery = "SELECT  * FROM " + TABLE_CONTACTS;
            Cursor cursor = sqLiteDb.rawQuery(selectQuery, null);

            // looping through all rows and adding to list
            if (cursor.moveToFirst()) {
                do {
                    SpannedTextData contact = new SpannedTextData();
                    contact.setVerseAId(cursor.getInt(cursor.getColumnIndex(KEY_verseAId)));
                    contact.setStartIndex(cursor.getInt(cursor.getColumnIndex(KEY_cursorStart)));
                    contact.setEndIndex(cursor.getInt(cursor.getColumnIndex(KEY_cursorEnd)));
                    contact.setSpannedText(cursor.getString(cursor.getColumnIndex(KEY_cursorSelection)));
                    contact.setColor(cursor.getString(cursor.getColumnIndex(KEY_selectedColor)));
                    contact.setHighlightflag(cursor.getInt(cursor.getColumnIndex(KEY_Highlightflag)));
                    contact.setServerid(cursor.getString(cursor.getColumnIndex(KEY_serverId)));
                    contact.setDeleted(cursor.getString(cursor.getColumnIndex(KEY_DELETED)));
                    contact.setModified(cursor.getString(cursor.getColumnIndex(KEY_MODIFIED)));
                    contactList.add(contact);
                } while (cursor.moveToNext());
            }

            if (cursor != null) {
                cursor.close();
            }
            close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        // return contact list
        return contactList;
    }

    // code to update the single contact
    public int updateHighlightData(SpannedTextData contact) {

        int i = 0;
        try {
            open();
            ContentValues values = new ContentValues();
            values.put(KEY_cursorStart, contact.getStartIndex());
            values.put(KEY_cursorEnd, contact.getEndIndex());
            values.put(KEY_cursorSelection, contact.getSpannedText());
            values.put(KEY_selectedColor, contact.getColor());
            values.put(KEY_Highlightflag, contact.getHighlightflag());

            // updating row
            i = sqLiteDb.update(TABLE_CONTACTS, values, KEY_verseAId + " = ?", new String[]{String.valueOf(contact.getVerseAId())});
            close();

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return i;
    }

    // Deleting single contact
    public void deleteHighlightData(String verseAId) {
        try {
            open();
            //sqLiteDb.delete(TABLE_CONTACTS, KEY_verseAId + " = ?", new String[]{String.valueOf(verseAId)});

            ContentValues contentValues = new ContentValues();
            contentValues.put(KEY_DELETED, "1");
            updateRowData(TABLE_CONTACTS, contentValues, KEY_verseAId + " = ?",
                    new String[]{String.valueOf(verseAId)});
            close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }


    // code to add the new contact
    public void addStudyDailyRecord(StudyDailyVerse contact) {
        try {
            open();
            ContentValues values = new ContentValues();
            values.put(KEY_uri, contact.getUri());
            values.put(KEY_showDate, contact.getShowDate());
            // Inserting Row
            sqLiteDb.insert(TABLE_studyDaily, null, values);
            //2nd argument is String containing nullColumnHack
            //Toast.makeText(context,"Saved Successfully"+values, Toast.LENGTH_LONG).show();

            close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    // code to get all contacts in a list view
    public ArrayList<StudyDailyVerse> getAllStudyDailyVerseData() {
        ArrayList<StudyDailyVerse> contactList = new ArrayList<StudyDailyVerse>();
        // Select All Query
        try {
            open();
            String selectQuery = "SELECT  * FROM " + TABLE_studyDaily;
            Cursor cursor = sqLiteDb.rawQuery(selectQuery, null);
            // looping through all rows and adding to list
            if (cursor.moveToFirst()) {
                do {
                    StudyDailyVerse contact = new StudyDailyVerse();

                    contact.setUri(cursor.getString(1));
                    contact.setShowDate(cursor.getString(2));

                    // Adding contact to list
                    contactList.add(contact);
                } while (cursor.moveToNext());
            }
            close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        // return contact list
        return contactList;
    }

    public boolean exiestHighLightData(String serverid) {
        boolean isExiest = false;
        try {
            open();
            String query = null;
            query = "SELECT * FROM " + TABLE_CONTACTS + " WHERE " + KEY_serverId + " = " + "'" + serverid + "'";
            Cursor cursor = sqLiteDb.rawQuery(query, null);
            if (cursor != null && sqLiteDb != null && sqLiteDb.isOpen()) {
                cursor.moveToFirst();
                if (cursor.getCount() != 0) {
                    isExiest = true;
                }
            }
            close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return isExiest;
    }

    public Cursor getHighlightPushToServer() {
        Cursor cursor = null;
        try {
            open();
            String query = null;
            query = "SELECT * FROM " + TABLE_CONTACTS + " WHERE " + KEY_serverId + " = '" + 0 + "'";
            cursor = sqLiteDb.rawQuery(query, null);
            if (cursor != null && sqLiteDb != null && sqLiteDb.isOpen()) {
                cursor.moveToFirst();
            }
            close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return cursor;
    }

    public Cursor getDeleteHightlightPushToServer() {
        Cursor cursor = null;
        try {
            open();
            String query = null;
            query = "SELECT * FROM " + TABLE_CONTACTS + " WHERE " + KEY_DELETED + " = '" + 1 + "'";
            cursor = sqLiteDb.rawQuery(query, null);
            if (cursor != null && sqLiteDb != null && sqLiteDb.isOpen()) {
                cursor.moveToFirst();
            }
            close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return cursor;
    }

    public class DatabaseHelper extends SQLiteOpenHelper {

        public DatabaseHelper(Context context) {
            super(context, DATABASE_NAME, null, Global.DATABASE_VERSION);
        }

        // Creating Tables
        @Override
        public void onCreate(SQLiteDatabase db) {
            db.execSQL(CREATE_HIGHLIGHT_TABLE);
            db.execSQL(CREATE_STUDYDAILY_TABLE);
        }

        // Upgrading database
        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

            if (oldVersion != newVersion) {

                try {
                    if (!isFieldExist(db, TABLE_CONTACTS, KEY_DELETED)) {
                        db.execSQL(DATABASE_ALTER_HIGHLIGHT_1);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

                try {
                    if (!isFieldExist(db, TABLE_CONTACTS, KEY_MODIFIED)) {
                        db.execSQL(DATABASE_ALTER_HIGHLIGHT_2);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

                try {
                    if (!isFieldExist(db, TABLE_CONTACTS, KEY_verseId)) {
                        db.execSQL(DATABASE_ALTER_HIGHLIGHT_3);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                try {
                    if (!isFieldExist(db, TABLE_CONTACTS, KEY_VerseURI)) {
                        db.execSQL(DATABASE_ALTER_HIGHLIGHT_4);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            // Drop older table if existed
            //db.execSQL("DROP TABLE IF EXISTS " + TABLE_CONTACTS);
            //db.execSQL("DROP TABLE IF EXISTS " + TABLE_studyDaily);
            // Create tables again
            //onCreate(db);
        }
    }

    private boolean isFieldExist(SQLiteDatabase inDatabase, String inTable, String columnToCheck) {
        Cursor mCursor = null;
        try {
            mCursor = inDatabase.rawQuery("SELECT * FROM " + inTable + " LIMIT 1", null);
            if (mCursor.getColumnIndex(columnToCheck) != -1)
                return true;
            else
                return false;
        } catch (Exception Exp) {
            // Something went wrong. Missing the database? The table?
            Log.e("existsColumnInTable", "When checking whether a column exists in the table, an error occurred: " + Exp.getMessage());
            return false;
        } finally {
            if (mCursor != null) mCursor.close();
        }
    }
}
