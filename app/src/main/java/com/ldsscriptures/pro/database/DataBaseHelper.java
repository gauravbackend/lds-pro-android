package com.ldsscriptures.pro.database;

import android.app.Activity;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Environment;
import android.util.Log;

import com.ldsscriptures.pro.utils.SessionManager;

import java.io.File;
import java.util.Locale;

public class DataBaseHelper extends SQLiteOpenHelper {


    private static String DB_PATH = "";

    private static String DB_NAME = "Catalog.sqlite";
    private static String DB_CITATION_NAME = "citations.sqlite";

    // tables name in Catalog.sqlite database...
    public static String LIBRARY_COLLECTION_TABLE = "library_collection";
    public static String LIBRARY_SECTION_TABLE = "library_section";
    public static String LIBRARY_ITEM_TABLE = "item";

    // SOME Common Params for all Tables
    public static String KEY_ID = "_id";
    public static String KEY_NAME = "NAME";
    public static String KEY_EXTERNAL_ID = "external_id";
    public static String KEY_POSITION = "position";

    //parameter of LIBRARY_COLLECTION_TABLE
    public static String KEY_LC_SECTION_ID = "library_section_id";
    public static String KEY_LC_LIB_SECTION_EXTERNAL_ID = "library_section_external_id";
    public static String KEY_LC_TITLE_HTML = "title_html";
    public static String KEY_LC_COVER_RENDITIONS = "cover_renditions";
    public static String KEY_LC_TYPE_ID = "type_id";

    //parameter of LIBRARY_SECTION_TABLE
    public static String KEY_LS_LIB_COLL_ID = "library_collection_id";
    public static String KEY_LS_LIB_COLL_EXTERNAL_ID = "library_collection_external_id";
    public static String KEY_LS_TITLE = "title";
    public static String KEY_LS_INDEX_TITLE = "index_title";

    //Parameters of click on LC in home screen
    public static String KEY_LS_RESULT_LIBRARY_SECTION_ID = "library_section_id";
    public static String KEY_LS_RESULT_LS_EXTERNAL_ID = "library_section_external_id";
    public static String KEY_LS_RESULT_TITLE_HTML = "title_html";
    public static String KEY_LS_RESULT_IS_OBSOLETE = "is_obsolete";
    public static String KEY_LS_RESULT_ITEM_ID = "item_id";
    public static String KEY_LS_RESULT_ITEM_EXTERNAL_ID = "item_external_id";
    public static String KEY_LS_RESULT_ITEM_COVER_RENDITIONS = "item_cover_renditions";
    public static String KEY_LS_RESULT_URI = "uri";
    public static String KEY_LS_RESULT_ITEMID = "itemid";

    private SQLiteDatabase myDataBase;
    private final Context myContext;

    public static boolean isDBFound = false;

    public DataBaseHelper(Context context) {
        super(context, DB_PATH + DB_NAME, null, 1);
        this.myContext = context;
        if (android.os.Build.VERSION.SDK_INT >= 17) {
            DB_PATH = Environment.getExternalStorageDirectory() + "/LDS/";
        } else {
            DB_PATH = Environment.getExternalStorageDirectory() + "/LDS/";
        }
    }

    public boolean checkDataBase() {

        SQLiteDatabase checkDB = null;
        try {
            String myPath = DB_PATH + DB_NAME;
            if (new File(myPath).exists()) {
                isDBFound = true;
                checkDB = SQLiteDatabase.openDatabase(myPath, null,
                        SQLiteDatabase.OPEN_READWRITE);
            } else {
                isDBFound = false;
            }
        } catch (Exception e) {
            isDBFound = false;
            if (checkDB != null) {
                checkDB.close();
            }
        } finally {
            if (checkDB != null) {
                checkDB.close();
            }
            this.close();
        }
        return checkDB != null ? true : false;
    }

    @Override
    public void onCreate(SQLiteDatabase arg0) {
        // TODO Auto-generated method stub
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // TODO Auto-generated method stub
    }

    public SQLiteDatabase openDataBase() throws SQLException {
        String myPath = DB_PATH + DB_NAME;

        myDataBase = SQLiteDatabase.openDatabase(myPath, null, SQLiteDatabase.OPEN_READWRITE);

        this.getReadableDatabase();
        this.getWritableDatabase();

        if (!myDataBase.isReadOnly()) {
            myDataBase.execSQL("PRAGMA foreign_keys=ON;");
        }
        return myDataBase;
    }

    public void closeDataBase() throws java.sql.SQLException {
        if (myDataBase != null && myDataBase.isOpen())
            myDataBase.close();
        super.close();
    }

    public Cursor getLCData() {
        Cursor cursor = null;
        try {
            openDataBase();
            String selectLcDataQuery = null;

            selectLcDataQuery = "SELECT lc.* , i.language_id, i.title FROM " + LIBRARY_COLLECTION_TABLE
                    + " lc, " + LIBRARY_ITEM_TABLE
                    + " i, " + LIBRARY_SECTION_TABLE + " ls where" +
                    " ls." + KEY_LS_LIB_COLL_ID + "=" + new SessionManager((Activity) myContext).getLangCollectionID() +
                    " and i.language_id=" + new SessionManager((Activity) myContext).getLanguageID() +
                    " and not(ls." + KEY_LS_TITLE + "='Support') " +
                    "and lc." + KEY_LC_SECTION_ID +
                    "=ls." + KEY_ID + " group by lc._id order by ls." + KEY_POSITION + ",lc." + KEY_POSITION + "";//" LIMIT 1";

            cursor = myDataBase.rawQuery(selectLcDataQuery, null);

            if (cursor != null) {
                cursor.moveToFirst();
            }

            closeDataBase();

        } catch (SQLException e) {
            e.printStackTrace();
        } catch (java.sql.SQLException e) {
            e.printStackTrace();
        }
        return cursor;
    }

    public Cursor getLSData(int itemId) {

        Cursor cursor = null;

        try {
            openDataBase();
            String selectImageQuery = null;

            selectImageQuery = "select li.*,i.item_cover_renditions,i.version,i.uri,i._id as itemid from library_section ls, library_item li, item i where i.is_obsolete=0 and ls.library_collection_id=" +
                    itemId +
                    " and ls._id=li.library_section_id and li.item_external_id=i.external_id order by ls.position,li.position";

            cursor = myDataBase.rawQuery(selectImageQuery, null);
            if (cursor != null) {
                cursor.moveToFirst();
            }
            closeDataBase();

        } catch (SQLException e) {
            e.printStackTrace();
        } catch (java.sql.SQLException e) {
            e.printStackTrace();
        }
        return cursor;
    }

    public Cursor getLSDataFirst(int itemId) {

        Cursor cursor = null;

        try {
            openDataBase();
            String selectImageQuery = null;

            selectImageQuery = "SELECT " +
                    "lc._id as _id," +
                    "lc.external_id as external_id," +
                    "lc.library_section_id as library_section_id," +
                    "lc.library_section_external_id as library_section_external_id," +
                    "lc.position as position," +
                    "lc.title_html as title_html," +
                    "lc.cover_renditions as item_cover_renditions" +
                    " FROM library_section ls, library_collection lc" +
                    " WHERE ls.library_collection_id=" +
                    itemId +
                    " AND ls.external_id=lc.library_section_external_id order by ls.position,lc.position";

            cursor = myDataBase.rawQuery(selectImageQuery, null);
            if (cursor != null) {
                cursor.moveToFirst();
            }
            closeDataBase();

        } catch (SQLException e) {
            e.printStackTrace();
        } catch (java.sql.SQLException e) {
            e.printStackTrace();
        }
        return cursor;
    }

    public boolean insertData(String tableName, ContentValues values) {
        try {
            openDataBase();
            myDataBase.insert(tableName, null, values);
            Log.e("data ", "inserted..");
            closeDataBase();
        } catch (java.sql.SQLException e) {
            e.printStackTrace();
        }


        return true;
    }

    public boolean updateRowData(String tableName, ContentValues values, String selection, String[] selectionArgs) {
        try {
            openDataBase();
            myDataBase.update(tableName, values, selection, selectionArgs);
            closeDataBase();
        } catch (java.sql.SQLException e) {
            e.printStackTrace();
        }
        return true;
    }

    public Cursor getFilteredLCData() {
        Cursor cursor = null;
        try {
            openDataBase();
            String selectLcDataQuery = null;
            String selectLsDataQuery = null;

            selectLcDataQuery = "SELECT lc._id FROM " + LIBRARY_COLLECTION_TABLE
                    + " lc, " + LIBRARY_SECTION_TABLE + " ls where" +
                    " ls." + KEY_LS_LIB_COLL_ID + "=" + new SessionManager((Activity) myContext).getLangCollectionID() +
                    " and not(ls." + KEY_LS_TITLE + "='Support') " +
                    "and lc." + KEY_LC_SECTION_ID +
                    "=ls." + KEY_ID + " order by ls." + KEY_POSITION + ",lc." + KEY_POSITION + "";


            selectLsDataQuery = "select ls._id, ls.library_collection_id, ls.title, i.uri, i._id as itemid,i.title as subTitle from library_section ls," +
                    "library_item li, item i where ls.library_collection_id = (" + selectLcDataQuery + ") and ls._id=li.library_section_id and li.item_external_id=i.external_id and not(i.item_cover_renditions='NULL') order by ls.position,li.position ";


            cursor = myDataBase.rawQuery(selectLsDataQuery, null);

            if (cursor != null) {
                cursor.moveToFirst();
            }

            closeDataBase();

        } catch (SQLException e) {
            e.printStackTrace();
        } catch (java.sql.SQLException e) {
            e.printStackTrace();
        }
        return cursor;
    }

    public Cursor getGroupFilteredLCData() {
        Cursor cursor = null;
        try {
            openDataBase();
            String selectLcDataQuery = null;
            String selectLsDataQuery = null;

            selectLcDataQuery = "SELECT lc._id FROM " + LIBRARY_COLLECTION_TABLE
                    + " lc, " + LIBRARY_SECTION_TABLE + " ls where" +
                    " ls." + KEY_LS_LIB_COLL_ID + "=" + new SessionManager((Activity) myContext).getLangCollectionID() +
                    " and not(ls." + KEY_LS_TITLE + "='Support') " +
                    "and lc." + KEY_LC_SECTION_ID +
                    "=ls." + KEY_ID + " order by ls." + KEY_POSITION + ",lc." + KEY_POSITION + "";


            selectLsDataQuery = "select ls.title from library_section ls," +
                    "library_item li, item i where ls.library_collection_id = (" + selectLcDataQuery + ") and ls._id=li.library_section_id and li.item_external_id=i.external_id and not(i.item_cover_renditions='NULL') GROUP BY ls.title order by ls.position, li.position ";


            cursor = myDataBase.rawQuery(selectLsDataQuery, null);

            if (cursor != null) {
                cursor.moveToFirst();
            }

            closeDataBase();

        } catch (SQLException e) {
            e.printStackTrace();
        } catch (java.sql.SQLException e) {
            e.printStackTrace();
        }
        return cursor;
    }

    public Cursor getLanguageData() {
        Cursor cursor = null;
        try {
            openDataBase();

            int languageID = 1;
            String deviceLanguage = "";
            deviceLanguage = Locale.getDefault().getLanguage();

            if (deviceLanguage.equals("ru")) {
                languageID = 174;
            } else if (deviceLanguage.equals("pt")) {
                languageID = 60;
            } else if (deviceLanguage.equals("es")) {
                languageID = 3;
            } else if (deviceLanguage.equals("de")) {
                languageID = 151;
            }

            String selectLcDataQuery = null;
            selectLcDataQuery = "select ln.name as NAME,l.* from language l, language_name ln where ln.localization_language_id=" + languageID + " and ln.language_id=l._id order by NAME";
            cursor = myDataBase.rawQuery(selectLcDataQuery, null);
            if (cursor != null) {
                cursor.moveToFirst();
            }

            closeDataBase();

        } catch (SQLException e) {
            e.printStackTrace();
        } catch (java.sql.SQLException e) {
            e.printStackTrace();
        }
        return cursor;
    }

    //=========================== Query For Get VerseText && VersePath ===========================//

    public Cursor getVerseTextAndVersePath(String VerseURI) {
        Cursor cursor = null;
        try {
            openDataBase();
            String selectLcDataQuery = null;

            selectLcDataQuery = "SELECT lc.* , i.language_id, i.title FROM " + LIBRARY_COLLECTION_TABLE
                    + " lc, " + LIBRARY_ITEM_TABLE
                    + " i, " + LIBRARY_SECTION_TABLE + " ls where" +
                    " ls." + KEY_LS_LIB_COLL_ID + "=" + new SessionManager((Activity) myContext).getLangCollectionID() +
                    " and i.language_id=" + new SessionManager((Activity) myContext).getLanguageID() +
                    " and not(ls." + KEY_LS_TITLE + "='Support')" +
                    " and lc." + KEY_LC_SECTION_ID +
                    "=ls." + KEY_ID +
                    " and lc.title_html LIKE" + " '" + VerseURI + "%'" +
                    " group by lc._id order by ls." + KEY_POSITION + ",lc." + KEY_POSITION + "";

            //Global.printLog("CITATION", "=======LC_QUERY=====" + selectLcDataQuery);

            cursor = myDataBase.rawQuery(selectLcDataQuery, null);

            if (cursor != null) {
                cursor.moveToFirst();
            }
            closeDataBase();

        } catch (SQLException e) {
            e.printStackTrace();
        } catch (java.sql.SQLException e) {
            e.printStackTrace();
        }
        return cursor;
    }

    public Cursor getVerseLSPath_new(int LcId, String VerseURI, String VerseURI1) {
        Cursor cursor = null;
        try {
            openDataBase();

            String selectImageQuery = null;
            selectImageQuery = "select ls.library_collection_id as LsId,li.*,i.item_cover_renditions,i.uri,i._id as itemid from library_section ls, library_item li, item i" +
                    " where ls.library_collection_id=" + LcId +
                    " and ls._id=li.library_section_id" +
                    " and li.item_external_id=i.external_id" +
                    " and i.uri in ('" + VerseURI + "'," + "'" + VerseURI1 + "')" +
                    " and not(i.item_cover_renditions='NULL')" +
                    " order by ls.position,li.position";

            cursor = myDataBase.rawQuery(selectImageQuery, null);

            //Global.printLog("CITATION", "=======LS_QUERY=====" + selectImageQuery);

            if (cursor != null) {
                cursor.moveToFirst();
            }
            closeDataBase();

        } catch (SQLException e) {
            e.printStackTrace();
        } catch (java.sql.SQLException e) {
            e.printStackTrace();
        }
        return cursor;
    }

    public Cursor getVerseTextAndVersePath_URI(String VerseURI) {
        Cursor cursor = null;
        try {
            openDataBase();
            String selectLcDataQuery = null;

            selectLcDataQuery = "SELECT item._id as ItemID," +
                    "library_item.library_section_id as LSID," +
                    "library_section.library_collection_id as LCID " +
                    "FROM item " +
                    "INNER JOIN library_item ON " +
                    "library_item.item_id = item._id " +
                    "INNER JOIN library_section ON " +
                    "library_section._id = library_item.library_section_id " +
                    "WHERE item.uri LIKE" + "'" + VerseURI + "%'" +
                    " AND item.language_id = " + "'" + new SessionManager((Activity) myContext).getLanguageID() + "'";

            // Global.printLog("CITATION", "=======LC_QUERY=====" + selectLcDataQuery);

            cursor = myDataBase.rawQuery(selectLcDataQuery, null);

            if (cursor != null) {
                cursor.moveToFirst();
            }
            closeDataBase();

        } catch (SQLException e) {
            e.printStackTrace();
        } catch (java.sql.SQLException e) {
            e.printStackTrace();
        }
        return cursor;
    }

    public Cursor getVerseLSPath(int LcId, String VerseURI) {
        Cursor cursor = null;
        try {
            openDataBase();

            String selectImageQuery = null;
            selectImageQuery = "select ls.library_collection_id as LsId,li.*,i.item_cover_renditions,i.uri,i._id as itemid from library_section ls, library_item li, item i" +
                    " where ls.library_collection_id=" + LcId +
                    " and ls._id=li.library_section_id" +
                    " and li.item_external_id=i.external_id" +
                    " and i.uri LIKE" + " '%/" + VerseURI + "'" +
                    " and not(i.item_cover_renditions='NULL')" +
                    " order by ls.position,li.position";

            cursor = myDataBase.rawQuery(selectImageQuery, null);

            //Global.printLog("CITATION", "=======LS_QUERY=====" + selectImageQuery);

            if (cursor != null) {
                cursor.moveToFirst();
            }
            closeDataBase();

        } catch (SQLException e) {
            e.printStackTrace();
        } catch (java.sql.SQLException e) {
            e.printStackTrace();
        }
        return cursor;
    }

    public Cursor getVerseLSPathEmpty(String VerseURI) {
        Cursor cursor = null;
        try {
            openDataBase();

            String selectImageQuery = null;
            selectImageQuery = "SELECT item._id as item_id,item.title as title_html FROM item WHERE uri = " + "'" +
                    VerseURI + "'" +
                    " AND language_id = " + new SessionManager((Activity) myContext).getLanguageID();

            cursor = myDataBase.rawQuery(selectImageQuery, null);

            //Global.printLog("EMPTYYYY", "=======LS_QUERY=====" + selectImageQuery);

            if (cursor != null) {
                cursor.moveToFirst();
            }
            closeDataBase();

        } catch (SQLException e) {
            e.printStackTrace();
        } catch (java.sql.SQLException e) {
            e.printStackTrace();
        }
        return cursor;
    }

}
