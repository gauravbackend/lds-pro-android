package com.ldsscriptures.pro.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.ldsscriptures.pro.utils.Global;
import com.ldsscriptures.pro.utils.SessionManager;

public class CommonDatabaseHelper {

    public static final String DATABASE_NAME = "LDSScriptures.db";

    public static final String KEY_ID = "_id";
    public static final String KEY_SERVERID = "serverid";
    public static final String KEY_LESSONNAME = "lessonname";
    public static final String KEY_LESSONDATE = "lessondate";
    public static final String KEY_LESSONDESC = "lessondesc";
    public static final String KEY_LESSONID = "lessonid";
    public static final String KEY_NOTETEXT = "notetext";
    public static final String KEY_LESSONTYPE = "lesson_type";
    public static final String KEY_URLTITLE = "urltitle";
    public static final String KEY_URL = "url";
    public static final String KEY_VERSEID = "verseID";
    public static final String KEY_VERSETEXT1 = "verseText";
    public static final String KEY_VERSEAID1 = "verseAID";
    public static final String KEY_VERSETITLE1 = "versetitle";
    public static final String KEY_VERSEAID2 = "verseaid";
    public static final String KEY_DELETED = "deleted";
    public static final String KEY_MODIFIED = "modified";
    public static final String KEY_URI = "uri";
    public static final String KEY_SORT_ORDER = "sort_order";

    // TABLE_JOURNAL field
    public static final String KEY_JOURNAL_TABLE_NAME = "tblJournal";
    public static final String KEY_JOURNALTITLE = "journaltitle";
    public static final String KEY_JOURNALTEXT = "journaltext";
    public static final String KEY_GROUPINGDATE = "groupingdate";

    public static final String TABLE_LESSONS = "tblLesson";
    public static final String TABLE_JOURNAL = "tblJournal";
    public static final String TABLE_SUB_ITEM_LESSONS = "tblLessonItem";

    // Table FOLLOWING Master
    public static final String USER_MASTER = "user_master";
    public static final String FOLLOWING_MASTER = "following_master";
    public static final String KEY_FRIEND_USER_ID = "friend_user_id";
    public static final String KEY_FRIEND_EMAIL = "friend_user_email";
    public static final String KEY_FRIEND_USERNAME = "friend_username";
    public static final String KEY_FRIEND_USER_CONTACT_NO = "friend_user_contact_no";
    public static final String KEY_FRIEND_USER_FLAG = "friend_user_flag";
    public static final String KEY_FRIEND_USER_INVITE_FLAG = "friend_user_invite_flag";

    // Table bookmark
    public static final String KEY_VERSEAID = "verseaid";
    public static final String KEY_ITEMURI = "itemuri";

    // Table bookmark
    public static final String TABLE_BOOKMARK_MARSTER = "tblBookmark";
    public static final String TABLE_BOOKMARK_MARSTER_TEMP = "tblBookmark_temp";

    public static final String TABLE_NOTE_MARSTER = "tblNoteDetail";
    public static final String TABLE_NOTE_MARSTER_TEMP = "tblNoteDetail_temp";

    // Table daily Master
    public static final String USER_DAILY = "daily_master";
    public static final String KEY_DAILY_ID = "daily_id";
    public static final String KEY_DAILY_LOCAL_USE_DATE = "daily_local_use_date";
    public static final String KEY_DAILY_LOCAL_USE_HOUR = "daily_local_use_hour";
    public static final String KEY_DAILY_MINUTES = "daily_minutes";
    public static final String KEY_DAILY_POINTS = "daily_points";
    public static final String KEY_DAILY_USER_ID = "user_id";

    //Table Citation Master
    public static final String CITATION_MASTER = "citation_master";
    public static final String KEY_CITATION_ID = "citation_id";
    public static final String KEY_CITATION_COUNT = "citation_count";
    public static final String KEY_CITATION_URI = "citation_uri";
    public static final String SERACH_MASTER = "search_master";
    public static final String KEY_SEARCH_ID = "search_id";
    public static final String KEY_SEARCH_STRING = "search_string";

    //Table Activity
    public static final String ACTIVITY_MASTER = "activity_master";
    public static final String KEY_ACTIVITY_ID = "activity_id";
    public static final String KEY_ACTIVITY_DATE = "activity_date";
    public static final String KEY_ACTIVITY_HOUR = "activity_hour";
    public static final String KEY_ACTIVITY_DAYOFWEEK = "activity_dayOfWeek";
    public static final String KEY_ACTIVITY_MONTH = "activity_month";
    public static final String KEY_ACTIVITY_YEAR = "activity_year";
    public static final String KEY_ACTIVITY_GOAL_POINT = "activity_goal_point";
    public static final String ACTIVITY_WEEKOFYEAR = "activity_weekofyear";
    public static final String KEY_ACTIVITY_WEEKSTART = "WeekStart";
    public static final String KEY_ACTIVITY_WEEKEND = "WeekEnd";
    public static final String KEY_ACTIVITY_DAYOFMONTH = "activity_DayofMonth";
    public static final String KEY_ACTIVITY_USER_ID = "activity_userId";

    // Table PLAN Master
    public static final String PLAN_MASTER = "plan_master";
    public static final String KEY_PLAN_ID = "plan_id";
    public static final String KEY_PLAN_NAME = "plan_name";
    public static final String KEY_PLAN_MAIN_LCID = "plan_Main_LCID";
    public static final String KEY_PLAN_SUB_LSID = "plan_Sub_LSID";
    public static final String KEY_PLAN_DAYS = "plan_days";
    public static final String KEY_PLAN_FLAG = "plan_flag";
    public static final String KEY_PLAN_READING_MIN = "plan_reading_min";
    public static final String KEY_PLAN_CREATED_DATE = "plan_created_date";

    public static final String KEY_TABLE_LESSON = "tblLesson";
    public static final String KEY_TABLE_LESSON_ITEM = "tblLessonItem";
    public static final String KEY_TABLE_JOURNAL = "tblJournal";


    String DATABASE_ALTER_LESSON_1 = "ALTER TABLE " + KEY_TABLE_LESSON + " ADD COLUMN " + KEY_DELETED + " text;";
    String DATABASE_ALTER_LESSON_2 = "ALTER TABLE " + KEY_TABLE_LESSON + " ADD COLUMN " + KEY_MODIFIED + " text;";

    String DATABASE_ALTER_LESSON_ITEM_1 = "ALTER TABLE " + KEY_TABLE_LESSON_ITEM + " ADD COLUMN " + KEY_DELETED + " text;";
    String DATABASE_ALTER_LESSON_ITEM_2 = "ALTER TABLE " + KEY_TABLE_LESSON_ITEM + " ADD COLUMN " + KEY_MODIFIED + " text;";
    String DATABASE_ALTER_LESSON_ITEM_3 = "ALTER TABLE " + KEY_TABLE_LESSON_ITEM + " ADD COLUMN " + KEY_SORT_ORDER + " text;";
    String DATABASE_ALTER_LESSON_ITEM_4 = "ALTER TABLE " + KEY_TABLE_LESSON_ITEM + " ADD COLUMN " + KEY_URI + " text;";

    String DATABASE_ALTER_JOURNAL_1 = "ALTER TABLE " + KEY_TABLE_JOURNAL + " ADD COLUMN " + KEY_DELETED + " text;";
    String DATABASE_ALTER_JOURNAL_2 = "ALTER TABLE " + KEY_TABLE_JOURNAL + " ADD COLUMN " + KEY_MODIFIED + " text;";


    private static final String CREATE_TABLE_LESSON_MASTER = "CREATE TABLE IF NOT EXISTS tblLesson " +
            "(_id integer primary key,serverid text,lessonname text,lessondate text,lessondesc text,deleted text,modified text," +
            " createdAt TIMESTAMP DEFAULT CURRENT_TIMESTAMP)";

    private static final String CREATE_TABLE_LESSON_ITEM_MASTER = "CREATE TABLE IF NOT EXISTS tblLessonItem" +
            " (_id integer primary key,lesson_type integer,itemtype text,serverid text," +
            "verseID text,verseText text,verseAID text,versetitle text,deleted text,modified text,uri text," +
            "lessonid text,notetext text,urltitle text,sort_order text, url text,createdAt TIMESTAMP DEFAULT CURRENT_TIMESTAMP)";

    private static final String CREATE_TABLE_JOURNAL_MASTER = "CREATE TABLE IF NOT EXISTS tblJournal " +
            "(_id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,serverid text,groupingdate text,journaltitle text,journaltext text,deleted text,modified text," +
            "createdAt TIMESTAMP DEFAULT CURRENT_TIMESTAMP)";

    private String CREATE_TABLE_BOOKMARK_MASTER = "CREATE TABLE IF NOT EXISTS " + TABLE_BOOKMARK_MARSTER +
            " (_id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,serverid text,verseaid text,chapteruri text,itemuri text,bookuri text,versetitle text,versenumber integer," +
            "versetext text,deleted text,modified text,sort_order text,createdat TIMESTAMP DEFAULT CURRENT_TIMESTAMP)";

    private String CREATE_TABLE_NOTE_MASTER = "CREATE TABLE IF NOT EXISTS " + TABLE_NOTE_MARSTER +
            " (_id integer primary key AUTOINCREMENT,notetext text,versetitle text,versenumber integer,versetext text," +
            "serverid text,verseaid text,chapteruri text,itemuri text,bookuri text,deleted text,modified text," +
            "createdAt TIMESTAMP DEFAULT CURRENT_TIMESTAMP)";

    public static final String CREATE_TABLE_FOLLOWING_MASTER = "CREATE TABLE " + FOLLOWING_MASTER + "("
            + KEY_FRIEND_USER_ID + " INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,"
            + KEY_FRIEND_USERNAME + " TEXT,"
            + KEY_FRIEND_USER_CONTACT_NO + " TEXT,"
            + KEY_FRIEND_USER_FLAG + " BOOL,"
            + KEY_FRIEND_USER_INVITE_FLAG + " BOOL,"
            + KEY_FRIEND_EMAIL + " TEXT" + ")";

    public static final String CREATE_TABLE_USER_MASTER = "CREATE TABLE " + USER_MASTER + "("
            + SessionManager.KEY_USER_SESSION_ID + " TEXT PRIMARY KEY NOT NULL,"
            + SessionManager.KEY_USER_ID + " TEXT,"
            + SessionManager.KEY_USER_USERNAME + " TEXT,"
            + SessionManager.KEY_USER_EMAIL + " TEXT,"
            + SessionManager.KEY_USR_IMG_URL + " TEXT,"
            + SessionManager.KEY_USER_FB_ID + " TEXT,"
            + SessionManager.KEY_USER_FB_USER + " TEXT,"
            + SessionManager.KEY_USER_PLAN_EXPIRE_DATE + " TEXT,"
            + SessionManager.KEY_USER_LAST_SYNC_DATE + " TEXT,"
            + SessionManager.KEY_USER_CONFIRMED + " TEXT,"
            + SessionManager.KEY_USER_SUBSCRIBED + " BOOL,"
            + SessionManager.KEY_USER_ACHIEVEMENTS + " INTEGER,"
            + SessionManager.KEY_USER_GOAL_POINTS + " TEXT,"
            + SessionManager.KEY_USER_DAY_STREAK + " INTEGER,"
            + SessionManager.KEY_USER_POINTS + " INTEGER,"
            + SessionManager.KEY_USER_ANNOTATIONS + " INTEGER,"
            + SessionManager.KEY_USER_GOAL_HIT + " INTEGER,"
            + SessionManager.KEY_USER_MIN_READING + " INTEGER,"
            + SessionManager.KEY_USER_PLAN_TYPE + " INTEGER"
            + ")";


    public static final String CREATE_TABLE_ACTIVITY_MASTER = "CREATE TABLE IF NOT EXISTS" + " " + ACTIVITY_MASTER + "("
            + KEY_ACTIVITY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,"
            + KEY_ACTIVITY_DATE + " TEXT,"
            + KEY_ACTIVITY_HOUR + " INTEGER,"
            + KEY_ACTIVITY_DAYOFWEEK + " INTEGER,"
            + KEY_ACTIVITY_MONTH + " INTEGER,"
            + KEY_ACTIVITY_YEAR + " INTEGER,"
            + ACTIVITY_WEEKOFYEAR + " INTEGER,"
            + KEY_ACTIVITY_DAYOFMONTH + " INTEGER,"
            + KEY_ACTIVITY_GOAL_POINT + " INTEGER,"
            + KEY_ACTIVITY_USER_ID + " INTEGER"
            + ")";

    public static final String CREATE_TABLE_PLAN_MASTER = "CREATE TABLE " + PLAN_MASTER + "("
            + KEY_PLAN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,"
            + KEY_PLAN_NAME + " TEXT,"
            + KEY_PLAN_MAIN_LCID + " INTEGER,"
            + KEY_PLAN_SUB_LSID + " INTEGER,"
            + KEY_PLAN_FLAG + " BOOL,"
            + KEY_PLAN_READING_MIN + " INTEGER,"
            + KEY_PLAN_CREATED_DATE + " TEXT,"
            + KEY_PLAN_DAYS + " TEXT" + ")";

    public static final String CREATE_TABLE_USER_POINTS_MINUTES = "CREATE TABLE " + USER_DAILY + "("
            + KEY_DAILY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,"
            + KEY_DAILY_LOCAL_USE_DATE + " TEXT,"
            + KEY_DAILY_LOCAL_USE_HOUR + " INTEGER,"
            + KEY_DAILY_MINUTES + " INTEGER,"
            + KEY_DAILY_POINTS + " INTEGER,"
            + KEY_DAILY_USER_ID + " INTEGER" +
            ")";

    public static final String CREATE_TABLE_CITATION = "CREATE TABLE " + CITATION_MASTER + "("
            + KEY_CITATION_ID + " INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,"
            + KEY_CITATION_COUNT + " TEXT,"
            + KEY_CITATION_URI + " TEXT" + ")";


    public static final String CREATE_TABLE_SEARCH = "CREATE TABLE " + SERACH_MASTER + "("
            + KEY_SEARCH_ID + " INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,"
            + KEY_SEARCH_STRING + " TEXT" + ")";


    private final Context context;
    private SQLiteDatabase sqLiteDb;
    SessionManager sessionManager;

    public CommonDatabaseHelper(Context context) {
        this.context = context;
        sessionManager = new SessionManager(context);
    }

    // ---opens the database---
    public CommonDatabaseHelper open() throws SQLException {
        DatabaseHelper dbHelper = new DatabaseHelper(context);

        sqLiteDb = dbHelper.getWritableDatabase();
        sqLiteDb = dbHelper.getReadableDatabase();

        if (!sqLiteDb.isReadOnly()) {
            // Enable foreign key constraints
            sqLiteDb.execSQL("PRAGMA foreign_keys=ON;");
        }
        return this;
    }

    // ---closes the database---
    public void close() {
        if (sqLiteDb != null && sqLiteDb.isOpen()) {
//            dbHelper.close();
            sqLiteDb.close();
        }
    }

    // ---Delete data from the table---
    public void clearDataFromTable() {
        try {
            open();
            deleteData(USER_MASTER);
            deleteData(KEY_TABLE_LESSON);
            deleteData(KEY_TABLE_LESSON_ITEM);
            deleteData(TABLE_JOURNAL);
            deleteData(FOLLOWING_MASTER);
            deleteData(PLAN_MASTER);
            deleteData(TABLE_BOOKMARK_MARSTER);
            deleteData(TABLE_NOTE_MARSTER);
            deleteData(ACTIVITY_MASTER);
            deleteData(USER_DAILY);
            deleteData(CITATION_MASTER);
            deleteData(SERACH_MASTER);
            close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public boolean insertData(String tableName, ContentValues values) {
        try {
            open();
            sqLiteDb.insert(tableName, null, values);
            close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return true;
    }

    public boolean insertDataIfNotExists(String tableName, ContentValues values) {
        try {
            open();
            sqLiteDb.insertWithOnConflict(tableName, null, values, SQLiteDatabase.CONFLICT_IGNORE);
            close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return true;
    }

    public boolean updateRowData(String tableName, ContentValues values, String selection, String[] selectionArgs) {
        try {
            open();
            sqLiteDb.update(tableName, values, selection, selectionArgs);
            close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return true;
    }

    public boolean deleteRowData(String tableName, String selection, String[] selectionArgs) {
        try {
            open();
            sqLiteDb.delete(tableName, selection, selectionArgs);
            close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return true;
    }

    public boolean deleteData(String tableName) {
        try {
            open();
            sqLiteDb.delete(tableName, null, null);
            close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return true;
    }

    public Cursor getLessonList() {
        Cursor cursor = null;
        try {
            open();
            String query = null;
            query = "SELECT * FROM tblLesson";
            cursor = sqLiteDb.rawQuery(query, null);
            if (cursor != null && sqLiteDb != null && sqLiteDb.isOpen()) {
                cursor.moveToFirst();
            }
            close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return cursor;
    }

    public Cursor getLessonLastItem() {
        Cursor cursor = null;
        try {
            open();
            String query = null;
            query = "SELECT * FROM tblLesson ORDER BY " + KEY_ID + " DESC LIMIT 1";
            cursor = sqLiteDb.rawQuery(query, null);
            if (cursor != null && sqLiteDb != null && sqLiteDb.isOpen()) {
                cursor.moveToFirst();
            }
            close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return cursor;
    }

    public int getJornalLastSyncItem() {
        int _id = 0;
        Cursor cursor = null;
        try {
            open();
            String query = null;
            query = "SELECT _id FROM " + TABLE_JOURNAL + " ORDER BY " + KEY_ID + " DESC LIMIT 1";
            cursor = sqLiteDb.rawQuery(query, null);
            if (cursor != null && sqLiteDb != null && sqLiteDb.isOpen()) {
                cursor.moveToFirst();
                _id = cursor.getInt(0);
            }
            close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return _id;
    }

    public Cursor getNotesList() {
        Cursor cursor = null;
        try {
            open();
            String query = null;
            query = "SELECT * FROM " + TABLE_NOTE_MARSTER;
            cursor = sqLiteDb.rawQuery(query, null);
            if (cursor != null && sqLiteDb != null && sqLiteDb.isOpen()) {
                cursor.moveToFirst();
            }
            close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return cursor;
    }

    public Cursor getBookmarkList() {
        Cursor cursor = null;
        try {
            open();
            String query = null;
            query = "SELECT * FROM tblBookmark" + " ORDER BY " + KEY_SORT_ORDER + " ASC";
            cursor = sqLiteDb.rawQuery(query, null);
            if (cursor != null && sqLiteDb != null && sqLiteDb.isOpen()) {
                cursor.moveToFirst();
            }
            close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return cursor;
    }

    public Cursor getJournalGroupList() {
        Cursor cursor = null;
        try {
            open();
            String query = null;
            query = "SELECT * FROM tblJournal GROUP BY groupingdate ORDER BY createdAt ASC;";
            cursor = sqLiteDb.rawQuery(query, null);
            if (cursor != null && sqLiteDb != null && sqLiteDb.isOpen()) {
                cursor.moveToFirst();
            }
            close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return cursor;
    }

    public Cursor getJournalList(String date) {
        Cursor cursor = null;
        try {
            open();
            String query = null;
            query = "select *from tblJournal where groupingdate='" + date + "'";
            cursor = sqLiteDb.rawQuery(query, null);
            if (cursor != null && sqLiteDb != null && sqLiteDb.isOpen()) {
                cursor.moveToFirst();
            }
            close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return cursor;
    }

    public Cursor getLessonSubList(String lessonId) {
        Cursor cursor = null;
        try {
            open();
            String query = null;
            query = "SELECT * FROM tblLessonItem WHERE lessonId" + "=" + "'" + lessonId + "'" + " ORDER BY " + KEY_SORT_ORDER + " ASC";
            cursor = sqLiteDb.rawQuery(query, null);
            if (cursor != null && sqLiteDb != null && sqLiteDb.isOpen()) {
                cursor.moveToFirst();
            }
            close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return cursor;
    }

    public Cursor getLessonSubListLastItem() {
        Cursor cursor = null;
        try {
            open();
            String query = null;
            query = "SELECT * FROM tblLessonItem ORDER BY " + KEY_ID + " DESC LIMIT 1";
            cursor = sqLiteDb.rawQuery(query, null);
            if (cursor != null) {
                cursor.moveToFirst();
            }
            close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return cursor;
    }

    public Cursor getLastSyncDate(String columnValue) {
        Cursor cursor = null;
        try {
            open();
            String selectRowQuery = null;
            selectRowQuery = "SELECT " + SessionManager.KEY_USER_LAST_SYNC_DATE + " FROM " + USER_MASTER + " WHERE "
                    + SessionManager.KEY_USER_ID + " = '" + columnValue + "'";
            cursor = sqLiteDb.rawQuery(selectRowQuery, null);

            if (cursor != null && sqLiteDb != null && sqLiteDb.isOpen()) {
                if (cursor.getCount() > 0) {
                    cursor.moveToFirst();
                    do {

                    } while (cursor.moveToNext());
                }
            }
            close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return cursor;
    }

    public Cursor getFollowingList() {
        Cursor cursor = null;
        try {
            open();
            String query = null;
            query = "SELECT * FROM " + FOLLOWING_MASTER;
            cursor = sqLiteDb.rawQuery(query, null);
            if (cursor != null && sqLiteDb != null && sqLiteDb.isOpen()) {
                cursor.moveToFirst();
            }
            close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return cursor;
    }

    public Cursor getPlanList() {
        Cursor cursor = null;
        try {
            open();
            String query = null;
            query = "SELECT * FROM " + PLAN_MASTER;
            cursor = sqLiteDb.rawQuery(query, null);
            if (cursor != null && sqLiteDb != null && sqLiteDb.isOpen()) {
                cursor.moveToFirst();
            }
            close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return cursor;
    }

    public Cursor getNotesList(String data_aid) {
        Cursor cursor = null;
        try {
            open();
            String query = null;
            query = "SELECT * FROM " + TABLE_NOTE_MARSTER + " where verseaid = '" + data_aid + "'";
            cursor = sqLiteDb.rawQuery(query, null);
            if (cursor != null && sqLiteDb != null && sqLiteDb.isOpen()) {
                cursor.moveToFirst();
            }
            close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return cursor;
    }

    public Cursor getBookmarkList(String data_aid) {
        Cursor cursor = null;
        try {
            open();
            String query = null;
            query = "SELECT * FROM " + TABLE_BOOKMARK_MARSTER + " where verseaid = '" + data_aid + "'";
            cursor = sqLiteDb.rawQuery(query, null);
            if (cursor != null) {
                cursor.moveToFirst();
            }
            close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return cursor;
    }

    public Cursor getWeekViseActivityRecord() {
        Cursor cursor = null;
        try {
            open();
            String query = null;

            query = "select min(activity_date) as WeekStart,max(activity_date) as WeekEnd,max(activity_goal_point) as activity_goal_point"
                    + "," +
                    KEY_ACTIVITY_ID + "," +
                    KEY_ACTIVITY_DATE + "," +
                    KEY_ACTIVITY_HOUR + "," +
                    KEY_ACTIVITY_DAYOFWEEK + "," +
                    KEY_ACTIVITY_MONTH + "," +
                    KEY_ACTIVITY_YEAR + " " +
                    "FROM" + " " + ACTIVITY_MASTER + " " +
                    "WHERE" + "  " + KEY_ACTIVITY_YEAR + "=" + "'" + Global.globalVarYear + "'" + " " + "AND" + " " +
                    KEY_ACTIVITY_USER_ID + "=" + "'" + Global.paraUserId + "'" + " " +
                    "GROUP BY" + " " + ACTIVITY_WEEKOFYEAR + "," + KEY_ACTIVITY_YEAR;

            cursor = sqLiteDb.rawQuery(query, null);
            if (cursor != null && sqLiteDb != null && sqLiteDb.isOpen()) {
                cursor.moveToFirst();
            }
            close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return cursor;
    }

    public Cursor getDayChartViseActivityRecord(String varDay) {
        Cursor cursor = null;
        try {
            open();
            String query = null;
            query = "SELECT max(activity_goal_point) as activity_goal_point"
                    + "," +
                    KEY_ACTIVITY_ID + "," +
                    KEY_ACTIVITY_DATE + "," +
                    KEY_ACTIVITY_HOUR + "," +
                    KEY_ACTIVITY_DAYOFWEEK + "," +
                    KEY_ACTIVITY_MONTH + "," +
                    KEY_ACTIVITY_YEAR + "," +
                    KEY_ACTIVITY_DAYOFMONTH + " " +
                    "FROM" + " " + ACTIVITY_MASTER + " " + "WHERE" + " " + KEY_ACTIVITY_DATE + "=" + "'" + varDay + "'" + " " + "AND" + " " +
                    KEY_ACTIVITY_USER_ID + "=" + "'" + Global.paraUserId + "'" + " " +
                    "GROUP BY" + " " + KEY_ACTIVITY_HOUR;
            cursor = sqLiteDb.rawQuery(query, null);
            if (cursor != null && sqLiteDb != null && sqLiteDb.isOpen()) {
                cursor.moveToFirst();
            }

            close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return cursor;
    }

    public Cursor getWeekChartViseActivityRecord(String weekStartString, String weekEndString) {

        Cursor cursor = null;
        try {
            open();
            String query = null;
            query = "SELECT max(activity_goal_point) as activity_goal_point"
                    + "," +
                    KEY_ACTIVITY_ID + "," +
                    KEY_ACTIVITY_DATE + "," +
                    KEY_ACTIVITY_HOUR + "," +
                    KEY_ACTIVITY_DAYOFWEEK + "," +
                    KEY_ACTIVITY_MONTH + "," +
                    KEY_ACTIVITY_YEAR + "," +
                    KEY_ACTIVITY_DAYOFMONTH + " " +
                    "FROM" + " " + ACTIVITY_MASTER + " " + "WHERE"
                    + " " + KEY_ACTIVITY_DATE + ">=" + "'" + weekStartString + "'"
                    + " AND " + KEY_ACTIVITY_DATE + "<=" + "'" + weekEndString + "'" + " " + "AND" + " " +
                    KEY_ACTIVITY_USER_ID + "=" + "'" + Global.paraUserId + "'" + " " +
                    "GROUP BY" + " " + KEY_ACTIVITY_DATE;
            cursor = sqLiteDb.rawQuery(query, null);
            if (cursor != null && sqLiteDb != null && sqLiteDb.isOpen()) {
                cursor.moveToFirst();
            }

            close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return cursor;
    }

    public Cursor getMonthChartViseActivityRecord(String monthStartString, String monthEndString) {
        Cursor cursor = null;
        try {
            open();
            String query = null;
            query = "SELECT max(activity_goal_point) as activity_goal_point"
                    + "," +
                    KEY_ACTIVITY_ID + "," +
                    KEY_ACTIVITY_DATE + "," +
                    KEY_ACTIVITY_HOUR + "," +
                    KEY_ACTIVITY_DAYOFWEEK + "," +
                    KEY_ACTIVITY_MONTH + "," +
                    KEY_ACTIVITY_YEAR + "," +
                    KEY_ACTIVITY_DAYOFMONTH + " " +
                    "FROM" + " " + ACTIVITY_MASTER + " " + "WHERE"
                    + " " + KEY_ACTIVITY_DATE + ">=" + "'" + monthStartString + "'"
                    + " AND " + KEY_ACTIVITY_DATE + "<=" + "'" + monthEndString + "'" + " " + "AND" + " " +
                    KEY_ACTIVITY_USER_ID + "=" + "'" + Global.paraUserId + "'" + " " +
                    "GROUP BY" + " " + KEY_ACTIVITY_DATE;
            cursor = sqLiteDb.rawQuery(query, null);
            if (cursor != null && sqLiteDb != null && sqLiteDb.isOpen()) {
                cursor.moveToFirst();
            }
            close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return cursor;
    }

    public Cursor getYearChartViseActivityRecord(String varYear) {
        Cursor cursor = null;
        try {
            open();
            String query = null;
            query = "SELECT max(activity_goal_point) as activity_goal_point"
                    + "," +
                    KEY_ACTIVITY_ID + "," +
                    KEY_ACTIVITY_DATE + "," +
                    KEY_ACTIVITY_HOUR + "," +
                    KEY_ACTIVITY_DAYOFWEEK + "," +
                    KEY_ACTIVITY_MONTH + "," +
                    KEY_ACTIVITY_YEAR + " " +
                    "FROM" + " " + ACTIVITY_MASTER + " " + "WHERE" + " " + KEY_ACTIVITY_YEAR + "=" + varYear + " " + "AND" + " " +
                    KEY_ACTIVITY_USER_ID + "=" + "'" + Global.paraUserId + "'" + " " +
                    "GROUP BY" + " " + KEY_ACTIVITY_MONTH;
            cursor = sqLiteDb.rawQuery(query, null);
            if (cursor != null && sqLiteDb != null && sqLiteDb.isOpen()) {
                cursor.moveToFirst();
            }
            close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return cursor;
    }

    public Cursor getDayViseActivityRecord() {
        Cursor cursor = null;
        try {
            open();
            String query = null;
            query = "SELECT max(activity_goal_point) as activity_goal_point"
                    + "," +
                    KEY_ACTIVITY_ID + "," +
                    KEY_ACTIVITY_DATE + "," +
                    KEY_ACTIVITY_HOUR + "," +
                    KEY_ACTIVITY_DAYOFWEEK + "," +
                    KEY_ACTIVITY_MONTH + "," +
                    KEY_ACTIVITY_YEAR + "," +
                    KEY_ACTIVITY_DAYOFMONTH + " " +
                    "FROM" + " " + ACTIVITY_MASTER + " " +
                    "WHERE" + "  " + KEY_ACTIVITY_YEAR + "=" + "'" + Global.globalVarYear + "'" + " " + "AND" + " " +
                    KEY_ACTIVITY_USER_ID + "=" + "'" + Global.paraUserId + "'" + " " +
                    "GROUP BY" + " " + KEY_ACTIVITY_DATE;
            cursor = sqLiteDb.rawQuery(query, null);
            if (cursor != null && sqLiteDb != null && sqLiteDb.isOpen()) {
                cursor.moveToFirst();
            }
            close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return cursor;
    }

    public Cursor getMonthViseActivityRecord() {
        Cursor cursor = null;
        try {
            open();
            String query = null;
            query = "SELECT min(activity_date) as WeekStart,max(activity_date) as WeekEnd,max(activity_goal_point) as activity_goal_point"
                    + "," +
                    KEY_ACTIVITY_ID + "," +
                    KEY_ACTIVITY_DATE + "," +
                    KEY_ACTIVITY_HOUR + "," +
                    KEY_ACTIVITY_DAYOFWEEK + "," +
                    KEY_ACTIVITY_MONTH + "," +
                    KEY_ACTIVITY_YEAR + "," +
                    KEY_ACTIVITY_DAYOFMONTH + " " +
                    "FROM" + " " + ACTIVITY_MASTER + " " +
                    "WHERE" + "  " + KEY_ACTIVITY_YEAR + "=" + "'" + Global.globalVarYear + "'" + " " + "AND" + " " +
                    KEY_ACTIVITY_USER_ID + "=" + "'" + Global.paraUserId + "'" + " " +
                    "GROUP BY" + " " + KEY_ACTIVITY_MONTH + "," + KEY_ACTIVITY_YEAR;
            cursor = sqLiteDb.rawQuery(query, null);
            if (cursor != null && sqLiteDb != null && sqLiteDb.isOpen()) {
                cursor.moveToFirst();
            }
            close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return cursor;
    }

    public Cursor getYearViseActivityRecord() {
        Cursor cursor = null;
        try {
            open();
            String query = null;
            query = "SELECT max(activity_goal_point) as activity_goal_point"
                    + "," +
                    KEY_ACTIVITY_ID + "," +
                    KEY_ACTIVITY_DATE + "," +
                    KEY_ACTIVITY_HOUR + "," +
                    KEY_ACTIVITY_DAYOFWEEK + "," +
                    KEY_ACTIVITY_MONTH + "," +
                    KEY_ACTIVITY_YEAR + "," +
                    KEY_ACTIVITY_DAYOFMONTH + " " +
                    "FROM" + " " + ACTIVITY_MASTER + " " +
                    "WHERE" + "  " + KEY_ACTIVITY_USER_ID + "=" + "'" + Global.paraUserId + "'" + " " + "AND" + " " +
                    KEY_ACTIVITY_USER_ID + "=" + "'" + Global.paraUserId + "'" + " " +
                    "GROUP BY" + " " + KEY_ACTIVITY_YEAR;
            cursor = sqLiteDb.rawQuery(query, null);
            if (cursor != null && sqLiteDb != null && sqLiteDb.isOpen()) {
                cursor.moveToFirst();
            }
            close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return cursor;
    }

    public int getPointLastRecord() {
        Cursor cursor = null;
        int sumPoint = 0;
        try {
            open();
            String query = null;

            String userId = sessionManager.getUserId();

            if (userId != null && !userId.isEmpty()) {
                query = "SELECT SUM(daily_points) FROM" + " " + USER_DAILY + " " +
                        "WHERE" + "  " + KEY_DAILY_USER_ID + "=" + "'" + userId + "'";

                cursor = sqLiteDb.rawQuery(query, null);

                if (cursor != null && sqLiteDb != null && sqLiteDb.isOpen()) {
                    cursor.moveToFirst();
                    sumPoint = cursor.getInt(0);
                }
                close();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return sumPoint;
    }

    public int getMinuteLastRecord() {
        int sumPoint = 0;
        Cursor cursor = null;
        try {
            open();
            String query = null;

            String userId = sessionManager.getUserId();

            if (userId != null && !userId.isEmpty()) {

                query = "SELECT SUM(daily_minutes) FROM" + " " + USER_DAILY + " " +
                        "WHERE" + "  " + KEY_DAILY_USER_ID + "=" + "'" + userId + "'";

                cursor = sqLiteDb.rawQuery(query, null);
                if (cursor != null && sqLiteDb != null && sqLiteDb.isOpen()) {
                    cursor.moveToFirst();
                    sumPoint = cursor.getInt(0);
                }
                close();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return sumPoint;
    }

    public boolean exiestJournalData(String serverid) {
        boolean isExiest = false;
        Cursor cursor = null;
        try {
            open();
            String query = null;

            query = "SELECT * FROM " + KEY_JOURNAL_TABLE_NAME + " WHERE " + KEY_SERVERID + " = " + "'" + serverid + "'";
            cursor = sqLiteDb.rawQuery(query, null);
            if (cursor != null && sqLiteDb != null && sqLiteDb.isOpen()) {
                cursor.moveToFirst();
                if (cursor.getCount() != 0) {
                    isExiest = true;
                }
            }
            close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return isExiest;
    }

    public boolean exiestBookmarkData(String serverid) {
        boolean isExiest = false;
        Cursor cursor = null;
        try {
            open();
            String query = null;

            query = "SELECT * FROM " + TABLE_BOOKMARK_MARSTER + " WHERE " + KEY_SERVERID + " = " + "'" + serverid + "'";
            cursor = sqLiteDb.rawQuery(query, null);
            if (cursor != null && sqLiteDb != null && sqLiteDb.isOpen()) {
                cursor.moveToFirst();
                if (cursor.getCount() != 0) {
                    isExiest = true;
                }
            }
            close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return isExiest;
    }

    public boolean exiestNoteData(String serverid) {
        boolean isExiest = false;
        Cursor cursor = null;
        try {
            open();
            String query = null;
            query = "SELECT * FROM " + TABLE_NOTE_MARSTER + " WHERE " + KEY_SERVERID + " = " + "'" + serverid + "'";
            cursor = sqLiteDb.rawQuery(query, null);
            if (cursor != null && sqLiteDb != null && sqLiteDb.isOpen()) {
                cursor.moveToFirst();
                if (cursor.getCount() != 0) {
                    isExiest = true;
                }
            }
            close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return isExiest;
    }

    public boolean exiestLessonData(String serverid) {
        boolean isExiest = false;
        Cursor cursor = null;
        try {
            open();
            String query = null;
            query = "SELECT * FROM " + KEY_TABLE_LESSON + " WHERE " + KEY_SERVERID + " = " + "'" + serverid + "'";
            cursor = sqLiteDb.rawQuery(query, null);
            if (cursor != null && sqLiteDb != null && sqLiteDb.isOpen()) {
                cursor.moveToFirst();
                if (cursor.getCount() != 0) {
                    isExiest = true;
                }
            }
            close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return isExiest;
    }

    public boolean exiestLessonItemData(String serverid) {
        boolean isExiest = false;
        Cursor cursor = null;
        try {
            open();
            String query = null;

            query = "SELECT * FROM " + KEY_TABLE_LESSON_ITEM + " WHERE " + KEY_SERVERID + " = " + "'" + serverid + "'";
            cursor = sqLiteDb.rawQuery(query, null);
            if (cursor != null && sqLiteDb != null && sqLiteDb.isOpen()) {
                cursor.moveToFirst();
                if (cursor.getCount() != 0) {
                    isExiest = true;
                }
            }
            close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return isExiest;
    }

    public Cursor getBookmarkPushToServer() {
        Cursor cursor = null;
        try {
            open();
            String query = null;
            query = "SELECT * FROM " + TABLE_BOOKMARK_MARSTER + " WHERE " + KEY_SERVERID + " = '" + 0 + "'";
            cursor = sqLiteDb.rawQuery(query, null);
            if (cursor != null && sqLiteDb != null && sqLiteDb.isOpen()) {
                cursor.moveToFirst();
            }
            close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return cursor;
    }

    public Cursor getNotePushToServer() {
        Cursor cursor = null;
        try {
            open();
            String query = null;
            query = "SELECT * FROM " + TABLE_NOTE_MARSTER + " WHERE " + KEY_SERVERID + " = '" + 0 + "'";
            cursor = sqLiteDb.rawQuery(query, null);
            if (cursor != null && sqLiteDb != null && sqLiteDb.isOpen()) {
                cursor.moveToFirst();
            }
            close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return cursor;
    }

    public Cursor getJournalPushToServer() {
        Cursor cursor = null;
        try {
            open();
            String query = null;
            query = "SELECT * FROM " + TABLE_JOURNAL + " WHERE " + KEY_SERVERID + " = '" + 0 + "'";
            cursor = sqLiteDb.rawQuery(query, null);
            if (cursor != null && sqLiteDb != null && sqLiteDb.isOpen()) {
                cursor.moveToFirst();
            }
            close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return cursor;
    }

    public Cursor getLessonPushToServer() {
        Cursor cursor = null;
        try {
            open();
            String query = null;
            query = "SELECT * FROM " + TABLE_LESSONS + " WHERE " + KEY_SERVERID + " = '" + 0 + "'";
            cursor = sqLiteDb.rawQuery(query, null);
            if (cursor != null && sqLiteDb != null && sqLiteDb.isOpen()) {
                cursor.moveToFirst();
            }
            close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return cursor;
    }

    public Cursor getLessonSubItemPushToServer() {
        Cursor cursor = null;
        try {
            open();
            String query = null;
            query = "SELECT * FROM " + TABLE_SUB_ITEM_LESSONS + " WHERE " + KEY_SERVERID + " = '" + 0 + "'";
            cursor = sqLiteDb.rawQuery(query, null);
            if (cursor != null && sqLiteDb != null && sqLiteDb.isOpen()) {
                cursor.moveToFirst();
            }
            close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return cursor;
    }

    public Cursor getUpdateBookmarkPushToServer() {
        Cursor cursor = null;
        try {
            open();
            String query = null;
            query = "SELECT * FROM " + TABLE_BOOKMARK_MARSTER + " WHERE " + KEY_MODIFIED + " = '" + 1 + "'" +
                    " AND " + KEY_SERVERID + " != '" + 0 + "'";
            cursor = sqLiteDb.rawQuery(query, null);
            if (cursor != null && sqLiteDb != null && sqLiteDb.isOpen()) {
                cursor.moveToFirst();
            }
            close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return cursor;
    }

    public Cursor getUpdateBookmarkSortingPushToServer() {
        Cursor cursor = null;
        try {
            open();
            String query = null;
            query = "SELECT * FROM " + TABLE_BOOKMARK_MARSTER + " WHERE " + KEY_MODIFIED + " = '" + 1 + "'"
                    + " AND " + KEY_SERVERID + " != '" + 0 + "'";
            cursor = sqLiteDb.rawQuery(query, null);
            if (cursor != null && sqLiteDb != null && sqLiteDb.isOpen()) {
                cursor.moveToFirst();
            }
            close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return cursor;
    }

    public Cursor getUpdateNotePushToServer() {
        Cursor cursor = null;
        try {
            open();
            String query = null;
            query = "SELECT * FROM " + TABLE_NOTE_MARSTER + " WHERE " + KEY_MODIFIED + " = '" + 1 + "'" +
                    " AND " + KEY_SERVERID + " != '" + 0 + "'";
            cursor = sqLiteDb.rawQuery(query, null);
            if (cursor != null && sqLiteDb != null && sqLiteDb.isOpen()) {
                cursor.moveToFirst();
            }
            close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return cursor;
    }

    public Cursor getUpdateLessonSortingPushToServer() {
        Cursor cursor = null;
        try {
            open();
            String query = null;
            query = "SELECT * FROM " + TABLE_SUB_ITEM_LESSONS + " WHERE " + KEY_MODIFIED + " = '" + 1 + "'"
                    + " AND " + KEY_SERVERID + " != '" + 0 + "'";

            cursor = sqLiteDb.rawQuery(query, null);
            if (cursor != null && sqLiteDb != null && sqLiteDb.isOpen()) {
                cursor.moveToFirst();
            }
            close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return cursor;
    }

    public Cursor getUpdateLessonPushToServer() {
        Cursor cursor = null;
        try {
            open();
            String query = null;
            query = "SELECT * FROM " + TABLE_LESSONS + " WHERE " + KEY_MODIFIED + " = '" + 1 + "'" +
                    " AND " + KEY_SERVERID + " != '" + 0 + "'";
            cursor = sqLiteDb.rawQuery(query, null);
            if (cursor != null && sqLiteDb != null && sqLiteDb.isOpen()) {
                cursor.moveToFirst();
            }
            close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return cursor;
    }

    public Cursor getDeleteBookmarkPushToServer() {
        Cursor cursor = null;
        try {
            open();
            String query = null;
            query = "SELECT * FROM " + TABLE_BOOKMARK_MARSTER + " WHERE " + KEY_DELETED + " = '" + 1 + "'";
            cursor = sqLiteDb.rawQuery(query, null);
            if (cursor != null && sqLiteDb != null && sqLiteDb.isOpen()) {
                cursor.moveToFirst();
            }
            close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return cursor;
    }

    public Cursor getDeleteNotePushToServer() {
        Cursor cursor = null;
        try {
            open();
            String query = null;
            query = "SELECT * FROM " + TABLE_NOTE_MARSTER + " WHERE " + KEY_DELETED + " = '" + 1 + "'";
            cursor = sqLiteDb.rawQuery(query, null);
            if (cursor != null && sqLiteDb != null && sqLiteDb.isOpen()) {
                cursor.moveToFirst();
            }
            close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return cursor;
    }

    public Cursor getDeleteLessonPushToServer() {
        Cursor cursor = null;
        try {
            open();
            String query = null;
            query = "SELECT * FROM " + TABLE_LESSONS + " WHERE " + KEY_DELETED + " = '" + 1 + "'";
            cursor = sqLiteDb.rawQuery(query, null);
            if (cursor != null && sqLiteDb != null && sqLiteDb.isOpen()) {
                cursor.moveToFirst();
            }
            close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return cursor;
    }

    public Cursor getDeleteLessonSubItemPushToServer() {
        Cursor cursor = null;
        try {
            open();
            String query = null;
            query = "SELECT * FROM " + TABLE_SUB_ITEM_LESSONS + " WHERE " + KEY_DELETED + " = '" + 1 + "'";
            cursor = sqLiteDb.rawQuery(query, null);
            if (cursor != null && sqLiteDb != null && sqLiteDb.isOpen()) {
                cursor.moveToFirst();
            }
            close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return cursor;
    }

    public Cursor getDeleteJournalPushToServer() {
        Cursor cursor = null;
        try {
            open();
            String query = null;
            query = "SELECT * FROM " + TABLE_JOURNAL + " WHERE " + KEY_DELETED + " = '" + 1 + "'";
            cursor = sqLiteDb.rawQuery(query, null);
            if (cursor != null && sqLiteDb != null && sqLiteDb.isOpen()) {
                cursor.moveToFirst();
            }
            close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return cursor;
    }

    public Cursor getUpdateJournalPushToServer() {
        Cursor cursor = null;
        try {
            open();
            String query = null;
            query = "SELECT * FROM " + TABLE_JOURNAL + " WHERE " + KEY_MODIFIED + " = '" + 1 + "'" +
                    " AND " + KEY_SERVERID + " != '" + 0 + "'";
            cursor = sqLiteDb.rawQuery(query, null);
            if (cursor != null && sqLiteDb != null && sqLiteDb.isOpen()) {
                cursor.moveToFirst();
            }
            close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return cursor;
    }

    public class DatabaseHelper extends SQLiteOpenHelper {

        public DatabaseHelper(Context context) {
            super(context, DATABASE_NAME, null, Global.DATABASE_VERSION);
        }

        @Override
        public void onCreate(SQLiteDatabase db) {

            db.execSQL(CREATE_TABLE_USER_MASTER);
            db.execSQL(CREATE_TABLE_LESSON_MASTER);
            db.execSQL(CREATE_TABLE_LESSON_ITEM_MASTER);
            db.execSQL(CREATE_TABLE_JOURNAL_MASTER);
            db.execSQL(CREATE_TABLE_FOLLOWING_MASTER);
            db.execSQL(CREATE_TABLE_PLAN_MASTER);
            db.execSQL(CREATE_TABLE_BOOKMARK_MASTER);
            db.execSQL(CREATE_TABLE_NOTE_MASTER);
            db.execSQL(CREATE_TABLE_ACTIVITY_MASTER);
            db.execSQL(CREATE_TABLE_USER_POINTS_MINUTES);
            db.execSQL(CREATE_TABLE_CITATION);
            db.execSQL(CREATE_TABLE_SEARCH);
        }

        @Override
        public void onUpgrade(SQLiteDatabase sqLiteDb, int oldVersion, int newVersion) {

            if (oldVersion != newVersion) {

                try {
                    if (!isFieldExist(sqLiteDb, KEY_TABLE_LESSON, KEY_DELETED)) {
                        sqLiteDb.execSQL(DATABASE_ALTER_LESSON_1);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

                try {
                    if (!isFieldExist(sqLiteDb, KEY_TABLE_LESSON, KEY_MODIFIED)) {
                        sqLiteDb.execSQL(DATABASE_ALTER_LESSON_2);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

                try {
                    if (!isFieldExist(sqLiteDb, KEY_TABLE_LESSON_ITEM, KEY_DELETED)) {
                        sqLiteDb.execSQL(DATABASE_ALTER_LESSON_ITEM_1);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

                try {
                    if (!isFieldExist(sqLiteDb, KEY_TABLE_LESSON_ITEM, KEY_MODIFIED)) {
                        sqLiteDb.execSQL(DATABASE_ALTER_LESSON_ITEM_2);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

                try {
                    if (!isFieldExist(sqLiteDb, KEY_TABLE_LESSON_ITEM, KEY_SORT_ORDER)) {
                        sqLiteDb.execSQL(DATABASE_ALTER_LESSON_ITEM_3);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

                try {
                    if (!isFieldExist(sqLiteDb, KEY_TABLE_LESSON_ITEM, KEY_URI)) {
                        sqLiteDb.execSQL(DATABASE_ALTER_LESSON_ITEM_4);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

                try {
                    if (!isFieldExist(sqLiteDb, KEY_TABLE_JOURNAL, KEY_DELETED)) {
                        sqLiteDb.execSQL(DATABASE_ALTER_JOURNAL_1);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

                try {
                    if (!isFieldExist(sqLiteDb, KEY_TABLE_JOURNAL, KEY_MODIFIED)) {
                        sqLiteDb.execSQL(DATABASE_ALTER_JOURNAL_2);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

                Global.printLog("ONUpgradeMethod", "=====ON UPGRADE===");

                try {
                    sqLiteDb.beginTransaction();
                    sqLiteDb.execSQL("ALTER TABLE " + TABLE_NOTE_MARSTER + " RENAME TO " + TABLE_NOTE_MARSTER_TEMP + ";");
                    sqLiteDb.execSQL(CREATE_TABLE_NOTE_MASTER);
                    String copyDataQuery = "INSERT INTO " + TABLE_NOTE_MARSTER + " SELECT * FROM " + TABLE_NOTE_MARSTER_TEMP;
                    sqLiteDb.execSQL(copyDataQuery);
                    sqLiteDb.execSQL("DROP table IF EXISTS " + TABLE_NOTE_MARSTER_TEMP);
                    sqLiteDb.setTransactionSuccessful();
                } catch (Exception e) {
                    e.printStackTrace();
                } finally {
                    sqLiteDb.endTransaction();
                }

                try {
                    sqLiteDb.beginTransaction();
                    sqLiteDb.execSQL("ALTER TABLE " + TABLE_BOOKMARK_MARSTER + " RENAME TO " + TABLE_BOOKMARK_MARSTER_TEMP + ";");
                    sqLiteDb.execSQL(CREATE_TABLE_BOOKMARK_MASTER);
                    String copyDataQuery = "INSERT INTO " + TABLE_BOOKMARK_MARSTER + " SELECT * FROM " + TABLE_BOOKMARK_MARSTER_TEMP;
                    sqLiteDb.execSQL(copyDataQuery);
                    sqLiteDb.execSQL("DROP table IF EXISTS " + TABLE_BOOKMARK_MARSTER_TEMP);
                    sqLiteDb.setTransactionSuccessful();
                } catch (Exception e) {
                    e.printStackTrace();
                } finally {
                    sqLiteDb.endTransaction();
                }
            }

            //sqLiteDb.execSQL("DROP table IF EXISTS " + DATABASE_NAME);
            //onCreate(sqLiteDb);
        }
    }

    private boolean isFieldExist(SQLiteDatabase inDatabase, String inTable, String columnToCheck) {
        Cursor mCursor = null;
        try {
            mCursor = inDatabase.rawQuery("SELECT * FROM " + inTable + " LIMIT 1", null);
            if (mCursor.getColumnIndex(columnToCheck) != -1)
                return true;
            else
                return false;
        } catch (Exception Exp) {
            // Something went wrong. Missing the database? The table?
            Log.e("existsColumnInTable", "When checking whether a column exists in the table, an error occurred: " + Exp.getMessage());
            return false;
        } finally {
            if (mCursor != null) mCursor.close();
        }
    }
}