package com.ldsscriptures.pro.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Environment;


public class CitationsDatabaseHelper {

    private static final int DATABASE_VERSION = 1;
    private static String DB_PATH = "";
    private static String DB_NAME = "citations.sqlite";
    private final Context context;
    private final String myPath;
    public static final String KEY_ID = "id";
    public static final String KEY_NAME = "name";
    public static final String KEY_CNT = "cnt";
    public static final String KEY_REF_URI = "ref_uri";
    public static final String KEY_SOURCE_GROUP = "source_group";
    public static final String KEY_SOURCE_SUBTITLE = "source_subtitle";

    private SQLiteDatabase sqLiteDb;

    public CitationsDatabaseHelper(Context context) {
        this.context = context;
        DB_PATH = Environment.getExternalStorageDirectory() + "/LDS/";
        myPath = DB_PATH + DB_NAME;
    }

    // ---opens the database---
    public CitationsDatabaseHelper open() throws SQLException {
        CitationsDatabaseHelper.DatabaseHelper dbHelper = new CitationsDatabaseHelper.DatabaseHelper(context);

        sqLiteDb = dbHelper.getWritableDatabase();
        sqLiteDb = dbHelper.getReadableDatabase();

        if (!sqLiteDb.isReadOnly()) {
            // Enable foreign key constraints
            sqLiteDb.execSQL("PRAGMA foreign_keys=ON;");
        }
        return this;
    }

    // ---closes the database---
    public void close() {
        if (sqLiteDb != null && sqLiteDb.isOpen()) {
//            dbHelper.close();
            sqLiteDb.close();
        }
    }

    public Cursor getSpecificContent() {
        Cursor cursor = null;
        try {
            open();
            String selectImageQuery = null;
            selectImageQuery = "select *,count(*) as cnt from citations group by ref_uri having count(*) >= 20 order by random() limit 1";
            cursor = sqLiteDb.rawQuery(selectImageQuery, null);

            if (cursor != null && sqLiteDb != null && sqLiteDb.isOpen()) {
                cursor.moveToFirst();
            }

            close();

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return cursor;
    }

    public Cursor getCitationList(String citeationItemUri, String citation_id) {
        Cursor cursor = null;
        try {
            open();
            String query = null;

            query = "select g.id,g.name,count(*) as cnt,c.ref_uri,c.source_group from citations c, groups g where c.ref_uri='" + citeationItemUri + "'  and c.ref_verse='" + citation_id + "' and g.id=c.source_group group by c.source_group;";
            cursor = sqLiteDb.rawQuery(query, null);

            if (cursor != null && sqLiteDb != null && sqLiteDb.isOpen()) {
                cursor.moveToFirst();
            }
            close();

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return cursor;
    }

    public Cursor getCitationSubList(String citeationItemUri, String sourceGroup, String citation_id) {
        Cursor cursor = null;
        try {
            open();
            String query = null;
            query = "SELECT * FROM citations WHERE source_group='" + sourceGroup + "' and ref_uri='" + citeationItemUri + "' and ref_verse='" + citation_id + "'";
            cursor = sqLiteDb.rawQuery(query, null);
            if (cursor != null && sqLiteDb != null && sqLiteDb.isOpen()) {
                cursor.moveToFirst();
            }
            close();

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return cursor;
    }

    public class DatabaseHelper extends SQLiteOpenHelper {

        public DatabaseHelper(Context context) {
            super(context, myPath, null, DATABASE_VERSION);
        }

        // Creating Tables
        @Override
        public void onCreate(SQLiteDatabase db) {

        }

        // Upgrading database
        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        }
    }

    public Cursor getCitationCountTable(String citeationItemUri) {

        Cursor cursor = null;
        try {
            open();
            String selectImageQuery = null;
            if (citeationItemUri.trim().length() != 0) {
                selectImageQuery = "SELECT * FROM citation_count WHERE uri='" + citeationItemUri + "'";
                cursor = sqLiteDb.rawQuery(selectImageQuery, null);
                if (cursor != null && sqLiteDb != null && sqLiteDb.isOpen()) {
                    cursor.moveToFirst();
                }
                close();
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return cursor;
    }

    public boolean insertData(String tableName, ContentValues values) {

        try {
            open();
            sqLiteDb.insert(tableName, null, values);
            close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return true;
    }

}





