package com.ldsscriptures.pro.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Environment;
import android.util.Log;

import java.io.File;
import java.io.IOException;

public class SubDataBaseHelper extends SQLiteOpenHelper {


    private static String SUB_DB_PATH = "";

    private static String SUB_DB_NAME = "package.sqlite";

    // SOME Common Params for all Tables
    public static String KEY_ID = "_id";
    public static String KEY_POSITION = "position";
    public static String KEY_TITLE_HTML = "title_html";
    public static String KEY_TITLE = "title";

    public static String KEY_NAV_SECTION_ID = "nav_section_id";
    public static String KEY_NAV_COLLECTION_ID = "nav_collection_id";
    public static String KEY_SUB_TITLE = "subtitle";
    public static String KEY_IMAGE_RENDITIONS = "image_renditions";
    public static String KEY_INDENT_LEVEL = "indent_level";
    public static String KEY_URI = "uri";

    public static String KEY_PREVIEW = "preview";
    public static String KEY_SUBITEM_ID = "subitem_id";

    public static String KEY_SUB_ITEM_HTML_C1CONTENT_HTML = "c1content_html";

    // reading screen audio parameter
    public static String KEY_AUDIO_SUBITEM_ID = "subitem_id";
    public static String KEY_AUDIO_MEDIA_URL = "media_url";
    public static String KEY_AUDIO_FILE_SIZE = "file_size";
    public static String KEY_AUDIO_DURATION = "duration";
    public static String KEY_AUDIO_REALTED_AUDIO_VOICE_ID = "related_audio_voice_id";

    // footnote parameter
    public static String KEY_FT_REF_ID = "ref_id";
    public static String KEY_FT_LABEL_HTML = "label_html";
    public static String KEY_FT_ORIGIN_ID = "origin_id";
    public static String KEY_FT_CONTENT_HTML = "content_html";
    public static String KEY_FT_WORD_OFFSET = "word_offset";
    public static String KEY_FT_BYTE_LOCATION = "byte_location";
    public static String KEY_FT_POSITION = "position";

    private SQLiteDatabase myDataBase;
    private final Context myContext;
    public String folderName;

    private static SQLiteDatabase db;
    public static boolean isDBFound = false;

    public SubDataBaseHelper(Context context, String lsId) {
        super(context, SUB_DB_PATH + SUB_DB_NAME, null, 20003);
        this.myContext = context;

        this.folderName = lsId;

        if (android.os.Build.VERSION.SDK_INT >= 17) {
            SUB_DB_PATH = Environment.getExternalStorageDirectory() + "/LDS/" + folderName + "/";
        } else {
            SUB_DB_PATH = Environment.getExternalStorageDirectory() + "/LDS/" + folderName + "/";
        }
    }

    public void createDataBase() throws IOException {
        //If the database does not exist, copy it from the assets.

        boolean mDataBaseExist = checkDataBase();
        if (!mDataBaseExist) {
            this.getReadableDatabase();
            this.close();
        }
    }

    public boolean checkDataBase() {
        try {
            if (android.os.Build.VERSION.SDK_INT >= 17) {
                SUB_DB_PATH = Environment.getExternalStorageDirectory() + "/LDS/" + folderName + "/";
            } else {
                SUB_DB_PATH = Environment.getExternalStorageDirectory() + "/LDS/" + folderName + "/";
            }
            String myPath = SUB_DB_PATH + SUB_DB_NAME;

            if (new File(myPath).exists()) {
                isDBFound = true;

            } else {
                isDBFound = false;
            }
        } catch (Exception e) {
            isDBFound = false;
        }

        return isDBFound;
    }

    public boolean checkDataBaseWithFolderName(String folderName) {

        SQLiteDatabase checkDB = null;
        try {
            if (android.os.Build.VERSION.SDK_INT >= 17) {
                SUB_DB_PATH = Environment.getExternalStorageDirectory() + "/LDS/" + folderName + "/";
            } else {
                SUB_DB_PATH = Environment.getExternalStorageDirectory() + "/LDS/" + folderName + "/";
            }
            String myPath = SUB_DB_PATH + SUB_DB_NAME;

            if (new File(myPath).exists()) {
                isDBFound = true;
                checkDB = SQLiteDatabase.openDatabase(myPath, null,
                        SQLiteDatabase.OPEN_READWRITE);
            } else {
                isDBFound = false;
            }
        } catch (Exception e) {
            isDBFound = false;
            Log.e("CheckDb", "DB not found");
            // database does't exist yet.
            if (checkDB != null) {
                checkDB.close();
            }
        } finally {
            if (checkDB != null) {
                checkDB.close();
            }
            this.close();
        }

        return checkDB != null ? true : false;
    }

    @Override
    public void onCreate(SQLiteDatabase arg0) {
        // TODO Auto-generated method stub
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // TODO Auto-generated method stub
    }

    public SQLiteDatabase openDataBase() throws SQLException {
        String myPath = SUB_DB_PATH + SUB_DB_NAME;
        if (new File(myPath).exists()) {
            return myDataBase = SQLiteDatabase.openDatabase(myPath, null, SQLiteDatabase.OPEN_READWRITE);
        }
        return null;
    }

    public synchronized void closeDataBase() throws java.sql.SQLException {
        if (myDataBase != null && myDataBase.isOpen())
            myDataBase.close();
        super.close();
    }

    public Cursor getFootNotes(int nextId) {
        Cursor cursor = null;
        try {
            openDataBase();

            SQLiteDatabase db = this.getReadableDatabase();
            db.getVersion();

            String selectionQuery = null;
            selectionQuery = "select * from related_content_item where subitem_id=" + nextId;
            cursor = myDataBase.rawQuery(selectionQuery, null);
            if (cursor != null) {
                cursor.moveToFirst();
            }
            closeDataBase();

        } catch (SQLException e) {
            e.printStackTrace();
        } catch (java.sql.SQLException e) {
            e.printStackTrace();
        }
        return cursor;
    }

    public Cursor getLsIndexDataS1() {
        Cursor cursor = null;
        try {
            openDataBase();

            SQLiteDatabase db = this.getReadableDatabase();
            db.getVersion();
            String selectionQuery = null;
            selectionQuery = "SELECT * FROM nav_collection where nav_section_id is NULL order by position";

            if (!selectionQuery.isEmpty()) {
                cursor = myDataBase.rawQuery(selectionQuery, null);
            }

            if (cursor != null) {
                cursor.moveToFirst();
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return cursor;
    }

    public Cursor getLsIndexDataS2(int nextId) {
        Cursor cursor = null;
        try {
            openDataBase();

            SQLiteDatabase db = this.getReadableDatabase();
            String selectionQuery = null;
            selectionQuery = "SELECT * FROM nav_section where nav_collection_id=" + nextId +
                    " order by position";

            cursor = myDataBase.rawQuery(selectionQuery, null);
            if (cursor != null) {
                cursor.moveToFirst();
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return cursor;
    }

    public Cursor getReadingHtmlData(int c0subitem_id) {
        Cursor cursor = null;
        try {
            openDataBase();

            SQLiteDatabase db = this.getReadableDatabase();
            String selectionQuery = null;
            selectionQuery = "select * from subitem_content_fts_content where c0subitem_id=" + c0subitem_id;
            cursor = myDataBase.rawQuery(selectionQuery, null);
            if (cursor != null) {
                cursor.moveToFirst();
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return cursor;
    }

    public Cursor getCrossRefReadingHtmlData(int c0subitem_id) {
        Cursor cursor = null;
        try {
            openDataBase();

            SQLiteDatabase db = this.getReadableDatabase();
            String selectionQuery = null;
            selectionQuery = "select * from subitem_content_fts_content where c0subitem_id=" + c0subitem_id;
            cursor = myDataBase.rawQuery(selectionQuery, null);
            if (cursor != null) {
                cursor.moveToFirst();
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return cursor;
    }

    public Cursor getReadingAudioFile(int c0subitem_id) {
        Cursor cursor = null;
        try {
            openDataBase();

            SQLiteDatabase db = this.getReadableDatabase();
            String selectionQuery = null;
            selectionQuery = "SELECT * FROM related_audio_item WHERE subitem_id=" + c0subitem_id;
            cursor = myDataBase.rawQuery(selectionQuery, null);
            if (cursor != null) {
                cursor.moveToFirst();
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return cursor;
    }

    public Cursor getLsIndexDataS3(int nextId) {
        Cursor cursor = null;
        try {
            openDataBase();

            SQLiteDatabase db = this.getReadableDatabase();
            String selectionQuery = null;
            selectionQuery = "SELECT * FROM nav_collection where nav_section_id=" +
                    nextId +
                    " order by position";
            cursor = myDataBase.rawQuery(selectionQuery, null);
            if (cursor != null) {
                cursor.moveToFirst();
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return cursor;
    }

    public Cursor getLsIndexDataS4(int nextId) {
        Cursor cursor = null;
        try {
            openDataBase();

            SQLiteDatabase db = this.getReadableDatabase();
            String selectionQuery = null;
            selectionQuery = "SELECT * FROM nav_item where nav_section_id=" +
                    nextId +
                    " order by position";
            cursor = myDataBase.rawQuery(selectionQuery, null);
            if (cursor != null) {
                cursor.moveToFirst();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return cursor;
    }

    public Cursor getTitleOnUrl(String uri) {
        Cursor cursor = null;
        try {
            openDataBase();

            SQLiteDatabase db = this.getReadableDatabase();
            String selectionQuery = null;
            selectionQuery = "select title_html from nav_item where uri='" + uri + "'";
            cursor = myDataBase.rawQuery(selectionQuery, null);
            if (cursor != null) {
                cursor.moveToFirst();
            }


        } catch (SQLException e) {
            e.printStackTrace();
        }
        return cursor;
    }

    public Cursor getLsIndexDataS5(int nextId) {
        Cursor cursor = null;
        try {
            openDataBase();

            SQLiteDatabase db = this.getReadableDatabase();
            String selectionQuery = null;
            selectionQuery = "SELECT * FROM nav_section where nav_collection_id=" +
                    nextId +
                    " order by position";
            cursor = myDataBase.rawQuery(selectionQuery, null);
            if (cursor != null) {
                cursor.moveToFirst();
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return cursor;
    }

    public Cursor getLsIndexDataS6(int nextId) {
        Cursor cursor = null;
        try {
            openDataBase();

            SQLiteDatabase db = this.getReadableDatabase();
            String selectionQuery = null;
            selectionQuery = "SELECT * FROM nav_item where nav_section_id=" +
                    nextId +
                    " order by position";
            cursor = myDataBase.rawQuery(selectionQuery, null);
            if (cursor != null) {
                cursor.moveToFirst();
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return cursor;
    }

    public Cursor getReadingData() {

        Cursor cursor = null;
        try {
            openDataBase();
            String selectionQuery = null;
            selectionQuery = "SELECT * FROM nav_item order by _id";

            cursor = myDataBase.rawQuery(selectionQuery, null);
            if (cursor != null) {
                cursor.moveToFirst();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return cursor;
    }

    public boolean insertData(String tableName, ContentValues values) {
        try {
            openDataBase();
            myDataBase.insert(tableName, null, values);
            closeDataBase();
        } catch (java.sql.SQLException e) {
            e.printStackTrace();
        }


        return true;
    }

    public boolean updateRowData(String tableName, ContentValues values, String selection, String[] selectionArgs) {
        try {
            openDataBase();
            myDataBase.update(tableName, values, selection, selectionArgs);
            closeDataBase();
        } catch (java.sql.SQLException e) {
            e.printStackTrace();
        }
        return true;
    }

    public Cursor getVerseTextFromURI(String VerseURI) {

        Cursor cursor = null;
        try {
            openDataBase();

            String selectionQuery = null;

            selectionQuery = "SELECT nav_item.uri," +
                    "subitem_content_fts_content.c0subitem_id," +
                    "subitem_content_fts_content.c1content_html," +
                    "nav_item._id," +
                    "nav_item.position," +
                    "nav_item.title_html " +
                    "FROM " +
                    "nav_item " +
                    "INNER JOIN " +
                    "subitem_content_fts_content " +
                    "ON nav_item._id = subitem_content_fts_content.c0subitem_id " +
                    "WHERE nav_item.uri=" + "'" + VerseURI + "'";

            cursor = myDataBase.rawQuery(selectionQuery, null);

            if (cursor != null) {
                cursor.moveToFirst();
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return cursor;
    }

    public Cursor getserachVerseTextFromURI(String VerseURI, int start_limit) {
        Cursor cursor = null;
        try {
            openDataBase();
            String selectionQuery = null;

            selectionQuery = "SELECT nav_item.uri," +
                    "subitem_content_fts_content.c0subitem_id," +
                    "subitem_content_fts_content.c1content_html," +
                    "nav_item._id," +
                    "nav_item.position," +
                    "nav_item.title_html " +
                    "FROM " +
                    "nav_item " +
                    "INNER JOIN " +
                    "subitem_content_fts_content " +
                    "ON nav_item._id = subitem_content_fts_content.c0subitem_id " +
                    "WHERE nav_item.uri LIKE" + " '" + VerseURI + "%'"//;
                    + "LIMIT" + " " + start_limit + "," + 3;

            cursor = myDataBase.rawQuery(selectionQuery, null);
            if (cursor != null) {
                cursor.moveToFirst();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return cursor;
    }

}
