package com.ldsscriptures.pro.service;

import android.annotation.SuppressLint;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Environment;
import android.os.IBinder;
import android.support.annotation.Nullable;

import com.ldsscriptures.pro.fragment.LibraryCollectionFragment;
import com.ldsscriptures.pro.model.Item;
import com.ldsscriptures.pro.utils.DownloadSubDBZipFile;
import com.ldsscriptures.pro.utils.Global;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;


public class DownloadSubDBService extends Service {

    private Context context;
    private ArrayList<Item> dbCatelogArraylist = new ArrayList<>();
    String Citation_Url, Citation_path;


    @SuppressLint("NewApi")
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        context = this;
        if (intent != null) {
            Citation_Url = (String) intent.getExtras().get("Citation_Url_intent");
            Citation_path = (String) intent.getExtras().get("Citation_path_intent");
        }

        dbCatelogArraylist = Global.getListSharedPreferneces(context);

        DownloadTask downloadTask = new DownloadTask();
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.HONEYCOMB) {
            downloadTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        } else {
            downloadTask.execute();
        }

        return START_STICKY;
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onStart(Intent intent, int startId) {
        super.onStart(intent, startId);
        context = this;
    }

    private class DownloadTask extends AsyncTask<Void, Void, Void> {

        @SuppressLint("SdCardPath")
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... params) {
            if (dbCatelogArraylist != null) {
                if (dbCatelogArraylist.size() > 30) {

                    for (int i = 0; i < 30; i++) {
                        if (!isFolderExists(dbCatelogArraylist.get(i).getId())) {


                            new DownloadSubDBZipFile(context, dbCatelogArraylist.get(i).getUrl(), dbCatelogArraylist.get(i).getId());
                        }
                    }

                } else {

                    for (int i = 0; i < dbCatelogArraylist.size(); i++) {
                        if (!isFolderExists(dbCatelogArraylist.get(i).getId())) {
                            new DownloadSubDBZipFile(context, dbCatelogArraylist.get(i).getUrl(), dbCatelogArraylist.get(i).getId());
                        }
                    }
                }
            }
            return null;
        }

        @SuppressLint({"SdCardPath", "NewApi"})
        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);

            DownloadTaskCitation downloadTask = new DownloadTaskCitation();
            downloadTask.execute(Citation_Url, Citation_path);

            if (LibraryCollectionFragment.getInstance() != null)
                LibraryCollectionFragment.getInstance().checkDatabse();
        }
    }

    private boolean isFolderExists(String id) {
        File destDir = new File(Global.AppFolderPath + id);

        if (!destDir.exists()) {
            return false;
        } else {
            if (destDir.isDirectory()) {
                String[] files = destDir.list();
                if (files == null)
                    return false;

                if (files.length == 0) {
                    //directory is empty
                    return false;
                } else
                    return true;
            }
        }
        return false;
    }

    public class DownloadTaskCitation extends AsyncTask<String, Integer, String> {

        @Override
        protected String doInBackground(String... sUrl) {
            InputStream input = null;
            OutputStream output = null;
            HttpURLConnection connection = null;
            try {

                Global.printLog("DOWNLOAD_FULL_DATABASE", "=====CITATION======DOWNLOAD========4444444====");

                String Citations_url = sUrl[0];
                String destPath = sUrl[1];

                Global.printLog("DOWNLOAD_FULL_DATABASE", "=====CITATION======Citations_url========4444444====" + Citations_url);
                Global.printLog("DOWNLOAD_FULL_DATABASE", "=====CITATION======destPath========4444444====" + destPath);

                URL url = new URL(Citations_url);

                File destDir1 = new File(destPath);
                if (!destDir1.exists()) {

                    Global.printLog("DOWNLOAD_FULL_DATABASE", "=====CITATION======DOWNLOAD========000000====");

                    connection = (HttpURLConnection) url.openConnection();
                    connection.connect();
                    if (connection.getResponseCode() != HttpURLConnection.HTTP_OK) {
                        return "Server returned HTTP " + connection.getResponseCode() + " " + connection.getResponseMessage();
                    }
                    int fileLength = connection.getContentLength();

                    String newFile = Environment.getExternalStorageDirectory() + "/LDS/";
                    String filename = "citations.sqlite";
                    String filePath = newFile + filename;

                    // download the file
                    input = connection.getInputStream();
                    output = new FileOutputStream(filePath);

                    byte data[] = new byte[4096];
                    long total = 0;
                    int count;
                    while ((count = input.read(data)) != -1) {
                        // allow canceling with back button
                        if (isCancelled()) {
                            input.close();
                            return null;
                        }
                        total += count;
                        // publishing the progress....
                        if (fileLength > 0) // only if total length is known
                            publishProgress((int) (total * 100 / fileLength));
                        output.write(data, 0, count);
                    }
                }

            } catch (Exception e) {
                return e.toString();
            } finally {
                try {
                    if (output != null)
                        output.close();
                    if (input != null)
                        input.close();
                } catch (IOException ignored) {
                }

                if (connection != null)
                    connection.disconnect();
            }
            return null;
        }
    }


}
