package com.ldsscriptures.pro.service;

import android.app.IntentService;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.ldsscriptures.pro.BuildConfig;
import com.ldsscriptures.pro.database.CommonDatabaseHelper;
import com.ldsscriptures.pro.database.HighlightDatabaseHelper;
import com.ldsscriptures.pro.model.AchivementsFlagModel;
import com.ldsscriptures.pro.model.SpannedTextData;
import com.ldsscriptures.pro.model.SyncItems;
import com.ldsscriptures.pro.model.SyncResponse;
import com.ldsscriptures.pro.model.VerseCrossReferenceData;
import com.ldsscriptures.pro.model.VerseTagsData;
import com.ldsscriptures.pro.retroutils.APIClient;
import com.ldsscriptures.pro.retroutils.APIInterface;
import com.ldsscriptures.pro.utils.Global;
import com.ldsscriptures.pro.utils.SessionManager;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;
import java.util.HashMap;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SyncIntentService extends IntentService {

    private static final String ACTION_START = "ACTION_START";
    private static final String ACTION_DELETE = "ACTION_DELETE";

    private SessionManager sessionManager;
    private SpannedTextData spannedTextData;
    private CommonDatabaseHelper commonDatabaseHelper;
    private HighlightDatabaseHelper highlightDb;
    private ArrayList<SyncItems> syncItemsArrayList = new ArrayList<SyncItems>();

    public SyncIntentService() {
        super(SyncIntentService.class.getSimpleName());
    }

    public static Intent createIntentStartNotificationService(Context context) {
        Intent intent = new Intent(context, SyncIntentService.class);
        intent.setAction(ACTION_START);
        return intent;
    }

    public static Intent createIntentDeleteNotification(Context context) {
        Intent intent = new Intent(context, SyncIntentService.class);
        intent.setAction(ACTION_DELETE);
        return intent;
    }

    @Override
    protected void onHandleIntent(Intent intent) {

        sessionManager = new SessionManager(this);
        commonDatabaseHelper = new CommonDatabaseHelper(this);
        highlightDb = new HighlightDatabaseHelper(this);

        boolean isChecked = sessionManager.getSyncStatus();
        if (isChecked) {
            boolean isOnWifi = sessionManager.getSyncWifiStatus();
            if (isOnWifi) {
                if (checkWifi()) {
                    syncData();
                }
            } else {
                syncData();
            }
        }
    }

    private void syncData() {

        APIInterface apiService = APIClient.getClient().create(APIInterface.class);
        String url = APIClient.BASE_URL;

        url += "sync.pl?user_id=" + getUserInfo(SessionManager.KEY_USER_ID) +
                "&session_id=" + getUserInfo(SessionManager.KEY_USER_SESSION_ID) +
                "&type=" + "" +
                "&device=" + APIClient.DEVICE_KEY +
                "&app_version=" + BuildConfig.VERSION_NAME +
                "&last_sync_date=" + sessionManager.getLastSyncDate() +
                "&locale=" + Global.LANGUAGE +
                "&apiauth=" + APIClient.AUTH_KEY;

        Call<SyncResponse<SyncItems>> callObject = apiService.syncData(url.replaceAll(" ", "%20"));

        callObject.enqueue(new Callback<SyncResponse<SyncItems>>() {

            @Override
            public void onResponse(Call<SyncResponse<SyncItems>> call, Response<SyncResponse<SyncItems>> response) {


                if (response.body().getLast_sync_date() != null) {
                    if (!response.body().getLast_sync_date().equals("null")) {

                        // store last_sync_date into session
                        sessionManager.setLastSyncDate(response.body().getLast_sync_date());
                    }
                }

                // store last_sync_date into session
                ContentValues contentValues_ = new ContentValues();
                contentValues_.put(SessionManager.KEY_USER_LAST_SYNC_DATE, response.body().getLast_sync_date());
                commonDatabaseHelper.updateRowData(CommonDatabaseHelper.USER_MASTER, contentValues_, SessionManager.KEY_USER_ID + " = ?",
                        new String[]{String.valueOf(getUserInfo(SessionManager.KEY_USER_ID))});

                syncItemsArrayList = response.body().getItems();
                if (syncItemsArrayList == null) syncItemsArrayList = new ArrayList<SyncItems>();

                // add lesson into local database
                for (int i = 0; i < syncItemsArrayList.size(); i++) {
                    ContentValues contentValues = new ContentValues();
                    SyncItems syncItems = syncItemsArrayList.get(i);

                    switch (syncItems.getType()) {

                        case "lesson":
                            boolean isLessonExiest = commonDatabaseHelper.exiestLessonData(syncItems.getId());
                            if (!isLessonExiest) {
                                contentValues.put("serverid", syncItems.getId());
                                contentValues.put("lessonname", syncItems.getTitle());
                                contentValues.put("lessondate", syncItems.getPresented());
                                contentValues.put("lessondesc", syncItems.getAudience());
                                contentValues.put(CommonDatabaseHelper.KEY_DELETED, syncItems.getDeleted());
                                contentValues.put(CommonDatabaseHelper.KEY_MODIFIED, syncItems.getModified());
                                commonDatabaseHelper.insertDataIfNotExists(CommonDatabaseHelper.TABLE_LESSONS, contentValues);
                            }
                            break;

                        case "lesson_item":
                            if (syncItems.getId() != null) {
                                boolean isLessonItemExiest = commonDatabaseHelper.exiestLessonItemData(syncItems.getId());
                                if (!isLessonItemExiest) {
                                    contentValues.put(CommonDatabaseHelper.KEY_LESSONID, syncItems.getLesson_id());
                                    contentValues.put(CommonDatabaseHelper.KEY_LESSONTYPE, syncItems.getItem_type());
                                    contentValues.put(CommonDatabaseHelper.KEY_URLTITLE, syncItems.getTitle());
                                    contentValues.put(CommonDatabaseHelper.KEY_NOTETEXT, syncItems.getNote());
                                    contentValues.put(CommonDatabaseHelper.KEY_VERSEID, syncItems.getVerse());
                                    contentValues.put(CommonDatabaseHelper.KEY_VERSEAID1, syncItems.getAid());
                                    contentValues.put(CommonDatabaseHelper.KEY_SERVERID, syncItems.getId());
                                    contentValues.put(CommonDatabaseHelper.KEY_URI, syncItems.getUri());
                                    contentValues.put(CommonDatabaseHelper.KEY_URL, syncItems.getUrl());
                                    contentValues.put(CommonDatabaseHelper.KEY_DELETED, syncItems.getDeleted());
                                    contentValues.put(CommonDatabaseHelper.KEY_MODIFIED, syncItems.getModified());
                                    contentValues.put(CommonDatabaseHelper.KEY_SORT_ORDER, syncItems.getSort_order());
                                    commonDatabaseHelper.insertDataIfNotExists(CommonDatabaseHelper.TABLE_SUB_ITEM_LESSONS, contentValues);
                                }
                                break;
                            }

                        case "journal":
                            boolean isJournalExiest = commonDatabaseHelper.exiestJournalData(syncItems.getId());
                            if (!isJournalExiest) {
                                contentValues.put(CommonDatabaseHelper.KEY_JOURNALTITLE, syncItems.getTitle());
                                contentValues.put(CommonDatabaseHelper.KEY_JOURNALTEXT, syncItems.getVerse());
                                contentValues.put(CommonDatabaseHelper.KEY_GROUPINGDATE, Global.getCurrentDate());
                                contentValues.put(CommonDatabaseHelper.KEY_SERVERID, syncItems.getId());
                                contentValues.put(CommonDatabaseHelper.KEY_DELETED, syncItems.getDeleted());
                                contentValues.put(CommonDatabaseHelper.KEY_MODIFIED, syncItems.getModified());
                                commonDatabaseHelper.insertDataIfNotExists(CommonDatabaseHelper.TABLE_JOURNAL, contentValues);
                            }
                            break;

                        case "bookmark":
                            boolean isBookmarkExiest = commonDatabaseHelper.exiestBookmarkData(syncItems.getId());
                            if (!isBookmarkExiest) {
                                contentValues.put("serverid", syncItems.getId());
                                contentValues.put("verseaid", syncItems.getAid());
                                contentValues.put("itemuri", syncItems.getUri());
                                contentValues.put("versetitle", syncItems.getTitle());
                                contentValues.put("versenumber", syncItems.getVerse());
                                contentValues.put(CommonDatabaseHelper.KEY_DELETED, syncItems.getDeleted());
                                contentValues.put(CommonDatabaseHelper.KEY_MODIFIED, syncItems.getModified());
                                contentValues.put(CommonDatabaseHelper.KEY_SORT_ORDER, syncItems.getSort_order());
                                commonDatabaseHelper.insertDataIfNotExists(CommonDatabaseHelper.TABLE_BOOKMARK_MARSTER, contentValues);
                            }
                            break;

                        case "tag":
                            updateTagsData(syncItems);
                            break;

                        case "note":
                            boolean isNoteExiest = commonDatabaseHelper.exiestNoteData(syncItems.getId());
                            if (!isNoteExiest) {
                                contentValues.put("notetext", syncItems.getNote());
                                contentValues.put("versetitle", syncItems.getTitle());
                                contentValues.put("versenumber", syncItems.getVerse());
                                contentValues.put("versetext", syncItems.getNote());
                                contentValues.put("serverid", syncItems.getId());
                                contentValues.put("verseaid", syncItems.getAid());
                                contentValues.put("itemuri", syncItems.getUri());
                                contentValues.put(CommonDatabaseHelper.KEY_DELETED, syncItems.getDeleted());
                                contentValues.put(CommonDatabaseHelper.KEY_MODIFIED, syncItems.getModified());
                                commonDatabaseHelper.insertDataIfNotExists(CommonDatabaseHelper.TABLE_NOTE_MARSTER, contentValues);
                            }
                            break;

                        case "achievements":
                            setAchivementFlag(syncItems);
                            break;

                        case "cross_ref":
                            updateCrossRef(syncItems);
                            break;

                        case "usage":
                            //sessionManager.setUserDailyPoints(Totalpoint);
                            //sessionManager.setUserMinReading(String.valueOf(Totalminut));
                            break;

                        case "highlight":
                            boolean isHighlightExiest = highlightDb.exiestHighLightData(syncItems.getId());
                            if (!isHighlightExiest) {
                                spannedTextData = new SpannedTextData(
                                        Integer.parseInt(syncItems.getAid()),
                                        Integer.parseInt(syncItems.getStart_index()),
                                        Integer.parseInt(syncItems.getEnd_index()),
                                        "",
                                        getColorName(syncItems.getColor()),
                                        Integer.parseInt(syncItems.getHighlight_type()),
                                        syncItems.getId(),
                                        syncItems.getDeleted(),
                                        syncItems.getModified(),
                                        "",
                                        "");
                                highlightDb.addHighLightData(spannedTextData);
                            }
                            break;

                        default:
                    }
                }

                // call the remain data again using "sync_again" with updated updated last sync data

                if (response.body().getSync_again() != null) {
                    if (response.body().getSync_again().equalsIgnoreCase("1")) {
                        syncData();
                    }
                }
            }

            @Override
            public void onFailure(Call<SyncResponse<SyncItems>> call, Throwable t) {
            }
        });
    }

    private void updateCrossRef(SyncItems syncItems) {
        ArrayList<VerseCrossReferenceData> verseCrossReferenceDataArrayList = new ArrayList<>();
        verseCrossReferenceDataArrayList = sessionManager.getPreferencesCrossRefArrayList(this);

        if (verseCrossReferenceDataArrayList == null)
            verseCrossReferenceDataArrayList = new ArrayList<VerseCrossReferenceData>();

        VerseCrossReferenceData verseCrossReferenceData = new VerseCrossReferenceData();

        if (verseCrossReferenceDataArrayList.size() > 0) {

            for (int i = 0; i < verseCrossReferenceDataArrayList.size(); i++) {
                VerseCrossReferenceData referenceData = verseCrossReferenceDataArrayList.get(i);

                if (referenceData.getRefAid1().equals(syncItems.getAid()) && referenceData.getRefAid2().equals(syncItems.getLink_aid())) {
                    referenceData.setServerId(syncItems.getId());
                    referenceData.setRefIndex1(syncItems.getVerse());
                    referenceData.setRefIndex2(syncItems.getLink_verse());
                    referenceData.setRefTitle1(syncItems.getUri());
                    referenceData.setRefTitle2(syncItems.getLink_uri());

                    verseCrossReferenceDataArrayList.set(i, referenceData);

                    break;
                }
            }

        } else {
            verseCrossReferenceData.setServerId(syncItems.getId());
            verseCrossReferenceData.setRefTitle1(syncItems.getUri());
            verseCrossReferenceData.setRefTitle2(syncItems.getLink_uri());
            verseCrossReferenceData.setRefIndex1(syncItems.getVerse());
            verseCrossReferenceData.setRefIndex2(syncItems.getLink_verse());
            verseCrossReferenceData.setRefAid1(syncItems.getAid());
            verseCrossReferenceData.setRefAid2(syncItems.getLink_aid());

            verseCrossReferenceDataArrayList.add(verseCrossReferenceData);
        }
        sessionManager.setPreferencesCrossRefArrayList(this, verseCrossReferenceDataArrayList);

        Gson gson = new Gson();

        String listString = gson.toJson(
                verseCrossReferenceDataArrayList,
                new TypeToken<ArrayList<VerseCrossReferenceData>>() {
                }.getType());

        try {
            JSONArray jsonArray = new JSONArray(listString);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void setAchivementFlag(SyncItems syncItems) {

        ArrayList<AchivementsFlagModel> achivementsArrList = new ArrayList<>();
        achivementsArrList = sessionManager.getAchivementsFlagArrayList();
        if (achivementsArrList == null) {
            achivementsArrList = new ArrayList<>();
            sessionManager.setAchivementsFlagArrayList();
        }

        for (int i = 0; i < achivementsArrList.size(); i++) {
            AchivementsFlagModel model = achivementsArrList.get(i);

            if (model.getIdentifier() != null) {
                if (model.getIdentifier().equalsIgnoreCase(syncItems.getAchievement_id())) {
                    model.setAchivements_flag(true);
                    model.setAchivements_dialogflag(true);
                    model.setServerid(syncItems.getId());
                    achivementsArrList.set(i, model);
                    break;
                }
            }

        }
        sessionManager.saveAchivementsFlagArraylist(achivementsArrList);
    }

    private void updateTagsData(SyncItems syncItems) {

        boolean isTagExiest = false;

        ArrayList<VerseTagsData> verseTagsDatasList = sessionManager.getPreferencesTagsArrayList(this);
        if (verseTagsDatasList != null) {
            for (int i = 0; i < verseTagsDatasList.size(); i++) {
                if (verseTagsDatasList.get(i).getServerid() != null) {
                    if (verseTagsDatasList.get(i).getServerid().equalsIgnoreCase(syncItems.getId())) {
                        isTagExiest = true;
                        break;
                    }
                }

            }
        }
        if (!isTagExiest) {
            if (syncItems.getDeleted().isEmpty()) {
                VerseTagsData verseTagsData = new VerseTagsData();
                verseTagsData.setServerid(syncItems.getId());
                verseTagsData.setVerseaid(syncItems.getAid());
                verseTagsData.setItemuri(syncItems.getUri());
                verseTagsData.setTagTitle(syncItems.getTitle());
                verseTagsData.setVerseNumber(syncItems.getVerse());
                verseTagsData.setTagname(syncItems.getTag());
                verseTagsData.setVerseText("");
                verseTagsData.setDeleted(syncItems.getDeleted());
                verseTagsData.setModified(syncItems.getModified());

                if (verseTagsDatasList != null) {
                    verseTagsDatasList.add(verseTagsData);
                } else {
                    verseTagsDatasList = new ArrayList<>();
                    verseTagsDatasList.add(verseTagsData);
                }
                sessionManager.setPreferencesTagsArrayList(this, verseTagsDatasList);
            }
        }

    }

    public String getUserInfo(String getStr) {
        String string = null;
        if (sessionManager.isLoggedIn()) {
            HashMap<String, String> hashMap = sessionManager.getLoginDetails();
            string = hashMap.get(getStr);

        }
        return string;
    }

    private boolean checkWifi() {
        ConnectivityManager connManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo mWifi = connManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);

        if (mWifi.isConnected()) {
            return true;
        }
        return false;
    }

    private String getColorName(String colorCode) {
        String colorName = "";

        switch (colorCode) {
            case "red":
                colorName = "#d83a35";
                break;
            case "orange":
                colorName = "#Ec7235";
                break;
            case "yellow":
                colorName = "#Fbf351";
                break;
            case "green":
                colorName = "#98c256";
                break;
            case "blue":
                colorName = "#4faaef";
                break;
            case "dark_blue":
                colorName = "#27539f";
                break;
            case "purple":
                colorName = "#7f63a2";
                break;
            case "pink":
                colorName = "#E79dbd";
                break;
            case "brown":
                colorName = "#Bd9c72";
                break;
            case "grey":
                colorName = "#b6b6b6";
                break;
        }

        return colorName;
    }


}
