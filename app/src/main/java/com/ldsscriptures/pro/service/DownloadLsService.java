package com.ldsscriptures.pro.service;

import android.annotation.SuppressLint;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.IBinder;
import android.util.Log;

import com.ldsscriptures.pro.R;
import com.ldsscriptures.pro.database.SubDataBaseHelper;
import com.ldsscriptures.pro.dbClass.GetDataFromDatabase;
import com.ldsscriptures.pro.dbClass.GetLibCollectionFromDB;
import com.ldsscriptures.pro.fragment.LibraryCollectionFragment;
import com.ldsscriptures.pro.fragment.LibrarySelectionFragment;
import com.ldsscriptures.pro.model.LsData;
import com.ldsscriptures.pro.utils.DownloadSingleSubDBZipFile;
import com.ldsscriptures.pro.utils.Global;

import java.io.File;
import java.util.ArrayList;


public class DownloadLsService extends Service {
    private Context context;
    private static final String TAG = "DownloadLsService";
    private ArrayList<LsData> lsDataArrayList;
    private ArrayList<LsData> lsArrayListToDownload = new ArrayList<>();
    private String LcID;

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @SuppressLint("NewApi")
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        context = this;
        Global global = new Global(context);
        lsArrayListToDownload = new ArrayList<>();
        if (intent != null)
            LcID = intent.getStringExtra(getString(R.string.lcID));
        if (LcID != null) {
            lsDataArrayList = new GetLibCollectionFromDB(context).getLsDataFromDb(LcID);
        }
        if (lsDataArrayList != null)
            for (int i = 0; i < lsDataArrayList.size(); i++) {
                SubDataBaseHelper subDataBaseHelper = new SubDataBaseHelper(context, "");
                if (!subDataBaseHelper.checkDataBaseWithFolderName(String.valueOf(lsDataArrayList.get(i).getItem_id()))) {
                    lsArrayListToDownload.add(lsDataArrayList.get(i));
                    lsDataArrayList.get(i).setDownloaded(false);
                } else {
                    lsDataArrayList.get(i).setDownloaded(true);
                }
            }

        DownloadTask downloadTask = new DownloadTask();
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.HONEYCOMB) {
            downloadTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        } else {
            downloadTask.execute();
        }


        return START_STICKY;
    }

    @Override
    public void onStart(Intent intent, int startId) {
        super.onStart(intent, startId);
        context = this;
    }


    private class DownloadTask extends AsyncTask<Void, Void, Void> {

        @SuppressLint("SdCardPath")
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... params) {

            if (lsArrayListToDownload != null && lsArrayListToDownload.size() > 0) {
                for (int i = 0; i < lsArrayListToDownload.size(); i++) {
                    if (!isFolderExists(String.valueOf(lsArrayListToDownload.get(i).getItem_id()))) {
                        if (LibraryCollectionFragment.getInstance() != null) {
                            String itemURL = LibraryCollectionFragment.getInstance().getItemURL(lsArrayListToDownload.get(i).getItem_id());
                            new DownloadSingleSubDBZipFile(context, itemURL, String.valueOf(lsArrayListToDownload.get(i).getItem_id()));
                       }
                    }
                }
            }
            return null;
        }

        @SuppressLint({"SdCardPath", "NewApi"})
        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
        }
    }

    private boolean isFolderExists(String id) {
        File destDir = new File(Global.AppFolderPath + id);

        if (!destDir.exists()) {
            return false;
        } else {
            if (destDir.isDirectory()) {
                String[] files = destDir.list();
                if (files == null)
                    return false;

                if (files.length == 0) {
                    //directory is empty
                    return false;
                } else
                    return true;
            }
        }
        return false;
    }
}
