package com.ldsscriptures.pro.service;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.AsyncTask;

import com.ldsscriptures.pro.utils.DownloadUtility;

/**
 * Created on 03/06/17 by iblinfotech.
 */

public class DownloadSubItem extends AsyncTask<Void, Void, Void> {

    public static String mUrl;
    public static String mID;
    public static Context context;

    @SuppressLint("SdCardPath")
    @Override
    protected void onPreExecute() {
        if (context != null)
            super.onPreExecute();
    }

    @Override
    protected Void doInBackground(Void... params) {
        new DownloadUtility().downloadDBZipFile(mUrl, mID);
        return null;
    }

    @SuppressLint({"SdCardPath", "NewApi"})
    @Override
    protected void onPostExecute(Void result) {
        super.onPostExecute(result);

    }
}
