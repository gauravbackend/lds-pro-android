package com.ldsscriptures.pro.service;

import android.app.IntentService;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.support.v4.content.WakefulBroadcastReceiver;

import com.ldsscriptures.pro.database.CommonDatabaseHelper;
import com.ldsscriptures.pro.utils.Global;
import com.ldsscriptures.pro.utils.SessionManager;

import java.text.SimpleDateFormat;
import java.util.Calendar;

public class ActivityIntentService extends IntentService {

    private static final String ACTION_START = "ACTION_START";
    private static final String ACTION_DELETE = "ACTION_DELETE";

    SessionManager sessionManager;
    String currantDate;
    int userGoalPoint, currantDayWeek, currantDayHour, currantDayMonth, currantDayYear, currantDayWeekofyear, currantDayofMonth;
    CommonDatabaseHelper commonDatabaseHelper;
    Calendar calendar;
    String paraUserId;

    public ActivityIntentService() {
        super(ActivityIntentService.class.getSimpleName());
    }

    public static Intent createIntentStartNotificationService(Context context) {
        Intent intent = new Intent(context, ActivityIntentService.class);
        intent.setAction(ACTION_START);
        return intent;
    }

    public static Intent createIntentDeleteNotification(Context context) {
        Intent intent = new Intent(context, ActivityIntentService.class);
        intent.setAction(ACTION_DELETE);
        return intent;
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        commonDatabaseHelper = new CommonDatabaseHelper(this);

        sessionManager = new SessionManager(getApplicationContext());
        currantDate = new SimpleDateFormat("yyyy-MM-dd").format(Calendar.getInstance().getTime());
        userGoalPoint = sessionManager.getUserDailyPoints();
        calendar = Calendar.getInstance();
        currantDayWeek = calendar.get(Calendar.DAY_OF_WEEK);
        currantDayHour = calendar.get(Calendar.HOUR_OF_DAY);
        currantDayMonth = calendar.get(Calendar.MONTH) + 1;
        currantDayYear = calendar.get(Calendar.YEAR);
        currantDayWeekofyear = calendar.get(Calendar.WEEK_OF_YEAR);
        currantDayofMonth = calendar.get(Calendar.DAY_OF_MONTH);

        paraUserId = sessionManager.getUserInfo(SessionManager.KEY_USER_ID);

        try {
            String action = intent.getAction();
            if (ACTION_START.equals(action)) {
                insertActivityData(currantDate, currantDayHour, currantDayWeek, currantDayMonth, currantDayYear, currantDayWeekofyear, currantDayofMonth, userGoalPoint);
            }
        } finally {
            WakefulBroadcastReceiver.completeWakefulIntent(intent);
        }
    }

    public void insertActivityData(String currantDate, int currantDayHour, int currantDayWeek, int currantDayMonth, int currantDayYear, int currantDayWeekofMonth, int currantDayofMonth, int userGoalPoint) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(CommonDatabaseHelper.KEY_ACTIVITY_DATE, currantDate);
        contentValues.put(CommonDatabaseHelper.KEY_ACTIVITY_HOUR, currantDayHour);
        contentValues.put(CommonDatabaseHelper.KEY_ACTIVITY_DAYOFWEEK, currantDayWeek);
        contentValues.put(CommonDatabaseHelper.KEY_ACTIVITY_MONTH, currantDayMonth);
        contentValues.put(CommonDatabaseHelper.KEY_ACTIVITY_YEAR, currantDayYear);
        contentValues.put(CommonDatabaseHelper.ACTIVITY_WEEKOFYEAR, currantDayWeekofMonth);
        contentValues.put(CommonDatabaseHelper.KEY_ACTIVITY_DAYOFMONTH, currantDayofMonth);
        contentValues.put(CommonDatabaseHelper.KEY_ACTIVITY_GOAL_POINT, userGoalPoint);
        contentValues.put(CommonDatabaseHelper.KEY_ACTIVITY_USER_ID, paraUserId);

        commonDatabaseHelper.insertData(CommonDatabaseHelper.ACTIVITY_MASTER, contentValues);
    }

}
