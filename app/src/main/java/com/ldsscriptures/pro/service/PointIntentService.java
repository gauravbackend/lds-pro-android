package com.ldsscriptures.pro.service;

import android.app.IntentService;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v4.content.WakefulBroadcastReceiver;

import com.ldsscriptures.pro.BuildConfig;
import com.ldsscriptures.pro.database.CommonDatabaseHelper;
import com.ldsscriptures.pro.model.UsageDetail;
import com.ldsscriptures.pro.retroutils.APIClient;
import com.ldsscriptures.pro.retroutils.APIInterface;
import com.ldsscriptures.pro.utils.SessionManager;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PointIntentService extends IntentService {

    private static final String ACTION_START = "ACTION_START";
    private static final String ACTION_DELETE = "ACTION_DELETE";

    public PointIntentService() {
        super(PointIntentService.class.getSimpleName());
    }

    public static Intent createIntentStartNotificationService(Context context) {
        Intent intent = new Intent(context, PointIntentService.class);
        intent.setAction(ACTION_START);
        return intent;
    }

    public static Intent createIntentDeleteNotification(Context context) {
        Intent intent = new Intent(context, PointIntentService.class);
        intent.setAction(ACTION_DELETE);
        return intent;
    }

    @Override
    protected void onHandleIntent(Intent intent) {

        try {
            String action = intent.getAction();
            if (ACTION_START.equals(action)) {
                new PointMinuteAPI().execute();
            }
        } finally {
            WakefulBroadcastReceiver.completeWakefulIntent(intent);
        }
    }

    private class PointMinuteAPI extends AsyncTask<String, Void, String> {

        SessionManager sessionManager;
        String currantDate;
        int currantDayWeek, currantDayHour, currantDayMonth, currantDayYear, currantDayWeekofyear, currantDayofMonth;
        CommonDatabaseHelper commonDatabaseHelper;
        Calendar calendar;
        String paraUserId, paraSessionId;
        int currantPoint = 0, currantMinute = 0, currantAudioPoint = 0, currantAudioMinute = 0;
        int userpoint = 0, userminute = 0;

        @Override
        protected void onPreExecute() {

        }

        @Override
        protected String doInBackground(final String... params) {

            commonDatabaseHelper = new CommonDatabaseHelper(getApplicationContext());
            sessionManager = new SessionManager(getApplicationContext());
            currantDate = new SimpleDateFormat("yyyy-MM-dd").format(Calendar.getInstance().getTime());
            calendar = Calendar.getInstance();
            currantDayWeek = calendar.get(Calendar.DAY_OF_WEEK);
            currantDayHour = calendar.get(Calendar.HOUR);
            currantDayMonth = calendar.get(Calendar.MONTH) + 1;
            currantDayYear = calendar.get(Calendar.YEAR);
            currantDayWeekofyear = calendar.get(Calendar.WEEK_OF_YEAR);
            currantDayofMonth = calendar.get(Calendar.DAY_OF_MONTH);

            currantAudioPoint = sessionManager.getUserMinAudioPoints();
            if (currantAudioPoint != 0) {
                currantPoint = currantAudioPoint + sessionManager.getUserDailyPoints();
            } else {
                currantPoint = sessionManager.getUserDailyPoints();
            }

            currantAudioMinute = sessionManager.getKeyUserMinAudio();
            if (currantAudioMinute != 0) {
                currantMinute = currantAudioMinute + Integer.parseInt(sessionManager.getUserMinReading());
            } else {
                currantMinute = Integer.parseInt(sessionManager.getUserMinReading());
            }

            paraUserId = sessionManager.getUserInfo(SessionManager.KEY_USER_ID);
            paraSessionId = sessionManager.getUserInfo(SessionManager.KEY_USER_SESSION_ID);

            DailyPointData();

            return "";
        }

        @Override
        protected void onPostExecute(String result) {
        }

        public void DailyPointData() {

            int GoalPoint = commonDatabaseHelper.getPointLastRecord();

            if (GoalPoint != 0) {
                int newUserPoint = 0;
                if (currantPoint > GoalPoint) {
                    newUserPoint = currantPoint - GoalPoint;
                    if (newUserPoint != 0) {
                        userpoint = newUserPoint;
                    }
                }
            } else {
                if (currantPoint != 0) {
                    userpoint = currantPoint;
                }
            }

            DailyMinuteData();
        }

        public void DailyMinuteData() {
            int GoalMinute = commonDatabaseHelper.getMinuteLastRecord();
            if (GoalMinute != 0) {
                int newUserMinute = 0;
                if (currantMinute > GoalMinute) {
                    newUserMinute = currantMinute - GoalMinute;
                    if (newUserMinute != 0) {
                        userminute = newUserMinute;
                    }
                }
            } else {
                if (currantMinute != 0) {
                    userminute = currantMinute;
                }
            }

            sentPoint_MinuteData();
        }

        public void sentPoint_MinuteData() {

          /*  Global.printLog("AUDIODATA", "=AudioMinute1111=" + currantAudioMinute +
                    "=AudioPoint1111=" + currantAudioPoint +
                    "=Minute1111=" + currantMinute +
                    "=Point1111=" + currantPoint +
                    "=userminute=" + userminute +
                    "=userpoint=" + userpoint);*/

            if (userminute != 0 || userpoint != 0) {

                insertDailyPointData(currantDate, currantDayHour, userminute, userpoint);
                //Global.printLog("AUDIODATA", "=userminute=" + userminute + "=userpoint=" + userpoint);
                String Subscribed = sessionManager.getKeySubscribed();
                if (Subscribed.equals("1")) {
                    PushPlAPI(userminute, userpoint);
                }

                sessionManager.setUserMinAudio(0);
                sessionManager.setKeyUserMinAudioPoint(0);
                userpoint = 0;
                userminute = 0;
                currantPoint = 0;
                currantMinute = 0;
                currantAudioPoint = 0;
                currantAudioMinute = 0;
            }
        }

        public void insertDailyPointData(String currantDate, int currantDayHour, int minutes, int points) {

            ContentValues contentValues = new ContentValues();
            contentValues.put(CommonDatabaseHelper.KEY_DAILY_LOCAL_USE_DATE, currantDate);
            contentValues.put(CommonDatabaseHelper.KEY_DAILY_LOCAL_USE_HOUR, currantDayHour);
            contentValues.put(CommonDatabaseHelper.KEY_DAILY_MINUTES, minutes);
            contentValues.put(CommonDatabaseHelper.KEY_DAILY_POINTS, points);
            contentValues.put(CommonDatabaseHelper.KEY_DAILY_USER_ID, paraUserId);

            commonDatabaseHelper.insertData(CommonDatabaseHelper.USER_DAILY, contentValues);
        }

        private void PushPlAPI(int paraMinutes, int paraPoints) {

            APIInterface apiService = APIClient.getClient().create(APIInterface.class);
            String url = APIClient.BASE_URL;

            url += "push.pl?user_id=" + paraUserId +
                    "&session_id=" + paraSessionId +
                    "&apiauth=" + APIClient.AUTH_KEY +
                    "&action=" + "add" +
                    "&type=" + "usage" +
                    "&device=" + APIClient.DEVICE_KEY +
                    "&app_version=" + BuildConfig.VERSION_NAME +
                    "&local_use_date=" + currantDate +
                    "&local_use_hour=" + currantDayHour +
                    "&minutes=" + paraMinutes +
                    "&points=" + paraPoints;

            Call<UsageDetail> callObject = apiService.usageInfo(url.replaceAll(" ", "%20"));

            callObject.enqueue(new Callback<UsageDetail>() {
                @Override
                public void onResponse(Call<UsageDetail> call, Response<UsageDetail> response) {

                    if (response.body() != null && !response.body().toString().trim().isEmpty()) {
                        if (response.body().getSuccess() == 1) {

                        }
                    }
                }

                @Override
                public void onFailure(Call<UsageDetail> call, Throwable t) {
                    call.cancel();
                }
            });
        }
    }
}
