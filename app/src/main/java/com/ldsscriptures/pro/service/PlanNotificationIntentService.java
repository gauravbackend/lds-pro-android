package com.ldsscriptures.pro.service;

import android.app.IntentService;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.WakefulBroadcastReceiver;

import com.ldsscriptures.pro.R;
import com.ldsscriptures.pro.activity.studytools.PlanActivity;
import com.ldsscriptures.pro.model.PlanReminderData;
import com.ldsscriptures.pro.model.PlanReminderMainData;
import com.ldsscriptures.pro.model.ReminderCheckItem;
import com.ldsscriptures.pro.model.SettingPlanReminderMainData;
import com.ldsscriptures.pro.utils.SessionManager;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.concurrent.TimeUnit;

public class PlanNotificationIntentService extends IntentService {

    private static final int NOTIFICATION_ID = 1;
    private static final String ACTION_START = "ACTION_START";
    private static final String ACTION_DELETE = "ACTION_DELETE";

    SessionManager sessionManager;
    String currant_date;

    public PlanNotificationIntentService() {
        super(PlanNotificationIntentService.class.getSimpleName());
    }

    public static Intent createIntentStartNotificationService(Context context) {
        Intent intent = new Intent(context, PlanNotificationIntentService.class);
        intent.setAction(ACTION_START);
        return intent;
    }

    public static Intent createIntentDeleteNotification(Context context) {
        Intent intent = new Intent(context, PlanNotificationIntentService.class);
        intent.setAction(ACTION_DELETE);
        return intent;
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        sessionManager = new SessionManager(getApplicationContext());
        currant_date = new SimpleDateFormat("dd/MM/yyyy").format(Calendar.getInstance().getTime());

        try {
            String action = intent.getAction();
            if (ACTION_START.equals(action)) {
                sendReminderNotification();
                settingSendReminderNotification();
            }
        } finally {
            WakefulBroadcastReceiver.completeWakefulIntent(intent);
        }
    }

    public void settingSendReminderNotification() {

        Calendar calendar = Calendar.getInstance();
        int currantDayWeek = calendar.get(Calendar.DAY_OF_WEEK);
        int currantDayHour = calendar.get(Calendar.HOUR);
        int currantdayminute = calendar.get(Calendar.MINUTE);

        ArrayList<SettingPlanReminderMainData> planReminderMainDataArrayList = new ArrayList<>();
        ArrayList<PlanReminderData> planReminderDatas = new ArrayList<>();
        planReminderMainDataArrayList = sessionManager.getSettingPlanReminderData();
        if (planReminderMainDataArrayList != null) {
            for (int i = 0; i < planReminderMainDataArrayList.size(); i++) {

                String planOfTime = planReminderMainDataArrayList.get(i).getPlanTime();
                String reminderDate = planReminderMainDataArrayList.get(i).getPlanDate();
                int dayOfHour = planReminderMainDataArrayList.get(i).getPlanSelectHour();
                int dayOfMinute = planReminderMainDataArrayList.get(i).getPlanSelectMinute();
                planReminderDatas = planReminderMainDataArrayList.get(i).getPlanReminderDataArrayList();

                for (int j = 0; j < planReminderDatas.size(); j++) {

                    int dayOfWeek = Integer.parseInt(planReminderDatas.get(j).getPlanReminderId());

                    if (currantDayHour == dayOfHour && currantdayminute == dayOfMinute && currantDayWeek == dayOfWeek) {

                        if (reminderDate.equals(currant_date)) {
                            processStartNotification();
                            planReminderMainDataArrayList.set(i, new SettingPlanReminderMainData(planOfTime, dayOfHour, dayOfMinute, currant_date, planReminderDatas));
                        } else {
                            int dateDifference = (int) getDateDiff(new SimpleDateFormat("dd/MM/yyyy"), reminderDate, currant_date);
                            if (dateDifference == 1) {
                                processStartNotification();
                                planReminderMainDataArrayList.set(i, new SettingPlanReminderMainData(planOfTime, dayOfHour, dayOfMinute, currant_date, planReminderDatas));
                            }
                        }
                    }
                }
            }
            sessionManager.setSettingPlanReminderData(planReminderMainDataArrayList);
        }
    }

    public void sendReminderNotification() {

        ArrayList<ReminderCheckItem> ReminderCheckItemArrayList = new ArrayList<>();
        ReminderCheckItemArrayList = sessionManager.getCheckedNotification();

        if (ReminderCheckItemArrayList != null) {

            for (int k = 0; k < ReminderCheckItemArrayList.size(); k++) {
                boolean sendNotiFlag = ReminderCheckItemArrayList.get(k).isPlanFlag();
                if (sendNotiFlag == true) {

                    ArrayList<PlanReminderMainData> planReminderMainDataArrayList = new ArrayList<>();
                    ArrayList<PlanReminderData> planReminderDatas = new ArrayList<>();
                    planReminderMainDataArrayList = sessionManager.getNotificationPlanReminderData();

                    if (planReminderMainDataArrayList != null) {
                        for (int i = 0; i < planReminderMainDataArrayList.size(); i++) {

                            int dayOfHour = planReminderMainDataArrayList.get(i).getPlanSelectHour();
                            int dayOfMinute = planReminderMainDataArrayList.get(i).getPlanSelectMinute();
                            planReminderDatas = planReminderMainDataArrayList.get(i).getPlanReminderDataArrayList();

                            for (int j = 0; j < planReminderDatas.size(); j++) {
                                int dayOfWeek = Integer.parseInt(planReminderDatas.get(j).getPlanReminderId());
                                if (dayOfWeek == 1) {
                                    sendNotiWeekDay(dayOfHour, dayOfMinute, dayOfWeek);
                                } else if (dayOfWeek == 2) {
                                    sendNotiWeekDay(dayOfHour, dayOfMinute, dayOfWeek);
                                } else if (dayOfWeek == 3) {
                                    sendNotiWeekDay(dayOfHour, dayOfMinute, dayOfWeek);
                                } else if (dayOfWeek == 4) {
                                    sendNotiWeekDay(dayOfHour, dayOfMinute, dayOfWeek);
                                } else if (dayOfWeek == 5) {
                                    sendNotiWeekDay(dayOfHour, dayOfMinute, dayOfWeek);
                                } else if (dayOfWeek == 6) {
                                    sendNotiWeekDay(dayOfHour, dayOfMinute, dayOfWeek);
                                } else if (dayOfWeek == 7) {
                                    sendNotiWeekDay(dayOfHour, dayOfMinute, dayOfWeek);
                                }
                            }
                        }
                    }
                }
            }
        }


    }

    public void sendNotiWeekDay(int hour, int minute, int week) {

        Calendar calendar = Calendar.getInstance();
        int currantDayWeek = calendar.get(Calendar.DAY_OF_WEEK);
        int currantDayHour = calendar.get(Calendar.HOUR);
        int currantdayminute = calendar.get(Calendar.MINUTE);

        if (currantDayHour == hour && currantdayminute == minute && currantDayWeek == week) {

            calendar.set(Calendar.HOUR_OF_DAY, hour);
            calendar.set(Calendar.MINUTE, minute);
            calendar.set(Calendar.DAY_OF_WEEK, week);

            ArrayList<ReminderCheckItem> ReminderCheckItemArrayList = new ArrayList<>();
            ReminderCheckItemArrayList = sessionManager.getCheckedNotification();

            if (ReminderCheckItemArrayList != null) {
                for (int i = 0; i < ReminderCheckItemArrayList.size(); i++) {
                    String var_Plan_id = ReminderCheckItemArrayList.get(i).getPlanid();
                    Boolean isChecked = ReminderCheckItemArrayList.get(i).isPlanFlag();
                    String getCurrantDate = ReminderCheckItemArrayList.get(i).getPlanDate();

                    if (getCurrantDate.equals("")) {
                        processStartNotification();
                        ReminderCheckItemArrayList.set(i, new ReminderCheckItem(var_Plan_id, isChecked, currant_date));
                    } else {

                        String lastDate = getCurrantDate;
                        int dateDifference = (int) getDateDiff(new SimpleDateFormat("dd/MM/yyyy"), lastDate, currant_date);
                        if (dateDifference == 1) {
                            processStartNotification();
                            ReminderCheckItemArrayList.set(i, new ReminderCheckItem(var_Plan_id, isChecked, currant_date));
                        }
                    }
                }
                sessionManager.setCheckedNotification(ReminderCheckItemArrayList);
            }

        }
    }

    public static long getDateDiff(SimpleDateFormat format, String oldDate, String newDate) {
        try {
            return TimeUnit.DAYS.convert(format.parse(newDate).getTime() - format.parse(oldDate).getTime(), TimeUnit.MILLISECONDS);
        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        }
    }

    private void processStartNotification() {
        // Do something. For example, fetch fresh data from backend to create a rich notification?

        Uri alarmSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        final NotificationCompat.Builder builder = new NotificationCompat.Builder(this);
        builder.setContentTitle(getResources().getString(R.string.app_name))
                .setAutoCancel(true)
                .setColor(getResources().getColor(R.color.sand))
                .setSound(alarmSound)
                .setContentText("Reading Plan Scheduled")
                .setSmallIcon(R.mipmap.ic_app_icon)
                .setVibrate(new long[]{1000, 1000, 1000, 1000, 1000});

        Intent mainIntent = new Intent(this, PlanActivity.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(this,
                NOTIFICATION_ID,
                mainIntent,
                PendingIntent.FLAG_UPDATE_CURRENT);
        builder.setContentIntent(pendingIntent);
        builder.setDeleteIntent(PlanNotificationEventReceiver.getDeleteIntent(this));

        final NotificationManager manager = (NotificationManager) this.getSystemService(Context.NOTIFICATION_SERVICE);
        manager.notify(NOTIFICATION_ID, builder.build());
    }
}
