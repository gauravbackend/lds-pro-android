package com.ldsscriptures.pro.service;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Environment;
import android.support.v4.content.WakefulBroadcastReceiver;

import com.ldsscriptures.pro.model.Item;
import com.ldsscriptures.pro.retroutils.APIClient;
import com.ldsscriptures.pro.retroutils.APIInterface;
import com.ldsscriptures.pro.retroutils.RetrieveDBListResponse;
import com.ldsscriptures.pro.utils.Global;
import com.ldsscriptures.pro.utils.SessionManager;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CitationIntentService extends IntentService {

    private static final int NOTIFICATION_ID = 1;
    private static final String ACTION_START = "ACTION_START";
    private static final String ACTION_DELETE = "ACTION_DELETE";

    SessionManager sessionManager;
    String currant_date;

    public CitationIntentService() {
        super(CitationIntentService.class.getSimpleName());
    }

    public static Intent createIntentStartNotificationService(Context context) {
        Intent intent = new Intent(context, CitationIntentService.class);
        intent.setAction(ACTION_START);
        return intent;
    }

    public static Intent createIntentDeleteNotification(Context context) {
        Intent intent = new Intent(context, CitationIntentService.class);
        intent.setAction(ACTION_DELETE);
        return intent;
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        sessionManager = new SessionManager(getApplicationContext());
        currant_date = new SimpleDateFormat("dd/MM/yyyy").format(Calendar.getInstance().getTime());
        try {
            String action = intent.getAction();
            if (ACTION_START.equals(action)) {

                retriveDatabse();
            }
        } finally {
            WakefulBroadcastReceiver.completeWakefulIntent(intent);
        }
    }


    private void retriveDatabse() {

        APIInterface apiService = APIClient.getClient().create(APIInterface.class);

        String url = APIClient.BASE_URL;

        url += "prodb3.pl?catalog=" + Global.DB_VERSION +
                "&lang=eng" +
                "&apiauth=" + APIClient.AUTH_KEY;

        Call<RetrieveDBListResponse<Item>> callObject = apiService.retrieveDB(url);

        callObject.enqueue(new Callback<RetrieveDBListResponse<Item>>() {
            @Override
            public void onResponse(Call<RetrieveDBListResponse<Item>> call, Response<RetrieveDBListResponse<Item>> response) {

                if (response.body() != null && !response.body().toString().trim().isEmpty()) {

                    String Citations_url = response.body().getCitations_url();
                    String destPath1 = Environment.getExternalStorageDirectory() + "/LDS/" + "citations.sqlite";

                    DownloadTask downloadTask = new DownloadTask();
                    downloadTask.execute(Citations_url, destPath1);
                }
            }

            @Override
            public void onFailure(Call<RetrieveDBListResponse<Item>> call, Throwable t) {
                call.cancel();

            }
        });
    }

    public class DownloadTask extends AsyncTask<String, Integer, String> {

        @Override
        protected String doInBackground(String... sUrl) {
            InputStream input = null;
            OutputStream output = null;
            HttpURLConnection connection = null;
            try {
                String Citations_url = sUrl[0];
                String destPath = sUrl[1];

                URL url = new URL(Citations_url);

                File destDir1 = new File(destPath);
                if (!destDir1.exists()) {

                    connection = (HttpURLConnection) url.openConnection();
                    connection.connect();
                    if (connection.getResponseCode() != HttpURLConnection.HTTP_OK) {
                        return "Server returned HTTP " + connection.getResponseCode() + " " + connection.getResponseMessage();
                    }
                    int fileLength = connection.getContentLength();

                    String newFile = Environment.getExternalStorageDirectory() + "/LDS/";
                    String filename = "citations.sqlite";
                    String filePath = newFile + filename;

                    // download the file
                    input = connection.getInputStream();
                    output = new FileOutputStream(filePath);

                    byte data[] = new byte[4096];
                    long total = 0;
                    int count;
                    while ((count = input.read(data)) != -1) {
                        // allow canceling with back button
                        if (isCancelled()) {
                            input.close();
                            return null;
                        }
                        total += count;
                        // publishing the progress....
                        if (fileLength > 0) // only if total length is known
                            publishProgress((int) (total * 100 / fileLength));
                        output.write(data, 0, count);
                    }
                }

            } catch (Exception e) {
                return e.toString();
            } finally {
                try {
                    if (output != null)
                        output.close();
                    if (input != null)
                        input.close();
                } catch (IOException ignored) {
                }

                if (connection != null)
                    connection.disconnect();
            }
            return null;
        }
    }


}
