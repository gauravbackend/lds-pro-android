package com.ldsscriptures.pro.service;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;
import android.support.annotation.Nullable;

import com.google.gson.JsonObject;
import com.ldsscriptures.pro.BuildConfig;
import com.ldsscriptures.pro.retroutils.APIClient;
import com.ldsscriptures.pro.retroutils.APIInterface;
import com.ldsscriptures.pro.utils.Global;
import com.ldsscriptures.pro.utils.RetriveMainDatabse;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class LDSMainDBService extends Service {

    private Context context;

    public LDSMainDBService() {
    }

    @Override
    public void onStart(Intent intent, int startId) {
        super.onStart(intent, startId);
        context = this;

    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public void onCreate() {
        super.onCreate();
        getDatabaseVersion();
    }

    private void getDatabaseVersion() {

        APIInterface apiService = APIClient.getClient().create(APIInterface.class);
        String url = APIClient.BASE_URL;

        url += "latestDB.pl?device=" + APIClient.DEVICE_KEY +
                "&appversion=" + BuildConfig.VERSION_NAME +
                "&apiauth=" + APIClient.AUTH_KEY;

        Call<JsonObject> callObject = apiService.getDBVersion(url);

        callObject.enqueue(new Callback<JsonObject>() {
            @Override
            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {

                if (response.body() != null && !response.body().toString().trim().isEmpty()) {

                    Global.DB_VERSION = response.body().get("dbversion").toString();

                    new RetriveMainDatabse(context);

                }
            }

            @Override
            public void onFailure(Call<JsonObject> call, Throwable t) {
                call.cancel();

            }
        });
    }


}