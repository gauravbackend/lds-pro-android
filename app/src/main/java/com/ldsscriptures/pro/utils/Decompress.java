package com.ldsscriptures.pro.utils;

import android.util.Log;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

/**
 * Created by iblinfotech on 05/02/18.
 */

public class Decompress {
    private File _zipFile;
    private InputStream _zipFileStream;
    private String ROOT_LOCATION;
    private static final String TAG = "UNZIPUTIL";

    public Decompress(File zipFile) {
        _zipFile = zipFile;

    }

    public void unzip(String id) {

        String zipFile = Global.AppDownloadFolderPath + "/" + id + "/file.zip";
        String unzipLocation = Global.AppFolderPath + id;
        UnzipUtility unzipper = new UnzipUtility();
        try {
            unzipper.unzip(id, zipFile, unzipLocation);
        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }
}
