package com.ldsscriptures.pro.utils;


import android.content.Context;
import android.content.Intent;
import android.os.Environment;

import com.ldsscriptures.pro.model.Download;
import com.ldsscriptures.pro.retroutils.APIClient;
import com.ldsscriptures.pro.retroutils.APIInterface;
import com.ldsscriptures.pro.service.DownloadSubDBService;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class DownloadMainDBZipFile {

    Context context;
    String Citation_Url, Citation_path;

    public DownloadMainDBZipFile(String Catalog_url, String destPath, Context context, String Citation_Url, String Citation_path) {

        this.context = context;
        this.Citation_Url = Citation_Url;
        this.Citation_path = Citation_path;


        File root = new File(Environment.getExternalStorageDirectory(), "LDS");
        if (!root.exists()) {
            root.mkdirs();

        }
        downloadDBZipFile(Catalog_url);

    }

    private void downloadDBZipFile(String catalog_url) {

        APIInterface apiService = APIClient.getClient().create(APIInterface.class);

        Call<ResponseBody> call = apiService.downloadFileWithDynamicUrl(catalog_url);

        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.body() != null && !response.body().toString().trim().isEmpty()) {


                    boolean writtenToDisk = false;
                    try {
                        writtenToDisk = writeResponseBodyToDisk(response.body());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                } else {

                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });
    }

    private File getOutputMediaFile(String filename) {

        if (Global.AppDownloadFolder != null) {
            if (!Global.AppDownloadFolder.exists()) {
                if (!Global.AppDownloadFolder.mkdirs()) {
                    return null;
                }
            }
        }
        File mediaFile;
        String mImageName = filename;
        mediaFile = new File(Global.AppDownloadFolder, mImageName);
        return mediaFile;
    }

    private boolean writeResponseBodyToDisk(ResponseBody body) throws IOException {


        int count;
        byte data[] = new byte[1024 * 4];
        long fileSize = body.contentLength();
        InputStream bis = new BufferedInputStream(body.byteStream(), 1024 * 8);
        File outputFile = getOutputMediaFile("file.zip");

        if (outputFile == null)
            return false;

        OutputStream output = new FileOutputStream(outputFile);
        long total = 0;
        long startTime = System.currentTimeMillis();
        int timeCount = 1;
        while ((count = bis.read(data)) != -1) {

            total += count;
            int totalFileSize = (int) (fileSize / (Math.pow(1024, 2)));
            double current = Math.round(total / (Math.pow(1024, 2)));

            int progress = (int) ((total * 100) / fileSize);

            long currentTime = System.currentTimeMillis() - startTime;

            Download download = new Download();
            download.setTotalFileSize(totalFileSize);

            if (currentTime > 1000 * timeCount) {

                download.setCurrentFileSize((int) current);
                download.setProgress(progress);

                timeCount++;
            }

            output.write(data, 0, count);
        }

        onDownloadComplete();
        output.flush();
        output.close();
        bis.close();

        return true;
    }

    private void onDownloadComplete() {
        unzip();
    }

    public void unzip() {

        String zipFile = Global.AppDownloadFolderPath + "/file.zip";

        UnzipUtility unzipper = new UnzipUtility();
        try {
            unzipper.unzip("", zipFile, Global.AppFolderPath);
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        Intent ir = new Intent(context, DownloadSubDBService.class);
        ir.putExtra("Citation_Url_intent", Citation_Url.toString());
        ir.putExtra("Citation_path_intent", Citation_path.toString());
        context.startService(ir);

    }


}
