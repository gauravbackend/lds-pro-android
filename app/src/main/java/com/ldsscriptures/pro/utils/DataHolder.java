package com.ldsscriptures.pro.utils;

import com.ldsscriptures.pro.model.LcData;
import com.ldsscriptures.pro.model.SerachData;
import com.ldsscriptures.pro.model.VerseData;

import java.util.ArrayList;

/**
 * Created by IBL InfoTech on 10/26/2017.
 */

public enum DataHolder {

    INSTANCE, INSTANCE1, INSTANCE2, INSTANCE3;

    private ArrayList<LcData> lcDataArrayList;
    private ArrayList<SerachData> categoryArrayList;
    private ArrayList<SerachData> lcDataArrayListString;
    ArrayList<VerseData> stringArrayList;

    public static boolean hasData() {
        return INSTANCE.categoryArrayList != null;
    }

    public static void setData(final ArrayList<SerachData> categoryArrayList) {
        INSTANCE.categoryArrayList = categoryArrayList;
    }

    public static ArrayList<SerachData> getData() {
        final ArrayList<SerachData> retList = INSTANCE.categoryArrayList;
        INSTANCE.categoryArrayList = null;
        return retList;
    }

    public static boolean hasData1() {
        return INSTANCE1.lcDataArrayListString != null;
    }

    public static void setData1(final ArrayList<SerachData> lcDataArrayListString) {
        INSTANCE1.lcDataArrayListString = lcDataArrayListString;
    }

    public static ArrayList<SerachData> getData1() {
        final ArrayList<SerachData> retList = INSTANCE1.lcDataArrayListString;
        INSTANCE1.lcDataArrayListString = null;
        return retList;
    }

    public static boolean hasData2() {
        return INSTANCE3.lcDataArrayList != null;
    }

    public static void setData2(final ArrayList<LcData> lcDataArrayList) {
        INSTANCE3.lcDataArrayList = lcDataArrayList;
    }

    public static ArrayList<LcData> getData2() {
        final ArrayList<LcData> retList = INSTANCE3.lcDataArrayList;
        INSTANCE3.lcDataArrayList = null;
        return retList;
    }


}

