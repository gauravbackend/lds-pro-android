package com.ldsscriptures.pro.utils;

import android.content.Context;
import android.graphics.Typeface;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.ldsscriptures.pro.R;

public class Font {

    public static void setAllTextView(Context context, ViewGroup parent, String fontName) {
        for (int i = parent.getChildCount() - 1; i >= 0; i--) {
            final View child = parent.getChildAt(i);
            if (child instanceof ViewGroup) {
                setAllTextView(context, (ViewGroup) child, fontName);
            } else if (child instanceof TextView) {
                ((TextView) child).setTypeface(getFont(context, fontName));
            }
        }
    }

    public static Typeface getFont(Context context, String fontName) {
        return Typeface.createFromAsset(context.getAssets(), fontName);
    }

    public static void setBoldFont(Context context, TextView textView, String sfont) {
        Typeface font = Typeface.createFromAsset(context.getAssets(), sfont);
//        Typeface font = Typeface.createFromAsset(context.getAssets(), context.getResources().getString(R.string.font_roboto_regular));
        textView.setTypeface(font);
    }

    public static void setFont(Context context, TextView textView) {
        Typeface font = Typeface.createFromAsset(context.getAssets(), context.getResources().getString(R.string.font_helvetica_neue));
        textView.setTypeface(font);
    }
}
