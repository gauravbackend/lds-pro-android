package com.ldsscriptures.pro.utils;

import android.content.Context;
import android.content.Intent;
import android.os.Environment;

import com.ldsscriptures.pro.fragment.LibraryCollectionFragment;
import com.ldsscriptures.pro.model.Item;
import com.ldsscriptures.pro.retroutils.APIClient;
import com.ldsscriptures.pro.retroutils.APIInterface;
import com.ldsscriptures.pro.retroutils.RetrieveDBListResponse;
import com.ldsscriptures.pro.service.LDSMainDBService;

import java.io.File;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Set;
import java.util.TreeSet;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RetriveMainDatabse {
    private static final String TAG = "RETVIVE MAIN DB METHOD";
    private final String AppFolderPath;
    private ArrayList<Item> dbCatelogArraylist = new ArrayList<>();
    private Context context;
    SessionManager sessionManager;

    public RetriveMainDatabse(Context context) {

        File root = new File(Environment.getExternalStorageDirectory(), "LDS");
        if (!root.exists()) {
            root.mkdirs();
        }

        AppFolderPath = Environment.getExternalStorageDirectory() + "/LDS/";
        this.context = context;
        retriveDatabse(context);
        sessionManager = new SessionManager(context);
    }

    public RetriveMainDatabse(Context context, String temp/*used for only differentiate parameterized constructot*/) {
        this.context = context;

        AppFolderPath = Environment.getExternalStorageDirectory() + "/LDS/";
        File fileOrDirectory_ = new File(AppFolderPath);
        DeleteRecursive(fileOrDirectory_, false);

        context.startService(new Intent(context, LDSMainDBService.class));
    }

    private void retriveDatabse(final Context context) {

        dbCatelogArraylist.clear();

        APIInterface apiService = APIClient.getClient().create(APIInterface.class);

        String url = APIClient.BASE_URL;

        url += "prodb3.pl?catalog=" + Global.DB_VERSION +
                "&lang=eng" +
                "&apiauth=" + APIClient.AUTH_KEY;

        Call<RetrieveDBListResponse<Item>> callObject = apiService.retrieveDB(url);

        callObject.enqueue(new Callback<RetrieveDBListResponse<Item>>() {
            @Override
            public void onResponse(Call<RetrieveDBListResponse<Item>> call, Response<RetrieveDBListResponse<Item>> response) {

                if (response.body() != null && !response.body().toString().trim().isEmpty()) {

                    String Citations_url = response.body().getCitations_url();
                    String destPath1 = AppFolderPath + "citations.sqlite";

                    dbCatelogArraylist = response.body().getItem();
                    dbCatelogArraylist = sortValues(dbCatelogArraylist);
                    Global.saveListToSharedPreferneces(context, dbCatelogArraylist);

                    String destPath = AppFolderPath + "Catalog.sqlite";

                    File destDir = new File(destPath);
                    if (!destDir.exists()) {
                        new DownloadMainDBZipFile(response.body().getCatalog_url(), destPath, context, Citations_url, destPath1);

                    } else {
                        Global.dismissProgressDialog();

                        if (LibraryCollectionFragment.getInstance() != null)
                            LibraryCollectionFragment.getInstance().checkDatabse();
                    }
                }
            }

            @Override
            public void onFailure(Call<RetrieveDBListResponse<Item>> call, Throwable t) {
                call.cancel();

            }
        });
    }

    public ArrayList<Item> sortValues(ArrayList<Item> list) {
        Set set = new TreeSet(new Comparator() {

            @Override
            public int compare(Object o1, Object o2) {
                return ((Item) o1).getId().compareTo(((Item) o2).getId());
            }
        });
        set.addAll(list);

        final ArrayList newList = new ArrayList(set);
        return newList;
    }

    public void DeleteRecursive(File fileOrDirectory, boolean wantToDelete) {

        if (fileOrDirectory.isDirectory()) {
            for (File child : fileOrDirectory.listFiles()) {

                if (!wantToDelete && !child.getName().contains("Catalog.sqlite")) {
                    child.delete();

                } else if (wantToDelete) {
                    child.delete();

                }
                DeleteRecursive(child, true);
            }
            if (wantToDelete) {

                fileOrDirectory.delete();
            }
        }
    }

}
