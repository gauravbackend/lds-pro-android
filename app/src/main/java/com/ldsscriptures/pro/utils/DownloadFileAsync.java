package com.ldsscriptures.pro.utils;

import android.os.AsyncTask;
import android.util.Log;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileDescriptor;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;

/**
 * Created by iblinfotech on 05/02/18.
 */

public class DownloadFileAsync extends AsyncTask<String, String, String> {

    private static final String TAG = "DOWNLOADFILE";
    private PostDownload callback;

    private FileDescriptor fd;
    private File file;
    private String downloadLocation;
    private String id;

    public DownloadFileAsync(String id, PostDownload callback) {
        this.callback = callback;
        this.id = id;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected String doInBackground(String... aurl) {
        int count;

        try {
            URL url = new URL(aurl[0]);
            URLConnection connection = url.openConnection();
            connection.connect();

            int lenghtOfFile = connection.getContentLength();

            InputStream input = new BufferedInputStream(url.openStream());
            file = getOutputMediaFile("file.zip", id);
            FileOutputStream output = new FileOutputStream(file); //context.openFileOutput("content.zip", Context.MODE_PRIVATE);
            fd = output.getFD();

            byte data[] = new byte[1024];
            long total = 0;
            while ((count = input.read(data)) != -1) {
                total += count;
                publishProgress("" + (int) ((total * 100) / lenghtOfFile));
                output.write(data, 0, count);
            }

            output.flush();
            output.close();
            input.close();
        } catch (Exception e) {
        }
        return null;

    }

    protected void onProgressUpdate(String... progress) {
        //Log.d(TAG,progress[0]);
    }

    @Override
    protected void onPostExecute(String unused) {
        if (callback != null) callback.downloadDone(file);
    }

    public static interface PostDownload {
        void downloadDone(File fd);
    }

    private File getOutputMediaFile(String filename, String id) {

        File mediaStorageDir = new File(Global.AppDownloadFolder + "/" + id);
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                return null;
            }
        }
        File mediaFile;
        String mImageName = filename;
        mediaFile = new File(mediaStorageDir, mImageName);
        return mediaFile;
    }

}
