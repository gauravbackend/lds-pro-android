package com.ldsscriptures.pro.utils;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.Rect;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Environment;
import android.renderscript.Allocation;
import android.renderscript.Element;
import android.renderscript.RenderScript;
import android.renderscript.ScriptIntrinsicBlur;
import android.support.design.widget.Snackbar;
import android.text.Html;
import android.text.SpannableString;
import android.text.format.DateFormat;
import android.util.Log;
import android.util.TypedValue;
import android.view.Display;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.amazonaws.com.google.gson.reflect.TypeToken;
import com.google.gson.Gson;
import com.ldsscriptures.pro.BuildConfig;
import com.ldsscriptures.pro.R;
import com.ldsscriptures.pro.dbClass.GetDataFromDatabase;
import com.ldsscriptures.pro.dbClass.GetLibCollectionFromDB;
import com.ldsscriptures.pro.model.BookmarkData;
import com.ldsscriptures.pro.model.GetVerseDataFromUri;
import com.ldsscriptures.pro.model.IndexWrapper;
import com.ldsscriptures.pro.model.Item;
import com.ldsscriptures.pro.model.LcData;
import com.ldsscriptures.pro.model.LsData;
import com.ldsscriptures.pro.model.LsFullIndexData;
import com.ldsscriptures.pro.model.LsIndexPreviewData;
import com.ldsscriptures.pro.model.LsMainIndexData;
import com.ldsscriptures.pro.model.LsSubIndexData;
import com.ldsscriptures.pro.model.MoreOptionMenuData;
import com.ldsscriptures.pro.model.VerseData;

import java.io.File;
import java.io.FileOutputStream;
import java.lang.reflect.Type;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Global {
    public static String CURRENT_AUDIO_PATH = "";
    public static String CURRENT_PDF_PATH = "";
    static SharedPreferences preferences;
    public static String GCMInstanceID = "123";
    static SharedPreferences.Editor editor;
    public static String DB_VERSION = "";
    private static final float BLUR_RADIUS = 25f;
    private static ProgressDialog progressDialog;
    private static File mFileTemp;
    public static String AppFolderPath;
    private final Context context;
    public static File AppDownloadFolder = null;
    public static String visible_flag = "0";
    public static String AppDownloadFolderPath = null;
    public static int DeviceWidth, DeviceHeight;
    public static final String ADD_NEW_TAB_PATH = "AddNewTabPath";
    public static final String ADD_NEW_TAB_CLASS_NAME = "AddNewTabClassName";
    public static boolean IS_FROM_READING_NOW = false;
    public static final BookmarkData bookmarkData = new BookmarkData();
    public static int index = 0;
    public static int top = 0;
    public static int recycleviewpos = 0;
    public static int recycleviewOffset;
    public static String selectedLibCollection;
    public static String selectedLibSelection;
    public static boolean downloadFolder;
    static LinearLayout layout1;
    public static int audioFlag = 0;
    public static String SHARE_LEVEL = "1";
    public static String LANGUAGE = "eng";
    public static final int DATABASE_VERSION = 3;

    // set arrylist for More option
    public static ArrayList<MoreOptionMenuData> moreOptionMenuDatas = new ArrayList<MoreOptionMenuData>() {
        {
            add(new MoreOptionMenuData());
            add(new MoreOptionMenuData());
            add(new MoreOptionMenuData());
            add(new MoreOptionMenuData());
        }
    };

    //    public static SimpleDateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
    public static SimpleDateFormat inputFormat = new SimpleDateFormat("MMM dd, yyyy", Locale.US);
    String currantDate;
    public static String globalVarYear;
    public static String paraUserId;
    public static String UserId;
    SessionManager sessionManager;
    public static String deleted_Date;

    public Global(Context context) {
        this.context = context;
        preferences = this.context.getSharedPreferences("LDS", 0);
        editor = preferences.edit();
        sessionManager = new SessionManager(context);

        try {
            currantDate = new SimpleDateFormat("yyyy-MM-dd").format(Calendar.getInstance().getTime());
            deleted_Date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(Calendar.getInstance().getTime());
            SimpleDateFormat gsdf = new SimpleDateFormat("yyyy-MM-dd");
            Date dt1 = null;
            dt1 = gsdf.parse(currantDate);
            globalVarYear = (String) DateFormat.format("yyyy", dt1);
            paraUserId = sessionManager.getUserInfo(SessionManager.KEY_USER_ID);

        } catch (ParseException e) {
            e.printStackTrace();
        }

        AppDownloadFolderPath = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS) + "/LDS";
        AppFolderPath = Environment.getExternalStorageDirectory() + "/LDS/";

        AppDownloadFolder = new File(AppDownloadFolderPath);
        WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        DeviceWidth = size.x;
        DeviceHeight = size.y;
        layout1 = new LinearLayout(context);
    }

    public static List<IndexWrapper> findIndexesForKeyword(String mainString, String searchString) {

        String regex = "\\b" + searchString + "\\b";
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(mainString);

        List<IndexWrapper> wrappers = new ArrayList<IndexWrapper>();

        while (matcher.find() == true) {
            int end = matcher.end();
            int start = matcher.start();
            IndexWrapper wrapper = new IndexWrapper(start, end, 0);
            wrappers.add(wrapper);
        }
        return wrappers;
    }

    public static void hideKeyboard(Context context) {
        try {
            // Check if no view has focus:
            View view = ((Activity) context).getCurrentFocus();
            if (view != null) {
                InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
            }
        } catch (Exception e) {
        }
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    public static Bitmap blur(Bitmap image, Activity activity) {
        if (null == image) return null;
        Bitmap outputBitmap = Bitmap.createBitmap(image);
        final RenderScript renderScript = RenderScript.create(activity);
        Allocation tmpIn = Allocation.createFromBitmap(renderScript, image);
        Allocation tmpOut = Allocation.createFromBitmap(renderScript, outputBitmap);

        //Intrinsic Gausian blur filter
        ScriptIntrinsicBlur theIntrinsic = ScriptIntrinsicBlur.create(renderScript, Element.U8_4(renderScript));
        theIntrinsic.setRadius(BLUR_RADIUS);
        theIntrinsic.setInput(tmpIn);
        theIntrinsic.forEach(tmpOut);
        tmpOut.copyTo(outputBitmap);
        return outputBitmap;
    }

    public static void showSnackBar(Activity activity, String stringMessage) {
        Snackbar snackbar = Snackbar.make(activity.findViewById(android.R.id.content), stringMessage, Snackbar.LENGTH_LONG);
        View snackBarView = snackbar.getView();
        snackBarView.setBackgroundColor(activity.getResources().getColor(R.color.black));
        TextView textView = (TextView) snackBarView.findViewById(android.support.design.R.id.snackbar_text);
        textView.setTextSize(14f);
        textView.setTextColor(Color.WHITE);
        snackbar.show();
    }

    /* convert string to date in milliseconds format */
    public static double stringToDate(String Date) {
        long timeInMilliseconds = 0;
        Date date;
        try {
            date = inputFormat.parse(Date);
            timeInMilliseconds = date.getTime();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return timeInMilliseconds;
    }

    /* convert string to date in milliseconds format */
    public static double longToDouble(long longVal) {
        long timeInMilliseconds = longVal;
        return longVal;
    }

    public static String longToCustomDate(String long_Date, SimpleDateFormat sdf_format) {
        if (!long_Date.equalsIgnoreCase("Event Date")) {
            long date = Long.parseLong(long_Date);
            final Date graph_date = new Date(date);
            final String formated_Graph_date = sdf_format.format(graph_date);
            return formated_Graph_date;
        } else return "";
    }

    public static Date longToDate(Long longDate) {
        Date dateFormat = new Date(longDate);
        return dateFormat;
    }

    public static Long dateToLong(Date date) {
        long longDate = 0L;
        try {
            longDate = date.getTime();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return longDate;
    }

    public static boolean isNetworkAvailable(Context context) {
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    public static void showToast(Context context, String string) {
        Toast.makeText(context, string, Toast.LENGTH_SHORT).show();
    }

    public static void showNetworkAlert(Context activity) {
        try {
            android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(activity);
            builder.setTitle("Warning");
            builder.setMessage("Please check your Internet Connection.")
                    .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            // Yes button clicked, do something
                            dialog.dismiss();
                        }
                    });

            builder.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void setPrefrenceString(Context context, String key, String value) {
        editor.putString(key, value);
        editor.commit();
    }

    public static void setPrefrenceInt(Context context, String key, int value) {
        editor.putInt(key, value);
        editor.commit();
    }

    public static void setPrefrenceBoolean(Context context, String key, boolean value) {
        editor.putBoolean(key, value);
        editor.commit();
    }

    public static String getPrefrenceString(Context context, String key, String defValue) {
        String value = preferences.getString(key, defValue);
        return value;
    }

    public static int getPrefrenceInt(Context context, String key, int defValue) {
        int value = preferences.getInt(key, defValue);
        return value;
    }

    public static boolean getPrefrenceBoolean(Context context, String key, boolean defValue) {
        boolean value = preferences.getBoolean(key, defValue);
        return value;
    }

    public static void printLog(String key, String value) {
        if (BuildConfig.DEBUG)
            Log.e("LDSlog " + key + "====", "=====" + value);
    }

    public static void printLog(Context context, String key, String value) {
        if (BuildConfig.DEBUG)
            Log.e(context.getClass().getName() + "---------" + key + "====", "=====" + value);
    }

    public static int calculateInSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight) {
        // Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {

            final int halfHeight = height / 2;
            final int halfWidth = width / 2;

            // Calculate the largest inSampleSize value that is a power of 2 and keeps both
            // height and width larger than the requested height and width.
            while ((halfHeight / inSampleSize) >= reqHeight
                    && (halfWidth / inSampleSize) >= reqWidth) {
                inSampleSize *= 2;
            }
        }

        return inSampleSize;
    }

    public static Bitmap decodeSampledBitmapFromResource(Resources res, int resId, int reqWidth, int reqHeight) {

        // First decode with inJustDecodeBounds=true to check dimensions
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeResource(res, resId, options);

        // Calculate inSampleSize
        options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);

        // Decode bitmap with inSampleSize set
        options.inJustDecodeBounds = false;
        return BitmapFactory.decodeResource(res, resId, options);
    }

    public static void SetBackground(Context context, ImageView ivSplash, int bg_splash) {
        ivSplash.setImageBitmap(decodeSampledBitmapFromResource(context.getResources(), bg_splash, 100, 100));
    }

    public static void showProgressDialog(Context context) {
        progressDialog = new ProgressDialog(context);

        progressDialog.setCancelable(false);
        progressDialog.setMessage("Please Wait");
        try {
            if (!((Activity) context).isFinishing()) {
                progressDialog.show();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void dismissProgressDialog() {
        if (progressDialog != null) {
            if (progressDialog.isShowing())
                progressDialog.dismiss();
        }
    }

    public static String takeScreenshot(Activity activity, int getTop) {

        Date now = new Date();
        android.text.format.DateFormat.format("yyyy-MM-dd_hh:mm:ss", now);

        String state = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED.equals(state)) {
            mFileTemp = new File(Environment.getExternalStorageDirectory(), System.currentTimeMillis() + ".jpg");
        } else {
            mFileTemp = new File(activity.getFilesDir(), System.currentTimeMillis() + ".jpg");
        }
        String imageFilePath = mFileTemp.getAbsolutePath();

        try {

            View view = activity.getWindow().getDecorView();
            view.setDrawingCacheEnabled(true);
            view.buildDrawingCache();
            Bitmap bitmap = view.getDrawingCache();
            Rect rect = new Rect();
            activity.getWindow().getDecorView().getWindowVisibleDisplayFrame(rect);

            int actionBarHeight = 0;
            TypedValue tv = new TypedValue();
            if (activity.getTheme().resolveAttribute(android.R.attr.actionBarSize, tv, true)) {
                actionBarHeight = TypedValue.complexToDimensionPixelSize(tv.data, activity.getResources().getDisplayMetrics());

            }

            int statusBarHeight = rect.top;
            int customHeight = statusBarHeight + getTop;

            int width = activity.getWindowManager().getDefaultDisplay().getWidth();
            int height = activity.getWindowManager().getDefaultDisplay().getHeight();
            Bitmap bitmap2 = Bitmap.createBitmap(bitmap, 0, customHeight, width, height - customHeight);
            view.destroyDrawingCache();

            File imageFile = new File(imageFilePath);
            FileOutputStream outputStream = new FileOutputStream(imageFile);
            int quality = 100;
            bitmap2.compress(Bitmap.CompressFormat.JPEG, quality, outputStream);
            outputStream.flush();
            outputStream.close();
//            openScreenshot(activity, imageFile);
        } catch (Throwable e) {
            // Several error may come out with file handling or OOM
            e.printStackTrace();
        }
        return imageFilePath;
    }

    public static void saveListToSharedPreferneces(Context context, ArrayList<Item> recentDealList) {
        SharedPreferences sharedpreferences =
                context.getSharedPreferences(context.getString(R.string.mainArrayList), Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedpreferences.edit();
        Gson gson = new Gson();

        String json = gson.toJson(recentDealList);
        editor.putString("ArrayList", json);
        editor.commit();
    }

    public static ArrayList<Item> getListSharedPreferneces(Context context) {

        SharedPreferences sharedPrefs = context.getSharedPreferences(context.getString(R.string.mainArrayList), Context.MODE_PRIVATE);
        Gson gson = new Gson();
        String json = sharedPrefs.getString("ArrayList", "");
        Type type = new TypeToken<ArrayList<Item>>() {
        }.getType();

        ArrayList<Item> recentDealList = gson.fromJson(json, type);

        return recentDealList;
    }

    public static Date currentDate() {
        Calendar calendar = Calendar.getInstance();
        return calendar.getTime();
    }

    public static String getTimeAgo(Date date, Context ctx) {

        if (date == null) {
            return null;
        }

        long time = date.getTime();

        Date curDate = currentDate();
        long now = curDate.getTime();
        if (time > now || time <= 0) {
            return null;
        }

        int dim = getTimeDistanceInMinutes(time);

        String timeAgo = null;

        if (dim == 0) {
            //timeAgo = ctx.getResources().getString(R.string.date_util_term_less);
            return ctx.getResources().getString(R.string.date_util_term_less);
        } else if (dim == 1) {
            return "1 " + ctx.getResources().getString(R.string.date_util_unit_minute);
        } else if (dim >= 2 && dim <= 44) {
            timeAgo = dim + " " + ctx.getResources().getString(R.string.date_util_unit_minutes);
        } else if (dim >= 45 && dim <= 89) {
            timeAgo = ctx.getResources().getString(R.string.date_util_prefix_about) + " " + ctx.getResources().getString(R.string.date_util_term_an) + " " + ctx.getResources().getString(R.string.date_util_unit_hour);
        } else if (dim >= 90 && dim <= 1439) {
            timeAgo = ctx.getResources().getString(R.string.date_util_prefix_about) + " " + (Math.round(dim / 60)) + " " + ctx.getResources().getString(R.string.date_util_unit_hours);
        } else if (dim >= 1440 && dim <= 2519) {
            timeAgo = "1 " + ctx.getResources().getString(R.string.date_util_unit_day);
        } else if (dim >= 2520 && dim <= 43199) {
            timeAgo = (Math.round(dim / 1440)) + " " + ctx.getResources().getString(R.string.date_util_unit_days);
        } else if (dim >= 43200 && dim <= 86399) {
            timeAgo = ctx.getResources().getString(R.string.date_util_prefix_about) + " " + ctx.getResources().getString(R.string.date_util_term_a) + " " + ctx.getResources().getString(R.string.date_util_unit_month);
        } else if (dim >= 86400 && dim <= 525599) {
            timeAgo = (Math.round(dim / 43200)) + " " + ctx.getResources().getString(R.string.date_util_unit_months);
        } else if (dim >= 525600 && dim <= 655199) {
            timeAgo = ctx.getResources().getString(R.string.date_util_prefix_about) + " " + ctx.getResources().getString(R.string.date_util_term_a) + " " + ctx.getResources().getString(R.string.date_util_unit_year);
        } else if (dim >= 655200 && dim <= 914399) {
            timeAgo = ctx.getResources().getString(R.string.date_util_prefix_over) + " " + ctx.getResources().getString(R.string.date_util_term_a) + " " + ctx.getResources().getString(R.string.date_util_unit_year);
        } else if (dim >= 914400 && dim <= 1051199) {
            timeAgo = ctx.getResources().getString(R.string.date_util_prefix_almost) + " 2 " + ctx.getResources().getString(R.string.date_util_unit_years);
        } else {
            timeAgo = ctx.getResources().getString(R.string.date_util_prefix_about) + " " + (Math.round(dim / 525600)) + " " + ctx.getResources().getString(R.string.date_util_unit_years);
        }

        return timeAgo + " " + ctx.getResources().getString(R.string.date_util_suffix);
    }

    private static int getTimeDistanceInMinutes(long time) {
        long timeDistance = currentDate().getTime() - time;
        return Math.round((Math.abs(timeDistance) / 1000) / 60);
    }

    public static CharSequence noTrailingwhiteLines(CharSequence text) {

        if (text != null && !text.toString().isEmpty() && !text.equals("")) {
            while (text.charAt(text.length() - 1) == '\n') {
                text = text.subSequence(0, text.length() - 1);
            }
        }

        return text;
    }

    // count date different using two different date
    public static long getDateDiff(SimpleDateFormat format, String oldDate, String newDate) {
        try {
            return TimeUnit.DAYS.convert(format.parse(newDate).getTime() - format.parse(oldDate).getTime(), TimeUnit.MILLISECONDS);
        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        }
    }

    // get current date
    public static String getCurrentDate() {
        //set default current date into date field.
        Calendar c = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("MMM dd, yyyy", Locale.US);
        return df.format(c.getTime());
    }

    public static GetVerseDataFromUri getVerseText(Context context, String todayURI) {

        ArrayList<LcData> lcDataArrayList = new GetLibCollectionFromDB(context).getLcArrayListFromDB();
        GetVerseDataFromUri getVerseDataFromUri = null;
        if (lcDataArrayList != null && lcDataArrayList.size() > 0) {
            for (int i = 0; i < lcDataArrayList.size(); i++) {
                if (todayURI.contains(lcDataArrayList.get(i).getTitle().toLowerCase())) {
                    String lcID = lcDataArrayList.get(i).getId();

                    //Global.printLog("VERSE", "=======lcID=======" + lcID.toString());

                    ArrayList<LsData> lsDataArrayList = lcDataArrayList.get(i).getLsDatas();
                    for (int j = 0; j < lsDataArrayList.size(); j++) {
                        if (todayURI.contains(lsDataArrayList.get(j).getUri())) {

                            String lsID = String.valueOf(lsDataArrayList.get(j).getItem_id());
                            String myString_PATH = lsDataArrayList.get(j).getTitle();

                            //Global.printLog("VERSE", "=======lsID=======" + lsID.toString());
                            ArrayList<LsFullIndexData> lsFullIndexDataArray = new GetDataFromDatabase(context, lsID).GetMainDataFromDB();

                            if (lsFullIndexDataArray != null && lsFullIndexDataArray.size() > 0) {
                                ArrayList<LsMainIndexData> itemMainIndexArray = lsFullIndexDataArray.get(0).getItemMainIndexArray();
                                //Global.printLog("VERSE", "=======itemMainIndexArray=======" + itemMainIndexArray.toString());
                                //Global.printLog("VERSE", "=======itemMainIndexArraySize=======" + itemMainIndexArray.size());
                                if (itemMainIndexArray != null && itemMainIndexArray.size() > 0) {

                                    ArrayList<LsSubIndexData> lsSubIndexDatas = itemMainIndexArray.get(0).getLsSubIndexDatas();
                                    if (lsSubIndexDatas != null && lsSubIndexDatas.size() > 0) {
                                        ArrayList<LsIndexPreviewData> priviewdata = lsSubIndexDatas.get(0).getLsIndexPreviewDatas();
                                        //Global.printLog("VERSE", "=======priviewdata=======" + priviewdata.toString());
                                        for (int l = 0; l < priviewdata.size(); l++) {
//                                                Global.printLog("VERSE", "==============" + "111");
                                            String uritext = priviewdata.get(l).getUri();
                                            //Global.printLog("VERSE", "==============" + "222");
                                            //Global.printLog("VERSE", "=======uritext=======" + uritext.toString());

                                            String[] URI_TXT = todayURI.split("\\.");
                                            if (URI_TXT[0].equalsIgnoreCase(uritext)) {

                                                String[] bits = priviewdata.get(l).getHtml_title().split(" ");
                                                myString_PATH = myString_PATH + " / " + bits[0] + " / " + priviewdata.get(l).getHtml_title();

                                                int lsSubID = priviewdata.get(l).getId();

                                                //Global.printLog("VERSE", "=======lsSubID=======" + lsSubID);

                                                ArrayList<VerseData> stringArrayList = new GetDataFromDatabase(context, lsID).getSubItemHtmlData(lsSubID);
                                                for (int m = 0; m < stringArrayList.size(); m++) {
                                                    try {
                                                        if (stringArrayList.get(m).getId().equals(URI_TXT[1])) {

                                                            if (stringArrayList.get(m).getStringSentense() != null && stringArrayList.get(m).getId() != null)
                                                                myString_PATH = myString_PATH + " : " + stringArrayList.get(m).getId();

                                                            String myStringNew = stringArrayList.get(m).getStringSentense().replace(stringArrayList.get(m).getId(), "");
                                                            SpannableString text = new SpannableString(Global.noTrailingwhiteLines(Html.fromHtml(myStringNew)));

                                                            String verse_txt = text.toString();
                                                            String verse_id = stringArrayList.get(m).getId();

                                                            //.. to remove a,b,c,d all for preview in VerseHighlightActivity.java
                                                            for (int i2 = 0; i2 < stringArrayList.get(m).getFootnoteArray().size(); i2++) {
                                                                verse_txt = verse_txt.replace(stringArrayList.get(m).getFootnoteArray().get(i2), stringArrayList.get(m).getFootnoteArray().get(i2).substring(1));
                                                            }
                                                            getVerseDataFromUri = new GetVerseDataFromUri();
                                                            getVerseDataFromUri.setVerseId(verse_id);
                                                            getVerseDataFromUri.setVerseText(verse_txt);

                                                            Global.printLog("VERSE", "=======verse_txt=======" + verse_txt);
                                                        }
                                                    } catch (Exception e) {
                                                        e.printStackTrace();
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        return getVerseDataFromUri;
    }

    public static String convertDBFormat(String inputDate) {
        SimpleDateFormat inputFormat = new SimpleDateFormat("MMM dd, yyyy", Locale.US);
        SimpleDateFormat outputFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
        Date outputDate = null;
        try {
            outputDate = inputFormat.parse(inputDate);
            return outputFormat.format(outputDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return outputFormat.format(outputDate);
    }

    public static String convertViewFormat(String inputDate) {
        SimpleDateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
        SimpleDateFormat outputFormat = new SimpleDateFormat("MMM dd, yyyy", Locale.US);
        Date outputDate = null;
        try {
            outputDate = inputFormat.parse(inputDate);
            return outputFormat.format(outputDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return outputFormat.format(outputDate);
    }

    public static String addLetter(String colorCode, String letter, int position) {
        return new StringBuilder(new String(colorCode)).insert(position, letter).toString();
    }
}
