package com.ldsscriptures.pro.utils;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Build;
import android.support.annotation.RequiresApi;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.ldsscriptures.pro.activity.LoginOneActivity;
import com.ldsscriptures.pro.activity.MainActivity;
import com.ldsscriptures.pro.activity.SetGoalActivity;
import com.ldsscriptures.pro.activity.subscriptionmenu.PremiumActivity;
import com.ldsscriptures.pro.database.CommonDatabaseHelper;
import com.ldsscriptures.pro.database.HighlightDatabaseHelper;
import com.ldsscriptures.pro.model.AchivementsFlagModel;
import com.ldsscriptures.pro.model.BookmarkData;
import com.ldsscriptures.pro.model.HistorySubData;
import com.ldsscriptures.pro.model.LessonSubItemData;
import com.ldsscriptures.pro.model.LevelModel;
import com.ldsscriptures.pro.model.PlanReminderMainData;
import com.ldsscriptures.pro.model.ReadingNowData;
import com.ldsscriptures.pro.model.ReminderCheckItem;
import com.ldsscriptures.pro.model.SettingPlanReminderMainData;
import com.ldsscriptures.pro.model.VerseCrossReferenceData;
import com.ldsscriptures.pro.model.VerseData;
import com.ldsscriptures.pro.model.VerseTagsData;
import com.ldsscriptures.pro.service.PointEventReceiver;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;

public class SessionManager {

    public static final String KEY_USER_SESSION_ID = "session_id";
    public static final String KEY_USER_PLAN_EXPIRE_DATE = "plan_expire_date";
    public static final String KEY_USER_CONFIRMED = "confirmed";
    public static final String KEY_USER_SUBSCRIBED = "subscribed";
    public static final String KEY_USER_ACHIEVEMENTS = "user_achievements";
    public static final String KEY_USER_PLAN_TYPE = "plan_type";

    // user goal point is for the display the Dashboard of Goal Activity.
    public static final String KEY_USER_GOAL_POINTS = "user_goal_points";
    public static final String KEY_USER_DAY_STREAK = "user_day_streak";
    public static final String KEY_USER_FB_USER = "user_fb_user";
    public static final String KEY_USER_LAST_SYNC_DATE = "user_last_sync_date";

    public static final String KEY_USR_IMG_URL = "user_img_url";
    public static final String KEY_USER_ANNOTATIONS = "user_annotations";
    public static final String KEY_USER_FB_ID = "user_fb_id";
    public static final String KEY_USER_USERNAME = "user_username";
    public static final String KEY_USER_EMAIL = "user_email";
    public static final String KEY_USER_GOAL_HIT = "user_goal_hit";
    public static final String KEY_USER_MIN_READING = "user_min_reading";
    public static final String KEY_USER_MIN_AUDIO = "user_min_audio";
    public static final String KEY_USER_MIN_AUDIO_POINT = "user_min_audio_point";
    public static final String KEY_USER_ID = "user_id";

    private static final String PREF_NAME = "LDS_Session";
    private static final String IS_LOGIN = "IsLoggedIn";
    private static final String IS_GOAL_SET = "IsGoalSet";
    private static final String IS_PREMIUM_SET = "IsPremiumSet";
    public static final String IS_PRIVATE_SET = "IsPrivateSet";
    private static final String IS_NIGHT_MODE = "IsNightMode";
    private static final String IS_AUTO_NIGHT_MODE = "isAutoNightMode";
    private static final String IS_PLAN_TYPE = "plan_type";
    public static final String IS_LAST_DATE = "IsLastDate";
    public static final String IS_DAILY_DATE = "IsDailyDate";
    public static final String IS_CURRANT_DATE = "IsCurrantDate";
    public static final String KEY_USER_POINTS = "user_points";
    public static final String KEY_DAILY_USER_POINTS = "user_daily_points";
    public static final String KEY_DATE_COUNTER = "user_date_count";
    public static final String KEY_GOAL_HIT = "GoalHit";
    public static final String KEY_DATE_COUNTER_CATEGORY2 = "user_date_count_category2";

    private static final String LAST_SYNC_DATE = "last_sync_date";
    private static final String SYNC_ON = "sync";
    private static final String SYNC_ON_WIFI_ONLY = "sync_on_wifi";
    public static final String KEY_DAILY_HIGHTLIGHTS = "Daily_Highlights";
    public static final String KEY_DAILY_TAGS = "Daily_Tags";
    public static final String KEY_DAILY_BOOKMARKS = "Daily_Bookmarks";
    public static final String KEY_OFFSET = "offset";

    public static final String KEY_FONT_SIZE = "font_size";
    public static final String KEY_FONT_STYLE = "font_style";
    public static boolean isHighlight = false;

    public static final String KEY_USER_LANG_ID = "language_id";
    public static final String KEY_USER_LANG_COLLECTIONID = "language_coll_id";
    public static final String KEY_USER_LANG_CHANGED = "language_changed";
    public static final String KEY_USER_LANG_CODE = "language_code";

    public static final String KEY_CITATIONS_URL = "Citations_url";
    public static final String KEY_DESTPATH = "destPath1";
    public static final String KEY_START_LIMIT = "start_limit";
    public static final String KEY_OLD_DB_VERSION = "old_dbversion";
    public static final String KEY_LATEST_DB_VERSION = "latest_dbversion";
    public static final String KEY_SUBSCRIBED = "subscribed";
    public static final String KEY_TIME_ZONE = "time_zone";
    public static final int IS_ACHIVEMENTS_LEVEL_1 = 300;
    public static final int IS_ACHIVEMENTS_LEVEL_2 = 1000;
    public static final int IS_ACHIVEMENTS_LEVEL_3 = 2500;
    public static final int IS_ACHIVEMENTS_LEVEL_4 = 5000;
    public static final int IS_ACHIVEMENTS_LEVEL_5 = 7500;
    public static final int IS_ACHIVEMENTS_LEVEL_6 = 10000;
    public static final int IS_ACHIVEMENTS_LEVEL_7 = 15000;
    public static final int IS_ACHIVEMENTS_LEVEL_8 = 25000;
    public static final int IS_ACHIVEMENTS_LEVEL_9 = 35000;
    public static final int IS_ACHIVEMENTS_LEVEL_10 = 50000;
    public static final int IS_ACHIVEMENTS_LEVEL_11 = 75000;
    public static final int IS_ACHIVEMENTS_LEVEL_12 = 100000;
    public static final int IS_ACHIVEMENTS_LEVEL_13 = 125000;
    public static final int IS_ACHIVEMENTS_LEVEL_14 = 150000;
    public static final int IS_ACHIVEMENTS_LEVEL_15 = 175000;
    public static final int IS_ACHIVEMENTS_LEVEL_16 = 200000;
    public static final int IS_ACHIVEMENTS_LEVEL_17 = 225000;
    public static final int IS_ACHIVEMENTS_LEVEL_18 = 250000;
    public static final int IS_ACHIVEMENTS_LEVEL_19 = 275000;
    public static final int IS_ACHIVEMENTS_LEVEL_20 = 300000;

    ArrayList<AchivementsFlagModel> achivementsFlagarraylist = new ArrayList<>();
    ArrayList<LevelModel> Levelarraylist = new ArrayList<>();

    public static Editor editor;
    SharedPreferences sharedPreferences;
    Activity mContext;
    Context mContext1;
    int PRIVATE_MODE = 0;
    CommonDatabaseHelper commonDatabaseHelper;
    HighlightDatabaseHelper highlightDb;

    // Constructor
    public SessionManager(Activity context) {
        this.mContext = context;
        sharedPreferences = context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        editor = sharedPreferences.edit();
        commonDatabaseHelper = new CommonDatabaseHelper(context);
        highlightDb = new HighlightDatabaseHelper(context);
    }

    public SessionManager(Context mContext1) {
        this.mContext1 = mContext1;
        sharedPreferences = mContext1.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        editor = sharedPreferences.edit();
    }

    public void createLoginSession(String session_id, String plan_expire_date, String confirmed, String subscribed, String user_achievements, String user_goal_points, String user_day_streak, String user_fb_user, String user_points, String user_img_url, String user_annotations, String user_fb_id, String user_username, String user_email, String user_goal_hit, String user_min_reading, String user_id, String private_status) {
        editor.putBoolean(IS_LOGIN, true);
        editor.putString(KEY_USER_SESSION_ID, session_id);
        editor.putString(KEY_USER_PLAN_EXPIRE_DATE, plan_expire_date);
        editor.putString(KEY_USER_CONFIRMED, confirmed);
        editor.putString(KEY_USER_SUBSCRIBED, subscribed);
        editor.putString(KEY_USER_ACHIEVEMENTS, user_achievements);
        editor.putString(KEY_USER_GOAL_POINTS, user_goal_points);
        editor.putString(KEY_USER_DAY_STREAK, user_day_streak);
        editor.putString(KEY_USER_FB_USER, user_fb_user);
        editor.putString(KEY_USER_POINTS, user_points);
        editor.putString(KEY_USR_IMG_URL, user_img_url);
        editor.putString(KEY_USER_ANNOTATIONS, user_annotations);
        editor.putString(KEY_USER_FB_ID, user_fb_id);
        editor.putString(KEY_USER_USERNAME, user_username);
        editor.putString(KEY_USER_EMAIL, user_email);
        editor.putString(KEY_USER_GOAL_HIT, user_goal_hit);
        editor.putString(KEY_USER_MIN_READING, user_min_reading);
        editor.putString(KEY_USER_ID, user_id);
        editor.putString(IS_PRIVATE_SET, private_status);
        editor.commit();
    }

    public void setPreferencesArrayList(Context context, ArrayList<ReadingNowData> tabsDataArrayList) {
        Gson gson = new Gson();
        String json = gson.toJson(tabsDataArrayList);
        editor.putString("tabsDataArrayList", json);
        editor.commit();
    }

    public ArrayList<ReadingNowData> getPreferencesArrayList(Context context) {

        Gson gson = new Gson();
        String json = sharedPreferences.getString("tabsDataArrayList", "");
        Type type = new TypeToken<ArrayList<ReadingNowData>>() {
        }.getType();

        ArrayList<ReadingNowData> keyboardDataArrayList = gson.fromJson(json, type);

        return keyboardDataArrayList;
    }

    public void setSyncStatus(boolean status) {
        editor.putBoolean(SYNC_ON, status);
        editor.commit();
    }

    public void setSyncWifiStatus(boolean status) {
        editor.putBoolean(SYNC_ON_WIFI_ONLY, status);
        editor.commit();
    }

    public boolean getSyncStatus() {
        return sharedPreferences.getBoolean(SYNC_ON, false);
    }

    public boolean getSyncWifiStatus() {
        return sharedPreferences.getBoolean(SYNC_ON_WIFI_ONLY, false);
    }

    public void setPreferencesHistoryArrayList(Context context, ArrayList<HistorySubData> historyDataArrayList) {
        Gson gson = new Gson();
        String json = gson.toJson(historyDataArrayList);
        editor.putString("historyDataArrayList", json);
        editor.commit();
    }

    public ArrayList<HistorySubData> getPreferencesHistoryArrayList(Context context) {
        Gson gson = new Gson();
        String json = sharedPreferences.getString("historyDataArrayList", "");
        Type type = new TypeToken<ArrayList<HistorySubData>>() {
        }.getType();
        ArrayList<HistorySubData> keyboardDataArrayList = gson.fromJson(json, type);
        return keyboardDataArrayList;
    }

    public void setPlanReminderData(ArrayList<PlanReminderMainData> spanDatas) {
        Gson gson = new Gson();
        String json = gson.toJson(spanDatas);
        editor.putString("PlanReminderMainData", json);
        editor.commit();
    }

    public ArrayList<PlanReminderMainData> getPlanReminderData() {
        Gson gson = new Gson();
        String json = sharedPreferences.getString("PlanReminderMainData", "");
        Type type = new TypeToken<ArrayList<PlanReminderMainData>>() {
        }.getType();

        ArrayList<PlanReminderMainData> spanDatas = gson.fromJson(json, type);

        return spanDatas;
    }

    public void setPreferencesTagsArrayList(Context context, ArrayList<VerseTagsData> tabsDataArrayList) {
        Gson gson = new Gson();
        String json = gson.toJson(tabsDataArrayList);
        editor.putString("verseTagsArrayList", json);
        editor.commit();
    }

    public ArrayList<VerseTagsData> getPreferencesTagsArrayList(Context context) {
        Gson gson = new Gson();
        String json = sharedPreferences.getString("verseTagsArrayList", "");
        Type type = new TypeToken<ArrayList<VerseTagsData>>() {
        }.getType();

        ArrayList<VerseTagsData> keyboardDataArrayList = gson.fromJson(json, type);

        return keyboardDataArrayList;
    }

    public void setPreferencesCrossRefArrayList(Context context, ArrayList<VerseCrossReferenceData> verseCrossReferenceDataArrayList) {
        Gson gson = new Gson();
        String json = gson.toJson(verseCrossReferenceDataArrayList);
        editor.putString("verseCrossReferenceDataArrayList", json);
        editor.commit();
    }

    public ArrayList<VerseCrossReferenceData> getPreferencesCrossRefArrayList(Context context) {
        Gson gson = new Gson();
        String json = sharedPreferences.getString("verseCrossReferenceDataArrayList", "");
        Type type = new TypeToken<ArrayList<VerseCrossReferenceData>>() {
        }.getType();
        ArrayList<VerseCrossReferenceData> keyboardDataArrayList = gson.fromJson(json, type);
        return keyboardDataArrayList;
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    public void checkLogin() {

        if (!isLoggedIn()) {
            Intent notLogin = new Intent(mContext, LoginOneActivity.class);
            notLogin.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            notLogin.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            mContext.startActivity(notLogin);
            mContext.overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
            mContext.finishAffinity();
        } else {
            checkGoalSet();
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    public void checkGoalSet() {

        if (!getLoginDetails().get(KEY_USER_GOAL_POINTS).equals("null")) {

            if (isPremiumSet()) {
                Intent login = new Intent(mContext, MainActivity.class);
                login.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                login.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                mContext.startActivity(login);
                mContext.overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
                mContext.finishAffinity();
            } else {
                Intent login = new Intent(mContext, PremiumActivity.class);
                login.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                login.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                mContext.startActivity(login);
                mContext.overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
                mContext.finishAffinity();
            }

        } else {
            if (!isGoalSet()) {
                Intent notLogin = new Intent(mContext, SetGoalActivity.class);
                notLogin.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                notLogin.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                mContext.startActivity(notLogin);
                mContext.overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
                mContext.finishAffinity();

            } else {
                if (isPremiumSet()) {
                    Intent login = new Intent(mContext, MainActivity.class);
                    login.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    login.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    mContext.startActivity(login);
                    mContext.overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
                    mContext.finishAffinity();
                } else {
                    Intent login = new Intent(mContext, PremiumActivity.class);
                    login.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    login.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    mContext.startActivity(login);
                    mContext.overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
                    mContext.finishAffinity();
                }

            }
        }

    }

    public HashMap<String, String> getLoginDetails() {
        HashMap<String, String> user = new HashMap<>();

        user.put(KEY_USER_SESSION_ID, sharedPreferences.getString(KEY_USER_SESSION_ID, null));
        user.put(KEY_USER_PLAN_EXPIRE_DATE, sharedPreferences.getString(KEY_USER_PLAN_EXPIRE_DATE, null));
        user.put(KEY_USER_CONFIRMED, sharedPreferences.getString(KEY_USER_CONFIRMED, null));
        user.put(KEY_USER_SUBSCRIBED, sharedPreferences.getString(KEY_USER_SUBSCRIBED, null));
        user.put(KEY_USER_ACHIEVEMENTS, sharedPreferences.getString(KEY_USER_ACHIEVEMENTS, null));
        user.put(KEY_USER_GOAL_POINTS, sharedPreferences.getString(KEY_USER_GOAL_POINTS, null));
        user.put(KEY_USER_DAY_STREAK, sharedPreferences.getString(KEY_USER_DAY_STREAK, null));
        user.put(KEY_USER_FB_USER, sharedPreferences.getString(KEY_USER_FB_USER, null));
        user.put(KEY_USER_POINTS, sharedPreferences.getString(KEY_USER_POINTS, null));
        user.put(KEY_USR_IMG_URL, sharedPreferences.getString(KEY_USR_IMG_URL, null));
        user.put(KEY_USER_ANNOTATIONS, sharedPreferences.getString(KEY_USER_ANNOTATIONS, null));
        user.put(KEY_USER_FB_ID, sharedPreferences.getString(KEY_USER_FB_ID, null));
        user.put(KEY_USER_USERNAME, sharedPreferences.getString(KEY_USER_USERNAME, null));
        user.put(KEY_USER_EMAIL, sharedPreferences.getString(KEY_USER_EMAIL, null));
        user.put(KEY_USER_GOAL_HIT, sharedPreferences.getString(KEY_USER_GOAL_HIT, null));
        user.put(KEY_USER_MIN_READING, sharedPreferences.getString(KEY_USER_MIN_READING, null));
        user.put(KEY_USER_ID, sharedPreferences.getString(KEY_USER_ID, null));
        user.put(IS_PRIVATE_SET, sharedPreferences.getString(IS_PRIVATE_SET, ""));
        return user;
    }

    public void setUserGoal(String userGoal) {
        editor.putString(KEY_USER_GOAL_POINTS, userGoal);
        editor.commit();
    }

    public void setParameter(String key, String value) {
        editor.putString(key, value);
        editor.commit();
    }

    public String getUserGoal() {
        return sharedPreferences.getString(KEY_USER_GOAL_POINTS, "");
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    public void logoutUser() {

        editor.putBoolean(IS_LOGIN, false);
        editor.putBoolean(IS_GOAL_SET, false);
        editor.putString(KEY_USER_MIN_READING, "0");
        editor.putInt(KEY_DAILY_USER_POINTS, 0);
        editor.clear();
        editor.commit();

        commonDatabaseHelper.clearDataFromTable();
        highlightDb.clearDataFromTable();

        PointEventReceiver.cancelAlarm(mContext);

        Intent logout = new Intent(mContext, LoginOneActivity.class);
        logout.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        logout.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        mContext.startActivity(logout);
        mContext.overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
        mContext.finishAffinity();
    }

    public boolean isLoggedIn() {
        return sharedPreferences.getBoolean(IS_LOGIN, false);
    }

    public static void setGoalSelected() {
        editor.putBoolean(IS_GOAL_SET, true);
        editor.commit();
    }

    public static void setAutoNightModeOn(boolean modeStatus) {
        editor.putBoolean(IS_AUTO_NIGHT_MODE, modeStatus);
        editor.commit();
    }

    public boolean isAutoNightMode() {
        return sharedPreferences.getBoolean(IS_AUTO_NIGHT_MODE, false);
    }

    public static void setNightModeOn() {
        editor.putBoolean(IS_NIGHT_MODE, true);
        editor.commit();
    }

    public static void setNightModeOff() {
        editor.putBoolean(IS_NIGHT_MODE, false);
        editor.commit();
    }

    public boolean isGoalSet() {
        return sharedPreferences.getBoolean(IS_GOAL_SET, false);
    }

    public boolean isNightMode() {
        return sharedPreferences.getBoolean(IS_NIGHT_MODE, false);
    }

    public void setLastDate(String last_date) {
        editor.putString(IS_LAST_DATE, last_date);
        editor.commit();
    }

    public int getFontSize() {
        return sharedPreferences.getInt(KEY_FONT_SIZE, 0);
    }

    public void setKeyFontSize(int fontSize) {
        editor.putInt(KEY_FONT_SIZE, fontSize);
        editor.commit();
    }

    public String getFontStyle() {
        return sharedPreferences.getString(KEY_FONT_STYLE, "");
    }

    public void setKeyFontStyle(String fontStyle) {
        editor.putString(KEY_FONT_STYLE, fontStyle);
        editor.commit();
    }

    public String getLastDate() {
        return sharedPreferences.getString(IS_LAST_DATE, "");
    }

    public void setUserMinReading(String userMinReading) {
        editor.putString(KEY_USER_MIN_READING, userMinReading);
        editor.commit();
    }

    public String getUserMinReading() {
        return sharedPreferences.getString(KEY_USER_MIN_READING, "0");
    }

    public int getUserDailyPoints() {
        return sharedPreferences.getInt(KEY_DAILY_USER_POINTS, 0);
    }

    public void setUserDailyPoints(int userDailyPoints) {
        editor.putInt(KEY_DAILY_USER_POINTS, getUserDailyPoints() + userDailyPoints);
        editor.commit();
    }

    public String getLastSyncDate() {
        return sharedPreferences.getString(LAST_SYNC_DATE, "");
    }

    public void setLastSyncDate(String lastSyncDate) {
        editor.putString(LAST_SYNC_DATE, lastSyncDate);
        editor.commit();
    }

    public int getDateCounter() {
        return sharedPreferences.getInt(KEY_DATE_COUNTER, 0);
    }

    public void setDateCounter(int dateCounter) {
        editor.putInt(KEY_DATE_COUNTER, getDateCounter() + dateCounter);
        editor.commit();
    }

    public void setResetDateCounter(int dateCounter) {
        editor.putInt(KEY_DATE_COUNTER, dateCounter);
        editor.commit();
    }

    public void setAchivementsFlagArrayList() {

        achivementsFlagarraylist = new ArrayList<>();
        //STREAK
        achivementsFlagarraylist.add(new AchivementsFlagModel(101, false, false, "streak_3", "0", ""));
        achivementsFlagarraylist.add(new AchivementsFlagModel(102, false, false, "streak_5", "0", ""));
        achivementsFlagarraylist.add(new AchivementsFlagModel(103, false, false, "streak_7", "0", ""));
        achivementsFlagarraylist.add(new AchivementsFlagModel(104, false, false, "streak_10", "0", ""));
        achivementsFlagarraylist.add(new AchivementsFlagModel(105, false, false, "streak_14", "0", ""));
        achivementsFlagarraylist.add(new AchivementsFlagModel(106, false, false, "streak_30", "0", ""));
        achivementsFlagarraylist.add(new AchivementsFlagModel(107, false, false, "streak_100", "0", ""));
        achivementsFlagarraylist.add(new AchivementsFlagModel(108, false, false, "streak_365", "0", ""));

        // GOALS
        achivementsFlagarraylist.add(new AchivementsFlagModel(201, false, false, "goal_hit", "0", ""));
        achivementsFlagarraylist.add(new AchivementsFlagModel(202, false, false, "goal_150", "0", ""));
        achivementsFlagarraylist.add(new AchivementsFlagModel(203, false, false, "goal_200", "0", ""));
        achivementsFlagarraylist.add(new AchivementsFlagModel(204, false, false, "goal_300", "0", ""));
        achivementsFlagarraylist.add(new AchivementsFlagModel(205, false, false, "goal_400", "0", ""));
        achivementsFlagarraylist.add(new AchivementsFlagModel(206, false, false, "goal_500", "0", ""));

        achivementsFlagarraylist.add(new AchivementsFlagModel(2011, false, false, "goal_3", "0", ""));
        achivementsFlagarraylist.add(new AchivementsFlagModel(2021, false, false, "goal_7", "0", ""));
        achivementsFlagarraylist.add(new AchivementsFlagModel(2031, false, false, "goal_14", "0", ""));
        achivementsFlagarraylist.add(new AchivementsFlagModel(2041, false, false, "goal_50", "0", ""));
        achivementsFlagarraylist.add(new AchivementsFlagModel(2051, false, false, "goal_100", "0", ""));
        achivementsFlagarraylist.add(new AchivementsFlagModel(2061, false, false, "goal_365", "0", ""));

        // SCRIPTURE FEAST
        achivementsFlagarraylist.add(new AchivementsFlagModel(301, false, false, "feast_5k", "0", ""));
        achivementsFlagarraylist.add(new AchivementsFlagModel(302, false, false, "feast_6k", "0", ""));
        achivementsFlagarraylist.add(new AchivementsFlagModel(303, false, false, "feast_7k", "0", ""));
        achivementsFlagarraylist.add(new AchivementsFlagModel(304, false, false, "feast_10k", "0", ""));
        achivementsFlagarraylist.add(new AchivementsFlagModel(305, false, false, "feast_15k", "0", ""));
        achivementsFlagarraylist.add(new AchivementsFlagModel(306, false, false, "feast_20k", "0", ""));
        achivementsFlagarraylist.add(new AchivementsFlagModel(307, false, false, "feast_30k", "0", ""));
        achivementsFlagarraylist.add(new AchivementsFlagModel(308, false, false, "feast_40k", "0", ""));
        achivementsFlagarraylist.add(new AchivementsFlagModel(309, false, false, "feast_50k", "0", ""));
        achivementsFlagarraylist.add(new AchivementsFlagModel(3010, false, false, "feast_75k", "0", ""));

        achivementsFlagarraylist.add(new AchivementsFlagModel(401, false, false, "hidden_11", "0", "January 01"));
        achivementsFlagarraylist.add(new AchivementsFlagModel(402, false, false, "hidden_214", "0", "February 14"));
        achivementsFlagarraylist.add(new AchivementsFlagModel(403, false, false, "hidden_46", "0", "April 06"));
        achivementsFlagarraylist.add(new AchivementsFlagModel(404, false, false, "hidden_515", "0", "May 15"));
        achivementsFlagarraylist.add(new AchivementsFlagModel(405, false, false, "hidden_724", "0", "July 24"));
        achivementsFlagarraylist.add(new AchivementsFlagModel(406, false, false, "hidden_922", "0", "September 22"));
        achivementsFlagarraylist.add(new AchivementsFlagModel(407, false, false, "hidden_1223", "0", "December 23"));
        achivementsFlagarraylist.add(new AchivementsFlagModel(408, false, false, "hidden_1225", "0", "December 25"));
        achivementsFlagarraylist.add(new AchivementsFlagModel(409, false, false, "hidden_100h", "0", ""));
        achivementsFlagarraylist.add(new AchivementsFlagModel(4010, false, false, "hidden_500h", "0", ""));
        achivementsFlagarraylist.add(new AchivementsFlagModel(4011, false, false, "hidden_1000h", "0", ""));
        achivementsFlagarraylist.add(new AchivementsFlagModel(4012, false, false, "hidden_100t", "0", ""));
        achivementsFlagarraylist.add(new AchivementsFlagModel(4013, false, false, "hidden_500t", "0", ""));
        achivementsFlagarraylist.add(new AchivementsFlagModel(4014, false, false, "hidden_1000t", "0", ""));
        achivementsFlagarraylist.add(new AchivementsFlagModel(4015, false, false, "hidden_100b", "0", ""));
        achivementsFlagarraylist.add(new AchivementsFlagModel(4016, false, false, "hidden_500b", "0", ""));
        achivementsFlagarraylist.add(new AchivementsFlagModel(4017, false, false, "hidden_1000b", "0", ""));


        saveAchivementsFlagArraylist(achivementsFlagarraylist);
    }

    public void saveAchivementsFlagArraylist(ArrayList<AchivementsFlagModel> achivementsFlagarraylist) {
        Gson gson = new Gson();
        String json = gson.toJson(achivementsFlagarraylist);
        editor.putString("achivementsFlagarraylist", json);
        editor.commit();
    }

    public ArrayList<AchivementsFlagModel> getAchivementsFlagArrayList() {
        Gson gson = new Gson();
        String json = sharedPreferences.getString("achivementsFlagarraylist", "");
        Type type = new TypeToken<ArrayList<AchivementsFlagModel>>() {
        }.getType();

        ArrayList<AchivementsFlagModel> AchivementsFlagDatas = gson.fromJson(json, type);

        return AchivementsFlagDatas;
    }

    public void setReadingNow(ReadingNowData readingNowData) {
        Gson gson = new Gson();
        String json = gson.toJson(readingNowData);
        editor.putString("readingNow", json);
        editor.commit();
    }

    public ReadingNowData getReadingNow() {
        Gson gson = new Gson();
        String json = sharedPreferences.getString("readingNow", "");
        Type type = new TypeToken<ReadingNowData>() {
        }.getType();
        ReadingNowData readingNowData = gson.fromJson(json, type);
        return readingNowData;
    }

    public void setGoalHit() {
        double intCurrentGoal = Double.parseDouble(String.valueOf(getUserDailyPoints()));
        double intTargetGoal = 0;
        if (getUserGoal() == "" || getUserGoal() == null || getUserGoal().equals("null")) {
            intTargetGoal = 0;
        } else {
            intTargetGoal = Double.parseDouble(getUserGoal());
        }

        int goalHitPercent = (int) Math.round((intCurrentGoal / intTargetGoal) * 100);
        editor.putString(KEY_GOAL_HIT, String.valueOf(goalHitPercent));
        editor.commit();


    }

    public String getGoalHit() {
        return sharedPreferences.getString(KEY_GOAL_HIT, "");
    }

    public void setDailyDate(String daily_date) {
        editor.putString(IS_DAILY_DATE, daily_date);
        editor.commit();
    }

    public String getDailyDate() {
        return sharedPreferences.getString(IS_DAILY_DATE, "");
    }

    public void setDateCounterCategory2(int dateCounter) {
        editor.putInt(KEY_DATE_COUNTER_CATEGORY2, getDateCounterCategory2() + dateCounter);
        editor.commit();
    }

    public int getDateCounterCategory2() {
        return sharedPreferences.getInt(KEY_DATE_COUNTER_CATEGORY2, 0);
    }

    public void setNotificationPlanReminderData(ArrayList<PlanReminderMainData> spanDatas) {
        Gson gson = new Gson();
        String json = gson.toJson(spanDatas);
        editor.putString("NotificationPlanReminderData", json);
        editor.commit();
    }

    public ArrayList<PlanReminderMainData> getNotificationPlanReminderData() {
        Gson gson = new Gson();
        String json = sharedPreferences.getString("NotificationPlanReminderData", "");
        Type type = new TypeToken<ArrayList<PlanReminderMainData>>() {
        }.getType();

        ArrayList<PlanReminderMainData> spanDatas = gson.fromJson(json, type);

        return spanDatas;
    }

    public void setCheckedNotification(ArrayList<ReminderCheckItem> spanDatas) {
        Gson gson = new Gson();
        String json = gson.toJson(spanDatas);
        editor.putString("ReminderCheckItem", json);
        editor.commit();
    }

    public ArrayList<ReminderCheckItem> getCheckedNotification() {
        Gson gson = new Gson();
        String json = sharedPreferences.getString("ReminderCheckItem", "");
        Type type = new TypeToken<ArrayList<ReminderCheckItem>>() {
        }.getType();

        ArrayList<ReminderCheckItem> spanDatas = gson.fromJson(json, type);

        return spanDatas;
    }

    public void setCurrantDate(String curramt_date) {
        editor.putString(IS_CURRANT_DATE, curramt_date);
        editor.commit();
    }

    public String getCurrantDate() {
        return sharedPreferences.getString(IS_CURRANT_DATE, "");
    }

    public void setSettingPlanReminderData(ArrayList<SettingPlanReminderMainData> spanDatas) {
        Gson gson = new Gson();
        String json = gson.toJson(spanDatas);
        editor.putString("SettingPlanReminderMainData", json);
        editor.commit();
    }

    public ArrayList<SettingPlanReminderMainData> getSettingPlanReminderData() {
        Gson gson = new Gson();
        String json = sharedPreferences.getString("SettingPlanReminderMainData", "");
        Type type = new TypeToken<ArrayList<SettingPlanReminderMainData>>() {
        }.getType();

        ArrayList<SettingPlanReminderMainData> spanDatas = gson.fromJson(json, type);

        return spanDatas;
    }

    public String getLanguageID() {
        return sharedPreferences.getString(KEY_USER_LANG_ID, "1");
    }

    public String getLangCollectionID() {
        return sharedPreferences.getString(KEY_USER_LANG_COLLECTIONID, "65537");
    }

    public void setLanguageID(String id) {
        editor.putString(KEY_USER_LANG_ID, id);
        editor.commit();
    }

    public void setLangCollID(String id) {
        editor.putString(KEY_USER_LANG_COLLECTIONID, id);
        editor.commit();
    }

    public void setLanguageChanged(boolean value) {
        editor.putBoolean(KEY_USER_LANG_CHANGED, value);
        editor.commit();
    }

    public void setLanguageCode(String code) {
        editor.putString(KEY_USER_LANG_CODE, code);
        editor.commit();
    }

    public boolean isLanguageChanged() {
        return sharedPreferences.getBoolean(KEY_USER_LANG_CHANGED, false);
    }

    public String getLanguageCode() {
        return sharedPreferences.getString(KEY_USER_LANG_CODE, "eng");
    }

    public String getUserInfo(String getStr) {
        String string = null;
        if (isLoggedIn()) {
            HashMap<String, String> hashMap = getLoginDetails();
            string = hashMap.get(getStr);
        }
        return string;
    }

    public String getAchivementCount() {
        ArrayList<AchivementsFlagModel> getAchivementsFlagArrayList1 = new ArrayList<>();
        String achivementCount = "0";

        Global.printLog("COUNT", "" + getAchivementsFlagArrayList().size());

        if (getAchivementsFlagArrayList().size() != 0) {
            for (int i = 0; i < getAchivementsFlagArrayList().size(); i++) {
                if (getAchivementsFlagArrayList().get(i).getAchivements_flag() == true) {
                    getAchivementsFlagArrayList1.add(getAchivementsFlagArrayList().get(i));
                }
            }
            achivementCount = String.valueOf(getAchivementsFlagArrayList1.size());
        }
        return achivementCount;
    }

    public void setLevelArrayList() {

        Levelarraylist.add(new LevelModel(IS_ACHIVEMENTS_LEVEL_1));
        Levelarraylist.add(new LevelModel(IS_ACHIVEMENTS_LEVEL_2));
        Levelarraylist.add(new LevelModel(IS_ACHIVEMENTS_LEVEL_3));
        Levelarraylist.add(new LevelModel(IS_ACHIVEMENTS_LEVEL_4));
        Levelarraylist.add(new LevelModel(IS_ACHIVEMENTS_LEVEL_5));
        Levelarraylist.add(new LevelModel(IS_ACHIVEMENTS_LEVEL_6));
        Levelarraylist.add(new LevelModel(IS_ACHIVEMENTS_LEVEL_7));
        Levelarraylist.add(new LevelModel(IS_ACHIVEMENTS_LEVEL_8));

        saveLevelArraylist(Levelarraylist);
    }

    public void saveLevelArraylist(ArrayList<LevelModel> levelarraylist) {
        Gson gson = new Gson();
        String json = gson.toJson(levelarraylist);
        editor.putString("levelarraylist", json);
        editor.commit();
    }

    public ArrayList<LevelModel> getLevelArrayList() {
        Gson gson = new Gson();
        String json = sharedPreferences.getString("levelarraylist", "");
        Type type = new TypeToken<ArrayList<LevelModel>>() {
        }.getType();

        ArrayList<LevelModel> levelModelArrayList = gson.fromJson(json, type);

        return levelModelArrayList;
    }

    public String getLevelCount() {
        String levelCount = "0";
        if (getLevelArrayList() != null) {

            for (int i = 0; i < getLevelArrayList().size(); i++) {

                int levelPoint = getLevelArrayList().get(i).getLevel_point();
                int dailypoint = getUserDailyPoints();

                if (dailypoint >= levelPoint) {
                    levelCount = String.valueOf(i + 1);
                }
            }
        }
        return levelCount;
    }

    public int getHightLightCount() {
        return sharedPreferences.getInt(KEY_DAILY_HIGHTLIGHTS, 0);
    }

    public void setHighLightCount(int userDailyHighLight) {
        editor.putInt(KEY_DAILY_HIGHTLIGHTS, getHightLightCount() + userDailyHighLight);
        editor.commit();
    }

    public int getTagCount() {
        return sharedPreferences.getInt(KEY_DAILY_TAGS, 0);
    }

    public void setTagCount(int tagCount) {
        editor.putInt(KEY_DAILY_TAGS, getTagCount() + tagCount);
        editor.commit();
    }

    public int getBookmarkCount() {
        return sharedPreferences.getInt(KEY_DAILY_BOOKMARKS, 0);
    }

    public void setBookmarkCount(int tagCount) {
        editor.putInt(KEY_DAILY_BOOKMARKS, getBookmarkCount() + tagCount);
        editor.commit();
    }

    public static void setPremiumSelected(boolean flag) {
        editor.putBoolean(IS_PREMIUM_SET, flag);
        editor.commit();
    }

    public boolean isPremiumSet() {
        return sharedPreferences.getBoolean(IS_PREMIUM_SET, false);
    }

    public void setReadInnerArrayList(ArrayList<VerseData> stringArrayList, String nextId) {
        Gson gson = new Gson();
        String json = gson.toJson(stringArrayList);
        editor.putString(nextId, json);
        editor.commit();
    }

    public ArrayList<VerseData> getReadInnerArrayList(String nextId) {
        Gson gson = new Gson();
        String json = sharedPreferences.getString(nextId, "");
        Type type = new TypeToken<ArrayList<VerseData>>() {
        }.getType();
        ArrayList<VerseData> stringArrayList = gson.fromJson(json, type);
        return stringArrayList;
    }

    public int getoffset() {
        return sharedPreferences.getInt(KEY_OFFSET, 0);
    }

    public void setoffset(int offsetpos) {
        editor.putInt(KEY_OFFSET, offsetpos);
        editor.commit();
    }

    public String getSubscribeType() {
        return sharedPreferences.getString(IS_PLAN_TYPE, "");
    }

    public void setSubscribeType(String plantype) {
        editor.putString(IS_PLAN_TYPE, plantype);
        editor.commit();
    }

    public String getUserId() {
        return sharedPreferences.getString(KEY_USER_ID, "");
    }

    public void setUserID(String userID) {
        editor.putString(KEY_USER_ID, userID);
        editor.commit();
    }

    public String getUserSessionId() {
        return sharedPreferences.getString(KEY_USER_SESSION_ID, "");
    }

    public void setUserSessionId(String sessionId) {
        editor.putString(KEY_USER_SESSION_ID, sessionId);
        editor.commit();
    }

    public int getStartLimit() {
        return sharedPreferences.getInt(KEY_START_LIMIT, 0);
    }

    public void setStartLimit(int startLimit) {
        editor.putInt(KEY_START_LIMIT, startLimit);
        editor.commit();
    }

    public String getKeyOldDbVersion() {
        return sharedPreferences.getString(KEY_OLD_DB_VERSION, "");
    }

    public void setKeyOldDbVersion(String dbVersion) {
        editor.putString(KEY_OLD_DB_VERSION, dbVersion);
        editor.commit();
    }

    public String getKeySubscribed() {
        return sharedPreferences.getString(KEY_SUBSCRIBED, "");
    }

    public void setKeySubscribed(String subscription) {
        editor.putString(KEY_SUBSCRIBED, subscription);
        editor.commit();
    }

    public String getKeyTimeZone() {
        return sharedPreferences.getString(KEY_TIME_ZONE, "");
    }

    public void setKeyTimeZone(String timeZone) {
        editor.putString(KEY_TIME_ZONE, timeZone);
        editor.commit();
    }

    public void saveLessionSortOrderArraylist(ArrayList<LessonSubItemData> LessionSortOrderArraylist) {
        Gson gson = new Gson();
        String json = gson.toJson(LessionSortOrderArraylist);
        editor.putString("LessionSortOrderArraylist", json);
        editor.commit();
    }

    public ArrayList<LessonSubItemData> getLessionSortOrderArraylist() {
        Gson gson = new Gson();
        String json = sharedPreferences.getString("LessionSortOrderArraylist", "");
        Type type = new TypeToken<ArrayList<LessonSubItemData>>() {
        }.getType();
        ArrayList<LessonSubItemData> LessionSortOrderArraylist = gson.fromJson(json, type);
        return LessionSortOrderArraylist;
    }

    public void saveBookmarkSortOrderArraylist(ArrayList<BookmarkData> BookmarkSortOrderArraylist) {
        Gson gson = new Gson();
        String json = gson.toJson(BookmarkSortOrderArraylist);
        editor.putString("BookmarkSortOrderArraylist", json);
        editor.commit();
    }

    public ArrayList<BookmarkData> getBookmarkSortOrderArraylist() {
        Gson gson = new Gson();
        String json = sharedPreferences.getString("BookmarkSortOrderArraylist", "");
        Type type = new TypeToken<ArrayList<BookmarkData>>() {
        }.getType();
        ArrayList<BookmarkData> BookmarkSortOrderArraylist = gson.fromJson(json, type);
        return BookmarkSortOrderArraylist;
    }

    public void setUserMinAudio(int userMinReading) {
        editor.putInt(KEY_USER_MIN_AUDIO, userMinReading);
        editor.commit();
    }

    public int getKeyUserMinAudio() {
        return sharedPreferences.getInt(KEY_USER_MIN_AUDIO, 0);
    }

    public int getUserMinAudioPoints() {
        return sharedPreferences.getInt(KEY_USER_MIN_AUDIO_POINT, 0);
    }

    public void setKeyUserMinAudioPoint(int keyUserMinAudioPoint) {
        editor.putInt(KEY_USER_MIN_AUDIO_POINT, keyUserMinAudioPoint);
        editor.commit();
    }
}
