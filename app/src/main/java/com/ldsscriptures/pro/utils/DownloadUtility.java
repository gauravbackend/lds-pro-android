package com.ldsscriptures.pro.utils;

import android.util.Log;

import com.ldsscriptures.pro.model.Download;
import com.ldsscriptures.pro.retroutils.APIClient;
import com.ldsscriptures.pro.retroutils.APIInterface;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class DownloadUtility {
    private static final String TAG = "DownloadUtility";

    public void downloadDBZipFile(String catalog_url, final String id) {
        APIInterface apiService = APIClient.getClient().create(APIInterface.class);

        Call<ResponseBody> call = apiService.downloadFileWithDynamicUrl(catalog_url);

        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.body() != null && !response.body().toString().trim().isEmpty()) {

                    boolean writtenToDisk = false;
                    try {
                        writtenToDisk = writeResponseBodyToDisk(response.body(), id);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                } else {
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });
    }

    private File getOutputMediaFile(String filename, String id) {
        File mediaStorageDir = new File(Global.AppDownloadFolder + "/" + id);

        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                return null;
            }
        }
        File mediaFile;
        String mImageName = filename;
        mediaFile = new File(mediaStorageDir, mImageName);
        return mediaFile;
    }

    private boolean writeResponseBodyToDisk(ResponseBody body, String id) throws IOException {

        int count;
        byte data[] = new byte[1024 * 4];
        long fileSize = body.contentLength();
        InputStream bis = new BufferedInputStream(body.byteStream(), 1024 * 8);
        File outputFile = getOutputMediaFile("file.zip", id);

        OutputStream output = new FileOutputStream(outputFile);
        long total = 0;
        long startTime = System.currentTimeMillis();
        int timeCount = 1;
        while ((count = bis.read(data)) != -1) {

            total += count;
            int totalFileSize = (int) (fileSize / (Math.pow(1024, 2)));
            double current = Math.round(total / (Math.pow(1024, 2)));

            int progress = (int) ((total * 100) / fileSize);

            long currentTime = System.currentTimeMillis() - startTime;

            Download download = new Download();
            download.setTotalFileSize(totalFileSize);

            if (currentTime > 1000 * timeCount) {

                download.setCurrentFileSize((int) current);
                download.setProgress(progress);
               timeCount++;
            }

            output.write(data, 0, count);
        }

        onDownloadComplete(id);
        output.flush();
        output.close();
        bis.close();

        return true;
    }

    private void onDownloadComplete(String id) {
        String zipFile = Global.AppDownloadFolderPath + "/" + id + "/file.zip";
        String unzipLocation = Global.AppFolderPath + id;

        UnzipUtility unzipper = new UnzipUtility();
        try {
            unzipper.unzip(id, zipFile, unzipLocation);
        } catch (Exception ex) {
            // some errors occurred
            ex.printStackTrace();
        }
    }
}
