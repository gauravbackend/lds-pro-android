package com.ldsscriptures.pro.utils;

import android.app.Activity;
import android.app.Dialog;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

import com.ldsscriptures.pro.R;

/**
 * Created by IBL InfoTech on 10/16/2017.
 */

public class ViewDialog {

    Dialog dialog;

    public void showDialog(Activity activity, String msg) {
        dialog = new Dialog(activity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.custom_dialog);

        GifView gifView1 = (GifView) dialog.findViewById(R.id.gif1);
        gifView1.play();

        TextView text = (TextView) dialog.findViewById(R.id.text_dialog);
        text.setText(msg);

        dialog.show();

    }

    public void dimissDialog(Activity activity) {
        dialog.dismiss();
    }
}
